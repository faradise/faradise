﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.Marketplace.MarketplaceController
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class UploadYml
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static int _categoryId;
        private static int _companyId;
        private static UploadResultModel _uploadResultModel;
        private static CompanyCategoriesModel _companyCategories;
        private static int _roomId;
        private static int _colorId;

        [Fact, TestPriority(3)]
        public async Task CreateCompany()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/create-company";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var companyModel = ServerHttpClient.HandleResponse<CreatedCompanyModel>(response, "Create company");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            _companyId = companyModel.Id;
        }

        [Fact, TestPriority(4)]
        public async Task ModifyCompany()
        {
            ModifyCompanyModel model = new ModifyCompanyModel() { Id = _companyId, Name = "TestCompanyName" };
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/modify-company";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var isModify = ServerHttpClient.HandleResponse<bool>(response, "Modify company");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isModify == true, $"Company not modify, route: {url}");
        }

        [Fact, TestPriority(5)]
        public async Task UploadTestYml()
        {
            byte[] bytes = File.ReadAllBytes("Data/yandex_market_price_08.08.18.xml");
            Assert.True(bytes.Length != 0, $"file not loaded");

            var content = new ByteArrayContent(bytes);
            var multipart = new MultipartFormDataContent();
            multipart.Add(content, "file", "yandex_market_price_08.08.18.xml");
            multipart.Add(new StringContent(_companyId.ToString()), "id");

            var url = TestConfig.BaseUrl + "/api/admin/marketplace/upload-yml";

            var response = await _client.MakePostMultipartFormRequestAsync(null, url, multipart);

            var uploadResultModel = ServerHttpClient.HandleResponse<UploadResultModel>(response, "Upload yml");
            //Assert.True(false, JsonConvert.SerializeObject(uploadResultModel));
            Assert.True((int)response.StatusCode == 200, $"Status code not OK: {(int)response.StatusCode}");
            Assert.True(uploadResultModel != null, $"UploadResultModel is null");
            _uploadResultModel = uploadResultModel;
        }

        [Fact, TestPriority(6)]
        public async Task AcceptYmlUpload()
        {
            PostProcessUploadModel model = new PostProcessUploadModel() { Id = _uploadResultModel.CompanyId, Upload = _uploadResultModel.Upload };
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/accept-upload-sync";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var isUpload = ServerHttpClient.HandleResponse<bool>(response, "Accept upload");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isUpload == true, $"Yml not upload, route: {url}");
        }

        [Fact, TestPriority(7)]
        public async Task GetCompanyCategories()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/get-company-categories";
            var response = await _client.MakeGetRequestAsync(null, url, new Dictionary<string, string>() { { "companyId", _companyId.ToString() } });
            var companyCategories = ServerHttpClient.HandleResponse<CompanyCategoriesModel>(response, "Get company categories");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(companyCategories != null, "CompanyCategories is null");
            _companyCategories = companyCategories;
        }

        [Fact, TestPriority(8)]
        public async Task CreateCategoriesForCompany()
        {
            foreach (var category in _companyCategories.Categories)
            {
                CreateCategoryModel model = new CreateCategoryModel() { Name = category.CompanyCategory + "_faradise" };

                var url = TestConfig.BaseUrl + "/api/admin/marketplace/create-category";
                var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
                var categoryId = ServerHttpClient.HandleResponse<int>(response, "Create category");
                Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
                category.Category = categoryId;
            }
        }

        [Fact, TestPriority(9)]
        public async Task MapCompanyCategories()
        {
            CompanyCategoriesModel model = new CompanyCategoriesModel() { CompanyId = _companyId, Categories = _companyCategories.Categories };
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/map-company-categories";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var isMap = ServerHttpClient.HandleResponse<bool>(response, "Map company categories");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isMap == true, $"Categories not mapped, route: {url}");
        }

        [Fact, TestPriority(10)]
        public async Task SetParentCategories()
        {
            foreach (var category in _companyCategories.Categories)
            {
                if (category.ParentCompanyCategoryId <= 0)
                    continue;
                var companyParentCategory = _companyCategories.Categories.FirstOrDefault(x => x.CompanyCategoryId == category.ParentCompanyCategoryId);
                if (companyParentCategory == null)
                    continue;
                ModifyCategoryModel model = new ModifyCategoryModel() { Id = category.Category, ParentId = companyParentCategory.Category };
                var url = TestConfig.BaseUrl + "/api/admin/marketplace/modify-category";
                var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
                var isModify = ServerHttpClient.HandleResponse<bool>(response, "Modify category");
                Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
                Assert.True(isModify == true, $"Category not modify, route: {url}");
            }
        }

        [Fact, TestPriority(11)]
        public async Task CreateTestRoom()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/add-category-room";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject($"TestRoom_{DateTime.Now.ToLongTimeString()}"));
            var isModify = ServerHttpClient.HandleResponse<bool>(response, "Create room");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isModify == true, $"Room not created, route: {url}");
        }

        [Fact, TestPriority(12)]
        public async Task GetRooms()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/get-category-rooms";
            var response = await _client.MakeGetRequestAsync(null, url, new Dictionary<string, string>());
            var rooms = ServerHttpClient.HandleResponse<RoomNameModel[]>(response, "Get rooms");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(rooms != null || rooms.Length == 0, "Rooms is null or empty");
            _roomId = rooms[0].RoomNameId;
        }

        [Fact, TestPriority(13)]
        public async Task MapRooms()
        {
            CompanyCategoryRoomModel model = new CompanyCategoryRoomModel() { RoomNameId = _roomId, CompanyCategoryId = _companyCategories.Categories.FirstOrDefault(x => x.Products > 0).CompanyCategoryId, CompanyId = _companyCategories.CompanyId };
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/add-company-category-room";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var isMap = ServerHttpClient.HandleResponse<bool>(response, "Map room categories");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isMap == true, $"Room not mapped, route: {url}");
        }

        [Fact, TestPriority(14)]
        public async Task CreateTestColor()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/add-marketplace-color";
            var addModel = new MarketplaceColorModel() { Name = $"TestColor_{DateTime.Now.ToLongTimeString()}", Hex = "11111" };
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(addModel));
            var isModify = ServerHttpClient.HandleResponse<bool>(response, "Create color");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isModify == true, $"Color not created, route: {url}");
        }

        [Fact, TestPriority(15)]
        public async Task GetColors()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/get-marketplace-colors";
            var response = await _client.MakeGetRequestAsync(null, url, new Dictionary<string, string>());
            var colors = ServerHttpClient.HandleResponse<MarketplaceColorModel[]>(response, "Get colors");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(colors != null || colors.Length == 0, "Colors is null or empty");
            _colorId = colors[0].ColorId;
        }

        [Fact, TestPriority(16)]
        public async Task MapColors()
        {
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/get-company-colors";
            var response = await _client.MakeGetRequestAsync(null, url, new Dictionary<string, string>());
            var companyColors = ServerHttpClient.HandleResponse<CompanyColorModel[]>(response, "Get company colors");
            Assert.True(companyColors != null && companyColors.Length > 0, "Company colors are empty");
            url = TestConfig.BaseUrl + "/api/admin/marketplace/map-company-color";
            var mapColor = new MapColorModel() { CompanyColorId = companyColors[0].Id, MarketplaceColorId = _colorId };
            response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(mapColor));
            var isMap = ServerHttpClient.HandleResponse<bool>(response, "Map colors categories");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isMap == true, $"Colors not mapped, route: {url}");
        }

        [Fact, TestPriority(17)]
        public async Task CreateCategory()
        {
            CreateCategoryModel model = new CreateCategoryModel() { Name = "TestCategory" };

            var url = TestConfig.BaseUrl + "/api/admin/marketplace/create-category";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var categoryId = ServerHttpClient.HandleResponse<int>(response, "Create category");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            _categoryId = categoryId;
        }

        [Fact, TestPriority(18)]
        public async Task ModifyCategory()
        {
            ModifyCategoryModel model = new ModifyCategoryModel() { Id = _categoryId, Name = "ModifyTestCategory" };
            var url = TestConfig.BaseUrl + "/api/admin/marketplace/modify-category";
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(model));
            var isModify = ServerHttpClient.HandleResponse<bool>(response, "Modify category");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(isModify == true, $"Category not modify, route: {url}");
        }
    }
}
