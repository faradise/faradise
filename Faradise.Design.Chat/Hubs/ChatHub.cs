﻿using Faradise.Design.Chat.Models.Enums;
using Microsoft.AspNetCore.SignalR;
using SignalR.Hubs.Models;
using SignalR.Services;
using System;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
    public class ChatHub : Hub
    {
        private IUserService _userService = null;
        private IMessageService _messageService = null;

        public ChatHub(IUserService userService, IMessageService messageService)
        {
            _userService = userService;
            _messageService = messageService;
        }

        public async Task Send(MessageModel message)
        {
            await Task.Run(() =>
            {
                var user = _userService.GetUser(Context.ConnectionId);
                if (!user.Validated)
                    _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
                else
                {
                    var group = user.Groups.Find(x => x.Name == message.GroupName);
                    if (group == null)
                        _messageService.ErrorMessage(Context.ConnectionId, "failed_joining_group");
                    else
                    {
                        if (message.Links.Length == 0)
                            _messageService.AddMessage(MessageType.Text, user, group, message.Text, message.Links);
                        else
                            _messageService.AddMessage(MessageType.Photo, user, group, message.Text, message.Links);
                    }
                }
            });
        }

        public async Task CallAdmin(int GroupName)
        {
            await Task.Run(() =>
            {
                var user = _userService.GetUser(Context.ConnectionId);
                if (!user.Validated)
                    _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
                else
                {
                    var group = user.Groups.Find(x => x.Name == GroupName);
                    if (group == null)
                        _messageService.ErrorMessage(Context.ConnectionId, "failed_joining_group");
                    else
                    {
                            _messageService.AddMessage(MessageType.CallAdmin, user, group, string.Empty, new string[0]);
                    }
                }
            });
        }

        public async Task AdminLeft(int GroupName)
        {
            await Task.Run(() =>
            {
                var user = _userService.GetUser(Context.ConnectionId);
                if (!user.Validated)
                    _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
                else
                {
                    if (user.UserId > 0)
                        _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
                    else
                    {
                        var group = user.Groups.Find(x => x.Name == GroupName);
                        if (group == null)
                            _messageService.ErrorMessage(Context.ConnectionId, "failed_joining_group");
                        else
                        {
                            _messageService.AddMessage(MessageType.AdminLeft, user, group, string.Empty, new string[0]);
                        }
                    }
                }
            });
        }

        public async Task LoadLastMessages(int groupName, int offset, int count)
        {
            var user = _userService.GetUser(Context.ConnectionId);
            if (!user.Validated)
                _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
            else
            {
                var group = user.Groups.Find(x => x.Name == groupName);
                if (group == null)
                    _messageService.ErrorMessage(Context.ConnectionId, "failed_joining_group");
                else
                {
                    var messages = _messageService.GetLastMessages(groupName, offset, count);
                    await Clients.Caller.SendAsync("Messages", messages);
                }
            }
        }

        public async Task LoadMessagesById(int groupName, int id, int count)
        {
            var user = _userService.GetUser(Context.ConnectionId);
            if (!user.Validated)
                _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
            else
            {
                var group = user.Groups.Find(x => x.Name == groupName);
                if (group == null)
                    _messageService.ErrorMessage(Context.ConnectionId, "failed_joining_group");
                else
                {
                    var messages = _messageService.GetMessagesById(groupName, id, count);
                    await Clients.Caller.SendAsync("Messages", messages);
                }
            }
        }

        public async Task JoinGroup(int groupName)
        {
            var user = _userService.GetUser(Context.ConnectionId);
            if (!user.Validated)
                _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
            else
            {
                var joined = await _userService.JoinGroup(groupName, user);
                if (joined)
                    await Clients.Caller.SendAsync("Joined", groupName);
            }
        }

        public async Task LeaveGroup(int groupName)
        {
            await Task.Run(() =>
            {
                var user = _userService.GetUser(Context.ConnectionId);
                if (!user.Validated)
                    _messageService.ErrorMessage(Context.ConnectionId, "user_not_validated");
                else _userService.LeaveGroup(groupName, user);
            });
        }

        public async Task Authorize(string token)
        {
            await Task.Run(() =>
            {
                var authorized = _userService.Authorize(token, Context.ConnectionId);
                if (!authorized)
                {
                    _messageService.ErrorMessage(Context.ConnectionId, "failed_authorization");
                    Context.Abort();
                }
            });
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.SendAsync("Connected");
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Task.Run(() =>
            {
                _userService.UserDisconnected(Context.ConnectionId);
            });
        }
    }
}
