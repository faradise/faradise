﻿using System.ComponentModel.DataAnnotations;

namespace SignalR.Hubs.Models
{
    public class MessageModel
    {
        /// <summary>
        /// Текст сообщения
        /// </summary>
        [Required]
        public string Text { get; set; }

        /// <summary>
        /// Ссылки на фото
        /// </summary>
        public string[] Links { get; set; }

        /// <summary>
        /// Группа
        /// </summary>
        [Required]
        public int GroupName { get; set; }
    }
}
