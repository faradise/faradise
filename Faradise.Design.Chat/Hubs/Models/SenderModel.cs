﻿namespace SignalR.Hubs.Models
{
    public class SenderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
