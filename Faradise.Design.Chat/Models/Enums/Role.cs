﻿namespace SignalR.Models.Enums
{
    public enum Role
    {
        Empty = 0,
        Unauthorized = 1,
        Authorized = 2,
        Client = 4,
        Designer = 8,
        Administrator = 16
    }
}
