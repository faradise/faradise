﻿namespace Faradise.Design.Chat.Models.Enums
{
    public enum MessageType
    {
        Text,
        Photo,
        CallAdmin,
        AdminLeft
    }
}
