﻿using System.Collections.Generic;

namespace SignalR.Models
{
    public class Group
    {
        public int Name { get; set; }
        public List<User> Users { get; set; }
    }
}
