﻿using System;

namespace Faradise.Design.Chat.Models
{
    public interface IMessage
    {
        string Type { get; set; }
        int MessageId { get; set; }
        int SenderId { get; set; }
        string Text { get; set; }
        DateTime TimeCreate { get; set; }
        string[] Links { get; set; }
    }
}
