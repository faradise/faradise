﻿using Faradise.Design.Chat.Models;
using System;

namespace SignalR.Models
{
    public class MessagePhoto : IMessage
    {
        public string Type { get; set; }
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public string Text { get; set; }
        public string[] Links { get; set; }
        public DateTime TimeCreate { get; set; }
    }
}
