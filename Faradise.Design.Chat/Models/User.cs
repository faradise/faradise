﻿using System.Collections.Generic;

namespace SignalR.Models
{
    public class User
    {
        public User()
        {
            Groups = new List<Group>();
        }

        public int UserId { get; set; }
        public string SignalId { get; set; }
        public bool Validated { get; set; }
        public string Name { get; set; }
        public List<Group> Groups { get; set; }
    }
}
