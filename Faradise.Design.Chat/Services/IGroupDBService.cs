﻿using SignalR.Models;

namespace SignalR.Services
{
    public interface IGroupDBService
    {
        Group AddToGroup(int groupName, User user);
        Group LeaveGroup(int groupName, User user);
        Group GetGroup(int groupName);
    }
}
