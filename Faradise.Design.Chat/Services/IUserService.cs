﻿using SignalR.Models;
using System.Threading.Tasks;

namespace SignalR.Services
{
    public interface IUserService
    {
        bool Authorize(string token, string signalId);
        void UserDisconnected(string signalId);
        Task<bool> JoinGroup(int groupName, User user);
        void LeaveGroup(int groupName, User user);
        User GetUser(string signalId);
        User GetUserById(int id);
    }
}
