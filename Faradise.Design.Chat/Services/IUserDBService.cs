﻿using SignalR.Models;

namespace SignalR.Services
{
    public interface IUserDBService
    {
        void AddUser(User user);
        void UpdateUser(User user);
        User GetUser(string signalId);
        User GetUserById(int id);
        void DeleteUser(string signalId);
    }
}
