﻿using Faradise.Design.Chat.Models;
using SignalR.Models;
using System;
using System.Collections.Generic;

namespace SignalR.Services
{
    public interface IMessageDBService
    {
        IMessage AddMessage(int groupName, IMessage message);
        IMessage[] GetLastMessages(int groupName, int offset, int count);
        IMessage[] GetMessagesById(int groupName, int id, int count);
        Dictionary<int,DateTime> GetLastServerNotificationTimes(int groupName);
        void SetUserAsNotifiedNow(int groupName, int[] users);
        int[] GetGroupUsers(int name);
    }
}
