﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Faradise.Design.Chat.Models;
using Faradise.Design.Controllers.API.Models.Chat;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using SignalR.Hubs;
using SignalR.Models;
using System.Linq;
using Faradise.Design.Chat.Models.Enums;

namespace SignalR.Services.Implementation
{
    public class MessageService : IMessageService
    {
        public IMessageDBService _messageDBService = null;
        private IHubContext<ChatHub> _hubContext = null;

        public MessageService(IMessageDBService messageDBService, IHubContext<ChatHub> hubContext)
        {
            _messageDBService = messageDBService;
            _hubContext = hubContext;
        }

        public async void AddMessage(MessageType type, User user, Group group, string text, string[] links)
        {
            IMessage message = null;
            switch (type)
            {
                case MessageType.Text:
                    message = _messageDBService.AddMessage(group.Name, new Message { Type = "message", SenderId = user.UserId, Text = text, TimeCreate = DateTime.UtcNow, Links = new string[0] });
                    break;
                case MessageType.Photo:
                    message = _messageDBService.AddMessage(group.Name, new MessagePhoto { Type = "message/photo", SenderId = user.UserId, Text = text, Links = links, TimeCreate = DateTime.UtcNow });
                    break;
                case MessageType.CallAdmin:
                    message = _messageDBService.AddMessage(group.Name, new MessagePhoto { Type = "system/admin-called", SenderId = user.UserId, Text = text, Links = links, TimeCreate = DateTime.UtcNow });
                    break;
                case MessageType.AdminLeft:
                    message = _messageDBService.AddMessage(group.Name, new MessagePhoto { Type = "system/admin-left", SenderId = user.UserId, Text = text, Links = links, TimeCreate = DateTime.UtcNow });
                    break;
            }
            var notifications = _messageDBService.GetLastServerNotificationTimes(group.Name);

            var usersToNotify = new Dictionary<int,bool>(3);
            var usersIds = _messageDBService.GetGroupUsers(group.Name);
            if(usersIds!=null)
            {
                var usersIdsList = usersIds.ToList();
                usersIdsList.RemoveAll(x => !group.Users.Select(z=>z.UserId).Any(y => y == x));
                if(usersIdsList.Any())
                {
                    foreach(var id in usersIdsList)
                    {
                        DateTime notifyTime = DateTime.MinValue;
                        if (id == user.UserId)
                            continue;
                        bool hasNotification = notifications.TryGetValue(id, out notifyTime);
                        if (!hasNotification || (DateTime.UtcNow - notifyTime).TotalHours >= 1)
                        {
                            if (!usersToNotify.ContainsKey(id))
                                usersToNotify.Add(id, hasNotification);
                        }
                    }
                }
            };
            foreach (var u in group.Users)
            {
                await _hubContext.Clients.Client(u.SignalId).SendAsync("Message", message);
                DateTime notifyTime = DateTime.MinValue;
                if (u.UserId == user.UserId)
                    continue;
                bool hasNotification = notifications.TryGetValue(u.UserId, out notifyTime);
                if (!hasNotification || (DateTime.UtcNow - notifyTime).TotalHours >= 1)
                {
                    if(!usersToNotify.ContainsKey(u.UserId))
                        usersToNotify.Add(u.UserId, hasNotification);
                }
            }
            if (usersToNotify.Count > 0)
                NotifyUsersAboutMessages(group.Name, usersToNotify);
        }

        private async void NotifyUsersAboutMessages(int groupName, Dictionary<int, bool> notifyUsers)
        {
            var notifyGroupModel = new NotifyGroupModel();
            notifyGroupModel.ProjectId = groupName;
            notifyGroupModel.Users = new int[notifyUsers.Count];
            notifyGroupModel.IsFirstMessage = new bool[notifyUsers.Count];
            int userIndex = 0;
            foreach(var notify in notifyUsers)
            {
                notifyGroupModel.Users[userIndex] = notify.Key;
                notifyGroupModel.IsFirstMessage[userIndex] = !notify.Value;
                userIndex++;
            }
            var jsonString = JsonConvert.SerializeObject(notifyGroupModel);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.PostAsync("http://faradise.design/api/messenger/notify-about-message", content);
            }
            _messageDBService.SetUserAsNotifiedNow(groupName, notifyGroupModel.Users);
        }

        public IMessage[] GetLastMessages(int groupName, int offset, int count)
        {
            return _messageDBService.GetLastMessages(groupName, offset, count);
        }

        public IMessage[] GetMessagesById(int groupName, int id, int count)
        {
            return _messageDBService.GetMessagesById(groupName, id, count);
        }

        public async void ErrorMessage(string signalId, string message)
        {
            await _hubContext.Clients.Client(signalId).SendAsync("Error", message);
        }
    }
}
