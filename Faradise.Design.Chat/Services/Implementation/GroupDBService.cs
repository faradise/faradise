﻿using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Services.Implementation
{
    public class GroupDBService : IGroupDBService
    {
        private List<Group> _groups = new List<Group>();
        private readonly FaradiseDBContext _context;

        public GroupDBService(FaradiseDBContext db)
        {
            _context = db;
        }
        public Group AddToGroup(int groupName, User user)
        {
            var group = _groups.Find(x => x.Name == groupName);
            if(group != null)
            {
                if(group.Users.Find(x => string.Equals(x.SignalId, user.SignalId)) == null)
                    group.Users.Add(user);
                var userEntity = _context.ChatUsers.FirstOrDefault(x => x.GroupId == groupName && x.UserId == user.UserId);
                if(userEntity==null)
                {
                    userEntity = new ChatGroupUserEntity
                    {
                        GroupId = groupName,
                        UserId = user.UserId
                    };
                    _context.ChatUsers.Add(userEntity);
                    _context.SaveChanges();
                }
                return group;
            }
            else
            {
                List<User> users = new List<User>();
                users.Add(user);
                group = new Group { Name = groupName, Users = users };
                _groups.Add(group);
                return group;
            }            
        }

        public Group GetGroup(int groupName)
        {
            return _groups.Find(x => x.Name == groupName);
        }

        public Group LeaveGroup(int groupName, User userModel)
        {
            var group = _groups.Find(x => x.Name == groupName);
            if(group != null)
            {
                var user = group.Users.Find(x => string.Equals(x.SignalId, userModel.SignalId));
                if (user != null)
                {
                    group.Users.Remove(user);
                    return group;
                }
            }
            var userEntity=_context.ChatUsers.FirstOrDefault(x => x.GroupId == groupName && x.UserId == userModel.UserId);
            if(userEntity!=null)
            {
                _context.ChatUsers.Remove(userEntity);
                _context.SaveChanges();
            }
            return null;
        }
    }
}
