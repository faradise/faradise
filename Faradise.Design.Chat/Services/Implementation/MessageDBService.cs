﻿using Faradise.Design.Chat.Models;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Newtonsoft.Json;
using SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Services.Implementation
{
    public class MessageDBService:IMessageDBService
    {
        private readonly FaradiseDBContext _context;

        public MessageDBService(FaradiseDBContext db)
        {
            _context = db;
        }

        public IMessage AddMessage(int groupName, IMessage message)
        {
            int? senderId = null;
            if (message.SenderId > 0)
                senderId = message.SenderId;

            var group = _context.ChatGroups.Find(groupName);
            if (group == null)
            {
                group = new MessageGroupEntity() { GroupId = groupName };
                _context.ChatGroups.Add(group);
                _context.SaveChanges();
            }
            var messageEntity = new ChatMessageEntity
            {
                GroupId = groupName,
                Type = message.Type,
                SenderId = senderId,
                Text=message.Text!=null?message.Text:string.Empty,
                TimeCreate = message.TimeCreate
            };
            _context.ChatMessages.Add(messageEntity);
            _context.SaveChanges();
            if(message.Links.Length>0)
            {
                foreach(var photo in message.Links)
                {
                    var entity = new ChatMessagePhotoEntity
                    {
                        MessageId = messageEntity.Id,
                        Link = photo
                    };
                    _context.ChatMessagePhotos.Add(entity);
                    _context.SaveChanges();
                }
            }
            message.MessageId = messageEntity.Id;
            return message;
        }

        public int[] GetGroupUsers(int name)
        {
            var ids = _context.ChatUsers.Where(x => x.GroupId == name)?.Select(x => x.UserId).ToArray();
            return ids ;
        }

        public IMessage[] GetLastMessages(int groupName, int offset, int count)
        {
            var messages = _context.ChatMessages
                .Where(x => x.GroupId == groupName)
                .OrderByDescending(x => x.Id)
                .Skip(offset)
                .Take(count)
                .OrderBy(x => x.Id)
                .GroupJoin(
                    _context.ChatMessagePhotos.DefaultIfEmpty(),
                    x => x.Id,
                    y => y.MessageId,
                    (x, y) => new { Message = x, Photos = y })
                .ToArray();

            var result = messages
                .Select(x => new Message
                {
                    MessageId = x.Message.Id,
                    SenderId = x.Message.SenderId ?? -1,
                    Text = x.Message.Text,
                    Type = x.Message.Type,
                    TimeCreate = x.Message.TimeCreate,
                    Links = x.Photos?.Select(p => p.Link).ToArray()
                })
                .ToArray();

            return result;
        }

        public Dictionary<int, DateTime> GetLastServerNotificationTimes(int groupName)
        {
            var group = _context.ChatGroups.Find(groupName);
            if (group == null)
                return null;
            return JsonConvert.DeserializeObject<Dictionary<int, DateTime>>(group.LastServerNotificationTime);
        }

        // Ну не GetMessagesById, а несколько мессаджей, предшествующих указанному
        public IMessage[] GetMessagesById(int groupName, int id, int count)
        {
            var messages = _context.ChatMessages
                .Where(x => x.GroupId == groupName)
                .Where(x => x.Id < id)
                .OrderByDescending(x => x.Id)
                .Take(count)
                .OrderBy(x => x.Id)
                .GroupJoin(
                    _context.ChatMessagePhotos.DefaultIfEmpty(),
                    x => x.Id,
                    y => y.MessageId,
                    (x, y) => new { Message = x, Photos = y })
                .ToArray();
            
            var result = messages
                .Select(x => new Message
                {
                    MessageId = x.Message.Id,
                    SenderId = x.Message.SenderId ?? -1,
                    Text = x.Message.Text,
                    Type = x.Message.Type,
                    TimeCreate = x.Message.TimeCreate,
                    Links = x.Photos?.Select(p => p.Link).ToArray()
                })
                .ToArray();

            return result;
        }

        // userdIds, not users
        public void SetUserAsNotifiedNow(int groupName, int[] users)
        {
            var group = _context.ChatGroups.Find(groupName);
            if (group == null)
                return;
            var lastServerNotificationTime = JsonConvert.DeserializeObject<Dictionary<int, DateTime>>(group.LastServerNotificationTime);
            foreach (var userId in users)
            {
                if (!lastServerNotificationTime.ContainsKey(userId))
                    lastServerNotificationTime.Add(userId, DateTime.UtcNow);
                else
                    lastServerNotificationTime[userId] = DateTime.UtcNow;
            }
            group.LastServerNotificationTime = JsonConvert.SerializeObject(lastServerNotificationTime);
            _context.SaveChanges();
        }

        public IMessage ToModel(ChatMessageEntity entity)
        {
            return new Message
            {
                MessageId = entity.Id,
                SenderId = entity.SenderId != null ? entity.SenderId.Value : -1,
                Text = entity.Text,
                Type = entity.Type,
                TimeCreate = entity.TimeCreate
            };
        }
    }
}
