﻿using Faradise.Design.Services;
using SignalR.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Json;

namespace SignalR.Services.Implementation
{
    public class UserService : IUserService
    {
        private ITokenService _tokenService = null;
        private IUserDBService _userDBService = null;
        private IGroupDBService _groupDBService = null;
        private IMessageService _messageService = null;

        public UserService(
            ITokenService tokenService, 
            IUserDBService userDBService,
            IGroupDBService groupDBService,
            IMessageService messageService)
        {
            _tokenService = tokenService;
            _userDBService = userDBService;
            _groupDBService = groupDBService;
            _messageService = messageService;
        }

        public bool Authorize(string token, string signalId)
        {
            var validatedToken = _tokenService.GetClaims(token);
            if (validatedToken != null)
            {
                var id = validatedToken.FindFirst("id");
                var intId = int.Parse(id.Value);

                _userDBService.AddUser(new User { UserId = intId, SignalId = signalId, Validated = true });

                return true;
            }
            else return false;
        }

        public User GetUser(string signalId)
        {
            return _userDBService.GetUser(signalId);
        }

        public User GetUserById(int id)
        {
            return _userDBService.GetUserById(id);
        }

        public async Task<bool> JoinGroup(int groupName, User user)
        {
            var values = new Dictionary<string, string>
            {
                { "groupId", groupName.ToString() },
                { "userId", user.UserId.ToString() }
            };

            var content = new FormUrlEncodedContent(values);
            var responseString = string.Empty;

            using (var client = new HttpClient())
            {
                var response = await client.PostAsync("http://faradise.design/api/messenger/confirm-user-on-group", content);
                responseString = await response.Content.ReadAsStringAsync();
            }

            var json = JsonValue.Parse(responseString);
            var code = "";
            var result = false;

            if (json.ContainsKey("code"))
                code = json["code"];

            if (json.ContainsKey("result"))
                result = json["result"];
            
            if (string.Equals(code, "ok") && result)
            {
                var group = _groupDBService.AddToGroup(groupName, user);
                user.Groups.Add(group);

                return true;
            }
            else
            {
                _messageService.ErrorMessage(user.SignalId, "failed_joining_group");

                return false;
            }
        }

        public void LeaveGroup(int groupName, User user)
        {
            var group = _groupDBService.LeaveGroup(groupName, user);
            user.Groups.Remove(group);
        }

        public void UserDisconnected(string signalId)
        {
            var user = GetUser(signalId);
            foreach(var group in user.Groups)
            {
                _groupDBService.LeaveGroup(group.Name, user);
            }
            user.Groups.Clear();

            _userDBService.DeleteUser(signalId);
        }
    }
}
