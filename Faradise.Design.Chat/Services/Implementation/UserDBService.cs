﻿using System.Collections.Generic;
using SignalR.Models;

namespace SignalR.Services.Implementation
{
    public class UserDBService : IUserDBService
    {
        private List<User> _users = new List<User>();

        public void AddUser(User user)
        {
            _users.Add(user);
        }

        public void DeleteUser(string signalId)
        {
            _users.RemoveAll(x => string.Equals(x.SignalId, signalId));
        }

        public User GetUser(string signalId)
        {
            return _users.Find(x => string.Equals(x.SignalId, signalId));
        }

        public User GetUserById(int id)
        {
            return _users.Find(x => x.UserId == id);
        }

        public void UpdateUser(User user)
        {
            var oldUser = _users.Find(x => x.UserId == user.UserId);
            oldUser.Name = user.Name;
            oldUser.Groups = user.Groups;
        }
    }
}
