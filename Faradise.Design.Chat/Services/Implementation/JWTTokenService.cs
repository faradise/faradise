﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Faradise.Design.Services.Implementation
{
    public class JWTTokenService : ITokenService
    {
        public string RequestToken(int id)
        {
            var sec = "mysupersecret_secretkey!123";
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(sec));

            var identity = new ClaimsIdentity(new GenericIdentity("identity"), new[] { new Claim("id", id.ToString()) });

            var jwt = new JwtSecurityToken(
                    audience: "myApp",
                    issuer: "issuer",
                    claims: identity.Claims,
                    expires: DateTime.UtcNow.AddMonths(2),
                    signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public bool ValidateToken(string token)
        {
            var principal = Validate(token);
            return principal != null ? true : false;
        }

        public ClaimsPrincipal GetClaims(string token)
        {
            return Validate(token);
        }

        private ClaimsPrincipal Validate(string token)
        {
            if (string.IsNullOrEmpty(token))
                return null;

            var handler = new JwtSecurityTokenHandler();

            var sec = "mysupersecret_secretkey!123";
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(sec));

            var parameters = new TokenValidationParameters
            {
                ValidAudience = "myApp",
                ValidIssuer = "issuer",
                IssuerSigningKey = securityKey,
                ValidateLifetime = true
            };

            SecurityToken validatedToken = null;
            ClaimsPrincipal principal = null;
            try
            {
                principal = handler.ValidateToken(token, parameters, out validatedToken);
            }
            catch { }

            if (principal == null)
                throw new Exception("The token is invalid");
            return principal;
        }
    }
}
