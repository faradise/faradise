﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Chat.Models;
using SignalR.Models;

namespace SignalR.Services.Implementation
{
    public class MockMessageDBService : IMessageDBService
    {
        private class MessageGroup
        {
            public MessageGroup(int groupName)
            {
                GroupName = groupName;
                Messages = new List<IMessage>();
                LastServerNotificationTime = new Dictionary<int, DateTime>();
            }
            public int GroupName;
            public List<IMessage> Messages;
            public Dictionary<int, DateTime> LastServerNotificationTime;
        }

        private List<MessageGroup> Groups = new List<MessageGroup>();

        public IMessage AddMessage(int groupName, IMessage message)
        {
            var group = Groups.FirstOrDefault(x => x.GroupName == groupName);
            if(group == null)
            {
                group = new MessageGroup(groupName);
                Groups.Add(group);
            }
            message.MessageId = group.Messages.Count;
            group.Messages.Add(message);
            return message;
        }


        public IMessage[] GetLastMessages(int groupName, int offset, int count)
        {
            var group = Groups.FirstOrDefault(x => x.GroupName == groupName);
            if (group == null)
                return null;

            var messages = group.Messages.FindAll(x => x.MessageId < group.Messages.Count - offset && x.MessageId >= group.Messages.Count - offset - count);
            return messages.ToArray();
        }

        public IMessage[] GetMessagesById(int groupName, int id, int count)
        {
            var group = Groups.FirstOrDefault(x => x.GroupName == groupName);
            if (group == null)
                return null;

            var messages = group.Messages.FindAll(x => x.MessageId < id && x.MessageId >= id - count);
            return messages.ToArray();
        }

        public Dictionary<int, DateTime> GetLastServerNotificationTimes(int groupName)
        {
            var group = Groups.FirstOrDefault(x => x.GroupName == groupName);
            if (group == null)
                return null;
            return group.LastServerNotificationTime;
        }

        public void SetUserAsNotifiedNow(int groupName, int[] users)
        {
            var group = Groups.FirstOrDefault(x => x.GroupName == groupName);
            if (group == null)
                return;
            foreach (var user in users)
            {
                if (!group.LastServerNotificationTime.ContainsKey(user))
                    group.LastServerNotificationTime.Add(user, DateTime.UtcNow);
                else
                    group.LastServerNotificationTime[user] = DateTime.UtcNow;
            }
        }

        public int[] GetGroupUsers(int name)
        {
            throw new NotImplementedException();
        }
    }
}
