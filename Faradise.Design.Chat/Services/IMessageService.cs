﻿using Faradise.Design.Chat.Models;
using Faradise.Design.Chat.Models.Enums;
using SignalR.Models;

namespace SignalR.Services
{
    public interface IMessageService
    {
        void AddMessage(MessageType type, User user, Group group, string text, string[] links);
        IMessage[] GetLastMessages(int groupName, int offset, int count);
        IMessage[] GetMessagesById(int groupName, int id, int count);
        void ErrorMessage(string signalId, string message);
    }
}
