﻿using System.Security.Claims;

namespace Faradise.Design.Services
{
    public interface ITokenService
    {
        string RequestToken(int id);
        bool ValidateToken(string token);
        ClaimsPrincipal GetClaims(string token);
    }
}
