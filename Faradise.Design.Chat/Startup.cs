﻿using Faradise.Design.Models.DAL;
using Faradise.Design.Services;
using Faradise.Design.Services.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Hubs;
using SignalR.Services;
using SignalR.Services.Implementation;

namespace Faradise.Design.Chat
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("Cors", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));

            services.AddSingleton<IUserDBService, UserDBService>();
            services.AddSingleton<IGroupDBService, GroupDBService>();
            services.AddTransient<ITokenService, JWTTokenService>();
            services.AddTransient<SignalR.Services.IUserService, SignalR.Services.Implementation.UserService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddSingleton<IMessageDBService, MessageDBService>();
            var cs = Configuration.GetSection("DB")["ConnectionString"];
            services.AddDbContext<FaradiseDBContext>(options =>
                options
                    .UseLazyLoadingProxies()
                    .UseSqlServer(cs));
            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("Cors");
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chat");
            });
        }
    }
}
