﻿using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Marketplace.DAL
{
    public class MarketplaceDBContext : DbContext
    {
        public MarketplaceDBContext(DbContextOptions<MarketplaceDBContext> options)
            : base(options)
        {   }

        public DbSet<CompanyEntity> Companies { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
    }
}
