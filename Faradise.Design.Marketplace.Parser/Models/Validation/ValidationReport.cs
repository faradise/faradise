﻿using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Marketplace.Models.Validation
{
    public class ValidationReport
    {
        public bool Valid => !(InvalidFields.Any() || Messages.Any());

        public Dictionary<string, List<InvalidField>> InvalidFields { get; private set; } =
            new Dictionary<string, List<InvalidField>>();

        public List<string> Messages { get; set; } = new List<string>();

        public void ReportInvalidFiled(InvalidField field)
        {
            if (InvalidFields.ContainsKey(field.FieldName))
                InvalidFields[field.FieldName].Add(field);
            else
                InvalidFields.Add(field.FieldName, new List<InvalidField> {field});
        }
    }

    public class InvalidField
    {
        public InvalidField(int rowIndex, string fieldName, string reason)
        {
            RowIndex = rowIndex;
            FieldName = fieldName;
            Reason = reason;
        }

        public int RowIndex { get; }
        public string FieldName { get; }
        public string Reason { get; }
    }
}