﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Faradise.Design.Marketplace.Models.Validation
{
    public class ValidatorProcessor<T> where T : class
    {
        private PropertyInfo[] RequiredProperties =>
            typeof(T).GetProperties()
                .Where(prop => prop.IsDefined(typeof(ValidateRequiredAttribute), false))
                .ToArray();

        private readonly IValidatorDataProvider _provider;
        private ValidationReport _validationReport;

        public ValidatorProcessor(IValidatorDataProvider provider)
        {
            _provider = provider;
        }

        //Todo: 1.check start string 2.csv urls 3. fieldInvalid Reason
        public ValidationReport Validate()
        {
            _validationReport = new ValidationReport();

            foreach (var field in RequiredProperties)
            {
                var attributeData = field.GetCustomAttribute<ValidateRequiredAttribute>();

                if (!_provider.ColumnExist(field.Name))
                {
                    if (attributeData.Disjunction == string.Empty
                        || !_provider.ColumnExist(attributeData.Disjunction))
                    {
                        _validationReport.Messages.Add($"Колонка {field.Name} отсутствует");
                    }

                    continue;
                }

                bool useRegex = true;
                var regex = new Regex(".*");
                var pattern = attributeData.Pattern;
                if (pattern == string.Empty)
                    useRegex = false;
                else
                    regex = new Regex(pattern);


                foreach (var cell in _provider.ColumnCells(field.Name))
                {
                    if (string.IsNullOrEmpty(cell.value))
                    {
                        _validationReport.ReportInvalidFiled(new InvalidField(cell.rowIndex, field.Name,
                            "Ячейка не может быть пустой"));
                        continue;
                    }

                    if (!useRegex) continue;
                    if (!regex.IsMatch(cell.value))
                        _validationReport.ReportInvalidFiled(new InvalidField(cell.rowIndex, field.Name,
                            "Не верное значение ячейки"));
                }

                if (attributeData.Restrictions != null)
                {
                    foreach (Restriction restriction in attributeData.Restrictions)
                    {
                        HandleRestriction(restriction, field.Name);
                    }
                }
            }

            return _validationReport;
        }

        public void HandleRestriction(Restriction restriction, string fieldName)
        {
            switch (restriction)
            {
                case Restriction.Unique:
                {
                    if (_provider.ColumnCells(fieldName).Count(tuple => !string.IsNullOrEmpty(tuple.value)) !=
                        _provider.ColumnCells(fieldName).Where(tuple => !string.IsNullOrEmpty(tuple.value))
                            .Select(tuple => tuple.value).Distinct().Count())
                    {
                        _validationReport.Messages.Add(
                            $"Колонка {fieldName} должна содержать только уникальные значения");
                    }
                }
                    break;
            }
        }
    }
}