﻿namespace Faradise.Design.Marketplace.Models.Validation
{
    public class ValidationHelper
    {
        public const string UrlPattern =
            "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)";
        public const string BooleanPattern = "^((?i)true|false|да|нет)$";
        public const string OnlyDigits = "^[0-9]*$";
        public const string PricePattern = @"\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})?";
    }
}