﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Faradise.Design.Marketplace.Models.Validation
{
    /// <summary>
    /// Отмечает, что значение поля не может быть пустым. Свойство Pattern задаёт необходимое регулярное выражение.
    /// </summary>
    public class ValidateRequiredAttribute : Attribute
    {
        public string Pattern { get; set; } = string.Empty;
        /// <summary>
        /// Если указанное поле существует, то поле считается валидным
        /// </summary>
        public string Disjunction { get; set; } = string.Empty;
        public Restriction[] Restrictions { get; set; }
    }

    public enum Restriction
    {
        None,

        /// <summary>
        /// Каждое значение должно быть уникально в рамках документа
        /// </summary>
        Unique
    }
}