﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Faradise.Design.Marketplace.Models.Validation
{
    class XlsxValidatorDataProvider : IValidatorDataProvider
    {
        private readonly DataSet _data;
        private readonly Dictionary<string, int> _columns;

        public XlsxValidatorDataProvider(DataSet data, Dictionary<string, int> columns)
        {
            _data = data;
            _columns = columns;
        }

        public IEnumerable<(string value, int rowIndex)> ColumnCells(string column)
        {
            for (var rowIndex = 1; rowIndex < _data.Tables[0].Rows.Count; rowIndex++)
            {
                var columnIndex = _columns[column.ToLower()];
                var row = _data.Tables[0].Rows[rowIndex][columnIndex].ToString();
                yield return (row, rowIndex + 1);
            }
        }

        public bool ColumnExist(string column)
        {
            return _columns.Any(pair => pair.Key.Equals(column, StringComparison.OrdinalIgnoreCase));
        }
    }
}