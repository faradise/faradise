﻿using System.Collections.Generic;

namespace Faradise.Design.Marketplace.Models.Validation
{
    public interface IValidatorDataProvider
    {
        IEnumerable<(string value, int rowIndex)> ColumnCells(string column);
        bool ColumnExist(string column);
    }
}