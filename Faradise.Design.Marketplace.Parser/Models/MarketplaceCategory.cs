﻿using System.Collections.Generic;

namespace Faradise.Design.Marketplace.Models
{
    public class MarketplaceCategory
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        
        public string Name { get; set; }

        public int Depth { get; set; }
        public List<MarketplaceCategory> Childs { get; set; }
    }
}
