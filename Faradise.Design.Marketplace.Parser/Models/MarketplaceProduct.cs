﻿using Faradise.Design.Marketplace.Models.Validation;

namespace Faradise.Design.Marketplace.Models
{
    public class MarketplaceProduct
    {
        [ValidateRequired(Disjunction = nameof(VendorCode))]
        public string Id { get; set; }

        [ValidateRequired] public int CategoryId { get; set; }
        [ValidateRequired] public string Name { get; set; }
        [ValidateRequired] public string Description { get; set; }

        [ValidateRequired(Pattern = ValidationHelper.PricePattern)]
        public int Price { get; set; }

        public int? PreviousPrice { get; set; }

        [ValidateRequired(Pattern = ValidationHelper.UrlPattern)]
        public string Url { get; set; }

        public string[] Pictures { get; set; }

        [ValidateRequired(Pattern = ValidationHelper.BooleanPattern)]
        public bool? Available { get; set; }

        public bool? Pickup { get; set; }
        public bool? Delivery { get; set; }
        public int? DeliveryCost { get; set; }
        public string DeliveryDays { get; set; }

        public string Seller { get; set; }
        public string Vendor { get; set; }

        [ValidateRequired (Restrictions = new[] {Restriction.Unique}, Disjunction = nameof(Id))]
        public string VendorCode { get; set; }

        [ValidateRequired(Pattern = ValidationHelper.BooleanPattern)]
        public bool? Warranty { get; set; }

        [ValidateRequired] public string[] Color { get; set; }
        public string Origin { get; set; }
        public string Size { get; set; }

        public float? Width { get; set; }
        public float? Length { get; set; }
        public float? Height { get; set; }

        public string[] Notes { get; set; }
    }
}