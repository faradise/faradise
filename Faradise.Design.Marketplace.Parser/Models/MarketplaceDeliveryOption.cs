﻿namespace Faradise.Design.Marketplace.Models
{
    public class MarketplaceDeliveryOption
    {
        public int Cost { get; set; }
        public string Days { get; set; }
    }
}
