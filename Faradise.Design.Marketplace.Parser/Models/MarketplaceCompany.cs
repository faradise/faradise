﻿using System.Collections.Generic;

namespace Faradise.Design.Marketplace.Models
{
    public class MarketplaceCompany
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }

        public List<MarketplaceCategory> Categories { get; set; }
        public List<MarketplaceProduct> Products { get; set; }
    }
}
