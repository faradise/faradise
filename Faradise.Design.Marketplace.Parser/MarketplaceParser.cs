﻿using Faradise.Design.Marketplace.Models;
using Faradise.Design.Marketplace.Xml;
using System;
using System.IO;
using System.Text;

namespace Faradise.Design.Marketplace
{
    public static class MarketplaceParser
    {
        static MarketplaceParser()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        public static MarketplaceCompany FromXml(Stream stream)
        {
            string text;
            using (var sr = new StreamReader(stream, Encoding.UTF8))
            {
                var encoding = DetectEncoding(sr);

                stream.Seek(0, SeekOrigin.Begin);

                using (var srr = new StreamReader(stream, encoding))
                    text = srr.ReadToEnd();
            }

            var parser = new XmlMarketplaceParser();
            var document = parser.Parse(text);

            return MarketplaceFactory.Create(document);
        }

        public static Encoding DetectEncoding(StreamReader reader)
        {
            var text = reader.ReadToEnd();
            if (text.ToLower().Contains("windows-1251"))
                return Encoding.GetEncoding("windows-1251");
            else return Encoding.UTF8;
        }
    }
}
