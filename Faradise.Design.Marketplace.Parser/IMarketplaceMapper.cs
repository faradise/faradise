﻿using Faradise.Design.Marketplace.Models;

namespace Faradise.Design.Marketplace
{
    public interface IMarketplaceMapper<T>
    {
        MarketplaceCompany Create(T source);
    }
}
