﻿using System.Xml.Serialization;

namespace Faradise.Design.Marketplace.Xml
{
    public class XmlMarketplace
    {
        public XmlShop shop;
    }

    public class XmlShop
    {
        public string name;
        public string url;

        [XmlArray("delivery-options"), XmlArrayItem("option")] public XmlDeliveryOption[] delivery_options;
        [XmlArray("categories"), XmlArrayItem("category")] public XmlCategory[] categories;
        [XmlArray("offers"), XmlArrayItem("offer")] public XmlOffer[] offers;
    }

    public class XmlCategory
    {
        [XmlText] public string name;

        [XmlAttribute(AttributeName = "id")] public int id;
        [XmlAttribute(AttributeName = "parentId")] public int parentId;
    }

    public class XmlOffer
    {
        [XmlAttribute(AttributeName = "id")] public string id;
        [XmlAttribute(AttributeName = "available")] public string available;

        public string vendorCode;
        public string url;

        public string price;
        public string oldprice;

        public string categoryId;

        public string vendor;

        public string name;
        public string model;

        public string description;

        public string sales_notes;

        public string delivery;
        public string pickup;
        public string store;

        public string manufacturer_warranty;
        public string country_of_origin;

        public string dimensions;
        
        [XmlElement("color")] public string[] color;

        [XmlArray("delivery-options"), XmlArrayItem("option")] public XmlDeliveryOption[] delivery_options;
        [XmlElement("picture")] public XmlPicture[] picture;
        [XmlElement("param")] public XmlOfferParameter[] parameters;
        [XmlElement("param2")] public XmlOfferParameter[] parameters2;
    }

    public class XmlDeliveryOption
    {
        [XmlAttribute(AttributeName = "cost")] public string cost;
        [XmlAttribute(AttributeName = "days")] public string days;
    }

    public class XmlPicture
    {
        [XmlText] public string url;
    }

    public class XmlOfferParameter
    {
        [XmlText] public string value;

        [XmlAttribute(AttributeName = "name")] public string name;
        [XmlAttribute(AttributeName = "unit")] public string unit;
    }
}
