﻿using Faradise.Design.Marketplace.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Faradise.Design.Marketplace.Xml
{
    public class XmlMarketplaceMapper : IMarketplaceMapper<XmlShop>
    {
        private static readonly string[] ColorAliases = new string[] { "цвет" };
        private static readonly string[] OriginAliases = new string[] { "cтрана происхождения", "страна" };

        private static readonly string[] DimensionsAliases = new string[] { "размеры", "габариты", "размер" };
        
        private static readonly string[] LengthAliases = new string[] { "длина", "длинна", "length" };
        private static readonly string[] WidthAliases = new string[] { "ширина", "width" };
        private static readonly string[] HeightAliases = new string[] { "высота", "height" };
        private static readonly string[] DepthAliases = new string[] { "глубина", "глубинна", "depth" };

        private static readonly string[] IgnoreParameters = new string[]
        {
            "available",
            "price",
            "галереи",
            "<",
            "buy",
            "предоплат",
            "http"
        };

        public MarketplaceCompany Create(XmlShop xml)
        {
            var company = new MarketplaceCompany
            {
                Name = xml.name,
                Url = xml.url,
                Categories = xml.categories.Select(CreateCategory).ToList(),
                Products = xml.offers.Select(x => CreateOffer(x, xml)).ToList()
            };

            return company;
        }

        private MarketplaceProduct CreateOffer(XmlOffer source, XmlShop shop)
        {
            var p = new MarketplaceProduct();

            p.Id = source.id;
            p.CategoryId = string.IsNullOrEmpty(source.categoryId) ? 0 : Convert.ToInt32(source.categoryId);

            p.Name = source.name ?? source.model;
            p.Description = source.description;

            // ReSharper disable once PossibleInvalidOperationException
            p.Price = ParsePrice(source.price).Value;
            p.PreviousPrice = ParsePrice(source.oldprice);

            p.Url = source.url;
            p.Pictures = source.picture?.Select(x => x.url).ToArray();

            p.Available = ParseBool(source.available);

            var deliveryOptions = ParseDeliveryOptions(source, shop);
            var deliveryOption = IsNullOrEmpty(deliveryOptions) ? null : deliveryOptions.First();

            p.Pickup = ParseBool(source.pickup);
            p.Delivery = ParseBool(source.delivery);
            p.DeliveryCost = deliveryOption?.Cost;
            p.DeliveryDays = deliveryOption?.Days;

            p.Vendor = source.vendor;
            p.VendorCode = source.vendorCode ?? (!string.IsNullOrEmpty(source.name) ? source.model : string.Empty);
            p.Warranty = ParseWarranty(source.manufacturer_warranty);

            p.Color = ParseColors(source);
            p.Origin = ParseOrigin(source);

            var size = ParseSize(source);

            p.Width = ParseFloat(size.Width);
            p.Height = ParseFloat(size.Height);
            p.Length = ParseFloat(size.Length);

            p.Size = size.Dimensions;

            p.Notes = ParseNotes(source);

            return p;
        }

        private int? ParsePrice(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            var ds = source
                .Replace(" ", string.Empty)
                .Replace(",", ".");

            if (string.IsNullOrEmpty(ds))
                return null;
            var d = Convert.ToDouble(ds, CultureInfo.InvariantCulture);
            var price = Convert.ToInt32(d);

            return price;
        }

        private float? ParseFloat(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            var numeric = new string(input
                .Replace(",", ".")
                .SkipWhile(x => !IsNumeric(x))
                .TakeWhile(IsNumeric)
                .ToArray());

            if (float.TryParse(numeric, out var result))
                return result;
            return null;
        }

        private bool IsNumeric(char c)
        {
            return char.IsDigit(c) || c == '.';
        }

        private bool? ParseWarranty(string source)
        {
            try
            {
                return ParseBool(source);
            }
            catch
            {
                return null;
            }
        }

        private MarketplaceDeliveryOption[] ParseDeliveryOptions(XmlOffer source, XmlShop shop)
        {
            XmlDeliveryOption[] options = null;
            if (source.delivery_options != null && source.delivery_options.Length > 0)
                options = source.delivery_options;
            else if (shop.delivery_options != null && shop.delivery_options.Length > 0)
                options = shop.delivery_options;

            if (options == null || options.Length > 0)
                return null;

            return options
                .Select(x =>
                    new MarketplaceDeliveryOption
                    {
                        Cost = Convert.ToInt32(x.cost),
                        Days = x.days
                    })
                .ToArray();
        }

        private string[] ParseColors(XmlOffer source)
        {
            if (source.color != null && source.color.Length > 0)
                return source.color.ToArray();
            else
            {
                var parameters = ExtractParameters(ColorAliases, source);
                if (IsNullOrEmpty(parameters))
                    return null;

                return parameters.Select(x => x.Value).ToArray();
            }
        }

        private string ParseOrigin(XmlOffer source)
        {
            if (!string.IsNullOrEmpty(source.country_of_origin))
                return source.country_of_origin;

            var origins = ExtractParameters(OriginAliases, source);
            return IsNullOrEmpty(origins) ? null : origins.FirstOrDefault()?.Value;
        }

        private Size ParseSize(XmlOffer source)
        {
            var size = new Size();

            var paramDimensions = ExtractParameters(DimensionsAliases, source);
            var paramWidth = PickParameterValue(ExtractParameters(WidthAliases, source));
            var paramLength = PickParameterValue(ExtractParameters(LengthAliases, source));
            var paramDepth = PickParameterValue(ExtractParameters(DepthAliases, source));
            var paramHeight = PickParameterValue(ExtractParameters(HeightAliases, source));

            size.Dimensions = source.dimensions;
            
            if (!IsNullOrEmpty(paramDimensions) && string.IsNullOrEmpty(size.Dimensions))
                size.Dimensions = paramDimensions.First().Value;

            string dimensionsWidth = null; 
            string dimensionsLength = null;
            string dimensionsDepth = null;
            string dimensionsHeight = null;

            if (!string.IsNullOrEmpty(size.Dimensions) 
                && !size.Dimensions.Contains(" ,")
                && !size.Dimensions.Contains(", ")
                && !size.Dimensions.Contains("("))
            {
                var split = size.Dimensions
                    .Replace("см", string.Empty, StringComparison.OrdinalIgnoreCase)
                    .Split(new[] {'x', 'х', '/', '*'}, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length > 2)
                {
                    dimensionsWidth = split[0].Trim();
                    dimensionsHeight = split[1].Trim();
                    dimensionsDepth = split[2].Trim();
                    dimensionsLength = split[2].Trim();
                }
                else if (split.Length > 1)
                {
                    dimensionsWidth = split[0].Trim();
                    dimensionsLength = split[1].Trim();
                }
            }

            size.Width = dimensionsWidth ?? dimensionsDepth ?? paramWidth ?? paramDepth;
            size.Height = dimensionsHeight ?? paramHeight;
            size.Length = dimensionsLength ?? paramLength;

            if (string.IsNullOrEmpty(size.Dimensions))
            {
                if (!string.IsNullOrEmpty(size.Width)
                    && !string.IsNullOrEmpty(size.Height)
                    && !string.IsNullOrEmpty(size.Length))
                {
                    size.Dimensions = string.Format("{0}/{1}/{2}",
                        size.Width ?? "...",
                        size.Height ?? "...",
                        size.Length ?? "...");
                }
            }

            if (string.IsNullOrEmpty(size.Dimensions))
            {
                if (!string.IsNullOrEmpty(size.Width)
                    && !string.IsNullOrEmpty(size.Length))
                {
                    size.Dimensions = string.Format("{0}/{1}",
                        size.Width ?? "...",
                        size.Length ?? "...");
                }
            }

            return size;
        }

        private string PickParameterValue(Parameter[] parameters)
        {
            if (IsNullOrEmpty(parameters))
                return null;
            return parameters.First().Value;
        }

        private string[] ParseNotes(XmlOffer source)
        {
            if (source.parameters == null)
                return null;

            var notes = new List<string>();
            if (!string.IsNullOrEmpty(source.sales_notes))
                notes.Add(source.sales_notes);

            foreach (var p in source.parameters)
            {
                if (p == null)
                    continue;

                var note = string.Format("{0}: {1} {2}", p.name, p.value, p.unit);

                if (!IgnoreParameters.Any(x => note.ToLower().Contains(x.ToLower())))
                    notes.Add(note);
            }

            return notes.ToArray();
        }

        private int? ParseInt(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;

            if (float.TryParse(source, out float p))
            {
                if (p > 0)
                    return (int) p;
            }

            return null;
        }

        private bool? ParseBool(string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            return bool.Parse(source);
        }

        private MarketplaceCategory CreateCategory(XmlCategory source)
        {
            return new MarketplaceCategory { Id = source.id, ParentId = source.parentId, Name = source.name };   
        }

        private Parameter[] ExtractParameters(string[] keywords, XmlOffer offer)
        {
            if (offer.parameters == null && offer.parameters2 == null)
                return null;

            var values = new List<Parameter>();

            if (offer.parameters != null)
            {
                for (var i = 0; i < offer.parameters.Length; i++)
                {
                    if (offer.parameters[i] == null)
                        continue;

                    foreach (var key in keywords)
                    {
                        if (offer.parameters[i] == null)
                            break;

                        if (offer.parameters[i].name.ToLower().Contains(key.ToLower()))
                        {
                            values.Add(new Parameter {Value = offer.parameters[i].value});
                            offer.parameters[i] = null;
                        }
                    }
                }
            }

            if (offer.parameters2 != null)
            {
                for (var i = 0; i < offer.parameters2.Length; i++)
                {
                    if (offer.parameters2[i] == null)
                        continue;

                    foreach (var key in keywords)
                    {
                        if (offer.parameters2[i] == null)
                            break;

                        if (offer.parameters2[i].name.ToLower().Contains(key.ToLower()))
                        {
                            values.Add(new Parameter { Value = offer.parameters2[i].value });
                            offer.parameters2[i] = null;
                        }
                    }
                }
            }

            if (values.Count > 0)
                return values.ToArray();
            return null;
        }

        private bool IsNullOrEmpty<T>(T[] source)
        {
            return source == null || source.Length == 0;
        }

        private class Parameter
        {
            public string Value { get; set; }

            public override string ToString()
            {
                return string.Format("{0}", Value);
            }
        }

        private class Size
        {
            public string Dimensions { get; set; }

            public string Width { get; set; }
            public string Length { get; set; }
            public string Height { get; set; }
        }
    }
}
