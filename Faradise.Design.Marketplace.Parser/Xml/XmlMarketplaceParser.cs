﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Faradise.Design.Marketplace.Xml
{
    public class XmlMarketplaceParser
    {
        public XmlShop Parse(string text)
        {
            using (var ms = new StringReader(text))
            using (var xmlReader = new XmlTextReader(ms))
            {
                var xRoot = new XmlRootAttribute
                {
                    ElementName = "shop",
                    IsNullable = true
                };

                xmlReader.MoveToContent();
                xmlReader.ReadToDescendant("shop");
                var serializer = new XmlSerializer(typeof(XmlShop), xRoot);
                var document = (XmlShop)serializer.Deserialize(xmlReader);

                return document;
            }
        }
    }
}

