﻿using Faradise.Design.Marketplace.Models;
using Faradise.Design.Marketplace.Xml;
using System;
using System.Collections.Generic;

namespace Faradise.Design.Marketplace
{
    public static class MarketplaceFactory
    {
        private static Dictionary<Type, MulticastDelegate> _creators = new Dictionary<Type, MulticastDelegate>();

        static MarketplaceFactory()
        {
            Add<XmlShop, XmlMarketplaceMapper>(() => new XmlMarketplaceMapper());
        }

        public static void Add<TSource, TCreator>(Func<TCreator> constructor)
            where TCreator : IMarketplaceMapper<TSource>
        {
            _creators.Add(typeof(TSource), constructor);
        }

        public static MarketplaceCompany Create<T>(T source)
        {
            if (!_creators.ContainsKey(typeof(T)))
                throw new ArgumentException(string.Format("Creator for {0} was not found", typeof(T).Name));

            var constructor = _creators[typeof(T)];
            var creator = (IMarketplaceMapper<T>)constructor.DynamicInvoke();
            return creator.Create(source);
        }
    }
}
