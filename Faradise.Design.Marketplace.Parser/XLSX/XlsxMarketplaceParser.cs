﻿using ExcelDataReader;
using Faradise.Design.Marketplace.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using Faradise.Design.Marketplace.Models.Validation;

namespace Faradise.Design.Marketplace.Xml
{
    public static class XlsxMarketplaceParser
    {

        private static Dictionary<string, int> _columnNames = new Dictionary<string,int>();
        //TODO:: Словарь соотношения категории к idШнику, сбрасывается при новом парсе
        private static Dictionary<string, MarketplaceCategory> _categoryToId = new Dictionary<string, MarketplaceCategory>();
        private static int _nextCategory = 1;


        private static Regex namePattern = new Regex(@"^\s*\b(title|name|название|имя|наименование|название модели)\b\s*$");
        private static Regex idPattern = new Regex(@"^\s*\bid\b\s*$");
        private static Regex availablePattern = new Regex(@"^\s*\b(available|доступность|в наличии|наличие в магазине)\b\s*$");
        private static Regex urlPattern = new Regex(@"^\s*\b(url|link|ссылка|ссылка на товар)\b\s*$");
        private static Regex pricePattern = new Regex(@"\b((P|p)rice|(Ц|ц)ена|(С|с)тоимость)\b");
        private static Regex oldPricePattern = new Regex(@"\b(old price|oldprice|старая цена|старая стоимость)\b"); //Куда надо добавить колонку wholesalePrice???
        private static Regex categoryIdPattern = new Regex(@"^\s*\b(categoryid|category|typeprefix|type|product type|категория)\b\s*$");
        private static Regex picturePattern = new Regex(@"\b(picture|image|ссылка на картинку)\b");
        private static Regex descriptionPattern = new Regex(@"\b(description|описание)\b");
        private static Regex vendorPattern = new Regex(@"^\s*\b(vendor|brand|производитель)\b\s*$");
        private static Regex vendorCodePattern = new Regex(@"\b(vendorcode|vendor-code|артикул)\b");
        private static Regex widthPattern = new Regex(@"(width|ширина)");
        private static Regex lengthPattern = new Regex(@"(length|длина)");
        private static Regex depthPattern = new Regex(@"(depth|глубина)");
        private static Regex heightPattern = new Regex(@"(высота|height)");
        private static Regex diameterPattern = new Regex(@"(diameter|диаметр)");
        private static Regex sizePattern = new Regex(@"(размер|size|габариты)");
        private static Regex originPattern = new Regex(@"(origin|страна|изготовитель)");
        private static Regex colorPattern = new Regex(@"(цвет|color)");
        private static Regex warrantyPattern = new Regex(@"\b(warranty|гарантия)\b");
        private static Regex deliveryPattern = new Regex(@"^\s*\b(delivery|shipment|доставка|условия доставки)\b\s*$");
        private static Regex deliveryCostPattern = new Regex(@"\b(delivery\s*(cost|price)|shipment\s*(cost|price)|(цена|стоимость)\s*доставки)\b");
        private static Regex deliverydaysPattern = new Regex(@"\b(delivery\s*(days|time)|shipment\s*(days|time)|(время|длительность)\s*доставки)\b");
        private static Regex pickupPattern = new Regex(@"\b(pickup|можно ли забрать)\b");
        private static Regex notesPattern = new Regex(@"\b(notes?|заметки)\b");

        private static Regex numericPattern = new Regex(@"[^\d.]");


        public static (MarketplaceCompany company, ValidationReport report) Parse(Stream stream)
        {
            //TODO:: Очистка словарей перед новым парсом
            _columnNames = new Dictionary<string, int>();
            _categoryToId = new Dictionary<string, MarketplaceCategory>();
            _nextCategory = 1;

            // Auto-detect format, supports:
            //  - Binary Excel files (2.0-2003 format; *.xls)
            //  - OpenXml Excel files (2007 format; *.xlsx)
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                // Choose one of either 1 or 2:

                // 1. Use the reader methods
                do
                {
                    while (reader.Read())
                    {
                        // reader.GetDouble(0);
                    }
                } while (reader.NextResult());

                // 2. Use the AsDataSet extension method
                var result = reader.AsDataSet();    
                var rows = result.Tables[0].Rows;
                var columnNames = rows[0];
                for (var i = 0; i < columnNames.ItemArray.Length; i++)
                {
                    var name = columnNames.ItemArray[i].ToString().ToLower();
                    if (idPattern.IsMatch(name))
                        TryAddColumn("id", i); //_columnNames.Add("id", i);
                    if (availablePattern.IsMatch(name))
                        TryAddColumn("available", i); // _columnNames.Add("available", i);
                    if (urlPattern.IsMatch(name))
                        TryAddColumn("url", i); // _columnNames.Add("url", i);
                    if (pricePattern.IsMatch(name))
                        TryAddColumn("price", i); // _columnNames.Add("price", i);
                    if (oldPricePattern.IsMatch(name))
                        TryAddColumn("oldprice", i); //_columnNames.Add("oldprice", i);
                    if (categoryIdPattern.IsMatch(name))
                        TryAddColumn("categoryid", i); //_columnNames.Add("categoryid", i);
                    if (picturePattern.IsMatch(name))
                        TryAddColumn("picture", i); // _columnNames.Add("picture", i);
                    if (namePattern.IsMatch(name))
                        TryAddColumn("name", i); //_columnNames.Add("name", i);
                    if (descriptionPattern.IsMatch(name))
                        TryAddColumn("description", i); //_columnNames.Add("description", i);
                    if (vendorPattern.IsMatch(name))
                        TryAddColumn("vendor", i); //_columnNames.Add("vendor", i);
                    if (vendorCodePattern.IsMatch(name))
                        TryAddColumn("vendorcode", i); //_columnNames.Add("vendorcode", i);
                    if (widthPattern.IsMatch(name))
                        TryAddColumn("width", i); //_columnNames.Add("width", i);
                    if (lengthPattern.IsMatch(name))
                        TryAddColumn("length", i); //_columnNames.Add("length", i);
                    if (heightPattern.IsMatch(name))
                        TryAddColumn("heigth", i); //_columnNames.Add("heigth", i);
                    if (depthPattern.IsMatch(name))
                        TryAddColumn("depth", i); //_columnNames.Add("depth", i);
                    if (sizePattern.IsMatch(name))
                        TryAddColumn("size", i); //_columnNames.Add("size", i);
                    if (diameterPattern.IsMatch(name))
                        TryAddColumn("diameter", i); //_columnNames.Add("diameter",i);
                    if (originPattern.IsMatch(name))
                        TryAddColumn("origin", i); //_columnNames.Add("origin", i);
                    if (colorPattern.IsMatch(name))
                        TryAddColumn("color", i); //_columnNames.Add("color", i);
                    if (warrantyPattern.IsMatch(name))
                        TryAddColumn("warranty", i); //_columnNames.Add("warranty", i);
                    if (deliveryPattern.IsMatch(name)) //(name=="delivery")
                        TryAddColumn("delivery", i);
                    if (deliveryCostPattern.IsMatch(name)) //(name.Contains("deliverycost"))
                        TryAddColumn("deliverycost", i);
                    if (deliverydaysPattern.IsMatch(name)) //(name.Contains("deliverydays"))
                        TryAddColumn("deliverydays", i);
                    if (pickupPattern.IsMatch(name)) //(name.Contains("pickup"))
                        TryAddColumn("pickup", i);
                    if (notesPattern.IsMatch(name)) //(name.Contains("note"))
                        TryAddColumn("notes", i);
                }

                var products = new List<MarketplaceProduct>();

                var report =
                    new ValidatorProcessor<MarketplaceProduct>(new XlsxValidatorDataProvider(result, _columnNames))
                        .Validate();

                if (!report.Valid)
                    return (null, report);
                for (var i = 1; i < rows.Count; i++)
                {
                    var product = new MarketplaceProduct();
                    var rowData = rows[i].ItemArray;
                    // ReSharper disable once PossibleInvalidOperationException
                    var priceString = ExtractColumnString(rowData, "price");
                    if (i == 1 && pricePattern.IsMatch(priceString))
                        continue;
                    product.Price = ExtractColumnPrice(rowData, "price").Value;
                    product.Name = ExtractColumnString(rowData, "name");
                    product.Id = ExtractColumnString(rowData, "id");
                    product.Available = ExtractColumnBool(rowData, "available");
                    product.Url = ExtractColumnString(rowData, "url");

                    product.PreviousPrice = ExtractColumnPrice(rowData, "oldprice");

                    product.CategoryId = ExtractCategory(rowData, "categoryid");


                    product.Description = ExtractColumnString(rowData, "description");
                    product.Vendor = ExtractColumnString(rowData, "vendor");
                    product.VendorCode = ExtractColumnString(rowData, "vendorcode");
                    product.Origin = ExtractColumnString(rowData, "origin");
                    product.Color = ExtractStringArray(rowData, "color");
                    product.Pictures = ExtractStringArray(rowData, "picture");
                    product.Warranty = ExtractColumnBool(rowData, "warranty");
                    product.Delivery = ExtractColumnBool(rowData, "delivery");
                    product.DeliveryCost = ExtractColumnInt(rowData, "deliverycost");
                    product.DeliveryDays = ExtractColumnString(rowData, "deliverydays");
                    product.Pickup = ExtractColumnBool(rowData, "pickup");
                    product.Notes = ExtractStringArray(rowData, "notes");

                    product.Length = ExtractColumnFloat(rowData, "length");
                    if (product.Length == null)
                        product.Length = ExtractColumnFloat(rowData, "diameter");

                    product.Width = ExtractColumnFloat(rowData, "width");
                    if (product.Width == null)
                        product.Width = ExtractColumnFloat(rowData, "depth");
                    if (product.Width == null)
                        product.Width = ExtractColumnFloat(rowData, "diameter");

                    product.Height = ExtractColumnFloat(rowData, "height");
                    product.Size = ExtractColumnString(rowData, "size", string.Empty);

                    if (string.IsNullOrEmpty(product.Id))
                        product.Id = product.VendorCode;
                    if (string.IsNullOrEmpty(product.Id))
                        throw new Exception($"cant find product id for product {product.Name}");

                    products.Add(product);
                }

                var categories = _categoryToId.Values.ToList();
                var sortCat = new List<MarketplaceCategory>();
                var count = categories.Count;
                sortCat.AddRange(categories.Where(x => x.ParentId == 0));
                sortCat.ForEach(x => categories.Remove(x));
                while (count > sortCat.Count)
                {
                    var toDel = new List<MarketplaceCategory>();
                    foreach (var cat in categories)
                    {
                        var parentIds = sortCat.Select(x => x.Id);
                        if (parentIds.Contains(cat.ParentId))
                        {
                            sortCat.Add(cat);
                            toDel.Add(cat);
                        }
                    }

                    toDel.ForEach(x => categories.Remove(x));
                }

                return (new MarketplaceCompany
                {
                    Name = "temporary", Categories = sortCat, Products = products
                }, report);


                // The result of each spreadsheet is in result.Tables
            }
        }

        private static void TryAddColumn(string columnName, int id)
        {
            if (!_columnNames.ContainsKey(columnName))
                _columnNames.Add(columnName, id);
        }

        //TODO:: просто генерим idшники для категорий и запоминаем их
        private static MarketplaceCategory GetCategory(string categoryName)
        {
            if (string.IsNullOrEmpty(categoryName))
                return null;
            MarketplaceCategory category;
            if (_categoryToId.TryGetValue(categoryName, out category))
                return category;
            else
            {
                category = new MarketplaceCategory { Id = _nextCategory, ParentId = 0, Name = categoryName };
                _categoryToId.Add(categoryName, category);
                _nextCategory++;
                return category;
            }
        }

        private static string ExtractColumnString(this object[] itemArray, string columnName, string defValue = null)
        {
            int columnIndex;
            if (_columnNames.TryGetValue(columnName, out columnIndex))
                return itemArray[columnIndex].ToString();
            else
                return defValue != null ? defValue : string.Empty;
        }

        private static int? ExtractColumnInt(this object[] itemArray, string columnName)
        {
            if (_columnNames.TryGetValue(columnName, out var columnIndex))
            {
                if (int.TryParse(itemArray[columnIndex].ToString(), out var value))
                    return value;
            }

            return null;
        }

        private static int? ExtractColumnPrice(this object[] itemArray, string columnName)
        {
            if (_columnNames.TryGetValue(columnName, out var columnIndex))
            {
                var sPrice = itemArray[columnIndex].ToString();
                sPrice = sPrice
                    .Replace(" ", string.Empty)
                    .Replace(',', '.');
                sPrice = numericPattern.Replace(sPrice, "");
                if (float.TryParse(sPrice, out var value))
                    return (int)value;
            }

            return null;
        }

        private static float? ExtractColumnFloat(this object[] itemArray, string columnName)
        {
            int columnIndex;
            if (_columnNames.TryGetValue(columnName, out columnIndex))
            {
                float value = 0;
                if (float.TryParse(itemArray[columnIndex].ToString(), out value))
                    return value;
            }
            return null;
        }

        private static bool? ExtractColumnBool(this object[] itemArray, string columnName)
        {
            int columnIndex;
            if (_columnNames.TryGetValue(columnName, out columnIndex))
            {
                bool boolean = false;
                var s = itemArray[columnIndex].ToString();
                s = s.Trim();
                if (bool.TryParse(s, out boolean))
                    return boolean;
                else
                {
                    s = s.ToLower();
                    if (s == "да")
                        return true;
                    else if (s == "нет")
                        return false;
                }
                return null;
            }
            else
                return null;
        }

        //TODO:: Пытаемся получить имя категории. если в колонке была какая то муть с разделителями - берем саму последнюю вложенную категорию.
        // Иначе берем напрямую строку в колонке
        private static int ExtractCategory(this object[] itemArray, string columnName)
        {
            int columnIndex;
            if (_columnNames.TryGetValue(columnName, out columnIndex))
            {
                var categories = itemArray[columnIndex].ToString();
                if (string.IsNullOrEmpty(categories))
                    return 0;
                categories = categories.ToLower();
                var array = categories.Split(new char[] { '.', ',', '\\', '/', '>' }, StringSplitOptions.RemoveEmptyEntries);
                //Смотрим весь список иерархии категорий у товара начиная с самой последней, создаем их и присваиваем родителей
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    var childCategory = GetCategory(array[i]);
                    if (childCategory != null && childCategory.ParentId == 0 && i > 0)
                    {
                        var parentCategory = GetCategory(array[i - 1]);
                        if (parentCategory != null)
                            childCategory.ParentId = parentCategory.Id;
                    }
                }
                var productCategory = array.Length > 1 ? array[array.Length - 1] : categories;
                return GetCategory(productCategory).Id;
            }
            else
                return 0;
        }

        private static string[] ExtractStringArray(this object[] itemArray, string columnName)
        {
            var colorstring = ExtractColumnString(itemArray, columnName);
            if (string.IsNullOrEmpty(colorstring))
                return new string[0];
            else
                return colorstring
                    .Split(';', ',', StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => x.Trim())
                    .ToArray();
        }

        private static void ParsePictures(MarketplaceProduct product, string pictureValue)
        {
            var pictures = pictureValue.Split(';', ',');
            product.Pictures = pictures;
        }
    }
    
}

