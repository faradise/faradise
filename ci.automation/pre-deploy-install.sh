#!/bin/bash

sudo apt-get update
sudo apt-get remove docker docker-engine docker.io
sudo apt-get install apt-transport-https ca-certificates -y curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce -y
sudo usermod -aG docker $(whoami)
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo service docker stop
sudo nohup docker daemon -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock &

sudo apt-get install nginx -y

sudo sh -c "echo '
user www-data;
worker_processes auto;
pid /run/nginx.pid;

events {
     worker_connections 768;
}

http {

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    client_max_body_size 128m;
    server_names_hash_bucket_size 64;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

	proxy_send_timeout 1800s;
	proxy_read_timeout 1800s;
	fastcgi_send_timeout 1800s;
	fastcgi_read_timeout 1800s;
	
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    gzip on;

    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
' > /etc/nginx/nginx.conf"

sudo sh -c "echo '
server {
    server_name $1.faradise.ru;
    client_max_body_size 128m;
    location / {
        proxy_pass http://localhost:3001/;
        proxy_set_header Host \$host;
    }
    location /chat/ {
        proxy_pass http://localhost:3003/;
        proxy_set_header Host \$host;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
    }

}
' > /etc/nginx/sites-available/$1.faradise.ru"

sudo sh -c "echo '
server {
    server_name kibana.$1.faradise.ru;
    client_max_body_size 128m;
    location / {
        proxy_pass http://localhost:5601/;
        proxy_set_header Host \$host;
    }
}
' > /etc/nginx/sites-available/kibana.$1.faradise.ru"

sudo sh -c "echo '
server {
    server_name admin.$1.faradise.ru;
    client_max_body_size 128m;
    location / {
        proxy_pass http://localhost:3002/;
        proxy_set_header Host \$host;
    }
}
' > /etc/nginx/sites-available/admin.$1.faradise.ru"

sudo rm /etc/nginx/sites-enabled/$1.faradise.ru
sudo rm /etc/nginx/sites-enabled/kibana.$1.faradise.ru
sudo rm /etc/nginx/sites-enabled/admin.$1.faradise.ru

sudo ln -s /etc/nginx/sites-available/$1.faradise.ru /etc/nginx/sites-enabled/
sudo ln -s /etc/nginx/sites-available/kibana.$1.faradise.ru /etc/nginx/sites-enabled/
sudo ln -s /etc/nginx/sites-available/admin.$1.faradise.ru /etc/nginx/sites-enabled/

sudo nginx -t

sudo service nginx restart

sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update -y
sudo apt-get install python-certbot-nginx -y
sudo nginx -t
sudo service nginx restart
sudo certbot --nginx -d $1.faradise.ru --email vz@awespace.ru --non-interactive --redirect
sudo service nginx restart

sudo service docker stop
sudo service docker start
