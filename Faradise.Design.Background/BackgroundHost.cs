﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Faradise.Design.Services.Backgroud;

namespace Faradise.Design.Background
{
    public class BackgroundHost
    {
        private readonly List<BackgroundHostServiceEntry> _hostedServices = new List<BackgroundHostServiceEntry>();

        public void Register<T>(TimeSpan period)
            where T : IBackgroundHostedService
        {
            Register<T>(period, period);
        }

        public void Register<T>(TimeSpan successPeriod, TimeSpan failPeriod)
            where T : IBackgroundHostedService
        {
            Register<T>(successPeriod, failPeriod, TimeSpan.Zero);
        }

        public void Register<T>(TimeSpan successPeriod, TimeSpan failPeriod, TimeSpan initialDelay)
            where T : IBackgroundHostedService
        {
            var wrapper = new BackgroundHostServiceEntry<T>(successPeriod, failPeriod, initialDelay);
            _hostedServices.Add(wrapper);
        }

        public void Begin(IServiceProvider serviceProvider)
        {
            foreach (var service in _hostedServices)
                service.Begin(serviceProvider);
        }

        public string ExtractReport()
        {
            var sb = new StringBuilder();

            var max = _hostedServices.Max(x => x.Type.Length);
            foreach (var s in _hostedServices)
            {
                var n = s.Type.PadLeft(max);
                sb.AppendLine($"{n} - {s.ExtractStatus()}");
            }

            sb.AppendLine();

            foreach (var s in _hostedServices)
            {
                sb.AppendLine(s.Type);
                sb.AppendLine(s.ExtractLogs());
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
