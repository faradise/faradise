﻿using System;
using System.Threading.Tasks;
using Faradise.Design.Services.Backgroud;

namespace Faradise.Design.Background
{
    public class DebugBackgroundService : IBackgroundHostedService
    {
        public async Task<bool> Process()
        {
            throw new Exception();
        }
    }
}
