﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Faradise.Design.Background
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var dir = Path.Combine("bin", "Debug", "netcoreapp2.0");

            if (Directory.Exists(dir))
            {
                File.Copy(Path.Combine(dir, "appsettings.json"), "appsettings.json", true);
                File.Copy(Path.Combine(dir, "appsettings.Develop.json"), "appsettings.Develop.json", true);
                File.Copy(Path.Combine(dir, "appsettings.staging.json"), "appsettings.staging.json", true);
                File.Copy(Path.Combine(dir, "appsettings.master.json"), "appsettings.master.json", true);
                File.Copy(Path.Combine(dir, "appsettings.integration.json"), "appsettings.integration.json", true);
            }
            
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
