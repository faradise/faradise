﻿using System;
using System.Collections.Generic;
using System.Threading;
using Faradise.Design.Services.Backgroud;
using Microsoft.Extensions.DependencyInjection;

namespace Faradise.Design.Background
{
    public class BackgroundHostServiceEntry<T> : BackgroundHostServiceEntry
        where T : IBackgroundHostedService
    {
        public BackgroundHostServiceEntry(
            TimeSpan successDelay, 
            TimeSpan failDelay,
            TimeSpan initialDelay) 
            : base(typeof(T), successDelay, failDelay, initialDelay)
        {}

        public override string Type => typeof(T).Name;
    }

    public abstract class BackgroundHostServiceEntry
    {
        private readonly Type _serviceType;
        private readonly TimeSpan _success;
        private readonly TimeSpan _fail;
        private readonly TimeSpan _initialDelay;

        private Timer _timer;

        private string _status = $"{DateTime.Now:s} - created";
        private readonly Queue<string> _logs = new Queue<string>();

        protected BackgroundHostServiceEntry(Type serviceType, TimeSpan successDelay, TimeSpan failDelay, TimeSpan initialDelay)
        {
            _serviceType = serviceType;
            _success = successDelay;
            _fail = failDelay;
            _initialDelay = initialDelay;
        }

        public abstract string Type { get; }

        public void Begin(IServiceProvider serviceProvider)
        {
            var state = new State(serviceProvider, _serviceType, _success, _fail);

            _timer = new Timer(Tick, state, _initialDelay, Timeout.InfiniteTimeSpan);
        }

        private async void Tick(object timerState)
        {
            var state = (State) timerState;
            var delay = state.DelayOnSuccess;
            try
            {
                using (var scope = state.Provider.CreateScope())
                {
                    var service = (IBackgroundHostedService) scope.ServiceProvider.GetService(state.ServiceType);
                    if (service == null)
                        throw new Exception("Service was not found");


                    _status = $"{DateTime.Now:s} - started processing";
                    _logs.Enqueue(_status);

                    var succeeded = await service.Process();
                    if (!succeeded)
                        delay = state.DelayOnFail;

                    _status = $"{DateTime.Now:s} - finished, succeeded: {(succeeded ? 1 : 0)}";
                    _logs.Enqueue(_status);

                }
            }
            catch (Exception ex)
            {
                _status = $"{DateTime.Now:s} - FAILED, message: {ex.Message}";
                _logs.Enqueue(_status);

                delay = state.DelayOnFail;
            }

            _timer.Change(delay, Timeout.InfiniteTimeSpan);
        }

        private struct State
        {
            public IServiceProvider Provider { get; }
            public Type ServiceType { get; }
            public TimeSpan DelayOnSuccess { get; }
            public TimeSpan DelayOnFail { get; }

            public State(IServiceProvider provider, Type type, TimeSpan success, TimeSpan fail)
            {
                Provider = provider;
                ServiceType = type;
                DelayOnSuccess = success;
                DelayOnFail = fail;
            }
        }

        public string ExtractStatus()
        {
            return _status;
        }

        public string ExtractLogs()
        {
            return string.Join('\n', _logs);
        }
    }
}
