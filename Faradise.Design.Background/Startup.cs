﻿using System;
using System.Threading;
using Faradise.Design.Services.Background;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Faradise.Design.Background
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public BackgroundHost BackgroundHost { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            BackgroundHost = new BackgroundHost();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (FaradiseEnvironment.Integration)
                return;

            ServiceRegistry.Configure(services, Configuration);

            using (var scope = services.BuildServiceProvider())
            {
                var service = scope.GetService<IndexUpdaterService>();
                var azureTask = service.CheckAzure();
                azureTask.Wait();
            }

            using (var scope = services.BuildServiceProvider())
            {
                var service = scope.GetService<TolokaTaskSegmentBackgroundService>();
                var azureTask = service.ClearStatus();
                azureTask.Wait();
            }

            BackgroundHost.Register<MarketplaceProcessingBackgroundService>(TimeSpan.FromMinutes(5));
            BackgroundHost.Register<PaymentCheckBackgroundService>(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(10));
            BackgroundHost.Register<FeedsBackgroundService>(TimeSpan.FromMinutes(15));
            BackgroundHost.Register<ArchiveUpdateBackgroundService>(TimeSpan.FromMinutes(1));
            BackgroundHost.Register<CompanyFeedAcceptorBackgroundService>(TimeSpan.FromMinutes(1));
            BackgroundHost.Register<AutoFeedTaskManagerBackgroundService>(TimeSpan.FromMinutes(5));
            BackgroundHost.Register<IndexUpdaterService>(TimeSpan.FromMinutes(5));
            BackgroundHost.Register<FeedAnalyzerBackgroundService>(TimeSpan.FromDays(1));
            BackgroundHost.Register<ProductCollectionBackgroudService>(TimeSpan.FromDays(1));
            

            BackgroundHost.Register<CacheWarmingBackgroundService>(TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(1));

            if (FaradiseEnvironment.Master)
            {
                BackgroundHost.Register<PictureUploadBackgroundService>(TimeSpan.FromMilliseconds(100), TimeSpan.FromMinutes(1));
                BackgroundHost.Register<TolokaTaskBackgroundService>(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(10));
                BackgroundHost.Register<TolokaTaskSegmentBackgroundService>(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(10));
            }
            else if (FaradiseEnvironment.Development)
            {
                BackgroundHost.Register<PictureUploadBackgroundService>(TimeSpan.FromMilliseconds(100), TimeSpan.FromMinutes(1));
                BackgroundHost.Register<TolokaTaskBackgroundService>(TimeSpan.FromSeconds(10));
                BackgroundHost.Register<TolokaTaskSegmentBackgroundService>(TimeSpan.FromSeconds(10));
            }
            else if(FaradiseEnvironment.Staging)
            {
                BackgroundHost.Register<TolokaTaskBackgroundService>(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5));
                BackgroundHost.Register<TolokaTaskSegmentBackgroundService>(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(5));
            }
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync(BackgroundHost.ExtractReport());
            });

            ThreadPool.QueueUserWorkItem((a) => { BackgroundHost.Begin(app.ApplicationServices); });
        }

    }
}
