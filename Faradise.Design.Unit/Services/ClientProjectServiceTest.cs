﻿using Xunit;

namespace Faradise.Design.Unit.Services
{

    public class ClientProjectServiceTest
    {        
        public class FillProjectTest
        {
            //private IDBClientProjectService _dbPrjService;
            //private IClientProjectService _descService;
            //private IProjectDefinitionService _defService;
            //private static int _prId = 0;

            //public static object[][] Info() { return new object[][]
            //{
            //    new object[]
            //    {
            //        ProjectTargetType.Office,
            //        new List<Room>() {
            //         {  Room.Bathroom },
            //         {  Room.Hallway },
            //         {  Room.LivingRoom }
            //        },
            //        new ReasonBinding[]
            //        {
            //            new ReasonBinding() { Name = ProjectReason.Renew, Description = "Ну вот так"},
            //            new ReasonBinding() { Name = ProjectReason.NewHouse, Description = "Переехал же"}
            //        }
            //    }
            //}; }

            //public static object[][] Styles() { return new object[][]
            //{
            //    new object[]
            //    {
            //        new StyleBinding[]
            //        {
            //            new StyleBinding() { Name = ProjectStyle.Modern, Description = "Очень модерн"},
            //            new StyleBinding() { Name = ProjectStyle.Fusion, Description = "Корайне фьюжн"}
            //        }
            //    }
            //}; }

            //public static object[][] Budget() { return new object[][]
            //{
            //    new object[]
            //    {
            //        new Budget()
            //        {
            //             Furniture = 300,
            //             Renovation = 100500
            //        }
            //    }
            //};}

            //public static object[][] DesignerReq() { return new object[][]
            //{
            //    new object[]
            //    {
            //        new DesignerRequirements()
            //        {
            //             AgeFrom = 20,
            //              AgeTo = 40,
            //               Gender = Gender.Female,
            //                MeetingAdress = "Корпульки",
            //                 NeedPersonalMeeting = true,
            //                  NeedPersonalСontrol = false,
            //                   Testing = new TestResults()
            //                   {
            //                        Questions = new TestResultsAnswer[]
            //                        {
            //                            new TestResultsAnswer() { Index = 0,  Answer = 1},
            //                            new TestResultsAnswer() { Index = 1, Answer = 3},
            //                            new TestResultsAnswer() { Index = 2, Answer =2}
            //                        }
            //                   }
            //        }
            //    }
            //}; }

            //public FillProjectTest()
            //{
            //    _dbPrjService = new MockDBClientProjectService();
            //    var mockDbDef = new MockDBProjectDefinitionService();
            //    var mockDBDesigner = new MockDBDesignerService();
            //    var designerService = new DesignerService(mockDBDesigner,null,null);
            //    _defService = new ProjectDefinitionService(mockDbDef, designerService, null, null);
            //    _descService = new ClientProjectService(_dbPrjService, _defService);
            //}

            [Fact]
            public void NewProjectIsCreated()
            {
                //int projectId = _defService.CreateProjectDefinition(0);
                //_descService.CreateProjectDescription(projectId);
                //Assert.True(projectId != -1, "New client project was not created");
                //_prId = projectId;
                //var prj = _descService.GetProjectDescription(_prId);
                //Assert.True(prj != null, "Unable to get project by its Id");

                Assert.True(true);
            }
            
            [Theory]
            [InlineData("Хатка бобра", "Воронеж")]
            public void SetNameAndCity(string name, string city)
            {
                NewProjectIsCreated();
                SetNameAndCityInternal(name, city);
            }

            private void SetNameAndCityInternal(string name, string city)
            {
                //var def = _defService.GetProjectDefinition(_prId);
                //Assert.True(def != null, "Unable to get project by its Id");
                //_defService.SetProjectName(_prId, name);
                //_defService.SetProjectCity(_prId, city);
                //def = _defService.GetProjectDefinition(_prId);
                //Assert.True(def.Name == name && def.City == city, "Name or City is different from test parameters");

                Assert.True(true);
            }

            //[Theory]
            //[MemberData(nameof(Info))]
            //public void FillProjectInfo(ProjectTargetType objectType, List<Room> rooms, ReasonBinding[] reasons)
            //{
            //    NewProjectIsCreated();
            //    FillProjectInfoInternal(objectType, rooms, reasons);
            //}

            //private Dictionary<Room, int> RoomsToDict(List<Room> rooms)
            //{
            //    if (rooms == null)
            //        return new Dictionary<Room, int>();
            //    return rooms.ToDictionary(x => x, y => rooms.Count(z => y == z));
            //}

            //private void FillProjectInfoInternal(ProjectTargetType objectType, List<Room> rooms, ReasonBinding[] reasons)
            //{
            //    var description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    _descService.SetProjectInformation(_prId, objectType, RoomsToDict(rooms), reasons);
            //    description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    Assert.True(description.ObjectType == objectType, "Set Object type is wrong");
            //    bool roomCheck = description.Rooms != null && description.Rooms.Count == rooms.Count;
            //    foreach (var roomPurpose in description.Rooms)
            //    {
            //        if (!rooms.Contains(roomPurpose))
            //        {
            //            Assert.True(false, "Rooms info is wrong");
            //            break;
            //        }
            //    }
            //    Assert.True(roomCheck, "Room info is wrong");
            //    for (int r = 0; r < reasons.Length; r++)
            //    {
            //        if (reasons[r].Name != description.Reasons[r].Name || reasons[r].Description != description.Reasons[r].Description)
            //        {
            //            Assert.True(false, "reason is wrong");
            //            break;
            //        }
            //    }
            //}

            //[Theory]
            //[MemberData(nameof(Styles))]
            //public void SetStyles(StyleBinding[] styles)
            //{
            //    NewProjectIsCreated();
            //    SetStylesInternal(styles);
            //}

            //private void SetStylesInternal(StyleBinding[] styles)
            //{
            //    var description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    _descService.SetProjectStyles(_prId, styles);
            //    description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    Assert.True(description.Styles != null, "project Styles is null");
            //    Assert.True(description.Styles.Length == styles.Length, "project Styles is wrong Length");
            //    for (int s = 0; s < styles.Length; s++)
            //    {
            //        if (styles[s].Name != description.Styles[s].Name || styles[s].Description != description.Styles[s].Description)
            //        {
            //            Assert.True(false, "style is wrong");
            //            break;
            //        }
            //    }
            //}

            //[Theory]
            //[MemberData(nameof(Budget))]
            //public void SetBudget(Budget budget)
            //{
            //    NewProjectIsCreated();
            //    SetBudgetInternal(budget);
            //}

            //private void SetBudgetInternal(Budget budget)
            //{
            //    var description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    _descService.SetProjectBudget(_prId, budget);
            //    description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    Assert.True(description.Budget != null, "Budget not set");
            //    Assert.True(description.Budget.Furniture == budget.Furniture, "Budget is wrong");
            //    Assert.True(description.Budget.Renovation == budget.Renovation, "Budget is wrong");
            //}

            //[Theory]
            //[MemberData(nameof(DesignerReq))]
            //public void SetDisignerRequirements(DesignerRequirements req)
            //{
            //    NewProjectIsCreated();
            //    SetDisignerRequirementsInternal(req);
            //}

            //private void SetDisignerRequirementsInternal(DesignerRequirements req)
            //{
            //    var description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    _descService.SetDisignerRequirements(_prId, req);
            //    description = _descService.GetProjectDescription(_prId);
            //    Assert.True(description != null, "Unable to get project by its Id");
            //    Assert.True(description.DesignerRequirements != null, "Designer requirements not set");
            //    Assert.True(description.DesignerRequirements.AgeFrom == req.AgeFrom, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.AgeTo == req.AgeTo, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.Gender == req.Gender, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.MeetingAdress == req.MeetingAdress, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.NeedPersonalMeeting == req.NeedPersonalMeeting, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.NeedPersonalСontrol == req.NeedPersonalСontrol, "Designer requirements is wrong");
            //    Assert.True(description.DesignerRequirements.Testing != null && description.DesignerRequirements.Testing.Questions != null, "Designer testing not set");
            //    for (int q = 0; q < req.Testing.Questions.Length; q++)
            //    {
            //        if (req.Testing.Questions[q].Index != description.DesignerRequirements.Testing.Questions[q].Index ||
            //            req.Testing.Questions[q].Answer != description.DesignerRequirements.Testing.Questions[q].Answer)
            //        {
            //            Assert.True(false, "question is wrong");
            //            break;
            //        }
            //    }
            //}

            //[Fact]
            //public void FillFullProjectTest()
            //{
            //    NewProjectIsCreated();
            //    SetNameAndCityInternal("Хатка бобра", "Воронеж");
            //    var rooms = new List<Room>() {
            //         {  Room.Bathroom},
            //         {  Room.Hallway},
            //         {  Room.LivingRoom}
            //        };
            //    var reasons = new ReasonBinding[]
            //    {
            //            new ReasonBinding() { Name = ProjectReason.Renew, Description = "Ну вот так"},
            //            new ReasonBinding() { Name = ProjectReason.NewHouse, Description = "Переехал же"}
            //    };
            //    FillProjectInfoInternal(ProjectTargetType.Office, rooms, reasons);
            //    var styles = new StyleBinding[]
            //        {
            //            new StyleBinding() { Name = ProjectStyle.Modern, Description = "Очень модерн"},
            //            new StyleBinding() { Name = ProjectStyle.Fusion, Description = "Корайне фьюжн"}
            //        };
            //    SetStylesInternal(styles);
            //    var designerReq = new DesignerRequirements()
            //    {
            //        AgeFrom = 20,
            //        AgeTo = 40,
            //        Gender = Gender.Female,
            //        MeetingAdress = "Корпульки",
            //        NeedPersonalMeeting = true,
            //        NeedPersonalСontrol = false,
            //        Testing = new TestResults()
            //        {
            //            Questions = new TestResultsAnswer[]
            //                        {
            //                            new TestResultsAnswer() { Index = 0,  Answer = 1},
            //                            new TestResultsAnswer() { Index = 1, Answer = 3},
            //                            new TestResultsAnswer() { Index = 2, Answer =2}
            //                        }
            //        }
            //    };
            //    SetDisignerRequirementsInternal(designerReq);
            //    var budget = new Budget()
            //    {
            //        Furniture = 300,
            //        Renovation = 100500
            //    };
            //    SetBudgetInternal(budget);
            //}
        }
    }
}
