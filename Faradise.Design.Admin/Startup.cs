﻿using Faradise.Design.Admin.Middleware;
using Faradise.Design.Admin.Options;
using Faradise.Design.Admin.Services;
using Faradise.Design.Admin.Services.Options;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Faradise.Design.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            switch (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"))
            {
                case "Develop":
                    services.Configure<EndpointOptions>(Configuration.GetSection("Endpoints").GetSection("Develop"));
                    break;
                case "master":
                    services.Configure<EndpointOptions>(Configuration.GetSection("Endpoints").GetSection("Master"));
                    break;
                case "stable":
                    services.Configure<EndpointOptions>(Configuration.GetSection("Endpoints").GetSection("Stable"));
                    break;
                case "staging":
                    services.Configure<EndpointOptions>(Configuration.GetSection("Endpoints").GetSection("Staging"));
                    break;
            }

            //services.Configure<EndpointOptions>(Configuration.GetSection("External"));
            services.Configure<FormOptions>(x => x.ValueCountLimit = 16384);
            services.Configure<HttpServiceOptions>(Configuration.GetSection("Endpoints").GetSection("API"));

            services.AddTransient<HttpService>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();

            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseBrowserLink();
            app.UseDeveloperExceptionPage();

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseSimpleAuthorization();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
