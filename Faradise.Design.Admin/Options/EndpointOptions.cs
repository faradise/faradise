﻿namespace Faradise.Design.Admin.Options
{
    public class EndpointOptions
    {
        public string API { get; set; }
        public string Chat { get; set; }
    }
}
