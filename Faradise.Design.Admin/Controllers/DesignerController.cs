﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("/designer")]
    [ResponseCache(NoStore = true)]
    public class DesignerController : Controller
    {
        private HttpService _http = null;

        public DesignerController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("registered/list")]
        public async Task<IActionResult> RegisteredList()
        {
            var response = await _http.Get<DesignerInfo[]>("/api/admin/get-designers");
            var model = response.Result
                .Where(x => !string.IsNullOrEmpty(x.Phone))
                .Where(x => x.Stage == DesignerRegistrationStage.Registered)
                .ToArray();

            return View(model);
        }

        [HttpGet("ready/list")]
        public async Task<IActionResult> ReadyList()
        {
            var response = await _http.Get<DesignerInfo[]>("/api/admin/get-designers");
            var model = response.Result.Where(x => x.Stage == DesignerRegistrationStage.ReadyToWork).ToArray();

            return View(model);
        }

        [HttpGet("approved/list")]
        public async Task<IActionResult> ApprovedList()
        {
            var response = await _http.Get<DesignerInfo[]>("/api/admin/get-designers");
            var model = response.Result.Where(x => x.Stage == DesignerRegistrationStage.TestApproved).ToArray();

            return View(model);
        }

        [HttpGet("ready/{id}")]
        public async Task<IActionResult> Ready(int id)
        {
            var response = await _http.Get<DesignerModel>("/api/admin/get-designer", new Dictionary<string, string> { { "designerId", id.ToString() } });
            var model = response.Result;

            var questionsResponse = await _http.Get<TestQuestionDescriptionModel[]>("/api/designer/getQuestions");
            var questions = questionsResponse.Result
                .ToDictionary(x => x.Id, y => new QuestionViewModel
                {
                    Text = y.Text,
                    Answers = y.Answers.ToDictionary(ak => ak.Id, av => av.Text)
                }
            );

            var projectsResponse = await _http.Get<int[]>("/api/admin/get-designer-projects", new Dictionary<string, string> { { "designerId", id.ToString() } });
            var projects = projectsResponse.Result;
            ViewBag.Projects = projects;

            ViewBag.Testing = new TestingViewModel { Questions = questions };

            return View(model);
        }

        [HttpGet("application/list")]
        public async Task<IActionResult> ApplicationList()
        {
            var response = await _http.Get<DesignerInfo[]>("/api/admin/get-designers");
            var model = response.Result.Where(x => x.Stage == DesignerRegistrationStage.SendTestTask).ToArray();

            return View(model);
        }

        [HttpGet("application/{id}")]
        public async Task<IActionResult> Application(int id)
        {
            var response = await _http.Get<DesignerModel>("/api/admin/get-designer", new Dictionary<string, string> { { "designerId", id.ToString() } });
            var model = response.Result;

            return View(model);
        }

        [HttpPost("apply")]
        public async Task<IActionResult> Apply(int id, DesignerLevel level)
        {
            var model = new ApplyDesignerModel { DesignerId = id, Level = level };
            var response = await _http.Post<bool>("/api/admin/apply-designer", model);

            return RedirectToAction("ApplicationList");
        }

        [HttpPost("reject")]
        public async Task<IActionResult> Reject(int id)
        {
            var response = await _http.Post<bool>("/api/admin/reject-designer", id);

            return RedirectToAction("ApplicationList");
        }
    }
}