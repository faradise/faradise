﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Models.Toloka;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    public class TolokaController : Controller
    {
        private HttpService _http = null;

        public TolokaController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("toloka-tasks")]
        public async Task<IActionResult> TolokaTasks()
        {
            var response = await _http.Get<TolokaTaskModel[]>("/api/toloka-admin/get-tasks");
            var model = response.Result;
            
            return await Task.FromResult(View(new TolokaTasksModel() { Tasks = model }));
        }

        [HttpGet("toloka-segments")]
        public async Task<IActionResult> TolokaSegments(int taskId)
        {
            var response = await _http.Get<TolokaTaskSegmentModel[]>("/api/toloka-admin/get-segments", new Dictionary<string, string>() { { "taskId", taskId.ToString() } });
            var model = response.Result;

            return await Task.FromResult(View(new TolokaSegmentsModel() { Segments = model, TaskId = taskId }));
        }

        [HttpPost("restart-segment")]
        public async Task<IActionResult> RestartSegment(int segmentId, int taskId)
        {
            var response = await _http.Post<bool>("/api/toloka-admin/restart-segment", segmentId);
            var model = response.Result;

            return await Task.FromResult(RedirectToAction("TolokaSegments", new { taskId }));
        }

        [HttpPost("delete-task")]
        public async Task<IActionResult> DeleteTask(int taskId)
        {
            var response = await _http.Post<bool>("/api/toloka-admin/delete-task", taskId);
            var model = response.Result;

            return await Task.FromResult(RedirectToAction("TolokaTasks"));
        }
    }
}
