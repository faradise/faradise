﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("daily-product")]
    [ResponseCache(NoStore = true)]
    public class DailyProductController : Controller
    {
        public int PRODUCTS_PER_PAGE = 10; 
        private HttpService _http = null;

        public DailyProductController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("get-possible-products-with-default-koef")]
        public async Task<IActionResult> GetPossibleProductsWithDefaultKoef(int page)
        {
            var companyKoef = 1.0f;
            var categoryKoef = 1.0f;
            var saleKoef = 1.0f;
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = page, companyKoef = companyKoef, categoryKoef = categoryKoef, saleKoef = saleKoef }));
        }

        [HttpGet("get-possible-products")]
        public async Task<IActionResult> GetPossibleProducts(int page, float companyKoef, float categoryKoef, float saleKoef)
        {
            //([FromQuery] int offset, [FromQuery] int count, [FromQuery] float companyKoef, [FromQuery] float categoryKoef, [FromQuery] float saleKoef)
            var current = await _http.Get<DailyProductModel>("api/admin/marketplace/daily-products/current");
            var products = await _http.Get<PossibleDailyProductModel[]>("api/admin/marketplace/daily-products/possible", new Dictionary<string, string>
            {
                { "offset" , (PRODUCTS_PER_PAGE * page).ToString() },
                { "count", PRODUCTS_PER_PAGE.ToString() },
                { "companyKoef",  companyKoef.ToString() },
                { "categoryKoef", categoryKoef.ToString() },
                { "saleKoef", saleKoef.ToString() }
            });
            var model = new SelectDailyProductModel
            {
                CategoryKoef = categoryKoef,
                CompanyKoef = companyKoef,
                SaleKoef = saleKoef,
                DailyProduct = current.Result,
                PossibleProducts = products.Result,
                Page = page,
                SelectedId = 0
            };
            return await Task.FromResult(View(model));
        }

        [HttpPost("refresh-page")]
        public async Task<IActionResult> RefreshPage(SelectDailyProductModel model)
        {
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = model.Page, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
        }

        [HttpPost("next-page")]
        public async Task<IActionResult> NextPage(SelectDailyProductModel model)
        {
            var nextPage = model.Page + 1;
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = nextPage, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
        }

        [HttpPost("prev-page")]
        public async Task<IActionResult> PrevPage(SelectDailyProductModel model)
        {
            int nextPage = Math.Max(0, model.Page - 1);
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = nextPage, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
        }

        [HttpPost("set-page")]
        public async Task<IActionResult> SetPage(SelectDailyProductModel model)
        {
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = model.Page, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
        }

        [HttpPost("select-product")]
        public async Task<IActionResult> SelectProduct(SelectDailyProductModel model)
        {
            if (model.SelectedId < 1)
                return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = model.Page, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
            await _http.Post<bool>("api/admin/marketplace/daily-products/set-daily-product", new SetDailyProductModel { ProductId = model.SelectedId });
            return await Task.FromResult(RedirectToAction("GetPossibleProducts", new { page = model.Page, companyKoef = model.CompanyKoef, categoryKoef = model.CategoryKoef, saleKoef = model.SaleKoef }));
        }

        [HttpGet("previous-list")]
        public async Task<IActionResult> PreviousList()
        {
            var response = await _http.Get<DailyProductModel[]>("api/admin/marketplace/daily-products/previous-list");
            var result = response.Result ?? new DailyProductModel[0];
            return await Task.FromResult(View(result));
        }
    }
}
