﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("adminbadge")]
    [ResponseCache(NoStore = true)]
    public class AdminBadgeController : Controller
    {
        private HttpService _http = null;

        public AdminBadgeController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("badge-list")]
        public async Task<IActionResult> BadgeList()
        {
            var response = await _http.Get<BadgeShortModel[]>("api/admin/badges/get-all-badges");
            var model = response.Result ?? new BadgeShortModel[0];
            //model = new BadgeShortModel[]
            //{
            //    new BadgeShortModel(){ Id = 1, Name = "Бадж_весь_такой_воттакой", IsActive = true },
            //    new BadgeShortModel(){ Id = 2, Name = "Бадж_весь_такой", IsActive = false },
            //    new BadgeShortModel(){ Id = 3, Name = "Бадж_весь", IsActive = true }
            //};
            return await Task.FromResult(View(model));
        }

        [HttpGet("get-badge")]
        public async Task<IActionResult> GetBadge(int id)
        {
            var response = await _http.Get<BadgeModel>("api/admin/badges/get-badge", new Dictionary<string, string> { { "badgeId", id.ToString() } });
            var badgeModel = response.Result;
            if (badgeModel == null)
                return await Task.FromResult(RedirectToAction("BadgeList"));
            if (badgeModel.AllMarketplace)
                return await Task.FromResult(RedirectToAction("AllMarketplaceBadge", badgeModel));
            else if (!badgeModel.Companies.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("CompaniesBadge", badgeModel));
            else if (!badgeModel.Categories.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("CategoryBadge", badgeModel));
            else if (!badgeModel.Products.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("ProductsBadge", badgeModel));
            else
                return await Task.FromResult(RedirectToAction("EmptyBadge", badgeModel ));
        }

        [HttpPost("deactivate-badge")]
        public async Task<IActionResult> DeactivateBadge(int id)
        {
            var response = await _http.Post<bool>("api/admin/badges/deactivate-badge", new DeactivateBadgeModel { BadgeId = id });
            return await Task.FromResult(RedirectToAction("BadgeList"));
        }

        [HttpPost("activate-badge")]
        public async Task<IActionResult> ActivateBadge(int id)
        {
            var response = await _http.Post<bool>("api/admin/badges/activate-badge", new ActivateBadgeModel { BadgeId = id });
            return await Task.FromResult(RedirectToAction("BadgeList"));
        }

        [HttpPost("delete-badge")]
        public async Task<IActionResult> DeleteBadge(int id)
        {
            var response = await _http.Post<bool>("api/admin/badges/delete-badge", new DeleteBadgeModel { BadgeId = id });
            return await Task.FromResult(RedirectToAction("BadgeList"));
        }

        [HttpPost("create-badge")]
        public async Task<IActionResult> CreateBadge(string name)
        {
            var response = await _http.Post<BadgeModel>("api/admin/badges/create-badge", new CreateBadgeModel { Name = name });
            var badgeModel = response.Result;
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = badgeModel.Id }));
        }

        [HttpPost("create-temp-badge")]
        public async Task<IActionResult> CreateBadgeTemp()
        {
            return await Task.FromResult(View());
        }

        [HttpGet("empty-badge")]
        public async Task<IActionResult> EmptyBadge(BadgeModel model)
        {
            return await Task.FromResult(View(model));
        }

        [HttpGet("all-marketplace-badge")]
        public async Task<IActionResult> AllMarketplaceBadge(BadgeModel model)
        {
            return await Task.FromResult(View(model));
        }

        [HttpPost("switch-off-marketplace")]
        public async Task<IActionResult> SwitchOffMarketplaceBadge(BadgeModel model)
        {
            model.AllMarketplace = false;
            var response = await _http.Post<BadgeModel>("api/admin/badges/update-badge", model);
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = model.Id }));
        }

        [HttpGet("category-badge")]
        public async Task<IActionResult> CategoryBadge(BadgeModel model)
        {
            var categoryList = await CategoryList();
            var selectionList = categoryList.Select(x => new CategoryForSelectionModel { Category = x, IsSelected = model.Categories != null && model.Categories.Any(catId => catId == x.Id) }).ToArray();
            ProcessChildCategoriesChanges(selectionList);
            var modifyModel = new CategoryBadgeModifyModel
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Description = model.Description,
                Picture = model.Picture,
                Categories = selectionList
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpGet("companies-badge")]
        public async Task<IActionResult> CompaniesBadge(BadgeModel model)
        {
            var companies = await GetCompanies();
            var selectionCompanies = companies.Companies.Select(x => new CompanySelectionModel
            {
                Id = x.Id,
                IsSelected = model.Companies != null && model.Companies.Any(c => c == x.Id),
                Name = x.Name
            }).ToArray();
            var modifyModel = new CompaniesBadgeModifyModel()
            {
                Id = model.Id,
                Name = model.Name,
                IsActive = model.IsActive,
                Description = model.Description,
                Picture = model.Picture,
                Companies = selectionCompanies
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpGet("products-badge")]
        public async Task<IActionResult> ProductsBadge(BadgeModel model)
        {
            var productstring = string.Empty;
            if (!model.Products.IsNullOrEmpty())
            {
                foreach (var p in model.Products)
                    productstring += p.ToString() + ";";
            }
            var modifyModel = new ProductsBadgeModifyModel
            {
                Id = model.Id,
                Description = model.Description,
                Picture = model.Picture,
                IsActive = model.IsActive,
                Name = model.Name,
                ProductString = productstring
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpPost("update-marketplace-badge")]
        public async Task<IActionResult> UpdateMarketplaceBadge(BadgeModel model)
        {
            model.AllMarketplace = true;
            var response = await _http.Post<BadgeModel>("api/admin/badges/update-badge", model);
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = model.Id }));
        }

        [HttpPost("update-products-badge")]
        public async Task<IActionResult> UpdateProductsBadge(ProductsBadgeModifyModel model)
        {
            var products = ParseProducts(model.ProductString);
            var modifyModel = new BadgeModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Picture = model.Picture,
                Description = model.Description,
                Products = products
            };
            var response = await _http.Post<BadgeModel>("api/admin/badges/update-badge", modifyModel);
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = model.Id }));
        }

        [HttpPost("update-category-badge")]
        public async Task<IActionResult> UpdateCategoryBadge(CategoryBadgeModifyModel model)
        {
            ProcessChildCategoriesChanges(model.Categories);
            var categories = model.Categories.Where(x => x.IsSelected).ToArray();
            categories = Cleanupcategories(categories);
            var modifyModel = new BadgeModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Picture = model.Picture,
                Description = model.Description,
                Categories = categories.Select(x => x.Category.Id).ToArray()
            };
            var response = await _http.Post<BadgeModel>("api/admin/badges/update-badge", modifyModel);
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = model.Id }));
        }

        [HttpPost("update-companies-badge")]
        public async Task<IActionResult> UpdateCompaniesBadge(CompaniesBadgeModifyModel model)
        {
            var modifyModel = new BadgeModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Picture = model.Picture,
                Description = model.Description,
                Companies = model.Companies.Where(x => x.IsSelected).Select(x => x.Id).ToArray()
            };
            var response = await _http.Post<BadgeModel>("api/admin/badges/update-badge", modifyModel);
            return await Task.FromResult(RedirectToAction("GetBadge", new { id = model.Id }));
        }

        private int[] ParseProducts(string productString)
        {
            if (string.IsNullOrEmpty(productString))
                return new int[0];
            var split = productString.Split(new char[] { '\n', '\r', '\t', ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 1)
                return new int[0];
            var productList = new List<int>();
            foreach (var s in split)
            {
                int productId = 0;
                if (int.TryParse(s, out productId))
                {
                    productList.Add(productId);
                }
            }
            return productList.ToArray();
        }

        public async Task<CompaniesModel> GetCompanies()
        {
            var response = await _http.Get<CompaniesModel>("/api/admin/marketplace/get-companies");
            return response.Result;
        }

        public async Task<AdminCategoryModel[]> CategoryList()
        {
            var response = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = response.Result;

            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,

                    Name = x.Name,
                    ImageUrl = x.ImageUrl,

                    InHeader = x.InHeader,
                    WasInHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    WasPopular = x.IsPopular,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Priority = x.Priority,
                    DailyProductPriority = x.DailyProductPriority,
                    IsEnabled = x.IsEnabled
                })
                .ToList();

            var model = new List<AdminCategoryModel>();

            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, model, 0);
            return model.ToArray();
        }

        private void ProcessCategoryHierarchy(AdminCategoryModel cat, List<AdminCategoryModel> source, List<AdminCategoryModel> categories, int depth)
        {
            cat.Depth = depth;
            categories.Add(cat);

            var childs = source.Where(x => x.ParentId == cat.Id);
            foreach (var child in childs)
                ProcessCategoryHierarchy(child, source, categories, depth + 1);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel[] source)
        {
            var root = source.Where(x => x.Category.ParentId == 0);
            foreach (var cat in root)
                ProcessChildCategoriesChanges(cat, source);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel cat, CategoryForSelectionModel[] source)
        {
            var childs = source.Where(x => x.Category.ParentId == cat.Category.Id);
            var isSelected = cat.IsSelected;
            foreach (var child in childs)
            {
                if (isSelected)
                    child.IsSelected = isSelected;
                ProcessChildCategoriesChanges(child, source);
            }
        }

        private CategoryForSelectionModel[] Cleanupcategories(CategoryForSelectionModel[] source)
        {
            var result = new List<CategoryForSelectionModel>();
            foreach (var cat in source)
            {
                if (cat.IsSelected && cat.Category.ParentId != 0)
                {
                    var parent = source.FirstOrDefault(x => x.Category.Id == cat.Category.ParentId);
                    if (parent != null && parent.IsSelected)
                        continue;
                }
                result.Add(cat);
            }
            return result.ToArray();
        }
    }
}
