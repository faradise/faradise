﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Models.CategoryParameters;
using Faradise.Design.Admin.Services;
using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Extensions;
using Faradise.Design.Models.CategoryParameters;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Admin.Controllers
{
    public class CategoryParametersController : Controller
    {
        private HttpService _http;
        private const string CategoryParametersApi = "api/admin/categories";

        public CategoryParametersController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("category-parameter-list")]
        public async Task<IActionResult> ParametersList()
        {
            var model =
                (await _http.Get<ParameterInfo[]>($"{CategoryParametersApi}/list")).Result;

            return View(model);
        }

        [HttpPost("create-parameter")]
        public async Task<IActionResult> CreateParameterModel(string name)
        {
            var model = new CreateParameterModel
            {
                Parameter = new ParameterInfo {Name = name},
                Categories = await _http.FillCategories()
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateParameter(CreateParameterModel model)
        {
            await _http.Post<bool>($"{CategoryParametersApi}/create-parameter", model.Parameter);
            return RedirectToAction("ParametersList");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteParameter(int id)
        {
            await _http.Get<bool>($"{CategoryParametersApi}/delete-parameter",
                new Dictionary<string, string> {{nameof(id), id.ToString()}});
            return RedirectToAction("ParametersList");
        }

        public async Task<IActionResult> DownloadReport()
        {
            var response = (await _http.Get<byte[]>($"{CategoryParametersApi}/actual-report")).Result;
            return File(response, "application/vnd.ms-excel", "FeedReport.xlsx");
        }

        [HttpPost("edit-parameter")]
        public async Task<IActionResult> EditParameter(int id)
        {
            var model = (await _http.Get<ParameterInfo>($"{CategoryParametersApi}/get-parameter",
                new Dictionary<string, string> {{nameof(id), id.ToString()}})).Result;

            var categories = await _http.FillCategories();
            categories.ForEach(item =>
            {
                if (model.Categories.Select(info => info.CategoryId).Contains(Convert.ToInt32(item.Value)))
                    item.Selected = true;
            });

            return View(new CreateParameterModel {Parameter = model, Categories = categories});
        }

        [HttpPost]
        public async Task<IActionResult> UpdateParameter(CreateParameterModel model)
        {
            await _http.Post<bool>($"{CategoryParametersApi}/update-parameter", model.Parameter);
            return RedirectToAction("ParametersList");
        }
    }
}