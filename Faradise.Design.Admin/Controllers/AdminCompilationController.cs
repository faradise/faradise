﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Compilations;
using Faradise.Design.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("admincompilation")]
    [ResponseCache(NoStore = true)]
    public class AdminCompilationController : Controller
    {
        private HttpService _http = null;

        public AdminCompilationController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("compilation-list")]
        public async Task<IActionResult> CompilationList()
        {
            var response = await _http.Get<CompilationListModel>("api/admin/compilations/get-compilation-list");
            var model = response.Result ?? new CompilationListModel();
            model.Compilations = model.Compilations ?? new CompilationShortModel[0];

            return await Task.FromResult(View(model));
        }

        [HttpGet("product-compilation")]
        public async Task<IActionResult> ProductCompilation(int id, CompilationSource source)
        {
            var response = await _http.Get<CompilationModel>("api/admin/compilations/get-compilation", new Dictionary<string, string> { { "compilationId", id.ToString() }, { "source", source.ToString() } });
            var model = response.Result;

            string products = string.Empty;
            if (model.Products != null)
                foreach (var p in model.Products)
                    products += p.ToString() + ";";
            var modModel = new ModifyProductCompilationModel { Id = model.Id, Name = model.Name, ProductString = products };
            return await Task.FromResult(View(modModel));
        }

        [HttpGet("category-compilation")]
        public async Task<IActionResult> CategoryCompilation(int id, CompilationSource source)
        {
            var response = await _http.Get<CompilationModel>("api/admin/compilations/get-compilation", new Dictionary<string, string> { { "compilationId", id.ToString() }, { "source", source.ToString() } });
            var model = response.Result;
            var categoryList = await CategoryList();
            var selectionList = categoryList.Select(x => new CategoryForSelectionModel { Category = x, IsSelected = model.Categories != null && model.Categories.Any(catId => catId == x.Id) }).ToArray();
            ProcessChildCategoriesChanges(selectionList);
            var modifyModel = new ModifyCategoryCompilationModel
            {
                Id = model.Id,
                Name = model.Name,
                Categories = selectionList
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpPost("create-compilation")]
        public async Task<IActionResult> CreateCompilation(CompilationSource source)
        {
            var response = await _http.Post<CompilationModel>("api/admin/compilations/create", new CreateCompilationModel { Source = source });
            var model = response.Result;
            if (model.Source == CompilationSource.ProductList)
                return await Task.FromResult(RedirectToAction("ProductCompilation", new { id = model.Id, source = model.Source }));
            else if (model.Source == CompilationSource.Categories)
                return await Task.FromResult(RedirectToAction("CategoryCompilation", new { id = model.Id, source = model.Source }));
            return await Task.FromResult(RedirectToAction("CompilationList")); ;
        }

        [HttpPost("delete-compilation")]
        public async Task<IActionResult> DeleteCompilation(int id,CompilationSource source)
        {
            var response = await _http.Post<bool>("api/admin/compilations/delete", new DeleteCompilationModel { Id = id, Source = source });
            return await Task.FromResult(RedirectToAction("CompilationList"));
        }

        [HttpPost("modify-product-compilation")]
        public async Task<IActionResult> ModifyProductCompilation(ModifyProductCompilationModel modifymodel)
        {
            var products = ParseProducts(modifymodel.ProductString);
            var modified = new CompilationModel { Id = modifymodel.Id, Name = modifymodel.Name, Source = CompilationSource.ProductList, Products = products };
            var response = await _http.Post<bool>("api/admin/compilations/modify", modified);
            return await Task.FromResult(RedirectToAction("ProductCompilation", new { id = modifymodel.Id, source = CompilationSource.ProductList }));
        }

        [HttpPost("modify-category-compilation")]
        public async Task<IActionResult> ModifyCategoryCompilation(ModifyCategoryCompilationModel modifymodel)
        {
            ProcessChildCategoriesChanges(modifymodel.Categories);
            var categories = modifymodel.Categories.Where(x => x.IsSelected).ToArray();
            categories = Cleanupcategories(categories);
            var modified = new CompilationModel { Id = modifymodel.Id, Name = modifymodel.Name, Source = CompilationSource.Categories, Categories = categories.Select(x => x.Category.Id).ToArray() };
            var response = await _http.Post<bool>("api/admin/compilations/modify", modified);
            return await Task.FromResult(RedirectToAction("CategoryCompilation", new { id = modifymodel.Id, source = CompilationSource.Categories }));
        }

        private int[] ParseProducts(string productString)
        {
            if (string.IsNullOrEmpty(productString))
                return new int[0];
            var split = productString.Split(new char[] { '\n', '\r', '\t', ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 1)
                return new int[0];
            var productList = new List<int>();
            foreach (var s in split)
            {
                int productId = 0;
                if (int.TryParse(s, out productId))
                {
                    productList.Add(productId);
                }
            }
            return productList.ToArray();
        }

        public async Task<AdminCategoryModel[]> CategoryList()
        {
            var response = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = response.Result;

            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,

                    Name = x.Name,
                    ImageUrl = x.ImageUrl,

                    InHeader = x.InHeader,
                    WasInHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    WasPopular = x.IsPopular,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Priority = x.Priority,
                    DailyProductPriority = x.DailyProductPriority,
                    IsEnabled = x.IsEnabled
                })
                .ToList();

            var model = new List<AdminCategoryModel>();

            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, model, 0);
            return model.ToArray();
        }

        private void ProcessCategoryHierarchy(AdminCategoryModel cat, List<AdminCategoryModel> source, List<AdminCategoryModel> categories, int depth)
        {
            cat.Depth = depth;
            categories.Add(cat);

            var childs = source.Where(x => x.ParentId == cat.Id);
            foreach (var child in childs)
                ProcessCategoryHierarchy(child, source, categories, depth + 1);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel[] source)
        {
            var root = source.Where(x => x.Category.ParentId == 0);
            foreach (var cat in root)
                ProcessChildCategoriesChanges(cat, source);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel cat, CategoryForSelectionModel[] source)
        {
            var childs = source.Where(x => x.Category.ParentId == cat.Category.Id);
            var isSelected = cat.IsSelected;
            foreach (var child in childs)
            {
                if (isSelected)
                    child.IsSelected = isSelected;
                ProcessChildCategoriesChanges(child, source);
            }
        }

        private CategoryForSelectionModel[] Cleanupcategories(CategoryForSelectionModel[] source)
        {
            var result = new List<CategoryForSelectionModel>();
            foreach (var cat in source)
            {
                if (cat.IsSelected && cat.Category.ParentId != 0)
                {
                    var parent = source.FirstOrDefault(x => x.Category.Id == cat.Category.ParentId);
                    if (parent != null && parent.IsSelected)
                        continue;
                }
                result.Add(cat);
            }
            return result.ToArray();
        }
    }
}
