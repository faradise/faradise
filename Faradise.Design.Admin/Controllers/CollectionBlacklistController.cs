﻿using Faradise.Design.Admin.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("collection-black-list")]
    [ResponseCache(NoStore = true)]
    public class CollectionBlacklistController : Controller
    {
        private HttpService _http = null;

        public CollectionBlacklistController(HttpService httpService)
        {
            _http = httpService;
        }

        [HttpGet("black-list")]
        public async Task<IActionResult> CollectionBlacklist()
        {
            var data = await _http.Get<List<string>>("api/admin/get-product-collection-blacklist");
            var list = data.Result ?? new List<string>();
            return await Task.FromResult(View(list));
        }

        [HttpPost("add-word")]
        public async Task<IActionResult> AddWord(string word)
        {
            var data = await _http.Post<List<string>>("api/admin/add-word-to-product-collection-blacklist", word);
            return await Task.FromResult(RedirectToAction("CollectionBlacklist"));
        }

        [HttpPost("remove-word")]
        public async Task<IActionResult> RemoveWord(string word)
        {
            var data = await _http.Post<List<string>>("api/admin/remove-word-from-product-collection-blacklist", word);
            return await Task.FromResult(RedirectToAction("CollectionBlacklist"));
        }
    }
}
