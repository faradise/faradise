﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Mailing;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Faradise.Design.Admin.Controllers
{
    [ResponseCache(NoStore = true)]
    public class ClientController : Controller
    {
        private HttpService _http = null;

        public ClientController(HttpService http)
        {
            _http = http;
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SmsMailing()
        {
            return View();
        }

        [HttpPost("client/send-mails")]
        public async Task<IActionResult> SendMails(string ids, string messageSms)
        {
            if (ids == null)
            {
                return await Task.FromResult(RedirectToAction("SmsMailing"));
            }
            var idsArr = ids.Trim().Split(',');
            var numbersIds = idsArr.Select(x => Convert.ToInt32(x)).ToArray();
            var response = await _http.Post<List<MailingResponseModel>>("/api/mailing/send-sms", new MailingModel { Ids = numbersIds, Message = messageSms });
            return await Task.FromResult(View(response.Result));
        }
    }
}