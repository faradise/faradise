﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Faradise.Design.Admin.Services;
using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Controllers.API.Models.Banner;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Admin.Controllers
{
    public class BannerController : Controller
    {
        private HttpService _http;
        private const string BannerApi = "api/admin/banner";

        public BannerController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("banners-list")]
        public async Task<IActionResult> BannerList()
        {
            var data = (await _http.Get<BannerListModel>($"{BannerApi}/get-banners-list")).Result;
            return await Task.FromResult(View(data));
        }

        [HttpPost("create-banner")]
        public async Task<IActionResult> CreateBannerModel(string name)
        {
            var model = new BannerModel {Name = name};
            ViewData["Categories"] = await _http.FillCategories();
            ViewData["Brands"] = await _http.FillBrands();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBanner(BannerModel model)
        {
            await _http.Post<int>($"{BannerApi}/create-banner", model);
            return RedirectToAction("BannerList");
        }

        [HttpGet("edit-banner")]
        public async Task<IActionResult> EditBanner(int id)
        {
            var model = (await _http.Get<BannerModel>($"{BannerApi}/get-banner",
                new Dictionary<string, string> {{nameof(id), id.ToString()}})).Result;
            ViewData["Categories"] = await _http.FillCategories();
            ViewData["Brands"] = await _http.FillBrands();
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UpdateBanner(BannerModel model)
        {
            await _http.Post<bool>($"{BannerApi}/update-banner", model);
            return RedirectToAction("BannerList");
        }

        [HttpPost]
        public async Task<IActionResult> ToggleBannerState(int id)
        {
            await _http.Get<bool>($"{BannerApi}/toggle-banner-state",
                new Dictionary<string, string> {{nameof(id), id.ToString()}});
            return RedirectToAction("BannerList");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteBanner(int id)
        {
            await _http.Get<bool>($"{BannerApi}/delete-banner",
                new Dictionary<string, string> {{nameof(id), id.ToString()}});
            return RedirectToAction("BannerList");
        }

        [HttpGet("picture-preview")]
        public ActionResult PicturePreview(string url)
        {
            return PartialView(model:url);
        }

    }
}