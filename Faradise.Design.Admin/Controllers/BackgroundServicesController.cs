﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Admin.Controllers
{
    [Route("background")]
    public class BackgroundServicesController : Controller
    {
        [HttpGet("status")]
        public async Task<string> Status()
        {
            using (var client = new HttpClient())
            using (var response = await client.GetAsync(new Uri("http://faradise.design.background:80")))
            {
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
