﻿using System.Threading.Tasks;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.CorruptedPictures;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Admin.Controllers
{
    public class CorruptedPicturesController : Controller
    {
        private HttpService _http;
        private const string CorruptedPicturesApi = "api/admin/pictures";


        public CorruptedPicturesController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("problem-partners-list")]
        public async Task<IActionResult> ProblemPartnersList()
        {
            var model = (await _http.Get<CorruptedPartnersList>($"{CorruptedPicturesApi}/problem-partners-list")).Result;
            return View(model);
        }
    }
}