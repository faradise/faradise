﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Options;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("project")]
    [ResponseCache(NoStore = true)]
    public class ProjectController : Controller
    {
        private HttpService _http = null;
        private EndpointOptions _externalOptions = null;
        private bool _priceMissMatch = false;

        public ProjectController(IOptions<EndpointOptions> options, HttpService http)
        {
            _http = http;
            _externalOptions = options.Value;
        }

        [HttpGet("description/list")]
        public async Task<IActionResult> DescriptionList()
        {
            var response = await _http.Get<AdminProjectInfo[]>("api/admin/get-projects");
            var model = response.Result;

            model = model
                .Where(x => x.Stage == ProjectStage.Description)
                .Where(x => x.Filled)
                .Where(x => x.DesignerId == null || x.DesignerId == 0)
                .OrderBy(x => x.AppointedDesigners)
                .ThenByDescending(x => x.RejectedDesigners)
                .ToArray();

            return await Task.FromResult(View(model));
        }

        [HttpGet("description/{id}")]
        public async Task<IActionResult> Description(int id)
        {
            var response = await _http.Get<AdminProjectModel>("/api/admin/get-project", new Dictionary<string, string> { { "projectId", id.ToString() } });
            var model = response.Result;
            model.Visualizations = model.Visualizations ?? new FullVisualizationModel[0];

            var questionsResponse = await _http.Get<TestQuestionDescriptionModel[]>("/api/client/project/getQuestions");
            var questions = questionsResponse.Result
                .ToDictionary(x => x.Id, y => new QuestionViewModel
                {
                    Text = y.Text,
                    Answers = y.Answers.ToDictionary(ak => ak.Id, av => av.Text)
                }
            );

            var appointedResponse = await _http.Get<DesignerInfo[]>("/api/admin/get-appointed-designers",
                new Dictionary<string, string> { { "projectId", id.ToString() } });
            var appointedDesigners = appointedResponse.Result;

            var rejectedResponse = await _http.Get<DesignerRejectedInfo[]>("/api/admin/get-rejected-designers",
                new Dictionary<string, string> { { "projectId", id.ToString() } });
            var rejectedDesigners = rejectedResponse.Result;

            var suitableResponse = await _http.Get<DesignerInfo[]>("/api/admin/get-suitable-designers",
                new Dictionary<string, string> { { "projectId", id.ToString() } });
            var suitableDesigners = suitableResponse.Result;

            ViewBag.Testing = new TestingViewModel { Questions = questions };

            ViewBag.Appointed = new DesignerShortListModel { Designers = appointedDesigners };
            ViewBag.Rejected = new DesignerRejectedShortListModel { Designers = rejectedDesigners };
            ViewBag.Suitable = new DesignerShortListModel { Designers = suitableDesigners };

            return View(model);
        }

        [HttpGet("development/list")]
        public async Task<IActionResult> DevelopmentList()
        {
            var response = await _http.Get<AdminProjectInfo[]>("api/admin/get-projects");
            var model = response.Result
                .Where(x => x.Stage > ProjectStage.Description)
                .OrderByDescending(x => x.Stage)
                .ThenBy(x => x.NeedAdmin)
                .ThenBy(x => x.HasChangesAfterLastAdminCheck)
                .ThenByDescending(x => x.CooperateStage)
                .ThenByDescending(x => x.DevelopmentStage)
                .ToArray();

            return await Task.FromResult(View(model));
        }

        [HttpPost("delete-project")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            var response = await _http.Post<bool>("/api/admin/delete-project", projectId);
            return RedirectToAction("DevelopmentList");
        }

        [HttpPost("delete-user")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            var response = await _http.Post<bool>("/api/admin/delete-user", userId);
            return RedirectToAction("DevelopmentList");
        }

        [HttpPost("delete-project-description")]
        public async Task<IActionResult> DeleteProjectDescription(int projectId)
        {
            var response = await _http.Post<bool>("/api/admin/delete-project", projectId);
            return RedirectToAction("DescriptionList");
        }

        [HttpPost("delete-user-description")]
        public async Task<IActionResult> DeleteUserDescription(int userId)
        {
            var response = await _http.Post<bool>("/api/admin/delete-user", userId);
            return RedirectToAction("DescriptionList");
        }

        [HttpPost("delete-room")]
        public async Task<IActionResult> DeleteRoom(int roomId, int projectId)
        {
            var response = await _http.Post<bool>("/api/admin/delete-room", roomId);
            return RedirectToAction("Development",new { id = projectId });
        }

        [HttpGet("development/{id}")]
        public async Task<IActionResult> Development(int id)
        {
            var response = await _http.Get<AdminProjectModel>("/api/admin/get-project", new Dictionary<string, string> { { "projectId", id.ToString() } });
            var model = response.Result;
            model.Visualizations = model.Visualizations ?? new FullVisualizationModel[0];
            model.Products = model.Products ?? new AdminProjectProductModel[0];

            var questionsResponse = await _http.Get<TestQuestionDescriptionModel[]>("/api/client/project/getQuestions");
            var questions = questionsResponse.Result
                .ToDictionary(x => x.Id, y => new QuestionViewModel
                {
                    Text = y.Text,
                    Answers = y.Answers.ToDictionary(ak => ak.Id, av => av.Text)
                }
            );

            ViewBag.Testing = new TestingViewModel { Questions = questions };

            return View(model);
        }

        [HttpPost("appoint-designer")]
        public async Task<IActionResult> Appoint(int projectId, int designerId)
        {
            var response = await _http.Post<bool>("/api/admin/appoint-designers", new AppointDesignerModel() { ProjectId = projectId, DesignersIds = new int[] { designerId } });
            return RedirectToAction("Description", new { id = projectId });
        }

        [HttpPost("remove-designer")]
        public async Task<IActionResult> RemoveDesigner(int projectId, int designerId)
        {
            var response = await _http.Post<bool>("/api/admin/remove-designer-from-pool", new RemoveDesignerFromPoolModel() { ProjectId = projectId, DesignerId = designerId });
            return RedirectToAction("Description", new { id = projectId });
        }

        [HttpGet("development/{id}/chat")]
        public async Task<IActionResult> Chat(int id)
        {
            var model = new ChatModel
            {
                ProjectId = id,
                EndpointAPI = _externalOptions.API,
                EndpointChat = _externalOptions.Chat
            };

            return await Task.FromResult(View(model));
        }

        [HttpPost("development/{id}/mark-viewed")]
        public async Task<IActionResult> MarkProjectViewed(int id)
        {
            var response = await _http.Post<bool>("/api/admin/mark-admin-viewed", id);
            var success = response.Result;

            return RedirectToAction("Development", new { id });
        }

        [HttpPost("development/{id}/mark-helped")]
        public async Task<IActionResult> MarkProjectHelped(int id)
        {
            var response = await _http.Post<bool>("/api/admin/reset-admin-attention", id);
            var success = response.Result;

            return RedirectToAction("Development", new { id });
        }

        [HttpGet("orders/{id}")]
        public async Task<IActionResult> OrderListByProject(int id)
        {
            var response = await _http.Get<ShortProductOrderModel[]>("api/admin/get-orders-for-project", new Dictionary<string, string> { { "projectId", id.ToString() } });
            var model = response.Result;
            return await Task.FromResult(View(model));
        }

        [HttpGet("order/{id}")]
        public async Task<IActionResult> Order(int id)
        {
            var response = await _http.Get<FullProductProjectOrderModel>("api/admin/get-orders-information", new Dictionary<string, string> { { "orderId", id.ToString() } });
            var model = response.Result;
            if (_priceMissMatch)
            {
                model.PriceMissMatch = true;
                _priceMissMatch = false;
            }
            return await Task.FromResult(View(model));
        }

        [HttpGet("payment/{id}")]
        public async Task<IActionResult> Payment(int orderId)
        {
            var response = await _http.Post<string>("api/admin/create-payment-for-project-order",new CreatePaymentForProjectModel { OrderId = orderId });
            var success = response.Result;
            if (success == "price_missmatch_error")
                _priceMissMatch = true;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/add-cost")]
        public async Task<IActionResult> AddCost(int orderId, int cost)
        {
            var response = await _http.Post<string>("api/admin/add-project-order-price", new SetAdditionalPriceForOrderModel { OrderId = orderId, Price=cost });
            var success = response.Result;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/add-delivery-cost")]
        public async Task<IActionResult> AddDeliveryCost(int orderId, int cost)
        {
            var response = await _http.Post<string>("api/admin/add-project-order-delivery-price", new SetAdditionalPriceForOrderModel { OrderId = orderId, Price = cost });
            var success = response.Result;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/delete-product")]
        public async Task<IActionResult> DeleteProduct(int orderId, int productId, int roomId, int count)
        {
            var response = await _http.Post<string>("api/admin/remove-goods-from-order", new RemoveGoodsFromOrderModel
            {
                OrderId = orderId,
                ProductId =productId,
                RoomId =roomId,
                Count =count });
            var success = response.Result;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/сomplete-order")]
        public async Task<IActionResult> CompleteProjectOrder(int orderId)
        {
            var response = await _http.Post<bool>("/api/admin/finish-project-order", orderId);
            var success = response.Result;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/cancel-order")]
        public async Task<IActionResult> CancelProjectOrder(int orderId)
        {
            var response = await _http.Post<bool>("/api/admin/cancel-project-order", orderId);
            var success = response.Result;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/move-order-to-work")]
        public async Task<IActionResult> MoveOrderToWork(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.InWork };
            var response = await _http.Post<bool>("api/admin/set-project-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/move-credit-order-to-work")]
        public async Task<IActionResult> MoveCreditOrderToWork(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditApprovedInWork };
            var response = await _http.Post<bool>("api/admin/set-project-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/set-credit-order-approved")]
        public async Task<IActionResult> SetCreditOrderToApproved(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditApprovedWaitingForManager };
            var response = await _http.Post<bool>("api/admin/set-project-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/set-credit-order-not-approved")]
        public async Task<IActionResult> SetCreditOrderToNotApproved(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditNotApproved };
            var response = await _http.Post<bool>("api/admin/set-project-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("visualizations/set-visulization-code")]
        public async Task<IActionResult> SetVisualizationCode(int projectId, string code)
        {
            var id = projectId;
            if (code == string.Empty || projectId == 0)
                return RedirectToAction("Development", new { id });
            var response = await _http.Post<bool>("/api/admin/complete-visualizations", new CompleteVisualizationModel() { ProjectId = projectId, VisualizationLink = code });
            var success = response.Result;
            return RedirectToAction("Development", new { id });
        }

        [HttpPost("notify-pool-finish")]
        public async Task<IActionResult> NotifyPoolFinish(int id)
        {
            var response = await _http.Post<bool>("/api/admin/notify-pool-is-finish", id);
            return RedirectToAction("Description", new { id = id });
        }
    }
}