﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("adminsale")]
    [ResponseCache(NoStore = true)]
    public class AdminSaleController : Controller
    {
        private HttpService _http = null;

        public AdminSaleController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("sale-list")]
        public async Task<IActionResult> SaleList()
        {
            var response = await _http.Get<SaleShortModel[]>("api/admin/sales/get-all-sales");
            var model = response.Result ?? new SaleShortModel[0];
            //model = new SaleShortModel[]
            //{
            //    new SaleShortModel(){ Id = 1, Name = "Бадж_весь_такой_воттакой", IsActive = true },
            //    new SaleShortModel(){ Id = 2, Name = "Бадж_весь_такой", IsActive = false },
            //    new SaleShortModel(){ Id = 3, Name = "Бадж_весь", IsActive = true }
            //};
            return await Task.FromResult(View(model));
        }

        [HttpGet("get-sale")]
        public async Task<IActionResult> GetSale(int id)
        {
            var response = await _http.Get<SaleModel>("api/admin/sales/get-sale", new Dictionary<string, string> { { "saleId", id.ToString() } });
            var saleModel = response.Result;
            if (saleModel == null)
                return await Task.FromResult(RedirectToAction("SaleList"));
            if (saleModel.AllMarketplace)
                return await Task.FromResult(RedirectToAction("AllMarketplaceSale", saleModel));
            else if (!saleModel.Companies.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("CompaniesSale", saleModel));
            else if (!saleModel.Categories.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("CategorySale", saleModel));
            else if (!saleModel.Products.IsNullOrEmpty())
                return await Task.FromResult(RedirectToAction("ProductsSale", saleModel));
            else
                return await Task.FromResult(RedirectToAction("EmptySale", saleModel));
        }

        [HttpPost("deactivate-sale")]
        public async Task<IActionResult> DeactivateSale(int id)
        {
            var response = await _http.Post<bool>("api/admin/sales/deactivate-sale", new ActivateSaleModel { SaleId = id });
            return await Task.FromResult(RedirectToAction("SaleList"));
        }

        [HttpPost("activate-sale")]
        public async Task<IActionResult> ActivateSale(int id)
        {
            var response = await _http.Post<bool>("api/admin/sales/activate-sale", new ActivateSaleModel { SaleId = id });
            return await Task.FromResult(RedirectToAction("SaleList"));
        }

        [HttpPost("delete-sale")]
        public async Task<IActionResult> DeleteSale(int id)
        {
            var response = await _http.Post<bool>("api/admin/sales/delete-sale", new ActivateSaleModel { SaleId = id });
            return await Task.FromResult(RedirectToAction("SaleList"));
        }

        [HttpPost("create-sale")]
        public async Task<IActionResult> CreateSale(string name)
        {
            var response = await _http.Post<SaleModel>("api/admin/sales/create-sale", new CreateSaleModel { Name = name });
            var saleModel = response.Result;
            return await Task.FromResult(RedirectToAction("GetSale", new { id = saleModel.Id }));
        }

        [HttpPost("create-temp-sale")]
        public async Task<IActionResult> CreateSaleTemp()
        {
            return await Task.FromResult(View());
        }

        [HttpGet("empty-sale")]
        public async Task<IActionResult> EmptySale(SaleModel model)
        {
            return await Task.FromResult(View(model));
        }

        [HttpGet("all-marketplace-sale")]
        public async Task<IActionResult> AllMarketplaceSale(SaleModel model)
        {
            return await Task.FromResult(View(model));
        }

        [HttpGet("category-sale")]
        public async Task<IActionResult> CategorySale(SaleModel model)
        {
            var categoryList = await CategoryList();
            var selectionList = categoryList.Select(x => new CategoryForSelectionModel { Category = x, IsSelected = model.Categories != null && model.Categories.Any(catId => catId == x.Id) }).ToArray();
            ProcessChildCategoriesChanges(selectionList);
            var modifyModel = new CategorySaleModifyModel
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Percent = model.Percents,
                Categories = selectionList,
                Description = model.Description
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpGet("companies-sale")]
        public async Task<IActionResult> CompaniesSale(SaleModel model)
        {
            var companies = await GetCompanies();
            var selectionCompanies = companies.Companies.Select(x => new CompanySelectionModel
            {
                Id = x.Id,
                IsSelected = model.Companies != null && model.Companies.Any(c => c == x.Id),
                Name = x.Name
            }).ToArray();
            var modifyModel = new CompaniesSaleModifyModel()
            {
                Id = model.Id,
                Name = model.Name,
                IsActive = model.IsActive,
                Percent = model.Percents,
                Companies = selectionCompanies,
                Description = model.Description
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpGet("products-sale")]
        public async Task<IActionResult> ProductsSale (SaleModel model)
        {
            var productstring = string.Empty;
            if (!model.Products.IsNullOrEmpty())
            {
                foreach (var p in model.Products)
                    productstring += p.ToString() + ";";
            }
            var modifyModel = new ProductsSaleModifyModel
            {
                Id = model.Id,
                Percent = model.Percents,
                IsActive = model.IsActive,
                Name = model.Name,
                Description = model.Description,
                ProductString = productstring
            };
            return await Task.FromResult(View(modifyModel));
        }

        [HttpPost("update-marketplace-sale")]
        public async Task<IActionResult> UpdateMarketplaceSale(SaleModel model)
        {
            model.AllMarketplace = true;
            var response = await _http.Post<SaleModel>("api/admin/sales/update-sale", model);
            return await Task.FromResult(RedirectToAction("GetSale", new { id = model.Id }));
        }

        [HttpPost("update-products-sale")]
        public async Task<IActionResult> UpdateProductsSale(ProductsSaleModifyModel model)
        {
            var products = ParseProducts(model.ProductString);
            var modifyModel = new SaleModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Percents = model.Percent,
                Products = products,
                Description = model.Description
            };
            var response = await _http.Post<SaleModel>("api/admin/sales/update-sale", modifyModel);
            return await Task.FromResult(RedirectToAction("GetSale", new { id = model.Id }));
        }

        [HttpPost("update-category-sale")]
        public async Task<IActionResult> UpdateCategorySale(CategorySaleModifyModel model)
        {
            ProcessChildCategoriesChanges(model.Categories);
            var categories = model.Categories.Where(x => x.IsSelected).ToArray();
            categories = Cleanupcategories(categories);
            var modifyModel = new SaleModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Percents = model.Percent,
                Description = model.Description,
                Categories = categories.Select(x => x.Category.Id).ToArray()
            };
            var response = await _http.Post<SaleModel>("api/admin/sales/update-sale", modifyModel);
            return await Task.FromResult(RedirectToAction("GetSale", new { id = model.Id }));
        }

        [HttpPost("update-companies-sale")]
        public async Task<IActionResult> UpdateCompaniesSale(CompaniesSaleModifyModel model)
        {
            var modifyModel = new SaleModel()
            {
                Id = model.Id,
                IsActive = model.IsActive,
                Name = model.Name,
                Percents = model.Percent,
                Description = model.Description,
                Companies = model.Companies.Where(x => x.IsSelected).Select(x => x.Id).ToArray()
            };
            var response = await _http.Post<SaleModel>("api/admin/sales/update-sale", modifyModel);
            return await Task.FromResult(RedirectToAction("GetSale", new { id = model.Id }));
        }

        private int[] ParseProducts(string productString)
        {
            if (string.IsNullOrEmpty(productString))
                return new int[0];
            var split = productString.Split(new char[] { '\n', '\r', '\t', ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 1)
                return new int[0];
            var productList = new List<int>();
            foreach (var s in split)
            {
                int productId = 0;
                if (int.TryParse(s, out productId))
                {
                    productList.Add(productId);
                }
            }
            return productList.ToArray();
        }

        public async Task<CompaniesModel> GetCompanies()
        {
            var response = await _http.Get<CompaniesModel>("/api/admin/marketplace/get-companies");
            return response.Result;
        }

        public async Task<AdminCategoryModel[]> CategoryList()
        {
            var response = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = response.Result;

            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,

                    Name = x.Name,
                    ImageUrl = x.ImageUrl,

                    InHeader = x.InHeader,
                    WasInHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    WasPopular = x.IsPopular,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Priority = x.Priority,
                    DailyProductPriority = x.DailyProductPriority,
                    IsEnabled = x.IsEnabled
                })
                .ToList();

            var model = new List<AdminCategoryModel>();

            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, model, 0);
            return model.ToArray();
        }

        private void ProcessCategoryHierarchy(AdminCategoryModel cat, List<AdminCategoryModel> source, List<AdminCategoryModel> categories, int depth)
        {
            cat.Depth = depth;
            categories.Add(cat);

            var childs = source.Where(x => x.ParentId == cat.Id);
            foreach (var child in childs)
                ProcessCategoryHierarchy(child, source, categories, depth + 1);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel[] source)
        {
            var root = source.Where(x => x.Category.ParentId == 0);
            foreach (var cat in root)
                ProcessChildCategoriesChanges(cat, source);
        }

        private void ProcessChildCategoriesChanges(CategoryForSelectionModel cat, CategoryForSelectionModel[] source)
        {
            var childs = source.Where(x => x.Category.ParentId == cat.Category.Id);
            var isSelected = cat.IsSelected;
            foreach (var child in childs)
            {
                if (isSelected)
                    child.IsSelected = isSelected;
                ProcessChildCategoriesChanges(child, source);
            }
        }

        private CategoryForSelectionModel[] Cleanupcategories(CategoryForSelectionModel[] source)
        {
            var result = new List<CategoryForSelectionModel>();
            foreach (var cat in source)
            {
                if (cat.IsSelected && cat.Category.ParentId != 0)
                {
                    var parent = source.FirstOrDefault(x => x.Category.Id == cat.Category.ParentId);
                    if (parent != null && parent.IsSelected)
                        continue;
                }
                result.Add(cat);
            }
            return result.ToArray();
        }
    }
}
