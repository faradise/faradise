﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Threading.Tasks;
using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Controllers.API.Models.Admin.Company;
using Faradise.Design.Controllers.API.Models.Tilda;
using Faradise.Design.Models.Admin.Company;
using Faradise.Design.Models.CategoryParameters;
using Newtonsoft.Json;

namespace Faradise.Design.Admin.Controllers
{
    [Route("marketplace")]
    [ResponseCache(NoStore = true)]
    public class MarketplaceController : Controller
    {
        private HttpService _http = null;

        private static SelectList _categories = null;
        private static RoomNameModel[] _rooms = null;
        private static YmlValidationModel _lastYmlValidation = null;

        public MarketplaceController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("company")]
        public async Task<IActionResult> CompanyList(bool showAdditionalInfo)
        {
            var response = await _http.Get<CompaniesModel>("/api/admin/marketplace/get-companies", new Dictionary<string, string> { { "loadAdditinalInfo", showAdditionalInfo.ToString() } });
            var model = response.Result;

            return await Task.FromResult(View(model));
        }

        [HttpGet("company/{id}")]
        public async Task<IActionResult> Company(int id)
        {
            var response = await _http.Get<CompanyModel>("/api/admin/marketplace/get-company?id=" + id);

            var model = response.Result;
            model.AdditionalNotes = model.AdditionalNotes ?? new List<string>();

            return await Task.FromResult(View(model));
        }

        [HttpGet("enable-company/{id}")]
        public async Task<IActionResult> EnableCompany(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/enable-company", new CompanyIdModel() { Id = id });
                        
            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpGet("disable-company/{id}")]
        public async Task<IActionResult> DisableCompany(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/disable-company", new CompanyIdModel() { Id = id });

            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpPost("company/create")]
        public async Task<IActionResult> CreateCompany()
        {
            var response = await _http.Post<CreatedCompanyModel>("/api/admin/marketplace/create-company");
            var model = response.Result;

            return await Task.FromResult(RedirectToAction("Company", new { id = model.Id }));
        }

        [HttpPost("company/modify")]
        public async Task<IActionResult> ModifyCompany([FromForm] ModifyCompanyModel model)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/modify-company", model);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("Company", new { id = model.Id }));
        }

        [HttpPost("company/addCompanyNote")]
        public async Task<IActionResult> AddCompanyNote([FromForm] int companyId, [FromForm] string newContent)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/add-company-note", new AddCompanyNoteModel { CompanyId = companyId, Note = newContent });
            return await Task.FromResult(RedirectToAction("Company", new { id = companyId }));
        }

        [HttpPost("company/deleteCompanyNote")]
        public async Task<IActionResult> DeleteCompanyNote([FromForm] int companyId, [FromForm] int noteId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/delte-company-note", new DeleteCompanyNoteModel { CompanyId = companyId, NoteId = noteId });
            return await Task.FromResult(RedirectToAction("Company", new { id = companyId }));
        }

        [HttpPost("company/saveCompanyNotes")]
        public async Task<IActionResult> SaveCompanyNotes([FromForm] int companyId, [FromForm] int noteId, [FromForm] string newContent)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/change-company-note", new ChangeCompanyNoteModel { CompanyId = companyId, NoteId = noteId, NewNote = newContent });
            return await Task.FromResult(RedirectToAction("Company", new { id = companyId }));
        }

        [DisableRequestSizeLimit]
        [HttpPost("company/{id}/upload")]
        public async Task<IActionResult> Upload(IFormFile file, int id)
        {
            byte[] bytes;
            using (var r = new BinaryReader(file.OpenReadStream()))
                bytes = r.ReadBytes((int)r.BaseStream.Length);

            var content = new ByteArrayContent(bytes);
            var multipart = new MultipartFormDataContent();
            multipart.Add(content, "file", file.FileName);
            multipart.Add(new StringContent(id.ToString()), "id");

            var response = await _http.PostContent<UploadResultModel>("/api/admin/marketplace/upload-yml", multipart);
            var model = response.Result;

            return await Task.FromResult(View(model));
        }

        [HttpPost("company/{id}/reject-upload/{upload}")]
        public async Task<IActionResult> RejectUpload(int id, string upload)
        {
            var content = new PostProcessUploadModel { Id = id, Upload = upload };
            var response = _http.Post<bool>("/api/admin/marketplace/reject-upload", content);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpPost("company/{id}/accept-upload/{upload}")]
        public async Task<IActionResult> AcceptUpload(int id, string upload)
        {
            var content = new PostProcessUploadModel { Id = id, Upload = upload };
            var response = _http.Post<bool>("/api/admin/marketplace/accept-upload", content);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpGet("company/{id}/categories")]
        public async Task<IActionResult> CompanyCategories(int id)
        {
            var response = await _http.Get<AdminCompanyCategoriesModel>("/api/admin/company/company-categories", new Dictionary<string, string> { { "companyId", id.ToString() }, { "filtration", CompanyCategoryFiltration.Unavailable.ToString() } });
            var companyCategories = response.Result;

            // с бека возвращать только количество валидных товаров
            // убирать категории, в которых 0 валидных товаров и нет детей
            // заполнить количество товаров родительских категорий суммой детей + товаров
            // отсортировать по:
            //  есть товары и не назначена категория
            //  есть товары и назначена не листовая категория

            var flat = new List<AdminCompanyCategoryModel>();
            foreach (var c in companyCategories.Categories)
                FlatternCategories(c, flat);
            companyCategories.Categories = flat;

            var categoriesResponse = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = categoriesResponse.Result;

            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,
                    Name = x.Name,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Depth = 0
                })
                .ToList();

            var categoriesList = new List<AdminCategoryModel>();

            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, categoriesList, 0);

            var categories = categoriesList
                .Select(x => new Category
                {
                    Id = x.Id,
                    Name = FormCategorySelectListName(x.Name, x.Depth)
                })
                .ToList();

            categories.Insert(0, new Category { Id = 0, Name = string.Empty });

            _categories = new SelectList(categories, "Id", "Name");

            var responseRooms= await _http.Get<RoomNameModel[]>("/api/admin/marketplace/get-category-rooms");
            _rooms = responseRooms.Result;

            ViewData["Categories"] = _categories;
            ViewData["Rooms"] = new SelectList(_rooms, "RoomNameId", "Name");

            return await Task.FromResult(View(companyCategories));
        }

        private void FlatternCategories(AdminCompanyCategoryModel cat, List<AdminCompanyCategoryModel> flat)
        {
            flat.Add(cat);

            if (cat.Childs == null || !cat.Childs.Any())
                return;

            foreach (var c in cat.Childs)
                FlatternCategories(c, flat);
        }

        // ЭТО
        [HttpGet("company/{companyId}/categories/{categoryId}")]
        public async Task<IActionResult> CompanyCategory(int companyId, int categoryId)
        {
            var response = await _http.Get<AdminCompanyCategoryItem>("/api/admin/company/company-category", new Dictionary<string, string> { { "companyId", companyId.ToString() }, { "categoryId", categoryId.ToString() } });
            var companyCategory = response.Result;

            // показывать красный бейдж если есть товары и не назначена категория
            // показывать желтый бейдж если есть товары и назначена не листовая категория

            var model = new CompanyCategoryHierarchyModel
            {
                CompanyId = companyId,

                Rooms = _rooms,
                Selector = _categories,

                Depth = companyCategory.Depth,

                Category = new CompanyCategoryModel
                {
                    Category = companyCategory.CategoryId,
                    CompanyCategory = companyCategory.Name,
                    CompanyCategoryId = categoryId,
                    Products = companyCategory.Products,
                    Rooms = companyCategory.Rooms ?? new RoomNameModel[0],
                    BadCategoryBinding = companyCategory.BadCategoryBinding
                }
            };

            return PartialView("CompanyCategoryItem", model);
        }

        // ЭТО:done
        [HttpGet("company/{companyId}/categories/{companyCategoryId}/remove-rooms")]
        public async Task<bool> RemoveAllRoomsFromCompanyCategory(int companyId, int companyCategoryId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/delete-all-company-category-rooms", new CompanyCategoryKeyModel
            {
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId
            });
            return true;
        }

        // ЭТО: done
        [HttpGet("company/{companyId}/categories/{companyCategoryId}/add-rooms")]
        public async Task<bool> AddAllRoomsToCompanyCategory(int companyId, int companyCategoryId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/add-all-company-category-rooms", new CompanyCategoryKeyModel
            {
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId
            });
            return true;
        }

        // ЭТО: done
        [HttpGet("company/{companyId}/categories/{companyCategoryId}/update-room/{roomId}/{check}")]
        public async Task<bool> ModifyCompanyCategoryRoom(int companyId, int companyCategoryId, int roomId, bool check)
        {

            if (check)
            {
                var response = await _http.Post<bool>("/api/admin/marketplace/add-company-category-room", new CompanyCategoryRoomModel
                {
                    RoomNameId = roomId,
                    CompanyId = companyId,
                    CompanyCategoryId = companyCategoryId
                });

                return true;
            }
            else
            {
                var response = await _http.Post<bool>("/api/admin/marketplace/delete-company-category-room", new CompanyCategoryRoomModel
                {
                    RoomNameId = roomId,
                    CompanyId = companyId,
                    CompanyCategoryId = companyCategoryId
                });

                return true;
            }
        }

        // ЭТО
        [HttpGet("company/{companyId}/categories/map/{companyCategoryId}/{categoryId}")]
        public async Task<string> MapCompanyCategory(int companyId, int companyCategoryId, int categoryId)
        {
            var response = await _http.Post<int[]>("/api/admin/marketplace/map-company-category", new MapCategoryModel
            {
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId,
                CategoryId = categoryId
            });

            return Newtonsoft.Json.JsonConvert.SerializeObject(response.Result);
        }

        // ЭТО:done
        [HttpGet("company/{id}/categories/add-room")]
        public async Task<IActionResult> AddCompanyCategoryRoom(int companyCategoryId, int companyId, int roomId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/add-company-category-room", new CompanyCategoryRoomModel
            {
                RoomNameId = roomId,
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId
            });
            return await Task.FromResult(RedirectToAction("CompanyCategories", companyId));
        }

        [HttpGet("company/{id}/categories/remove-room")]
        public async Task<IActionResult> RemoveCompanyCategoryRoom(int companyCategoryId, int companyId, int roomId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/delete-company-category-room", new CompanyCategoryRoomModel
            {
                RoomNameId = roomId,
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId
            });
            return await Task.FromResult(RedirectToAction("CompanyCategories", companyId));
        }

        //private void FillCompanyCategoriesModel(CompanyCategoryModel category, AdminCompanyCategoriesModel model, List<CompanyCategoryModel> list, int depth)
        //{
        //    model.CompanyCategories.Add(new CompanyCategoryHierarchyModel { Category = category, Depth = depth });

        //    foreach (var c in list.Where(x => x.ParentCompanyCategoryId == category.CompanyCategoryId))
        //        FillCompanyCategoriesModel(c, model, list, depth + 1);
        //}

        //[HttpPost("company/map-categories")]
        //public async Task<IActionResult> MapCategories([FromForm] AdminCompanyCategoriesModel model)
        //{
        //    var resuqetModel = new CompanyCategoriesModel
        //    {
        //        CompanyId = model.CompanyId,
        //        Categories = model.CompanyCategories.Select(x => x.Category).ToList()
        //    };

        //    var response = await _http.Post<bool>("/api/admin/marketplace/map-company-categories", resuqetModel);
        //    var success = response.Result;

        //    return await Task.FromResult(RedirectToAction("CompanyCategories", new { id = model.CompanyId }));
        //}

        [HttpGet("category/list")]
        public async Task<IActionResult> CategoryList()
        {
            var response = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = response.Result;

            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,

                    Name = x.Name,
                    ImageUrl = x.ImageUrl,

                    InHeader = x.InHeader,
                    WasInHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    WasPopular = x.IsPopular,
                    DeliveryDays = x.DeliveryDays,
                    DeliveryCost = x.DeliveryCost,
                    Priority = x.Priority,
                    DailyProductPriority = x.DailyProductPriority,
                    IsEnabled = x.IsEnabled
                })
                .ToList();

            var model = new List<AdminCategoryModel>();

            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, model, 0);

            return await Task.FromResult(View(model.ToArray()));
        }

        [HttpGet("category/category-products-list")]
        public async Task<IActionResult> CategoryProductsList(int companyId, int companyCategoryId)
        {
            var response = await _http.Get<List<ProductInfoModel>>("/api/admin/marketplace/get-category-products", new Dictionary<string, string> { { "companyId", companyId.ToString() }, { "companyCategoryId", companyCategoryId.ToString() } });

            var products = response.Result;
            string categoryName = "Нет товаров";
            if (products != null && products.Count != 0)
                categoryName = products.First().CompanyCategoryName;

            CategoryProductsListModel productsModel = new CategoryProductsListModel() { Products = response.Result, Selector = _categories, CompanyCategoryName = categoryName };

            return await Task.FromResult(View(productsModel));
        }

        [HttpPost("category/change-product-category")]
        public async Task<bool> ChangeProductCategory(int id, int val)
        {
            var response = await _http.Get<bool>("/api/admin/marketplace/change-product-category", new Dictionary<string, string> { { "productId", id.ToString() }, { "categoryId", val.ToString() } });

            return response.Result;
        }

        private void ProcessCategoryHierarchy(AdminCategoryModel cat, List<AdminCategoryModel> source, List<AdminCategoryModel> categories, int depth)
        {
            cat.Depth = depth;
            categories.Add(cat);

            var childs = source.Where(x => x.ParentId == cat.Id);
            foreach (var child in childs)
                ProcessCategoryHierarchy(child, source, categories, depth + 1);
        }

        [HttpGet("room/list")]
        public async Task<IActionResult> RoomList()
        {
            var response = await _http.Get<RoomNameModel[]>("/api/admin/marketplace/get-category-rooms");
            var roomsModel = response.Result;
            return await Task.FromResult(View(roomsModel));
        }

        [HttpGet("color/list")]
        public async Task<IActionResult> ColorList()
        {
            var response = await _http.Get<MarketplaceColorModel[]>("/api/admin/marketplace/get-marketplace-colors");
            var roomsModel = response.Result;
            return await Task.FromResult(View(roomsModel));
        }

        [HttpGet("color/colorMapping")]
        public async Task<IActionResult> ColorMapping()
        {
            var responseMarketplaceColors = await _http.Get<MarketplaceColorModel[]>("/api/admin/marketplace/get-marketplace-colors");
            var marketplaceColors = responseMarketplaceColors.Result;
            var responseCompanyColors = await _http.Get<CompanyColorModel[]>("/api/admin/marketplace/get-company-colors");
            var companyColors = responseCompanyColors.Result;
            ViewData["MarketplaceColors"] = new SelectList(marketplaceColors, "ColorId", "Name");
            return await Task.FromResult(View(companyColors));
        }

        [HttpPost("room/edit")]
        public async Task<IActionResult> EditRoomName(RoomNameModel room)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/edit-category-room", room);
            return await Task.FromResult(RedirectToAction("RoomList"));
        }

        [HttpPost("room/create")]
        public async Task<IActionResult> AddRoomName(string room)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/add-category-room", room);
            return await Task.FromResult(RedirectToAction("RoomList"));
        }

        [HttpPost("color/edit")]
        public async Task<IActionResult> EditColor(MarketplaceColorModel color)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/edit-marketplace-color", color);
            return await Task.FromResult(RedirectToAction("ColorList"));
        }

        [HttpPost("color/create")]
        public async Task<IActionResult> AddColor(string color, string hex)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/add-marketplace-color", new MarketplaceColorModel { ColorId = 0, Name = color, Hex = hex });
            return await Task.FromResult(RedirectToAction("ColorList"));
        }

        [HttpPost("color/map")]
        public async Task<IActionResult> MapColor(int companyColorId, int marketplaceColorId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/map-company-color", new MapColorModel {CompanyColorId=companyColorId, MarketplaceColorId=marketplaceColorId });
            return await Task.FromResult(RedirectToAction("ColorMapping"));
        }

        [HttpPost("category/list/modify")]
        public async Task<IActionResult> ModifyCategoryList(AdminCategoryModel[] model)
        {
            var categoriesToChange = model
                .Where(x => x.WasInHeader != x.InHeader || x.WasPopular != x.IsPopular)
                .ToArray();

            foreach (var category in categoriesToChange)
            {
                var modifyModel = new ModifyCategoryModel
                {
                    Id = category.Id,
                    Name = category.Name,
                    InHeader = category.InHeader,
                    IsPopular = category.IsPopular,
                    ImageUrl = category.ImageUrl,
                    ParentId = category.ParentId,
                };

                var response = await _http.Post<bool>("/api/admin/marketplace/modify-category", modifyModel);
                var success = response.Result;
            }

            return await Task.FromResult(RedirectToAction("CategoryList"));
        }

        [HttpGet("category/{id}")]
        public async Task<IActionResult> Category(int id)
        {
            var response = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
            var categoriesModel = response.Result;

            var flatCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    ImageUrl = x.ImageUrl,
                    InHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    WasInHeader = x.InHeader,
                    WasPopular = x.IsPopular,
                    ParentId = x.ParentId,
                    IsEnabled = x.IsEnabled,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Priority = x.Priority,
                    DailyProductPriority = x.DailyProductPriority
                })
                .ToList();

            var model = flatCategories.FirstOrDefault(x => x.Id == id);

            var imagesResponse = await _http.Get<CategoryImageModel[]>("api/admin/marketplace/category-images");
            var images = imagesResponse.Result;

            ViewBag.Images = images;

            var hierarchialCategories = new List<AdminCategoryModel>();
            var root = flatCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, flatCategories, hierarchialCategories, 0);

            var categories = hierarchialCategories.Select(
                x => new Category
                {
                    Id = x.Id,
                    Name = FormCategorySelectListName(x.Name, x.Depth)
                })
                .ToList();

            categories.Insert(0, new Category { Id = 0, Name = string.Empty });

            ViewData["Categories"] = new SelectList(categories, "Id", "Name");
            ViewData["Parameters"] = (await _http.FillCategoryParameters());

            var categoryParametersIds = (await _http.Get<ParameterInfo[]>("api/admin/categories/category-parameters",
                    new Dictionary<string, string> {{nameof(id), id.ToString()}})).Result
                .Select(info => info.Id.ToString()).ToArray();
            foreach (var item in (SelectList) ViewData["Parameters"])
            {
                item.Selected = categoryParametersIds.Contains(item.Value);
            }



            return await Task.FromResult(View(model));
        }

        private string FormCategorySelectListName(string name, int depth)
        {
            var res = string.Empty;
            for (var i = 0; i < depth; i++)
                res += "--";
            res += " ";
            res += name;

            return res;
        }

        [HttpPost("category/modify")]
        public async Task<IActionResult> ModifyCategory([FromForm] ModifyCategoryModel model)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/modify-category", model);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("Category", new { id = model.Id }));
        }

        [HttpPost("category/delete")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var model = new DeleteCategoryModel { Id = id };
            var response = await _http.Post<bool>("/api/admin/marketplace/delete-category", model);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("CategoryList"));
        }

        [HttpPost("category/create")]
        public async Task<IActionResult> CreateCategory([FromForm] string name)
        {
            var model = new CreateCategoryModel { Name = name };
            var response = await _http.Post<bool>("/api/admin/marketplace/create-category", model);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("CategoryList"));
        }

        [HttpGet("enable-category/{id}")]
        public async Task<IActionResult> EnableCategory(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/enable-category", new CategoryIdModel() { Id = id });

            return await Task.FromResult(RedirectToAction("Category", new { id }));
        }

        [HttpGet("disable-category/{id}")]
        public async Task<IActionResult> DisableCategory(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/disable-category", new CategoryIdModel() { Id = id });

            return await Task.FromResult(RedirectToAction("Category", new { id }));
        }

        [HttpPost("categiory/upload-images")]
        public async Task<IActionResult> UploadCategoryImages([FromForm] List<IFormFile> files, [FromForm] int id)
        {
            var multipart = new MultipartFormDataContent();

            foreach (var file in files)
            {
                byte[] bytes;
                using (var r = new BinaryReader(file.OpenReadStream()))
                    bytes = r.ReadBytes((int)r.BaseStream.Length);

                var content = new ByteArrayContent(bytes);

                multipart.Add(content, "files", file.FileName);
            }

            var response = await _http.PostContent<bool>("/api/admin/marketplace/upload-category-images", multipart);
            var success = response.Result;

            return await Task.FromResult(RedirectToAction("Category", new { id }));
        }

        [DisableRequestSizeLimit]
        [HttpPost("validation/yml/check-file")]
        public async Task<IActionResult> CheckFile(IFormFile file)
        {
            byte[] bytes;
            using (var r = new BinaryReader(file.OpenReadStream()))
                bytes = r.ReadBytes((int)r.BaseStream.Length);

            var content = new ByteArrayContent(bytes);
            var multipart = new MultipartFormDataContent();
            multipart.Add(content, "file", file.FileName);

            var response = await _http.PostContent<UploadResultModel>("/api/admin/marketplace/show-yml-content", multipart);
            var responseData = response.Result;
            
            _lastYmlValidation = new YmlValidationModel()
            {
                YmlModel = responseData,
                DownloadUrl = string.Empty,
                 DownloadFailed = false,
                  ParseFailed = response.Code != "ok"
            };

            return await Task.FromResult(RedirectToAction("YmlValidation"));
        }

        
        [DisableRequestSizeLimit]
        [HttpPost("validation/yml/download-file")]
        public async Task<ActionResult> DownloadYMLFile(string url)
        {
            byte[] data = null;
            try
            {
                using (var client = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url)))
                    {
                        //request.Headers.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
                        //request.Headers.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
                        request.Headers.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                        //request.Headers.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                        using (var response = await client.SendAsync(request).ConfigureAwait(false))
                        {
                            using (HttpContent content = response.Content)
                            {
                                data = await content.ReadAsByteArrayAsync();
                            }
                            //response.EnsureSuccessStatusCode();
                            //using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                            //using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                            //using (var streamReader = new StreamReader(decompressedStream))
                            //{
                            //    sdsd = await streamReader.ReadToEndAsync().ConfigureAwait(false);
                            //}
                        }
                    }
                }
            }
            catch
            {
                _lastYmlValidation = new YmlValidationModel { DownloadFailed = true, DownloadUrl = url, ParseFailed = false, YmlModel = new UploadResultModel() };
                return await Task.FromResult(RedirectToAction("YmlValidation"));
            }
            return File(data, MediaTypeNames.Application.Octet,$"yml_{DateTime.Now.ToLongTimeString()}.xml");
        }

        [HttpGet("validation/yml/index")]
        public async Task<IActionResult> YmlValidation()
        {
            var model = _lastYmlValidation;
            _lastYmlValidation = null;
            return await Task.FromResult(View(model));
        }

        [HttpGet("validation/yml/uploadQueue")]
        public async Task<IActionResult> YmlUploadQueue()
        {
            var response = await _http.Get<YMLUploadTaskModel[]>("/api/admin/marketplace/get-yml-upload-queue", new Dictionary<string, string>());
            return await Task.FromResult(View(response.Result));
        }

        [HttpPost("validation/yml/delete-yml-upload-task")]
        public async Task<IActionResult> DeleteYMLUploadTask(int taskId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/delete-yml-upload-task", taskId);
            return await Task.FromResult(RedirectToAction("YmlUploadQueue"));
        }

        [HttpPost("validation/yml/process-yml-upload-forced")]
        public async Task<IActionResult> ProcessYMLUploadForced(int taskId)
        {
            var response = await _http.Post<bool>("/api/admin/marketplace/process-yml-upload-forced", taskId);
            return await Task.FromResult(RedirectToAction("YmlUploadQueue"));
        }


        [HttpGet("moderation")]
        public async Task<IActionResult> Moderation()
        {
            var response = await _http.Get<ModerationCountsModel>("/api/admin/marketplace/get-moderation-product");
            return await Task.FromResult(View(response.Result));
        }

        [HttpGet("moderationform")]
        public async Task<IActionResult> ModerationForm(string type)
        {
            var responseProduct = await _http.Get<GetModerationProductModel>("/api/admin/marketplace/get-product-for-moderation", new Dictionary<string, string>() { { "type", type } });
            var product = responseProduct.Result;

            var response = await _http.Get<MarketplaceColorModel[]>("/api/admin/marketplace/get-marketplace-colors");
            var colors = response.Result;
            var model = new ModerationFormModel() { Product = product, Type = type, Colors = colors };
            
            return await Task.FromResult(View(model));
        }

        [HttpPost("moderationsetproduct")]
        public async Task<bool> ModerationSetProduct(string type, string description, string colorId, int rating, int productId)
        {
            var colors = JsonConvert.DeserializeObject<int[]>(colorId);

            ModerationProductModel model = new ModerationProductModel() { ProductId = productId, ColorId = colors, Rating = rating, Description = description };

            var response = await _http.Post<bool>("/api/admin/marketplace/set-moderation-product", model);


            //await ModerationForm(type);

            return true;// RedirectToAction("ModerationForm", new { type });
        }

        [HttpGet("products-stats")]
        public async Task<IActionResult> ProductsStats()
        {
            var response = await _http.Get<Design.Models.Marketplace.ProductsStats>("/api/admin/products-stats");

            return View(response.Result);
        }

        [HttpGet("disable-autofeed/{id}")]
        public async Task<IActionResult> DisableAutoFeed(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/disable-autofeed", new CompanyIdModel() { Id = id });

            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpGet("enable-autofeed/{id}")]
        public async Task<IActionResult> EnableAutoFeed(int id)
        {
            var resp = await _http.Post<bool>("/api/admin/marketplace/enable-autofeed", new CompanyIdModel() { Id = id });

            return await Task.FromResult(RedirectToAction("Company", new { id }));
        }

        [HttpGet("findproduct")]
        public async Task<IActionResult> FindProduct()
        {
            var model = new FindProductModel() { Products = new List<ProductInfoModel>(), Selector = _categories };
            return await Task.FromResult(View(model));
        }

        [HttpGet("savebaskets")]
        public async Task<IActionResult> SavedBaskets()
        {
            var model = new UserSavedBasketsModel() ;
            return await Task.FromResult(View(model));
        }

        [HttpPost("savebaskets")]
        public async Task<IActionResult> SavedBaskets(int userId)
        {
            var model = await _http.Get<UserSavedBasketsModel>("/api/savedBaskets/get-saved-baskets-by-user", new Dictionary<string, string>() { { "userId", userId.ToString() }});
            return await Task.FromResult(View(model.Result));
        }

        [HttpPost("searchforproduct")]
        public async Task<IActionResult> SearchForProduct(int id, string vendorCode)
        {
            return await Task.FromResult(RedirectToAction("FindProduct", new { id = id, vendorCode = vendorCode }));
        }

        [HttpGet("lottery")]
        public async Task<IActionResult> Lottery()
        {
            var response = await _http.Get<TildaLotteryRequest[]>("/tilda/get-lottery-requests");
            return View(response.Result);
        }

        [HttpGet("findproduct/{id}")]
        public async Task<IActionResult> FindProduct(int id, string vendorCode)
        {
            List<ProductInfoModel> models = null;
            if (id == 0 && string.IsNullOrEmpty(vendorCode))
                models = new List<ProductInfoModel>();
            else
            {
                if (vendorCode == null)
                    vendorCode = string.Empty;
                var resp = await _http.Get<List<ProductInfoModel>>("/api/admin/marketplace/get-product", new Dictionary<string, string>() { { "productId", id.ToString() }, { "vendorCode", vendorCode } });
                models = resp.Result ?? new List<ProductInfoModel>();
            }

            if (_categories == null)
            {
                var categoriesResponse = await _http.Get<CategoriesModel>("/api/admin/marketplace/get-categories");
                var categoriesModel = categoriesResponse.Result;

                var adminCategories = categoriesModel.Categories
                    .Select(x => new AdminCategoryModel
                    {
                        Id = x.Id,
                        ParentId = x.ParentId,
                        Name = x.Name,
                        DeliveryCost = x.DeliveryCost,
                        DeliveryDays = x.DeliveryDays,
                        Depth = 0
                    })
                    .ToList();

                var categoriesList = new List<AdminCategoryModel>();

                var root = adminCategories.Where(x => x.ParentId == 0);
                foreach (var cat in root)
                    ProcessCategoryHierarchy(cat, adminCategories, categoriesList, 0);

                var categories = categoriesList
                    .Select(x => new Category
                    {
                        Id = x.Id,
                        Name = FormCategorySelectListName(x.Name, x.Depth)
                    })
                    .ToList();

                categories.Insert(0, new Category { Id = 0, Name = string.Empty });

                _categories = new SelectList(categories, "Id", "Name");
            }

            var newModel = new FindProductModel() { Products = models, Selector = _categories };

            return await Task.FromResult(View(newModel));
        }

        [HttpGet("download-collection-csv/{companyId}")]
        public async Task<IActionResult> DownloadCollectionsCSV(int companyId)
        {
            var csvBytes = await _http.Get<byte[]>("api/admin/get-collections-csv-for-company", new Dictionary<string, string> { { "companyId", companyId.ToString() } });
            return File(csvBytes.Result, "application/text", "comany-" + companyId.ToString() + "-collection.csv");
        }

        [HttpGet("get-collections/{companyId}")]
        public async Task<IActionResult> CompanyCollections(int companyId)
        {
            var result = await _http.Get<AdminCompanyProductCollectionsModel>("api/admin/get-prdouct-collections-for-company", new Dictionary<string, string> { { "companyId", companyId.ToString() } });
            var collections = result.Result;
            if (collections.Collections == null)
                collections.Collections = new List<AdminProductCollectionShortModel>();
            return await Task.FromResult(View(collections));
        }

        [HttpGet("get-collection/{collectionId}")]
        public async Task<IActionResult> CompanyCollection(int collectionId)
        {
            var result = await _http.Get<AdminProductCollectionFullModel>("api/admin/get-product-collection", new Dictionary<string, string> { { "collectionId", collectionId.ToString() } });
            return await Task.FromResult(View(result.Result));
        }

        [HttpPost("change-collection")]
        public async Task<IActionResult> ChangeCollection(AdminProductCollectionFullModel model)
        {
            var change = new AdminProductCollectionShortModel
            {
                Available = model.Available,
                Id = model.Id,
                KeyWord = model.KeyWord,
                Name = model.Name
            };
            var result = await _http.Post<bool>("api/admin/change-product-collection", change);
            return await Task.FromResult(RedirectToAction("CompanyCollection", new { collectionId = model.Id }));
        }

        [HttpPost("manually-update-company-collections")]
        public async Task<IActionResult> ManuallyUpdateCompanyCollections(int companyId)
        {
            var result = await _http.Post<bool>("api/admin/create-collections-for-company", companyId);
            return await Task.FromResult(RedirectToAction("CompanyCollections", new { companyId = companyId }));
        }
    }
}