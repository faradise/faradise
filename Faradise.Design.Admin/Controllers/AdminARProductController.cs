﻿using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("ar-product")]
    [ResponseCache(NoStore = true)]
    public class AdminARProductController : Controller
    {
        private HttpService _http = null;

        public AdminARProductController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("ar-product/{id}")]
        public async Task<IActionResult> ARProduct(int id)
        {
            var product = await _http.Get<AdminARProductModel>("api/admin/marketplace/ar/get-ar-product-info", new Dictionary<string, string> { { "arProductId", id.ToString() } });
            if (product.Result != null)
                return View(product.Result);
            return RedirectToAction("ARProductList");
        }

        [HttpPost("set-ar-to-product")]
        public async Task<IActionResult> SetArToProduct(int? productId, int arId)
        {
            var result = await _http.Post<bool>("api/admin/marketplace/ar/set-product-to-ar", new SetArProductModel { ArProductId = arId, ProductId = productId });
            return RedirectToAction("ARProduct", new { id = arId });
        }

        [HttpGet("ar-product-list")]
        public async Task<IActionResult> ARProductList()
        {
            var products = await _http.Get<AdminARProductShortModel[]>("api/admin/marketplace/ar/get-products");
            return View(products.Result);
        }

        [HttpGet("parse-csv")]
        public async Task<IActionResult> ParseCSV()
        {
            await _http.Get<AdminARProductShortModel[]>("api/admin/marketplace/parsing");
            return RedirectToAction("ARProductList");
        }
    }
}
