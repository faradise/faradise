﻿using Faradise.Design.Admin.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("authentication")]
    public class AdminAuthenticationController : Controller
    {
        private const string ADMIN_LOGIN = "faradise";
        private const string ADMIN_PASSWORD = "F31NWp121991";

        [HttpGet]
        public IActionResult Login(string to)
        {
            return View("Login", new AuthenticationModel());
        }

        [HttpPost]
        public async Task<IActionResult> Login(AuthenticationModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Name == ADMIN_LOGIN && model.Password == ADMIN_PASSWORD)
                {
                    await Authenticate(model.Name, model.Password);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                    return View(model);
                }
            }

            return View(model);
        }

        private async Task Authenticate(string name, string password)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, name)
            };

            ClaimsIdentity id = new ClaimsIdentity(
                claims, 
                "ApplicationCookie", 
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, 
                new ClaimsPrincipal(id));
        }
    }
}
