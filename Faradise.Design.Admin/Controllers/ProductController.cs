﻿using System.Collections.Generic;
using Faradise.Design.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Faradise.Design.Admin.Services;
using Faradise.Design.Models.Admin.Product;
using System;
using System.IO;

namespace Faradise.Design.Admin.Controllers
{
    [Route("product")]
    public class ProductController : Controller
    {
        private HttpService _http = null;

        public ProductController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("images-priority/{id}")]
        public async Task<IActionResult> ImagesPriority(int id)
        {
            var response = await _http.Get<ProductImages>("api/admin/product/images-priority", new Dictionary<string, string> {{"productId", id.ToString()}});
            var model = response.Result;

            return View(model);
        }

        [HttpPost("set-images-priority")]
        public async Task<IActionResult> SetImagesPriority([FromForm] ProductImages model)
        {
            var response = await _http.Post<bool>("api/admin/product/set-images-priority", model);
            return RedirectToAction("ImagesPriority", new {id = model.ProductId});
        }

        [HttpGet]
        public async Task<IActionResult> SetImagesPriorityy([FromForm] ProductImages model)
        {
            var response = await _http.Post<bool>("api/admin/product/set-images-priority", model);
            return RedirectToAction("ImagesPriority", new { id = model.ProductId });
        }

        [HttpGet("download-yml")]
        public async Task<IActionResult> DownloadYml()
        {
            return View(new ModifyProductCompilationModel());
        }

        [HttpPost("download-feed-by-ids")]
        public async Task<IActionResult> ModifyProductCompilation(ModifyProductCompilationModel modifymodel)
        {
            var products = ParseProducts(modifymodel.ProductString);
            var response = await _http.Post<byte[]>("api/admin/feed/get-feed-by-ids", products);
            var bytes = response.Result;
            var file = File(bytes, "application/xml", modifymodel.Name+".xml");
            return file;
        }



        private int[] ParseProducts(string productString)
        {
            if (string.IsNullOrEmpty(productString))
                return new int[0];
            var split = productString.Split(new char[] { '\n', '\r', '\t', ';', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length < 1)
                return new int[0];
            var productList = new List<int>();
            foreach (var s in split)
            {
                int productId = 0;
                if (int.TryParse(s, out productId))
                {
                    productList.Add(productId);
                }
            }
            return productList.ToArray();
        }
    }
}
