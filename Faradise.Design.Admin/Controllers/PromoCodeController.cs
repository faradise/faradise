﻿using Faradise.Design.Admin.Models.PromoCode;
using Faradise.Design.Admin.Services;
using Faradise.Design.Models.PromoCode;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [ResponseCache(NoStore = true)]
    public class PromoCodeController : Controller
    {
        private HttpService _http = null;

        public PromoCodeController(HttpService http)
        {
            _http = http;
        }


        [HttpGet("promocode/list")]
        public async Task<IActionResult> PromoCodeList()
        {
            var response = await _http.Get<PromoCodeExport[]>("api/admin/get-promo-codes");
            var responseModel = response.Result;

            var model = new PromoCodeListModel()
            {
                PromoCodes = responseModel.Select(x => new PromoCodeModel()
                {
                    Name = x.Name,
                    Count = x.Count,
                    CountUses = x.Used,
                    StartDate = x.StartDate.AddHours(3),
                    ExpireDate = x.ExpireDate.AddHours(3),
                    Percents = x.PercentsDiscount,
                    Absolute = x.AbsoluteDisocunt,
                    PromoCodeType = x.CodeType,
                    MinPrice = x.MinPrice
                }).ToList()
            };

            return await Task.FromResult(View(model));
        }

        [HttpPost("promocode/check-promo-code")]
        public async Task<bool> CheckPromoCode(string name)
        {
            var response = await _http.Get<PromoCodeExport>("api/admin/get-promo-code", new Dictionary<string, string> { { "name", name } });
            var model = response.Result;

            if (model != null)
                return true;
            else
                return false;
        }

        [HttpGet("promocode/create-menu")]
        public async Task<IActionResult> CreatePromoCode()
        {
            var model = new CreatePromoCodeModel() { StartDate = DateTime.Now, ExpireDate = DateTime.Now };

            return await Task.FromResult(View(model));
        }

        [HttpPost("promocode/create")]
        public async Task<IActionResult> Create(PromoCodeModel model)
        {
            var m = new PromoCodeExport()
            {
                Name = model.Name,
                Count = model.Count,
                CodeType = model.PromoCodeType,
                StartDate = model.StartDate,
                ExpireDate = model.ExpireDate,
                PercentsDiscount = model.Percents,
                AbsoluteDisocunt = model.Absolute,
                MinPrice = model.MinPrice
            };
            _http.Post<bool>("/api/admin/create-promo-code", m);

            return await Task.FromResult(RedirectToAction("PromoCodeList"));
        }

        [HttpPost("promocode/find")]
        public async Task<IActionResult> FindPromoCode([FromForm] string code)
        {
            var response = await _http.Get<PromoCode>("api/admin/find-promo-code", new Dictionary<string, string> { { "code", code } });
            var model = response.Result;
            if (model != null)
            {
                model.StartDate.AddHours(3);
                model.ExpireDate.AddHours(3);
            }

            return await Task.FromResult(View(model));
        }

        [HttpGet("promocode/get-csv")]
        public async Task<IActionResult> GetPromoCodesCsv(string code)
        {
            var response = await _http.Get<PromoCode[]>("api/admin/get-codes-csv", new Dictionary<string, string> { { "code", code } });
            var model = response.Result;

            var exportCsv = new StringBuilder();

            exportCsv.AppendLine(string.Format("{0};{1};{2};{3};{4}", "Code", "issue-start", "issue-end", "available-from", "available-till"));

            foreach(var m in model)
            {
                exportCsv.AppendLine(string.Format("{0};{1};{2};{3};{4}", m.Code, m.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), m.ExpireDate.ToString("yyyy-MM-dd HH:mm:ss"), m.StartDate.ToString("yyyy-MM-dd HH:mm:ss"), m.ExpireDate.ToString("yyyy-MM-dd HH:mm:ss")));
            }


            var export = exportCsv.ToString();
            var bytes = Encoding.UTF8.GetBytes(export);

            var name = string.Format("export_{0}_{1}.csv",
                code,
                DateTime.Now.ToString("yyyy-MM-dd-hh:mm"));

            return File(bytes, "text/csv", name);
        }
    }
}
