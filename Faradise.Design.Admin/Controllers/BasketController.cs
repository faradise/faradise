﻿using Faradise.Design.Admin.Services;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Controllers
{
    [Route("marketplace")]
    [ResponseCache(NoStore = true)]
    public class BasketController : Controller
    {
        private HttpService _http = null;
        private bool _priceMissMatch = false;

        public BasketController(HttpService http)
        {
            _http = http;
        }

        [HttpGet("orders/list")]
        public async Task<IActionResult> OrderList()
        {
            var response = await _http.Get<ShortProductOrderModel[]>("api/admin/get-basket-orders", new Dictionary<string, string> { { "from", "0" }, { "count", int.MaxValue.ToString() } });
            var model = response.Result;
            return await Task.FromResult(View(model));
        }

        [HttpPost("order/add-delivery-price")]
        public async Task<IActionResult> AddAdditinalPrice(int orderId, int cost)
        {
            var response = await _http.Post<bool>("api/admin/add-basket-price", new SetAdditionalPriceForOrderModel { OrderId = orderId, Price = cost });
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/add-basket-delivery-price")]
        public async Task<IActionResult> AddDeliveryPrice(int orderId, int cost)
        {
            var response = await _http.Post<bool>("api/admin/add-basket-delivery-price", new SetAdditionalPriceForOrderModel { OrderId = orderId, Price = cost });
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpGet("order/{id}")]
        public async Task<IActionResult> Order(int id)
        {
            var response = await _http.Get<FullBasketOrderModel>("api/admin/get-basket-order", new Dictionary<string, string> { { "orderId", id.ToString() } });
            var model = response.Result;
            if (_priceMissMatch)
            {
                model.PriceMissmatch = true;
                _priceMissMatch = false;
            }
            return await Task.FromResult(View(model));
        }

        [HttpPost("order/complete-order")]
        public async Task<IActionResult> CompleteOrder(int orderId)
        {
            var response = await _http.Post<bool>("api/admin/finish-basket-order", orderId);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/move-order-to-work")]
        public async Task<IActionResult> MoveOrderToWork(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.InWork };
            var response = await _http.Post<bool>("api/admin/set-basket-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/move-credit-order-to-work")]
        public async Task<IActionResult> MoveCreditOrderToWork(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditApprovedInWork };
            var response = await _http.Post<bool>("api/admin/set-basket-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/set-credit-order-approved")]
        public async Task<IActionResult> SetCreditOrderToApproved(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditApprovedWaitingForManager };
            var response = await _http.Post<bool>("api/admin/set-basket-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/set-credit-order-not-approved")]
        public async Task<IActionResult> SetCreditOrderToNotApproved(int orderId)
        {
            var setStatusModel = new SetOrderStatusModel { OrderId = orderId, Status = OrderStatus.CreditNotApproved };
            var response = await _http.Post<bool>("api/admin/set-basket-order-status", setStatusModel);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/cancel-order")]
        public async Task<IActionResult> CancelOrderToWork(int orderId)
        {
            var response = await _http.Post<bool>("api/admin/cancel-basket-order", orderId);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/refresh-order-price")]
        public async Task<IActionResult> RefreshOrderPrice(int orderId)
        {
            var response = await _http.Post<bool>("api/admin/refresh-basket-order-price", orderId);
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/add-payment")]
        public async Task<IActionResult> AddPaymentToOrder(int orderId)
        {
            var response = await _http.Post<string>("api/admin/create-payment-for-basket-order", orderId);
            if (response.Result == "price_missmatch_error")
                _priceMissMatch = true;
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/remove-products")]
        public async Task<IActionResult> RemoveProducts(int orderId, int productId, int count)
        {
            count = Math.Max(0, count);
            var response = await _http.Post<string>("api/admin/remove-products-from-basket-order", new RemoveProductsFromBasketOrderModel { ProductId = productId, OrderId = orderId, Count = count });
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/add-products")]
        public async Task<IActionResult> AddProducts(int orderId, int productId, int count)
        {
            count = Math.Max(0, count);
            var response = await _http.Post<string>("api/admin/add-products-to-basket-order", new AddProductsToBasketOrderModel { ProductId = productId, OrderId = orderId, Count = count });
            return RedirectToAction("Order", new { id = orderId });
        }

        [HttpPost("order/change-order")]
        public async Task<IActionResult> ChangeOrderDetails(FullBasketOrderModel model)
        {
            var changesModel = new ChangeOrderDescriptionModel
            {
                Adress = model.Adress,
                City = model.City,
                Description = model.Description,
                Email = model.Email,
                Name = model.Name,
                OrderId = model.OrderId,
                Phone = model.Phone,
                Surname = model.Surname
            };
            var response = await _http.Post<bool>("api/admin/change-basket-order-info", changesModel);
            return RedirectToAction("Order", new { id = model.OrderId });
        }

        [HttpPost("order/set-promocode")]
        public async Task<IActionResult> SetPromocode(int orderId, string promocode)
        {
            var response = await _http.Post<bool>("api/admin/change-basket-order-promocode", new ChangeBasketOrderPromocodeModel { OrderId = orderId, PromoCode = promocode });
            return RedirectToAction("Order", new { id = orderId });
        }


        [HttpPost("order/change-payment-method")]
        public async Task<IActionResult> ChangePaymentMethod(PaymentMethod selected, int orderId)
        {
            var response = await _http.Post<bool>("api/admin/change-payment-method", new ChangePaymentMethodModel { OrderId = orderId, PaymentMethod = selected });
            return RedirectToAction("Order", new { id = orderId });
        }
    }
}
