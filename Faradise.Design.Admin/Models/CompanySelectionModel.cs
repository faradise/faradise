﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class CompanySelectionModel
    {
        public int Id { get; set; }
        public bool IsSelected { get; set; }
        public string Name { get; set; }
    }
}
