﻿namespace Faradise.Design.Admin.Models
{
    public class ChatModel
    {
        public int ProjectId { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int DesignerId { get; set; }
        public string DesignerName { get; set; }

        public string EndpointAPI { get; set; }
        public string EndpointChat { get; set; }
    }
}
