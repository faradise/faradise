﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Faradise.Design.Admin.Models
{
    public class CompanyCategoryHierarchyModel
    {
        public int CompanyId { get; set; }

        public RoomNameModel[] Rooms { get; set; }
        public SelectList Selector { get; set; }

        public CompanyCategoryModel Category { get; set; }
        public int Depth { get; set; }
    }
}
