﻿using Faradise.Design.Controllers.API.Models.Admin;

namespace Faradise.Design.Admin.Models
{
    public class DesignerRejectedShortListModel
    {
        public DesignerRejectedInfo[] Designers { get; set; }
    }
}
