﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class YmlValidationModel
    {
        public string DownloadUrl { get; set; }
        public UploadResultModel YmlModel { get; set; }
        public bool DownloadFailed { get; set; }
        public bool ParseFailed { get; set; }
    }
}
