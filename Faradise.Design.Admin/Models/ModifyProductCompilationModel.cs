﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class ModifyProductCompilationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductString { get; set; }
    }
}
