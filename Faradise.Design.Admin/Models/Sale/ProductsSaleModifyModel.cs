﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class ProductsSaleModifyModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int Percent { get; set; }
        public string Description { get; set; }
        public string ProductString { get; set; }
    }
}
