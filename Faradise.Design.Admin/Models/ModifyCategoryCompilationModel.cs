﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class ModifyCategoryCompilationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CategoryForSelectionModel[] Categories { get; set; }
    }

    public class CategoryForSelectionModel
    {
        public bool IsSelected { get; set; }
        public AdminCategoryModel Category { get; set; }
    }
}
