﻿using Faradise.Design.Models.Toloka;

namespace Faradise.Design.Admin.Models
{
    public class TolokaTasksModel
    {
        public TolokaTaskModel[] Tasks { get; set; }
    }
}
