﻿using System.Collections.Generic;

namespace Faradise.Design.Admin.Models
{
    public class TestingViewModel
    {
        public Dictionary<int, QuestionViewModel> Questions { get; set; }
    }
}
