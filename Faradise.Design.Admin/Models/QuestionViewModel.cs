﻿using System.Collections.Generic;

namespace Faradise.Design.Admin.Models
{
    public class QuestionViewModel
    {
        public string Text { get; set; }
        public Dictionary<int, string> Answers { get; set; }
    }
}
