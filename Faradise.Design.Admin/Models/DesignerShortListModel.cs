﻿using Faradise.Design.Controllers.API.Models.Admin;

namespace Faradise.Design.Admin.Models
{
    public class DesignerShortListModel
    {
        public DesignerInfo[] Designers { get; set; }
    }
}
