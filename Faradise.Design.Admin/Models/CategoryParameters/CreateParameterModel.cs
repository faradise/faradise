﻿using System;
using Faradise.Design.Models.CategoryParameters;
using Faradise.Design.Models.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Faradise.Design.Admin.Models.CategoryParameters
{
    public class CreateParameterModel
    {
        public ParameterInfo Parameter { get; set; }
        public SelectList Categories { get; set; }
        public SelectList Types => new SelectList(Enum.GetValues(typeof(ParameterType)));
    }
}