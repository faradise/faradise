﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Faradise.Design.Admin.Models
{
    public class CategoryProductsListModel
    {
        public List<ProductInfoModel> Products { get; set; }
        public SelectList Selector { get; set; }
        public string CompanyCategoryName { get; set; }
    }
}
