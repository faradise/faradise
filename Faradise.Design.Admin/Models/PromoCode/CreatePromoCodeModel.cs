﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models.PromoCode
{
    public class CreatePromoCodeModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public int MinPrice { get; set; }
        public int Percents { get; set; }
        public int Absolute { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public PromoCodeType PromoCodeType { get; set; }
    }
}
