﻿using Faradise.Design.Models.Enums;
using System;

namespace Faradise.Design.Admin.Models.PromoCode
{
    public class PromoCodeModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public int CountUses { get; set; }
        public int Percents { get; set; }
        public int Absolute { get; set; }
        public int MinPrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public PromoCodeType PromoCodeType { get; set; }
    }
}
