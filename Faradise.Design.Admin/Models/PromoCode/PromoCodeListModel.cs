﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models.PromoCode
{
    public class PromoCodeListModel
    {
        public List<PromoCodeModel> PromoCodes { get; set; }
    }
}
