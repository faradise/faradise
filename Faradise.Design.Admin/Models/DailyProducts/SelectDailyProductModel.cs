﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Models
{
    public class SelectDailyProductModel
    {
        public DailyProductModel DailyProduct { get; set; }
        public PossibleDailyProductModel[] PossibleProducts { get; set; }
        public float CompanyKoef { get; set; }
        public float CategoryKoef { get; set; }
        public float SaleKoef { get; set; }
        public int Page { get; set; }
        public int SelectedId { get; set; }
    }
}
