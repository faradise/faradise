﻿using Faradise.Design.Models.Toloka;

namespace Faradise.Design.Admin.Models
{
    public class TolokaSegmentsModel
    {
        public TolokaTaskSegmentModel[] Segments { get; set; }
        public int TaskId { get; set; }
    }
}
