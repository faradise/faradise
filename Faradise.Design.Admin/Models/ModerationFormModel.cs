﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;

namespace Faradise.Design.Admin.Models
{
    public class ModerationFormModel
    {
        public GetModerationProductModel Product { get; set; }
        public string Type { get; set; }
        public MarketplaceColorModel[] Colors { get; set; }
    }
}
