﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Middleware
{
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthorizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/authentication") || context.User.Identity.IsAuthenticated)
                await _next.Invoke(context);
            else context.Response.Redirect("/authentication");
        }
    }

    public static class AuthorizationMiddlewareExtension
    {
        public static IApplicationBuilder UseSimpleAuthorization(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}
