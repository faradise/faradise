﻿using Faradise.Design.Admin.Services.Classes;
using Faradise.Design.Admin.Services.Options;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Services
{
    public class HttpService
    {
        private HttpServiceOptions _options = null;

        public HttpService(IOptions<HttpServiceOptions> options)
        {
            _options = options.Value;
        }

        public async Task<Response<T>> Get<T>(string route, Dictionary<string, string> query = null)
        {
            using (var client = new HttpClient())
            {
                var uri = CombineUri(route, query);
                var response = await client.GetAsync(uri);
                var responseString = await response.Content.ReadAsStringAsync();

                return ParseResult<T>(responseString);
            }
        }

        public async Task<Response<T>> PostContent<T>(string route, HttpContent content)
        {
            using (var client = new HttpClient())
            {
                var uri = CombineUri(route);
                var response = await client.PostAsync(uri, content);
                var responseString = await response.Content.ReadAsStringAsync();

                return ParseResult<T>(responseString);
            }
        }

        public async Task<Response<T>> Post<T>(string route, object body = null)
        {
            using (var client = new HttpClient())
            {
                var uri = CombineUri(route);
                var bodyJson = JsonConvert.SerializeObject(body);
                var httpContent = new StringContent(bodyJson, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(uri, httpContent);
                var responseString = await response.Content.ReadAsStringAsync();

                return ParseResult<T>(responseString);
            }
        }

        private Response<T> ParseResult<T>(string responseString)
        {
            var result = JsonConvert.DeserializeObject<Response<T>>(responseString);
            return result;
        }

        private string CombineUri(string relative, IDictionary<string, string> query = null)
        {
            var stringBaseUri = "http://faradise.design:80";
            var baseUri = new Uri(stringBaseUri);
            var uri = new Uri(baseUri, relative);
            var stringUri = uri.ToString();

            if (query == null)
                return stringUri;
            else return QueryHelpers.AddQueryString(stringUri, query);
        }

        private static bool HasProperty(dynamic obj, string name)
        {
            Type type = obj.GetType();

            if (type == typeof(ExpandoObject))
                return ((IDictionary<string, object>)obj).ContainsKey(name);
            
            return type.GetProperty(name) != null;
        }
    }
}
