﻿namespace Faradise.Design.Admin.Services.Options
{
    public class HttpServiceOptions
    {
        public string Uri { get; set; }
    }
}
