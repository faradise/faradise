﻿using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Services
{
    public static class TranslateService
    {
        private static readonly Dictionary<ProjectStage, string> _projectStageNames = new Dictionary<ProjectStage, string>
        {
            {ProjectStage.Description, "Стадия описания" },
            {ProjectStage.Preparetions, "Стадия подготовки" },
            {ProjectStage.Development, "Стадия разработки" }
        };

        private static readonly Dictionary<DevelopmentStage, string> _projectDevelopmentStageNames = new Dictionary<DevelopmentStage, string>
        {
            { DevelopmentStage.Moodboard, "Стадия мудборда" },
            { DevelopmentStage.Brigade, "Стадия ТЗ для бригады" },
            { DevelopmentStage.Collage, "Стадия коллажа" },
            { DevelopmentStage.Curation, "Стадия надзора" },
            { DevelopmentStage.Plan, "Стадия плана" },
            { DevelopmentStage.Visualization, "Стадия визуализации" },
            { DevelopmentStage.WaitForFinish, "Ожидание завершения" },
            { DevelopmentStage.Zoning, "Стадия зонирования" }

        };

        private static readonly Dictionary<CooperateStage, string> _projectDescriptionStageNames = new Dictionary<CooperateStage, string>
        {
            {CooperateStage.AdditionalInfo, "Заполнение дополнительной информации" },
            {CooperateStage.Budget, "Заполнение бюджета" },
            {CooperateStage.Meeting, "Встреча" },
            {CooperateStage.PhotoAndProportions, "Заполнение фото и пропорций" },
            {CooperateStage.Style, "Выбор стилей" },
            {CooperateStage.WhatToSave, "Выбор вещей, остающихся на объекте" }

        };

        public static string GetTranslation(this ProjectStage stage)
        {
            return _projectStageNames.GetValueOrDefault(stage);
        }

        public static string GetTranslation(this DevelopmentStage stage)
        {
            return _projectDevelopmentStageNames.GetValueOrDefault(stage);
        }

        public static string GetTranslation(this CooperateStage stage)
        {
            return _projectDescriptionStageNames.GetValueOrDefault(stage);
        }
    }
}
