﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Admin.Services.Classes
{
    public static class ArrayExtension
    {
        public static bool IsNullOrEmpty(this Array array)
        {
            if (array == null)
                return true;
            return array.Length == 0;
        }

        public static bool IsNullOrEmpty<T>(this List<T> list)
        {
            if (list == null)
                return true;
            return list.Count == 0;
        }
    }
}
