﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Models.CategoryParameters;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Faradise.Design.Admin.Services.Classes
{
    public static class ApiExtensions
    {
        public static async Task<SelectList> FillCategories(this HttpService service,
            string route = "/api/admin/marketplace/get-categories")
        {
            var categoriesModel = (await service.Get<CategoriesModel>(route)).Result;
            var adminCategories = categoriesModel.Categories
                .Select(x => new AdminCategoryModel
                {
                    Id = x.Id,
                    ParentId = x.ParentId,
                    Name = x.Name,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Depth = 0
                })
                .ToList();
            var categoriesList = new List<AdminCategoryModel>();
            var root = adminCategories.Where(x => x.ParentId == 0);
            foreach (var cat in root)
                ProcessCategoryHierarchy(cat, adminCategories, categoriesList, 0);
            var categories = categoriesList
                .Select(x => new Category
                {
                    Id = x.Id,
                    Name = FormCategorySelectListName(x.Name, x.Depth)
                })
                .ToList();
            categories.Insert(0, new Category {Id = 0, Name = string.Empty});

            void ProcessCategoryHierarchy(AdminCategoryModel cat, List<AdminCategoryModel> source,
                List<AdminCategoryModel> category, int depth)
            {
                cat.Depth = depth;
                category.Add(cat);

                var childs = source.Where(x => x.ParentId == cat.Id);
                foreach (var child in childs)
                    ProcessCategoryHierarchy(child, source, category, depth + 1);
            }

            string FormCategorySelectListName(string name, int depth)
            {
                var res = string.Empty;
                for (var i = 0; i < depth; i++)
                    res += "--";
                res += " ";
                res += name;

                return res;
            }

            return new SelectList(categories, "Id", "Name");
        }

        public static async Task<SelectList> FillBrands(this HttpService service,
            string route = "/api/admin/marketplace/get-companies")
        {
            var companies = (await service.Get<CompaniesModel>(route))
                .Result
                .Companies;
            companies.Insert(0, new CompanyInfo {Id = 0, Name = string.Empty});
            return new SelectList(companies, nameof(CompanyInfo.Id), nameof(CompanyInfo.Name));
        }

        public static async Task<SelectList> FillCategoryParameters(this HttpService service,
            string route = "api/admin/categories/list")
        {
            var parameters = (await service.Get<ParameterInfo[]>(route)).Result;
            return new SelectList(parameters, nameof(ParameterInfo.Id), nameof(ParameterInfo.Name));
        }
    }
}