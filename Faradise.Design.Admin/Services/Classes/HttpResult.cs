﻿namespace Faradise.Design.Admin.Services.Classes
{
    public class Response<T>
    {
        public string Code { get; set; }
        public string Reason { get; set; }
        public string Message { get; set; }

        public T Result { get; set; }
    }
}
