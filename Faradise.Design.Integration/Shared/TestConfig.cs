﻿using System;

namespace Faradise.Design.Integration
{
    public static class TestConfig
    {
        private const string URL_ENVIRONMENT_VARIABLE = "INTEGRATION_DEPLOYMENT_URL";
        private const string LOCALHOST_TARGET_URL = "http://localhost:3001";

        public static string BaseUrl { get; private set; }

        static TestConfig()
        {
            var targetUrl = Environment.GetEnvironmentVariable(URL_ENVIRONMENT_VARIABLE);
            if (string.IsNullOrEmpty(targetUrl))
                targetUrl = LOCALHOST_TARGET_URL;

            BaseUrl = targetUrl;
        }
    }
}
