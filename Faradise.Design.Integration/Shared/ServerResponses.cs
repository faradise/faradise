﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faradise.Design.Integration
{
    public class EmptyResponse
    {
        public string code;
        public string reason;
        public string message;
    }

    public class ServerResponse<T>
    {
        public string code;
        public T result;
        public string reason;
        public string message;
    }
}
