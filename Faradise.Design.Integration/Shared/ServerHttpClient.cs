﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration
{
    public class ServerHttpClient
    {
        private readonly HttpClient _client = null;

        public ServerHttpClient(bool useCookie)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.UseCookies = true;
            clientHandler.CookieContainer = new CookieContainer();

            _client = new HttpClient(clientHandler);
        }

        public ServerHttpClient()
        {
            _client = new HttpClient();
        }

        public async Task<HttpResponseMessage> MakeGetRequestAsync(string token, string route, Dictionary<string, string> parameters)
        {
            var url = route;
            if (parameters != null && parameters.Count > 0)
            {
                url += "?";
                foreach (var p in parameters)
                    url += p.Key + "=" + p.Value + "&";
                url.Remove(url.Length - 1);
            }
            var msg = new HttpRequestMessage(HttpMethod.Get, url);
            if (!string.IsNullOrEmpty(token))
                msg.Headers.Add("token", token);
            msg.Headers.Add("accept", "text/plain");
            var response = await _client.SendAsync(msg);
            return response;
        }

        public async Task<HttpResponseMessage> MakePostJsonRequestAsync(string token, string route, string json)
        {
            var msg = new HttpRequestMessage(HttpMethod.Post, route);
            if (!string.IsNullOrEmpty(token))
                msg.Headers.Add("token", token);
            HttpResponseMessage response = null;
            if (json != null && !string.IsNullOrEmpty(json))
            {
                msg.Content = new StringContent(json, Encoding.UTF8, "application/json");
            }
            else
                msg.Headers.Add("accept", "text/plain");
            response = await _client.SendAsync(msg);
            return response;
        }

        public async Task<HttpResponseMessage> MakeGetObjRequestAsync(string token, string route, Object obj)
        {
            HttpResponseMessage response = null;
            string pString = string.Empty;
            if (obj != null)
            {
                pString = "?";
                var parapms = obj.GetType().GetProperties();
                foreach (var p in parapms)
                    pString += p.Name + "=" + p.GetValue(obj).ToString() + "&";
                pString = pString.Remove(pString.Length - 1);
            }
            var msg = new HttpRequestMessage(HttpMethod.Get, route + pString);
            if (!string.IsNullOrEmpty(token))
                msg.Headers.Add("token", token);
            msg.Headers.Add("accept", "text/plain");
            response = await _client.SendAsync(msg);
            return response;
        }

        public async Task<HttpResponseMessage> MakePostStringAsync(string token,string route, string content)
        {
            var msg = new HttpRequestMessage(HttpMethod.Post, route);
            if (!string.IsNullOrEmpty(token))
                msg.Headers.Add("token", token);
            HttpResponseMessage response = null;
            msg.Content = new StringContent(content);
            //msg.Headers.Add("accept", "text/plain");
            response = await _client.SendAsync(msg);
            return response;
        }

        public async Task<HttpResponseMessage> MakePostFormRequestAsync(string token, string route, Dictionary<string,string> formData)
        {
            var msg = new HttpRequestMessage(HttpMethod.Post, route);
            if (!string.IsNullOrEmpty(token))
                msg.Headers.Add("token", token);
            HttpResponseMessage response = null;
            msg.Headers.Add("accept", "text/plain");
            if (formData != null && formData.Count > 0)
            {
                var keysPaires = formData.Select(x => new KeyValuePair<string, string>(x.Key, x.Value)).ToArray();
                var content = new FormUrlEncodedContent(keysPaires);
                msg.Content = content;
            }
            response = await _client.SendAsync(msg);
            return response;
        }

        public async Task<HttpResponseMessage> MakePostMultipartFormRequestAsync(string token, string route, HttpContent content)
        {
            if (!string.IsNullOrEmpty(token))
                content.Headers.Add("token", token);
            HttpResponseMessage response = null;
            //content.Headers.Add("accept", "text/plain");
            
            response = await _client.PostAsync(route, content);
            return response;
        }

        public static void HandleResponse(HttpResponseMessage response, string step)
        {
            EmptyResponse json = null;
            string code = string.Empty;
            string message = string.Empty;
            string reason = string.Empty;
            if (response != null && response.Content != null)
            {
                var responseJsonString = response.Content.ReadAsStringAsync().Result;
                json = Newtonsoft.Json.JsonConvert.DeserializeObject<EmptyResponse>(responseJsonString);
                code = json != null ? json.code : string.Empty;
                reason = json != null ? json.reason : string.Empty;
                message = json != null ? json.message : string.Empty;
            }
            Assert.True(response.IsSuccessStatusCode && json.code == "ok",
                        $"request failed: STATUS_CODE: {response.StatusCode}, SERVER_CODE: {code}, MESSAGE: {message}, REASON:{reason} ON STEP {step}");
        }

        public static T HandleResponse<T>(HttpResponseMessage response, string step)
        {
            ServerResponse<T> json = null;
            string code = string.Empty;
            string message = string.Empty;
            string reason = string.Empty;
            if (response != null && response.Content != null)
            {
                var responseJsonString = response.Content.ReadAsStringAsync().Result;
                json = Newtonsoft.Json.JsonConvert.DeserializeObject<ServerResponse<T>>(responseJsonString);
                code = json != null ? json.code : string.Empty;
                reason = json != null ? json.reason : string.Empty;
                message = json != null ? json.message : string.Empty;
            }
            Assert.True(response.IsSuccessStatusCode && json.code == "ok",
                        $"request failed: STATUS_CODE: {response.StatusCode}, SERVER_CODE: {code}, MESSAGE: {message}, REASON:{reason} ON STEP {step}");
            return json.result;
        }
    }
}
