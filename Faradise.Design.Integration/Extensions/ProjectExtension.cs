﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using System.Linq;
using Xunit;

namespace Faradise.Design.Integration
{
    public static class ProjectExtension
    {
        public static void ValidateNameAndCity(this ProjectDescriptionModel project, SetBasicModel nameAndCity)
        {
            Assert.True(project.Name == nameAndCity.ProjectName, "Project name is wrong");
            Assert.True(project.City == nameAndCity.City, "Project city is wrong");
        }

        public static void ValidateInfo(this ProjectDescriptionModel project, SetInfoModel info)
        {
            Assert.True(project.ObjectType == info.ObjectType, "Set Object type is wrong");
            Assert.True(project.Rooms != null && project.Rooms.Length == info.Rooms.Length, "Room count is wrong");
            foreach (var room in project.Rooms)
            {
                var sameRoom = info.Rooms.FirstOrDefault(x => x.Purpose == room.Purpose && x.Count == room.Count);
                if (sameRoom == null)
                {
                    Assert.True(false, "Rooms info is wrong");
                    break;
                }
            }
            Assert.True(project.Reasons != null && project.Reasons.Length == info.Reasons.Length, "Reason count is wrong");
            foreach (var reason in project.Reasons)
            {
                var sameReason = info.Reasons.FirstOrDefault(x => x.Name == reason.Name && x.Description == reason.Description);
                if (sameReason == null)
                {
                    Assert.True(false, "Missing reason");
                    break;
                }
            }
        }

        public static void ValidateStyles(this ProjectDescriptionModel project, SetStylesModel styles)
        {
            Assert.True(project != null, "Unable to get project by its Id");
            Assert.True(project.Styles != null, "project Styles is null");
            Assert.True(project.Styles.Length == styles.Styles.Length, "project Styles is wrong Length");
            foreach (var style in project.Styles)
            {
                var sameStyle = styles.Styles.FirstOrDefault(x => x.Name == style.Name && x.Description == style.Description);
                if (sameStyle == null)
                {
                    Assert.True(false, "style is wrong");
                    break;
                }
            }
        }

        public static void ValidateBudget(this ProjectDescriptionModel project, SetBudgetModel budget)
        {
            Assert.True(project.Budget != null, "Budget not set");
            Assert.True(project.Budget.FurnitureBudget == budget.Budget.FurnitureBudget, "Budget is wrong");
            Assert.True(project.Budget.RenovationBudget == budget.Budget.RenovationBudget, "Budget is wrong");
        }

        public static void ValidateDesignerRequirements(this ProjectDescriptionModel project, DesignerRequirementsModel req)
        {
            Assert.True(project.DesignerRequirements != null, "Designer requirements not set");
            Assert.True(project.DesignerRequirements.AgeFrom == req.AgeFrom, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.AgeTo == req.AgeTo, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.Gender == req.Gender, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.MeetingAdress == req.MeetingAdress, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.NeedPersonalMeeting == req.NeedPersonalMeeting, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.NeedPersonalControl == req.NeedPersonalControl, "Designer requirements is wrong");
            Assert.True(project.DesignerRequirements.Testing != null && project.DesignerRequirements.Testing.Questions != null, "Designer testing not set");
            foreach (var answer in project.DesignerRequirements.Testing.Questions)
            {
                var same = req.Testing.Questions.FirstOrDefault(x => x.Index == answer.Index && x.Answer == answer.Answer);
                if (same == null)
                {
                    Assert.True(false, "question is wrong");
                    break;
                }
            }
        }
    }
}
