﻿using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Integration.Extensions
{
    public static class ClientDesignerModelsExtension
    {
        public static bool IsEqualTo(this List<ShortRoomInfoModel> source, List<ShortRoomInfoModel> other)
        {
            if (source == null)
                return other == null;
            if (other == null)
                return source == null;
            if (source.Count != other.Count)
                return false;
            foreach (var s in source)
            {
                if (!other.Any(x => x.IsEqualTo(s)))
                    return false;
            }
            return true;
        }

        public static bool IsEqualTo(this ShortRoomInfoModel source, ShortRoomInfoModel other)
        {
            if (source == null)
                return other == null;
            if (other == null)
                return source == null;
            if (source.RoomId != other.RoomId)
                return false;
            var p1 = source.Proportions;
            var p2 = other.Proportions;
            return source.Proportions.IsEqualTo(other.Proportions);
        }

        public static bool IsEqualTo(this RoomProportionsModel source, RoomProportionsModel other)
        {
            if (source == null)
                return other == null;
            if (other == null)
                return source == null;
            return source.HeightCeiling == other.HeightCeiling && source.HeightDoor == other.HeightDoor &&
                source.SideA == other.SideA && source.SideB == other.SideB && source.WidthDoor == other.WidthDoor;
        }
    }
}
