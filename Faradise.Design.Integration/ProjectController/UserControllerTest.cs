﻿using Faradise.Design.Controllers.API.Models.User;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.ProjectController
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class UserControllerTest
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static string _token = string.Empty;
        private static string _email = "fara@mail.com";
        private static string _name = "hasarden";
        private static string _phone = "+79031413072";
        
        [Fact, TestPriority(0)]
        public async Task SetEmail()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");

            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");

            _token = token;
            var emailModel = new EmailModel() { Email = _email };

            url = TestConfig.BaseUrl + "/api/user/setEmail";
            response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(emailModel));
            ServerHttpClient.HandleResponse(response, "set email");
        }

        [Fact, TestPriority(2)]
        public async Task SetName()
        {
            var nameModel = new NameModel { Name = _name };
            var url = TestConfig.BaseUrl + "/api/user/setName";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(nameModel));
            ServerHttpClient.HandleResponse(response, "set name");
        }

        [Fact, TestPriority(3)]
        public async Task SetPhone()
        {
            var model = new PhoneModel { Phone = _phone };
            var url = TestConfig.BaseUrl + "/api/user/setPhone";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse(response, "set phone");
        }

        [Fact, TestPriority(4)]
        public async Task Authorize()
        {
            var url = TestConfig.BaseUrl + "/api/user/authorize";
            var response = await _client.MakePostJsonRequestAsync(_token, url, null);
            var model = ServerHttpClient.HandleResponse<AuthorizeModel>(response, "authorize");

            Assert.Equal(model.Email, _email);
            Assert.Equal(model.Name, _name);
            Assert.Equal(model.Phone, _phone);
        }
    }
}
