﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.ProjectController
{
    [Collection("Project Stages")]
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class ProjectControllerTests
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static int _projectId = 0;
        private static string _token = string.Empty;
        private static readonly Random _random = new Random(System.DateTime.Now.Millisecond);

        private static SetBasicModel _nameAndCity = new SetBasicModel() { ProjectName = "Сарай", City = "Нюкатаун" };

        private static SetInfoModel _info = new SetInfoModel()
        {
            ObjectType = ProjectTargetType.Cafe,
            Rooms = new RoomsModel[] {
                new RoomsModel() { Purpose = Room.ChildrenRoom, Count = 1},
                new RoomsModel() { Purpose = Room.LivingRoom, Count = 2}
            },
            Reasons = new ReasonModel[] {
                new ReasonModel() { Name = ProjectReason.NewHouse, Description = "Переехфл в сарай"}
            }
        };

        private static SetStylesModel _styles = new SetStylesModel()
        {
            Styles = new StyleModel[] {
                new StyleModel() { Name = ProjectStyle.Industrial, Description = "так модно"},
                new StyleModel() { Name = ProjectStyle.Minimalism, Description = "так дешево"}
            }
        };

        private static SetBudgetModel _budget = new SetBudgetModel()
        {
            Budget = new BudgetModel()
            {
                FurnitureBudget = 300,
                RenovationBudget = 100500
            }
        };

        private static DesignerRequirementsModel _designerReq = new DesignerRequirementsModel()
        {
            AgeFrom = 10,
            AgeTo = 30,
            Gender = Gender.Female,
            MeetingAdress = "На Земле",
            NeedPersonalMeeting = true,
            NeedPersonalControl = false,
        };

        private static TestResultsModel _designerTest = new TestResultsModel();

        [Fact, TestPriority(0)]
        public async Task CreateClientToken()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");

            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");

            _token = token;
        }


        [Fact, TestPriority(1)]
        public async Task CreateProject()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/create";
            var response = await _client.MakePostJsonRequestAsync(_token, url, null);
            var projectId = ServerHttpClient.HandleResponse<int?>(response, " Create project");
            Assert.True(projectId != null, $"project id is empty, route: {url}");
            _projectId = projectId.Value;
        }

        private async Task GetProject(string step)
        {
            var url = TestConfig.BaseUrl + "/api/client/project/get";
            var response = await _client.MakeGetRequestAsync(_token, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var project = ServerHttpClient.HandleResponse<ProjectDescriptionModel>(response, step);
            Assert.True(project != null, $"Project not recieved by id, route: {url}");
        }

        [Fact, TestPriority(2)]
        public async Task GetProjectAfterCreation()
        {
            await GetProject("Get project after creation");
        }

        [Fact, TestPriority(3)]
        public async Task GetAllProjects()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/list";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var project = ServerHttpClient.HandleResponse<ProjectListModel>(response, "Get all projects by user");
            Assert.True(project != null && project.Projects != null && project.Projects.Length > 0, $"User dont has projects after creation, route: {url}");
        }

        [Fact, TestPriority(4)]
        public async Task SetBasicInfo()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/setBasicInfo";
            var basicInfo = _nameAndCity;
            basicInfo.Id = _projectId;
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(basicInfo));
            ServerHttpClient.HandleResponse(response, "Set basic Info");
        }

        [Fact, TestPriority(5)]
        public async Task SetAdditionalInfo()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/setAdditionalInfo";
            var info = _info;
            info.Id = _projectId;
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(info));
            ServerHttpClient.HandleResponse(response, "Set additional info");
        }

        [Fact, TestPriority(6)]
        public async Task SetStyles()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/setStyles";
            var styles = _styles;
            styles.Id = _projectId;
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(styles));
            ServerHttpClient.HandleResponse(response, "Set styles");
        }

        [Fact, TestPriority(7)]
        public async Task SetBudget()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/setBudget";
            var budget = _budget;
            budget.Id = _projectId;
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(budget));
            ServerHttpClient.HandleResponse(response, "Set budget");
        }

        [Fact, TestPriority(8)]
        public async Task GetClientDesignTest()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/getQuestions";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var test = ServerHttpClient.HandleResponse<TestQuestionDescriptionModel[]>(response, "Get designer test");
            Assert.True(test != null && test.Length > 0, "Designer test is empty");
            var answerList = new List<TestResultsAnswerModel>();
            foreach (var t in test)
                answerList.Add(new TestResultsAnswerModel() { Index = t.Id, Answer = t.Answers[_random.Next(0, t.Answers.Length)].Id });
            _designerTest = new TestResultsModel() {  Questions = answerList.ToArray() };
        }

        [Fact, TestPriority(9)]
        public async Task SetDesignerReuirements()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/setDesignerRequirements";
            var req = _designerReq;
            req.Id = _projectId;
            req.Testing = _designerTest;
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(req));
            ServerHttpClient.HandleResponse(response, "Set designer requirements");
        }

        [Fact, TestPriority(10)]
        public async Task SetProjectFilled()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/finish-filling";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(new FinishModel() { ProjectId = _projectId }));
            ServerHttpClient.HandleResponse(response, "Set Project Filled");
            url = TestConfig.BaseUrl + "/api/client/project/get";
            response = await _client.MakeGetRequestAsync(_token, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var project = ServerHttpClient.HandleResponse<ProjectDescriptionModel>(response, "Get project after fillment check");
            Assert.True(project != null, $"Project not recieved by id, route: {url}");
            Assert.True(project.FinishFill == true, "Project does not check fill finish");
        }

        [Fact, TestPriority(11)]
        public async Task CheckProjectAfterRegistration()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/get";
            var response = await _client.MakeGetRequestAsync(_token, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var project = ServerHttpClient.HandleResponse<ProjectDescriptionModel>(response, "Get project after register complete");
            Assert.True(project != null, $"Project not recieved by id, route: {url}");
            project.ValidateNameAndCity(_nameAndCity);
            project.ValidateInfo(_info);
            project.ValidateStyles(_styles);
            project.ValidateBudget(_budget);
            project.ValidateDesignerRequirements(_designerReq);
        }
    }
}
