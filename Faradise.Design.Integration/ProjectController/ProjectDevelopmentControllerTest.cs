﻿using Faradise.Design.Controllers.API.Models;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Controllers.API.Models.User;
using Faradise.Design.Integration.Extensions;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDevelopment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.ProjectController
{
    [Collection("Project Stages")]
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class ProjectDevelopmentControllerTest
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static int _projectId = 0;
        private static string _clientToken = string.Empty;
        private static string _designerToken = string.Empty;

        private static int _productId = -1;
        private static int _orderRoomId = -1;
        private static int _productPrice = 0;
        private static string _designerName = string.Empty;
        private static int _designerId = -1;
        private static RoomPurposeModel[] _rooms = null;

        [Fact, TestPriority(0)]
        public async Task PrepareStagesBeforeThis()
        {
            var prepTest = new ProjectPreparationsControllerTest();
            try
            {
                await prepTest.CreateClientToken();
                await prepTest.CreateDesignerToken();
                await prepTest.CreateProject();
                await prepTest.AddDesignerToPoolByNastya();
                await prepTest.UserSelectDesigner();
                await prepTest.UserConfirmDesigner();
                await prepTest.DesignerConfirmUser();
                await prepTest.UserSetProportions();
                await prepTest.MoveToStageStyles();
                await prepTest.UserSetStyles();
                await prepTest.MoveToStageBudget();
                await prepTest.UserSetBudget();
                await prepTest.MoveToStageAdditionalInfo();
                await prepTest.SetAdditionalInfo();
                await prepTest.MoveToStageWhatToSave();
                await prepTest.EndProjectPreparations();
                _clientToken = ProjectPreparationsControllerTest.ClientToken;
                _designerToken = ProjectPreparationsControllerTest.DesignerToken;
                _projectId = ProjectPreparationsControllerTest.ProjectId;

                var url = TestConfig.BaseUrl + "/api/project/development/get-rooms";
                var roomRequest = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
                _rooms = ServerHttpClient.HandleResponse<RoomPurposeModel[]>(roomRequest, "Get rooms");
            } catch
            {
                Assert.True(false, "Some problems with preparation phases");
            }
        }

        [Fact, TestPriority(1)]
        public async Task CurrentStageIsMoodboard()
        {
            await CheckDevelopmentStage(DevelopmentStage.Moodboard, "CurrentStageIsMoodboard");
        }

        [Fact, TestPriority(2)]
        public async Task DesginerFinishMoodboard()
        {
            await AddPhotosToRooms(DevelopmentStage.Moodboard);
            await DesignerConfirmDevelopmentStage(DevelopmentStage.Moodboard);
        }

        [Fact, TestPriority(3)]
        public async Task ClientConfirmMoodboard()
        {
            await ClientConfirmDevelopmentStage(DevelopmentStage.Moodboard);
        }

        [Fact, TestPriority(4)]
        public async Task CurrentStageIsZoning()
        {
            await CheckDevelopmentStage(DevelopmentStage.Zoning, "CurrentStageIsZoning");
        }

        [Fact, TestPriority(5)]
        public async Task DesginerFinishZoning()
        {
            await AddPhotosToRooms(DevelopmentStage.Zoning);
            await DesignerConfirmDevelopmentStage(DevelopmentStage.Zoning);
        }

        [Fact, TestPriority(6)]
        public async Task ClientConfirmZoning()
        {
            await ClientConfirmDevelopmentStage(DevelopmentStage.Zoning);
        }

        [Fact, TestPriority(7)]
        public async Task CurrentStageIsCollage()
        {
            await CheckDevelopmentStage(DevelopmentStage.Collage, "CurrentStageIsCollage");
        }

        [Fact, TestPriority(8)]
        public async Task DesginerFinishCollage()
        {
            await AddPhotosToRooms(DevelopmentStage.Collage);
            await DesignerConfirmDevelopmentStage(DevelopmentStage.Collage);
        }

        [Fact, TestPriority(9)]
        public async Task ClientConfirmCollage()
        {
            await ClientConfirmDevelopmentStage(DevelopmentStage.Collage);
        }


        [Fact, TestPriority(100)]
        public async Task DesignerOpenMarketplace()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-categories";
            var response = await _client.MakeGetRequestAsync(_designerToken, url, null);
            var model = ServerHttpClient.HandleResponse<CategoryModel[]>(response, "Designer get categories");
            Assert.True(model != null && model.Length > 0, "Categories ncount is 0");

            url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            response = await _client.MakeGetRequestAsync(_designerToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" },
                    { "Category", model[0].Id.ToString() } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");

            url = TestConfig.BaseUrl + "/api/marketplace/get-product";
            response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "productId", products.Products[0].Id.ToString() } });
            var productModel = ServerHttpClient.HandleResponse<ProductModel>(response, "Designer get single product");
            Assert.True(productModel != null, "product model is null");
            _productId = productModel.Id;
            _productPrice = productModel.Price;
        }

        [Fact, TestPriority(101)]
        public async Task AddMarketplaceProductToProject()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-possible-projects";
            var response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "productId", _productId.ToString() } });
            var model = ServerHttpClient.HandleResponse<List<ProjectSelectionModel>>(response, "Designer get possible marketplace projects");
            Assert.True(model != null && model.Count > 0, "No projects in model");
            var project = model.FirstOrDefault(x => x.ProjectId == _projectId);
            Assert.True(project != null, "project not in markeplace list");
            _orderRoomId = project.Rooms[0].RoomId;

            url = TestConfig.BaseUrl + "/api/marketplace/add-product-to-project";
            var addModel = new AddProductToProjectModel() { RoomId = _orderRoomId, Count = 2, ProductId = _productId, ProjectId = _projectId };
            response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(addModel));
            ServerHttpClient.HandleResponse(response, "add product to project");

            url = TestConfig.BaseUrl + "/api/marketplace/get-products-for-project-room";
            var getModel = new GetOrderedProductsForRoomModel() { ProjectId = _projectId, RoomId = _orderRoomId };
            response = await _client.MakeGetObjRequestAsync(_designerToken, url, getModel);
            var orderedList = ServerHttpClient.HandleResponse<List<OrderedProductModel>>(response, "Get ordered products");
            Assert.True(orderedList != null && orderedList.Count > 0, "order list is null");
            Assert.True(orderedList.Any(x => x.ShortProductModel.Id == _productId && x.Count == 2 && !x.PaidFor), "orderd item not in list or item count is invalid");
        }

        [Fact, TestPriority(102)]
        public async Task RemoveOrderedItemFromRoom()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/remove-product-from-project";
            var removeModel = new RemoveProductFromProjectModel() { RoomId = _orderRoomId, Count = 1, ProductId = _productId, ProjectId = _projectId };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(removeModel));
            ServerHttpClient.HandleResponse(response, "remove product to project");

            url = TestConfig.BaseUrl + "/api/marketplace/get-products-for-project-room";
            var getModel = new GetOrderedProductsForRoomModel() { ProjectId = _projectId, RoomId = _orderRoomId };
            response = await _client.MakeGetObjRequestAsync(_clientToken, url, getModel);
            var orderedList = ServerHttpClient.HandleResponse<List<OrderedProductModel>>(response, "Get ordered products");
            Assert.True(orderedList != null && orderedList.Count > 0, "order list is null");
            Assert.True(orderedList.Any(x => x.ShortProductModel.Id == _productId && x.Count == 1 && !x.PaidFor), "orderd item not in list or item count is invalid");
        }

        [Fact, TestPriority(103)]
        public async Task ClientCreateOrder()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/issue-order-for-project";
            var issueOrderModel = new IssueOrderModel() { ProjectId = _projectId, Products = new ProductOrderForProjectModel[] { new ProductOrderForProjectModel() { Count = 1, ProductId = _productId, RoomId = _orderRoomId } } };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(issueOrderModel));
            ServerHttpClient.HandleResponse(response, "issue order response");

            url = TestConfig.BaseUrl + "/api/marketplace/get-products-for-project-room";
            var getModel = new GetOrderedProductsForRoomModel() { ProjectId = _projectId, RoomId = _orderRoomId };
            response = await _client.MakeGetObjRequestAsync(_clientToken, url, getModel);
            var orderedList = ServerHttpClient.HandleResponse<List<OrderedProductModel>>(response, "Get ordered products");
            Assert.True(orderedList != null && orderedList.Count > 0, "order list is null");
            Assert.True(orderedList.Any(x => x.ShortProductModel.Id == _productId && x.Count == 1 && x.PaidFor), "orderd item not in list or item count is invalid or item paid status is invalid"); 
        }

        [Fact, TestPriority(104)]
        public async Task AdminCreatePayment()
        {
            var adminClient = new ServerHttpClient();

            var url = TestConfig.BaseUrl + "/api/admin/get-orders-for-project";
            var response = await adminClient.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var ordersModel = ServerHttpClient.HandleResponse<ShortProductOrderModel[]>(response, "admin get projects orders");
            Assert.True(ordersModel != null && ordersModel.Length == 1, "order list size is invalid");

            url = TestConfig.BaseUrl + "/api/admin/get-orders-information";
            response = await adminClient.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "orderId", ordersModel[0].OrderId.ToString() } });
            var orderInfoModel = ServerHttpClient.HandleResponse<FullProductProjectOrderModel>(response, "admin get project order inforamtion");
            Assert.True(orderInfoModel != null, "Order information is null");
            Assert.True(orderInfoModel.BasePrice == _productPrice, "order price is invalid");
            Assert.True(orderInfoModel.Items.Length == 1 && orderInfoModel.Items[0].ProductId == _productId && orderInfoModel.Items[0].Count == 1, "order produc list is invalid");

            //url = TestConfig.BaseUrl + "/api/admin/create-payment-for-project-order";
            //var jsonCreatePayment = JsonConvert.SerializeObject(new CreatePaymentForProjectModel() { OrderId = orderInfoModel.OrderId });
            //response = await adminClient.MakePostJsonRequestAsync(_clientToken, url, jsonCreatePayment);
            //var paymentRoute = ServerHttpClient.HandleResponse<string>(response, "admin create payment");
            //Assert.True(!string.IsNullOrEmpty(paymentRoute), "payment route is empty");
        }

        [Fact, TestPriority(213)]
        public async Task CurrentStageIs3DVisuals()
        {
            await CheckDevelopmentStage(DevelopmentStage.Visualization, "CurrentStageIs3DVisuals");
        }

        [Fact, TestPriority(214)]
        public async Task UserSkipVisualization()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Visualization);
        }

        [Fact, TestPriority(215)]
        public async Task CurrentStageIsPlans()
        {
            await CheckDevelopmentStage(DevelopmentStage.Plan, "CurrentStageIsPlans");
        }

        [Fact, TestPriority(216)]
        public async Task UserSkipPlans()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Plan);
        }

        [Fact, TestPriority(217)]
        public async Task CurrentStageIsBrigade()
        {
            await CheckDevelopmentStage(DevelopmentStage.Brigade, "CurrentStageIsBrigade");
        }

        [Fact, TestPriority(218)]
        public async Task UserSkipBrigade()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Brigade);
        }

        [Fact, TestPriority(219)]
        public async Task CurrentStageIsCurator()
        {
            await CheckDevelopmentStage(DevelopmentStage.Curation, "CurrentStageIsCurator");
        }

        [Fact, TestPriority(220)]
        public async Task UserSkipCurator()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Curation);
        }

        [Fact, TestPriority(221)]
        public async Task CurrentStageIsFinish()
        {
            await CheckDevelopmentStage(DevelopmentStage.WaitForFinish, "CurrentStageIsFinish");
        }

        private async Task DesignerConfirmDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/confirm-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage confirm {current}");
        }

        private async Task ClientConfirmDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/confirm-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage confirm {current}");
        }

        private async Task ClientSkipDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/skip-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage skip {current}");
        }

        private async Task CheckDevelopmentStage(DevelopmentStage validStage, string step)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/get-current-stage";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var stageModel = ServerHttpClient.HandleResponse<ProjectStageModel>(response, step);
            Assert.True(stageModel != null, $"stage is not valid on step {step} : stage model is empty");
            Assert.True(stageModel.Stage == validStage, $"stage is not valid on step {step} : {validStage} != {stageModel.Stage}");
        }

        private async Task AddPhotosToRooms(DevelopmentStage developmentStage)
        {
            foreach (var room in _rooms)
            {
                byte[] bytes = File.ReadAllBytes("Data/1.png");
                Assert.True(bytes.Length != 0, $"file not loaded");

                var content = new ByteArrayContent(bytes);
                var multipart = new MultipartFormDataContent();
                multipart.Add(content, "file", "1.png");
                multipart.Add(new StringContent(room.RoomId.ToString()), "RoomId");
                multipart.Add(new StringContent(_projectId.ToString()), "ProjectId");

                string url = string.Empty;
                switch (developmentStage)
                {
                    case DevelopmentStage.Moodboard:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-moodboard-photo";
                        break;
                    case DevelopmentStage.Zoning:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-zone-photo";
                        break;
                    case DevelopmentStage.Collage:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-collage-photo";
                        break;
                }
                var response = await _client.MakePostMultipartFormRequestAsync(_designerToken, url, multipart);

                Assert.True((int)response.StatusCode == 200, $"Status code not OK: {(int)response.StatusCode}");
            }
        }
    }
}
