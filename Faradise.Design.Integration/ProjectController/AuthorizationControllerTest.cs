﻿using Faradise.Design.Controllers.API.Models.Authentication;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.ProjectController
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class AuthorizationControllerTest
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static string _phone = GenerateRandomPhoneNubmer();
        private static string _code = "0000";
        private static string _token = string.Empty;

        private static string GenerateRandomPhoneNubmer()
        {
            var random = new System.Random(DateTime.Now.Millisecond);
            var number = random.Next(1000000, 9999999);
            return "7916" + number.ToString();
        }

        [Fact, TestPriority(0)]
        public async Task CreateClient()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _token = token;
        }

        [Fact, TestPriority(1)]
        public async Task CreateDesigner()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createDesigner";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create designer");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
        }

        [Fact, TestPriority(2)]
        public async Task ValidateToken()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/validateToken";
            var validateModel = new ValidateModel() { Token = _token };
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(validateModel));
            var isValid = ServerHttpClient.HandleResponse<bool>(response, "Validate token");
            Assert.True(isValid, $"recieved token is invalid, route: {url}");
        }

        [Fact, TestPriority(3)]
        public async Task RequestCode()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/requestCode";
            var requestCodeModel = new RequestCodeModel() { Phone = _phone };
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(requestCodeModel));
            ServerHttpClient.HandleResponse(response, "Requiest code");
        }

        [Fact, TestPriority(4)]
        public async Task BindPhoneByCode()
        {
            await RequestCode();

            var url = TestConfig.BaseUrl + "/api/authentication/bind";
            var bindModel = new BindModel() { Phone = _phone, Code = _code };
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(bindModel));
            ServerHttpClient.HandleResponse(response, "Bind phone by code");
        }

        [Fact, TestPriority(5)]
        public async Task Login()
        {
            await RequestCode();

            var url = TestConfig.BaseUrl + "/api/authentication/login";
            var loginModel = new LoginModel() { Phone = _phone, Code = _code };
            var response = await _client.MakePostJsonRequestAsync(null, url, JsonConvert.SerializeObject(loginModel));
            ServerHttpClient.HandleResponse(response, "Login");
            var model = ServerHttpClient.HandleResponse<LoggedInModel>(response, "login user");
            Assert.True(!string.IsNullOrEmpty(model.Token), $"recieved login token is empty, route: {url}");
        }
    }
}