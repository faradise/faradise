﻿using Faradise.Design.Controllers.API.Models;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Integration.Extensions;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDevelopment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.ProjectController
{
    [Collection("Project Stages")]
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class ProjectDevelopmentControllerTest2
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static int _projectId = 0;
        private static string _clientToken = string.Empty;
        private static string _designerToken = string.Empty;

        private static ProjectDescriptionModel _projectModel = null;
        private static List<RoomInfoModel> _rooms = null;
        private static List<RoomPurposeModel> _roomsPurpose = null;
        private static string _designerName = string.Empty;
        private static int _designerId = -1;

        private static List<MoodboardModel> _moodboardModel = new List<MoodboardModel>();
        private static List<ZoneModel> _zoneModel = new List<ZoneModel>();
        private static List<CollageModel> _collageModel = new List<CollageModel>();

        [Fact, TestPriority(0)]
        public async Task PrepareStagesBeforeThis()
        {
            var prepTest = new ProjectPreparationsControllerTest();
            try
            {
                await prepTest.CreateClientToken();
                await prepTest.CreateDesignerToken();
                await prepTest.CreateProject();
                await prepTest.AddDesignerToPoolByNastya();
                await prepTest.UserSelectDesigner();
                await prepTest.UserConfirmDesigner();
                await prepTest.DesignerConfirmUser();
                await prepTest.UserSetProportions();
                await prepTest.MoveToStageStyles();
                await prepTest.UserSetStyles();
                await prepTest.MoveToStageBudget();
                await prepTest.UserSetBudget();
                await prepTest.MoveToStageAdditionalInfo();
                await prepTest.SetAdditionalInfo();
                await prepTest.MoveToStageWhatToSave();
                await prepTest.EndProjectPreparations();
                _clientToken = ProjectPreparationsControllerTest.ClientToken;
                _designerToken = ProjectPreparationsControllerTest.DesignerToken;
                _projectId = ProjectPreparationsControllerTest.ProjectId;
                //_rooms = ProjectPreparationsControllerTest.Rooms;
            } catch
            {
                Assert.True(false, "Some problems with preparation phases");
            }
        }

        [Fact, TestPriority(1)]
        public async Task CurrentStageIsMoodboard()
        {
            await CheckDevelopmentStage(DevelopmentStage.Moodboard, "CurrentStageIsMoodboard");
        }

        public async Task GetRooms()
        {
            var url = TestConfig.BaseUrl + "/api/project/development/get-rooms";
            var response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var rooms = ServerHttpClient.HandleResponse<RoomPurposeModel[]>(response, "Get rooms");
            Assert.True(rooms.Length != 0, "Rooms length is null");
            _roomsPurpose = rooms.ToList();
        }

        [Fact, TestPriority(2)]
        public async Task DesginerFinishMoodboard()
        {
            await GetRooms();
            await AddPhotosToRooms(DevelopmentStage.Moodboard);

            await DesignerConfirmDevelopmentStage(DevelopmentStage.Moodboard);
        }

        [Fact, TestPriority(3)]
        public async Task ClientConfirmMoodboard()
        {
            await CheckPhotos(DevelopmentStage.Moodboard);
            await DeletePhotos(DevelopmentStage.Moodboard);

            await ClientConfirmDevelopmentStage(DevelopmentStage.Moodboard);
        }

        [Fact, TestPriority(4)]
        public async Task CurrentStageIsZoning()
        {
            await CheckDevelopmentStage(DevelopmentStage.Zoning, "CurrentStageIsZoning");
        }

        [Fact, TestPriority(5)]
        public async Task DesginerFinishZoning()
        {
            await AddPhotosToRooms(DevelopmentStage.Zoning);

            await DesignerConfirmDevelopmentStage(DevelopmentStage.Zoning);
        }

        [Fact, TestPriority(6)]
        public async Task ClientConfirmZoning()
        {
            await CheckPhotos(DevelopmentStage.Zoning);
            await DeletePhotos(DevelopmentStage.Zoning);

            await ClientConfirmDevelopmentStage(DevelopmentStage.Zoning);
        }

        [Fact, TestPriority(7)]
        public async Task CurrentStageIsCollage()
        {
            await CheckDevelopmentStage(DevelopmentStage.Collage, "CurrentStageIsCollage");
        }

        [Fact, TestPriority(8)]
        public async Task DesginerFinishCollage()
        {
            await AddPhotosToRooms(DevelopmentStage.Collage);

            await DesignerConfirmDevelopmentStage(DevelopmentStage.Collage);
        }

        [Fact, TestPriority(9)]
        public async Task ClientConfirmCollage()
        {
            await CheckPhotos(DevelopmentStage.Collage);
            await DeletePhotos(DevelopmentStage.Collage);

            await ClientConfirmDevelopmentStage(DevelopmentStage.Collage);
        }

        [Fact, TestPriority(10)]
        public async Task CurrentStageIs3DVisuals()
        {
            await CheckDevelopmentStage(DevelopmentStage.Visualization, "CurrentStageIs3DVisuals");
        }

        [Fact, TestPriority(11)]
        public async Task UserSkipVisualization()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Visualization);
        }

        [Fact, TestPriority(12)]
        public async Task CurrentStageIsPlans()
        {
            await CheckDevelopmentStage(DevelopmentStage.Plan, "CurrentStageIsPlans");
        }

        [Fact, TestPriority(13)]
        public async Task UserSkipPlans()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Plan);
        }

        [Fact, TestPriority(14)]
        public async Task CurrentStageIsBrigade()
        {
            await CheckDevelopmentStage(DevelopmentStage.Brigade, "CurrentStageIsBrigade");
        }

        [Fact, TestPriority(15)]
        public async Task UserSkipBrigade()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Brigade);
        }

        [Fact, TestPriority(16)]
        public async Task CurrentStageIsCurator()
        {
            await CheckDevelopmentStage(DevelopmentStage.Curation, "CurrentStageIsCurator");
        }

        [Fact, TestPriority(17)]
        public async Task UserSkipCurator()
        {
            await ClientSkipDevelopmentStage(DevelopmentStage.Curation);
        }

        [Fact, TestPriority(18)]
        public async Task CurrentStageIsFinish()
        {
            await CheckDevelopmentStage(DevelopmentStage.WaitForFinish, "CurrentStageIsFinish");
        }

        private async Task DesignerConfirmDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/confirm-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage confirm {current}");
        }

        private async Task ClientConfirmDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/confirm-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage confirm {current}");
        }

        private async Task ClientSkipDevelopmentStage(DevelopmentStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/skip-stage";
            var model = new ConfirmedDevelopStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));
            ServerHttpClient.HandleResponse<bool>(response, $"stage skip {current}");
        }

        private async Task CheckDevelopmentStage(DevelopmentStage validStage, string step)
        {
            var url = TestConfig.BaseUrl + "/api/project/development/get-current-stage";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var stageModel = ServerHttpClient.HandleResponse<ProjectStageModel>(response, step);
            Assert.True(stageModel != null, $"stage is not valid on step {step} : stage model is empty");
            Assert.True(stageModel.Stage == validStage, $"stage is not valid on step {step} : {validStage} != {stageModel.Stage}");
        }

        public async Task AddPhotosToRooms(DevelopmentStage developmentStage)
        {
            foreach (var room in _roomsPurpose)
            {
                byte[] bytes = File.ReadAllBytes("Data/1.png");
                Assert.True(bytes.Length != 0, $"file not loaded");

                var content = new ByteArrayContent(bytes);
                var multipart = new MultipartFormDataContent();
                multipart.Add(content, "file", "1.png");
                multipart.Add(new StringContent(room.RoomId.ToString()), "RoomId");
                multipart.Add(new StringContent(_projectId.ToString()), "ProjectId");

                string url = string.Empty;
                switch (developmentStage)
                {
                    case DevelopmentStage.Moodboard:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-moodboard-photo";
                        break;
                    case DevelopmentStage.Zoning:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-zone-photo";
                        break;
                    case DevelopmentStage.Collage:
                        url = TestConfig.BaseUrl + "/api/project/development/upload-collage-photo";
                        break;
                }
                var response = await _client.MakePostMultipartFormRequestAsync(_designerToken, url, multipart);

                Assert.True((int)response.StatusCode == 200, $"Status code not OK: {(int)response.StatusCode}");
            }
        }

        public async Task CheckPhotos(DevelopmentStage developmentStage)
        {
            foreach (var room in _roomsPurpose)
            {
                string url = string.Empty;
                switch (developmentStage)
                {
                    case DevelopmentStage.Moodboard:
                        url = TestConfig.BaseUrl + "/api/project/development/get-moodboards";
                        var response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() }, { "roomId", room.RoomId.ToString() } });
                        var photos = ServerHttpClient.HandleResponse<MoodboardModel>(response, $"Get {developmentStage} {room.RoomId.ToString()}");
                        Assert.True(photos.MoodboardPhotos.Length != 0, $"{developmentStage} photos length is null: roomId {photos.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {photos.MoodboardPhotos.Length}");
                        _moodboardModel.Add(photos);
                        break;
                    case DevelopmentStage.Zoning:
                        url = TestConfig.BaseUrl + "/api/project/development/get-zones";
                        var responseZone = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() }, { "roomId", room.RoomId.ToString() } });
                        var photosZone = ServerHttpClient.HandleResponse<ZoneModel>(responseZone, $"Get {developmentStage} {room.RoomId.ToString()}");
                        Assert.True(photosZone.ZonePhotos.Length != 0, $"{developmentStage} photos length is null: roomId {photosZone.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {photosZone.ZonePhotos.Length}");
                        _zoneModel.Add(photosZone);
                        break;
                    case DevelopmentStage.Collage:
                        url = TestConfig.BaseUrl + "/api/project/development/get-collages";
                        var responseCollage = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() }, { "roomId", room.RoomId.ToString() } });
                        var photosCollage = ServerHttpClient.HandleResponse<CollageModel>(responseCollage, $"Get {developmentStage} {room.RoomId.ToString()}");
                        Assert.True(photosCollage.CollagePhotos.Length != 0, $"{developmentStage} photos length is null: roomId {photosCollage.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {photosCollage.CollagePhotos.Length}");
                        _collageModel.Add(photosCollage);
                        break;
                }
            }
        }

        public async Task DeletePhotos(DevelopmentStage developmentStage)
        {
            string url = string.Empty;
            switch (developmentStage)
            {
                case DevelopmentStage.Moodboard:
                    url = TestConfig.BaseUrl + "/api/project/development/delete-moodboard-photo";
                    foreach (var moodboard in _moodboardModel)
                    {
                        DeleteFileModel model = new DeleteFileModel() { ProjectId = _projectId, RoomId = moodboard.RoomId, FileId = moodboard.MoodboardPhotos[0].FileId };
                        var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
                        Assert.True((int)response.StatusCode == 200, $"{developmentStage} photos length is null: roomId {moodboard.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {moodboard.MoodboardPhotos.Length}");
                    }
                        break;
                case DevelopmentStage.Zoning:
                    url = TestConfig.BaseUrl + "/api/project/development/delete-zone-photo";
                    foreach (var zone in _zoneModel)
                    {
                        DeleteFileModel model = new DeleteFileModel() { ProjectId = _projectId, RoomId = zone.RoomId, FileId = zone.ZonePhotos[0].FileId };
                        var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
                        Assert.True((int)response.StatusCode == 200, $"{developmentStage} photos length is null: roomId {zone.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {zone.ZonePhotos.Length}");
                    }
                    break;
                case DevelopmentStage.Collage:
                    url = TestConfig.BaseUrl + "/api/project/development/delete-collage-photo";
                    foreach (var collage in _collageModel)
                    {
                        DeleteFileModel model = new DeleteFileModel() { ProjectId = _projectId, RoomId = collage.RoomId, FileId = collage.CollagePhotos[0].FileId };
                        var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
                        Assert.True((int)response.StatusCode == 200, $"{developmentStage} photos length is null: roomId {collage.RoomId}, projectId {_projectId}, designerToken {_designerToken}, {collage.CollagePhotos.Length}");
                    }
                    break;
            }
        }
    }
}
