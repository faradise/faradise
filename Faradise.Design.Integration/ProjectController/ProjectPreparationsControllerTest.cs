﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Integration.Extensions;
using System.Net.Http;
using System.Net;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.User;

namespace Faradise.Design.Integration.ProjectController
{
    [Collection("Project Stages")]
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class ProjectPreparationsControllerTest
    {
        public static string ClientToken { get { return _clientToken; } }
        public static string DesignerToken { get { return _designerToken; } }
        public static int ProjectId { get { return _projectId; } }

        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static int _projectId = 0;
        private static string _clientToken = string.Empty;
        private static string _designerToken = string.Empty;

        private static ProjectDescriptionModel _projectModel = null;
        private static List<RoomInfoModel> _rooms = null;
        private static string _designerName = string.Empty;
        private static int _designerId = -1;


        [Fact, TestPriority(0)]
        public async Task CreateClientToken()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _clientToken = token;
        }

        [Fact, TestPriority(1)]
        public async Task CreateDesignerToken()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createDesigner";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create designer");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _designerToken = token;
            url = TestConfig.BaseUrl + "/api/designer/setBasicInfo";
            _designerName = "designer_" + System.Guid.NewGuid().ToString();
            var designerInfoModel = new SetDesignerBasicInfoModel()
            {
                Basic = new DesignerBaseModel()
                {
                    Name = _designerName,
                    About = "hi",
                    Age = 12,
                    City = "Mi",
                    Email = "Mo@mo.net",
                    Gender = Gender.Female,
                    Surname = "mi",
                    WorkExperience = Models.Enums.WorkExperience.MoreThanFiveYears
                },
                PortfolioLink = "http://sd/dsd/",
                Styles = new DesignerStyleModel[] { new DesignerStyleModel() { Description = "ssdsd", Name = ProjectStyle.Eclecticism } },

            };
            response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(designerInfoModel));
            ServerHttpClient.HandleResponse(response, "Set designer info");
        }

        [Fact, TestPriority(2)]
        public async Task CreateProject()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/create";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, null);
            var projectId = ServerHttpClient.HandleResponse<int?>(response, " Create project");
            Assert.True(projectId != null, $"project id is empty, route: {url}");
            _projectId = projectId.Value;

            var info = new SetInfoModel()
            {
                Id = _projectId,
                ObjectType = ProjectTargetType.Cafe,
                Rooms = new RoomsModel[] {
                new RoomsModel() { Purpose = Room.ChildrenRoom, Count = 1},
                new RoomsModel() { Purpose = Room.LivingRoom, Count = 2}
            },
                Reasons = new ReasonModel[] {
                new ReasonModel() { Name = ProjectReason.NewHouse, Description = "Переехфл в сарай"}
            }};
            url = TestConfig.BaseUrl + "/api/client/project/setAdditionalInfo";
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(info));
            ServerHttpClient.HandleResponse(response, "Set additional info");

            url = TestConfig.BaseUrl + "/api/client/project/setStyles";
            var styles = new SetStylesModel()
            {
                Id = _projectId,
                Styles = new StyleModel[] {
                new StyleModel() { Name = ProjectStyle.Industrial, Description = "так модно"},
                new StyleModel() { Name = ProjectStyle.Minimalism, Description = "так дешево"}
            } };
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(styles));
            ServerHttpClient.HandleResponse(response, "Set styles");

            url = TestConfig.BaseUrl + "/api/client/project/setBudget";
            var budget = new SetBudgetModel()
            {
                Id = _projectId,
                Budget = new BudgetModel()
                {
                    FurnitureBudget = 1000,
                    RenovationBudget = 5000
                }
            };
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(budget));
            ServerHttpClient.HandleResponse(response, "Set budget");

            url = TestConfig.BaseUrl + "/api/client/project/get";
            response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var project = ServerHttpClient.HandleResponse<ProjectDescriptionModel>(response, "Get created projectId");
            Assert.True(project != null, $"Project not recieved by id, route: {url}");
            _projectModel = project;
        }

        [Fact, TestPriority(3)]
        public async Task AddDesignerToPoolByNastya()
        {
            //var url = TestConfig.BaseUrl + "/admin";
            //var loginForm = new Dictionary<string, string>() { { "Name", "faradise" }, { "Password", "F31NWp121991" } };
            var adminClient = new ServerHttpClient();
            //var response = await adminClient.MakePostFormRequestAsync(_clientToken, url, loginForm);
            //Assert.True(response.IsSuccessStatusCode, "Admin login failed");

            //var url = TestConfig.BaseUrl + "/admin/users/get-designers";
            //var response = await adminClient.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "offset", "0" }, { "count", "10000" } });
            //var designers = ServerHttpClient.HandleResponse<DesignerDatingModel[]>(response, "Admin get designers");
            //var createdDesigner = designers.FirstOrDefault(x => x.Name.Contains(_designerName));
            //Assert.True(createdDesigner != null, "Created designer not found in list");

            var url = TestConfig.BaseUrl + "/api/user/authorize";
            var response = await _client.MakePostJsonRequestAsync(_designerToken, url, null);
            var model = ServerHttpClient.HandleResponse<AuthorizeModel>(response, "authorize");

            _designerId = model.Id;

            url = TestConfig.BaseUrl + "/admin/project/add-designer-to-pool";
            var addModel = new DesignerSelectModel() { DesignerId = _designerId, ProjectId = _projectId };
            response = await adminClient.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(addModel));
            ServerHttpClient.HandleResponse(response, "add designer to pool");

        }

        [Fact, TestPriority(4)]
        public async Task UserSelectDesigner()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/get-designers";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var pool = ServerHttpClient.HandleResponse<DesignerDatingModel[]>(response, "Client get pool");
            Assert.True(pool != null && pool.Length > 0, "No designers in pool");

            var createdDesigner = pool.FirstOrDefault(x => x.Id == _designerId);
            Assert.True(createdDesigner != null, "Created designer not found in pool");

            url = TestConfig.BaseUrl + "/api/client/project/select-designer";
            var selectModel = new DesignerSelectModel() { DesignerId = _designerId, ProjectId = _projectId };
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(selectModel));
            ServerHttpClient.HandleResponse(response, "client select designer");

        }

        [Fact, TestPriority(5)]
        public async Task DesignerHasProjectInList()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/list";
            var response = await _client.MakeGetRequestAsync(_designerToken, url, null);
            var model = ServerHttpClient.HandleResponse<ProjectListModel>(response, "designerGetProjectList");
            Assert.True(model.Projects != null && model.Projects.Length > 0, "Designer dont have progect in list");
        }


        [Fact, TestPriority(5)]
        public async Task DesignerHasProjectDescription()
        {
            var url = TestConfig.BaseUrl + "/api/client/project/get-short-description";
            var response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var model = ServerHttpClient.HandleResponse<ShortProjectInfoModel>(response, "DesignerHasProjectDescription");
            Assert.True(model != null && model.Rooms.Length > 0, "Designer dont have project info");
        }

        [Fact, TestPriority(6)]
        public async Task UserConfirmDesigner()
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/confirm-designer";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, 
                JsonConvert.SerializeObject(new ConfirmDesignerModel { ProjectId = _projectId }));
            ServerHttpClient.HandleResponse(response, "user confirm designer");
        }

        [Fact, TestPriority(7)]
        public async Task DesignerConfirmUser()
        {
            await DesignerConfirmStage(CooperateStage.Meeting);
        }

        [Fact, TestPriority(8)]
        public async Task CheckStageChangedToPhotoAndProportion()
        {
            await CheckStage(CooperateStage.PhotoAndProportions, "Check stage changed to photo and proportion");
        }

        [Fact, TestPriority(9)]
        public async Task UserSetProportions()
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/get-room-information";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var roomModels = ServerHttpClient.HandleResponse<List<RoomInfoModel>>(response, "get room infos");
            Assert.True(roomModels != null, "Room models in null");
            Assert.True(roomModels.Count == _projectModel.Rooms.Sum(x => x.Count), "Some rooms are not created");

            var rooms = new UploadRoomsModel()
            {
                ProjectId = _projectId,
                RoomsInformation = new List<ShortRoomInfoModel>(roomModels.Count)
            };

            foreach (var roomModel in roomModels)
            {
                var proportions = new RoomProportionsModel()
                {
                    HeightCeiling = roomModel.RoomId * 100,
                    HeightDoor = roomModel.RoomId * 80,
                    SideA = roomModel.RoomId * 1000,
                    SideB = roomModel.RoomId * 2000,
                    WidthDoor = roomModel.RoomId * 40
                };
                rooms.RoomsInformation.Add(new ShortRoomInfoModel() { RoomId = roomModel.RoomId, Proportions = proportions });
            }

            url = TestConfig.BaseUrl + "/api/project/preparations/set-room-information";
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(rooms));
            ServerHttpClient.HandleResponse(response, "set room information");

            url = TestConfig.BaseUrl + "/api/project/preparations/get-room-information";
            response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            roomModels = ServerHttpClient.HandleResponse<List<RoomInfoModel>>(response, "get room infos");
            Assert.True(roomModels != null, "Room models in null");
            Assert.True(roomModels.Count == rooms.RoomsInformation.Count, "Some rooms is missing");

            foreach (var roomModel in roomModels)
            {
                var shortModel = rooms.RoomsInformation.FirstOrDefault(x => x.RoomId == roomModel.RoomId);
                Assert.True(shortModel != null, $" Model with id {roomModel.RoomId} is missing");
                Assert.True(shortModel.Proportions.IsEqualTo(roomModel.Proportions), "Proportions is wrong");
            }
            _rooms = roomModels;
        }

        [Fact, TestPriority(10)]
        public async Task MoveToStageStyles()
        {
            await DesignerConfirmStage(CooperateStage.PhotoAndProportions);
            await CheckStage(CooperateStage.Style, "Check stage moved to style stage");
        }

        [Fact, TestPriority(11)]
        public async Task UserSetStyles()
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/get-styles";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var styleModels = ServerHttpClient.HandleResponse<List<ClientDesignerStyleModel>>(response, "get room styles");
            Assert.True(styleModels != null, "Room models in null");
            Assert.True(styleModels.Count == _projectModel.Styles.Length, "Some styles are not created");

            var styles = new SetDesignStylesModel()
            {
                ProjectId = _projectId,
                Styles = new List<ClientDesignerStyleModel>()
                 {
                     new ClientDesignerStyleModel() { Name = ProjectStyle.Fusion, Description = "Фью-фью"},
                     new ClientDesignerStyleModel() { Name = ProjectStyle.ModernClassic, Description = "Старое, но новое"}
                 }
            };

            url = TestConfig.BaseUrl + "/api/project/preparations/set-favorite-styles";
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(styles));
            ServerHttpClient.HandleResponse(response, "set styles information");

            url = TestConfig.BaseUrl + "/api/project/preparations/get-styles";
            response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            styleModels = ServerHttpClient.HandleResponse<List<ClientDesignerStyleModel>>(response, "get room styles");
            Assert.True(styleModels != null, "Room models in null");
            Assert.True(styles.Styles.Count == styleModels.Count, "Some styles are not created");

            foreach (var style in styles.Styles)
            {
                var model = styleModels.FirstOrDefault(x => x.Name == style.Name);
                Assert.True(model != null, $" Style with name {style.Name} is missing");
                Assert.True(model.Description == style.Description, "Style description is wrong");
            }
        }

        [Fact, TestPriority(12)]
        public async Task MoveToStageBudget()
        {
            await DesignerConfirmStage(CooperateStage.Style);
            await CheckStage(CooperateStage.Budget, "Check stage moved to style stage");
        }

        [Fact, TestPriority(13)]
        public async Task UserSetBudget()
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/get-budget";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var budgetModel = ServerHttpClient.HandleResponse<ClientDesignerBudgetModel>(response, "get budget");
            Assert.True(budgetModel != null, "Budget model in null");
            Assert.True(budgetModel.FurnitureBudget == _projectModel.Budget.FurnitureBudget, "Furniture budget is wrong");
            Assert.True(budgetModel.RenovationBudget == _projectModel.Budget.RenovationBudget, "Renovation budget is wrong");

            var setBudgetModel = new ClientDesignerBudgetModel
            {
                ProjectId = _projectId,
                FurnitureBudget = 9000,
                RenovationBudget = 100500
            };

            url = TestConfig.BaseUrl + "/api/project/preparations/set-budget";
            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(setBudgetModel));
            ServerHttpClient.HandleResponse(response, "set budget");

            url = TestConfig.BaseUrl + "/api/project/preparations/get-budget";
            response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            budgetModel = ServerHttpClient.HandleResponse<ClientDesignerBudgetModel>(response, "get budget");
            Assert.True(budgetModel != null, "Budget model in null");
            Assert.True(budgetModel.FurnitureBudget == setBudgetModel.FurnitureBudget, "Furniture budget is wrong");
            Assert.True(budgetModel.RenovationBudget == setBudgetModel.RenovationBudget, "Renovation budget is wrong");
        }

        [Fact, TestPriority(14)]
        public async Task MoveToStageAdditionalInfo()
        {
            await DesignerConfirmStage(CooperateStage.Budget);
            await CheckStage(CooperateStage.AdditionalInfo, "Check stage moved to additional info stage");
        }

        [Fact, TestPriority(15)]
        public async Task SetAdditionalInfo()
        {
            var setInfo = new SetAdditionalInformationModel
            {
                ProjectId = _projectId,
                Information = new List<AdditionalInformationForRoomModel>()
            };

            foreach (var room in _rooms)
            {
                var roomAddInfo = new AdditionalInformationForRoomModel() { RoomId = room.RoomId };
                roomAddInfo.Questions = new List<AdditionalQuestionModel>()
                {
                    new AdditionalQuestionModel() { Index = 1, Answer = "Вопросик" + room.RoomId.ToString() }
                };
                setInfo.Information.Add(roomAddInfo);
            }

            var url = TestConfig.BaseUrl + "/api/project/preparations/set-information";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(setInfo));
            ServerHttpClient.HandleResponse(response, "set additional information");

            url = TestConfig.BaseUrl + "/api/project/preparations/get-additional-info";
            response = await _client.MakeGetRequestAsync(_designerToken, url, new Dictionary<string, string>() { { "projectId", _projectId.ToString() } });
            var addInfoModel = ServerHttpClient.HandleResponse<List<AdditionalInformationForRoomModel>>(response, "get room additional info");
            Assert.True(addInfoModel != null, "Room additional info model in null");
            Assert.True(addInfoModel.Count == setInfo.Information.Count, "Some info about rooms is missing");

            foreach (var info in setInfo.Information)
            {
                var roomInfo = addInfoModel.FirstOrDefault(x => x.RoomId == info.RoomId);
                Assert.True(roomInfo != null, $"Room info for room {info.RoomId} is missing");
                Assert.True(roomInfo.Questions.Count == info.Questions.Count, $"Some info about room {info.RoomId} is missing");
                foreach (var q in info.Questions)
                {
                    var qInfo = roomInfo.Questions.FirstOrDefault(x => x.Index == q.Index && q.Answer == x.Answer);
                    Assert.True(qInfo != null, $"Question info for room {info.RoomId} of if {q.Index} is missing or wrong");
                }
            }
        }

        [Fact, TestPriority(16)]
        public async Task MoveToStageWhatToSave()
        {
            await DesignerConfirmStage(CooperateStage.AdditionalInfo);
            await CheckStage(CooperateStage.WhatToSave, "Check stage moved to WhatToSave stage");
        }

        [Fact, TestPriority(17)]
        public async Task EndProjectPreparations()
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/set-old-stuff-ready";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url,
                JsonConvert.SerializeObject(new OldStuffReadyModel { ProjectId = _projectId }));
            ServerHttpClient.HandleResponse(response, "save old stuff is ready");

            await DesignerConfirmStage(CooperateStage.WhatToSave);
            await CheckStage(CooperateStage.WhatToSave, "Check stage is WhatToSave");
        }

        private async Task DesignerConfirmStage(CooperateStage current)
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/confirm-stage";
            var model = new ConfirmPreparationStageModel() { ProjectId = _projectId, Stage = current };
            var response = await _client.MakePostJsonRequestAsync(_designerToken, url, JsonConvert.SerializeObject(model));
            var isNextSatge = ServerHttpClient.HandleResponse<bool>(response, $"stage confirm {current}");
            Assert.True(isNextSatge, $"stage not changed");
        }
        
        private async Task CheckStage(CooperateStage validStage, string step)
        {
            var url = TestConfig.BaseUrl + "/api/project/preparations/get-current-stage";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string> { { "projectId", _projectId.ToString() } });
            var stageModel = ServerHttpClient.HandleResponse<StageStatusModel>(response, step);
            Assert.True(stageModel != null, $"stage is not valid on step {step} : stage model is empty");
            Assert.True(stageModel.Stage == validStage, $"stage is not valid on step {step} : {validStage} != {stageModel.Stage}");
        }
    }
}
