﻿using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.DesignerController
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class DesignerControllerTests
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static string _token = string.Empty;
        private static GetDesignerBasicInfoModel _basicInfoModel;
        private static TestResultsModel _testing;
        private static PortfolioModel _portfolio;
        private static DesignerPossibilityModel _possibilities;
        private static DesignerLegalModel _legalData;
        private static TestQuestionDescriptionModel[] _test;
        private static int _portfolioId;
        private static string _photoLink = string.Empty;

        [Fact, TestPriority(1)]
        public async Task CreateDesignerToken()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createDesigner";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create designer");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _token = token;
        }

        [Fact, TestPriority(2)]
        public async Task CreateDesignerBasicModel()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setBasicInfo";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(DesignerForTests.BasicInfoModel));
            bool isCreate = isCreate = ServerHttpClient.HandleResponse<bool>(response, "Create designer basic model");
            Assert.True(isCreate == true && (int)response.StatusCode == 200, $"designer basic model not created, route: {url}");
        }

        [Fact, TestPriority(3)]
        public async Task SetAvatar()
        {
            byte[] bytes = File.ReadAllBytes("Data/1.png");
            Assert.True(bytes.Length != 0, $"file not loaded");

            var content = new ByteArrayContent(bytes);
            var multipart = new MultipartFormDataContent();
            multipart.Add(content, "photo", "1.png");

            var url = TestConfig.BaseUrl + "/api/designer/addAvatar";
            var response = await _client.MakePostMultipartFormRequestAsync(_token, url, multipart);

            var path = ServerHttpClient.HandleResponse<string>(response, "Create file path");
            Assert.True(!string.IsNullOrEmpty(path), $"file path not created: {url}");
            _photoLink = path;
        }

        [Fact, TestPriority(4)]
        private async Task GetDesignerBasicModel()
        {
            var url = TestConfig.BaseUrl + "/api/designer/basicInfo";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var basicInfo = ServerHttpClient.HandleResponse<GetDesignerBasicInfoModel>(response, "Get designer basic model");

            Assert.True(DesignerControllerCompare.Compare(basicInfo, new GetDesignerBasicInfoModel() { Basic = DesignerForTests.BasicInfoModel.Basic,
                PortfolioLink = DesignerForTests.BasicInfoModel.PortfolioLink,
                Styles = DesignerForTests.BasicInfoModel.Styles,
                PhotoLink = _photoLink
            }), $"basic model not recieved, route: {url}");
            _basicInfoModel = basicInfo;
        }

        [Fact, TestPriority(5)]
        public async Task SetTestLink()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setTestLink";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(new SetTestLink { TestLink = "link"}));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Create test link");
            Assert.True(isCreate == true, $"test link not created, route: {url}");
        }

        [Fact, TestPriority(6)]
        private async Task GetTestJobInfo()
        {
            var url = TestConfig.BaseUrl + "/api/designer/getTestJobInfo";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var testJobInfo = ServerHttpClient.HandleResponse<GetTestJobInfo>(response, "Get test job info model");
            Assert.True(testJobInfo != null
                && testJobInfo.Name == DesignerForTests.Designer.Base.Name
                && testJobInfo.TestTime != 0
                && testJobInfo.TestApproved == false, $"test job info not found, route: {url}");
        }

        [Fact, TestPriority(7)]
        public async Task SetTesting()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setTestResults";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(DesignerForTests.Designer.Testing));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Set test");
            Assert.True(isCreate == true, $"tests not created, route: {url}");
        }

        [Fact, TestPriority(8)]
        private async Task GetTesting()
        {
            var url = TestConfig.BaseUrl + "/api/designer/testResults";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var testing = ServerHttpClient.HandleResponse<TestResultsModel>(response, "Get test");
            Assert.True(testing.Compare(DesignerForTests.Designer.Testing), $"tests not recieved, route: {url}");
            _testing = testing;
        }

        [Fact, TestPriority(9)]
        public async Task CreatePortfolio()
        {
            var url = TestConfig.BaseUrl + "/api/designer/createPortfolio";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var portfolioId =ServerHttpClient.HandleResponse<int>(response, "Create portfolio");
            Assert.True(portfolioId != 0, $"portfolio not created, route: {url}");
            DesignerForTests.Designer.Portfolio.Portfolios[0].Id = portfolioId;

            var response2 = await _client.MakeGetRequestAsync(_token, url, null);
            var portfolioId2 = ServerHttpClient.HandleResponse<int>(response2, "Create portfolio");
            _portfolioId = portfolioId2;
            DesignerForTests.Designer.Portfolio.Portfolios[1].Id = portfolioId2;
            Assert.True(portfolioId != 0, $"portfolio not created, route: {url}");
        }

        [Fact, TestPriority(10)]
        public async Task SetPortfolioDescription()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setPortfolioDescription";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(new PortfolioDescriptionModel { Id = DesignerForTests.Designer.Portfolio.Portfolios[0].Id, Name = DesignerForTests.Designer.Portfolio.Portfolios[0].Name, Description = DesignerForTests.Designer.Portfolio.Portfolios[0].Description }));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Set portfolio description");
            Assert.True(isCreate == true, $"portfolio description not created, route: {url}");
        }

        [Fact, TestPriority(11)]
        public async Task SetPortfoliosDescription()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setPortfoliosDescription";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(
                new PortfolioDescriptionsModel {
                    Descriptions = new PortfolioDescriptionModel[] { new PortfolioDescriptionModel { Id = DesignerForTests.Designer.Portfolio.Portfolios[0].Id, Name = DesignerForTests.Designer.Portfolio.Portfolios[0].Name, Description = DesignerForTests.Designer.Portfolio.Portfolios[0].Description },
                                                                     new PortfolioDescriptionModel { Id = DesignerForTests.Designer.Portfolio.Portfolios[1].Id, Name = DesignerForTests.Designer.Portfolio.Portfolios[1].Name, Description = DesignerForTests.Designer.Portfolio.Portfolios[1].Description } }
                }));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Set portfolios description");
            Assert.True(isCreate == true, $"portfolios description not created, route: {url}");
        }

        [Fact, TestPriority(12)]
        public async Task AddPortfolioPhoto()
        {
            byte[] bytes = File.ReadAllBytes("Data/1.png");
            Assert.True(bytes.Length != 0, $"file not loaded");

            var content = new ByteArrayContent(bytes);
            var multipart = new MultipartFormDataContent();
            multipart.Add(content, "photo", "1.png");
            multipart.Add(new StringContent(DesignerForTests.Designer.Portfolio.Portfolios[0].Id.ToString()), "PortfolioId");

            var url = TestConfig.BaseUrl + "/api/designer/addPortfolioPhoto";
            var response = await _client.MakePostMultipartFormRequestAsync(_token, url, multipart);

            var photoModel = ServerHttpClient.HandleResponse<PortfolioPhotoModel>(response, "Create file path");
            Assert.True(photoModel != null, $"file path not created: {url}");
            DesignerForTests.Designer.Portfolio.Portfolios[0].PhotoLinks[0].Id = photoModel.Id;
            DesignerForTests.Designer.Portfolio.Portfolios[0].PhotoLinks[0].PhotoLink = photoModel.PhotoLink;
        }

        [Fact, TestPriority(13)]
        private async Task GetPortfolioModel()
        {
            var url = TestConfig.BaseUrl + "/api/designer/portfolio";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var portfolio = ServerHttpClient.HandleResponse<PortfolioModel>(response, "Get portfolio");
            Assert.True(portfolio.Compare(DesignerForTests.Designer.Portfolio), $"portfolio not recieved, route: {url}");
            _portfolio = portfolio;
        }

        [Fact, TestPriority(14)]
        public async Task RemovePortfolio()
        {
            var url = TestConfig.BaseUrl + "/api/designer/removePortfolio";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(new RemovePortfolioModel { Id = _portfolioId }));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Remove portfolio");
            Assert.True(isCreate == true, $"portfolio not deleted, route: {url}");
        }

        [Fact, TestPriority(15)]
        public async Task SetLegalData()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setLegal";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(DesignerForTests.Designer.LegalModel));
            var isCreate = ServerHttpClient.HandleResponse<bool>(response, "Set legal data");
            Assert.True(isCreate == true, $"legal data not created, route: {url}");
        }

        [Fact, TestPriority(16)]
        private async Task GetLegalData()
        {
            var url = TestConfig.BaseUrl + "/api/designer/legal";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var legalData = ServerHttpClient.HandleResponse<DesignerLegalModel>(response, "Get legal data");
            Assert.True(legalData.Compare(DesignerForTests.Designer.LegalModel), $"legal data not recieved, route: {url}");
        }

        [Fact, TestPriority(17)]
        private async Task GetDesignerTest()
        {
            var url = TestConfig.BaseUrl + "/api/designer/getQuestions";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var test = ServerHttpClient.HandleResponse<TestQuestionDescriptionModel[]>(response, "Get designer test");
            Assert.True(test != null, $"designer test not recieved, route: {url}");
            _test = test;
        }

        [Fact, TestPriority(18)]
        private async Task SetDesignerPossibilities()
        {
            var url = TestConfig.BaseUrl + "/api/designer/setPossibilities";
            var response = await _client.MakePostJsonRequestAsync(_token, url, JsonConvert.SerializeObject(DesignerForTests.Designer.Possibilities));
            var success = ServerHttpClient.HandleResponse<bool>(response, "Set designer possibilities");
            Assert.True(success, $"basic model not recieved, route: {url}");
        }

        [Fact, TestPriority(19)]
        private async Task GetFullDesignerTest()
        {
            var url = TestConfig.BaseUrl + "/api/designer/fullInfo";
            var response = await _client.MakeGetRequestAsync(_token, url, null);
            var designer = ServerHttpClient.HandleResponse<DesignerModel>(response, "Get designer");
            Assert.True(designer.Possibilities.Compare(DesignerForTests.Designer.Possibilities), $"Possibilities not recieved, route: {url}");
        }
    }
}