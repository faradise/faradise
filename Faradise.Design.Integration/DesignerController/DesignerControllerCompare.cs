﻿using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faradise.Design.Integration.DesignerController
{
    public static class DesignerControllerCompare
    {
        public static bool Compare(GetDesignerBasicInfoModel model1, GetDesignerBasicInfoModel model2)
        {
            if(model1.Basic.Name != model2.Basic.Name
                || model1.Basic.Surname != model2.Basic.Surname
                || model1.Basic.Surname != model2.Basic.Surname
                || model1.Basic.Age != model2.Basic.Age
                || model1.Basic.City != model2.Basic.City
                || model1.Basic.Gender != model2.Basic.Gender
                || model1.Basic.Email != model2.Basic.Email
                || model1.Basic.About != model2.Basic.About
                || model1.Basic.WorkExperience != model2.Basic.WorkExperience)
                return false;

            if (!string.Equals(model1.PortfolioLink, model2.PortfolioLink))
                return false;

            for (int i = 0; i < model1.Styles.Length; i++)
            {
                if(model1.Styles[i].Name != model2.Styles[i].Name
                    || model1.Styles[i].Description != model2.Styles[i].Description)
                return false;
            }

            return true;
        }

        public static bool Compare(this TestResultsModel model1, TestResultsModel model2)
        {
            for (int i = 0; i < model1.Questions.Length; i++)
            {
                if (model1.Questions[i].Index != model2.Questions[i].Index
                    || model1.Questions[i].Answer != model2.Questions[i].Answer)
                    return false;
            }
            return true;
        }

        public static bool Compare(this PortfolioModel model1, PortfolioModel model2)
        {
            for (int i = 0; i < model1.Portfolios.Length; i++)
            {
                if (model1.Portfolios[i].Id != model2.Portfolios[i].Id
                    || model1.Portfolios[i].Name != model2.Portfolios[i].Name
                    || model1.Portfolios[i].Description != model2.Portfolios[i].Description)
                    return false;

                for (int j = 0; j < model1.Portfolios[j].PhotoLinks.Length; j++)
                {
                    if (model1.Portfolios[j].PhotoLinks[j].Id != model2.Portfolios[j].PhotoLinks[j].Id
                        || model1.Portfolios[j].PhotoLinks[j].PhotoLink != model2.Portfolios[j].PhotoLinks[j].PhotoLink)
                        return false;
                }
            }

            return true;
        }

        public static bool Compare(this DesignerLegalModel model1, DesignerLegalModel model2)
        {
            if (model1.LegalType != model2.LegalType)
                return false;

            if (model1.LegalType == Models.LegalType.Personal)
            {
                if (model1.PersonalLegalInfo.Name != model2.PersonalLegalInfo.Name
                    || model1.PersonalLegalInfo.MiddleName != model2.PersonalLegalInfo.MiddleName
                    || model1.PersonalLegalInfo.Surname != model2.PersonalLegalInfo.Surname
                    //|| model1.PersonalLegalInfo.Phone != model2.PersonalLegalInfo.Phone
                    || model1.PersonalLegalInfo.PassportSeries != model2.PersonalLegalInfo.PassportSeries
                    || model1.PersonalLegalInfo.PassportIssuedBy != model2.PersonalLegalInfo.PassportIssuedBy
                    || model1.PersonalLegalInfo.DateOfIssue != model2.PersonalLegalInfo.DateOfIssue
                    || model1.PersonalLegalInfo.DepartmentCode != model2.PersonalLegalInfo.DepartmentCode
                    || model1.PersonalLegalInfo.RegistrationAddress != model2.PersonalLegalInfo.RegistrationAddress
                    || model1.PersonalLegalInfo.ActualAddress != model2.PersonalLegalInfo.ActualAddress
                    || model1.PersonalLegalInfo.Email != model2.PersonalLegalInfo.Email)
                    return false;
            }
            else
            {
                if (model1.CompanyLegalInfo.Name != model2.CompanyLegalInfo.Name
                    || model1.CompanyLegalInfo.OGRN != model2.CompanyLegalInfo.OGRN
                    || model1.CompanyLegalInfo.INN != model2.CompanyLegalInfo.INN
                    || model1.CompanyLegalInfo.KPP != model2.CompanyLegalInfo.KPP
                    || model1.CompanyLegalInfo.BankName != model2.CompanyLegalInfo.BankName
                    || model1.CompanyLegalInfo.CheckingAccount != model2.CompanyLegalInfo.CheckingAccount
                    || model1.CompanyLegalInfo.BIC != model2.CompanyLegalInfo.BIC
                    || model1.CompanyLegalInfo.SoleExecutiveBody != model2.CompanyLegalInfo.SoleExecutiveBody
                    || model1.CompanyLegalInfo.NameSoleExecutiveBody != model2.CompanyLegalInfo.NameSoleExecutiveBody)
                    return false;
            }
            return true;
        }

        public static bool Compare(this DesignerPossibilityModel model1, DesignerPossibilityModel model2)
        {
            if (model1.GoToPlaceInYourCity != model2.GoToPlaceInYourCity
                    || model1.GoToPlaceInAnotherCity != model2.GoToPlaceInAnotherCity
                    || model1.PersonalСontrol != model2.PersonalСontrol
                    || model1.PersonalMeeting != model2.PersonalMeeting
                    || model1.SkypeMeeting != model2.SkypeMeeting
                    || model1.RoomMeasurements != model2.RoomMeasurements
                    || model1.Blueprints != model2.Blueprints)
                return false;

            return true;
        }
    }
}
