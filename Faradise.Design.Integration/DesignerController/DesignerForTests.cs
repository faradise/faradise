﻿using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.ProjectDescription;
using System;

namespace Faradise.Design.Integration.DesignerController
{
    class DesignerForTests
    {
        public static DesignerModel Designer = new DesignerModel
        {
            Id = 1,
            Base = new DesignerBaseModel
            {
                Name = "Имя",
                Surname = "Фамилия",
                Age = 26,
                City = "Москва",
                Gender = Gender.Male,
                Email = "123@123.ru",
                About = "О себе",
                WorkExperience = Models.Enums.WorkExperience.MoreThanFiveYears
            },
            Phone = "+79990000001",
            PortfolioLink = "",
            Portfolio = new PortfolioModel
            {
                Portfolios = new PortfolioProjectModel[2]
                    {
                        new PortfolioProjectModel
                        {
                            Id = 1,
                            Name = "Name",
                            Description = "Description",
                            PhotoLinks = new PortfolioPhotoModel[1]
                            {
                                new PortfolioPhotoModel() { Id = 0, PhotoLink = "" }
                            }
                        },
                        new PortfolioProjectModel
                        {
                            Id = 2,
                            Name = "Name2",
                            Description = "Description2"
                        }
                    }
            },
            Styles = new DesignerStyleModel[1]
                {
                    new DesignerStyleModel
                    {
                        Name = ProjectStyle.Country,
                        Description = "Description"
                    }
                },
            TestJob = new TestJobInfoModel
            {
                TestLink = "",
                TestTime = new DateTime(),
                TestApproved = false
            },
            PhotoLink = "",
                Level = DesignerLevel.Specialist,
                Possibilities = new DesignerPossibilityModel
                {
                    GoToPlaceInYourCity = true,
                    GoToPlaceInAnotherCity = true,
                    PersonalСontrol = true,
                    PersonalMeeting = true,
                    SkypeMeeting = false,
                    RoomMeasurements = false,
                    Blueprints = true
                },
                Testing = new TestResultsModel
                {
                    Questions = new TestResultsAnswerModel[1]
                    {
                        new TestResultsAnswerModel
                        {
                            Index = 1,
                            Answer = 3
                        }
                    }
                },
                LegalModel = new DesignerLegalModel
                {
                    LegalType = LegalType.Personal,
                    CompanyLegalInfo = new CompanyLegalInfoModel(),
                    PersonalLegalInfo = new PersonalLegalInfoModel
                    {
                        Surname = "Surname",
                        Name = "Name",
                        MiddleName = "MiddleName",
                        PassportSeries = "1234567890",
                        PassportIssuedBy = "qwe",
                        DateOfIssue = "1.1.2018",
                        DepartmentCode = "123",
                        RegistrationAddress = "dsfsdf",
                        ActualAddress = "dfdfdf",
                        Email = "123@234.ru"
                    }
                }
            };


        public static SetDesignerBasicInfoModel BasicInfoModel = new SetDesignerBasicInfoModel
        {
            Basic = Designer.Base,
            PortfolioLink = Designer.PortfolioLink,
            Styles = Designer.Styles
        };
    }
}
