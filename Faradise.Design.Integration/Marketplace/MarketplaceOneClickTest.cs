﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Faradise.Design.Integration.Marketplace
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class MarketplaceOneClickTest
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static string _clientToken = string.Empty;
        private static int _productId = 0;
        private static int _productCount = 0;
        private static int _orderId = 0;

        [Fact, TestPriority(0)]
        public async Task CreateClient()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _clientToken = token;
        }

        [Fact, TestPriority(1)]
        public async Task GetProducts()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "1" } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");

            _productId = products.Products[0].Id;
        }

        [Fact, TestPriority(2)]
        public async Task OrderOneClick()
        {
            _productCount = 1;
            var url = TestConfig.BaseUrl + "/api/marketplace/create-one-click-order";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject
                (new OneClickOrderModel
                {
                    DeliveryCost = 0,
                    Email = "lalala@mail.com",
                    Name = "Тестовый человек",
                    PaymentMethod = Models.Enums.PaymentMethod.YandexKassa,
                    Phone = "79165554433",
                    ProductCount = _productCount,
                    ProductId = _productId
                }));
            _orderId = ServerHttpClient.HandleResponse<int>(response, " Create one click order");
            Assert.True(_orderId != 0, "one click order id is null");
        }

        [Fact, TestPriority(3)]
        public async Task GetOrderOneClick()
        {
            var url = TestConfig.BaseUrl + "/api/admin/get-basket-order";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "orderId", _orderId.ToString() } });
            var model = ServerHttpClient.HandleResponse<FullBasketOrderModel>(response, "Get basket order admin");

            Assert.True(model != null, $"Order is null");
            Assert.True(model.OrderId == _orderId, $"Order id is wrong {JsonConvert.SerializeObject(model)}");
            Assert.True(model.Products.Length > 0 && model.Products[0].ProductId == _productId && model.Products[0].Count == _productCount, $"Product data is wrong");
        }
    }
}
