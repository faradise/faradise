﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;

namespace Faradise.Design.Integration.Marketplace
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class MarketplaceControllerTests
    {
        private static string _clientToken = string.Empty;
        private static ProductModel _product = null;
        private static AvailableFiltersModel _filterValues = null;
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static CategoryModel[] _categories = null;
        private static CategoryModel[] _subCategories = null;

        private static CategoryModel TestCategoty { get { return _categories.FirstOrDefault(); } }

        private static int _roomId = 0;

        [Fact, TestPriority(0)]
        public async Task CreateClient()
        {
            var url = TestConfig.BaseUrl + "/api/authentication/createClient";
            var response = await _client.MakePostJsonRequestAsync(null, url, null);
            var token = ServerHttpClient.HandleResponse<string>(response, "Create client");
            Assert.True(!string.IsNullOrEmpty(token), $"recieved token is empty, route: {url}");
            _clientToken = token;
        }

        [Fact, TestPriority(1)]
        public async Task GetCategories()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-categories";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, null);
            var model = ServerHttpClient.HandleResponse<CategoryModel[]>(response, "Client get categories");
            Assert.True(model != null && model.Length > 0, "Categories ncount is 0 or wrong");
            _categories = model;
        }

        [Fact, TestPriority(2)]
        public async Task GetSubCategories()
        {
            // TODO:: COMMENTED DUE TO MODEL FIXES

            //var parentCategory = _categories.FirstOrDefault(x => x.Childs > 0);
            //Assert.True(parentCategory != null, "Categories have no childs");
            //var url = TestConfig.BaseUrl + "/api/marketplace/get-subcategories";
            //var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "id", parentCategory.Id.ToString() } });
            //var model = ServerHttpClient.HandleResponse<CategoryModel[]>(response, "Client get subcategories");
            //Assert.True(model != null && model.Length > 0, "SubCategories ncount is 0 or wrong");
            //_subCategories = model;
        }

        [Fact, TestPriority(3)]
        public async Task GetProductsInCategory()
        {
            var url  = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "10" },
                    { "Category", TestCategoty.Id.ToString() } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");

            url = TestConfig.BaseUrl + "/api/marketplace/get-product";
            response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "productId", products.Products[0].Id.ToString() } });
            var productModel = ServerHttpClient.HandleResponse<ProductModel>(response, "Designer get single product");
            Assert.True(productModel != null, "product model is null");
            _product = productModel;
        }

        [Fact, TestPriority(4)]
        public async Task TestPriceFiltration()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" },
                    { "Category", TestCategoty.Id.ToString() },
                    { "PriceTo", _product.Price.ToString() },
                    { "PriceFrom", (_product.Price - 1).ToString()} });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");
        }

        [Fact, TestPriority(5)]
        public async Task TestWordFiltration()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" },
                    { "SearchWords",  _product.Name } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");
        }

        [Fact, TestPriority(6)]
        public async Task TestGetFilterValues()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-filters";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() { { "categoryId", TestCategoty.Id.ToString()} });
            var filterValues = ServerHttpClient.HandleResponse<AvailableFiltersModel>(response, "get filter values");
            Assert.True(filterValues != null, "no filter values ");
            _filterValues = filterValues;
            Assert.True(_filterValues.Brands != null && _filterValues.Brands.Length > 0, "no brands in test");
        }

        [Fact, TestPriority(7)]
        public async Task TestBrandFiltration()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" },
                    { "Brands",  _filterValues.Brands[0].Id.ToString() } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");
        }

        [Fact, TestPriority(8)]
        public async Task TestGetRooms()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-rooms";
            var response = await _client.MakeGetRequestAsync(null, url, new Dictionary<string, string>());
            var rooms = ServerHttpClient.HandleResponse<RoomNameModel[]>(response, "Get rooms");
            Assert.True((int)response.StatusCode == 200, $"Status code not OK, route: {url}");
            Assert.True(rooms != null || rooms.Length == 0, "Rooms is null or empty");
            _roomId = rooms[0].RoomNameId;
        }

        [Fact, TestPriority(8)]
        public async Task TestRoomFiltration()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            var response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" },
                    { "Rooms",  _roomId.ToString() } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length > 0, "no products return after filtration");
        }
    }
}
