﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Faradise.Design.Integration.ProjectController;
using Newtonsoft.Json;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.Enums;
using System.Linq;

namespace Faradise.Design.Integration.Marketplace
{
    [TestCaseOrderer("Faradise.Design.Integration.PriorityOrderer", "Faradise.Design.Integration")]
    public class MarketplaceBasketTests
    {
        private static readonly ServerHttpClient _client = new ServerHttpClient();
        private static string _clientToken = string.Empty;

        private static int _productIdFirst = -1;
        private static int _productIdSecond = -1;
        private static int _productPrice = 0;
        private static int _orderId = 0;

        [Fact, TestPriority(0)]
        public async Task PrepareStagesBeforeThis()
        {
            var prepTest = new ProjectPreparationsControllerTest();
            await prepTest.CreateClientToken();

            _clientToken = ProjectPreparationsControllerTest.ClientToken;
            Assert.True(!string.IsNullOrEmpty(_clientToken), $"{_clientToken}");
        }

        [Fact, TestPriority(1)]
        public async Task DesignerOpenMarketplace()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-categories";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, null);
            var model = ServerHttpClient.HandleResponse<CategoryModel[]>(response, "Get categories");
            Assert.True(model != null && model.Length > 0, "Categories ncount is 0");

            url = TestConfig.BaseUrl + "/api/marketplace/get-products-filtered";
            response = await _client.MakeGetRequestAsync(_clientToken, url,
                new Dictionary<string, string>() {
                    { "Offset", "0" },
                    { "Count", "2" } });
                    //{ "Category", string.Empty } });
            var products = ServerHttpClient.HandleResponse<GetProductsModel>(response, "get filtrated products");
            Assert.True(products != null && products.Products.Length >= 2, "no products return after filtration");

            _productIdFirst = products.Products[0].Id;
            _productIdSecond = products.Products[1].Id;
        }

        [Fact, TestPriority(2)]
        public async Task AddProductToBasket()
        {
            //Assert.True(false, $"{_productIdFirst}, {_productIdSecond}");

            ProductCountModel modelFirst = new ProductCountModel() { ProductId = _productIdFirst, Count = 2 };
            ProductCountModel modelSecond = new ProductCountModel() { ProductId = _productIdSecond, Count = 2 };

            var url = TestConfig.BaseUrl + "/api/marketplace/add-product-count-to-basket";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(modelFirst));

            Assert.True((int)response.StatusCode == 200, $"Status code first {response.StatusCode}");

            response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(modelSecond));

            Assert.True((int)response.StatusCode == 200, $"Status code second {response.StatusCode}");
        }

        [Fact, TestPriority(3)]
        public async Task GetMarketCart()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-market-basket";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, null);
            var model = ServerHttpClient.HandleResponse<ShopBasketModel>(response, "get basket");

            Assert.True(model.Products[0].Product.Id == _productIdFirst && model.Products[1].Product.Id == _productIdSecond, $"{JsonConvert.SerializeObject(model)}");
        }

        [Fact, TestPriority(4)]
        public async Task RemoveProductFromBasket()
        {
            ProductCountModel modelFirst = new ProductCountModel() { ProductId = _productIdFirst, Count = 2 };

            var url = TestConfig.BaseUrl + "/api/marketplace/remove-product-count-from-basket";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(modelFirst));

            Assert.True((int)response.StatusCode == 200, $"Status code first {response.StatusCode}");
        }

        [Fact, TestPriority(4)]
        public async Task RemoveAllProductFromBasket()
        {
            RemoveAllProductsFromBasketModel model = new RemoveAllProductsFromBasketModel() { ProductId = _productIdSecond };

            var url = TestConfig.BaseUrl + "/api/marketplace/remove-product-from-basket-completely";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));

            Assert.True((int)response.StatusCode == 200, $"Status code first {response.StatusCode}");
        }

        [Fact, TestPriority(5)]
        public async Task CheckRemoveProductFromBasket()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/get-market-basket";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, null);
            var model = ServerHttpClient.HandleResponse<ShopBasketModel>(response, "get basket");

            Assert.True(model.Products.Length == 0, $"{JsonConvert.SerializeObject(model)}");
        }

        [Fact, TestPriority(6)]
        public async Task OrderBasket()
        {
            await AddProductToBasket();

            MakeOrderModel model = new MakeOrderModel() { Adress = "Adress",
                ClientName = "ClientName",
                ClientSurname = "ClientSurname",
                Description = "Description",
                Phone = "79990000001",
                Email = "1@1.com" };

            var url = TestConfig.BaseUrl + "/api/marketplace/order-basket";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(model));
            var orderId = ServerHttpClient.HandleResponse<int>(response, "get filtrated products");
            _orderId = orderId;
            Assert.True((int)response.StatusCode == 200, $"Order basket response: {response}");
        }

        [Fact, TestPriority(11)]
        public async Task AdminGetBasketOrder()
        {
            var url = TestConfig.BaseUrl + "/api/admin/get-basket-order";
            var response = await _client.MakeGetRequestAsync(_clientToken, url, new Dictionary<string, string>() { { "orderId", _orderId.ToString() }});
            var model = ServerHttpClient.HandleResponse<FullBasketOrderModel>(response, "Get basket order admin");

            Assert.True(model.OrderId == _orderId, $"Order is null {JsonConvert.SerializeObject(model)}");
        }

        [Fact, TestPriority(12)]
        public async Task AdminCreatePaymentForBasketOrder()
        {
            var url = TestConfig.BaseUrl + "/api/admin/create-payment-for-basket-order";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, _orderId.ToString() );
            var model = ServerHttpClient.HandleResponse<string>(response, "Create payment for basket order admin");

            Assert.True((int)response.StatusCode == 200 && !string.IsNullOrEmpty(model), $"Create payment is false {model}");
        }

        [Fact, TestPriority(13)]
        public async Task AdminMoveOrderToWork()
        {
            var url = TestConfig.BaseUrl + "/api/admin/set-basket-order-status";
            var setStatusModel = new SetOrderStatusModel { OrderId = _orderId, Status = OrderStatus.InWork };
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, JsonConvert.SerializeObject(setStatusModel));
            
            Assert.True((int)response.StatusCode == 200, $"Bad move basket order to work");
        }

        [Fact, TestPriority(14)]
        public async Task AdminFinishBasketOrder()
        {
            var url = TestConfig.BaseUrl + "/api/admin/finish-basket-order";
            var response = await _client.MakePostJsonRequestAsync(_clientToken, url, _orderId.ToString() );
            
            Assert.True((int)response.StatusCode == 200, $"Bad finish basket order");
        }

        [Fact, TestPriority(15)]
        public async Task AddProductListToBasket()
        {
            var url = TestConfig.BaseUrl + "/api/marketplace/add-product-list-to-basket";
            var response = await _client.MakePostJsonRequestAsync
                (_clientToken, url, JsonConvert.SerializeObject(new AddProductListToBasketModel
                {
                    Products = new ProductCountModel[] {
                        new ProductCountModel
                        {
                            Count = 2,
                            ProductId = _productIdFirst
                        },
                        new ProductCountModel
                        {
                             ProductId = _productIdSecond,
                             Count = 3
                        }
                    }
                }));

            Assert.True((int)response.StatusCode == 200, $"Bad add products to basket response");

            url = TestConfig.BaseUrl + "/api/marketplace/get-market-basket";
            response = await _client.MakeGetRequestAsync(_clientToken, url, null);
            var model = ServerHttpClient.HandleResponse<ShopBasketModel>(response, "get basket");

            Assert.True((int)response.StatusCode == 200, $"Bad get basket response");

            Assert.True(model.Products != null && model.Products.Length <= 2, "Wrond product type count in basket");
            Assert.True(model.Products.Any(x => x.Product.Id == _productIdFirst && x.Count == 2), "Product information is wrong");
            Assert.True(model.Products.Any(x => x.Product.Id == _productIdSecond && x.Count == 3), "Product information is wrong");
        }
    }
}
