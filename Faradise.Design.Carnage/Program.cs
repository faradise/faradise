﻿using Faradise.Design.Marketplace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Marketplace.Xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Faradise.Design.Controllers.API;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Services;
using Faradise.Design.Services.Background;
using Faradise.Design.Services.Production.DB;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Text;
using Faradise.Design.Middleware;
using Faradise.Design.Middleware.Models;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Carnage
{
    public static class MethodInfoHelper
    {
        public static MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression)
        {
            var member = expression.Body as MethodCallExpression;

            if (member != null)
                return member.Method;

            throw new ArgumentException("Expression is not a method", "expression");
        }
    }


    class Program
    {
        private static string CurrentDb = "develop";

        private static string DevelopConnectionString =
            $"Server=tcp:faradise.database.windows.net,1433;Initial Catalog=faradise-design.{CurrentDb};Persist Security Info=False;User ID=faradise;Password=F31NWp121991;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        static void Main(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<FaradiseDBContext>();
            optionsBuilder.UseSqlServer(DevelopConnectionString);
            var context = new FaradiseDBContext(optionsBuilder.Options);
            //var cdn = new CDNService(null);
            //var product = context.Products.Find(119441);
            //product.JSONNotes =
            //    JsonConvert.SerializeObject(JsonConvert.DeserializeObject<string[]>(product.JSONNotes)
            //        .Append("ЦеНа:53454"));
            var service = new FeedAnalyzerBackgroundService(context, null);
            var result = service.Process();

            Console.ReadKey();
            ;
            MethodInfo mi = MethodInfoHelper
                .GetMethodInfo<APIMarketplaceController>(x => x.GetCompaniesModel(false));

            var files = new string[]
            {
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\bellus.xlsx",
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\valid.xls",
                //"C:\\Users\\Awespace8\\Downloads\\convertcsv.xlsx",
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\Blackwood_YML.xlsx",
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\decoline1.xlsx"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\iM - каталог Faradise.xlsx"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\Le Corbusier Art.xls"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\leander.xls"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\relaxa.xlsx"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\YML-TATOMI-2.xlsx"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\valid vendorCode not unique.xls"
                //"C:\\Users\\Awespace8\\Downloads\\ExcelTweak\\Blackwood_YML — vendorCodeFixed.xlsx"
                "C:\\Users\\Awespace11\\Downloads\\ExcelTweak\\yandex_msk.yml"
            };


            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            var companies = new List<MarketplaceCompany>();

            StringBuilder stringBuilder = new StringBuilder();

            foreach (var file in files)
            {
                using (var stream = File.OpenRead(file))
                {
                    //var parsingResult = XlsxMarketplaceParser.Parse(stream);


                    var parsingResult = MarketplaceParser.FromXml(stream);
                    companies.Add(parsingResult);

                    Console.WriteLine(parsingResult.Name);


                    MarketplaceCategory root = new MarketplaceCategory()
                    {
                        Id = 0,
                        ParentId = 0,
                        Name = "root",
                        Depth = 0
                    };

                    Childs.GetChildCategory(root, parsingResult.Categories, stringBuilder);
                    Console.WriteLine(stringBuilder.ToString());
                }
            }

            using (FileStream fstream = new FileStream(@"C:\\Users\\Awespace11\\Downloads\\ExcelTweak\\yandex_msk.csv",
                FileMode.OpenOrCreate))
            {
                // преобразуем строку в байты
                byte[] array = System.Text.Encoding.Default.GetBytes(stringBuilder.ToString());
                // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
                Console.WriteLine("Текст записан в файл");
            }

            var prev = companies
                .SelectMany(x => x.Products.Where(y => y.PreviousPrice != null))
                .ToArray();

            var companyCount = companies.Count;
            var categories = companies.Sum(x => x.Categories.Count);
            var products = companies.Sum(x => x.Products.Count);


            Console.WriteLine(companyCount);
            Console.WriteLine(categories);
            Console.WriteLine(products);
        }
    }

    public static class Childs
    {
        public static void GetChildCategory(MarketplaceCategory category, List<MarketplaceCategory> categories,
            StringBuilder stringBuilder)
        {
            foreach (var c in categories)
            {
                if (c.ParentId == category.Id)
                {
                    if (category.Childs == null)
                        category.Childs = new List<MarketplaceCategory>();

                    c.Depth = category.Depth + 1;

                    stringBuilder.AppendLine(string.Format("{0},\"{1}\"", c.Id, GetName(c.Name, c.Depth)));

                    category.Childs.Add(c);
                    GetChildCategory(c, categories, stringBuilder);
                }
            }
        }

        private static string GetName(string name, int depth)
        {
            var newName = string.Empty;

            for (int i = 0; i < depth - 1; i++)
                newName += "-- ";

            return newName + name;
        }
    }
}