﻿using System;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Faradise.Design.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        
        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var corsHeaders = new HeaderDictionary();
            foreach (var pair in context.Response.Headers)
            {
                if (!pair.Key.ToLower().StartsWith("access-control-"))
                    continue;
                corsHeaders[pair.Key] = pair.Value;
            }

            try
            {
                context.Response.OnStarting(o =>
                {
                    var ctx = (HttpContext)o;
                    var headers = ctx.Response.Headers;
                    foreach (var pair in corsHeaders)
                    {
                        if (headers.ContainsKey(pair.Key))
                            continue;
                        headers.Add(pair.Key, pair.Value);
                    }

                    return Task.CompletedTask;

                }, context);

                await _next(context);
            }
            catch (ModelValidationException ex)
            {
                await HandleException(context, "bad_request", 200, ex, ex.Errors);
            }
            catch (ServiceException ex)
            {
                await HandleException(context, "service_error", 200, ex, ex.Message, ex.Reason);
            }
            catch (AuthorizationException ex)
            {
                await HandleException(context, "forbidden", 401, ex, ex.Message);
            }
            catch (Exception ex)
            {
                await HandleException(context, "internal_server_error", 500, ex, ex.Message);
            }
        }

        private async Task HandleException(HttpContext context, string code, int statusCode, Exception ex, object message, object reason = null)
        {
            var err = string.Empty;
            if (reason == null)
                err = JsonConvert.SerializeObject(new { code, message });
            else err = JsonConvert.SerializeObject(new { code, reason, message });

            context.Items.Add(typeof(Exception), ex);

            context.Response.Clear();
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = statusCode;

            await context.Response.WriteAsync(err);
        }

        private class ExceptionWrapper : Exception
        {
            public object Details { get; private set; }

            public ExceptionWrapper(Exception source, string message, object details)
                : base(message, source)
            {
                Details = details;
            }
        }
    }
}
