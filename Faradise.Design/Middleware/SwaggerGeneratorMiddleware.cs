﻿using Faradise.Design.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Faradise.Design.Middleware
{
    public class SwaggerAuthorizedMiddleware
    {
        private readonly RequestDelegate _next;

        public SwaggerAuthorizedMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.StartsWithSegments("/swagger") && !context.User.Identity.IsAuthenticated)
            {
                context.Response.Redirect("/auth?to=swagger");
                return;
            }

            await _next.Invoke(context);
        }
    }

    public static class SwaggerMiddlewareExtensions
    {
        public static IApplicationBuilder UseFaradiseSwagger(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<SwaggerAuthorizedMiddleware>();
            builder.UseSwagger();
            return builder.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Faradise API v1");
            });
        }
    }

    public static class SwaggerFaradiseServiceCollectionExtensions
    {
        public static IServiceCollection AddFaradiseSwagger(this IServiceCollection services)
        {
            return services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", GenSwaggerDoc());
                c.DescribeAllEnumsAsStrings();
                c.OperationFilter<AddAuthTokenHeaderParameter>();
                c.OperationFilter<SwaggerUploadOpperationFilter>();

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        private static Info GenSwaggerDoc()
        {
            return new Info { Title = "Faradise Design API", Version = "v1" };
        }
    }
}
