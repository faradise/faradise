﻿namespace Faradise.Design.Middleware.Models
{
    public class RequestInfo : UtilityLog
    {
        public string Path { get; set; }
        public int ExecutingTime { get; set; }
        public string UserId { get; set; }
        public string Query { get; set; }

        public string ResponseBody { get; set; }
        public string RequestBody { get; set; }
        public int StatusCode { get; set; }
    }
}