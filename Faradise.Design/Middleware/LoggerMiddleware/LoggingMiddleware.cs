﻿using System;
using System.IO;
using System.Threading.Tasks;
using Faradise.Design.Middleware.Models;
using Faradise.Design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace Faradise.Design.Middleware
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Request.EnableRewind();
            var requestBody = await new StreamReader(context.Request.Body).ReadToEndAsync();
            context.Request.Body.Seek(0, SeekOrigin.Begin);

            using (var responseBodyMemoryStream = new MemoryStream())
            {
                var startTime = DateTime.Now;
                var originalResponseBodyReference = context.Response.Body;
                context.Response.Body = responseBodyMemoryStream;
                await _next(context);
                context.Response.Body.Seek(0, SeekOrigin.Begin);
                var responseBody = await new StreamReader(context.Response.Body).ReadToEndAsync();
                context.Response.Body.Seek(0, SeekOrigin.Begin);

                var data = new RequestInfo
                {
                    Path = context.Request.Path,
                    Query = context.Request.QueryString.Value,
                    RequestBody = requestBody,
                    ResponseBody = responseBody,
                    ExecutingTime = (int) (DateTime.Now - startTime).TotalMilliseconds,
                    StatusCode = context.Response.StatusCode
                };

                if (context.Items.ContainsKey(typeof(User)))
                    data.UserId = context.Items[typeof(User)].ToString();

                if (context.Items.ContainsKey(typeof(Exception)))
                {
                    var exception = (Exception) context.Items[typeof(Exception)];
                    ContextualLoggerWrapper.Error(data, exception);
                    
                }
                else ContextualLoggerWrapper.Information(data);

                await responseBodyMemoryStream.CopyToAsync(originalResponseBodyReference);
            }
        }
    }
}