﻿using System.Runtime.CompilerServices;

namespace Faradise.Design.Middleware
{
    public class CustomLog : LogBase
    {
        public CustomLog()
        {
        }

        public CustomLog(string message, params object[] args)
        {
            Message = message;
            Args = args;
        }

        public string Message { get; set; }
        public object[] Args { get; set; }
    }
}