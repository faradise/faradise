﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using Faradise.Design.Extensions;

namespace Faradise.Design.Middleware
{
    public abstract class UtilityLog : LogBase
    {
        protected bool Enabled => true;
    }

    public class LogBase { }
}