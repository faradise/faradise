﻿using System;
using System.Linq;
using Serilog.Events;
using Serilog.Filters;

namespace Faradise.Design.Middleware
{
    public static class MatchingExtensions
    {
        public static Func<LogEvent, bool> FromBaseClass<T>() where T : class => log =>
            (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
                from assemblyType in domainAssembly.GetTypes()
                where assemblyType.IsSubclassOf(typeof(T))
                select assemblyType)
            .Any(typeName => ((Func<LogEvent, bool>) typeof(Matching).GetMethods()
                    .Where(info => info.IsGenericMethod)
                    .First(info => info.Name == "FromSource")
                    .MakeGenericMethod(typeName)
                    .Invoke(null, null))
                .Invoke(log));
    }
}