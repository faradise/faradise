﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Faradise.Design.Middleware
{
    public static class ContextualLoggerWrapper
    {
        public static void Error<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Error(string.Empty);

        public static void Error<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Error(exception, string.Empty);

        public static void Information<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Information(string.Empty);

        public static void Information<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Information(exception, string.Empty);

        /// <summary>
        /// Does not send to sink
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        public static void Debug<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Debug(string.Empty);

        public static void Debug<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Debug(exception, string.Empty);

        public static void Fatal<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Fatal(string.Empty);

        public static void Fatal<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Fatal(exception, string.Empty);

        public static void Verbose<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Verbose(string.Empty);

        public static void Verbose<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Verbose(exception, string.Empty);

        public static void Warning<T>(T data) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Warning(string.Empty);

        public static void Warning<T>(T data, Exception exception) where T : LogBase =>
            Log.ForContext<T>()
                .ForContext(nameof(T), data, true)
                .Warning(exception, string.Empty);
    }
}