﻿using Faradise.Design.Internal.Exceptions;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Faradise.Design.Utilities
{
    public class PhoneUtility
    {
        private MaskData[] _masks = null;

        public class MaskData
        {
            public PhoneMaskModel Mask;
            public string RegPattern;
            public string NormalizedMask;
            public string SuggestPattern;
        }

        [Serializable]
        private class PhoneMaskArray
        {
            public PhoneMaskModel[] masks;
        }

        [Serializable]
        public class PhoneMaskModel
        {
            public string mask;
            public string cc;
            public string name_en;
            public string desc_en;
            public string name_ru;
            public string desc_ru;
        }

        public PhoneUtility()
        {
            var location = Path.Combine(Directory.GetCurrentDirectory(), "Utilities");
            var directory = Path.Combine(location, "PhoneCodes");
            var config = Path.Combine(directory, "phone-codes.json");

            using (StreamReader file = File.OpenText(config))
            {
                string json = file.ReadToEnd();
                PhoneMaskArray masks = JsonConvert.DeserializeObject<PhoneMaskArray>("{\"masks\":" + json + "}");
                PushMasks(masks);
            }
        }

        private void PushMasks(PhoneMaskArray masks)
        {
            _masks = new MaskData[masks.masks.Length];
            for (int t = 0; t < masks.masks.Length; t++)
            {
                var m = new MaskData();
                m.Mask = masks.masks[t];
                var pattern = "[^0-9,#]";
                m.NormalizedMask = "^" + Regex.Replace(masks.masks[t].mask, pattern, "");
                m.RegPattern = m.NormalizedMask.Replace("#","\\d");
                int lastDigit = 0;
                for (int i = 0; i < m.RegPattern.Length; i++)
                    if (char.IsDigit(m.RegPattern[i]))
                        lastDigit = i;
                m.SuggestPattern = m.RegPattern.Remove(lastDigit + 1);
                _masks[t] = m;
            }
        }

        public bool CheckMaskByNumber(MaskData maskData, string phone)
        {
            return (maskData.NormalizedMask.Length - 1) == phone.Length && Regex.IsMatch(phone, maskData.RegPattern);
        }

        public string FixPhone(string phone)
        {
            char[] ch = phone.ToCharArray().Where(x => char.IsDigit(x) == true).ToArray();
            phone = new string(ch);
            if(GetMaskFor(phone) != null)
                return phone;
            else            
                throw new ServiceException("Phone is invalid", "bad_phone");            
        }

        private MaskData GetMaskFor(string phone)
        {
            if (_masks == null)
                return null;
            foreach (var m in _masks)
            {
                if (CheckMaskByNumber(m, phone))
                    return m;
            }
            return null;
        }

        private MaskData GetSuggestMaskFor(string phone)
        {
            if (_masks == null)
                return null;
            foreach (var m in _masks)
            {
                if (((m.NormalizedMask.Length - 1) >= phone.Length) && Regex.IsMatch(phone, m.SuggestPattern))
                    return m;
            }
            return null;
        }
    }
}
