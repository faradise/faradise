﻿using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Faradise.Design.Utilities
{
    public static class ResizeImage
    {
        public static MemoryStream Resize(this MemoryStream imageStream, int maxSide)
        {
            using (var saveStream = new MemoryStream())
            {
                Image<Rgba32> img = Image.Load(imageStream.ToArray());
                if(img.Width >= img.Height)
                {
                    img.Mutate(x => x.Resize(maxSide, (int)(img.Height * maxSide / img.Width)));
                }
                else
                {
                    img.Mutate(x => x.Resize((int)(img.Width * maxSide / img.Height), maxSide));
                }
                img.Save(saveStream, ImageFormats.Png);
                return saveStream;
            }
        }
    }
}
