﻿using Faradise.Design.Models;
using System.Collections.Generic;
using System.IO;

namespace Faradise.Design.Utilities.Parser
{
    public interface IARProductsParser
    {
        List<ARProduct> Parse(Stream stream);
    }
}
