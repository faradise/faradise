﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;

namespace Faradise.Design.Utilities.Parser
{
    public class ARProductCSVParser : IARProductsParser
    {
        public List<ARProduct> Parse(Stream stream)
        {
            var columns = new Dictionary<string, int>();
            var products = new List<ARProduct>();
            using (var reader = new StreamReader(stream))
            {
                var headers = reader.ReadLine().Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                for (int h = 0; h < headers.Length; h++)
                    columns.Add(headers[h], h);
                int lineId = 1;
                while (true)
                {
                    var line = reader.ReadLine();
                    if (string.IsNullOrEmpty(line))
                        break;
                    var parsedLine = line.Split(new char[] { '\t' });
                    var product = new ARProduct();
                    product.Name = GetString(lineId, "name", parsedLine, columns);
                    product.FaradiseProductId = GetInt(lineId, "pid", parsedLine, columns);
                    product.VendorCode = GetString(lineId, "article", parsedLine, columns);
                    product.FaradiseColorId = GetInt(lineId, "cid", parsedLine, columns);
                    product.Color = GetString(lineId, "color_name", parsedLine, columns);
                    product.Vertiacal = HasValue(lineId, "orientation", parsedLine, columns);
                    product.Render = GetString(lineId, "render", parsedLine, columns);
                    product.IosBFM = new ARParsedModel { FaradiseProductId = product.FaradiseProductId, GeometryUrl = GetString(lineId, "bundle_name", parsedLine, columns) };
                    product.AndroidBFM = new ARParsedModel { FaradiseProductId = product.FaradiseProductId, GeometryUrl = GetString(lineId, "bundle_name", parsedLine, columns) };
                    product.IosSCN = new ARParsedModel { FaradiseProductId = product.FaradiseProductId, GeometryUrl = GetString(lineId, "model_scn", parsedLine, columns) };
                    product.AndroidSFB = new ARParsedModel { FaradiseProductId = product.FaradiseProductId, GeometryUrl = GetString(lineId, "model_sfb", parsedLine, columns) };

                    var diffuse = GetString(lineId, "map_diffuse", parsedLine, columns);
                    var normal = GetString(lineId, "map_normal", parsedLine, columns);
                    var metalic = GetString(lineId, "map_metalic", parsedLine, columns);
                    var roughness = GetString(lineId, "map_roughness", parsedLine, columns);
                    var metalicRouthness = GetString(lineId, "map_metallic_roughness", parsedLine, columns);

                    product.IosBFM.DiffuseUrl = product.IosSCN.DiffuseUrl = product.AndroidBFM.DiffuseUrl = product.AndroidSFB.DiffuseUrl = diffuse;
                    product.IosBFM.NormalUrl = product.IosSCN.NormalUrl = product.AndroidBFM.NormalUrl = product.AndroidSFB.NormalUrl = normal;
                    product.IosBFM.MetalicUrl = product.IosSCN.MetalicUrl = product.AndroidBFM.MetalicUrl = product.AndroidSFB.MetalicUrl = metalic;
                    product.IosBFM.RoughnessUrl = product.IosSCN.RoughnessUrl = product.AndroidBFM.RoughnessUrl = product.AndroidSFB.RoughnessUrl = roughness;
                    product.IosBFM.MetallicRoughnessUrl = product.IosSCN.MetallicRoughnessUrl = product.AndroidBFM.MetallicRoughnessUrl = product.AndroidSFB.MetallicRoughnessUrl = metalicRouthness;

                    products.Add(product);
                    lineId++;
                }
            }
            return products;
        }

        public string GetString(int lineIndex, string columnName, string[] split,  Dictionary<string,int> columns)
        {
            int columnId;
            if (!columns.TryGetValue(columnName, out columnId))
                throw new ServiceException($"column {columnName} not filled for line {lineIndex}", "ar_product_parse_error");
            return split[columnId];
        }

        public int GetInt(int lineIndex, string columnName, string[] split, Dictionary<string, int> columns)
        {
            int columnId;
            if (!columns.TryGetValue(columnName, out columnId))
                throw new ServiceException($"column {columnName} not filled for line {lineIndex}", "ar_product_parse_error");
            int value = 0;
            if (!int.TryParse(split[columnId], out value))
                throw new ServiceException($"column {columnName} not int for line {lineIndex}", "ar_product_parse_error");
            return value;
        }

        public bool HasValue(int lineIndex, string columnName, string[] split, Dictionary<string, int> columns)
        {
            int columnId;
            if (!columns.TryGetValue(columnName, out columnId))
                throw new ServiceException($"column {columnName} not filled for line {lineIndex}", "ar_product_parse_error");
            return !string.IsNullOrEmpty(split[columnId]);
        }
    }
}
