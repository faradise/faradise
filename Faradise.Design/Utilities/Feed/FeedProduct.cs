﻿namespace Faradise.Design.Utilities.Feed
{
    public class FeedProduct
    {
        public int Id { get; set; }
        public bool Available { get; set; }

        public int Price { get; set; }
        public int? PreviousPrice { get; set; }

        public int CategoryId { get; set; }
        public string[] Categories { get; set; }

        public string[] Pictures { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public float? Width { get; set; }
        public float? Height { get; set; }
        public float? Length { get; set; }

        public string Color { get; set; }

        public string Vendor { get; set; }
        public string VendorCode { get; set; }

        public string Model { get; set; }

        public bool? Warranty { get; set; }
    }
}
