﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    public class YmlOffer
    {
        [XmlAttribute(AttributeName = "id")] public int Id;
        [XmlAttribute(AttributeName = "available")] public bool Available;

        [XmlElement("url")] public string Url;

        [XmlElement("price")] public int Price;
        [XmlElement("oldprice")] public string PreviousPrice;
        [XmlElement("currencyId")] public string CurrencyId = "RUR";

        [XmlElement("categoryId")] public string CategoryId;

        [XmlElement("picture")] public string[] Pictures;

        [XmlElement("store")] public bool Store = false;
        [XmlElement("pickup")] public bool Pickup = false;
        [XmlElement("delivery")] public bool Delivery = true;

        [XmlArray("delivery-options"), XmlArrayItem("option")] public XmlDeliveryOption[] DeliveryOptions;

        [XmlElement("name")] public string Name;

        [XmlElement("vendor")] public string Vendor;
        [XmlElement("model")] public string Model;

        [XmlElement("dimensions")] public string Size;
        
        [XmlElement("description")] public string Description;

        [XmlElement("manufacturer_warranty")] public bool ManufacturerWarranty;

        [XmlElement("param")] public YmlParameter[] Parameters;
    }

    public class XmlDeliveryOption
    {
        [XmlAttribute(AttributeName = "cost")] public string cost;
        [XmlAttribute(AttributeName = "days")] public string days;
    }
}
