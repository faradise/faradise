﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    public class YmlCategory
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id;

        [XmlAttribute(AttributeName = "parentId")]
        public string ParentId;

        [XmlText]
        public string Name;
    }
}
