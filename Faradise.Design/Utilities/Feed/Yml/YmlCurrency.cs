﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    public class YmlCurrency
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id;

        [XmlAttribute(AttributeName = "rate")]
        public int Rate;
    }
}
