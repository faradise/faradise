﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    public class YmlParameter
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name;

        [XmlText]
        public string Value;

        [XmlAttribute(AttributeName = "unit")]
        public string Unit;
    }
}