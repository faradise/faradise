﻿using System;
using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    [XmlRoot("yml_catalog")]
    public class YmlCatalog
    {
        [XmlElement("shop")] public YmlShop Shop;
        [XmlAttribute("date")] public string Date = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
    }
}
