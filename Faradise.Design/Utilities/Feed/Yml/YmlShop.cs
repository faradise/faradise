﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Yml
{
    public class YmlShop
    {
        [XmlElement("name")] public string Name = "Faradise";
        [XmlElement("company")] public string Company = "Faradise";
        [XmlElement("url")] public string Url = "https://faradise.ru";

        [XmlArray("currencies")] [XmlArrayItem("currency")]
        public YmlCurrency[] Currencies = { new YmlCurrency { Id = "RUR", Rate = 1 } };

        [XmlArray("categories")]
        [XmlArrayItem("category")]
        public YmlCategory[] Categories;

        [XmlArray("delivery-options"), XmlArrayItem("option")]
        public XmlDeliveryOption[] DeliveryOptions;

        [XmlArray("offers")]
        [XmlArrayItem("offer")]
        public YmlOffer[] Offers;
    }
}