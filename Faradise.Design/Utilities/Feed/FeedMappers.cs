﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Models.Feed.Google;
using Faradise.Design.Models.Feed.Yml;

namespace Faradise.Design.Utilities.Feed
{
    public static class FeedMappers
    {
        public static YmlOffer ToYml(this FeedProduct x)
        {
            var offer = new YmlOffer
            {
                Id = x.Id,
                Available = x.Available,

                Url = $"https://faradise.ru/product/{x.Id}",

                Price = x.Price,
                PreviousPrice = x.PreviousPrice?.ToString(),

                CategoryId = x.CategoryId.ToString(),

                Pictures = x.Pictures
                    .Take(5)
                    .ToArray(),

                Name = x.Name,
                Description = x.Description,

                Vendor = x.Vendor,
                Model = x.Model,

                Size = CreateYmlSize(x.Width, x.Height, x.Length),
                Parameters = CreateYmlParameters(x.Width, x.Height, x.Length),

                ManufacturerWarranty = x.Warranty ?? false,
            };

            return offer;
        }

        private static YmlParameter[] CreateYmlParameters(float? width, float? height, float? length)
        {
            var list = new List<YmlParameter>();

            if (width != null)
                list.Add(new YmlParameter { Name = "Ширина", Unit = "см", Value = width.ToString() });

            if (height != null)
                list.Add(new YmlParameter { Name = "Высота", Unit = "см", Value = height.ToString() });

            if (length != null)
                list.Add(new YmlParameter { Name = "Длина", Unit = "см", Value = length.ToString() });

            return list.ToArray();
        }

        private static string CreateYmlSize(float? width, float? height, float? length)
        {
            if (width.HasValue && height.HasValue && length.HasValue)
                return $"{width}/{height}/{length}".Replace(",", ".");
            return null;
        }

        public static GoogleEntry ToGoogle(this FeedProduct x)
        {
            var entry = new GoogleEntry
            {
                Id = x.Id.ToString(),

                Title = x.Name,
                Description = x.Description,

                Link = $"https://faradise.ru/product/{x.Id}",

                ImageLink = x.Pictures.FirstOrDefault(),
                AdditionalImageLink = x.Pictures
                    .Skip(1)
                    .Take(5)
                    .ToArray(),

                Availability = x.Available ? "in stock" : "out of stock",

                Price = $"{x.Price} RUB",
                SalePrice = x.PreviousPrice != null ? $"{x.PreviousPrice} RUB" : null,

                Brand = x.Vendor,
                MPN = x.Id.ToString(),
                Color = x.Color,

                ProductType = CreateGoogleProductType(x.Categories),
                
            };

            var customLabel0 = GetCustomLabel0ByPrice(x.Price);
            if (customLabel0 != null)
                entry.CustomLabel0 = customLabel0;

            return entry;
        }

        private static string GetCustomLabel0ByPrice(int price)
        {
            if (price == 0)
                return null;

            var priceK = (int)(price / 10000.0f);

            if (priceK < 1)
                return null;

            if (priceK < 10)
                return (priceK * 10000).ToString();

            if (priceK < 12)
                return "100000";

            if (priceK < 15)
                return "120000";

            return "150000";
        }

        private static string CreateGoogleProductType(IEnumerable<string> categories)
        {
            var list = categories.ToList();
            var type = list.LastOrDefault();

            if (string.IsNullOrEmpty(type))
                return null;

            list.RemoveAt(list.Count - 1);

            while (list.Any())
            {
                type = $"{list.Last()} > {type}";
                list.RemoveAt(list.Count - 1);
            }

            return type;
        }
    }
}
