﻿using System;
using System.Xml.Serialization;
using Faradise.Design.Models.Feed.Google;

namespace Faradise.Design.Utilities.Feed.Google
{
    [XmlRoot("feed")]
    public class GoogleFeed
    {
        [XmlElement("title")] public string Title = "Faradise";
        [XmlElement("link")] public GoogleLink Link = new GoogleLink();
        [XmlElement("updated")] public string Updated = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");

        [XmlElement("entry")] public GoogleEntry[] Entries;
    }
}
