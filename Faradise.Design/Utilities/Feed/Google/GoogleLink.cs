﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Google
{
    public class GoogleLink
    {
        [XmlAttribute("href")] public string Link = "https://faradise.ru";
        [XmlAttribute("rel")] public string Rel = "self";
    }
}
