﻿using System.Xml.Serialization;

namespace Faradise.Design.Models.Feed.Google
{
    public class GoogleEntry
    {
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "id")] public string Id;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "title")] public string Title;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "description")] public string Description;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "link")] public string Link;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "image_link")] public string ImageLink;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "additional_image_link")] public string[] AdditionalImageLink;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "condition")] public string Condition = "new";
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "availability")] public string Availability;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "price")] public string Price;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "sale_price")] public string SalePrice;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "brand")] public string Brand;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "mpn")] public string MPN;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "product")] public string ProductType;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "color")] public string Color;
        [XmlElement(Namespace = "http://base.google.com/ns/1.0", ElementName = "custom_label_0")] public string CustomLabel0;
    }
}