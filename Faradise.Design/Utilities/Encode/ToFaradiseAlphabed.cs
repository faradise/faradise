﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Utilities.Encode
{
    public static class ToFaradiseAlphabed
    {
        private const string CharList = "FARDSE2018ILOVCT3NGUHBX5MQK7946PJWYZ";

        /// <summary>
        /// Encode the given number into a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static String Encode(int input)
        {
            if (input < 0) throw new ArgumentOutOfRangeException("input", input, "input cannot be negative");

            char[] clistarr = CharList.ToCharArray();
            var result = new Stack<char>();
            while (input != 0)
            {
                result.Push(clistarr[input % 36]);
                input /= 36;
            }
            return new string(result.ToArray());
        }

        public static int GetCharPos(char c)
        {
            return CharList.IndexOf(c);
        }

        /// <summary>
        /// Decode the Encoded string into a number
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Int64 Decode(string input)
        {
            var reversed = input.ToLower().Reverse();
            long result = 0;
            int pos = 0;
            foreach (char c in reversed)
            {
                result += CharList.IndexOf(c) * (long)Math.Pow(36, pos);
                pos++;
            }
            return result;
        }
    }
}
