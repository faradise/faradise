﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class wetwe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectMarketplaceOrders_Payments_PaymentId",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectMarketplaceOrders_Payments_PaymentId",
                table: "ProjectMarketplaceOrders",
                column: "PaymentId",
                principalTable: "Payments",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectMarketplaceOrders_Payments_PaymentId",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectMarketplaceOrders_Payments_PaymentId",
                table: "ProjectMarketplaceOrders",
                column: "PaymentId",
                principalTable: "Payments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
