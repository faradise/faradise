﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceColors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MarketplaceColorId",
                table: "ProductColorEntity",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MarketplaceColors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceColors", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductColorEntity_MarketplaceColorId",
                table: "ProductColorEntity",
                column: "MarketplaceColorId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductColorEntity_MarketplaceColors_MarketplaceColorId",
                table: "ProductColorEntity",
                column: "MarketplaceColorId",
                principalTable: "MarketplaceColors",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductColorEntity_MarketplaceColors_MarketplaceColorId",
                table: "ProductColorEntity");

            migrationBuilder.DropTable(
                name: "MarketplaceColors");

            migrationBuilder.DropIndex(
                name: "IX_ProductColorEntity_MarketplaceColorId",
                table: "ProductColorEntity");

            migrationBuilder.DropColumn(
                name: "MarketplaceColorId",
                table: "ProductColorEntity");
        }
    }
}
