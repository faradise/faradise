﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class ProductCollection_ManualDisable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ManualDisable",
                table: "ProductCollections",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ManualDisable",
                table: "ProductCollections");
        }
    }
}
