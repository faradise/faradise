﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class BannersMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Banners",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    PictureWeb = table.Column<string>(nullable: true),
                    PictureMobile = table.Column<string>(nullable: true),
                    PictureMobileBackground = table.Column<string>(nullable: true),
                    CompilationName = table.Column<string>(nullable: true),
                    ProductId = table.Column<int>(nullable: true),
                    CategoryId = table.Column<int>(nullable: true),
                    PriceFrom = table.Column<int>(nullable: true),
                    PriceTo = table.Column<int>(nullable: true),
                    BrandId = table.Column<int>(nullable: true),
                    Updated = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banners", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Banners");
        }
    }
}
