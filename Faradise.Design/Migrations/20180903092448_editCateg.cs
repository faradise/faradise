﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class editCateg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "MarketPlaceCategories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "InHeader",
                table: "MarketPlaceCategories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPopular",
                table: "MarketPlaceCategories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "MarketPlaceCategories");

            migrationBuilder.DropColumn(
                name: "InHeader",
                table: "MarketPlaceCategories");

            migrationBuilder.DropColumn(
                name: "IsPopular",
                table: "MarketPlaceCategories");
        }
    }
}
