﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class abs_value_promocode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Absolute",
                table: "PromoCodeExports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinPrice",
                table: "PromoCodeExports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "SupportPromoCode",
                table: "MarketplaceOrderProducts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Absolute",
                table: "PromoCodeExports");

            migrationBuilder.DropColumn(
                name: "MinPrice",
                table: "PromoCodeExports");

            migrationBuilder.DropColumn(
                name: "SupportPromoCode",
                table: "MarketplaceOrderProducts");
        }
    }
}
