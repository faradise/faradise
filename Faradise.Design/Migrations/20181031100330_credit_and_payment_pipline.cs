﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class credit_and_payment_pipline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeliveryPrice",
                table: "ProjectMarketplaceOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsReady",
                table: "ProjectMarketplaceOrders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SelectedPaymentMethod",
                table: "ProjectMarketplaceOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "ProjectMarketplaceOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Method",
                table: "Payments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PlatformRoute",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reciept",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeliveryPrice",
                table: "MarketplaceOrders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsReady",
                table: "MarketplaceOrders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SelectedPaymentMethod",
                table: "MarketplaceOrders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryPrice",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "IsReady",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "SelectedPaymentMethod",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "ProjectMarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "Method",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "PlatformRoute",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "Reciept",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "DeliveryPrice",
                table: "MarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "IsReady",
                table: "MarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "SelectedPaymentMethod",
                table: "MarketplaceOrders");
        }
    }
}
