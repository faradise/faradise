﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class basketLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SavedBaskets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserOwnerId = table.Column<int>(nullable: false),
                    JSONProductsIds = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedBaskets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ViewsSavedBaskets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    SavedBasketId = table.Column<int>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViewsSavedBaskets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ViewsSavedBaskets_SavedBaskets_SavedBasketId",
                        column: x => x.SavedBasketId,
                        principalTable: "SavedBaskets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ViewsSavedBaskets_SavedBasketId",
                table: "ViewsSavedBaskets",
                column: "SavedBasketId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ViewsSavedBaskets");

            migrationBuilder.DropTable(
                name: "SavedBaskets");
        }
    }
}
