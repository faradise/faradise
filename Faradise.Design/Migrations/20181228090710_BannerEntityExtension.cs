﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class BannerEntityExtension : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DiscountFilter",
                table: "Banners",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Ordering",
                table: "Banners",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Banners",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountFilter",
                table: "Banners");

            migrationBuilder.DropColumn(
                name: "Ordering",
                table: "Banners");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "Banners");
        }
    }
}
