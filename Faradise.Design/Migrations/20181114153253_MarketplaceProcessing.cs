﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceProcessing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Processed",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SalesUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Processed",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "SalesUpdated",
                table: "MarketplaceEntity");
        }
    }
}
