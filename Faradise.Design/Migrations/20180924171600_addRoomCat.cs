﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addRoomCat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RoomNames",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomNames", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompanyCategoryRooms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    CompanyCategoryId = table.Column<int>(nullable: false),
                    RoomNameId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyCategoryRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyCategoryRooms_RoomNames_RoomNameId",
                        column: x => x.RoomNameId,
                        principalTable: "RoomNames",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyCategoryRooms_CompanyCategories_CompanyId_CompanyCategoryId",
                        columns: x => new { x.CompanyId, x.CompanyCategoryId },
                        principalTable: "CompanyCategories",
                        principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategoryRooms_RoomNameId",
                table: "CompanyCategoryRooms",
                column: "RoomNameId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategoryRooms_CompanyId_CompanyCategoryId",
                table: "CompanyCategoryRooms",
                columns: new[] { "CompanyId", "CompanyCategoryId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyCategoryRooms");

            migrationBuilder.DropTable(
                name: "RoomNames");
        }
    }
}
