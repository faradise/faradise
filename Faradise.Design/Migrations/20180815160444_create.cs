﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectDefinitions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    DesignerId = table.Column<int>(nullable: true),
                    DesignerConfirmed = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Stage = table.Column<int>(nullable: false),
                    AbandonStage = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDefinitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestAnswerDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuestionId = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    For = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestAnswerDescriptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TestQuestionDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Text = table.Column<string>(nullable: true),
                    For = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestQuestionDescriptions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Role = table.Column<int>(nullable: false),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VerificationCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Number = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerificationCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    FurnitureBudget = table.Column<int>(nullable: false),
                    RenovationBudget = table.Column<int>(nullable: false),
                    ObjectType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDescriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDescriptions_ProjectDefinitions_Id",
                        column: x => x.Id,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    FurnitureBudget = table.Column<int>(nullable: false),
                    RenovationBudget = table.Column<int>(nullable: false),
                    Stage = table.Column<int>(nullable: false),
                    ClientIsReady = table.Column<bool>(nullable: false),
                    DesignerIsReady = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowDescriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowDescriptions_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowOldFurnishPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowOldFurnishPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowOldFurnishPhotos_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowOldFurniturePhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowOldFurniturePhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowOldFurniturePhotos_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowStyles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Style = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowStyles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowStyles_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Aliases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    AliasType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aliases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Aliases_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Designers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    WorkExperience = table.Column<int>(nullable: false),
                    PortfolioLink = table.Column<string>(nullable: true),
                    TestJobLink = table.Column<string>(nullable: true),
                    TestApproved = table.Column<bool>(nullable: false),
                    PhotoLink = table.Column<string>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    LegalType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Designers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Designers_Users_Id",
                        column: x => x.Id,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerRequirements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    AgeFrom = table.Column<int>(nullable: false),
                    AgeTo = table.Column<int>(nullable: false),
                    PersonalMeetingRequired = table.Column<bool>(nullable: false),
                    PersonalMeetingAddress = table.Column<string>(nullable: true),
                    PersonalControlRequired = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerRequirements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DesignerRequirements_ProjectDescriptions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDescriptionRooms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Room = table.Column<int>(nullable: false),
                    PlanUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDescriptionRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDescriptionRooms_ProjectDescriptions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDescriptionsReasons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Reason = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDescriptionsReasons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDescriptionsReasons_ProjectDescriptions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDescriptionStyles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Style = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDescriptionStyles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDescriptionStyles_ProjectDescriptions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TestResultAnswers",
                columns: table => new
                {
                    ProjectId = table.Column<int>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false),
                    AnswerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestResultAnswers", x => new { x.ProjectId, x.QuestionId });
                    table.ForeignKey(
                        name: "FK_TestResultAnswers_TestAnswerDescriptions_AnswerId",
                        column: x => x.AnswerId,
                        principalTable: "TestAnswerDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestResultAnswers_ProjectDescriptions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TestResultAnswers_TestQuestionDescriptions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "TestQuestionDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerCompanyLegalInfos",
                columns: table => new
                {
                    DesignerId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OGRN = table.Column<string>(nullable: true),
                    INN = table.Column<string>(nullable: true),
                    KPP = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    CheckingAccount = table.Column<string>(nullable: true),
                    BIC = table.Column<string>(nullable: true),
                    SoleExecutiveBody = table.Column<string>(nullable: true),
                    NameSoleExecutiveBody = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerCompanyLegalInfos", x => x.DesignerId);
                    table.ForeignKey(
                        name: "FK_DesignerCompanyLegalInfos_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerPersonalLegalInfos",
                columns: table => new
                {
                    DesignerId = table.Column<int>(nullable: false),
                    Surname = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    PassportSeries = table.Column<string>(nullable: true),
                    PassportIssuedBy = table.Column<string>(nullable: true),
                    DateOfIssue = table.Column<string>(nullable: true),
                    DepartmentCode = table.Column<string>(nullable: true),
                    RegistrationAddress = table.Column<string>(nullable: true),
                    ActualAddress = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerPersonalLegalInfos", x => x.DesignerId);
                    table.ForeignKey(
                        name: "FK_DesignerPersonalLegalInfos_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerPortfolioProjects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DesignerId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerPortfolioProjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DesignerPortfolioProjects_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerPossibilities",
                columns: table => new
                {
                    DesignerId = table.Column<int>(nullable: false),
                    GoToPlaceInYourCity = table.Column<bool>(nullable: false),
                    GoToPlaceInAnotherCity = table.Column<bool>(nullable: false),
                    PersonalСontrol = table.Column<bool>(nullable: false),
                    PersonalMeeting = table.Column<bool>(nullable: false),
                    SkypeMeeting = table.Column<bool>(nullable: false),
                    RoomMeasurements = table.Column<bool>(nullable: false),
                    Blueprints = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerPossibilities", x => x.DesignerId);
                    table.ForeignKey(
                        name: "FK_DesignerPossibilities_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerStyles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DesignerId = table.Column<int>(nullable: false),
                    Name = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerStyles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DesignerStyles_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerTestJobInfos",
                columns: table => new
                {
                    DesignerId = table.Column<int>(nullable: false),
                    TestLink = table.Column<string>(nullable: true),
                    TestTime = table.Column<DateTime>(nullable: false),
                    TestApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerTestJobInfos", x => x.DesignerId);
                    table.ForeignKey(
                        name: "FK_DesignerTestJobInfos_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerTestResultsAnswers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DesignerId = table.Column<int>(nullable: false),
                    QuestionId = table.Column<int>(nullable: false),
                    AnswerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerTestResultsAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DesignerTestResultsAnswers_TestAnswerDescriptions_AnswerId",
                        column: x => x.AnswerId,
                        principalTable: "TestAnswerDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DesignerTestResultsAnswers_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DesignerTestResultsAnswers_TestQuestionDescriptions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "TestQuestionDescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowPlanOldPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowPlanOldPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowPlanOldPhotos_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowRoomAdditionalInformations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    QuestionIndex = table.Column<int>(nullable: false),
                    Answer = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowRoomAdditionalInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowRoomAdditionalInformations_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowRoomPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowRoomPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowRoomPhotos_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectFlowRoomsProportions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    SideA = table.Column<int>(nullable: false),
                    SideB = table.Column<int>(nullable: false),
                    HeightDoor = table.Column<int>(nullable: false),
                    WidthDoor = table.Column<int>(nullable: false),
                    HeightCeiling = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectFlowRoomsProportions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectFlowRoomsProportions_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DesignerPortfolioProjectPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PortfolioProjectId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DesignerPortfolioProjectPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DesignerPortfolioProjectPhotos_DesignerPortfolioProjects_PortfolioProjectId",
                        column: x => x.PortfolioProjectId,
                        principalTable: "DesignerPortfolioProjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aliases_UserId",
                table: "Aliases",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerPortfolioProjectPhotos_PortfolioProjectId",
                table: "DesignerPortfolioProjectPhotos",
                column: "PortfolioProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerPortfolioProjects_DesignerId",
                table: "DesignerPortfolioProjects",
                column: "DesignerId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerRequirements_ProjectId",
                table: "DesignerRequirements",
                column: "ProjectId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DesignerStyles_DesignerId",
                table: "DesignerStyles",
                column: "DesignerId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerTestResultsAnswers_AnswerId",
                table: "DesignerTestResultsAnswers",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerTestResultsAnswers_DesignerId",
                table: "DesignerTestResultsAnswers",
                column: "DesignerId");

            migrationBuilder.CreateIndex(
                name: "IX_DesignerTestResultsAnswers_QuestionId",
                table: "DesignerTestResultsAnswers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDescriptionRooms_ProjectId",
                table: "ProjectDescriptionRooms",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDescriptionsReasons_ProjectId",
                table: "ProjectDescriptionsReasons",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDescriptionStyles_ProjectId",
                table: "ProjectDescriptionStyles",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowDescriptions_ProjectId",
                table: "ProjectFlowDescriptions",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowOldFurnishPhotos_ProjectId",
                table: "ProjectFlowOldFurnishPhotos",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowOldFurniturePhotos_ProjectId",
                table: "ProjectFlowOldFurniturePhotos",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowPlanOldPhotos_RoomId",
                table: "ProjectFlowPlanOldPhotos",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowRoomAdditionalInformations_RoomId",
                table: "ProjectFlowRoomAdditionalInformations",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowRoomPhotos_RoomId",
                table: "ProjectFlowRoomPhotos",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowRoomsProportions_RoomId",
                table: "ProjectFlowRoomsProportions",
                column: "RoomId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProjectFlowStyles_ProjectId",
                table: "ProjectFlowStyles",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TestResultAnswers_AnswerId",
                table: "TestResultAnswers",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_TestResultAnswers_QuestionId",
                table: "TestResultAnswers",
                column: "QuestionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aliases");

            migrationBuilder.DropTable(
                name: "DesignerCompanyLegalInfos");

            migrationBuilder.DropTable(
                name: "DesignerPersonalLegalInfos");

            migrationBuilder.DropTable(
                name: "DesignerPortfolioProjectPhotos");

            migrationBuilder.DropTable(
                name: "DesignerPossibilities");

            migrationBuilder.DropTable(
                name: "DesignerRequirements");

            migrationBuilder.DropTable(
                name: "DesignerStyles");

            migrationBuilder.DropTable(
                name: "DesignerTestJobInfos");

            migrationBuilder.DropTable(
                name: "DesignerTestResultsAnswers");

            migrationBuilder.DropTable(
                name: "ProjectDescriptionsReasons");

            migrationBuilder.DropTable(
                name: "ProjectDescriptionStyles");

            migrationBuilder.DropTable(
                name: "ProjectFlowDescriptions");

            migrationBuilder.DropTable(
                name: "ProjectFlowOldFurnishPhotos");

            migrationBuilder.DropTable(
                name: "ProjectFlowOldFurniturePhotos");

            migrationBuilder.DropTable(
                name: "ProjectFlowPlanOldPhotos");

            migrationBuilder.DropTable(
                name: "ProjectFlowRoomAdditionalInformations");

            migrationBuilder.DropTable(
                name: "ProjectFlowRoomPhotos");

            migrationBuilder.DropTable(
                name: "ProjectFlowRoomsProportions");

            migrationBuilder.DropTable(
                name: "ProjectFlowStyles");

            migrationBuilder.DropTable(
                name: "TestResultAnswers");

            migrationBuilder.DropTable(
                name: "VerificationCodes");

            migrationBuilder.DropTable(
                name: "DesignerPortfolioProjects");

            migrationBuilder.DropTable(
                name: "ProjectDescriptionRooms");

            migrationBuilder.DropTable(
                name: "TestAnswerDescriptions");

            migrationBuilder.DropTable(
                name: "TestQuestionDescriptions");

            migrationBuilder.DropTable(
                name: "Designers");

            migrationBuilder.DropTable(
                name: "ProjectDescriptions");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "ProjectDefinitions");
        }
    }
}
