﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addBTechFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BrigadeTechSpecs",
                columns: table => new
                {
                    ProjectId = table.Column<int>(nullable: false),
                    DesignerId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    PublishTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrigadeTechSpecs", x => x.ProjectId);
                    table.ForeignKey(
                        name: "FK_BrigadeTechSpecs_ProjectDevelopments_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDevelopments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BrigadeTechSpecFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BrigadeTechSpecId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrigadeTechSpecFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrigadeTechSpecFiles_BrigadeTechSpecs_BrigadeTechSpecId",
                        column: x => x.BrigadeTechSpecId,
                        principalTable: "BrigadeTechSpecs",
                        principalColumn: "ProjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BrigadeTechSpecFiles_BrigadeTechSpecId",
                table: "BrigadeTechSpecFiles",
                column: "BrigadeTechSpecId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BrigadeTechSpecFiles");

            migrationBuilder.DropTable(
                name: "BrigadeTechSpecs");
        }
    }
}
