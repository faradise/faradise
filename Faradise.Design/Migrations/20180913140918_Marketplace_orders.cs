﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class Marketplace_orders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectMarketplaceOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    PaymentId = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    BasePrice = table.Column<int>(nullable: false),
                    AdditionalPrice = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Adress = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectMarketplaceOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectMarketplaceOrders_Payments_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "Payments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProjectMarketplaceOrders_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoomGoods_OrderId",
                table: "RoomGoods",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectMarketplaceOrders_PaymentId",
                table: "ProjectMarketplaceOrders",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectMarketplaceOrders_ProjectId",
                table: "ProjectMarketplaceOrders",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomGoods_ProjectMarketplaceOrders_OrderId",
                table: "RoomGoods",
                column: "OrderId",
                principalTable: "ProjectMarketplaceOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomGoods_ProjectMarketplaceOrders_OrderId",
                table: "RoomGoods");

            migrationBuilder.DropTable(
                name: "ProjectMarketplaceOrders");

            migrationBuilder.DropIndex(
                name: "IX_RoomGoods_OrderId",
                table: "RoomGoods");
        }
    }
}
