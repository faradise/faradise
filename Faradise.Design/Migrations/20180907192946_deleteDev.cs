﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class deleteDev : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectDevelopmentStageData");

            migrationBuilder.DropTable(
                name: "ProjectDevelopments");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectDevelopments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CurrentStage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDevelopmentStageData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientIsReady = table.Column<bool>(nullable: false),
                    DesignerIsReady = table.Column<bool>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    Stage = table.Column<int>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    TotalLength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopmentStageData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDevelopmentStageData_ProjectDevelopments_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDevelopments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDevelopmentStageData_ProjectId",
                table: "ProjectDevelopmentStageData",
                column: "ProjectId");
        }
    }
}
