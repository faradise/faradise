﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceProcessingRemovedUnusedEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Badges_MarketplaceEntity_MarketplaceId",
                table: "Badges");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales");

            migrationBuilder.DropTable(
                name: "BadgeToCategories");

            migrationBuilder.DropTable(
                name: "BadgeToCompanies");

            migrationBuilder.DropTable(
                name: "BadgeToProducts");

            migrationBuilder.DropTable(
                name: "SaleToCategories");

            migrationBuilder.DropTable(
                name: "SaleToCompanies");

            migrationBuilder.DropTable(
                name: "SaleToProducts");

            migrationBuilder.DropIndex(
                name: "IX_Sales_MarketplaceId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Badges_MarketplaceId",
                table: "Badges");

            migrationBuilder.DropColumn(
                name: "MarketplaceId",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "MarketplaceId",
                table: "Badges");

            migrationBuilder.AddColumn<int>(
                name: "BadgeId",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SaleId",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceEntity_BadgeId",
                table: "MarketplaceEntity",
                column: "BadgeId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceEntity_SaleId",
                table: "MarketplaceEntity",
                column: "SaleId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketplaceEntity_Badges_BadgeId",
                table: "MarketplaceEntity",
                column: "BadgeId",
                principalTable: "Badges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketplaceEntity_Sales_SaleId",
                table: "MarketplaceEntity",
                column: "SaleId",
                principalTable: "Sales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketplaceEntity_Badges_BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketplaceEntity_Sales_SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.DropIndex(
                name: "IX_MarketplaceEntity_BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropIndex(
                name: "IX_MarketplaceEntity_SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.AddColumn<int>(
                name: "MarketplaceId",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MarketplaceId",
                table: "Badges",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BadgeToCategories",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToCategories", x => new { x.BadgeId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_BadgeToCategories_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToCategories_MarketPlaceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MarketPlaceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BadgeToCompanies",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToCompanies", x => new { x.BadgeId, x.CompanyId });
                    table.ForeignKey(
                        name: "FK_BadgeToCompanies_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BadgeToProducts",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToProducts", x => new { x.BadgeId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_BadgeToProducts_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleToCategories",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToCategories", x => new { x.SaleId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_SaleToCategories_MarketPlaceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MarketPlaceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToCategories_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleToCompanies",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToCompanies", x => new { x.SaleId, x.CompanyId });
                    table.ForeignKey(
                        name: "FK_SaleToCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToCompanies_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleToProducts",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToProducts", x => new { x.SaleId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_SaleToProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToProducts_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                unique: true,
                filter: "[MarketplaceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Badges_MarketplaceId",
                table: "Badges",
                column: "MarketplaceId",
                unique: true,
                filter: "[MarketplaceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToCategories_CategoryId",
                table: "BadgeToCategories",
                column: "CategoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToCompanies_CompanyId",
                table: "BadgeToCompanies",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToProducts_ProductId",
                table: "BadgeToProducts",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToCategories_CategoryId",
                table: "SaleToCategories",
                column: "CategoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToCompanies_CompanyId",
                table: "SaleToCompanies",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToProducts_ProductId",
                table: "SaleToProducts",
                column: "ProductId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Badges_MarketplaceEntity_MarketplaceId",
                table: "Badges",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
