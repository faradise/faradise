﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceFeedRefactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketplaceEntity_Badges_BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_MarketplaceEntity_Sales_SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.DropIndex(
                name: "IX_MarketplaceEntity_BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropIndex(
                name: "IX_MarketplaceEntity_SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "Above10kFeed",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "Above10kFeedUpdated",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "BadgeId",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedGoogle",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedGoogleUpdated",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedYml",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "SaleId",
                table: "MarketplaceEntity");

            migrationBuilder.RenameColumn(
                name: "FeedYmlUpdated",
                table: "MarketplaceEntity",
                newName: "FeedUpdated");

            migrationBuilder.CreateTable(
                name: "MarketplaceFeeds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MarketplaceId = table.Column<int>(nullable: false),
                    Format = table.Column<int>(nullable: false),
                    Availability = table.Column<int>(nullable: false),
                    Cost = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceFeeds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MarketplaceFeeds_MarketplaceEntity_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "MarketplaceEntity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceFeeds_MarketplaceId",
                table: "MarketplaceFeeds",
                column: "MarketplaceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MarketplaceFeeds");

            migrationBuilder.RenameColumn(
                name: "FeedUpdated",
                table: "MarketplaceEntity",
                newName: "FeedYmlUpdated");

            migrationBuilder.AddColumn<string>(
                name: "Above10kFeed",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Above10kFeedUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "BadgeId",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FeedGoogle",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FeedGoogleUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FeedYml",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SaleId",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceEntity_BadgeId",
                table: "MarketplaceEntity",
                column: "BadgeId");

            migrationBuilder.CreateIndex(
                name: "IX_MarketplaceEntity_SaleId",
                table: "MarketplaceEntity",
                column: "SaleId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketplaceEntity_Badges_BadgeId",
                table: "MarketplaceEntity",
                column: "BadgeId",
                principalTable: "Badges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MarketplaceEntity_Sales_SaleId",
                table: "MarketplaceEntity",
                column: "SaleId",
                principalTable: "Sales",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
