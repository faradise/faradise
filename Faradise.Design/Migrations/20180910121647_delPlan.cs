﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class delPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectDescriptionRooms_RoomPlans_PlanRoomId",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropTable(
                name: "RoomPlanPhotos");

            migrationBuilder.DropTable(
                name: "RoomPlans");

            migrationBuilder.DropIndex(
                name: "IX_ProjectDescriptionRooms_PlanRoomId",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropColumn(
                name: "PlanRoomId",
                table: "ProjectDescriptionRooms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlanRoomId",
                table: "ProjectDescriptionRooms",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RoomPlans",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ArchiveUrl = table.Column<string>(nullable: true),
                    IsComplete = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlans", x => x.RoomId);
                });

            migrationBuilder.CreateTable(
                name: "RoomPlanPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    PlanId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlanPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomPlanPhotos_RoomPlans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "RoomPlans",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDescriptionRooms_PlanRoomId",
                table: "ProjectDescriptionRooms",
                column: "PlanRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomPlanPhotos_PlanId",
                table: "RoomPlanPhotos",
                column: "PlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectDescriptionRooms_RoomPlans_PlanRoomId",
                table: "ProjectDescriptionRooms",
                column: "PlanRoomId",
                principalTable: "RoomPlans",
                principalColumn: "RoomId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
