﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class compilations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CategoryCompilations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryCompilations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCompilations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCompilations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompilationsToCategories",
                columns: table => new
                {
                    CompilationId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompilationsToCategories", x => new { x.CompilationId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_CompilationsToCategories_MarketPlaceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MarketPlaceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompilationsToCategories_CategoryCompilations_CompilationId",
                        column: x => x.CompilationId,
                        principalTable: "CategoryCompilations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompilationsToProducts",
                columns: table => new
                {
                    CompilationId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompilationsToProducts", x => new { x.CompilationId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_CompilationsToProducts_ProductCompilations_CompilationId",
                        column: x => x.CompilationId,
                        principalTable: "ProductCompilations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompilationsToProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompilationsToCategories_CategoryId",
                table: "CompilationsToCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_CompilationsToProducts_ProductId",
                table: "CompilationsToProducts",
                column: "ProductId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompilationsToCategories");

            migrationBuilder.DropTable(
                name: "CompilationsToProducts");

            migrationBuilder.DropTable(
                name: "CategoryCompilations");

            migrationBuilder.DropTable(
                name: "ProductCompilations");
        }
    }
}
