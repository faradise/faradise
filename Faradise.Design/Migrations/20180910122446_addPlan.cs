﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RoomPlans",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    ArchiveUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlans", x => x.RoomId);
                    table.ForeignKey(
                        name: "FK_RoomPlans_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomPlanPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlanId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlanPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomPlanPhotos_RoomPlans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "RoomPlans",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoomPlanPhotos_PlanId",
                table: "RoomPlanPhotos",
                column: "PlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoomPlanPhotos");

            migrationBuilder.DropTable(
                name: "RoomPlans");
        }
    }
}
