﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class delFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VisualizationTechSpecs_ProjectDefinitions_ProjectId",
                table: "VisualizationTechSpecs");

            migrationBuilder.DropIndex(
                name: "IX_VisualizationTechSpecs_ProjectId",
                table: "VisualizationTechSpecs");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "VisualizationTechSpecs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "VisualizationTechSpecs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationTechSpecs_ProjectId",
                table: "VisualizationTechSpecs",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_VisualizationTechSpecs_ProjectDefinitions_ProjectId",
                table: "VisualizationTechSpecs",
                column: "ProjectId",
                principalTable: "ProjectDefinitions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
