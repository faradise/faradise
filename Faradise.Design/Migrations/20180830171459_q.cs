﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class q : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_ParentCompanyCategoryId",
                table: "CompanyCategories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_ParentCompanyCategoryId",
                table: "CompanyCategories",
                column: "ParentCompanyCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyCategoryId",
                table: "CompanyCategories",
                column: "ParentCompanyCategoryId",
                principalTable: "CompanyCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
