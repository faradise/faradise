﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addFullTextIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    CREATE FULLTEXT CATALOG products_catalog AS DEFAULT
                ",
                true);

            migrationBuilder.Sql(
                @"
                    CREATE FULLTEXT INDEX ON dbo.Products(Name,Description) 
                    KEY INDEX PK_Products
                    ON products_catalog
                    WITH STOPLIST = SYSTEM
                ",
                true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
