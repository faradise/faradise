﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class add_company_info : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompanyProductionMask",
                table: "Companies",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ContactPersonEmail",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPersonName",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPersonPhone",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeliveryInfo",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LegalName",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NotesJson",
                table: "Companies",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompanyProductionMask",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ContactPersonEmail",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ContactPersonName",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ContactPersonPhone",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "DeliveryInfo",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "LegalName",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "NotesJson",
                table: "Companies");
        }
    }
}
