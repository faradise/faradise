﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "UserMadeDecision",
                table: "ProjectDevelopmentStageData",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "VisualizationTechSpecs",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false),
                    DesignerId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    PublishTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisualizationTechSpecs", x => x.RoomId);
                    table.ForeignKey(
                        name: "FK_VisualizationTechSpecs_Designers_DesignerId",
                        column: x => x.DesignerId,
                        principalTable: "Designers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisualizationTechSpecs_ProjectDefinitions_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VisualizationTechSpecs_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VisualizationProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    VisualizationTechSpecId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisualizationProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisualizationProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VisualizationProducts_VisualizationTechSpecs_VisualizationTechSpecId",
                        column: x => x.VisualizationTechSpecId,
                        principalTable: "VisualizationTechSpecs",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VisualizationTechSpecFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TechSpecId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisualizationTechSpecFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisualizationTechSpecFiles_VisualizationTechSpecs_TechSpecId",
                        column: x => x.TechSpecId,
                        principalTable: "VisualizationTechSpecs",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationProducts_ProductId",
                table: "VisualizationProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationProducts_VisualizationTechSpecId",
                table: "VisualizationProducts",
                column: "VisualizationTechSpecId");

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationTechSpecFiles_TechSpecId",
                table: "VisualizationTechSpecFiles",
                column: "TechSpecId");

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationTechSpecs_DesignerId",
                table: "VisualizationTechSpecs",
                column: "DesignerId");

            migrationBuilder.CreateIndex(
                name: "IX_VisualizationTechSpecs_ProjectId",
                table: "VisualizationTechSpecs",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VisualizationProducts");

            migrationBuilder.DropTable(
                name: "VisualizationTechSpecFiles");

            migrationBuilder.DropTable(
                name: "VisualizationTechSpecs");

            migrationBuilder.DropColumn(
                name: "UserMadeDecision",
                table: "ProjectDevelopmentStageData");
        }
    }
}
