﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addDateFill : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasChangesAfterLastAdminCheck",
                table: "ProjectDefinitions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "NeedAdmin",
                table: "ProjectDefinitions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ProjectFillDate",
                table: "ProjectDefinitions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasChangesAfterLastAdminCheck",
                table: "ProjectDefinitions");

            migrationBuilder.DropColumn(
                name: "NeedAdmin",
                table: "ProjectDefinitions");

            migrationBuilder.DropColumn(
                name: "ProjectFillDate",
                table: "ProjectDefinitions");
        }
    }
}
