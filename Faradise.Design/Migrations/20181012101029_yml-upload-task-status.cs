﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class ymluploadtaskstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFailed",
                table: "YmlUpdates");

            migrationBuilder.AddColumn<string>(
                name: "FailMessage",
                table: "YmlUpdates",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "YmlUpdates",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FailMessage",
                table: "YmlUpdates");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "YmlUpdates");

            migrationBuilder.AddColumn<bool>(
                name: "IsFailed",
                table: "YmlUpdates",
                nullable: false,
                defaultValue: false);
        }
    }
}
