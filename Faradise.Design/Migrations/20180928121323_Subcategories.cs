﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class Subcategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "MarketPlaceCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceCategories_ParentId",
                table: "MarketPlaceCategories",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketPlaceCategories_MarketPlaceCategories_ParentId",
                table: "MarketPlaceCategories",
                column: "ParentId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketPlaceCategories_MarketPlaceCategories_ParentId",
                table: "MarketPlaceCategories");

            migrationBuilder.DropIndex(
                name: "IX_MarketPlaceCategories_ParentId",
                table: "MarketPlaceCategories");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "MarketPlaceCategories");
        }
    }
}
