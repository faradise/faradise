﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class delViz : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RoomVizualizationPhotos");
            migrationBuilder.DropTable(
                            name: "RoomVizualizations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomVizualizationEntity_ProjectDescriptionRooms_RoomId",
                table: "RoomVizualizationEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_RoomVizualizationPhotoEntity_RoomVizualizationEntity_VizualizationId",
                table: "RoomVizualizationPhotoEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoomVizualizationPhotoEntity",
                table: "RoomVizualizationPhotoEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoomVizualizationEntity",
                table: "RoomVizualizationEntity");

            migrationBuilder.RenameTable(
                name: "RoomVizualizationPhotoEntity",
                newName: "RoomVizualizationPhotos");

            migrationBuilder.RenameTable(
                name: "RoomVizualizationEntity",
                newName: "RoomVizualizations");

            migrationBuilder.RenameIndex(
                name: "IX_RoomVizualizationPhotoEntity_VizualizationId",
                table: "RoomVizualizationPhotos",
                newName: "IX_RoomVizualizationPhotos_VizualizationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoomVizualizationPhotos",
                table: "RoomVizualizationPhotos",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoomVizualizations",
                table: "RoomVizualizations",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomVizualizationPhotos_RoomVizualizations_VizualizationId",
                table: "RoomVizualizationPhotos",
                column: "VizualizationId",
                principalTable: "RoomVizualizations",
                principalColumn: "RoomId",
                onDelete: ReferentialAction.Cascade);

            
        }
    }
}
