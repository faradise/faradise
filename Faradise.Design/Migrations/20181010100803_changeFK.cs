﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class changeFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_CompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "CompanyId", "ParentCompanyCategoryId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_CompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "CompanyId", "ParentCompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_CompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_CompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "ParentCompanyId", "ParentCompanyCategoryId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "ParentCompanyId", "ParentCompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.SetNull);
        }
    }
}
