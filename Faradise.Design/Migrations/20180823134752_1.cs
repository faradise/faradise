﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDefinitions_ClientId",
                table: "ProjectDefinitions",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDefinitions_DesignerId",
                table: "ProjectDefinitions",
                column: "DesignerId");

            migrationBuilder.AddForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools",
                column: "DesignerId",
                principalTable: "Designers",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectDefinitions_Users_ClientId",
                table: "ProjectDefinitions",
                column: "ClientId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectDefinitions_Designers_DesignerId",
                table: "ProjectDefinitions",
                column: "DesignerId",
                principalTable: "Designers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectDefinitions_Users_ClientId",
                table: "ProjectDefinitions");

            migrationBuilder.DropForeignKey(
                name: "FK_ProjectDefinitions_Designers_DesignerId",
                table: "ProjectDefinitions");

            migrationBuilder.DropIndex(
                name: "IX_ProjectDefinitions_ClientId",
                table: "ProjectDefinitions");

            migrationBuilder.DropIndex(
                name: "IX_ProjectDefinitions_DesignerId",
                table: "ProjectDefinitions");

            migrationBuilder.AddForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools",
                column: "DesignerId",
                principalTable: "Designers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
