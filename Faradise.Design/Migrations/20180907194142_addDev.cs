﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addDev : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectDevelopments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    CurrentStage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDevelopments_ProjectDefinitions_Id",
                        column: x => x.Id,
                        principalTable: "ProjectDefinitions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDevelopmentStageData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Stage = table.Column<int>(nullable: false),
                    DesignerIsReady = table.Column<bool>(nullable: false),
                    ClientIsReady = table.Column<bool>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    TotalLength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopmentStageData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDevelopmentStageData_ProjectDevelopments_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDevelopments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDevelopmentStageData_ProjectId",
                table: "ProjectDevelopmentStageData",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectDevelopmentStageData");

            migrationBuilder.DropTable(
                name: "ProjectDevelopments");
        }
    }
}
