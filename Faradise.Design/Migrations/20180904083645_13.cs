﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class _13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CollagePhotoArchiveUrl",
                table: "ProjectDescriptionRooms",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MoodboardPhotoArchiveUrl",
                table: "ProjectDescriptionRooms",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlanRoomId",
                table: "ProjectDescriptionRooms",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZonePhotoArchiveUrl",
                table: "ProjectDescriptionRooms",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProjectDevelopments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentStage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoomCollagePhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomCollagePhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomCollagePhotos_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomGoods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    PaidFor = table.Column<bool>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomGoods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomGoods_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomMoodboardPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomMoodboardPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomMoodboardPhotos_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomPlans",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Price = table.Column<int>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    ArchiveUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlans", x => x.RoomId);
                });

            migrationBuilder.CreateTable(
                name: "RoomVizualizations",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Price = table.Column<int>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    ArchiveUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomVizualizations", x => x.RoomId);
                });

            migrationBuilder.CreateTable(
                name: "RoomZonePhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomZonePhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomZonePhotos_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectDevelopmentStageData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    Stage = table.Column<int>(nullable: false),
                    DesignerIsReady = table.Column<bool>(nullable: false),
                    ClientIsReady = table.Column<bool>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    TotalLength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDevelopmentStageData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectDevelopmentStageData_ProjectDevelopments_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectDevelopments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomPlanPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlanId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomPlanPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomPlanPhotos_RoomPlans_PlanId",
                        column: x => x.PlanId,
                        principalTable: "RoomPlans",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomVizualizationPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VizualizationId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomVizualizationPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomVizualizationPhotos_RoomVizualizations_VizualizationId",
                        column: x => x.VizualizationId,
                        principalTable: "RoomVizualizations",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDescriptionRooms_PlanRoomId",
                table: "ProjectDescriptionRooms",
                column: "PlanRoomId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDevelopmentStageData_ProjectId",
                table: "ProjectDevelopmentStageData",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomCollagePhotos_RoomId",
                table: "RoomCollagePhotos",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomGoods_RoomId",
                table: "RoomGoods",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomMoodboardPhotos_RoomId",
                table: "RoomMoodboardPhotos",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomPlanPhotos_PlanId",
                table: "RoomPlanPhotos",
                column: "PlanId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomVizualizationPhotos_VizualizationId",
                table: "RoomVizualizationPhotos",
                column: "VizualizationId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomZonePhotos_RoomId",
                table: "RoomZonePhotos",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProjectDescriptionRooms_RoomPlans_PlanRoomId",
                table: "ProjectDescriptionRooms",
                column: "PlanRoomId",
                principalTable: "RoomPlans",
                principalColumn: "RoomId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProjectDescriptionRooms_RoomPlans_PlanRoomId",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropTable(
                name: "ProjectDevelopmentStageData");

            migrationBuilder.DropTable(
                name: "RoomCollagePhotos");

            migrationBuilder.DropTable(
                name: "RoomGoods");

            migrationBuilder.DropTable(
                name: "RoomMoodboardPhotos");

            migrationBuilder.DropTable(
                name: "RoomPlanPhotos");

            migrationBuilder.DropTable(
                name: "RoomVizualizationPhotos");

            migrationBuilder.DropTable(
                name: "RoomZonePhotos");

            migrationBuilder.DropTable(
                name: "ProjectDevelopments");

            migrationBuilder.DropTable(
                name: "RoomPlans");

            migrationBuilder.DropTable(
                name: "RoomVizualizations");

            migrationBuilder.DropIndex(
                name: "IX_ProjectDescriptionRooms_PlanRoomId",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropColumn(
                name: "CollagePhotoArchiveUrl",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropColumn(
                name: "MoodboardPhotoArchiveUrl",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropColumn(
                name: "PlanRoomId",
                table: "ProjectDescriptionRooms");

            migrationBuilder.DropColumn(
                name: "ZonePhotoArchiveUrl",
                table: "ProjectDescriptionRooms");
        }
    }
}
