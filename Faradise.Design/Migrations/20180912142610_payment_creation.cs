﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class payment_creation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "RoomVisualizations");

            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "RoomPlans");

            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "RoomVisualizations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "RoomPlans",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdempotenceKey = table.Column<string>(nullable: true),
                    YandexId = table.Column<string>(nullable: true),
                    RawJson = table.Column<string>(nullable: true),
                    PaymentRoute = table.Column<string>(nullable: true),
                    PaymentTarget = table.Column<int>(nullable: false),
                    Amount = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoomVisualizations_PaymentId",
                table: "RoomVisualizations",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomPlans_PaymentId",
                table: "RoomPlans",
                column: "PaymentId");

            migrationBuilder.AddForeignKey(
                name: "FK_RoomPlans_Payments_PaymentId",
                table: "RoomPlans",
                column: "PaymentId",
                principalTable: "Payments",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_RoomVisualizations_Payments_PaymentId",
                table: "RoomVisualizations",
                column: "PaymentId",
                principalTable: "Payments",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RoomPlans_Payments_PaymentId",
                table: "RoomPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_RoomVisualizations_Payments_PaymentId",
                table: "RoomVisualizations");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_RoomVisualizations_PaymentId",
                table: "RoomVisualizations");

            migrationBuilder.DropIndex(
                name: "IX_RoomPlans_PaymentId",
                table: "RoomPlans");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "RoomVisualizations");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "RoomPlans");

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "RoomVisualizations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "RoomPlans",
                nullable: false,
                defaultValue: false);
        }
    }
}
