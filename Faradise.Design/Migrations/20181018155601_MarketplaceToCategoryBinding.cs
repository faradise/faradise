﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceToCategoryBinding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales");

            migrationBuilder.AddColumn<int>(
                name: "MarketplaceId",
                table: "MarketPlaceCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MarketPlaceCategories_MarketplaceId",
                table: "MarketPlaceCategories",
                column: "MarketplaceId");

            migrationBuilder.AddForeignKey(
                name: "FK_MarketPlaceCategories_MarketplaceEntity_MarketplaceId",
                table: "MarketPlaceCategories",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales",
                column: "BrandId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MarketPlaceCategories_MarketplaceEntity_MarketplaceId",
                table: "MarketPlaceCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_MarketPlaceCategories_MarketplaceId",
                table: "MarketPlaceCategories");

            migrationBuilder.DropColumn(
                name: "MarketplaceId",
                table: "MarketPlaceCategories");

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales",
                column: "BrandId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
