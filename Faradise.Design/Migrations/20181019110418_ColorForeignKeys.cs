﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class ColorForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyColors_MarketplaceColors_MarketplaceColorId",
                table: "CompanyColors");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyColors_MarketplaceColors_MarketplaceColorId",
                table: "CompanyColors",
                column: "MarketplaceColorId",
                principalTable: "MarketplaceColors",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyColors_MarketplaceColors_MarketplaceColorId",
                table: "CompanyColors");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyColors_MarketplaceColors_MarketplaceColorId",
                table: "CompanyColors",
                column: "MarketplaceColorId",
                principalTable: "MarketplaceColors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
