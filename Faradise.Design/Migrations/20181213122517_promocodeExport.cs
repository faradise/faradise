﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class promocodeExport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "PromoCodes");

            migrationBuilder.RenameColumn(
                name: "Percents",
                table: "PromoCodes",
                newName: "ExportId");

            migrationBuilder.RenameColumn(
                name: "ExpireDate",
                table: "PromoCodes",
                newName: "UseDate");

            migrationBuilder.AddColumn<bool>(
                name: "Used",
                table: "PromoCodes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PromoCodeExports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Percents = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ExpireDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodeExports", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PromoCodes_ExportId",
                table: "PromoCodes",
                column: "ExportId");

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCodes_PromoCodeExports_ExportId",
                table: "PromoCodes",
                column: "ExportId",
                principalTable: "PromoCodeExports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PromoCodes_PromoCodeExports_ExportId",
                table: "PromoCodes");

            migrationBuilder.DropTable(
                name: "PromoCodeExports");

            migrationBuilder.DropIndex(
                name: "IX_PromoCodes_ExportId",
                table: "PromoCodes");

            migrationBuilder.DropColumn(
                name: "Used",
                table: "PromoCodes");

            migrationBuilder.RenameColumn(
                name: "UseDate",
                table: "PromoCodes",
                newName: "ExpireDate");

            migrationBuilder.RenameColumn(
                name: "ExportId",
                table: "PromoCodes",
                newName: "Percents");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "PromoCodes",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
