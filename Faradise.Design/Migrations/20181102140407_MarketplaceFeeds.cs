﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceFeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FeedGoogle",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FeedGoogleUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FeedYml",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FeedYmlUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FeedGoogle",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedGoogleUpdated",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedYml",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "FeedYmlUpdated",
                table: "MarketplaceEntity");
        }
    }
}
