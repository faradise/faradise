﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MarketplaceId",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MarketplaceId",
                table: "Companies",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MarketplaceEntity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MarketplaceEntity", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sales_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                unique: true,
                filter: "[MarketplaceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_MarketplaceId",
                table: "Companies",
                column: "MarketplaceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_MarketplaceEntity_MarketplaceId",
                table: "Companies",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales",
                column: "MarketplaceId",
                principalTable: "MarketplaceEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_MarketplaceEntity_MarketplaceId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketplaceEntity_MarketplaceId",
                table: "Sales");

            migrationBuilder.DropTable(
                name: "MarketplaceEntity");

            migrationBuilder.DropIndex(
                name: "IX_Sales_MarketplaceId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Companies_MarketplaceId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "MarketplaceId",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "MarketplaceId",
                table: "Companies");
        }
    }
}
