﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceCategoriesFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete from Products");
            migrationBuilder.Sql("delete from CompanyCategories");
            migrationBuilder.Sql("delete from MarketPlaceCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_MarketPlaceCategories_CategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CompanyId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyCategories",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_CompanyId",
                table: "CompanyCategories");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "CompanyCategories");

            migrationBuilder.AddColumn<int>(
                name: "CompanyCategoryFileId",
                table: "Products",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyCategories",
                table: "CompanyCategories",
                columns: new[] { "CompanyId", "CompanyCategoryId" });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CompanyId_CompanyCategoryId",
                table: "Products",
                columns: new[] { "CompanyId", "CompanyCategoryId" });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_CategoryId",
                table: "CompanyCategories",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools",
                column: "DesignerId",
                principalTable: "Designers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products",
                columns: new[] { "CompanyId", "CompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_CompanyId_CompanyCategoryId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyCategories",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_CategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropColumn(
                name: "CompanyCategoryFileId",
                table: "Products");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "CompanyCategories",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyCategories",
                table: "CompanyCategories",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CompanyId",
                table: "Products",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_CompanyId",
                table: "CompanyCategories",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_DesignerPools_Designers_DesignerId",
                table: "DesignerPools",
                column: "DesignerId",
                principalTable: "Designers",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_MarketPlaceCategories_CategoryId",
                table: "Products",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
