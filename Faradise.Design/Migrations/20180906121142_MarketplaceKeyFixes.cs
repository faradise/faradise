﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class MarketplaceKeyFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Companies_CompanyId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products",
                columns: new[] { "CompanyId", "CompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_MarketPlaceCategories_CategoryId",
                table: "CompanyCategories",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Companies_CompanyId",
                table: "Products",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_CompanyCategories_CompanyId_CompanyCategoryId",
                table: "Products",
                columns: new[] { "CompanyId", "CompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
