﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class new360 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArchiveUrl",
                table: "RoomVisualizations",
                newName: "VisualizationCode");

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "RoomGoods",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "RoomGoods");

            migrationBuilder.RenameColumn(
                name: "VisualizationCode",
                table: "RoomVisualizations",
                newName: "ArchiveUrl");
        }
    }
}
