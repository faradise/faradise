﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class sales_and_badges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales");

            migrationBuilder.DropForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_BrandId",
                table: "Sales");

            migrationBuilder.DropIndex(
                name: "IX_Sales_CategoryId",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "BrandId",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Sales");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Sales",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateJson",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Updated",
                table: "Sales",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateTable(
                name: "Badges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    UpdateJson = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    MarketplaceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Badges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Badges_MarketplaceEntity_MarketplaceId",
                        column: x => x.MarketplaceId,
                        principalTable: "MarketplaceEntity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "SaleToCategories",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToCategories", x => new { x.SaleId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_SaleToCategories_MarketPlaceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MarketPlaceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToCategories_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleToCompanies",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToCompanies", x => new { x.SaleId, x.CompanyId });
                    table.ForeignKey(
                        name: "FK_SaleToCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToCompanies_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SaleToProducts",
                columns: table => new
                {
                    SaleId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleToProducts", x => new { x.SaleId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_SaleToProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SaleToProducts_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BadgeToCategories",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToCategories", x => new { x.BadgeId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_BadgeToCategories_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToCategories_MarketPlaceCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "MarketPlaceCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BadgeToCompanies",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToCompanies", x => new { x.BadgeId, x.CompanyId });
                    table.ForeignKey(
                        name: "FK_BadgeToCompanies_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToCompanies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BadgeToProducts",
                columns: table => new
                {
                    BadgeId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadgeToProducts", x => new { x.BadgeId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_BadgeToProducts_Badges_BadgeId",
                        column: x => x.BadgeId,
                        principalTable: "Badges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BadgeToProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Badges_MarketplaceId",
                table: "Badges",
                column: "MarketplaceId",
                unique: true,
                filter: "[MarketplaceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToCategories_CategoryId",
                table: "BadgeToCategories",
                column: "CategoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToCompanies_CompanyId",
                table: "BadgeToCompanies",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BadgeToProducts_ProductId",
                table: "BadgeToProducts",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToCategories_CategoryId",
                table: "SaleToCategories",
                column: "CategoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToCompanies_CompanyId",
                table: "SaleToCompanies",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SaleToProducts_ProductId",
                table: "SaleToProducts",
                column: "ProductId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BadgeToCategories");

            migrationBuilder.DropTable(
                name: "BadgeToCompanies");

            migrationBuilder.DropTable(
                name: "BadgeToProducts");

            migrationBuilder.DropTable(
                name: "SaleToCategories");

            migrationBuilder.DropTable(
                name: "SaleToCompanies");

            migrationBuilder.DropTable(
                name: "SaleToProducts");

            migrationBuilder.DropTable(
                name: "Badges");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "UpdateJson",
                table: "Sales");

            migrationBuilder.DropColumn(
                name: "Updated",
                table: "Sales");

            migrationBuilder.AddColumn<int>(
                name: "BrandId",
                table: "Sales",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Sales",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sales_BrandId",
                table: "Sales",
                column: "BrandId",
                unique: true,
                filter: "[BrandId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_CategoryId",
                table: "Sales",
                column: "CategoryId",
                unique: true,
                filter: "[CategoryId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_Companies_BrandId",
                table: "Sales",
                column: "BrandId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Sales_MarketPlaceCategories_CategoryId",
                table: "Sales",
                column: "CategoryId",
                principalTable: "MarketPlaceCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
