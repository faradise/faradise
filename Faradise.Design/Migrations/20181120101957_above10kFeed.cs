﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class above10kFeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Above10kFeed",
                table: "MarketplaceEntity",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Above10kFeedUpdated",
                table: "MarketplaceEntity",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Above10kFeed",
                table: "MarketplaceEntity");

            migrationBuilder.DropColumn(
                name: "Above10kFeedUpdated",
                table: "MarketplaceEntity");
        }
    }
}
