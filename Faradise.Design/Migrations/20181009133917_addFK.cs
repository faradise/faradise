﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentCompanyId",
                table: "CompanyCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "ParentCompanyId", "ParentCompanyCategoryId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories",
                columns: new[] { "ParentCompanyId", "ParentCompanyCategoryId" },
                principalTable: "CompanyCategories",
                principalColumns: new[] { "CompanyId", "CompanyCategoryId" },
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyCategories_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropIndex(
                name: "IX_CompanyCategories_ParentCompanyId_ParentCompanyCategoryId",
                table: "CompanyCategories");

            migrationBuilder.DropColumn(
                name: "ParentCompanyId",
                table: "CompanyCategories");
        }
    }
}
