﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class promocode_add_params : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PromoCode",
                table: "MarketplaceOrders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PromoCodeDiscount",
                table: "MarketplaceOrderProducts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PromoCode",
                table: "MarketplaceOrders");

            migrationBuilder.DropColumn(
                name: "PromoCodeDiscount",
                table: "MarketplaceOrderProducts");
        }
    }
}
