﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class addArchived : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Archived",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Archived",
                table: "ProjectDefinitions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Archived",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Archived",
                table: "ProjectDefinitions");
        }
    }
}
