﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class ARProducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ARProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FaradiseProductId = table.Column<int>(nullable: false),
                    FaradiseColorId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    VendorCode = table.Column<string>(nullable: true),
                    Vertical = table.Column<bool>(nullable: false),
                    Render = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ARProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ARProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "ARModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ARProductId = table.Column<int>(nullable: false),
                    Platform = table.Column<int>(nullable: false),
                    Format = table.Column<int>(nullable: false),
                    Model = table.Column<string>(nullable: true),
                    Diffuse = table.Column<string>(nullable: true),
                    Normal = table.Column<string>(nullable: true),
                    Metallic = table.Column<string>(nullable: true),
                    Roughness = table.Column<string>(nullable: true),
                    MetallicRoughness = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ARModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ARModels_ARProducts_ARProductId",
                        column: x => x.ARProductId,
                        principalTable: "ARProducts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ARModels_ARProductId",
                table: "ARModels",
                column: "ARProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ARProducts_ProductId",
                table: "ARProducts",
                column: "ProductId",
                unique: true,
                filter: "[ProductId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ARModels");

            migrationBuilder.DropTable(
                name: "ARProducts");
        }
    }
}
