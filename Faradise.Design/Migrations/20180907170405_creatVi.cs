﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class creatVi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "RoomGoods");


            migrationBuilder.CreateTable(
                name: "RoomVisualizations",
                columns: table => new
                {
                    RoomId = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    ArchiveUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomVisualizations", x => x.RoomId);
                    table.ForeignKey(
                        name: "FK_RoomVisualizations_ProjectDescriptionRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "ProjectDescriptionRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoomVisualizationPhotos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VisualizationId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoomVisualizationPhotos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoomVisualizationPhotos_RoomVisualizations_VisualizationId",
                        column: x => x.VisualizationId,
                        principalTable: "RoomVisualizations",
                        principalColumn: "RoomId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RoomVisualizationPhotos_VisualizationId",
                table: "RoomVisualizationPhotos",
                column: "VisualizationId");

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropTable(
                name: "RoomVisualizationPhotos");

            migrationBuilder.DropTable(
                name: "RoomVisualizations");

            

            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "RoomGoods",
                nullable: false,
                defaultValue: 0);
        }
    }
}
