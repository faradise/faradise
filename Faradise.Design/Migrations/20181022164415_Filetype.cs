﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Faradise.Design.Migrations
{
    public partial class Filetype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MarketplaceColorId",
                table: "ProductColors");

            migrationBuilder.AddColumn<int>(
                name: "Filetype",
                table: "YmlUpdates",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Filetype",
                table: "YmlUpdates");

            migrationBuilder.AddColumn<int>(
                name: "MarketplaceColorId",
                table: "ProductColors",
                nullable: true);
        }
    }
}
