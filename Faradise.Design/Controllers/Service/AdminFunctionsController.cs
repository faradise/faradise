﻿using System.Linq;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{

    /// <summary>
    /// Запросы в этом класе являются временными заглушками под функции администратора. Скорее всего весь контроллер будет удален в будущем.
    /// </summary>

    [Route("admin")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AdminFunctionsController : APIController
    {
        private readonly IDesignerPoolService _poolService = null;
        private readonly IDesignerService _designerService = null;
        private readonly IUserService _userService = null;

        public AdminFunctionsController(IDesignerPoolService poolService, IDesignerService designerService, IUserService userService)
        {
            _poolService = poolService;
            _designerService = designerService;
            _userService = userService;
        }

        /// <summary>
        /// Получить дизайнеров
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet("users/get-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerDatingModel[] GetDesigners(int offset, int count)
        {
            var designers = _designerService.GetDesigners(offset, count);
            if (designers == null)
                return new DesignerDatingModel[0];
            return designers.Select(x =>
                new DesignerDatingModel()
                {
                    Id = x.Id,
                    Description = x.Base.About,
                    Name = x.Base.Name + " " + x.Base.Surname,
                    PortfolioLink = x.PortfolioLink,
                    AvatarUrl = x.PhotoLink,
                    Age = x.Base.Age,
                    City = x.Base.City,
                    Gender = x.Base.Gender,
                    PersonalСontrol = x.Possibilities.PersonalСontrol
                }).ToArray();
        }

        /// <summary>
        /// Добавить дизайнера в пул для проекта
        /// </summary>
        [HttpPost("project/add-designer-to-pool")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddDesignerToPool([FromBody] DesignerSelectModel model)
        {
            var designer = _userService.ExtractUserById(model.DesignerId);
            if (designer == null)
                throw new ServiceException($"Unable to found designer with id {model.DesignerId}", "user_not_found");
            _poolService.AddDesignerToPool(model.ProjectId, model.DesignerId);
        }

        /// <summary>
        /// Удалить дизайнера из пула проекта
        /// </summary>
        /// <param name="designer"></param>
        [HttpPost("project/remove-designer-from-pool")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RemoveDesignerFromPool([FromBody] DesignerSelectModel designer)
        {
            _poolService.RemoveDesignerFromPool(designer.ProjectId, designer.DesignerId);
        }

        /// <summary>
        /// Отметить пул как готовый и отослать уведомление об этом клиенту
        /// </summary>
        /// <param name="projectId"></param>
        [HttpPost("project/finish-designer-pool")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void FinishDesignerPool([FromBody] int projectId)
        {
            _poolService.SetPoolIsFinished(projectId);
        }

        /// <summary>
        /// Получить текущий пул дизайнеров для проекта
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("project/get-designer-pool")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerDatingModel[] GetPoolForProject(int projectId)
        {
            var pool = _poolService.GetDesignersPool(projectId);
            return pool != null ? pool.Select(x => x.ToModel()).ToArray() : new DesignerDatingModel[0];
        }

        /// <summary>
        /// Подтвердить выполнение тестового задания
        /// </summary>
        /// <param name="designerId">Id дизайнера</param>
        /// <returns></returns>
        [HttpGet("designer/setTestApproved")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetTestApproved(int designerId)
        {
            _designerService.SetTestApproved(designerId, true);
            return true;
        }
    }
}
