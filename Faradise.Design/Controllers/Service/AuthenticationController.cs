﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Faradise.Design.Controllers.Service.Models.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.Service
{
    [Route("auth")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AuthenticationController : Controller
    {
        private const string ADMIN_LOGIN = "faradise";
        private const string ADMIN_PASSWORD = "F31NWp121991";

        [HttpGet]
        public IActionResult Login(string to)
        {
            return View("Login", new AuthenticationModel { To = to });
        }

        [HttpPost]
        public async Task<IActionResult> Login(AuthenticationModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Name == ADMIN_LOGIN && model.Password == ADMIN_PASSWORD)
                {
                    await Authenticate(model.Name, model.Password); // аутентификация

                    switch (model.To)
                    {
                        case "swagger":
                            return Redirect("/swagger/");
                        case "admin":
                            return Redirect("/admin/home/");
                        default: return Redirect("/");
                    }
                }
                else ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        private async Task Authenticate(string name, string password)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, name)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}