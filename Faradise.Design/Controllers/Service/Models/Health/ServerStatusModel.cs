﻿using System;

namespace Faradise.Design.Controllers.Service.Models.Health
{
    public class ServerStatusModel
    {
        public string Environment { get; set; }
        public TimeSpan Uptime { get; set; }
    }
}
