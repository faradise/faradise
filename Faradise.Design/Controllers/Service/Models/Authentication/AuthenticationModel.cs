﻿namespace Faradise.Design.Controllers.Service.Models.Authentication
{
    public class AuthenticationModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string To { get; set; }
    }
}
