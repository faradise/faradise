﻿using Faradise.Design.Controllers.Service.Models.Health;
using Faradise.Design.Internal;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace Faradise.Design.Controllers.Service
{
    [Route("/service")]
    public class HealthController : ServiceController
    {
        private IConfiguration _configuration = null;
        private IPersistentStorageService _persistent = null;
        private ICacheService _cache = null;

        public HealthController(IConfiguration configuration, IPersistentStorageService storage, ICacheService cache)
        {
            _configuration = configuration;
            _persistent = storage;
            _cache = cache;
        }

        /// <summary>
        /// Check service health state
        /// </summary>
        /// <returns>true</returns>
        [HttpGet("healthcheck")]
        public bool HealthCheck()
        {
            return true;
        }

        /// <summary>
        /// Returns basic server information
        /// </summary>
        [HttpGet("status")]
        public JsonResult Status()
        {
            var status = new ServerStatusModel
            {
                Environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"),
                Uptime = DateTime.Now - Startup.TimeStarted
            };

            return new JsonResult(status);
        }

        [HttpGet("exception")]
        public bool Exception()
        {
            throw new Exception("EXCEPTION");
        }

        [HttpGet("azure-availability")]
        public bool AzureSearch()
        {
            return _persistent.AzureSearchAvailable ?? false;
        }

        [HttpGet("drop-cache")]
        public bool DropCache()
        {
            _cache.Drop();

            return true;
        }
    }
}