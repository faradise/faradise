﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services.Background;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/feed")]
    public class AdminFeedController : APIController
    {
        private FeedsBackgroundService _feedsBackgroundService = null;
        public AdminFeedController(FeedsBackgroundService feedsBackgroundService)
        {
            _feedsBackgroundService = feedsBackgroundService;
        }

        [HttpPost("get-feed-by-ids")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public async Task<byte[]> GetFeedByProductIds([FromBody] int[] productId)
        {
            return await _feedsBackgroundService.GetFeedByProductIds(productId);
        }
    }
}