﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Filters;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Models.ProjectPreparations;
using Faradise.Design.Internal;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Controllers.API.Models;
using Faradise.Design.Controllers.API.Models.Shared;

namespace Faradise.Design.Controllers
{
    [Route("api/project/preparations")]
    public class ProjectPreparationsController : APIController
    {
        private readonly IProjectPreparationsService _preparationsService = null;
        private readonly ICDNService _cdnService = null;
        private readonly IProjectDefinitionService _defService = null;

        public ProjectPreparationsController(IProjectPreparationsService clientDesignerService, ICDNService cdnService, IProjectDefinitionService defService)
        {
            _preparationsService = clientDesignerService;
            _cdnService = cdnService;
            _defService = defService;
        }

        /// <summary>
        /// Отказаться от дизайнера
        /// </summary>
        [HttpPost("abandon-designer")]
        [APIAuthorize(Roles = Role.Client)]
        public void AbandonDesigner([FromBody] AbandonDesignerModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _defService.AbandonDesigner(model.ProjectId, model.Description);
        }

        /// <summary>
        /// Утвердить дизанера на проект
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("confirm-designer")]
        [APIAuthorize(Roles = Role.Client)]
        public void ConfirmDesigner([FromBody] ConfirmDesignerModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _preparationsService.ConfirmDesigner(model.ProjectId);
            _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.Meeting, Role.Client, true);
        }


        /// <summary>
        /// Загрузить фото комнаты
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("upload-room-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<int> UploadRoomPhoto([FromForm] UploadRoomPhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.PhotoAndProportions))
                throw new ServiceException($"This action available only in {CooperateStage.PhotoAndProportions} stage", "action_wrong_stage");
            var cdnUrl = await _cdnService.UploadData(model.FileUpload, _cdnService.GetPathForRoomPhoto(model.ProjectId));
            if (string.IsNullOrEmpty(cdnUrl))
                throw new ServiceException($"File upload error", "file_upload_error");
            return _preparationsService.AddRoomPhoto(model.RoomId, cdnUrl);
        }

        /// <summary>
        /// Загрузить план комнаты
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("upload-room-plan")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task UploadRoomPlan([FromForm] UploadRoomPhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.PhotoAndProportions))
                throw new ServiceException($"This action available only in {CooperateStage.PhotoAndProportions} stage", "action_wrong_stage");
            var cdnUrl = await _cdnService.UploadData(model.FileUpload, _cdnService.GetPathForRoomPlan(model.ProjectId));
            if (string.IsNullOrEmpty(cdnUrl))
                throw new ServiceException($"File upload error", "file_upload_error");
            _preparationsService.SetPlan(model.RoomId, cdnUrl);
        }

        /// <summary>
        /// Загрузить фото старой мебели
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("upload-old-furniture-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<int> UploadOldFurniture([FromForm] UploadOldFurniturePhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.WhatToSave))
                throw new ServiceException($"This action available only in {CooperateStage.WhatToSave} stage", "action_wrong_stage");
            var cdnUrl = await _cdnService.UploadData(model.File, _cdnService.GetPathForOldFurniturePhoto(model.ProjectId));
            if (string.IsNullOrEmpty(cdnUrl))
                throw new ServiceException($"File upload error", "file_upload_error");
            return _preparationsService.AddOldFurniturePhoto(model.ProjectId, cdnUrl);
        }

        /// <summary>
        /// Загрузить фото старой отделки
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("upload-old-furnish-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<int> UploadOldFurnish([FromForm] UploadOldFurniturePhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.WhatToSave))
                throw new ServiceException($"This action available only in {CooperateStage.WhatToSave} stage", "action_wrong_stage");
            var cdnUrl = await _cdnService.UploadData(model.File, _cdnService.GetPathForOldFurnishPhoto(model.ProjectId));
            if (string.IsNullOrEmpty(cdnUrl))
                throw new ServiceException($"File upload error", "file_upload_error");
            return _preparationsService.AddOldFurnishPhoto(model.ProjectId, cdnUrl);
        }

        /// <summary>
        /// Удалить фото комнаты
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("delete-room-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteRoomPhoto([FromBody] DeleteRoomPhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.PhotoAndProportions))
                throw new ServiceException($"This action available only in {CooperateStage.PhotoAndProportions} stage", "action_wrong_stage");
            var photo = _preparationsService.GetRooms(model.ProjectId).SelectMany(x => x.Photos).FirstOrDefault(x => x.PhotoId == model.PhotoId);
            if (photo == null)
                throw new ServiceException($"Photo not found Id: {model.PhotoId}", "photo_not_found");
            //_cdnService.DeleteFileAtPathAsync(photo.Link);
            _preparationsService.DeleteRoomPhoto(model.PhotoId);
        }

        /// <summary>
        /// Удалить план комнаты
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("delete-room-plan")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteRoomPlan([FromBody] DeleteRoomPlanModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.PhotoAndProportions))
                throw new ServiceException($"This action available only in {CooperateStage.PhotoAndProportions} stage", "action_wrong_stage");
            var room = _preparationsService.GetRooms(model.ProjectId).FirstOrDefault(x => x.RoomId == model.RoomId);
            if (room == null)
                throw new ServiceException($"Unable to found roomId Id: {model.RoomId}", "room_not_found");
            if (string.IsNullOrEmpty(room.Plan))
                throw new ServiceException($"Plan not found foor roomId Id: {model.RoomId}", "plan_not_found");
            //_cdnService.DeleteFileAtPathAsync(room.Plan);
            _preparationsService.DeletePlan(model.RoomId);
        }

        /// <summary>
        /// Удалить фото старой мебели
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("delete-old-furniture-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteOldFurniture([FromBody] DeleteOldStuffPhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.WhatToSave))
                throw new ServiceException($"This action available only in {CooperateStage.WhatToSave} stage", "action_wrong_stage");
            var oldStuff = _preparationsService.GetOldFurniture(model.ProjectId);
            if (oldStuff == null || oldStuff.FurniturePhotos == null)
                throw new ServiceException($"Old stuff not registered for peoject id {model.ProjectId}", "old_furniture_not_found");
            var photo = oldStuff.FurniturePhotos.FirstOrDefault(x => x.PhotoId == model.PhotoId);
            if (photo == null)
                throw new ServiceException($"Photo not found Id: {model.PhotoId}", "photo_not_found");
            //_cdnService.DeleteFileAtPathAsync(photo.Link);
            _preparationsService.DeleteOldFurniturePhoto(model.PhotoId);
        }

        /// <summary>
        /// Удалить фото старой отделки
        /// </summary>
        /// <param name="model"></param>
        // Вероятно, аплод всякого нужно вынести куда-то, аплодят не только из этого контроллера
        [HttpPost("delete-old-furnish-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteOldFurnish([FromBody] DeleteOldStuffPhotoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.WhatToSave))
                throw new ServiceException($"This action available only in {CooperateStage.WhatToSave} stage", "action_wrong_stage");
            var oldStuff = _preparationsService.GetOldFurniture(model.ProjectId);
            if (oldStuff == null || oldStuff.FurnishPhotos == null)
                throw new ServiceException($"Old stuff not registered for peoject id {model.ProjectId}", "old_furnish_not_found");
            var photo = oldStuff.FurnishPhotos.FirstOrDefault(x => x.PhotoId == model.PhotoId);
            if (photo == null)
                throw new ServiceException($"Photo not found Id: {model.PhotoId}", "photo_not_found");
           // _cdnService.DeleteFileAtPathAsync(photo.Link);
            _preparationsService.DeleteOldFurnishPhoto(model.PhotoId);
        }

        /// <summary>
        /// Отправить данные о комнатах (размеры)
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-room-information")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void UploadRooms([FromBody] UploadRoomsModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.PhotoAndProportions))
                throw new ServiceException($"This action available only in {CooperateStage.PhotoAndProportions} stage", "action_wrong_stage");
            var roomInfos = model.RoomsInformation != null ? model.RoomsInformation.Select(x => x.ToService()).ToList() : new List<ShortRoomInfo>();
            _preparationsService.SetInfoAboutProjectRooms(model.ProjectId, roomInfos);
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.PhotoAndProportions, Role.Client, true);
        }

        /// <summary>
        /// Отправить стили проекта
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-favorite-styles")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetFavoriteStyles([FromBody] SetDesignStylesModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.Style))
                throw new ServiceException($"This action available only in {CooperateStage.Style} stage", "action_wrong_stage");
            var styles = new Dictionary<ProjectStyle, string>(model.Styles.Count);
            foreach (var s in model.Styles)
                styles.Add(s.Name, s.Description);
            _preparationsService.SetInfoAboutFavoriteStyles(model.ProjectId, styles);
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.Style, Role.Client, true);
        }

        /// <summary>
        /// Отправить дополнительную информацию о комнатах
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-information")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetAdditionalInformation([FromBody] SetAdditionalInformationModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.AdditionalInfo))
                throw new ServiceException($"This action available only in {CooperateStage.AdditionalInfo} stage", "action_wrong_stage");
            var addInfos = model.Information != null ? model.Information.Select(x => x.ToService()).ToList() : new List<AdditionalInformationForRoom>();
            _preparationsService.SetAdditionalInformation(model.ProjectId, addInfos);
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.AdditionalInfo, Role.Client, true);
        }

        /// <summary>
        /// Отправить бюджет
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-budget")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetBudget([FromBody] ClientDesignerBudgetModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.Budget))
                throw new ServiceException($"This action available only in {CooperateStage.Budget} stage", "action_wrong_stage");
            _preparationsService.SetBudgetForProject(model.ProjectId, model.FurnitureBudget, model.RenovationBudget);
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.Budget, Role.Client, true);
        }

        /// <summary>
        /// Отправить подтверждение того, что все фото старого барахла залиты
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-old-stuff-ready")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetOldStuffReady([FromBody] OldStuffReadyModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (!_preparationsService.CheckIfStageIsCurrent(model.ProjectId, CooperateStage.WhatToSave))
                throw new ServiceException($"This action available only in {CooperateStage.WhatToSave} stage", "action_wrong_stage");
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SetStageStatus(model.ProjectId, CooperateStage.WhatToSave, Role.Client, true);
        }

        /// <summary>
        /// Пропустить стадию
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("skip-stage")]
        [APIAuthorize(Roles = Role.Client)]
        public void SkipStage([FromBody] SkipPreparationsStageModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (user.Role.ContainsRole(Role.Client))
                _preparationsService.SkipStage(model.ProjectId, model.Stage);
        }

        /// <summary>
        /// Получить полные данные о комнатах
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-room-information")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<RoomInfoModel> GetRooms(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _preparationsService.GetRooms(projectId);
            return rooms != null ? rooms.Select(x => x.ToModel()).ToList() : new List<RoomInfoModel>();
        }

        /// <summary>
        /// Получить установленные сейчас стили
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-styles")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<ClientDesignerStyleModel> GetStyles(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var styles = _preparationsService.GetStyles(projectId);
            return styles != null ? styles.Select(x => new ClientDesignerStyleModel() { Name = x.Key, Description = x.Value}).ToList() : new List<ClientDesignerStyleModel>();
        }

        /// <summary>
        /// Получить установленный сейчас бюджет
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-budget")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ClientDesignerBudgetModel GetBudget(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var budget = _preparationsService.GetBudget(projectId);
            return budget.ToModel();
        }

        /// <summary>
        /// Получить установленную сейчас дополнительную информацию
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-additional-info")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<AdditionalInformationForRoomModel> GetAdditionalInfo(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var infos = _preparationsService.GetRoomAdditionalInfos(projectId);
            return infos != null ? infos.Select(x => x.ToModel()).ToList() : new List<AdditionalInformationForRoomModel>();
        }

        /// <summary>
        /// Получить данные о установленном старом барахле (фото мебели и отделки)
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-old-furtinture")]
        [APIAuthorize(Roles = Role.Authorized)]
        public OldFurnitureModel GetOldFurniture(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var oldStuff = _preparationsService.GetOldFurniture(projectId);
            return oldStuff != null ? oldStuff.ToModel() : null;
        }

        /// <summary>
        /// Получить текущий этап проекта
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-current-stage")]
        [APIAuthorize(Roles = Role.Authorized)]
        public StageStatusModel GetCurrentStage(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var stage = _preparationsService.GetCurrentStage(projectId);
            return new StageStatusModel() { Stage = stage.Stage, ClientIsReady = stage.ClientIsReady };
        }

        /// <summary>
        /// Подтвердить дизайнером переход на следующий этап
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("confirm-stage")]
        [APIAuthorize(Roles = Role.Designer)]
        public bool ConfirmStage([FromBody] ConfirmPreparationStageModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _preparationsService.SetStageStatus(model.ProjectId, model.Stage, Role.Designer, true);
            return true;
        }

        /// <summary>
        /// Позвать админа в проект
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("add-admin")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddAdmin([FromBody] AddAdminToProjectModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _defService.SetProjectNeedAdminAttention(model.ProjectId, true);
            //TODO: добавить админа в чат?
        }
    }
}