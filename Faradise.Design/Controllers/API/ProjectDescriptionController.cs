﻿using Microsoft.AspNetCore.Mvc;
using Faradise.Design.Filters;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Controllers.API.DataMappers;
using System.Linq;
using System.Collections.Generic;
using Faradise.Design.Internal;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Models.DesignerPool;
using System;

namespace Faradise.Design.Controllers.API
{
    [Route("api/")]
    public class ProjectDescriptionController : APIController
    {
        private readonly IClientProjectService _descriptionsService = null;
        private readonly IUserService _userService = null;
        private readonly IProjectDefinitionService _defService = null;
        private readonly IDesignerPoolService _poolService = null;
        private readonly IProjectPreparationsService _projectPreparationsService = null;
        private readonly IDesignerService _designerService = null;

        public ProjectDescriptionController(IUserService userService, IClientProjectService roomService, IProjectDefinitionService defService, IDesignerPoolService poolService, IProjectPreparationsService projectPreparationsService, IDesignerService designerService)
        {
            _descriptionsService = roomService;
            _userService = userService;
            _defService = defService;
            _poolService = poolService;
            _projectPreparationsService = projectPreparationsService;
            _designerService = designerService;
        }

        /// <summary>
        /// Получить проект по его Id
        /// </summary>
        /// <param name="projectId">Id проекта</param>
        [HttpGet("client/project/get")]
        [APIAuthorize(Roles = Role.Client)]
        public ProjectDescriptionModel Get([FromQuery] int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var description = _descriptionsService.GetProjectDescription(projectId);
            if (description == null)
                throw new ServiceException($"Description with id {projectId} not found", "project_not_found");
            var model = description.ToModel();
            var project = _defService.GetProjectDefinition(projectId);
            if (project == null)
                throw new ServiceException($"Project with id {projectId} not found", "project_not_found");
            model.Name = project.Name;
            model.City = project.City;
            model.Stage = project.Stage;
            model.Status = project.Status;
            model.FinishFill = project.ProjectFill;
            model.SecondsLeftToFillDesignerPool = project.ProjectFillDate.HasValue ? (int) Math.Min(0, (DateTime.Now - project.ProjectFillDate).Value.TotalSeconds) : 0;
            return model;
        }


        /// <summary>
        /// Получить все проекты пользователя
        /// </summary>
        [HttpGet("client/project/list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProjectListModel GetAll()
        {
            var user = (User)HttpContext.Items[typeof(User)];
            var projects = _defService.GetProjectsByUser(user.Id);
            if (projects == null)
                return new ProjectListModel() { Projects = new ProjectShortModel[0] };

            return new ProjectListModel()
            {
                Projects = projects.Select(x => new ProjectShortModel { Id = x.Id, Name = x.Name, Stage = x.Stage, Status = x.Status }).ToArray()
            };
        }

        /// <summary>
        /// Создать новый проект
        /// </summary>
        [HttpPost("client/project/create")]
        [APIAuthorize(Roles = Role.Client)]
        public int Create()
        {
            var user = (User)HttpContext.Items[typeof(User)];
            int projectId = _defService.CreateProjectDefinition(user.Id);
            _descriptionsService.CreateProjectDescription(projectId);
            return projectId;
        }

        /// <summary>
        /// Отправить информацию
        /// </summary>
        /// <param name="model">имя и город проекта</param>
        /// <returns>Успешность операции</returns>
        [HttpPost("client/project/setBasicInfo")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetBasicModel([FromBody] SetBasicModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.Id, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");

            if (!string.IsNullOrEmpty(model.UserName))
                _userService.AddUserName(user.Id, model.UserName);

            if (!string.IsNullOrEmpty(model.UserPhone))
                _userService.AddUserPhone(user.Id, model.UserPhone);

            _defService.SetProjectName(model.Id, model.ProjectName);
            if (!string.IsNullOrEmpty(model.City))
                _defService.SetProjectCity(model.Id, model.City);
        }

        /// <summary>
        /// Отправить информацию о проекте
        /// </summary>
        /// <param name="model">информация о проекте</param>
        [HttpPost("client/project/setAdditionalInfo")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetInfo([FromBody] SetInfoModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.Id, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = new Dictionary<Room, int>(model.Rooms.Length);
            foreach (var r in model.Rooms)
                rooms.Add(r.Purpose, r.Count);
            var reasons = new ReasonBinding[model.Reasons.Length];
            for (int r = 0; r < reasons.Length; r++)
                reasons[r] = new ReasonBinding() { Name = model.Reasons[r].Name, Description = model.Reasons[r].Description };
            _descriptionsService.SetProjectInformation(model.Id, model.ObjectType, rooms, reasons);
        }

        /// <summary>
        /// Отправить список стилей для проекта
        /// </summary>
        /// <param name="model">список стилей</param>
        /// <returns>Успешность операции</returns>
        [HttpPost("client/project/setStyles")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetStyles([FromBody] SetStylesModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.Id, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var styles = model.Styles.Select(x => new StyleBinding() { Name = x.Name, Description = x.Description }).ToArray();
            _descriptionsService.SetProjectStyles(model.Id, styles);
        }

        /// <summary>
        /// Отправить и установить данные о бюджете
        /// </summary>
        /// <param name="model">бюджет</param>
        /// <returns>Успешность операции</returns>
        [HttpPost("client/project/setBudget")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetBudget([FromBody] SetBudgetModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.Id, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _descriptionsService.SetProjectBudget(model.Id, model.Budget.ToService());
        }

        /// <summary>
        /// Отправить список требований к дизайнеру
        /// </summary>
        /// <param name="model">список требований</param>
        /// <returns>Успешность операции</returns>
        [HttpPost("client/project/setDesignerRequirements")]
        [APIAuthorize(Roles = Role.Client)]
        public void SetDesignerRequirements([FromBody] DesignerRequirementsModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.Id, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
             if(!_descriptionsService.ValidationTest(model.Testing))
                throw new ServiceException($"Test incorrect", "test_incorrect");
            _descriptionsService.SetDisignerRequirements(model.Id, model.ToService());
        }

        /// <summary>
        /// Получить весь тест по дизайну 
        /// </summary>
        /// <returns>Список тестовых вопросов и ответов</returns>
        [HttpGet("client/project/getQuestions")]
        [APIAuthorize(Roles = Role.Authorized | Role.Client)]
        public TestQuestionDescriptionModel[] GetQuestions()
        {
            var model =  _descriptionsService.GetDesignerQuestions().Select(x => x.ToModel()).ToArray();
            return model;
        }

        /// <summary>
        /// Получить подборку дизайнеров
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("client/project/get-designers")]
        [APIAuthorize(Roles = Role.Client)]
        public DesignerDatingModel[] GetPreselectedDesigners(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            return _poolService.GetDesignersPool(projectId).Select(x => x.ToModel()).ToArray();
        }
        
        /// <summary>
        /// Удалить дизайнеров из подборки
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Сколько дизайнеров осталось на выбор</returns>
        [HttpPost("client/project/remove-designers")]
        [APIAuthorize(Roles = Role.Client)]
        public int RemoveDesigners([FromBody] RemoveDesignersModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _poolService.RemoveDesignersFromPool(model.ProjectId, model.DesignersToRemove.Select(x => new RemoveDesigner() { DesignerId = x.DesignerId, Description = x.Description }).ToArray());
            return _poolService.GetDesignersCount(model.ProjectId);
        }

        /// <summary>
        /// Выбрать дизайнера на проект по его id
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("client/project/select-designer")]
        [APIAuthorize(Roles = Role.Client)]
        public void SelectDesigner([FromBody] DesignerSelectModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var designer = _userService.ExtractUserById(model.DesignerId);
            if (designer == null)
                throw new ServiceException($"Unable to found designer with id {model.DesignerId}", "user_not_found");
            _defService.AssignDesigner(model.ProjectId, model.DesignerId);
            _projectPreparationsService.StartStagePreparations(model.ProjectId);
        }

        /// <summary>
        ///Отметить проект как заполненный пользователем
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("client/project/finish-filling")]
        [APIAuthorize(Roles = Role.Client)]
        public void FinishProjectFill([FromBody] FinishModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _defService.FinishFill(model.ProjectId, user);
        }


        /// <summary>
        ///Получить краткую информацию о проекте
        /// </summary>
        [HttpGet("client/project/get-short-description")]
        [APIAuthorize(Roles = Role.Designer)]
        public ShortProjectInfoModel ProjectShortDescription([FromQuery] int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var description = _descriptionsService.GetProjectDescription(projectId);
            if (description == null)
                throw new ServiceException($"Description with id {projectId} not found", "project_not_found");
            var model = description.ToShortInfoModel();
            var project = _defService.GetProjectDefinition(projectId);
            if (project == null)
                throw new ServiceException($"Project with id {projectId} not found", "project_not_found");
            var client = _userService.ExtractUserById(project.ClientId);
            if (client == null)
                throw new ServiceException($"Unable to find client id {project.ClientId} for peoject id {project.Id}", "client_not_found");
            model.ClientName = client.Name;
            model.City = project.City;
            return model;
        }

        /// <summary>
        /// Получить инфу о учасниках проекта
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("client/project/get-members")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProjectMembersModel GetMembers([FromQuery] int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"User permission denied", "project_not_own_by_client");

            var project = _defService.GetProjectDefinition(projectId);
            var membersModel = new ProjectMembersModel();
            var client = _userService.ExtractUserById(project.ClientId);
            if (client == null)
                throw new ServiceException($"Project client not found", "client_not_found");

            membersModel.ClientName = client.Name;

            if (project.DesignerId > 0)
            {
                var designer = _designerService.GetDesigner(project.DesignerId);
                if (designer == null)
                    throw new ServiceException($"Project designer not found", "designer_not_found");

                membersModel.DesignerAvatarUrl = designer.PhotoLink;
                membersModel.DesignerName = designer.Base.Name;
            }

            return membersModel;
        }
    }
}