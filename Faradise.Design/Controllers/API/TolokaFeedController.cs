﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.RegularExpressions;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Models.Toloka.Input;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Faradise.Design.Controllers.API
{
    [Route("toloka")]
    public class TolokaFeedController : Controller
    {
        private readonly IDBTolokaSourceService _dbSource = null;
        private IMarketplaceService _marketplaceService;

        public TolokaFeedController(IDBTolokaSourceService dbSource, IMarketplaceService marketplaceService)
        {
            _dbSource = dbSource;
            _marketplaceService = marketplaceService;
        }

        [HttpGet("sizes/{offset}/{count}")]
        public IActionResult ExtractSizeSource(int category, [Required] int offset, [Required] int count)
        {
            var input = _dbSource.ExtractSizeSource(category, offset, count);
            var stream = CreateSizeInputSource(input);

            return File(stream, "text/tab-separated-values", string.Format("sizes_{0}.tsv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        [HttpGet("photos/{offset}/{count}/{countphoto}")]
        public IActionResult ExtractPhotoSource(int category, [Required] int offset, [Required] int count, [Required] int countphoto)
        {
            var input = _dbSource.ExtractPhotoSource(category, offset, count, countphoto);
            var stream = CreatePhotoInputSource(input, countphoto);

            return File(stream, "text/tab-separated-values", string.Format("photos_{0}_{1}.tsv", countphoto, DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        [HttpGet("colors/{offset}/{count}")]
        public IActionResult ExtractColorSource(int withoutСategory, [Required] int offset, [Required] int count)
        {
            var input = _dbSource.ExtractPhotoColorSource(withoutСategory, offset, count, 1);
            var colors = _marketplaceService.GetMarketplaceColors();
            var stream = CreateColorInputSource(input, colors);

            return File(stream, "text/tab-separated-values", string.Format("colors_{0}.tsv", DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        [HttpGet("products/{offset}/{count}")]
        public IActionResult ExtractProducts(int category, [Required] int offset, [Required] int count, string vendor)
        {
            var input = _dbSource.ExtractProducts(category, offset, count, vendor);
            var stream = CreateProductsInputSource(input);

            return File(stream, "text/tab-separated-values", string.Format("products_{0}_{1}.tsv", vendor, DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        [HttpGet("descriptions/{offset}/{count}/{maxLetter}")]
        public IActionResult ExtractDescriptionSource(int category, [Required] int offset, [Required] int count, string vendor, [Required] int maxLetter)
        {
            var input = _dbSource.ExtractDescriptionSource(category, offset, count, vendor, maxLetter);
            var stream = CreateProductsInputSource(input);

            return File(stream, "text/tab-separated-values", string.Format("products_{0}_{1}.tsv", vendor, DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        [HttpGet("descriptions-askona/{offset}/{count}")]
        public IActionResult ExtractDescriptionAskonaSource(int category, [Required] int offset, [Required] int count)
        {
            string vendor = "Askona";
            var input = _dbSource.ExtractProducts(category, offset, count, vendor);
            List<TolokaInputProducts> products = new List<TolokaInputProducts>();
            foreach (var i in input)
            {
                i.Name = DeleteSizesFromNameAskona(i.Name);
                TolokaInputProducts containedProduct = products.Find(x => x.Name == i.Name);
                if(containedProduct == null)
                {
                    i.Ids.Add(i.Id);
                    products.Add(i);
                }
                else
                {
                    containedProduct.Ids.Add(i.Id);
                }
            }

            var stream = CreateAskonaDescriptionsInputSource(products.ToArray());

            return File(stream, "text/tab-separated-values", string.Format("products_{0}_{1}.tsv", vendor, DateTime.Now.ToString("yyyy-MM-dd-hh-mm")));
        }

        private byte[] CreateSizeInputSource(TolokaInputSize[] input)
        {
            using (var ms = new MemoryStream())
            using (var w = new StreamWriter(ms))
            {
                w.Write("INPUT:id");
                w.Write("\t");

                w.Write("INPUT:link");
                
                w.WriteLine();

                foreach (var i in input)
                {
                    w.Write(i.Id);
                    w.Write("\t");

                    w.Write(i.Url);
                    
                    w.WriteLine();
                }

                w.Flush();
                
                return ms.ToArray();
            }
        }

        private byte[] CreateProductsInputSource(TolokaInputProducts[] input)
        {
            using (var ms = new MemoryStream())
            using (var w = new StreamWriter(ms))
            {
                w.Write("INPUT:id");
                w.Write("\t");

                w.Write("INPUT:name");
                w.Write("\t");

                w.Write("INPUT:link");

                w.WriteLine();

                foreach (var i in input)
                {
                    w.Write(i.Id);
                    w.Write("\t");

                    w.Write(i.Name);
                    w.Write("\t");

                    w.Write(i.Url);

                    w.WriteLine();
                }

                w.Flush();

                return ms.ToArray();
            }
        }

        private byte[] CreateAskonaDescriptionsInputSource(TolokaInputProducts[] input)
        {
            using (var ms = new MemoryStream())
            using (var w = new StreamWriter(ms))
            {
                w.Write("INPUT:id");
                w.Write("\t");

                w.Write("INPUT:ids");
                w.Write("\t");

                w.Write("INPUT:name");
                w.Write("\t");

                w.Write("INPUT:link");

                w.WriteLine();

                foreach (var i in input)
                {
                    w.Write(i.Id);
                    w.Write("\t");

                    w.Write(JsonConvert.SerializeObject(i.Ids));
                    w.Write("\t");

                    w.Write(i.Name);
                    w.Write("\t");

                    w.Write(i.Url);

                    w.WriteLine();
                }

                w.Flush();

                return ms.ToArray();
            }
        }

        private byte[] CreatePhotoInputSource(TolokaInputPhoto[] input, int countPhoto)
        {
            using (var ms = new MemoryStream())
            using (var w = new StreamWriter(ms))
            {
                w.Write("INPUT:id");
                w.Write("\t");

                w.Write("INPUT:photos");

                w.WriteLine();

                foreach (var i in input)
                {
                    w.Write(i.Id);
                    w.Write("\t");

                    w.Write(FormatPhotosToloka(JsonConvert.SerializeObject(i.PhotoUrls)));

                    w.WriteLine();
                }

                w.Flush();

                return ms.ToArray();
            }
        }

        private byte[] CreateColorInputSource(TolokaInputPhoto[] input, MarketplaceColorModel[] colors)
        {
            var jsonColor = FormatJsonToloka(JsonConvert.SerializeObject(colors));

            using (var ms = new MemoryStream())
            using (var w = new StreamWriter(ms))
            {
                w.Write("INPUT:id");
                w.Write("\t");

                w.Write("INPUT:colors");
                w.Write("\t");

                w.Write("INPUT:photos");
                
                w.WriteLine();

                foreach (var i in input)
                {
                    w.Write(i.Id);
                    w.Write("\t");

                    w.Write(jsonColor);
                    w.Write("\t");

                    w.Write(FormatPhotosToloka(JsonConvert.SerializeObject(i.PhotoUrls)));

                    w.WriteLine();
                }

                w.Flush();

                return ms.ToArray();
            }
        }

        private string Cleanup(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            return string.Format("\"{0}\"", source
                .Replace("\r", string.Empty)
                .Replace("\n", " ")
                .Replace("\t", " ")
                .Replace("\"", string.Empty)
                .Replace("\'", string.Empty)
                .Replace("{", string.Empty)
                .Replace("}", string.Empty)
                .Replace("\\", string.Empty));
        }

        private string FormatJsonToloka(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            
            return string.Format("\"{0}\"", source
                .Replace("\"", "\"\"")
                .Replace(",\"", "\\,\"")
                .Replace("[", string.Empty)
                .Replace("]", string.Empty)
                );
        }

        private string FormatPhotosToloka(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;


            return string.Format("\"{0}\"", source
                .Replace("\"", string.Empty)
                .Replace("[", string.Empty)
                .Replace("]", string.Empty)
                );
        }

        private string DeleteSizesFromNameAskona(string input)
        {
            var output = "";
            Regex regex = new Regex("(([0-9][0-9][0-9])|([0-9][0-9])) x (([0-9][0-9][0-9])|([0-9][0-9])).*");
            output = regex.Replace(input, "");
            return output;
        }
    }
}
