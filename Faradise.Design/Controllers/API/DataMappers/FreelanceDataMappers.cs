﻿using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.VisulizationFreelance;
using Faradise.Design.Models.VisualizationFreelance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class FreelanceDataMappers
    {
        public static VisualisationTechSpecModel ToModel(this VisualisationTechSpec data)
        {
            return new VisualisationTechSpecModel()
            {
                Description = data.Description,
                Files = data.SpecFiles != null ? data.SpecFiles.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0],
                RoomId = data.RoomId,
                Status = data.Status
            };
        }
    }
}
