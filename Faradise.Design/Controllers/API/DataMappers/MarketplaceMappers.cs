﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.PromoCode;
using System.Linq;
using Unidecode.NET;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class MarketplaceMappers
    {
        public static CategoryModel ToModel(this Category category)
        {
            return new CategoryModel
            {
                Id = category.Id,
                ParentId = category.ParentId,
                Name = category.Name,
                NameTransliterated = category.Name
                    .Unidecode()
                    .Trim()
                    .ToLower()
                    .Replace(" ", "_")
                    .Replace(",", "_"),
                ImageUrl = category.ImageUrl,
                InHeader = category.InHeader,
                IsPopular = category.IsPopular,
                IsEnabled = category.IsEnabled,
                DeliveryCost = category.DeliveryCost,
                DeliveryDays = category.DeliveryDays,
                Priority = category.Priority,
                DailyProductPriority = category.DailyProductPriority
            };
        }

        public static ApplyPromoCodeResultModel ToModel(this PromoCodeApplyResult promoResult)
        {
            return new ApplyPromoCodeResultModel()
            {
                Error = promoResult.Error,
                PercentDiscount = promoResult.PercentDiscount,
                AbsoluteDiscount = promoResult.AbsoluteDiscount,
                ExpireDate = promoResult.ExpireDate,
                StartDate = promoResult.StartDate,
                Products = promoResult.Products != null ? promoResult.Products.Select(x => new PromoCodeProductModel
                {
                    Count = x.Count,
                    PriceWithoutPromocode = x.PriceWithoutPromocode,
                    PriceWithPromocode = x.PriceWithPromocode,
                    ProductId = x.ProductId,
                    ProductName = x.ProductName,
                    SupportPromocode = x.SupportPromocode
                }).ToArray()
                : new PromoCodeProductModel[0]
            };
        }

        public static ShortProductModel ToModel(this ProductInfo product)
        {
            var model = new ShortProductModel
            {
                Id = product.Id,
                Description = product.Description,
                PreviousPrice = product.PreviousPrice,
                Price = product.Price,
                Name = product.Name,
                Badge = product.Badge,
                BadgeDescription = product.BadgeDescription,
                SaleDescription = product.SaleDescription,

                Seller = product.Seller,
                Vendor = product.Vendor,

                ARSupport = product.ARSupport
            };

            if (product.PhotoUrl != null)
            {
                if (product.PhotoUrl.Length > 0)
                    model.PhotoUrl = product.PhotoUrl[0];

                if (product.PhotoUrl.Length > 1)
                    model.AdditionalPhotoUrl = product.PhotoUrl[1];
            }

            if (model.Price > 0 && model.PreviousPrice.HasValue && model.Price < model.PreviousPrice)
                model.Discount = (int) (0.5 + (model.PreviousPrice - model.Price) / (float) model.PreviousPrice * 100);

            return model;
        }

        public static ShortProductModel ToShortModel(this Product product)
        {
            return new ShortProductModel()
            {
                Id = product.Id,
                Description = product.Description,
                PreviousPrice = product.PreviousPrice,
                Price = product.Price,
                Name = product.Name,
                PhotoUrl = product.Pictures.Any()
                    ? product.Pictures.First()
                    : string.Empty,
                Badge = product.Badge,
                BadgeDescription = product.Description,
                SaleDescription = product.SaleDescription
            };
        }

        public static ProductModel ToModel(this Product product)
        {
            var colorString = string.Empty;
            if (product.Color != null && product.Color.Length > 0)
            {
                colorString = product.Color.Aggregate(colorString, (current, color) => current + (", " + color));
                colorString = colorString.Remove(0, 2);
            }

            return new ProductModel
            {
                Id = product.Id,
                Description = product.Description,
                PreviousPrice = product.PreviousPrice,
                Price = product.Price,
                Name = product.Name,
                Color = colorString,
                HasInStorage = product.Available,
                ModelNumber = product.VendorCode,
                Vendor = product.Vendor,
                Seller = product.Seller,
                PhotoUrls = product.Pictures,
                Waranty = product.Warranty ?? false,

                Origin = product.Origin,

                Size = product.Size,

                Width = product.Width?.ToString(),
                Length = product.Length?.ToString(),
                Height = product.Height?.ToString(),

                Notes = product.Notes,
                Badge = product.Badge,
                BadgeDescription = product.BadgeDescription,
                SaleDescription = product.SaleDescription,

                CategoryDeliveryCost = product.CategoryDeliveryCost,
                CategoryDeliveryDays = product.CategoryDeliveryDays
            };
        }

        public static PossibleDailyProductModel ToModel(this PossibleDailyProduct product)
        {
            return new PossibleDailyProductModel
            {
                Id = product.Id,
                Name = product.Name,
                PhotoUrl = product.PhotoUrl,
                PreviousPrice = product.PreviousPrice,
                Price = product.Price,
                CompanyName = product.CompanyName,
                TotalPriority = product.TotalPriority
            };
        }
    }
}