﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models.DesignerPool;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class ProjectDataMapperExtension
    {
        public static ProjectDescriptionModel ToModel(this ProjectDescription project)
        {
            var roomModels = new List<RoomsModel>();
            if (project.Rooms != null)
            {
                foreach (var model in project.Rooms)
                {
                    var oldRoom = roomModels.FirstOrDefault(x => x.Purpose == model);
                    if (oldRoom != null)
                        oldRoom.Count++;
                    else
                        roomModels.Add(new RoomsModel() { Purpose = model, Count = 1 });
                }
            }
            return new ProjectDescriptionModel()
            {
                ProjectId = project.ProjectId,
                ObjectType = project.ObjectType,
                Rooms = roomModels.ToArray(),
                Reasons = project.Reasons != null ? project.Reasons.Select(x => x.ToModel()).ToArray() : null,
                Budget = project.Budget != null ? project.Budget.ToModel() : null,
                Styles = project.Styles != null ? project.Styles.Select(x => x.ToModel()).ToArray() : null,
                DesignerRequirements = project.DesignerRequirements != null ? project.DesignerRequirements.ToModel() : null,
            };
        }

        public static ShortProjectInfoModel ToShortInfoModel(this ProjectDescription project)
        {
            var roomModels = new List<RoomsModel>();
            if (project.Rooms != null)
            {
                foreach (var model in project.Rooms)
                {
                    var oldRoom = roomModels.FirstOrDefault(x => x.Purpose == model);
                    if (oldRoom != null)
                        oldRoom.Count++;
                    else
                        roomModels.Add(new RoomsModel() { Purpose = model, Count = 1 });
                }
            }
            return new ShortProjectInfoModel()
            {
                ProjectId = project.ProjectId,
                ObjectType = project.ObjectType,
                Rooms = roomModels.ToArray(),
                Reasons = project.Reasons != null ? project.Reasons.Select(x => x.ToModel()).ToArray() : null,
                Budget = project.Budget != null ? project.Budget.ToModel() : null,
                Styles = project.Styles != null ? project.Styles.Select(x => x.ToModel()).ToArray() : null
            };
        }

        public static DesignerRequirementsModel ToModel(this DesignerRequirements requirements)
        {
            return new DesignerRequirementsModel()
            {
                AgeFrom = requirements.AgeFrom,
                AgeTo = requirements.AgeTo,
                Gender = requirements.Gender,
                MeetingAdress = requirements.MeetingAdress,
                NeedPersonalMeeting = requirements.NeedPersonalMeeting,
                NeedPersonalControl = requirements.NeedPersonalСontrol,
                Testing = requirements.Testing != null ? requirements.Testing.ToModel() : null
            };
        }

        public static TestResultsModel ToModel(this TestResults test)
        {
            return new TestResultsModel()
            {
                Questions = test.Questions != null ? test.Questions.Select(x => new TestResultsAnswerModel() { Answer = x.Answer, Index = x.Index }).ToArray() : new TestResultsAnswerModel[0]
            };
        }

        public static StyleModel ToModel(this StyleBinding style)
        {
            return new StyleModel()
            {
                Name = style.Name,
                Description = style.Description
            };
        }

        public static ReasonModel ToModel(this ReasonBinding reason)
        {
            return new ReasonModel()
            {
                Description = reason.Description,
                Name = reason.Name
            };
        }

        public static BudgetModel ToModel(this Budget budget)
        {
            return new BudgetModel()
            {
                FurnitureBudget = budget.Furniture,
                RenovationBudget = budget.Renovation
            };
        }

        public static DesignerRequirements ToService(this DesignerRequirementsModel model)
        {
            return new DesignerRequirements()
            {
                AgeFrom = model.AgeFrom,
                AgeTo = model.AgeTo,
                Gender = model.Gender,
                MeetingAdress = model.MeetingAdress,
                NeedPersonalMeeting = model.NeedPersonalMeeting,
                NeedPersonalСontrol = model.NeedPersonalControl,
                Testing = model.Testing.ToService()
            };
        }

        public static TestResults ToService(this TestResultsModel model)
        {
            return new TestResults()
            {
                Questions = model.Questions != null ? model.Questions.Select(x => new TestResultsAnswer() { Answer = x.Answer, Index = x.Index }).ToArray() : new TestResultsAnswer[0]
            };
        }

        public static TestQuestionDescriptionModel ToModel(this TestQuestionDescription question)
        {
            return new TestQuestionDescriptionModel()
            {
                Id = question.Id,
                Text = question.Text,
                Answers = question.Answers.Select(x => new TestAnswerDescriptionModel() { Id = x.Id, Text = x.Text }).ToArray()
            };
        }

        public static Budget ToService(this BudgetModel model)
        {
            return new Budget()
            {
                Furniture = model.FurnitureBudget,
                Renovation = model.RenovationBudget
            };
        }

        public static DesignerDatingModel ToModel(this DesignerDatingInfo info)
        {
            return new DesignerDatingModel()
            {
                Id = info.Id,
                Description = info.Description,
                Name = info.Name,
                PortfolioLink = info.PortfolioLink,
                AvatarUrl = info.AvatarUrl,
                Age = info.Age,
                City = info.City,
                Gender = info.Gender,
                PersonalСontrol = info.PersonalСontrol
            };
        }
    }
}
