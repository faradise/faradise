﻿using Faradise.Design.Controllers.API.Models.Brigade;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Models.Brigade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class BrigadeDataMappers
    {
        public static BrigadeTechSpecModel ToModel(this BrigadeTechSpec brigadeTechSpec)
        {
            return new BrigadeTechSpecModel()
            {
                Description = brigadeTechSpec.Description,
                Files = brigadeTechSpec.SpecFiles != null ? brigadeTechSpec.SpecFiles.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0],
                ProjectId = brigadeTechSpec.ProjectId,
                Status = brigadeTechSpec.Status
            };
        }
    }
}
