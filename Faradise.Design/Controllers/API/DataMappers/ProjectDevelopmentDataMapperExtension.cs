﻿using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class ProjectDevelopmentDataMapperExtension
    {
        public static ProjectStageModel ToModel(this DevelopmentStageStatus status)
        {
            return new ProjectStageModel()
            {
                ClientIsReady = status.ClientIsReady,
                DesignerIsReady = status.DesignerIsReady,
                Stage = status.Stage,
                TimeLeftSeconds = status.TimeLeft,
                HasUnfinishedWork = status.HasUnfinishedWork,
                UserMadeDecision = status.UserMadeDecision,
                IsPaid = status.IsPaid,
                NeedPayment = status.NeedPayment,
                PaymentFailed = status.PaymentFailed
            };
        }

        public static MoodboardModel ToModel(this Moodboard moodboard)
        {
            return new MoodboardModel()
            {
                RoomId = moodboard.RoomId,
                MoodboardPhotos = moodboard.Photos != null ? moodboard.Photos.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0]
            };
        }

        public static CollageModel ToModel(this Collage collage)
        {
            return new CollageModel()
            {
                RoomId = collage.RoomId,
                CollagePhotos = collage.Photos != null ? collage.Photos.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0]
            };
        }

        public static ZoneModel ToModel(this Zone zone)
        {
            return new ZoneModel()
            {
                RoomId = zone.RoomId,
                ZonePhotos = zone.Photos != null ? zone.Photos.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0]
            };
        }

        public static PlanModel ToModel(this Plan plan)
        {
            return new PlanModel()
            {
                RoomId = plan.RoomId,
                PlanFiles = plan.Files != null ? plan.Files.Select(x => new FileModel() { FileId = x.FileId, FileLink = x.Link }).ToArray() : new FileModel[0],
                Price = plan.Price
            };
        }

        public static VisualizationModel ToModel(this Visualization visualization)
        {
            return new VisualizationModel()
            {
                RoomId = visualization.RoomId,
                Price = visualization.Price,
                VisualizationCode = visualization.VisualizationCode
            };
        }
    }
}
