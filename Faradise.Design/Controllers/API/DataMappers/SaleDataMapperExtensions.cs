﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.Sale;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class SaleDataMapperExtensions
    {
        public static SaleModel ToModel(this Sale sale)
        {
            SaleUpdateJson updateInfo = !string.IsNullOrEmpty(sale.UpdateJson) ? JsonConvert.DeserializeObject<SaleUpdateJson>(sale.UpdateJson) : null;
            return new SaleModel
            {
                Id = sale.Id,
                IsActive = sale.IsActive,
                Name = sale.Name,
                Description = sale.Description,
                Percents = sale.Percent,
                AllMarketplace = updateInfo != null ? updateInfo.AllMarkeplace : false,
                Products = updateInfo != null ? updateInfo.ProdyctIds : new int[0],
                Categories = updateInfo != null ? updateInfo.CategoryIds : new int[0],
                Companies = updateInfo != null ? updateInfo.CompaniesId : new int[0]
            };
        }

        public static SaleShortModel ToModel(this SaleShortInfo sale)
        {
            return new SaleShortModel
            {
                Id = sale.Id,
                IsActive = sale.IsActive,
                Name = sale.Name
            };
        }
    }
}
