﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.ProjectDefinition;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class AdminDataMapperExtensions
    {
        public static List<DesignerInfo> ToModel(this List<DesignerShortInfo> designersInfo)
        {
            return designersInfo.Select(x => new DesignerInfo
            {
                Id = x.Id,
                Name = x.Name,
                Surname = x.Surname,
                Email=x.Email,
                Phone=x.Phone,
                AppointedProjectsCount = x.AppointedProjectsCount,
                ProjectsInWorkCount = x.ProjectsInWorkCount,
                RejectedProjectsCount = x.RejectedProjectsCount,
                Stage = x.Stage
            }).ToList();
        }


    }
}
