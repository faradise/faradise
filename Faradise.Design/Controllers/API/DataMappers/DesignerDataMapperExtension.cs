﻿using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models.Designer;
using System.Linq;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class DesignerDataMapperExtension
    {
        public static DesignerModel ToModel(this Designer designer)
        {
            return new DesignerModel()
            {
                Id = designer.Id,
                Stage = designer.Stage,
                Base = designer.Base != null ? ToModel(designer.Base) : new DesignerBaseModel(),
                Phone = designer.Phone,
                PortfolioLink = designer.PortfolioLink,
                Portfolio = designer.Portfolio != null ? ToModel(designer.Portfolio) : new PortfolioModel(),
                TestJob = designer.TestJob != null ? ToModel(designer.TestJob) : new TestJobInfoModel(),
                Styles = designer.Styles != null ? ToModel(designer.Styles) : new DesignerStyleModel[0],
                PhotoLink = designer.PhotoLink,
                Level = designer.Level,
                Possibilities = designer.Possibilities != null ? ToModel(designer.Possibilities) : new DesignerPossibilityModel(),
                Testing = designer.Testing != null ? designer.Testing.ToModel() : new TestResultsModel(),
                LegalModel = designer.LegalModel != null ? ToModel(designer.LegalModel) : new DesignerLegalModel()
            };
        }

        public static DesignerBaseModel ToModel(this DesignerBase designer)
        {
            return new DesignerBaseModel()
            {
                Name = designer.Name,
                Surname = designer.Surname,
                Age = designer.Age,
                City = designer.City,
                Gender = designer.Gender,
                Email = designer.Email,
                About = designer.About,
                WorkExperience = designer.WorkExperience,
                Stage=designer.Stage
                
            };
        }

        public static TestJobInfoModel ToModel(TestJobInfo info)
        {
            return new TestJobInfoModel
            {
                TestLink = info.TestLink,
                TestTime = info.TestTime,
                TestApproved = info.TestApproved
            };
        }

        public static PortfolioModel ToModel(this Portfolio portfolio)
        {
            return new PortfolioModel
            {
                Portfolios = portfolio?.Portfolios?.Select(x => new PortfolioProjectModel { Id = x.Id, Name = x.Name, Description = x.Description, PhotoLinks = x.PhotoLinks.ToModel() }).ToArray()
            };
        }

        public static PortfolioDescriptionModel ToModel(this PortfolioDescription portfolio)
        {
            return new PortfolioDescriptionModel { Id = portfolio.Id, Name = portfolio.Name, Description = portfolio.Description };
        }

        public static PortfolioDescriptionsModel ToModel(this PortfolioDescriptions portfolios)
        {
            return new PortfolioDescriptionsModel { Descriptions = portfolios?.Descriptions?.Select(x => x.ToModel()).ToArray() };
        }

        public static PortfolioPhotoModel[] ToModel(this PortfolioPhoto[] photo)
        {
            var photos = photo?.Select(x => new PortfolioPhotoModel { Id = x.Id, PhotoLink = x.PhotoLink }).ToArray();
            return photos;
        }

        public static AddPortfolioPhotoModel ToModel(this AddPortfolioPhoto model)
        {
            return new AddPortfolioPhotoModel
            {
                PortfolioId = model.PortfolioId,
                Photo = model.Photo
            };
        }

        public static RemovePortfolioPhotoModel ToModel(this RemovePortfolioPhoto model)
        {
            return new RemovePortfolioPhotoModel
            {
                PortfolioId = model.PortfolioId,
                PhotoId = model.PhotoId
            };
        }

        public static DesignerStyleModel[] ToModel(this DesignerStyle[] designer)
        {
            if (designer == null)
                return null;
            return designer?.Select(x => new DesignerStyleModel { Name = x.Name, Description = x.Description }).ToArray();
        }

        public static DesignerPossibilityModel ToModel(this DesignerPossibility possibility)
        {
            return new DesignerPossibilityModel
            {
                GoToPlaceInYourCity = possibility.GoToPlaceInYourCity,
                GoToPlaceInAnotherCity = possibility.GoToPlaceInAnotherCity,
                PersonalСontrol = possibility.PersonalСontrol,
                PersonalMeeting = possibility.PersonalMeeting,
                SkypeMeeting = possibility.SkypeMeeting,
                RoomMeasurements = possibility.RoomMeasurements,
                Blueprints = possibility.Blueprints
            };
        }

        public static DesignerLegalModel ToModel(this DesignerLegal designer)
        {
            return new DesignerLegalModel
            {
                LegalType = designer.LegalType,
                CompanyLegalInfo =designer.CompanyLegalInfo!=null? new CompanyLegalInfoModel
                {
                    Name = designer.CompanyLegalInfo.Name,
                    OGRN = designer.CompanyLegalInfo.OGRN,
                    INN = designer.CompanyLegalInfo.INN,
                    KPP = designer.CompanyLegalInfo.KPP,
                    BankName = designer.CompanyLegalInfo.BankName,
                    CheckingAccount = designer.CompanyLegalInfo.CheckingAccount,
                    BIC = designer.CompanyLegalInfo.BIC,
                    SoleExecutiveBody = designer.CompanyLegalInfo.SoleExecutiveBody,
                    NameSoleExecutiveBody = designer.CompanyLegalInfo.NameSoleExecutiveBody
                }:null,
                PersonalLegalInfo =designer.PersonalLegalInfo!=null? new PersonalLegalInfoModel
                {
                    Surname = designer.PersonalLegalInfo.Surname,
                    Name = designer.PersonalLegalInfo.Name,
                    MiddleName = designer.PersonalLegalInfo.MiddleName,
                    Phone = designer.PersonalLegalInfo.Phone,
                    PassportSeries = designer.PersonalLegalInfo.PassportSeries,
                    PassportIssuedBy = designer.PersonalLegalInfo.PassportIssuedBy,
                    DateOfIssue = designer.PersonalLegalInfo.DateOfIssue,
                    DepartmentCode = designer.PersonalLegalInfo.DepartmentCode,
                    RegistrationAddress = designer.PersonalLegalInfo.RegistrationAddress,
                    ActualAddress = designer.PersonalLegalInfo.ActualAddress,
                    Email = designer.PersonalLegalInfo.Email
                }:null
            };
        }


        public static Designer ToService(this DesignerModel designer)
        {
            return new Designer()
            {
                Id = designer.Id,
                Stage = designer.Stage,
                Base = ToService(designer.Base),
                Phone = designer.Phone,
                PortfolioLink = designer.PortfolioLink,
                Portfolio = ToService(designer.Portfolio),
                TestJob = designer.TestJob != null ? ToService(designer.TestJob) : new TestJobInfo(),
                Styles = ToService(designer.Styles),
                PhotoLink = designer.PhotoLink,
                Level = designer.Level,
                Possibilities = ToService(designer.Possibilities),
                Testing = designer.Testing.ToService(),
                LegalModel = ToService(designer.LegalModel)
            };
        }

        public static DesignerBase ToService(this DesignerBaseModel designer)
        {
            return new DesignerBase()
            {
                Name = designer.Name,
                Surname = designer.Surname,
                Age = designer.Age,
                City = designer.City,
                Gender = designer.Gender,
                Email = designer.Email,
                About = designer.About,
                WorkExperience = designer.WorkExperience,
                Stage=designer.Stage
            };
        }

        public static TestJobInfo ToService(TestJobInfoModel info)
        {
            return new TestJobInfo
            {
                TestLink = info.TestLink,
                TestTime = info.TestTime,
                TestApproved = info.TestApproved
            };
        }

        public static Portfolio ToService(this PortfolioModel portfolio)
        {
            return new Portfolio
            {
                Portfolios = portfolio?.Portfolios?.Select(x => new PortfolioProject { Id = x.Id, Name = x.Name, Description = x.Description, PhotoLinks = x.PhotoLinks.ToService() }).ToArray()
            };
        }

        public static PortfolioDescription ToService(this PortfolioDescriptionModel portfolio)
        {
            return new PortfolioDescription { Id = portfolio.Id, Name = portfolio.Name, Description = portfolio.Description };
        }

        public static PortfolioDescriptions ToService(this PortfolioDescriptionsModel portfolios)
        {
            return new PortfolioDescriptions { Descriptions = portfolios?.Descriptions?.Select(x => x.ToService()).ToArray() };
        }

        public static PortfolioPhoto[] ToService(this PortfolioPhotoModel[] photo)
        {
            var photos = photo?.Select(x => new PortfolioPhoto { Id = x.Id, PhotoLink = x.PhotoLink }).ToArray();
            return photos;
        }

        public static AddPortfolioPhoto ToService(this AddPortfolioPhotoModel model)
        {
            return new AddPortfolioPhoto
            {
                PortfolioId = model.PortfolioId,
                Photo = model.Photo
            };
        }

        public static RemovePortfolioPhoto ToService(this RemovePortfolioPhotoModel model)
        {
            return new RemovePortfolioPhoto
            {
                PortfolioId = model.PortfolioId,
                PhotoId = model.PhotoId
            };
        }

        public static DesignerStyle[] ToService(this DesignerStyleModel[] designer)
        {
            return designer?.Select(x => new DesignerStyle { Name = x.Name, Description = x.Description }).ToArray();
        }

        public static DesignerPossibility ToService(this DesignerPossibilityModel possibility)
        {
            return new DesignerPossibility
            {
                GoToPlaceInYourCity = possibility.GoToPlaceInYourCity,
                GoToPlaceInAnotherCity = possibility.GoToPlaceInAnotherCity,
                PersonalСontrol = possibility.PersonalСontrol,
                PersonalMeeting = possibility.PersonalMeeting,
                SkypeMeeting = possibility.SkypeMeeting,
                RoomMeasurements = possibility.RoomMeasurements,
                Blueprints = possibility.Blueprints
            };
        }

        public static DesignerLegal ToService(this DesignerLegalModel designer)
        {
            return new DesignerLegal
            {
                LegalType = designer.LegalType,
                CompanyLegalInfo = new CompanyLegalInfo
                {
                    Name = designer.CompanyLegalInfo.Name,
                    OGRN = designer.CompanyLegalInfo.OGRN,
                    INN = designer.CompanyLegalInfo.INN,
                    KPP = designer.CompanyLegalInfo.KPP,
                    BankName = designer.CompanyLegalInfo.BankName,
                    CheckingAccount = designer.CompanyLegalInfo.CheckingAccount,
                    BIC = designer.CompanyLegalInfo.BIC,
                    SoleExecutiveBody = designer.CompanyLegalInfo.SoleExecutiveBody,
                    NameSoleExecutiveBody = designer.CompanyLegalInfo.NameSoleExecutiveBody
                },
                PersonalLegalInfo = new PersonalLegalInfo
                {
                    Surname = designer.PersonalLegalInfo.Surname,
                    Name = designer.PersonalLegalInfo.Name,
                    MiddleName = designer.PersonalLegalInfo.MiddleName,
                    Phone = designer.PersonalLegalInfo.Phone,
                    PassportSeries = designer.PersonalLegalInfo.PassportSeries,
                    PassportIssuedBy = designer.PersonalLegalInfo.PassportIssuedBy,
                    DateOfIssue = designer.PersonalLegalInfo.DateOfIssue,
                    DepartmentCode = designer.PersonalLegalInfo.DepartmentCode,
                    RegistrationAddress = designer.PersonalLegalInfo.RegistrationAddress,
                    ActualAddress = designer.PersonalLegalInfo.ActualAddress,
                    Email = designer.PersonalLegalInfo.Email
                }
            };
        }
    }
}
