﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static  class MarketplaceDataMapperExtension
    {
        public static CompanyModel ToModel(this Company company)
        {
            return new CompanyModel
            {
                Id = company.Id,
                Name = company.Name,
                Email = company.Email,
                Url = company.Url,
                AdditionalNotes = !string.IsNullOrEmpty(company.NotesJson) ? JsonConvert.DeserializeObject<List<string>>(company.NotesJson) : new List<string>(),
                ContactPersonEmail = company.ContactPersonEmail,
                ContactPersonName = company.ContactPersonName,
                ContactPersonPhone = company.ContactPersonPhone,
                DeliveryInfo = company.DeliveryInfo,
                HasDesignFurniture = (company.CompanyProductionMask & CompanyProduction.DesignFurniture) == CompanyProduction.DesignFurniture,
                HasElectric = (company.CompanyProductionMask & CompanyProduction.Electronics) == CompanyProduction.Electronics,
                HasFurniture = (company.CompanyProductionMask & CompanyProduction.Furniture) == CompanyProduction.Furniture,
                HasMaterials = (company.CompanyProductionMask & CompanyProduction.Materials) == CompanyProduction.Materials,
                LegalName = company.LegalName,
                IsEnabled = company.IsEnabled,
                AutoLoadFeed = company.EnableAutofeed,
                FeedUrl = company.FeedUrl,

                AllCategories = company.AllCategories,
                AllProducts = company.AllProducts,
                CanTradeCategories = company.CanTradeCategories,
                CanTradeProducts = company.CanTradeProducts,
                InTradeCategories = company.InTradeCategories,
                InTradeProducts = company.InTradeProducts,
                NotInTradeCategories = company.NotInTradeCategories,
                NotInTradeProducts = company.NotInTradeProducts,
                SalesCategories = company.SalesCategories,
                SalesProducts = company.SalesProducts,
                ProductsNotInLeafs = company.ProductsNotInLeafs,
                MaxSale = company.MaxSale,
                MinSale = company.MinSale,
                DailyProductPriority = company.DailyProductPriority,
                DontSupportPromocode = company.DontSupportPromocode
            };
        }

        public static CompanyCategoryModel ToModel(this CompanyCategory category)
        {
            return new CompanyCategoryModel
            {
                Category = category.CategoryId,
                CompanyCategory = category.CompanyCategoryName,
                CompanyCategoryId = category.CompanyCategoryId,
                ParentCompanyCategoryId = category.ParentCompanyCategoryId,
                Products = category.ProductsCount,
                Rooms=category.Rooms.Select(x=>new RoomNameModel { RoomNameId=x.Id, Name=x.Name}).ToArray(),
                Depth=category.Depth
            };
        }

        public static MapCategory ToService(this CompanyCategoryModel model, int companyId)
        {
            return new MapCategory { CompanyId = companyId, CategoryId = model.Category, CompanyCategoryId = model.CompanyCategoryId };
        }

        public static CompanyCategory ToService(this MarketplaceCategory category, int companyId)
        {
            return new CompanyCategory
            {
                CompanyId = companyId,
                CategoryId = 0,
                CompanyCategoryId = category.Id,
                CompanyCategoryName = category.Name,
                ParentCompanyCategoryId = category.ParentId
            };
        }

        public static Product ToService(this MarketplaceProduct product, int companyId)
        {
            return new Product
            {
                CompanyProductId = product.Id,
                CategoryId = 0,
                CompanyId=companyId,
                CompanyCategoryId = product.CategoryId,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                PreviousPrice = product.PreviousPrice,
                Url = product.Url,
                Pictures = product.Pictures,
                Available = product.Available,
                Pickup = product.Pickup,
                Delivery = product.Delivery,
                DeliveryCost = product.DeliveryCost,
                DeliveryDays = product.DeliveryDays,
                Seller=product.Seller,
                Vendor = product.Vendor,
                VendorCode = product.VendorCode,
                Warranty = product.Warranty,
                Color = product.Color,
                Origin = product.Origin,
                Size = product.Size,
                Width = product.Width,
                Height = product.Height,
                Length = product.Length,
                Notes = product.Notes
            };
        }

        public static Category ToService(this ModifyCategoryModel model)
        {
            return new Category
            {
                Id = model.Id,
                Name = model.Name,
                InHeader = model.InHeader,
                IsPopular = model.IsPopular,
                ImageUrl = model.ImageUrl,
                ParentId = model.ParentId,
                DeliveryCost = model.DeliveryCost,
                DeliveryDays = model.DeliveryDays,
                Priority = model.Priority,
                DailyProductPriority = model.DailyProductPriority,
                SelectedParametersIds = model.SelectedParametersIds
            };
        }
    }
}
