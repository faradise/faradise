﻿using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.DAL;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class BannerDataMapperExtensions
    {
        public static BannerEntity ToDbo(this BannerModel model)
        {
            return new BannerEntity
            {
                Id = model.Id,
                Name = model.Name,
                Active = model.Active,
                PriceFrom = model.PriceFrom,
                ProductId = model.ProductId,
                BrandId = model.BrandId,
                PictureMobileBackground = model.PictureMobileBackground,
                CategoryId = model.CategoryId,
                PriceTo = model.PriceTo,
                PictureWeb = model.PictureWeb,
                CompilationName = model.CompilationName,
                PictureMobile = model.PictureMobile,
                Order = model.Order,
                Updated = model.Upated,
                DiscountFilter = model.DiscountFilter,
                Ordering = model.Ordering,
                Url = model.Url
            };
        }

        public static BannerModel ToModel(this BannerEntity entity)
        {
            return new BannerModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Active = entity.Active,
                PriceFrom = entity.PriceFrom,
                ProductId = entity.ProductId,
                BrandId = entity.BrandId,
                PictureMobileBackground = entity.PictureMobileBackground,
                CategoryId = entity.CategoryId,
                PriceTo = entity.PriceTo,
                PictureWeb = entity.PictureWeb,
                CompilationName = entity.CompilationName,
                PictureMobile = entity.PictureMobile,
                Order = entity.Order,
                Upated = entity.Updated,
                Url = entity.Url,
                DiscountFilter = entity.DiscountFilter,
                Ordering = entity.Ordering
            };
        }

        public static Banner ToExternal(this BannerEntity entity)
        {
            return new Banner
            {
                Id = entity.Id,
                Name = entity.Name,
                Active = entity.Active,
                PriceFrom = entity.PriceFrom,
                ProductId = entity.ProductId,
                BrandId = entity.BrandId,
                PictureMobileBackground = entity.PictureMobileBackground,
                CategoryId = entity.CategoryId,
                PriceTo = entity.PriceTo,
                PictureWeb = entity.PictureWeb,
                CompilationName = entity.CompilationName,
                PictureMobile = entity.PictureMobile,
                Order = entity.Order,
                Upated = entity.Updated,
                Url = entity.Url,
                DiscountFilter = entity.DiscountFilter,
                Ordering = entity.Ordering
            };
        }
    }
}