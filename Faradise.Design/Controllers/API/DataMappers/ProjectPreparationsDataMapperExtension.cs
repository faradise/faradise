﻿using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Models.ProjectPreparations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class ProjectPreparationsDataMapperExtension
    {
        public static AdditionalInformationForRoom ToService(this AdditionalInformationForRoomModel model)
        {
            return new AdditionalInformationForRoom()
            {
                RoomId = model.RoomId, 
                Questions = model.Questions != null ? model.Questions.Select(x => new AdditionalQuestion() { Answer = x.Answer, Index = x.Index }).ToList() : new List<AdditionalQuestion>()
            };
        }

        public static AdditionalInformationForRoomModel ToModel(this AdditionalInformationForRoom room)
        {
            return new AdditionalInformationForRoomModel()
            {
                RoomId = room.RoomId,
                Questions = room.Questions != null ? room.Questions.Select(x => new AdditionalQuestionModel() { Answer = x.Answer, Index = x.Index }).ToList() : new List<AdditionalQuestionModel>()
            };
        }

        public static RoomInfoModel ToModel(this RoomInfo room)
        {
            return new RoomInfoModel()
            {
                RoomId = room.RoomId,
                Photos = room.Photos != null ? room.Photos.Select(x => new PhotoModel() { PhotoId = x.PhotoId, Link = x.Link }).ToList() : new List<PhotoModel>(),
                PlanLink = room.Plan,
                Proportions = room.Proportions != null ? room.Proportions.ToModel() : null,
                Purpose = room.Purpose
            };
        }

        public static ShortRoomInfo ToService(this ShortRoomInfoModel model)
        {
            return new ShortRoomInfo()
            {
                RoomId = model.RoomId,
                Proportions = model.Proportions != null ? model.Proportions.ToServive() : null
            };
        }

        public static RoomProportionsModel ToModel(this RoomProportions proportions)
        {
            return new RoomProportionsModel()
            {
                HeightCeiling = proportions.HeightCeiling,
                HeightDoor = proportions.HeightDoor,
                SideA = proportions.SideA,
                SideB = proportions.SideB,
                WidthDoor = proportions.WidthDoor
            };
        }

        public static RoomProportions ToServive(this RoomProportionsModel model)
        {
            return new RoomProportions()
            {
                HeightCeiling = model.HeightCeiling,
                HeightDoor = model.HeightDoor,
                SideA = model.SideA,
                SideB = model.SideB,
                WidthDoor = model.WidthDoor
            };
        }

        public static ClientDesignerBudgetModel ToModel(this ClientDesignerBudget budget)
        {
            return new ClientDesignerBudgetModel()
            {
                FurnitureBudget = budget.FurnitureBudget,
                RenovationBudget = budget.RenovationBudget
            };
        }

        public static PhotoModel ToModel(this Photo photo)
        {
            return new PhotoModel() { Link = photo.Link, PhotoId = photo.PhotoId };
        }

        public static OldFurnitureModel ToModel(this OldFurniture oldFurenture)
        {
            return new OldFurnitureModel()
            {
                FurnishPhotos = oldFurenture.FurnishPhotos != null ? oldFurenture.FurnishPhotos.Select(x => x.ToModel()).ToList() : new List<PhotoModel>(),
                FurniturePhotos = oldFurenture.FurniturePhotos != null ? oldFurenture.FurniturePhotos.Select(x => x.ToModel()).ToList() : new List<PhotoModel>(),
            };
        }
    }
}
