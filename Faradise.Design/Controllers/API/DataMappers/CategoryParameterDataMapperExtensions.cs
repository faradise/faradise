﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Models.CategoryParameters;
using Newtonsoft.Json;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class CategoryParameterDataMapperExtensions
    {
        public static ParameterInfo ToModel(this ParameterEntity entity)
        {
            return new ParameterInfo
            {
                Id = entity.Id,
                Name = entity.Name,
                Type = entity.Type,
                GlobalParameter = entity.GlobalParameter,
                Synonyms = JsonConvert.DeserializeObject<List<string>>(entity.Synonyms),
                Categories = entity.Categories.Select(parameterEntity => new CategoryParameterInfo
                {
                    CategoryId = parameterEntity.CategoryId,
                    CategoryName = parameterEntity.Category.Name
                }).ToList()
            };
        }

        public static ParameterEntity ToDbo(this ParameterInfo model)
        {
            return new ParameterEntity
            {
                Name = model.Name,
                Type = model.Type,
                GlobalParameter = model.GlobalParameter,
                Synonyms = JsonConvert.SerializeObject(model.Synonyms),
                Categories = model.SelectedCategories == null
                    ? new List<CategoryParameterEntity>()
                    : model.SelectedCategories
                        .Select(categoryId => new CategoryParameterEntity {CategoryId = categoryId}).ToList()
            };
        }
    }
}