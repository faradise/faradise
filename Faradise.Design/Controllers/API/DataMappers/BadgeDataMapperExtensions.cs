﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.Badge;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.DataMappers
{
    public static class BadgeDataMapperExtensions
    {
        public static BadgeShortModel ToModel(this BadgeShortInfo badge)
        {
            return new BadgeShortModel
            {
                Id = badge.Id,
                IsActive = badge.IsActive,
                Name = badge.Name
            };
        }

        public static BadgeModel ToModel(this Badge badge)
        {
            BadgeUpdateJson updateInfo = !string.IsNullOrEmpty(badge.UpdateJson) ? JsonConvert.DeserializeObject<BadgeUpdateJson>(badge.UpdateJson) : null;
            return new BadgeModel
            {
                Id = badge.Id,
                IsActive = badge.IsActive,
                Picture = badge.Picture,
                Description = badge.Description,
                Name = badge.Name,
                Products = updateInfo != null ? updateInfo.ProdyctIds : new int[0],
                AllMarketplace = updateInfo != null ? updateInfo.AllMarkeplace : false,
                Categories = updateInfo != null ? updateInfo.CategoryId : new int[0],
                Companies = updateInfo != null ? updateInfo.CompaniesId : new int[0]
            };
        }
    }
}
