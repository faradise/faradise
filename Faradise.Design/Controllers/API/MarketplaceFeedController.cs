﻿using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Feed;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("marketplace/feed/")]
    public class MarketplaceFeedController : Controller
    {
        private readonly IDBFeedService _dbQuery = null;

        public MarketplaceFeedController(IDBFeedService dbQuery)
        {
            _dbQuery = dbQuery;
        }

        [HttpGet("download")]
        public async Task<IActionResult> Download([FromQuery] FeedRequestModel model)
        {
            var url = await _dbQuery.ExtractFeedLink(model.Format, model.Availability, model.Cost);
            if (string.IsNullOrEmpty(url))
                return Forbid();

            return Redirect(url);
        }

        //[HttpGet("yml")]
        //public IActionResult GetYmlFeed()
        //{
        //    var link = _dbQuery.GetYmlFeedLink();

        //    if (string.IsNullOrEmpty(link))
        //        return Forbid();

        //    return Redirect(link);
        //}

        //[HttpGet("yml-short")]
        //public IActionResult GetShortYmlFeed()
        //{
        //    var link = _dbQuery.GetYmlAbove10KFeedLink();

        //    if (string.IsNullOrEmpty(link))
        //        return Forbid();

        //    return Redirect(link);
        //}

        //[HttpGet("google")]
        //public IActionResult GetGoogleFeed()
        //{
        //    var link = _dbQuery.GetGoogleFeedLink();

        //    if (string.IsNullOrEmpty(link))
        //        return Forbid();

        //    return Redirect(link);
        //}
    }
}