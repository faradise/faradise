﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Compilations;
using Faradise.Design.Internal;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Faradise.Design.Controllers.API
{
    [Route("api/compilations")]
    public class CompilationController : APIController
    {
        private readonly IDBCompilationQueryService _dbService = null;

        public CompilationController(IDBCompilationQueryService dbService)
        {
            _dbService = dbService;
        }

        [HttpGet("get-compilation-products")]
        public CompilationProductListModel GetCompilationProducts([FromQuery] string name, [FromQuery] int offset, [FromQuery] int count)
        {
            var query = _dbService.GetProductForCompilation(name, offset, count);
            var result = new CompilationProductListModel
            {
                Count = query.AllCount,
                Products = query.Products
                    .Select(x => x.ToModel())
                    .ToArray()
            };

            return result;
        }
    }
}
