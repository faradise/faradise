﻿using Faradise.Design.Controllers.API.Models.Mailing;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Faradise.Design.Utilities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Faradise.Design.Controllers.API
{
    [Route("api/mailing")]
    public class MailingController : APIController
    {
        private ISMSService _smsService = null;
        private IUserService _userService = null;
        private PhoneUtility _phoneUtility = null;

        public MailingController(ISMSService smsService, IUserService userService, PhoneUtility phoneUtility)
        {
            _smsService = smsService;
            _userService = userService;
            _phoneUtility = phoneUtility;
        }
        /// <summary>
        /// Рассылка смс
        /// </summary>
        [HttpPost("send-sms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<MailingResponseModel> SendSMS([FromBody] MailingModel mailModel)
        {
            var users = mailModel.Ids;
            var message = mailModel.Message;
            List<MailingResponseModel> list = new List<MailingResponseModel>();
            foreach(var userId in users)
            {
                MailingResponseModel model = new MailingResponseModel();
                var user = _userService.ExtractUserById(userId);
                if(user == null)
                {
                    model.Id = userId;
                    model.Status = "user-not-found";
                    list.Add(model);
                }
                else
                {
                    model.Id = userId;
                    string phone = string.Empty;
                    try
                    {
                        phone = _phoneUtility.FixPhone(user.Phone);
                    }
                    catch
                    {
                        model.Phone = user.Phone;
                        model.Status = "phone-not-valid";
                        list.Add(model);
                        continue;
                    }

                    model.Phone = phone;
                    model.Status = _smsService.TrySendSMS(user.Phone, message);
                    list.Add(model);
                }
            }
            return list;
        }
    }
}
