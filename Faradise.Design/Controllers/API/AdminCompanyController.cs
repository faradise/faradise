﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.Models.Admin.Company;
using Faradise.Design.Internal;
using Faradise.Design.Models.Admin.Company;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/company")]
    public class AdminCompanyController : APIController
    {
        private readonly IDBAdminCompanyService _dbCompany = null;

        public AdminCompanyController(IDBAdminCompanyService dbCompany)
        {
            _dbCompany = dbCompany;
        }

        [HttpGet("company-categories")]
        public AdminCompanyCategoriesModel ExtractCompanyCategories([FromQuery] GetCompanyCategoriesModel model)
        {
            var plain = _dbCompany
                .ExtractCompanyCategories(model.CompanyId)
                .Select(x => new AdminCompanyCategoryModel
                {
                    CompanyCategoryId = x.CompanyCategoryId,
                    ParentCompanyCategoryId = x.ParentCompanyCategoryId,
                    Products = x.Products,
                    CategoryId = x.CategoryId,
                    BadCategoryBinding = x.BadCategoryBinding,
                })
                .ToList();

            foreach (var p in plain)
            {
                p.Childs = plain
                    .Where(x => x.ParentCompanyCategoryId == p.CompanyCategoryId)
                    .ToList();
            }

            var clean = false;
            while (!clean)
            {
                clean = true;

                for (var i = 0; i < plain.Count; i++)
                {
                    if (plain[i].Products > 0 || plain[i].Childs.Any())
                        continue;

                    plain.RemoveAt(i);
                    i--;
                    clean = false;
                }

                foreach (var p in plain)
                {
                    p.Childs = plain
                        .Where(x => x.ParentCompanyCategoryId == p.CompanyCategoryId)
                        .ToList();
                }
            }

            var categories = plain
                .Where(x => x.ParentCompanyCategoryId == 0)
                .ToList();

            categories = OrderBy(categories, model.Filtration);

            return new AdminCompanyCategoriesModel
            {
                CompanyId = model.CompanyId,
                Categories = categories
            };
        }

        [HttpGet("company-category")]
        public AdminCompanyCategoryItem ExtractCompanyCategory(int companyId, int categoryId)
        {
            return _dbCompany.ExtractCompanyCategory(companyId, categoryId);
        }

        private List<AdminCompanyCategoryModel> OrderBy(List<AdminCompanyCategoryModel> source, CompanyCategoryFiltration filtration)
        {
            if (source == null)
                return null;

            foreach (var s in source)
                s.Childs = OrderBy(s.Childs, filtration);

            switch (filtration)
            {
                case CompanyCategoryFiltration.Available:
                    return source.OrderByDescending(x => x.TotalProducts)
                        .ToList();
                case CompanyCategoryFiltration.Unavailable:
                    return source
                        .OrderByDescending(x => x.MissedCategoriesBindingCount)
                        .ThenByDescending(x => x.BadCategoriesBindingCount)
                        .ThenByDescending(x => x.TotalProducts)
                        .ToList();
                default:
                    return source;
            }
        }
    }
}
