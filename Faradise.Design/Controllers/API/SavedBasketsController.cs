﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Faradise.Design.Services.Configs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Controllers.API
{
    [Route("api/savedBaskets")]
    public class SavedBasketsController : APIController
    {
        private readonly IBasketService _basketService;
        private readonly MarketplaceOptions _marketOptions;

        public SavedBasketsController(
            IBasketService basketService,
            IOptions<MarketplaceOptions> marketOptions
)
        {
            _basketService = basketService;
            _marketOptions = marketOptions.Value;
        }

        /// <summary>
        /// Добавить в корзину продукты из сохраненной корзины
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("add-product-count-to-basket-from-saved-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddProductToBasketFromSavedBasket(AddProductsToBasketFromSavedBasketModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.AddProductToBasketFromSavedBasket(user, model.SavedBasketId);
        }

        /// <summary>
        /// Получить массив продуктов сохраненной корзины
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-saved-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ShopBasketModel GetSavedBasket(int basketId)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            var cartProducts = _basketService.GetAllProductsInSavedBasket(basketId, user);
            return new ShopBasketModel
            {
                Products = cartProducts != null ? cartProducts.Select(x => new BasketProductModel
                {
                    Product = x.Product.ToModel(),
                    Count = x.Count
                }).ToArray() : new BasketProductModel[0]
            };
        }

        /// <summary>
        /// Сохранить корзину
        /// </summary>
        /// <returns></returns>
        [HttpPost("save-market-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int SaveMarketBasket([FromBody] SavedBasketModel model)
        {
            var userId = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            var basketId = _basketService.SaveBasket(model.ProductsIds, userId);
            return basketId;
        }

        /// <summary>
        /// Получить все корзины, сохраненные пользователем
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-saved-baskets-by-user")]
        [APIAuthorize(Roles = Role.Authorized)]
        public UserSavedBasketsModel GetAllSavedBasketsByUser(int userId)
        {
            var baskets = _basketService.GetAllSavedBasketByUser(userId).Select(x => new SavedBasket
            {
                Id = x.Id,
                Views = x.Views,
                Link = _marketOptions.SavedBasketsRouteBase + $"/get-saved-basket?basketId={x.Id}"
            }).ToArray();

            return new UserSavedBasketsModel
            {
                UserId = userId,
                Baskets = baskets
            };
        }

    }
}