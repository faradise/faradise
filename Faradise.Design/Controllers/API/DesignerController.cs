﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/designer")]
    public class DesignerController : APIController
    {

        private IDesignerService _designerService = null;
        private ICDNService _cdnService = null;
        private IUserService _userService = null;

        public DesignerController(IDesignerService designerService, ICDNService cdnService, IUserService userService)
        {
            _designerService = designerService;
            _cdnService = cdnService;
            _userService = userService;
        }

        /// <summary>
        /// Получить посадочную модель дизайнера
        /// </summary>
        [HttpGet("basicInfo")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public GetDesignerBasicInfoModel GetCreateModel()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var createModel = new GetDesignerBasicInfoModel
            {
                Basic = _designerService.GetBaseModel(id).ToModel(),
                Phone = _designerService.GetPhone(id),
                PortfolioLink = _designerService.GetPortfolioLink(id),
                PhotoLink = _designerService.GetPhotoLink(id),
                Styles = _designerService.GetStyles(id).ToModel(),
                Stage = _designerService.GetRegistrationStage(id)
            };
            return createModel;
        }

        /// <summary>
        /// Отправить посадочную информацию о дизайнере
        /// </summary>
        /// <param name="model">информация о дизайнере</param>
        [HttpPost("setBasicInfo")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool Create([FromBody] SetDesignerBasicInfoModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;

            _designerService.SetBaseModel(id, model.Basic.ToService());
            _designerService.SetPortfolioLink(id, model.PortfolioLink);
            _designerService.SetStyles(id, model.Styles.ToService());
            _userService.AddUserName(id, model.Basic.Name);
            _userService.AddEmail(id, model.Basic.Email);

            return true;
        }

        /// <summary>
        /// Установить возможности дизайнера
        /// </summary>
        /// <param name="model">Возможности дизайнера</param>
        [HttpPost("setPossibilities")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetPossibilities([FromBody] DesignerPossibilityModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetPossibilities(id, model.ToService());
            return true;
        }

        /// <summary>
        /// Получить информацию о тестовом задании
        /// </summary>
        [HttpGet("getTestJobInfo")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public GetTestJobInfo TestJobInfo()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var test = _designerService.GetTestJobInfo(id);
            if (test != null)
            {
                var twentyFourHours = new DateTime().AddHours(24).Ticks;
                var time = (int)(TimeSpan.FromTicks(twentyFourHours - (DateTime.UtcNow.Ticks - test.TestTime.Ticks)).TotalSeconds);
                if (time < 0)
                    time = 0;
                var info = new GetTestJobInfo
                {
                    Name = _designerService.GetBaseModel(id).ToModel().Name,
                    Level = _designerService.GetLevel(id),
                    TestLink = test.TestLink,
                    TestTime = time, //TODO проверить!
                    TestApproved = test.TestApproved
                };
                return info;
            }
            else
                throw new ServiceException("User not found", "user_not_found");
        }

        /// <summary>
        /// Отправить ссылку на тестовое задание
        /// </summary>
        /// <param name="model">информация о дизайнере</param>
        [HttpPost("setTestLink")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetTestLink([FromBody] SetTestLink model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetTestLink(id, model.TestLink);
            return true;
        }

        /// <summary>
        /// Загрузить фото пользователя и вернуть адрес фото
        /// </summary>
        /// <param name="photo">Форма с фотографией</param>
        [HttpPost("addAvatar")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public async Task<string> AddAvatar([FromForm] DesignerAvatarModel photo)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var path = await _cdnService.UploadData(photo.Photo, _cdnService.GetPathForDesignerAvatar(id));
            _designerService.AddAvatar(id, path);
            return path;
        }

        /// <summary>
        /// Удалить фото пользователя
        /// </summary>
        [HttpGet("removeAvatar")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool RemoveAvatar()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.RemoveAvatar(id);
            return true;
        }

        /// <summary>
        /// Получить полную модель дизайнера
        /// </summary>
        [HttpGet("fullInfo")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public DesignerModel GetFullModel()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var designer = _designerService.GetDesigner(id).ToModel();
            return designer;
        }

        /// <summary>
        /// Получить результаты теста
        /// </summary>
        [HttpGet("testResults")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public TestResultsModel GetTest()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var test = _designerService.GetTest(id).ToModel();
            return test;
        }

        /// <summary>
        /// Отправить результаты теста
        /// </summary>
        /// <param name="model">результаты теста</param>
        [HttpPost("setTestResults")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetTest([FromBody] TestResultsModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetTest(id, model.ToService());
            return true;
        }

        /// <summary>
        /// Создать портфолио
        /// </summary>
        [HttpGet("createPortfolio")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public int CreatePortfolio()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var portfolio = _designerService.CreatePortfolio(id);
            return portfolio;
        }

        /// <summary>
        /// Получить портфолио
        /// </summary>
        [HttpGet("portfolio")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public PortfolioModel GetPortfolio()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var portfolio = _designerService.GetPortfolio(id).ToModel();
            return portfolio;
        }

        /// <summary>
        /// Получить портфолио по id дизйнера
        /// </summary>
        [HttpGet("portfolio-for-specific-designer")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PortfolioModel GetPortfolio(int designerId)
        {
            var portfolio = _designerService.GetPortfolio(designerId).ToModel();
            return portfolio;
        }

        /// <summary>
        /// Удалить портфолио
        /// </summary>
        /// <param name="model">портфолио</param>
        [HttpPost("removePortfolio")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetPortfolio([FromBody] RemovePortfolioModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.RemovePortfolio(id, model.Id);
            return true;
        }

        /// <summary>
        /// Отправить название и описание портфолио
        /// </summary>
        /// <param name="model">портфолио</param>
        [HttpPost("setPortfolioDescription")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetPortfolio([FromBody] PortfolioDescriptionModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetPortfolio(id, model.Id, model.Name, model.Description);
            return true;
        }

        /// <summary>
        /// Отправить массив названий и описаний портфолио
        /// </summary>
        /// <param name="model">портфолио</param>
        [HttpPost("setPortfoliosDescription")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetPortfolios([FromBody] PortfolioDescriptionsModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetPortfolios(id, model.ToService());
            return true;
        }

        /// <summary>
        /// Добавить фото в портфолио
        /// </summary>
        /// <param name="model">портфолио</param>
        [HttpPost("addPortfolioPhoto")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public async Task<PortfolioPhotoModel> AddPortfolioPhoto([FromForm] AddPortfolioPhotoModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var path = await _cdnService.UploadData(model.Photo, _cdnService.GetPathForDesignerPortfolioPhoto(id, model.PortfolioId));
            var photoId = _designerService.AddPortfolioPhoto(id, model.PortfolioId, path);
            return new PortfolioPhotoModel { PhotoLink = path, Id = photoId };
        }

        /// <summary>
        /// Удалить фото из портфолио
        /// </summary>
        [HttpPost("removePortfolioPhoto")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool RemovePortfolioPhoto([FromBody] RemovePortfolioPhotoModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.RemovePortfolioPhoto(id, model.ToService());
            return true;
        }

        /// <summary>
        /// Получить юридические данные
        /// </summary>
        [HttpGet("legal")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public DesignerLegalModel GetLegalModel()
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var legaldata = _designerService.GetLegalModel(id).ToModel();
            return legaldata;
        }

        /// <summary>
        /// Отправить юридические данные
        /// </summary>
        /// <param name="model">юридические данные</param>
        [HttpPost("setLegal")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public bool SetLegalModel([FromBody] DesignerLegalModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            _designerService.SetLegalModel(id, model.ToService());
            return true;
        }

        /// <summary>
        /// Получить список вопросов
        /// </summary>
        [HttpGet("getQuestions")]
        [APIAuthorize(Roles = Role.Authorized | Role.Designer)]
        public TestQuestionDescriptionModel[] GetDesignerTest()
        {
            return _designerService
                .GetDesignerQuestions()
                .Select(x => x.ToModel())
                .ToArray();
        }
    }
}
