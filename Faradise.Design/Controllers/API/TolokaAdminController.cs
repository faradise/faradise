﻿using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Toloka;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("/api/toloka-admin")]
    public class TolokaAdminController : APIController
    {
        private ITolokaService _tolokaService = null;
        private ICDNService _cdn = null;

        public TolokaAdminController(ITolokaService tolokaService, ICDNService cdn)
        {
            _tolokaService = tolokaService;
            _cdn = cdn;
        }

        [HttpPost("add-task")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<bool> AddTask([FromForm] UploadTSVModel model)
        {
            return await _tolokaService.UploadTaskToCDN(model);
        }

        [HttpGet("get-tasks")]
        [APIAuthorize(Roles = Role.Authorized)]
        public TolokaTaskModel[] GetTasks()
        {
            return _tolokaService.GetTasks();
        }

        [HttpPost("delete-task")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteTask([FromBody] int taskId)
        {
            _tolokaService.DeleteTask(taskId);
        }

        [HttpGet("get-segments")]
        [APIAuthorize(Roles = Role.Authorized)]
        public TolokaTaskSegmentModel[] GetSegments(int taskId)
        {
            return _tolokaService.GetSegments(taskId);
        }

        [HttpPost("restart-segment")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RestartSegment([FromBody] int segmentId)
        {
            _tolokaService.RestartSegment(segmentId);
        }
    }
}
