﻿using System;

namespace Faradise.Design.Controllers.API.Models.Tilda
{
    public class TildaLotteryRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
    }
}
