﻿using Faradise.Design.Controllers.API.Models.User;

namespace Faradise.Design.Controllers.API.Models.Authentication
{
    public class LoggedInModel
    {
        public string Token { get; set; }
        public AuthorizeModel User { get; set; }
    }
}
