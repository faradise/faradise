﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Authentication
{
    public class RequestCodeModel
    {
        /// <summary>
        /// Номер телефона
        /// </summary>
        [Required] public string Phone { get; set; }
    }
}
