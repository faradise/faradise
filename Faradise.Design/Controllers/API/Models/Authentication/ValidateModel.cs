﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Authentication
{
    public class ValidateModel
    {
        /// <summary>
        /// JWT токен для проверки
        /// </summary>
        [Required] public string Token { get; set; }
    }
}
