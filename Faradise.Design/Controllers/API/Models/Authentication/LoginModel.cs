﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Authentication
{
    public class LoginModel
    {
        /// <summary>
        /// Номер телефона
        /// </summary>
        [Required] public string Phone { get; set; }

        /// <summary>
        /// Код подтверждения высланный на номер телефона
        /// </summary>
        [Required] public string Code { get; set; }
    }
}
