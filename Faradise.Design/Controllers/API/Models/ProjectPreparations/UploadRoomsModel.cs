﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class UploadRoomsModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Инофрмация о комнатах и их размерах
        /// </summary>
        [Required]
        public List<ShortRoomInfoModel> RoomsInformation { get; set; }
    }
} 
