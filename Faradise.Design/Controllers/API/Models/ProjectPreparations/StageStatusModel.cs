﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class StageStatusModel
    {
        /// <summary>
        /// Текущая стадия 2го шага
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public CooperateStage Stage { get; set; }
        /// <summary>
        /// Клиент завершил стадию
        /// </summary>
        [Required]
        public bool ClientIsReady { get; set; }
    }
}
