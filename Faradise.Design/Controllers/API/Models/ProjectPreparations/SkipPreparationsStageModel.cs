﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class SkipPreparationsStageModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Стадия 2го шага, которую мы пропускаем
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public CooperateStage Stage { get; set; }
    }
}
