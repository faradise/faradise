﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class RoomProportionsModel
    {
        /// <summary>
        /// Размер одной стороны
        /// </summary>
        [Required]
        public int SideA { get; set; }

        /// <summary>
        /// Размер другой стороны стороны
        /// </summary>
        [Required]
        public int SideB { get; set; }

        /// <summary>
        /// Высота дверного проема
        /// </summary>
        public int HeightDoor { get; set; }

        /// <summary>
        /// Ширина дверного проема
        /// </summary>
        public int WidthDoor { get; set; }

        /// <summary>
        /// Высота потолков
        /// </summary>
        public int HeightCeiling { get; set; }
    }
}
