﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class SetAdditionalInformationModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Список дополнительной инфы по каждой комнате
        /// </summary>
        public List<AdditionalInformationForRoomModel> Information { get; set; }
    }
}
