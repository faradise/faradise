﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class RoomInfoModel
    {
        /// <summary>
        /// Index комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Назначение комнаты
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Room Purpose { get; set; }

        /// <summary>
        /// Фото комнаты
        /// </summary>
        public List<PhotoModel> Photos { get; set; }

        /// <summary>
        /// План комнаты
        /// </summary>
        public string PlanLink { get; set; }

        /// <summary>
        /// Размеры
        /// </summary>
        public RoomProportionsModel Proportions { get; set; }
    }
}
