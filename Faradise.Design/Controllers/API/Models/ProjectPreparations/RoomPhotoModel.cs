﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class PhotoModel
    {
        /// <summary>
        /// id фото
        /// </summary>
        [Required]
        public int PhotoId { get; set; }

        /// <summary>
        /// Ссылка
        /// </summary>
        [Required]
        public string Link { get; set; }
    }
}
