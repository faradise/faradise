﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class OldFurnitureModel
    {
        /// <summary>
        /// Фото старой мебели
        /// </summary>
        public List<PhotoModel> FurniturePhotos { get; set; }
        /// <summary>
        /// Фото старой отделки
        /// </summary>
        public List<PhotoModel> FurnishPhotos { get; set; }
    }
}
