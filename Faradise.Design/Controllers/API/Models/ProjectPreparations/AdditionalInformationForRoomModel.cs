﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class AdditionalInformationForRoomModel
    {
        /// <summary>
        /// Index комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

    
        public List<AdditionalQuestionModel> Questions { get; set; }
    }
}
