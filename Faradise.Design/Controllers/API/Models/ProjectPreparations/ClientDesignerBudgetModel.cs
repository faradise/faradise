﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class ClientDesignerBudgetModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Бюджет на мебель
        /// </summary>
        [Required]
        public int FurnitureBudget {get;set;}

        /// <summary>
        /// Бюджет на ремонт
        /// </summary>
        [Required]
        public int RenovationBudget { get; set; }

    }
}
