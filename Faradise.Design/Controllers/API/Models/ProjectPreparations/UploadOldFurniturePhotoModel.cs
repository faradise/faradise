﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectPreparations
{
    public class UploadOldFurniturePhotoModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        public IFormFile File { get; set; }
    }
}
