﻿using Faradise.Design.Models.Enums;
using System;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalProducts { get; set; }
        public int MappedProducts { get; set; }
        public int TotalCategories { get; set; }
        public int MappedCategories { get; set; }

        public bool NotInTradeCategories { get; set; }
        public bool MapNotToLeafCategories { get; set; }

        public bool IsEnabled { get; set; }

        public CompanyLastFeedInfo LastFeed { get; set; }
    }
}
