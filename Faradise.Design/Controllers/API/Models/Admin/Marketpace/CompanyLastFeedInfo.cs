﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyLastFeedInfo
    {
        public YMLUpdateTaskStatus LastFeedStatus { get; set; }
        public DateTime LastFeedStatusChangeTime { get; set; }
        public YMLDownloadSource Source { get; set; }
    }
}
