﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class ChangeCompanyNoteModel
    {
        [Required]
        public int CompanyId { get; set; }
        public int NoteId { get; set; }
        public string NewNote { get; set; }
    }
}
