﻿using Faradise.Design.Marketplace.Models.Validation;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class UploadResultModel
    {
        public int CompanyId { get; set; }
        public string Upload { get; set; }
        public string Company { get; set; }
        public int Categories { get; set; }
        public int Products { get; set; }
        public ValidationReport ValidationReport { get; set; }
    }
}