﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class AdminProductCollectionFullModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string KeyWord { get; set; }
        public bool Available { get; set; }
        public List<AdminProductCollectionNameModel> Products { get; set; }
    }
}
