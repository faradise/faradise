﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class DailyProductModel
    {
        public DateTime OfferTime { get; set; }
        public PossibleDailyProductModel Product { get; set; }
    }
}
