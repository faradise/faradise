﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyCategoryRoomModel
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public int RoomNameId { get; set; }
    }
}
