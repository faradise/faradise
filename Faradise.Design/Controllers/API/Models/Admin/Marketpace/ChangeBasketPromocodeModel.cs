﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class ChangeBasketOrderPromocodeModel
    {
        [Required]
        public int OrderId { get; set; }
        public string PromoCode { get; set; }
    }
}
