﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class AddCompanyNoteModel
    {
        [Required]
        public int CompanyId { get; set; }
        [Required]
        public string Note { get; set; }
    }
}
