﻿namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class ProductInfoModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductPrice { get; set; }
        public string Seller { get; set; }
        public string Vendor { get; set; }
        public string VendorLink { get; set; }
        public string CompanyName { get; set; }
        public string PhotoUrl { get; set; }
        public string CompanyCategoryName { get; set; }
        public string CategoryName { get; set; }
        public string VendorCode { get; set; }

        public float? Height { get; set; }
        public float? Width { get; set; }
        public float? Length { get; set; }
    }
}
