﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompaniesModel
    {
        public List<CompanyInfo> Companies { get; set; }
        public int TotalCompanies { get; set; }
        public int TotalProducts { get; set; }
        public int MappedProducts { get; set; }
    }
}
