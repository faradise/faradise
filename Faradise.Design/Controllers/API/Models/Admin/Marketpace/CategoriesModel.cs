﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CategoriesModel
    {
        public List<CategoryModel> Categories { get; set; }
    }
}
