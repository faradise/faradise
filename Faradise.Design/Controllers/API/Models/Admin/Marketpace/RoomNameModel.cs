﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class RoomNameModel
    {
        public int RoomNameId { get; set; }
        public string Name { get; set; }
    }
}
