﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class ModifyCompanyModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string LegalName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhone { get; set; }
        public string ContactPersonEmail { get; set; }
        public bool HasFurniture { get; set; }
        public bool HasDesignFurniture { get; set; }
        public bool HasMaterials { get; set; }
        public bool HasElectric { get; set; }
        public string DeliveryInfo { get; set; }
        public string FeedUrl { get; set; }
        public int DailyProductPriority { get; set; }
        public bool DontSupportPromocode { get; set; }
    }
}
