﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CategoryImageModel
    {
        public int ImageId { get; set; }
        public string Url { get; set; }
    }
}
