﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyCategoriesModel
    {
        public int CompanyId { get; set; }
        public List<CompanyCategoryModel> Categories { get; set; }
    }
}
