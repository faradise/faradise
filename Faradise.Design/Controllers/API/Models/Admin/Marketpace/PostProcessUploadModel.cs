﻿namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class PostProcessUploadModel
    {
        public int Id { get; set; }
        public string Upload { get; set; }
    }
}
