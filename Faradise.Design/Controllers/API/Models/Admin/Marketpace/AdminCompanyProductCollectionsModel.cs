﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class AdminCompanyProductCollectionsModel
    {
        public int CompanyId { get; set; }
        public List<AdminProductCollectionShortModel> Collections { get; set; }
    }
}
