﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class ChangePaymentMethodModel
    {
        [Required]
        public int OrderId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
    }
}
