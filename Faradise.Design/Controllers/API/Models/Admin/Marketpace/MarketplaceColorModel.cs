﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class MarketplaceColorModel
    {
        public int ColorId { get; set; }
        public string Name { get; set; }
        public string Hex { get; set; }
    }
}
