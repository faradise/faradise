﻿namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class GetCategoryProductsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CompanyCategoryId { get; set; }
        public string CompanyCategoryName { get; set; }
        public string Url { get; set; }

        public int Price { get; set; }

        public string[] Pictures { get; set; }

        public string Seller { get; set; }
        public string Vendor { get; set; }
        public string VendorCode { get; set; }

        public float? Height { get; set; }
        public float? Width { get; set; }
        public float? Length { get; set; }
    }
}
