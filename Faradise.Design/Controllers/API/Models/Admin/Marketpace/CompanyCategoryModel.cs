﻿namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyCategoryModel
    {
        public int CompanyCategoryId { get; set; }
        public int ParentCompanyCategoryId { get; set; }
        public string CompanyCategory { get; set; } // CompanyCategoryName
        public RoomNameModel[] Rooms { get; set; }
        public int Depth { get; set; }
        public int Products { get; set; } // ProductsCount

        public int Category { get; set; } //CategoryId
        public bool BadCategoryBinding { get; set; }
    }
}