﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyColorModel
    {
        public int Id { get; set; }
        public int? MarketplaceColorId { get; set; }
        public string Name { get; set; }
        public string MarketplaceColorName { get; set; }
    }
}
