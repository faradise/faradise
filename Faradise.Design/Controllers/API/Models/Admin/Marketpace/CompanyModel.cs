﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Admin.Marketpace
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public string LegalName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhone { get; set; }
        public string ContactPersonEmail { get; set; }
        public bool HasFurniture { get; set; }
        public bool HasDesignFurniture { get; set; }
        public bool HasMaterials { get; set; }
        public bool HasElectric { get; set; }
        public string DeliveryInfo { get; set; }
        public List<string> AdditionalNotes { get; set; }

        public string FeedUrl { get; set; }
        public bool AutoLoadFeed { get; set; }

        public bool IsEnabled { get; set; }

        public int AllProducts { get; set; }
        public int AllCategories { get; set; }

        public int CanTradeCategories { get; set; }
        public int CanTradeProducts { get; set; }

        public int InTradeCategories { get; set; }
        public int InTradeProducts { get; set; }

        public int NotInTradeCategories { get; set; }
        public int NotInTradeProducts { get; set; }

        public int SalesCategories { get; set; }
        public int SalesProducts { get; set; }

        public bool ProductsNotInLeafs { get; set; }

        public int? MinSale { get; set; }
        public int? MaxSale { get; set; }
        public CompanyLastFeedModel CompanyLastFeedModel { get; set; }

        public int DailyProductPriority { get; set; }
        public bool DontSupportPromocode { get; set; }
    }
}
