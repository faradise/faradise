﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AdminARProductModel
    {
        public int Id { get; set; }
        public int FaradiseProductId { get; set; }
        public int FaradiseColorId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string VendorCode { get; set; }
        public bool Vertiacal { get; set; }
        public string Render { get; set; }

        public AdminARPlatformModel IosBFM { get; set; }
        public AdminARPlatformModel IosSCN { get; set; }

        public AdminARPlatformModel AndroidBFM { get; set; }
        public AdminARPlatformModel AndroidSFB { get; set; }
    }
}
