﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AdminARProductShortModel
    {
        public int Id { get; set; }
        public string FaradiseName { get; set; }
        public int FaradiseId { get; set; }
        public string Color { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
    }
}
