﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class FullBasketOrderModel
    {
        public int OrderId { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }
        public int BasePrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int DeliveryPrice { get; set; }
        public BasketProductModel[] Products { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public int? PaymentId { get; set; }
        public string PaymentLink { get; set; }
        public string PlatformLink { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime CreationTime { get; set; }
        public PaymentMethod SelectedPaymentMethod { get; set; }
        public bool PriceMissmatch { get; set; }
        public string PromoCode { get; set; }
        public int PromoCodePercents { get; set; }
        public int PromoCodeAbsolute { get; set; }
        public int AbsoluteDiscount { get; set; }
    }
}
