﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectPreparations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AdminProjectModel
    {
        public int Id { get; set; }

        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }

        public string Name { get; set; }
        public string City { get; set; }
        public int BudgetFurniture { get; set; }
        public int BudgetRenovation { get; set; }
        public bool DesignerIsApproved { get; set; }
        public int AppointedDesignersCount { get; set; }
        public int RejectedDesignersCount { get; set; }
        public int DesignerId { get; set; }
        public bool NeedAdmin { get; set; }
        public bool HasChangesAfterLastAdminCheck { get; set; }

        public ProjectStage Stage { get; set; }
        public ProjectStatus Status { get; set; }
        public ProjectTargetType ObjectType { get; set; }
        public TestResults TestAnswers { get; set; }

        public List<Room> Rooms { get; set; }

        public List<StyleModel> Styles { get; set; }

        public List<ReasonModel> Reasons { get; set; }

        public bool IsFilled { get; set; }
        public DateTime FillTime { get; set; }


        public DesignerRequirements DesignerRequirements { get; set; }

        public CooperateStage PreparationStage { get; set; }

        public List<RoomInfoModel> RoomInfosAndProportions { get; set; }

        public List<AdditionalInformationForRoomModel> RoomAdditionsInfos { get; set; }

        public List<ClientDesignerStyleModel> PreparationsStyles { get; set; }

        public ClientDesignerBudgetModel PreparationBudget { get; set; }

        public OldFurnitureModel OldStuffPhotos { get; set; }

        public DevelopmentStage DevelopmentStage { get; set; }
        public List<MoodboardModel> Moodsboards { get; set; }
        public List<CollageModel> Collages { get; set; }
        public List<ZoneModel> Zones { get; set; }
        public FullVisualizationModel[] Visualizations { get; set; }
        public List<PlanModel> WorkPlans { get; set; }

        public AdminProjectProductModel[] Products { get; set; }
    }
}
