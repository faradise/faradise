﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class BasketProductModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int BasePrice { get; set; }
        public int Count { get; set; }
        public string Vendor { get; set; }
        public string VendorCode { get; set; }
        public string VendorLink { get; set; }
    }
}
