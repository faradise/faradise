﻿using Faradise.Design.Models.Enums;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class DesignerRejectedInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string RejectReason { get; set; }
        public int AppointedProjectsCount { get; set; }
        public int ProjectsInWorkCount { get; set; }
        public int RejectedProjectsCount { get; set; }
        public DesignerRegistrationStage Stage { get; set; }
    }
}
