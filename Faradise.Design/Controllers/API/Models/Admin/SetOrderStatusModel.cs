﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class SetOrderStatusModel
    {
        public int OrderId { get; set; }
        public OrderStatus Status { get; set; }
    }
}
