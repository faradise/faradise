﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class FullProductProjectOrderModel
    {
        public int OrderId { get; set; }
        public int ProjectId { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }
        public int BasePrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int DeliveryPrice { get; set; }
        public ProductOrderItemsModel[] Items { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public int? PaymentId { get; set; }
        public string PaymentLink { get; set; }
        public string PlatformLink { get; set; }
        public DateTime CreationTime { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public bool PriceMissMatch { get; set; }
    }
}
