﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDevelopment;
using System;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AdminProjectInfo
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        public string ClientName { get; set; }
        public string ClientPhone { get; set; }
        public string ClientEmail { get; set; }
        public DateTime CreationDate { get; set; }

        public bool Filled { get; set; }

        public bool NeedAdmin { get; set; }
        public bool HasChangesAfterLastAdminCheck { get; set; }

        public ProjectStatus Status { get; set; }
        public ProjectStage Stage { get; set; }

        public CooperateStage CooperateStage { get; set; }
        public DevelopmentStage DevelopmentStage { get; set; }
        public int OrderCount { get; set; }

        public int AppointedDesigners { get; set; }
        public int RejectedDesigners { get; set; }

        public int? DesignerId { get; set; }
        public int? ClientId { get; set; }
    }
}
