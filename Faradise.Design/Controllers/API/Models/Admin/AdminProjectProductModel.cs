﻿using Faradise.Design.Models;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AdminProjectProductModel
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public int Count { get; set; }
        public Room RoomName { get; set; }
        public int RoomId { get; set; }
    }
}
