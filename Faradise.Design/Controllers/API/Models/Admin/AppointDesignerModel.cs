﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class AppointDesignerModel
    {
        public int ProjectId { get; set; }
        public int[] DesignersIds { get; set; }
    }
}
