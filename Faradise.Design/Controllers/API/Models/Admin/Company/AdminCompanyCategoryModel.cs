﻿using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Controllers.API.Models.Admin.Company
{
    public class AdminCompanyCategoryModel
    {
        public int CompanyCategoryId { get; set; }
        public int ParentCompanyCategoryId { get; set; }

        public int Products { get; set; }

        public int? CategoryId { get; set; }
        public bool BadCategoryBinding { get; set; }

        public List<AdminCompanyCategoryModel> Childs { get; set; }
        public int TotalProducts { get { return Products + Childs?.Sum(x => x.Products) ?? 0; } }

        public int BadCategoriesBindingCount
        {
            get
            {
                return BadCategoryBinding
                    ? 1
                    : 0
                      +
                      Childs?.Sum(x => x.BadCategoriesBindingCount) ?? 0;
            }

        }

        public int MissedCategoriesBindingCount
        {
            get
            {
                return CategoryId == null && Products > 0
                    ? 1
                    : 0
                      +
                      Childs?.Sum(x => x.MissedCategoriesBindingCount) ?? 0;
            }
        }
    }
}
