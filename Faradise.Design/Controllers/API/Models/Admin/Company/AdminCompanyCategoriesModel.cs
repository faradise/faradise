﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Admin.Company
{
    public class AdminCompanyCategoriesModel
    {
        public int CompanyId { get; set; }
        public List<AdminCompanyCategoryModel> Categories { get; set; }
    }
}
