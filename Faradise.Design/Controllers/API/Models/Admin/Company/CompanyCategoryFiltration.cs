﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin.Company
{
    public enum CompanyCategoryFiltration
    {
        Default,
        Available,
        Unavailable
    }
}
