﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ServiceStack.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Admin.Company
{
    public class GetCompanyCategoriesModel
    {
        [Required, Range(1, int.MaxValue)]
        public int CompanyId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public CompanyCategoryFiltration Filtration { get; set; }
    }
}
