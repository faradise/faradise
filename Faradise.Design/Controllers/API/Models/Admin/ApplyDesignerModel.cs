﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class ApplyDesignerModel
    {
        public int DesignerId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public DesignerLevel Level { get; set; }
    }
}
