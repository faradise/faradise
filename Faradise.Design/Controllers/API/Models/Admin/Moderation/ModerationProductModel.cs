﻿namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class ModerationProductModel
    {
        public int ProductId { get; set; }
        public int Rating { get; set; }
        public int[] ColorId { get; set; }
        public string Description { get; set; }
    }
}
