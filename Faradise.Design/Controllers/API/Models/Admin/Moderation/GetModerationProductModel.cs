﻿namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class GetModerationProductModel
    {
        public int Id { get; set; }
        public string[] PhotoUrls { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int[] Colors { get; set; }
        public int[] MarketplaceColors { get; set; }
    }
}
