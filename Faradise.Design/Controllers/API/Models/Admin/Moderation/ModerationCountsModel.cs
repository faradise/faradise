﻿namespace Faradise.Design.Admin.Models
{
    public class ModerationCountsModel
    {
        public int ProductWithoutColorCount { get; set; }
        public int ProductWithoutRatingCount { get; set; }
    }
}
