﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class RemoveDesignerFromPoolModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        public int DesignerId { get; set; }
    }
}
