﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class CompleteVisualizationModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        public string VisualizationLink { get; set; }
    }
}
