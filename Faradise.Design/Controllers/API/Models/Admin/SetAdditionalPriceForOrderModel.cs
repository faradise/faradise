﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class SetAdditionalPriceForOrderModel
    {
        public int OrderId { get; set; }
        public int Price { get; set; }
    }
}
