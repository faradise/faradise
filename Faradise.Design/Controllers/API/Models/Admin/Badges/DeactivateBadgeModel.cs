﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class DeactivateBadgeModel
    {
        public int BadgeId { get; set; }
    }
}
