﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class BadgeModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public string Name { get; set; }
        public int[] Categories { get; set; }
        public int[] Companies { get; set; }
        public int[] Products { get; set; }
        public bool AllMarketplace { get; set; }
        public bool IsActive { get; set; }
    }
}
