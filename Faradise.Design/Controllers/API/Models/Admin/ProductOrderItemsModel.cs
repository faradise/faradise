﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class ProductOrderItemsModel
    {
        public int RoomId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
    }
}
