﻿using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class FullVisualizationModel
    {
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public int Price { get; set; }
        public bool IsSelected { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
        public bool IsComplete { get; set; }
        public string VisualizationCode { get; set; }
    }
}
