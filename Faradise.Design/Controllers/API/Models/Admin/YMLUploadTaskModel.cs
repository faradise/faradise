﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Admin
{
    public class YMLUploadTaskModel
    {
        public int TaskId { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreationTime { get; set; }
        public int Priority { get; set; }
        public string YmlLink { get; set; }
        public YMLUpdateTaskStatus Status { get; set; }
        public string FailMessage { get; set; }
    }
}
