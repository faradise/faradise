﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class PortfolioDescriptionModel
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Название проекта портфолио
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Описание проекта портфолио
        /// </summary>
        public string Description { get; set; }
    }
}
