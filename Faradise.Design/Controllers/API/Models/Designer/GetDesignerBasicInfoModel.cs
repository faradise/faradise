﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class GetDesignerBasicInfoModel
    {
        /// <summary>
        /// Основная информация
        /// </summary>
        [Required]
        public DesignerBaseModel Basic { get; set; }

        /// <summary>
        /// Стадия регистрации
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public DesignerRegistrationStage Stage { get; set; }

        /// <summary>
        /// Телефон дизайнера
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Портфолио
        /// </summary>
        public string PortfolioLink { get; set; }

        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoLink { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        [Required]
        public DesignerStyleModel[] Styles { get; set; }
    }
}
