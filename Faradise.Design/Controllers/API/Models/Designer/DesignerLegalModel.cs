﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class DesignerLegalModel
    {
        /// <summary>
        /// Юридическое или физическое лицо
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public LegalType LegalType { get; set; }

        /// <summary>
        /// Юридическое лицо
        /// </summary>
        public CompanyLegalInfoModel CompanyLegalInfo { get; set; }

        /// <summary>
        /// Физическое лицо
        /// </summary>
        public PersonalLegalInfoModel PersonalLegalInfo { get; set; }
    }
}
