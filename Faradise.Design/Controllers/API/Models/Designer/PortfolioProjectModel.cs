﻿namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class PortfolioProjectModel
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название проекта портфолио
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание проекта портфолио
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Фотографии проекта портфолио
        /// </summary>
        public PortfolioPhotoModel[] PhotoLinks { get; set; }
    }
}
