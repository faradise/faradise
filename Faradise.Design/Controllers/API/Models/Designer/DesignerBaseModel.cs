﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class DesignerBaseModel
    {
        /// <summary>
        /// Имя дизайнера
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Фамилия дизайнера
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Возраст дизайнера
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Город дизайнера
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Пол дизайнера
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        /// <summary>
        /// Email дизайнера
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// О себе
        /// </summary>
        public string About { get; set; }

        /// <summary>
        /// Опыт работы
        /// </summary>
        public WorkExperience WorkExperience { get; set; }

        public DesignerRegistrationStage Stage { get; set; }
    }
}
