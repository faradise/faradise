﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class DesignerModel
    {
        /// <summary>
        /// Id дизайнера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Стадия регистрации
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public DesignerRegistrationStage Stage { get; set; }

        /// <summary>
        /// Основная информация
        /// </summary>
        [Required]
        public DesignerBaseModel Base { get; set; }

        /// <summary>
        /// Телефон дизайнера
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Ссылка на портфолио
        /// </summary>
        public string PortfolioLink { get; set; }

        /// <summary>
        /// Портфолио
        /// </summary>
        [Required]
        public PortfolioModel Portfolio { get; set; }

        /// <summary>
        /// Тестовое задание
        /// </summary>
        public TestJobInfoModel TestJob { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        [Required]
        public DesignerStyleModel[] Styles { get; set; }

        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoLink { get; set; }

        /// <summary>
        /// Уровень (ступень) дизайнера
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public DesignerLevel Level { get; set; }

        /// <summary>
        /// Возможности
        /// </summary>
        [Required]
        public DesignerPossibilityModel Possibilities { get; set; }

        /// <summary>
        /// Резултаты теста подбора дизайнера
        /// </summary>
        [Required]
        public TestResultsModel Testing { get; set; }

        /// <summary>
        /// Юридические данные
        /// </summary>
        [Required]
        public DesignerLegalModel LegalModel { get; set; }
    }
}
