﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class PortfolioDescriptionsModel
    {
        /// <summary>
        /// Массив портфолио
        /// </summary>
        [Required]
        public PortfolioDescriptionModel[] Descriptions { get; set; }
    }
}
