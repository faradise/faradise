﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class PortfolioModel
    {
        [Required]
        public PortfolioProjectModel[] Portfolios { get; set; }
    }    
}
