﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class DesignerAvatarModel
    {
        /// <summary>
        /// Фото файл
        /// </summary>
        [Required]
        public IFormFile Photo { get; set; }
    }
}
