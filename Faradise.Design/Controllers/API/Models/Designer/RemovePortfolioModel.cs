﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class RemovePortfolioModel
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        [Required]
        public int Id { get; set; }
    }
}
