﻿using System;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class TestJobInfoModel
    {
        /// <summary>
        /// Ссылка на тестовое задание
        /// </summary>
        public string TestLink { get; set; }

        /// <summary>
        /// Время отправки тестового задания
        /// </summary>
        public DateTime TestTime { get; set; }

        /// <summary>
        /// Заявка одобрена
        /// </summary>
        public bool TestApproved { get; set; }
    }
}
