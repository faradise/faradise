﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class SetDesignerBasicInfoModel
    {
        /// <summary>
        /// Основная информация
        /// </summary>
        [Required]
        public DesignerBaseModel Basic { get; set; }

        /// <summary>
        /// Портфолио
        /// </summary>
        public string PortfolioLink { get; set; }

        ///// <summary>
        ///// Ссылка на тестовое задание
        ///// </summary>
        //public string TestJobLink { get; set; }
        
        /// <summary>
        /// Список стилей
        /// </summary>
        [Required]
        public DesignerStyleModel[] Styles { get; set; }
    }
}
