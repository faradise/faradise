﻿namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class PortfolioPhotoModel
    {
        /// <summary>
        /// Id фото
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Фотографии проекта портфолио
        /// </summary>
        public string PhotoLink { get; set; }
    }
}
