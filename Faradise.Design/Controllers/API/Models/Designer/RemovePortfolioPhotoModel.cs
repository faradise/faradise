﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class RemovePortfolioPhotoModel
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        [Required]
        public int PortfolioId { get; set; }

        /// <summary>
        /// Id фото
        /// </summary>
        [Required]
        public int PhotoId { get; set; }
    }
}
