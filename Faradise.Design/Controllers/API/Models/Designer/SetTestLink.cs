﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class SetTestLink
    {
        /// <summary>
        /// Ссылка на тестовое задание
        /// </summary>
        [Required]
        public string TestLink { get; set; }
    }
}
