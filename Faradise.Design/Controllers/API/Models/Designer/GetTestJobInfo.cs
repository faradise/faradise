﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class GetTestJobInfo
    {
        /// <summary>
        /// Имя дизайнера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Ссылка на тестовое задание
        /// </summary>
        public string TestLink { get; set; }

        /// <summary>
        /// Количество секнд до окончания
        /// </summary>
        public int TestTime { get; set; }

        /// <summary>
        /// Заявка одобрена
        /// </summary>
        public bool TestApproved { get; set; }

        /// <summary>
        /// Уровень (ступень) дизайнера
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public DesignerLevel Level { get; set; }
    }
}
