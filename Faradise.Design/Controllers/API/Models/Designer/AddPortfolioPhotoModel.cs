﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Designer
{
    public class AddPortfolioPhotoModel
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        [Required]
        public int PortfolioId { get; set; }

        /// <summary>
        /// Фото файл
        /// </summary>
        [Required]
        public IFormFile Photo { get; set; }
    }
}
