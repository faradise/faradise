﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Chat
{
    public class NotifyGroupModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        [MinLength(1)]
        public int[] Users { get; set; }

        [Required]
        [MinLength(1)]
        public bool[] IsFirstMessage { get; set; }
    }
}
