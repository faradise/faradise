﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Chat
{
    public class NotifySingleModel
    {
        [Required]
        public int ProjectId { get; set; }
    }
}
