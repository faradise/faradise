﻿using Faradise.Design.Utilities.Feed;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ServiceStack.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Feed
{
    public class FeedRequestModel
    {
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public FeedFormatType Format { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public FeedAvailabilityType Availability { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public FeedCostType Cost { get; set; }
    }
}
