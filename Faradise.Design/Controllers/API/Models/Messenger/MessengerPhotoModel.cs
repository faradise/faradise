﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Messenger
{
    public class MessengerPhotoModel
    {
        /// <summary>
        /// Название группы
        /// </summary>
        [Required]
        public string GroupName { get; set; }

        /// <summary>
        /// Фото файл
        /// </summary>
        [Required]
        public IFormFile Photo { get; set; }
    }
}
