﻿namespace Faradise.Design.Controllers.API.Models.Toloka
{
    public enum TolokaOutputType
    {
        SofaSize,
        SofaTwoPhoto,
        SofaPhotos,
        SofaDescription,
        AskonaDescription,
        Colors
    }
}
