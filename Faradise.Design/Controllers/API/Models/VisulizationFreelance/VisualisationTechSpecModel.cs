﻿using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.VisulizationFreelance
{
    public class VisualisationTechSpecModel
    {
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public int? WorkerId { get; set; }
        public string Description { get; set; }
        public FileModel[] Files { get; set; }
        public VisualizationWorkStatus Status { get; set; }
    }
}
