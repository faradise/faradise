﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.VisulizationFreelance
{
    public class DeleteVisualTechSepecFileModel
    {
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public int FileId { get; set; }
    }
}
