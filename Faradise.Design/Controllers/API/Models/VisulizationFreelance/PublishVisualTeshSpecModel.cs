﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.VisulizationFreelance
{
    public class PublishVisualTeshSpecModel
    {
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
    }
}
