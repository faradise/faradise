﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models
{
    public class AbandonDesignerModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int ProjectId { get; set; }
        public string Description { get; set; }
    }
}
