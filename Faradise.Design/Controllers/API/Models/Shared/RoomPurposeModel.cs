﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models
{
    public class RoomPurposeModel
    {
        /// <summary>
        /// id комнаты
        /// </summary>
        public int RoomId { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Room Purpose { get; set; }
    }
}
