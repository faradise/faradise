﻿using Faradise.Design.Controllers.API.Models.Marketplace;
namespace Faradise.Design.Controllers.API.Models.Compilations
{
    public class CompilationProductListModel
    {
        public int Count { get; set; }
        public ShortProductModel[] Products { get; set; }
    }
}
