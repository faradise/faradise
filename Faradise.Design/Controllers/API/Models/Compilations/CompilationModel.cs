﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Compilations
{
    public class CompilationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CompilationSource Source { get; set; }
        public int[] Products { get; set; }
        public int[] Categories { get; set; }
    }
}
