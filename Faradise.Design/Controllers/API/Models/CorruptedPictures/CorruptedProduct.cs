﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.CorruptedPictures
{
    public class CorruptedProduct
    {
        public string ProductName { get; set; }
        public int ProductId { get; set; }
        public List<string> CorruptedPictures { get; set; }
    }
}