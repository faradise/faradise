﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.CorruptedPictures
{
    public class CorruptedPartnersList
    {
        public List<CorruptedPartner> CorruptedPartners { get; set; }
    }
}