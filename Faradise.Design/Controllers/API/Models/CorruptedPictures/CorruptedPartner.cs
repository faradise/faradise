﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.CorruptedPictures
{
    public class CorruptedPartner
    {
        public string PartnerName { get; set; }
        public int PartnerId { get; set; }
        public List<CorruptedProduct> CorruptedProducts { get; set; }
    }
}