﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Brigade
{
    public class SetBrigadeTechSpecModel
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
    }
}
