﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Brigade
{
    public class UploadBrigadeTechSepecFileModel
    {
        public int ProjectId { get; set; }
        public IFormFile File { get; set; }
    }
}
