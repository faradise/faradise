﻿using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Models.Brigade;

namespace Faradise.Design.Controllers.API.Models.Brigade
{
    public class BrigadeTechSpecModel
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public FileModel[] Files { get; set; }
        public BrigadeWorkStatus Status { get; set; }
    }
}
