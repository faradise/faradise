﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Brigade
{
    public class DeleteBrigadeTechSepecFileModel
    {
        public int ProjectId { get; set; }
        public int FileId { get; set; }
    }
}
