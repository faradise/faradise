﻿using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class AvailableFiltersModel
    {
        public int Products { get; set; }

        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }

        public ColorName[] Colors { get; set; }
        public BrandName[] Brands { get; set; }
        public RoomName[] Rooms { get; set; }

        public bool ContainsARProducts { get; set; }

        public ProductMeasurements Measurements { get; set; }
    }
}
