﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ProductCountModel
    {
        /// <summary>
        /// Id товара
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// Количество товара
        /// </summary>
        [Required]
        public int Count { get; set; }
    }
}
