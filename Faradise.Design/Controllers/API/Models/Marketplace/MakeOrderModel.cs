﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class MakeOrderModel
    {
        /// <summary>
        /// Имя клиента
        /// </summary>
        [Required]
        public string ClientName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string ClientSurname { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        [Required]
        public string Phone { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        [Required]
        public string Adress { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// Стоимость доставки(прерасчитанная)
        /// </summary>
        public int DeliveryCost { get; set; }

        /// <summary>
        /// Промокод
        /// </summary>
        public string PromoCode { get; set; }
    }
}
