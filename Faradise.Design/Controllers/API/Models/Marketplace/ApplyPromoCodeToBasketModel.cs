﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ApplyPromoCodeToBasketModel
    {
        public string Code { get; set; }
    }
}
