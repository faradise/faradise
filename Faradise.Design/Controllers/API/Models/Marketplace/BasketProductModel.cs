﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class BasketProductModel
    {
        [Required]
        public ShortProductModel Product { get; set; }

        [Required]
        public int Count { get; set; }
    }
}
