﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class SimpleFiltrationModel
    {
        public string SearchWord { get; set; }
        public int From { get; set; }
        public int Count { get; set; }
        public int Category { get; set; }
    }
}
