﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class MapColorModel
    {
        public int CompanyColorId { get; set; }
        public int MarketplaceColorId { get; set; }
    }
}
