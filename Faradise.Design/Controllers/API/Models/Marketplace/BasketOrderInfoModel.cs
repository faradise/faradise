﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class BasketOrderInfoModel
    {
        public int OrderId { get; set; }
        public string ClientName { get; set; }
        public BasketOrderItemModel[] Items { get; set; }
        public int BasePrice { get; set; }
        public int DeliveryPrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int AbsoluteDiscount { get; set; }
    }
}
