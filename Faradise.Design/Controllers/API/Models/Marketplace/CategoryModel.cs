﻿namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public bool IsPopular { get; set; }
        public bool InHeader { get; set; }
        public string ImageUrl { get; set; }
        public string NameTransliterated { get; set; }
        public bool IsEnabled { get; set; }
        public int DeliveryCost { get; set; }
        public int DeliveryDays { get; set; }
        public int Priority { get; set; }
        public int DailyProductPriority { get; set; }
    }
}