﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class PriceFiltrationModel
    {
        public SimpleFiltrationModel Base { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public FilterOrdering Ordering { get; set; }
    }
}
