﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class RoomSelectionModel
    {
        public int RoomId { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Room Purpose { get; set; }
        public string Size { get; set; }
        public bool HasProduct { get; set; }
        public int ProductCount { get; set; }
    }
}
