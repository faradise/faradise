﻿namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ProductMeasurements
    {
        public float? MinWidth { get; set; }
        public float? MaxWidth { get; set; }
        public float? MinHeight { get; set; }
        public float? MaxHeight { get; set; }
        public float? MinLength { get; set; }
        public float? MaxLength { get; set; }

        public bool Empty()
        {
            return MinWidth == null &&
                   MaxWidth == null &&
                   MinHeight == null &&
                   MaxHeight == null &&
                   MinLength == null &&
                   MaxLength == null;
        }
    }
}