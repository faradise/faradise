﻿namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class MarketplaceRoomModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
