﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class MapCategoryModel
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public int CategoryId { get; set; }
    }
}
