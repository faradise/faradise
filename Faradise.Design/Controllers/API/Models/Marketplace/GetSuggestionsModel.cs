﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class GetSuggestionsModel
    {
        public int From { get; set; }
        public int Count { get; set; }
    }
}
