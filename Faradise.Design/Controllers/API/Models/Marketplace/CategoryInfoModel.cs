﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class CategoryInfoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameTransliterated { get; set; }
    }
}
