﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class AskForCallModel
    {
        public string Name { get; set; }

        [Required]
        public string Phone { get; set; }
    }
}
