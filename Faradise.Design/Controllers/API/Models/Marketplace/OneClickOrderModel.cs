﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class OneClickOrderModel
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int ProductCount { get; set; }

        [Required]
        [MinLength(1)]
        public string Name { get; set; }

        [Required]
        [MinLength(1)]
        public string Phone { get; set; }

        /// <summary>
        /// Email, необязателен
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// Стоимость доставки(прерасчитанная)
        /// </summary>
        public int DeliveryCost { get; set; }

        /// <summary>
        /// Промокод
        /// </summary>
        public string PromoCode { get; set; }
    }
}
