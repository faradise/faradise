﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class RemoveProductFromBasketModel
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Count { get; set; }
    }
}
