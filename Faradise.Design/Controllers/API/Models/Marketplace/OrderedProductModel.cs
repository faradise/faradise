﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class OrderedProductModel
    {
        public ShortProductModel ShortProductModel { get; set; }
        public bool PaidFor { get; set; }
        public int Count { get; set; }
        public string Color { get; set; }
        public bool? IsAvailableNow { get; set; }
    }
}
