﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class PromoCodeProductModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Count { get; set; }
        public int PriceWithPromocode { get; set; }
        public int PriceWithoutPromocode { get; set; }
        public bool SupportPromocode { get; set; }
    }
}
