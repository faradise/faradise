﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class RemoveProductFromProjectModel
    {
        public int ProductId { get; set; }
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public int Count { get; set; }
    }
}
