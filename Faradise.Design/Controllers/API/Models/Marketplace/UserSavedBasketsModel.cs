﻿using Faradise.Design.Models.Marketplace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class UserSavedBasketsModel
    {
        public int UserId { get; set; }
        public SavedBasket[] Baskets { get; set; }
    }

    public class SavedBasket
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public BasketView[] Views { get; set; }
    }
}
