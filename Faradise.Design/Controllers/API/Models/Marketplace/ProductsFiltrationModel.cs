﻿using Faradise.Design.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ProductsFiltrationModel
    {
        [Required] public int Offset { get; set; }
        [Required] public int Count { get; set; }

        public FilterOrdering Ordering { get; set; }

        public int Category { get; set; }

        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }

        public string SearchWords { get; set; }

        public int[] Brands { get; set; }
        public int[] Rooms { get; set; }
        public int[] Colors { get; set; }

        public bool AROnly { get; set; }
        public bool SalesOnly { get; set; }

        public float? MinWidth { get; set; }
        public float? MaxWidth { get; set; }
        public float? MinHeight { get; set; }
        public float? MaxHeight { get; set; }
        public float? MinLength { get; set; }
        public float? MaxLength { get; set; }
    }
}