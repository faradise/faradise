﻿using System.Collections.Generic;
using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string[] PhotoUrls { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int? PreviousPrice { get; set; }
        public int Price { get; set; }
        public int? Discount { get; set; }
        public bool Waranty { get; set; }
        public string ModelNumber { get; set; }
        public bool? HasInStorage { get; set; }
        public string Color { get; set; }
        public string Vendor { get; set; }
        public string Seller { get; set; }
        public List<CategoryInfo> Path { get; internal set; }
        public string Origin { get; internal set; }

        public string Size { get; internal set; }

        public string Width { get; set; }
        public string Length { get; set; }
        public string Height { get; set; }

        public string[] Notes { get; internal set; }

        public string Badge { get; set; }
        public string BadgeDescription { get; set; }
        public string SaleDescription { get; set; }

        public ARProductDefinition ARDefinition { get; set; }

        public int CategoryDeliveryCost { get; set; }
        public int CategoryDeliveryDays { get; set; }
    }
}
