﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class GetProductsModel
    {
        public int AllCount { get; set; }
        public ShortProductModel[] Products { get; set; }
    }
}
