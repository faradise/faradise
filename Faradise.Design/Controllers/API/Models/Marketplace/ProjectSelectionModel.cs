﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ProjectSelectionModel
    {
        public int ProjectId;
        public string ProjectName;
        public RoomSelectionModel[] Rooms;
    }
}
