﻿using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ShortProductModel
    {
        public int Id { get; set; }
        public string PhotoUrl { get; set; }
        public string AdditionalPhotoUrl { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int? PreviousPrice { get; set; }
        public int Price { get; set; }
        public int? Discount { get; set; }
        public string Badge { get; set; }
        public string BadgeDescription { get; set; }
        public string SaleDescription { get; set; }
        public string Seller { get; set; }
        public string Vendor { get; set; }

        public bool ARSupport { get; set; }
    }
}
