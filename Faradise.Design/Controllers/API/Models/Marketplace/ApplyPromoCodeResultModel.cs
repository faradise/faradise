﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class ApplyPromoCodeResultModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public PromocodeError Error { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime StartDate { get; set; }
        public int PercentDiscount { get; set; }
        public int AbsoluteDiscount { get; set; }
        public PromoCodeProductModel[] Products { get; set; }
    }
}
