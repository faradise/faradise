﻿using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class IssueOrderModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        [MinLength(1)]
        public ProductOrderForProjectModel[] Products { get; set; }

        public string City { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Предрасчитанная стоимость доставки
        /// </summary>
        public int DeliveryCost { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentMethod PaymentMethod { get; set; }
    }
}
