﻿namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class CategoryHierarchyModel
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }

        public string Name { get; set; }
        public string NameTransliterated { get; set; }

        public CategoryHierarchyModel[] Childs { get; set; }
    }
}
