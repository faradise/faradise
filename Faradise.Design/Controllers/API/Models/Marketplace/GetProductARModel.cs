﻿using System.ComponentModel.DataAnnotations;
using Faradise.Design.Models.Marketplace;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Faradise.Design.Controllers.API.Models.Marketplace
{
    public class GetProductARModel
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int ProductId { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public ARPlatform Platform { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public ARFormat Format { get; set; }
    }
}
