﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.User
{
    public class EmailModel
    {
        /// <summary>
        /// Email адрес
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
