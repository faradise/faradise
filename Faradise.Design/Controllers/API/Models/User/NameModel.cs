﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.User
{
    public class NameModel
    {
        [Required]
        public string Name { get; set; }
    }
}
