﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.User
{
    public class PhoneModel
    {
        [Required]
        public string Phone { get; set; }
    }
}
