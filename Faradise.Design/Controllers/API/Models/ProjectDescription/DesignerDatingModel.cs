﻿using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class DesignerDatingModel
    {
        /// <summary>
        /// Id дизайнера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя дизайнера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ссылка на портфолио
        /// </summary>
        public string PortfolioLink { get; set; }

        /// <summary>
        /// Ссылка на аватарку
        /// </summary>
        public string AvatarUrl { get; set; }

        /// <summary>
        /// Возраст дезайнера
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Город дизайнера
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Доступен надзор
        /// </summary>
        public bool PersonalСontrol { get; set; }
    }
}
