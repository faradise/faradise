﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ProjectMembersModel
    {
        public string DesignerName { get; set; }
        public string DesignerAvatarUrl { get; set; }

        public string ClientName { get; set; }
    }
}
