﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class TestResultsModel
    {
        [Required]
        public TestResultsAnswerModel[] Questions { get; set; }
    }
}
