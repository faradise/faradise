﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class DesignerRequirementsModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Gender Gender { get; set; }

        /// <summary>
        /// Не моложе чем 
        /// </summary>
        [Required]
        public int AgeFrom { get; set; }

        /// <summary>
        /// Не старше чем 
        /// </summary>
        [Required]
        public int AgeTo { get; set; }

        /// <summary>
        /// Нужена ли возможность встречи
        /// </summary>
        [Required]
        public bool NeedPersonalMeeting { get; set; }

        /// <summary>
        /// Адрес для встреч, если они нужны
        /// </summary>
        public string MeetingAdress { get; set; }

        /// <summary>
        /// Нужен ли авторский надзор во время ремонта
        /// </summary>
        [Required]
        public bool NeedPersonalControl { get; set; }

        /// <summary>
        /// Резултаты теста подбора дизайнера. Содержание теста получается через отдельный запрос, см API
        /// </summary>
        public TestResultsModel Testing { get; set; }
    }
}
