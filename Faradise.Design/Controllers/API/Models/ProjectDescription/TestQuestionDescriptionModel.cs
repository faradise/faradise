﻿namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class TestQuestionDescriptionModel
    {
        /// <summary>
        /// Уникальный id вопроса
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Текст вопроса
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Массив возможных ответов
        /// </summary>
        public TestAnswerDescriptionModel[] Answers { get; set; }
    }
}
