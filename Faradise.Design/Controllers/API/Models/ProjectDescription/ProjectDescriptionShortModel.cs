﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ProjectShortModel
    {
        /// <summary>
        /// Id проекта, по которому потом можно будет получить/изменить его
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Стадия проекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectStage Stage { get; set; }

        /// <summary>
        /// Статус проекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectStatus Status { get; set; }
    }
}
