﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class RoomsModel
    {
        /// <summary>
        /// Назначение комнаты
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Room Purpose { get; set; }
        /// <summary>
        /// Кол-во комнат с таким назначением
        /// </summary>
        [Required]
        [Range(1,int.MaxValue)]
        public int Count { get; set; }
    }
}
