﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class StyleModel
    {
        /// <summary>
        /// Тип стиля
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectStyle Name { get; set; }

        /// <summary>
        /// Дополнительное описание
        /// </summary>
        public string Description { get; set; }
    }
}
