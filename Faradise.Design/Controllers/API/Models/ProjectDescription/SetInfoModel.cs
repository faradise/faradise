﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class SetInfoModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Тип объекта
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectTargetType ObjectType { get; set; }

        /// <summary>
        /// Список комнат по назначениям
        /// </summary>
        [Required]
        [MinLength(1)]
        public RoomsModel[] Rooms { get; set; }

        /// <summary>
        /// Причины для заказа
        /// </summary>
        [Required]
        public ReasonModel[] Reasons { get; set; }
    }
}
