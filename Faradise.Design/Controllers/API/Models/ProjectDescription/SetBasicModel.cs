﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class SetBasicModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя. Опционально
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Номер телефона пользователя. Опционально
        /// </summary>
        public string UserPhone { get; set; }

        /// <summary>
        /// Имя проекта
        /// </summary>
        [Required]
        public string ProjectName { get; set; }

        /// <summary>
        /// Город проекта
        /// </summary>
        public string City { get; set; }
    }
}
