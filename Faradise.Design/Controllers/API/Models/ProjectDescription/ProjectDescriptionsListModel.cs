﻿namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ProjectListModel
    {
        /// <summary>
        /// Списое проектов
        /// </summary>
        public ProjectShortModel[] Projects { get; set; }
    }
}
