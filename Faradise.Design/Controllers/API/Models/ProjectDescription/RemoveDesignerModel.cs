﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class RemoveDesignerModel
    {
        public int DesignerId { get; set; }
        public string Description { get; set; }
    }
}
