﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class TestResultsAnswerModel
    {
        /// <summary>
        /// Уникальный id вопроса
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        public int Index { get; set; }

        /// <summary>
        /// Уникальный id ответа
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        public int Answer { get; set; }
    }
}
 