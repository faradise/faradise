﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class BudgetModel
    {
        /// <summary>
        /// Бюджет на мебель
        /// </summary>
        [Required] public int FurnitureBudget { get; set; }

        /// <summary>
        /// Бюджет на ремонт
        /// </summary>
        [Required] public int RenovationBudget { get; set; }
    }
}
