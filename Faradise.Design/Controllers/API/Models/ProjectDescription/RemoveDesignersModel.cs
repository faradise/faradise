﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class RemoveDesignersModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Список с Id дизайнеров на удаление
        /// </summary>
        public RemoveDesignerModel[] DesignersToRemove { get; set; }
    }
}
