﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ReasonModel
    {
        /// <summary>
        /// Причина ремонта 
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectReason Name { get; set; }

        /// <summary>
        /// Дополнительное описание 
        /// </summary>
        public string Description { get; set; }
    }
}
