﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ProjectDescriptionModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Тип объекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectTargetType ObjectType { get; set; }

        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Город проекта
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Список комнат по назначениям
        /// </summary>
        public RoomsModel[] Rooms { get; set; }

        /// <summary>
        /// Причины для заказа
        /// </summary>
        public ReasonModel[] Reasons { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        public StyleModel[] Styles { get; set; }

        /// <summary>
        /// Бюджет проекта
        /// </summary>
        public BudgetModel Budget { get; set; }

        /// <summary>
        /// Требования к дизайнеру
        /// </summary>
        public DesignerRequirementsModel DesignerRequirements { get; set; }

        /// <summary>
        /// Текущий этап проекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectStage Stage { get; set; }

        /// <summary>
        /// Статус проекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// Пользователь закончил заполнение проекта
        /// </summary>
        public bool FinishFill { get; set; }

        /// <summary>
        /// Теоретическое время, которое осталось до выдачи подборки дизайнеров платформой (в секундах)
        /// </summary>
        public int SecondsLeftToFillDesignerPool { get; set; }
    }
}
