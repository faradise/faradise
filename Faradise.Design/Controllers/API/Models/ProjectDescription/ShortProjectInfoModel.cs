﻿using Faradise.Design.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class ShortProjectInfoModel
    {
        public int ProjectId { get; set; }
        public string City { get; set; }
        public string ClientName { get; set; }

        /// <summary>
        /// Тип объекта
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProjectTargetType ObjectType { get; set; }


        /// <summary>
        /// Список комнат по назначениям
        /// </summary>
        public RoomsModel[] Rooms { get; set; }

        /// <summary>
        /// Причины для заказа
        /// </summary>
        public ReasonModel[] Reasons { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        public StyleModel[] Styles { get; set; }

        /// <summary>
        /// Бюджет проекта
        /// </summary>
        public BudgetModel Budget { get; set; }

    }
}
