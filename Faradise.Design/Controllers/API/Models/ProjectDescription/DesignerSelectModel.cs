﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class DesignerSelectModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Id дизайнера
        /// </summary>
        public int DesignerId { get; set; }
    }
}
