﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class SetStylesModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        [Required]
        public StyleModel[] Styles { get; set; }
    }
}
