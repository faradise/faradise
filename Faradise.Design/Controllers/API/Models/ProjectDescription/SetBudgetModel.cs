﻿using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class SetBudgetModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Бюджет
        /// </summary>
        [Required]
        public BudgetModel Budget { get; set; }
    }
}
