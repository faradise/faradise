﻿namespace Faradise.Design.Controllers.API.Models.ProjectDescription
{
    public class TestAnswerDescriptionModel
    {
        /// <summary>
        ///  Уникальный id ответа
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Текст ответа
        /// </summary>
        public string Text { get; set; }
    }
}
