﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class UploadRoomFileModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Id комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Файл
        /// </summary>
        [Required]
        public IFormFile File { get; set; }
    }
}
