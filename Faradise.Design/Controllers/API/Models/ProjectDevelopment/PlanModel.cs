﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class PlanModel
    {
        /// <summary>
        /// Id комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Массиво файлов с планами
        /// </summary>
        public FileModel[] PlanFiles { get; set; }

        /// <summary>
        /// Цена планирования одной комнаты
        /// </summary>
        public int Price { get; set; }

    }
}
