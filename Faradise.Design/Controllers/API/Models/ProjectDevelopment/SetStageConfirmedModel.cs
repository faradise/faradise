﻿using Faradise.Design.Models.ProjectDevelopment;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class ConfirmedDevelopStageModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Стадия 3го шага
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public DevelopmentStage Stage { get; set; }
    }
}
