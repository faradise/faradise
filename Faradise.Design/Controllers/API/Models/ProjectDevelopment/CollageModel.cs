﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class CollageModel
    {
        /// <summary>
        /// id комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Массив с изображениями коллажей
        /// </summary>
        public FileModel[] CollagePhotos { get; set; }
    }
}
