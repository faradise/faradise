﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class OrderVisualizationModel
    {
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Список комнат на визуализацию
        /// </summary>
        public int[] RoomsId { get; set; }
    }
}
