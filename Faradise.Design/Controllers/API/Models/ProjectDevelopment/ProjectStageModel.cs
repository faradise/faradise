﻿using Faradise.Design.Models.ProjectDevelopment;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class ProjectStageModel
    {
        /// <summary>
        /// Теукщая стадия 3го шага
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public DevelopmentStage Stage { get; set; }

        /// <summary>
        /// Дизайнер отметил стадию как завершенную
        /// </summary>
        public bool DesignerIsReady { get; set; }

        /// <summary>
        /// Клиент отметил стадию как завершенную
        /// </summary>
        public bool ClientIsReady { get; set; }

        /// <summary>
        /// Секунд осталось до конца стадии
        /// </summary>
        public int TimeLeftSeconds { get; set; }

        /// <summary>
        /// Есть ли незаверешенная заказанная работа по визуализации или чертежам
        /// </summary>
        public bool HasUnfinishedWork { get; set; }

        /// <summary>
        /// Пользователь приянл решение, если оно вообще было в этой стадии
        /// </summary>
        public bool UserMadeDecision { get; set; }

        /// <summary>
        /// Все заказанные объекты оплачены
        /// </summary>
        public bool IsPaid { get; set; }

        /// <summary>
        /// Нужно ли показывать платежку
        /// </summary>
        public bool NeedPayment { get; set; }

        /// <summary>
        /// Какой-то платеж был, но прошел неудачно
        /// </summary>
        public bool PaymentFailed { get; set; }
    }
}
