﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class FinishPlanModel
    {
        [Required]
        public int ProjectId { get; set; }

        [Required]
        public int RoomId { get; set; }
    }
}
