﻿
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class MoodboardModel
    {
        /// <summary>
        /// Id комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Массив с изображениями мудбордов
        /// </summary>
        public FileModel[] MoodboardPhotos;
    }
}
