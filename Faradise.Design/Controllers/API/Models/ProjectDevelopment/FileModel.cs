﻿
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class FileModel
    {
        /// <summary>
        /// Id файла/фото
        /// </summary>
        [Required]
        public int FileId { get; set; }

        /// <summary>
        /// Ссылка url
        /// </summary>
        [Required]
        public string FileLink { get; set; }
    }
}
