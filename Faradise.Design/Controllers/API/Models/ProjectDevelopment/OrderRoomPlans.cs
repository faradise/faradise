﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class OrderRoomPlans
    {
        public int ProjectId { get; set; }

        /// <summary>
        /// Id заказанных комнат
        /// </summary>
        public int[] RoomsId { get; set; }
    }
}
