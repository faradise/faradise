﻿using Faradise.Design.Models.ProjectDevelopment;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class GetDevelopStageModel
    {
        /// <summary>
        /// Id проекта
        /// </summary>
        [Required]
        public int ProjectId { get; set; }

        /// <summary>
        /// Текущая стадия 3го шага
        /// </summary>
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public DevelopmentStage Stage { get; set; }
    }
}
