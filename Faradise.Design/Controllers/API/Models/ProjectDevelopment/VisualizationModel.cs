﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.ProjectDevelopment
{
    public class VisualizationModel
    {
        /// <summary>
        /// Id комнаты
        /// </summary>
        [Required]
        public int RoomId { get; set; }

        /// <summary>
        /// Код для вставки визуализации в страницу
        /// </summary>
        public string VisualizationCode { get; set; }

        /// <summary>
        /// Цена визацализации за одну комнату
        /// </summary>
        [Required]
        public int Price { get; set; }
    }
}
