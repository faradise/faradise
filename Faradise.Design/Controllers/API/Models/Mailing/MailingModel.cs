﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API.Models.Mailing
{
    public class MailingModel
    {
        public int[] Ids { get; set; }
        public string Message { get; set; }
    }
}
