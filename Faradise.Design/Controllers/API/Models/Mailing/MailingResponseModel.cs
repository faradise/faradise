﻿namespace Faradise.Design.Controllers.API.Models.Mailing
{
    public class MailingResponseModel
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public string Status { get; set; }
    }
}
