﻿using System;

namespace Faradise.Design.Controllers.API.Models.Banner
{
    public class BannerShortModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int Order { get; set; }
        public DateTime Updated { get; set; }
    }
}