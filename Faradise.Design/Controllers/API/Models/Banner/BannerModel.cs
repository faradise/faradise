﻿using System;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Controllers.API.Models.Banner
{
    public class BannerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PictureWeb { get; set; }
        public string PictureMobile { get; set; }
        public string PictureMobileBackground { get; set; }


        public string CompilationName { get; set; }
        public int? ProductId { get; set; }
        public int? CategoryId { get; set; }
        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }
        public int? BrandId { get; set; }
        public FilterOrdering? Ordering { get; set; }
        public bool? DiscountFilter { get; set; }

        public string Url { get; set; }

        public bool Active { get; set; }
        public int Order { get; set; }
        public DateTime Upated { get; set; }


    }
}