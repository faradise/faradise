﻿namespace Faradise.Design.Controllers.API.Models.Banner
{
    public class BannerList
    {
        public Banner[] Banners { get; set; }
    }
}