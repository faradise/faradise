﻿using System.Collections.Generic;

namespace Faradise.Design.Controllers.API.Models.Banner
{
    public class BannerListModel
    {
        public BannerShortModel[] Banners { get; set; }
    }
}