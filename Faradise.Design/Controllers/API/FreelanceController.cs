﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.VisulizationFreelance;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/freelance")]
    public class FreelanceController : APIController
    {
        private readonly IVisualizationFreelanceService _visualsFreelanceService = null;
        private readonly ICDNService _cdnService = null;
        private readonly IProjectDefinitionService _defService = null;

        public FreelanceController(IVisualizationFreelanceService visualsFreelanceService, ICDNService cdnService, IProjectDefinitionService defService)
        {
            _visualsFreelanceService = visualsFreelanceService;
            _cdnService = cdnService;
            _defService = defService;
        }

        /// <summary>
        /// Установить описание визуализации одной комнаты для фрилансера
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("visulization/set-techspec")]
        [APIAuthorize(Roles = Role.Designer)]
        public void SetFreelanceTechSpec([FromBody] SetVisulizationTechSpecModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _visualsFreelanceService.SetDescription(model.ProjectId, model.RoomId, model.Description);
        }

        /// <summary>
        /// Получить описание визуализации одной комнаты для фрилансера 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet("visulization/get-techspec")]
        [APIAuthorize(Roles = Role.Designer)]
        public VisualisationTechSpecModel GetFreelanceTechSpec(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var data = _visualsFreelanceService.GetVisualizationTechSpec(projectId, roomId, user.Id);
            return data != null ? data.ToModel() : null;
        }

        /// <summary>
        /// Добавить файл для задания по визуализации
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("visulization/add-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> AddVisualizationFile([FromForm] UploadVisualTechSepecFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForFreelanceVisualizationFile(model.ProjectId));
            return _visualsFreelanceService.AddFile(model.ProjectId, model.RoomId, url, model.File.FileName);
        }

        /// <summary>
        /// Удалить файл для задания по визуализации
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("visulization/remove-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public void RemoveVisualizationFile([FromBody] DeleteVisualTechSepecFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _visualsFreelanceService.RemoveFile(model.ProjectId, model.RoomId, model.FileId);
        }

        /// <summary>
        /// Опубликовать ТЗ
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("visulization/publish-tech-spec")]
        [APIAuthorize(Roles = Role.Designer)]
        public void PublishFreelanceTechSpec([FromBody] PublishVisualTeshSpecModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _visualsFreelanceService.Publish(model.ProjectId, model.RoomId, user.Id);
        }
    }
}
