﻿using Faradise.Design.Controllers.API.Models.Authentication;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Microsoft.AspNetCore.Mvc;
using Faradise.Design.Services;
using Faradise.Design.Models;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Utilities;
using Faradise.Design.Controllers.API.Models.User;

namespace Faradise.Design.Controllers.API
{
    [Route("/api/authentication")]
    public class AuthenticationController : APIController
    {
        private IUserService _userService = null;
        private ITokenService _tokenService = null;
        private IValidationService _validationService = null;
        private PhoneUtility _phoneUtility = null;

        public AuthenticationController(IUserService userService, ITokenService tokenService, IValidationService requestCodeService, PhoneUtility phoneUtility)
        {
            _userService = userService;
            _tokenService = tokenService;
            _validationService = requestCodeService;
            _phoneUtility = phoneUtility;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        [HttpPost("createClient")]
        public string CreateClient()
        {
            var user = _userService.CreateUser(Role.Client | Role.Authorized);
            var token = _tokenService.RequestToken(user.Id);

            return token;
        }

        /// <summary>
        /// Создать дизайнера
        /// </summary>
        [HttpPost("createDesigner")]
        public string CreateDesigner()
        {
            var user = _userService.CreateUser(Role.Designer | Role.Authorized);
            var token = _tokenService.RequestToken(user.Id);

            return token;
        }

        #region CreateAdmin
        //[HttpPost("createAdmin")]
        //public string CreateAdmin()
        //{
        //    var token = _tokenService.RequestToken(-1);

        //    return token;
        //}
        #endregion

        /// <summary>
        /// Проверяет токен на валидность
        /// </summary>
        /// <param name="model"></param>
        /// <returns>true, если токен валиден, иначе false</returns>
        [HttpPost("validateToken")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public bool Validate([FromBody] ValidateModel model)
        {
            var claims = _tokenService.GetClaims(model.Token);
            if (claims == null)
                return false;
            var stringId = claims.FindFirst("id");
            var id = int.Parse(stringId.Value);

            var user = _userService.ExtractUserById(id);

            return user != null;
        }
        
        /// <summary>
        /// Вход по указанному номеру телефона и коду подтверждения
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Токен пользователя</returns>
        [HttpPost("login")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public LoggedInModel Login([FromBody] LoginModel model)
        {
            var phone = _phoneUtility.FixPhone(model.Phone);
            var valid = _validationService.VerifyValidationCode(phone, model.Code);
            if (!valid)
                throw new ServiceException("Verification code in invalid", "invalid_code");

            var user = _userService.ExtractUserByAlias(phone, AliasType.Phone);
            if (user == null)
                throw new ServiceException("User with alias {phone} was not found", "no_such_user");
            
            var role = string.Empty;
            if (user.Role.ContainsRole(Role.Designer))
                role = Role.Designer.ToString();
            else if (user.Role.ContainsRole(Role.Client))
                role = Role.Client.ToString();
            else
                role = "Unassigned";

            var loggedIn = new LoggedInModel
            {
                Token = _tokenService.RequestToken(user.Id),
                User = new AuthorizeModel
                {
                    Id = user.Id,
                    Name = user.Name,
                    Role = role,
                    Email = user.Email,
                    Phone = user.Phone
                }
            };

            return loggedIn;
        }

        /// <summary>
        /// Привязывает номер телефона по коду к текущему пользователю
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("bind")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void BindDesigner([FromBody] BindModel model)
        {
            var phone = _phoneUtility.FixPhone(model.Phone);
            var valid = _validationService.VerifyValidationCode(phone, model.Code);
            if (!valid)
                throw new ServiceException("Verification code in invalid", "invalid_code");

            var user = (User)HttpContext.Items[typeof(User)];

            _userService.AddAlias(user.Id, AliasType.Phone, phone);
        }

        /// <summary>
        /// Запрашивает код для указанного номера телефона
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("requestCode")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public void RequestCode([FromBody] RequestCodeModel model)
        {
            var phone = _phoneUtility.FixPhone(model.Phone);
            _validationService.SendValidationCode(phone);
        }

        [HttpGet("check-phone")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public bool CheckPhone(string phone)
        {
            var fixedPhone = _phoneUtility.FixPhone(phone);
            var exists = _userService.CheckPhoneInUse(fixedPhone);

            return exists;
        }
    }
}