﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Marketplace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Marketplace.Models.Validation;
using Faradise.Design.Marketplace.Xml;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.YMLUpdater;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Faradise.Design.Services.Implementation;
using Faradise.Design.Models.DAL;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/marketplace")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APIMarketplaceController : APIController
    {
        private const string UPLOAD_FOLDER = "upload";

        private IMarketplaceService _marketplaceService;
        private ICDNService _cdnService = null;
        private IDBMarketplaceService _dbMarketplace = null;
        private IAdminService _adminService;

        private FaradiseDBContext _context = null;

        public APIMarketplaceController(
            IMarketplaceService marketplaceService,
            ICDNService cdnService,
            IDBMarketplaceService dbMarketplace,
            IAdminService adminService, FaradiseDBContext context)
        {
            _marketplaceService = marketplaceService;
            _cdnService = cdnService;

            _dbMarketplace = dbMarketplace;

            _adminService = adminService;
            _context = context;
        }

        [HttpGet("get-categories")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CategoriesModel GetCategories()
        {
            var model = new CategoriesModel
            {
                Categories = _marketplaceService.GetCategories()
                    .Select(x => x.ToModel())
                    .ToList()
            };

            return model;
        }

        [HttpPost("create-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int AddCategory([FromBody] CreateCategoryModel model)
        {
            return _marketplaceService.AddCategory(new Category { Name = model.Name });
        }

        [HttpPost("enable-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool EnableCategory([FromBody] CategoryIdModel model)
        {
            _marketplaceService.EnableCategory(model.Id);
            return true;
        }

        [HttpPost("disable-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DisableCategory([FromBody] CategoryIdModel model)
        {
            _marketplaceService.DisableCategory(model.Id);
            return true;
        }

        [HttpPost("delete-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteCategory([FromBody] DeleteCategoryModel model)
        {
            _marketplaceService.DeleteCategory(model.Id);

            return true;
        }

        [HttpPost("modify-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ModifyCategory([FromBody] ModifyCategoryModel model)
        {
            _marketplaceService.EditCategory(model);

            return true;
        }

        [HttpGet("get-companies")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompaniesModel GetCompaniesModel([FromQuery] bool loadAdditinalInfo)
        {
            var model = _marketplaceService.GetAllCompanies(loadAdditinalInfo);

            return model;
        }

        [HttpGet("get-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompanyModel GetCompanyModel([FromQuery] int id)
        {
            var model = _marketplaceService.GetCompanyById(id);
            if (model != null)
            {
                var feedInfo = _dbMarketplace.GetCompanyLastFeedInfo(id);
                model.CompanyLastFeedModel = feedInfo != null ? new CompanyLastFeedModel { LastFeedStatus = feedInfo.LastFeedStatus, LastFeedStatusChangeTime = feedInfo.LastFeedStatusChangeTime, Source = feedInfo.Source } : null;
            }
            return model;
        }

        //[HttpGet("upload-azure-products")]
        //[APIAuthorize(Roles = Role.Authorized)]
        //public void UploadProducts([FromQuery] int id)
        //{
        //    _azure.CreateIndex<Design.Models.AzureSearch.Product>("products");
        //    _azure.UploadProducts("products");
        //    _searchService.GetQueryFilters(26, "products");
        //    _searchService.GetProductsByFilter(new ProductsQueryFiltersValues { Offset = 0, Count =1000, Category = 27}, "products");
        //}

        [HttpGet("parsing")]
        [APIAuthorize(Roles = Role.Administrator)]
        public void ParseCSV()
        {
            var service = new CSVParseService(_context);
            service.Parse();
        }

        [HttpPost("create-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CreatedCompanyModel CreateCompany()
        {
            var model = new CreatedCompanyModel { Id = _marketplaceService.CreateCompany() };
            return model;
        }

        [HttpPost("enable-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool EnableCompany([FromBody] CompanyIdModel model)
        {
            _marketplaceService.EnableCompany(model.Id);
            return true;
        }

        [HttpPost("disable-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DisableCompany([FromBody] CompanyIdModel model)
        {
            _marketplaceService.DisableCompany(model.Id);
            return true;
        }

        [HttpPost("modify-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ModifyCompany([FromBody] ModifyCompanyModel model)
        {
            return _marketplaceService.ModifyCompany(model);
        }

        [HttpPost("enable-autofeed")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool EnableCompanyAutofeed([FromBody] CompanyIdModel model)
        {
            _marketplaceService.SetCompanyAutofeed(model.Id, true);
            return true;
        }

        [HttpPost("disable-autofeed")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DisableCompanyAutofeed([FromBody] CompanyIdModel model)
        {
            _marketplaceService.SetCompanyAutofeed(model.Id, false);
            return true;
        }

        [HttpPost("add-company-note")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddCompanyNote([FromBody] AddCompanyNoteModel model)
        {
            _marketplaceService.AddCompanyNote(model.CompanyId, model.Note);
            return true;
        }

        [HttpPost("change-company-note")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangeCompanyNote([FromBody] ChangeCompanyNoteModel model)
        {
            _marketplaceService.ChangeCompanyNote(model.CompanyId, model.NoteId, model.NewNote);
            return true;
        }

        [HttpPost("delte-company-note")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteCompanyNote([FromBody] DeleteCompanyNoteModel model)
        {
            _marketplaceService.DeleteCompanyNote(model.CompanyId, model.NoteId);
            return true;
        }

        [HttpPost("upload-yml")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public UploadResultModel UploadYml([FromForm] IFormFile file, [FromForm] int id)
        {
            var text = string.Empty;
            var report = new ValidationReport();
            MarketplaceCompany company = null;
            string filename = string.Empty;
            var fileExtension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            using (var stream = file.OpenReadStream())
            {
                var filetype = YMLTypeExtensions.MapFileExtension(fileExtension);
                if (filetype == Design.Models.Enums.UploadFiletype.YML)
                    company = MarketplaceParser.FromXml(stream);
                else if (filetype == Design.Models.Enums.UploadFiletype.EXCEL)
                    (company, report) = XlsxMarketplaceParser.Parse(stream);
            }

            if (!report.Valid)
                return new UploadResultModel { ValidationReport = report, CompanyId = id };

            using (var stream = file.OpenReadStream())
            {
                filename = Guid.NewGuid().ToString() + fileExtension;
                if (!Directory.Exists(UPLOAD_FOLDER))
                    Directory.CreateDirectory(UPLOAD_FOLDER);
                using (var filestream = System.IO.File.Create(string.Format("{0}/{1}", UPLOAD_FOLDER, filename)))
                {
                    stream.CopyTo(filestream);
                    filestream.Dispose();
                }
            }

            var model = new UploadResultModel
            {
                CompanyId = id,

                Upload = filename,

                Company = company.Name,
                Categories = company.Categories.Count,
                Products = company.Products.Count,
                ValidationReport = report
            };

            return model;
        }

        [HttpPost("upload-xlsx-content")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public void UploadXlsx([FromForm] IFormFile file, [FromForm] int id)
        {
            var text = string.Empty;

            MarketplaceCompany company;
            string filename = string.Empty;
            string fileExtension = string.Empty;
            using (var stream = file.OpenReadStream())
            {
                filename = file.FileName;
                if (!Directory.Exists(UPLOAD_FOLDER))
                    Directory.CreateDirectory(UPLOAD_FOLDER);
                using (var filestream = System.IO.File.Create(string.Format("{0}/{1}", UPLOAD_FOLDER, filename)))
                {
                    stream.CopyTo(filestream);
                    company = XlsxMarketplaceParser.Parse(filestream).company;
                    company.Id = id;
                    filestream.Dispose();
                }
            }
        }

        [HttpPost("show-yml-content")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public UploadResultModel ShowYmlContent([FromForm] IFormFile file)
        {
            var text = string.Empty;

            MarketplaceCompany company = null;
            var fileExtension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            using (var stream = file.OpenReadStream())
            {
                var filetype = YMLTypeExtensions.MapFileExtension(fileExtension);
                if (filetype == Design.Models.Enums.UploadFiletype.YML)
                    company = MarketplaceParser.FromXml(stream);
                else if (filetype == Design.Models.Enums.UploadFiletype.EXCEL)
                    company= XlsxMarketplaceParser.Parse(stream).company;
            }

            var model = new UploadResultModel
            {
                Company = company.Name,
                Categories = company.Categories.Count,
                Products = company.Products.Count,
                Upload = "Temporary",
                CompanyId = 0
            };

            return model;
        }

        [HttpGet("get-yml-upload-queue")]
        [APIAuthorize(Roles = Role.Authorized)]
        public YMLUploadTaskModel[] GetYmlUploadQueue()
        {
            var tasks = _marketplaceService.GetAllPendingYMLUpdateTasks();
            return tasks != null
                ? tasks.Select(x => new YMLUploadTaskModel
                {
                    TaskId = x.Id,
                    CompanyId = x.CompanyId,
                    CreationTime = x.CreateTime,
                    Priority = x.Priority,
                    YmlLink = x.YMLLink,
                    Status = x.Status,
                    FailMessage = x.FailMessage
                })
                    .OrderBy(x => x.Status)
                    .ToArray()
                : new YMLUploadTaskModel[0];
        }

        [HttpPost("delete-yml-upload-task")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteYmlUploadTask([FromBody] int taskId)
        {
            _marketplaceService.DeleteYMLUpdateTask(taskId);
            return true;
        }

        [HttpPost("accept-upload")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<bool> AcceptYmlUpload([FromBody] PostProcessUploadModel model)
        {
            if (!Directory.Exists(UPLOAD_FOLDER))
                Directory.CreateDirectory(UPLOAD_FOLDER);

            var bytes = System.IO.File.ReadAllBytes(string.Format("{0}/{1}", UPLOAD_FOLDER, model.Upload));
            var fileExtension = model.Upload.Substring(model.Upload.LastIndexOf('.'));
            var ymlLink = await _cdnService.UploadData(bytes, _cdnService.GetPathForYMLUpload(), "yml_" + DateTime.Now.ToLongTimeString() + "_" + DateTime.Now.ToLongDateString(), fileExtension);
            _marketplaceService.AddYMLUpdateTask(model.Id, ymlLink, YMLTypeExtensions.MapFileExtension(fileExtension), 1, YMLDownloadSource.CDN);

            System.IO.File.Delete(string.Format("{0}/{1}", UPLOAD_FOLDER, model.Upload));

            return true;
        }

        [HttpPost("accept-upload-sync")]
        [DisableRequestSizeLimit]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AcceptYmlUploadSync([FromBody] PostProcessUploadModel model)
        {
            if (!Directory.Exists(UPLOAD_FOLDER))
                Directory.CreateDirectory(UPLOAD_FOLDER);

            MarketplaceCompany company = null;
            var fileExtension = model.Upload.Substring(model.Upload.LastIndexOf('.'));
            using (var stream = System.IO.File.OpenRead(string.Format("{0}/{1}", UPLOAD_FOLDER, model.Upload)))
            {
                var filetype = YMLTypeExtensions.MapFileExtension(fileExtension);
                if (filetype == Design.Models.Enums.UploadFiletype.YML)
                    company = MarketplaceParser.FromXml(stream);
                else if (filetype == Design.Models.Enums.UploadFiletype.EXCEL)
                    company = XlsxMarketplaceParser.Parse(stream).company;
            }
            company.Id = model.Id;
            _marketplaceService.UpdateCompany(company);

            System.IO.File.Delete(string.Format("{0}/{1}", UPLOAD_FOLDER, model.Upload));

            return true;
        }

        [HttpPost("process-yml-upload-forced")]
        public bool ProcessYMLUploadForced([FromBody] int taskId)
        {
            _marketplaceService.MarkYMLUpdateTaskAsPending(taskId);
            return true;
        }

        [HttpPost("reject-upload")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool RejectYmlUpload([FromBody] PostProcessUploadModel model)
        {
            if (!Directory.Exists(UPLOAD_FOLDER))
                Directory.CreateDirectory(UPLOAD_FOLDER);

            System.IO.File.Delete(string.Format("{0}/{1}", UPLOAD_FOLDER, model.Upload));

            return true;
        }

        // Возвращает список категории компании
        // и их привязку к внутренним категориям
        // Если категория компании отвязана от внутренней категории
        // То в привязке возвращается 0
        [HttpGet("get-company-categories")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompanyCategoriesModel CompanyCategories([FromQuery] int companyId)
        {
            return _marketplaceService.GetCompanyCategories(companyId);
        }

        [HttpGet("get-company-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompanyCategoryModel GetCompanyCategory([FromQuery] CompanyCategoryKeyModel key)
        {
            return _marketplaceService.GetCompanyCategory(key.CompanyId, key.CompanyCategoryId);
        }

        // Должно привязать категории компании к внутренним категориям.
        // Если в качестве внутренней категории пришел 0, значит категория компании
        // отвязана от маркетплейса
        [HttpPost("map-company-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int[] MapCompanyCategory([FromBody] MapCategoryModel model)
        {
            return _marketplaceService.MapCompanyCategory(model);
        }

        [HttpPost("map-company-categories")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool MapCompanyCategories([FromBody] CompanyCategoriesModel model)
        {
            return _marketplaceService.MapCompanyCategories(model);
            //return true;
        }

        [HttpPost("upload-category-images")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<bool> UploadCategoryImages([FromForm] IFormFile[] files)
        {
            foreach (var file in files)
            {
                var url = await _cdnService.UploadData(file, _cdnService.GetPathForCategotyPhotos());
                _marketplaceService.SaveCategoryImage(url);
            }

            return true;
        }

        [HttpGet("category-images")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CategoryImageModel[] GetCategotyImages()
        {
            return _marketplaceService.GetCategoryImages().Select(x => new CategoryImageModel() { ImageId = x.Key, Url = x.Value }).ToArray();
        }

        [HttpPost("add-category-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddCategoryRoom([FromBody] string roomName)
        {
            return _marketplaceService.AddCategoryRoom(roomName);
            //return true;
        }

        [HttpPost("edit-category-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool EditCategoryRoom([FromBody] RoomNameModel model)
        {
            return _marketplaceService.EditCategoryRoom(model);
            //return true;
        }


        [HttpGet("get-category-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public RoomNameModel[] GetCategoryRooms()
        {
            return _marketplaceService.GetCategoryRooms();
            //return true;
        }

        [HttpPost("add-marketplace-color")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddMarketplaceColor([FromBody] MarketplaceColorModel model)
        {
            return _marketplaceService.AddMarketplaceColor(model);
            //return true;
        }

        [HttpPost("edit-marketplace-color")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool EditMarketplaceColor([FromBody] MarketplaceColorModel model)
        {
            return _marketplaceService.EditMarketplaceColor(model);
            //return true;
        }


        [HttpGet("get-marketplace-colors")]
        [APIAuthorize(Roles = Role.Authorized)]
        public MarketplaceColorModel[] GetMarketplaceColors()
        {
            return _marketplaceService.GetMarketplaceColors();
            //return true;
        }

        [HttpPost("map-company-color")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool MapCompanyColor([FromBody] MapColorModel model)
        {
            return _marketplaceService.MapCompanyColor(model);
            //return true;
        }

        [HttpPost("add-company-category-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddCompanyCategoryRoom([FromBody] CompanyCategoryRoomModel model)
        {
            return _marketplaceService.AddCompanyCategoryRoom(model);
            //return true;
        }

        [HttpPost("delete-company-category-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteCompanyCategoryRoom([FromBody] CompanyCategoryRoomModel model)
        {
            return _marketplaceService.DeleteCompanyCategoryRoom(model);
            //return true;
        }

        [HttpGet("get-rooms-by-company-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public RoomNameModel[] GetCompanyCategoryRooms([FromBody] CompanyCategoryKeyModel companyCategoryKey)
        {
            return _marketplaceService.GetRoomsInCompanyCategory(companyCategoryKey);
            //return true;
        }

        [HttpPost("delete-all-company-category-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteAllCompanyCategoryRooms([FromBody] CompanyCategoryKeyModel companyCategoryKey)
        {
            return _marketplaceService.DeleteAllCompanyCategoryRooms(companyCategoryKey);
            //return true;
        }

        [HttpPost("add-all-company-category-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddAllCompanyCategoryRooms([FromBody] CompanyCategoryKeyModel companyCategoryKey)
        {
            return _marketplaceService.AddAllCompanyCategoryRooms(companyCategoryKey);
            //return true;
        }

        [HttpGet("get-company-colors")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompanyColorModel[] GetCompanyColors()
        {
            return _marketplaceService.GetCompanyColors();
            //return true;
        }

        [HttpGet("get-moderation-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ModerationCountsModel GetModerationProduct()
        {
            var model = _adminService.GetModerationProduct();
            return model;
        }

        [HttpGet("get-product-for-moderation")]
        [APIAuthorize(Roles = Role.Authorized)]
        public GetModerationProductModel GetProductForModeration(string type)
        {
            var model = _adminService.GetProductForModeration(type);
            return model;
        }

        [HttpPost("set-moderation-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetModerationProduct([FromBody] ModerationProductModel model)
        {
            _adminService.SetModerationProduct(model);
            return true;
        }

        [HttpGet("get-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<ProductInfoModel> GetProduct([FromQuery] int productId, [FromQuery] string vendorCode)
        {
            List<Product> products = new List<Product>();
            if (productId > 0)
            {
                var product = _marketplaceService.GetProductById(productId);
                if (product == null)
                    throw new ServiceException($"Product with id {productId} not found", "product_not_found");
                products.Add(product);
            }

            if (!string.IsNullOrEmpty(vendorCode))
            {
                var vendorCodeProducts = _marketplaceService.GetProductsByVendorCode(vendorCode);
                if (vendorCodeProducts != null)
                    products.AddRange(vendorCodeProducts);
            }

            List<ProductInfoModel> result = new List<ProductInfoModel>();
            foreach (var product in products)
            {

                CompanyModel company = product.CompanyId != 0 ? _marketplaceService.GetCompanyById(product.CompanyId) : null;

                var companyCategory = _marketplaceService.GetCompanyCategory(product.CompanyId, product.CompanyCategoryId);

                result.Add(new ProductInfoModel
                {
                    ProductId = product.Id,
                    ProductName = product.Name,
                    ProductPrice = product.Price.ToString(),
                    Seller=product.Seller,
                    Vendor = product.Vendor,
                    CompanyName = company != null ? company.Name : "Не присвоено",
                    PhotoUrl = product.Pictures[0] != null ? product.Pictures[0] : string.Empty,
                    CompanyCategoryName = companyCategory.CompanyCategory,
                    VendorCode = product.VendorCode
                });
            }

            return result;
        }

        [HttpGet("get-category-products")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<ProductInfoModel> GetCategoryProducts([FromQuery] int companyId, [FromQuery] int companyCategoryId)
        {
            List<GetCategoryProductsModel> products = _adminService.GetCategoryProducts(companyId, companyCategoryId);
            if (products == null)
                throw new ServiceException($"Product with companyCategoryId {companyCategoryId} not found", "product_not_found");

            string company = _marketplaceService.GetCompanyById(companyId).Name ?? "Не присвоено";

            var productsInfo = products.Select(x => new ProductInfoModel {
                ProductId = x.Id,
                ProductName = x.Name,
                ProductPrice = x.Price.ToString(),
                Seller=x.Seller,
                Vendor = x.Vendor,
                VendorLink = x.Url,
                CompanyName = company,
                PhotoUrl = x.Pictures[0] != null ? x.Pictures[0] : string.Empty,
                CompanyCategoryName = x.CompanyCategoryName,
                CategoryName = x.CategoryName,
                Height = x.Height,
                Length = x.Length,
                Width = x.Width,
                VendorCode = x.VendorCode
            }).ToList();

            return productsInfo;
        }

        [HttpGet("change-product-category")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangeProductCategory([FromQuery] int productId, [FromQuery] int categoryId)
        {
            return _adminService.ChangeProductCategory(productId, categoryId);
        }

        [HttpGet("ar/get-ar-product-info")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AdminARProductModel GetArProductInfo([FromQuery] int arProductId)
        {
            var product = _adminService.GetArProductFullInfo(arProductId);
            return new AdminARProductModel
            {
                Id = product.Id,
                Color = product.Color,
                FaradiseColorId = product.FaradiseColorId,
                FaradiseProductId = product.FaradiseProductId,
                Name = product.Name,
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                Render = product.Render,
                VendorCode = product.VendorCode,
                Vertiacal = product.Vertiacal,
                AndroidBFM = CreateArModel(product.AndroidBFM),
                AndroidSFB = CreateArModel(product.AndroidSFB),
                IosBFM = CreateArModel(product.IosBFM),
                IosSCN = CreateArModel(product.IosSCN)
            };
        }

        [HttpPost("ar/set-product-to-ar")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetProducToAr([FromBody] SetArProductModel model)
        {
            _adminService.MapARProductToProduct(model.ProductId, model.ArProductId);
            return true;
        }

        [HttpGet("ar/get-products")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AdminARProductShortModel[] GetArProducts()
        {
            var products = _adminService.GetArProductList();
            if (products == null)
                return new AdminARProductShortModel[0];
            return products.Select(x => new AdminARProductShortModel
            {
                Id = x.Id,
                Color = x.Color,
                FaradiseName = x.FaradiseName,
                FaradiseId = x.FaradiseId,
                ProductId = x.ProductId != 0 ? x.ProductId : null,
                ProductName = x.ProductName
            }).ToArray();
        }

        [HttpGet("daily-products/possible")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PossibleDailyProductModel[] GetPossibleDailyProducts([FromQuery] int offset, [FromQuery] int count, [FromQuery] float companyKoef, [FromQuery] float categoryKoef, [FromQuery] float saleKoef)
        {
            var products = _adminService.GetPossibleDailyProducts(offset, count, companyKoef, categoryKoef, saleKoef);
            return products != null ? products.Select(x => x.ToModel()).ToArray() : new PossibleDailyProductModel[0];
        }

        [HttpGet("daily-products/current")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DailyProductModel GetCurrentDailyProduct()
        {
            var product = _adminService.GetDailyProduct();
            return product != null ? new DailyProductModel { Product = product.Product.ToModel(), OfferTime = product.OfferTime } : null;
        }

        [HttpPost("daily-products/set-daily-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetDailyProduct([FromBody] SetDailyProductModel model)
        {
            if (model == null || model.ProductId < 1)
                return false;
            _adminService.SetDailyProduct(model.ProductId);
            return true;
        }

        [HttpGet("daily-products/previous-list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DailyProductModel[] GetPreviousDailyProducts()
        {
            var products = _adminService.GetPreviousDailyProducts();
            return products != null ? products.Select(x => new DailyProductModel { Product = x.Product.ToModel(), OfferTime = x.OfferTime }).ToArray() : new DailyProductModel[0];
        }

        private AdminARPlatformModel CreateArModel(ARParsedModel serviceModel)
        {
            if (serviceModel == null)
                return null;
            return new AdminARPlatformModel
            {
                DiffuseUrl = serviceModel.DiffuseUrl,
                GeometryUrl = serviceModel.GeometryUrl,
                MetalicUrl = serviceModel.MetalicUrl,
                MetallicRoughnessUrl = serviceModel.MetallicRoughnessUrl,
                NormalUrl = serviceModel.NormalUrl,
                RoughnessUrl = serviceModel.RoughnessUrl
            };
        }
    }
}