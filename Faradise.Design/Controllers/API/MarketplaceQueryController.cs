﻿using System.Linq;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services;
using Faradise.Design.Services.AzureSearch;
using Microsoft.AspNetCore.Mvc;
using Unidecode.NET;
using Faradise.Design.Services.DataMappers;

namespace Faradise.Design.Controllers.API
{
    [Route("api/marketplace")]
    public class MarketplaceQueryController : APIController
    {
        private readonly IDBMarketplaceQueryService _queryService = null;
        
        public MarketplaceQueryController(IDBMarketplaceQueryService queryService)
        {
            _queryService = queryService;
        }

        /// <summary>
        /// Полчить категории
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-categories")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CategoryModel[] GetCategories()
        {
            var categories = _queryService
                .GetCategories()
                .Where(x => x.ParentId == 0)
                .Where(x => x.IsEnabled)
                .Select(x => x.ToModel())
                .ToList();
            return categories.ToArray();
        }

        /// <summary>
        /// Полчить подкатегории
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-subcategories")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CategoryModel[] GetSubcategories([FromQuery] int id)
        {
            var categories = _queryService
                .GetSubcategories(id)
                .Select(x => x.ToModel())
                .ToList();
            return categories.ToArray();
        }

        [HttpGet("get-category-hierarchy")]
        public CategoryHierarchyModel[] GetCategoryHierarchy()
        {
            var hierarchy = _queryService.GetCategoryHierarchy();

            return hierarchy
                .Select(ToModel)
                .ToArray();
        }

        /// <summary>
        /// Возвращает значения для фильтрации
        /// </summary>
        /// <param name="categoryId">Категория, по которой проводится фильтрация</param>
        /// <returns></returns>
        [HttpGet("get-filters")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AvailableFiltersModel GetFilterValues([FromQuery] int categoryId)
        {
            var filters = _queryService.GetQueryFilters(categoryId);

            return new AvailableFiltersModel
            {
                Products = filters.Products,
                MinPrice = filters.MinPrice,
                MaxPrice = filters.MaxPrice,

                Brands = filters.Brands.ToArray(),
                Colors = filters.Colors.ToArray(),
                Rooms = filters.Rooms.ToArray(),

                ContainsARProducts = filters.ContainsARProducts,

                Measurements = filters.Measurements
            };
        }

        /// <summary>
        /// Возвращает отфильтрованный список продуктов
        /// </summary>
        /// <param name="model">Модель для фильтрации</param>
        /// <param name="platform"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        [HttpGet("get-products-filtered")]
        [APIAuthorize(Roles = Role.Authorized)]
        public GetProductsModel FilterProducts(
            [FromQuery] ProductsFiltrationModel model,
            [FromQuery] ARPlatform platform = ARPlatform.Browser,
            [FromQuery] ARFormat format = ARFormat.None)
        {
            var filters = new ProductsQueryFiltersValues
            {
                Category = model.Category > 0 ? new int?(model.Category) : null,

                PriceFrom = model.PriceFrom > 0 ? new int?(model.PriceFrom) : null,
                PriceTo = model.PriceTo > 0 ? new int?(model.PriceTo) : null,

                Ordering = model.Ordering,
                Offset = model.Offset,
                Count = model.Count,

                AR = model.AROnly,

                Brands = model.Brands,
                Colors = model.Colors,
                Rooms = model.Rooms,

                SalesOnly = model.SalesOnly,

                SearchWords = string.IsNullOrEmpty(model.SearchWords) ? null : model.SearchWords,
                Measurements = new ProductMeasurements
                {
                    MinWidth = model.MinWidth,
                    MaxWidth = model.MaxWidth,
                    MinHeight = model.MinHeight,
                    MaxHeight = model.MaxHeight,
                    MinLength = model.MinLength,
                    MaxLength = model.MaxLength
                }
            };

            var products = _queryService.GetProductsByFilters(filters, platform, format);
            var result = new GetProductsModel
            {
                AllCount = products.AllCount,
                Products = products.Products
                    .Select(x => x.ToModel())
                    .ToArray()
            };

            return result;
        }

        //Получить продукт дня (на самом деле не дня)
        [HttpGet("get-daily-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ShortProductModel GetDailyProduct()
        {
            var product = _queryService.GetDailyProduct();
            return product != null ? product.ToModel() : null;
        }

        /// <summary>
        /// Возвращает категории в иерархическом порядке до указанной
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet("get-category-path")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CategoryInfoModel[] GetCategoryPath(int categoryId)
        {
            return _queryService
                .GetCategoryPath(categoryId)
                .Select(x => new CategoryInfoModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    NameTransliterated = x.Name
                        .Unidecode()
                        .Trim()
                        .ToLower()
                        .Replace(" ", "_")
                        .Replace(",", "_")
                })
                .ToArray();
        }

        /// <summary>
        /// Возвращает количество доступных в маркетплейсе товаров
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-available-products-count")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public int GetProductsCount()
        {
            return _queryService.GetAvailableProductsCount();
        }

        [HttpGet("get-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProductModel GetProduct([FromQuery] int productId, [FromQuery] ARPlatform platform = ARPlatform.Browser,
            [FromQuery] ARFormat format = ARFormat.None)
        {
            var product = _queryService.GetProductById(productId);
            if (product == null)
                throw new ServiceException($"Product with id {productId} not found", "product_not_found");

            var result = product.ToModel();

            result.Path = _queryService
                .GetProductCategoryPath(productId)
                .ToList();

            if (platform != ARPlatform.Browser && format != ARFormat.None)
                result.ARDefinition = _queryService.GetARDefinition(productId, platform, format);

            if (result.Notes != null && result.Notes.Any())
            {
                result.Notes = result.Notes
                    .Where(x => !x.ToLower().Contains("available"))
                    .Where(x => !x.ToLower().Contains("price"))
                    .Where(x => !x.ToLower().Contains("галереи"))
                    .Where(x => !x.ToLower().Contains("<"))
                    .Where(x => !x.ToLower().Contains("buy"))
                    .Where(x => !x.ToLower().Contains("предоплат"))
                    .Where(x => !x.ToLower().Contains("http"))
                    .Where(x => !x.ToLower().Contains("тип начисления бонусов"))
                    .Where(x => !x.ToLower().Contains("регионы с флагом"))
                    .Where(x => !x.ToLower().Contains("наличие 3d-модели"))
                    .Where(x => !x.ToLower().Contains("ортировка"))
                    .Where(x => !x.ToLower().Contains("комментари"))
                    .Where(x => !x.ToLower().Contains("остаток"))
                    .Where(x => !x.ToLower().Contains("упаковк"))
                    .Where(x => !x.ToLower().Contains("itemtype"))
                    .Where(x => !x.ToLower().Contains("width"))
                    .Where(x => !x.ToLower().Contains("height"))
                    .Where(x => !x.ToLower().Contains("length"))
                    .Where(x => !x.ToLower().Contains("weight"))
                    .ToArray();
            }
            else result.Notes = new string[0];

            if (result.Price > 0 && result.PreviousPrice.HasValue && result.Price < result.PreviousPrice)
                result.Discount =
                    (int) (0.5 + (result.PreviousPrice - result.Price) / (float) result.PreviousPrice * 100);

            return result;
        }

        #region Mappers

        private static CategoryHierarchyModel ToModel(CategoryHierarchy category)
        {
            return new CategoryHierarchyModel
            {
                Id = category.Id,

                Name = category.Name,
                ImageUrl = category.ImageUrl,
                NameTransliterated = category.Name
                    .Unidecode()
                    .Trim()
                    .ToLower()
                    .Replace(" ", "_")
                    .Replace(",", "_"),

                Childs = category.Childs
                    .Select(ToModel)
                    .ToArray()
            };
        }

        #endregion
    }
}