﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Brigade;
using Faradise.Design.Controllers.API.Models.VisulizationFreelance;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/freelance")]
    public class BrigadeController : APIController
    {
        private readonly IBrigadeService _brigadeService = null;
        private readonly ICDNService _cdnService = null;
        private readonly IProjectDefinitionService _defService = null;

        public BrigadeController(IBrigadeService brigadeService, ICDNService cdnService, IProjectDefinitionService defService)
        {
            _brigadeService = brigadeService;
            _cdnService = cdnService;
            _defService = defService;
        }

        /// <summary>
        /// Установить описание тз одной комнаты для бригады
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("brigade/set-techspec")]
        [APIAuthorize(Roles = Role.Designer)]
        public void SetFreelanceTechSpec([FromBody] SetBrigadeTechSpecModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _brigadeService.SetDescription(model.ProjectId, model.Description);
        }

        /// <summary>
        /// Получить описание тз для бригады 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("brigade/get-techspec")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BrigadeTechSpecModel GetFreelanceTechSpec(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var data = _brigadeService.GetBrigadeTechSpec(projectId, user.Id);
            return data != null ? data.ToModel() : null;
        }

        /// <summary>
        /// Добавить файл для тз бригады
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("brigade/add-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> AddVisualizationFile([FromForm] UploadBrigadeTechSepecFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForBrigadeFiles(model.ProjectId));
            return _brigadeService.AddFile(model.ProjectId, url, model.File.FileName);
        }

        /// <summary>
        /// Удалить файл для тз бригады
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("brigade/remove-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public void RemoveVisualizationFile([FromBody] DeleteBrigadeTechSepecFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _brigadeService.RemoveFile(model.ProjectId, model.FileId);
        }

        /// <summary>
        /// Опубликовать ТЗ
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("brigade/publish-tech-spec")]
        [APIAuthorize(Roles = Role.Designer)]
        public void PublishFreelanceTechSpec([FromBody] PublishBrigadeTeshSpecModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _brigadeService.Publish(model.ProjectId, user.Id);
        }
    }
}
