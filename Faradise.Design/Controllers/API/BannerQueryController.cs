﻿using System;
using System.Linq;
using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/banner")]
    public class BannerQueryController : APIController
    {
        private readonly IDBBannerQueryService _queryService;

        public BannerQueryController(IDBBannerService dbService, IDBBannerQueryService queryService)
        {
            _queryService = queryService;
        }

        /// <summary>
        /// Получение баннера по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("get-banner")]
        [APIAuthorize(Roles = Role.Authorized)]
        public Banner Get([FromQuery] int id)
        {
            return _queryService.GetBanner(id);
        }

        /// <summary>
        /// Получение всех активных баннеров, отсортированных по приоритету
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-banners-list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BannerList GetActiveBanners()
        {
            var banners = _queryService.GetAllActiveBanners();
            return new BannerList
            {
                Banners = banners.Any() ? banners.OrderByDescending(banner => banner.Order).ToArray() : new Banner[0]
            };
        }
    }
}