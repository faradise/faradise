﻿using Faradise.Design.Controllers.API.Models.Compilations;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProductCompilation;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/compilations")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APICompilationsController : APIController
    {
        private ICompilationService _compilationService;

        public APICompilationsController(ICompilationService compilationService)
        {
            _compilationService = compilationService;
        }

        [HttpPost("create")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompilationModel CreateCompilation([FromBody] CreateCompilationModel model)
        {
            var compId = _compilationService.CreateCompilation(model.Source);
            return new CompilationModel { Id = compId, Source = model.Source };
        }

        [HttpPost("delete")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteCompilation([FromBody] DeleteCompilationModel model)
        {
            _compilationService.DeleteCompilation(model.Id, model.Source);
            return true;
        }

        [HttpPost("modify")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ModifyCompilation([FromBody] CompilationModel model)
        {
            _compilationService.UpdateCompilation(new ProductCompilation { Id = model.Id, Name = model.Name, Source = model.Source, Categories = model.Categories, Products = model.Products });
            return true;
        }

        [HttpGet("get-compilation")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompilationModel GetCompilation([FromQuery] int compilationId, [FromQuery] CompilationSource source)
        {
            var comp = _compilationService.GetCompilation(compilationId, source);
            return comp != null ? new CompilationModel { Id = comp.Id, Categories = comp.Categories, Name = comp.Name, Products = comp.Products } : null;
        }

        [HttpGet("get-compilation-list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CompilationListModel GetCompilationList()
        {
            var list = _compilationService.GetCompilations();
            var modelList = list != null ? list.Select(x => new CompilationShortModel { Id = x.Id, Name = x.Name, Source = x.Source }).ToArray() : new CompilationShortModel[0];
            return new CompilationListModel { Compilations = modelList };
        }
    }
}
