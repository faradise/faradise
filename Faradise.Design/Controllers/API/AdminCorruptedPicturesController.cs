﻿using System;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.CorruptedPictures;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/pictures")]
    public class AdminCorruptedPicturesController : APIController
    {
        private readonly IDBCorruptedPicturesService _dbService;

        public AdminCorruptedPicturesController(IDBCorruptedPicturesService dbService)
        {
            _dbService = dbService;
        }

        [HttpGet("problem-partners-list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CorruptedPartnersList ProblemPartnersList()
        {
            var partners = _dbService.GetAllInvalidPartners();
            return new CorruptedPartnersList {CorruptedPartners = partners};
        }
    }
}