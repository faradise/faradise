﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Services.Configs;
using Faradise.Design.Models.Enums;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.Marketplace;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Faradise.Design.Controllers.API.Models
{
    [Route("api/marketplace")]
    public class MarketplaceController : APIController
    {
        private readonly IMarketplaceService _marketplaceService;
        private readonly IProjectDefinitionService _defService;
        private readonly IProjectPreparationsService _prepService;
        private readonly IProjectDevelopmentService _devService;
        private readonly IBasketService _basketService;
        private readonly IDBBasketService _dbBasketService;
        private readonly IDBMarketplaceService _dbMarketplaceService;
        private readonly MarketplaceOptions _marketOptions;
        private readonly IPaymentService _paymentService;
        private readonly IUserService _userService;
        private readonly IMailSendingService _mailService;
        private readonly IPromoCodeService _promocodeService;

        public MarketplaceController(
            IMarketplaceService marketplaceService, 
            IProjectDefinitionService defService, 
            IProjectPreparationsService prepService, 
            IProjectDevelopmentService devService, 
            IBasketService basketService,
            IDBBasketService dbBasketService,
            IDBMarketplaceService dbMarketplaceService,
            IOptions<MarketplaceOptions> marketOptions,
            IPaymentService paymentService,
            IUserService userService,
            IMailSendingService mailService,
            IPromoCodeService promocodeService)
        {
            _marketplaceService = marketplaceService;
            _defService = defService;
            _prepService = prepService;
            _devService = devService;
            _basketService = basketService;
            _dbBasketService = dbBasketService;
            _dbMarketplaceService = dbMarketplaceService;
            _marketOptions = marketOptions.Value;
            _paymentService = paymentService;
            _userService = userService;
            _mailService = mailService;
            _promocodeService = promocodeService;
        }

        [HttpGet("get-possible-projects")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<ProjectSelectionModel> GetPossibleProjectsAndRooms(int productId)
        {
            var user = (Design.Models.User)HttpContext.Items[typeof(Design.Models.User)];
            var userProjects = _defService.GetProjectsByUser(user.Id);
            if (userProjects == null || userProjects.Length < 1)
                return new List<ProjectSelectionModel>();
            var projectList = new List<ProjectSelectionModel>();
            foreach (var project in userProjects)
            {
                if (project.Stage < ProjectStage.Development)
                    continue;
                var pModel = new ProjectSelectionModel();
                pModel.ProjectName = project.Name;
                pModel.ProjectId = project.Id;
                var rooms = _prepService.GetRooms(project.Id);
                if (rooms == null)
                    continue;
                pModel.Rooms = new RoomSelectionModel[rooms.Count];
                for(int rId = 0; rId < rooms.Count; rId++)
                {
                    var r = rooms[rId];
                    var roomSelectionModel = new RoomSelectionModel() { RoomId = r.RoomId, Purpose = r.Purpose, Size = r.Proportions != null ? r.Proportions.SideA + "x" + r.Proportions.SideB : string.Empty };
                    var orderedItemsForRoom = _devService.GetMarketplaceGoods(r.RoomId);
                    var countInRoom = productId != 0 && orderedItemsForRoom != null && orderedItemsForRoom.MarketplaceItems != null ? orderedItemsForRoom.MarketplaceItems.Where(x => x.ProductId == productId).Sum(x => x.Count) : 0;
                    roomSelectionModel.HasProduct = countInRoom > 0;
                    roomSelectionModel.ProductCount = countInRoom;
                    pModel.Rooms[rId] = roomSelectionModel;
                }
                projectList.Add(pModel);
            }
            return projectList;
        }

        [HttpPost("add-product-to-project")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddProductsToProject([FromBody] AddProductToProjectModel model)
        {
            if (model.RoomId == 0 || model.ProductId == 0)
                throw new ServiceException("Room or Projects not specified", "room_or_project_invalid");
            var user = (Design.Models.User)HttpContext.Items[typeof(Design.Models.User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var product = _marketplaceService.GetProductById(model.ProductId);
            var item = new MarketplaceItem();
            item.Count = model.Count;
            item.ProductId = model.ProductId;
            item.PaidFor = false;
            _devService.AddMarketplaceGoods(new RoomGoods()
            {
                RoomId = model.RoomId,
                MarketplaceItems = new List<MarketplaceItem>() { item }
            });
        }

        [HttpPost("remove-product-from-project")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RemoveProductsFromProject([FromBody] RemoveProductFromProjectModel model)
        {
            if (model.RoomId == 0 || model.ProductId == 0)
                throw new ServiceException("Room or Projects not specified", "room_or_project_invalid");
            var user = (Design.Models.User)HttpContext.Items[typeof(Design.Models.User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var product = _marketplaceService.GetProductById(model.ProductId);
            var item = new MarketplaceItem();
            item.Count = model.Count;
            item.ProductId = model.ProductId;
            item.PaidFor = false;
            _devService.RemoveMarketplaceGoods(new RoomGoods()
            {
                RoomId = model.RoomId,
                MarketplaceItems = new List<MarketplaceItem>() { item }
            });
        }

        [HttpGet("get-products-for-project-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<OrderedProductModel> GetProductsForProjectAndRoom(GetOrderedProductsForRoomModel model)
        {
            var user = (Design.Models.User)HttpContext.Items[typeof(Design.Models.User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");

            var goods = _devService.GetMarketplaceGoods(model.RoomId);
            if (goods == null || goods.MarketplaceItems == null || goods.MarketplaceItems.Count < 1)
                return new List<OrderedProductModel>();

            var roomProducts = new List<OrderedProductModel>();
            foreach (var marketItem in goods.MarketplaceItems)
            {
                var product = _marketplaceService.GetProductById(marketItem.ProductId);
                if (product == null)
                    continue;

                var color = product.Color != null ? product.Color.FirstOrDefault() : string.Empty;
                var orderProduct = new OrderedProductModel()
                {
                    ShortProductModel = product.ToShortModel(),
                    Count = marketItem.Count,
                    PaidFor = marketItem.PaidFor,
                    Color = color,
                    IsAvailableNow = product.Available
                };

                roomProducts.Add(orderProduct);
            }

            return roomProducts;
        }

        [HttpPost("issue-order-for-project")]
        [APIAuthorize(Roles = Role.Client)]
        public void IssueOrderForProject([FromBody] IssueOrderModel model)
        {
            var user = (Design.Models.User)HttpContext.Items[typeof(Design.Models.User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var orderDescriptoion = new OrderDescription()
            {
                Adress = model.Adress,
                City = string.Empty,
                Description = model.Description,
                Email = model.Email,
                Name = model.Name,
                Phone = model.Phone,
                Surname = model.Surname
            };
            _devService.IssueOrder(model.ProjectId, orderDescriptoion, model.Products.Select(x => new ProductOrder() { ProductId = x.ProductId, Count = x.Count, RoomId = x.RoomId }).ToArray(), model.PaymentMethod, model.DeliveryCost);
        }

        [HttpGet("get-rooms")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public RoomNameModel[] GetMarketplaceRooms()
        {
            return _marketplaceService.GetCategoryRooms();
        }

        /// <summary>
        /// Добавить продукты в корзину
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("add-product-count-to-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddProductToBasket([FromBody] ProductCountModel model)
        {
            if (model.Count < 1)
                return;
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.AddProductToBasket(user, model.ProductId, model.Count);
        }

        
        /// <summary>
        /// Получить массив продуктов из корзины
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-market-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ShopBasketModel GetMarketBasket()
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            var cartProducts = _basketService.GetAllProductsInBasket(user);
            return new ShopBasketModel
            {
                Products = cartProducts != null ? cartProducts.Select(x => new BasketProductModel
                {
                    Product = x.Product.ToModel(),
                    Count = x.Count
                }).ToArray() : new BasketProductModel[0]
            };
        }

        
        /// <summary>
        /// Установить корзину, вычистив старую перед этим
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("set-market-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetMarketBasket([FromBody] SetBasketModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.SetBasketBin(user, model.Products != null ? model.Products.Select(x => new ProductCount { Count = x.Count, ProductId = x.ProductId }).ToArray() : new ProductCount[0]);
        }

        /// <summary>
        /// Добавить список продуктов в корзину
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("add-product-list-to-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetMarketBasket([FromBody] AddProductListToBasketModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.AddProductsListToBasket(user, model.Products != null ? model.Products.Select(x => new ProductCount { ProductId = x.ProductId, Count = x.Count }).ToArray() : new ProductCount[0]);
        }

        /// <summary>
        /// Удалить продукт из корзины (количество)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("remove-product-count-from-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RemoveProductFromBasket([FromBody] ProductCountModel model)
        {
            if (model.Count < 1)
                return;
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.RemoveProductFromBasket(user, model.ProductId, model.Count);
        }

        /// <summary>
        /// Удалить продукт из корзины (полностью)
        /// </summary>
        /// <returns></returns>
        [HttpPost("remove-product-from-basket-completely")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RemoveAllProductFromBasket([FromBody] RemoveAllProductsFromBasketModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.RemoveAllProductsFromBasket(user, model.ProductId);
        }

        /// <summary>
        /// Удалить все продукты из корзины
        /// </summary>
        /// <returns></returns>
        [HttpPost("clear-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void ClearBasket()
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            _basketService.ClearBasket(user);
        }

        /// <summary>
        /// Оформить заказ из корзины
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("order-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int OrderBasket([FromBody] MakeOrderModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
                throw new ServiceException("email not specified in order description", "email_is_empty");
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            var orderid = _basketService.CreateOrderFromBasket(user, new OrderDescription()
            {
                Adress = model.Adress,
                City = string.Empty,
                Description = model.Description,
                Email = model.Email,
                Name = model.ClientName,
                Phone = model.Phone,
                Surname = model.ClientSurname,
                PromoCode = model.PromoCode
            }, model.PaymentMethod, model.DeliveryCost);
            return orderid;
        }

        /// <summary>
        /// Оформить заказ в один клик
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create-one-click-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int CreateOneClickOrder([FromBody] OneClickOrderModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]).Id;
            var orderDescription = new OneClickOrderDescription
            {
                Email = model.Email ?? string.Empty,
                Name = model.Name,
                Phone = model.Phone,
                PromoCode = model.PromoCode
            };
            var orderid = _basketService.CreateOneClickOrder(user, model.ProductId, model.ProductCount, orderDescription, model.PaymentMethod, model.DeliveryCost);
            return orderid;
        }

        /// <summary>
        /// Получить информацию о заказе
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet("order/basket/info")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BasketOrderInfoModel GetBasketOrder([FromQuery] int orderId)
        {
            var order = _basketService.GetOrderInfo(orderId);
            var orderInfoModel = new BasketOrderInfoModel();
            orderInfoModel.OrderId = order.OrderId;
            orderInfoModel.ClientName = order.OrderDescription.Name;
            orderInfoModel.Items = order.Products.Select(x => new BasketOrderItemModel { Name = x.Name, PricePerOne = x.Price, ProductId = x.Id, Count = x.Count }).ToArray();
            orderInfoModel.BasePrice = order.BasePrice;
            orderInfoModel.DeliveryPrice = order.DeliveryPrice;
            orderInfoModel.AdditionalPrice = order.AdditionalPrice;
            orderInfoModel.AbsoluteDiscount = order.AbsoluteDiscount;
            return orderInfoModel;
        }

        /// <summary>
        /// Получение ссылки на оплату проекта
        /// </summary>
        /// <param name="order_id"></param>
        [HttpGet("order/payroute/project/{order_id}")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task GetProjectPaymentRoute(int order_id)
        {
            var order = _devService.GetOrderInformation(order_id);
            var client = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]);
            if (client == null)
            {
                Response.Redirect(_marketOptions.ProjectOrderRedirectUrl);
                return;
            }
            if (!_defService.CheckUserOwnsService(order.ProjectId, client.Id) || 
                order == null ||
                !order.IsReady ||
                order.PaymentId == null ||
                order.PaymentMethod != PaymentMethod.YandexKassa) //TODO: payment method check here
            {
                Response.Redirect(_marketOptions.ProjectOrderRedirectUrl);
                return;
            }
            try
            {
                var payment = await _paymentService.UpdateKassaPaymentRoute(order.PaymentId.Value);
                Response.Redirect(payment.PaymentRoute);
            } catch
            {
                Response.Redirect(_marketOptions.ProjectOrderRedirectUrl);
            }
        }

        /// <summary>
        /// Получение ссылки на оплату корзины
        /// </summary>
        /// <param name="order_id"></param>
        [HttpGet("order/payroute/market/{order_id}")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task GetBasketPaymentRoute(int order_id)
        {
            var order = _basketService.GetOrderInfo(order_id);
            if (order == null ||
                !order.IsReady ||
                order.PaymentId == null ||
                order.SelectedPaymentMethod != PaymentMethod.YandexKassa) //TODO: payment method check here
            {
                Response.Redirect(_marketOptions.BasketOrderRedirectUrl);
                return;
            }
            try
            {
                var payment = await _paymentService.UpdateKassaPaymentRoute(order.PaymentId.Value);
                Response.Redirect(payment.PaymentRoute);
            }  catch
            {
                Response.Redirect(_marketOptions.BasketOrderRedirectUrl);
            }
        }

        [HttpPost("callback/ask-for-call")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AskForCall([FromBody] AskForCallModel model)
        {
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]);
            if (user != null)
            {
                if (string.IsNullOrEmpty(user.Name) && !string.IsNullOrEmpty(model.Name))
                    _userService.AddUserName(user.Id, model.Name);
                if (string.IsNullOrEmpty(user.Phone) && !string.IsNullOrEmpty(model.Phone))
                    _userService.AddUserPhone(user.Id, model.Phone);
            }
            var template = new CustomerDevelopmentTemplate
            {
                clientid = user != null ? user.Id.ToString() : @"N/A",
                return_url = string.Empty,
                name = model.Name ?? @"N/A",
                phone = model.Phone ?? @"N/A"
            };
            _mailService.SendEmail<CustomerDevelopmentTemplate>(new Services.Implementation.MailSending.EmailModel()
            {
                TemplateEngine = "velocity",
                TemplateModel = template,
                TemplateName = _mailService.CustomerDevelopmentTemplateId,
                ToAddress = new string[] { _mailService.CustomerDevelopmentEmail }
            });
        }

        [HttpPost("order/apply-promocode-to-basket")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ApplyPromoCodeResultModel ApplyPromocodeToBasket([FromBody] ApplyPromoCodeToBasketModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
                return new ApplyPromoCodeResultModel { Error = PromocodeError.NotFound };
            var user = ((Design.Models.User)HttpContext.Items[typeof(Design.Models.User)]);
            var applyResult = _promocodeService.ApplyPromocodeToBasket(model.Code, user.Id);
            return applyResult.ToModel();
        }

        [HttpPost("order/apply-promocode-to-products")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ApplyPromoCodeResultModel ApplyPromocodeToProducts([FromBody] ApplyPromoCodeToProductsModel model)
        {
            if (string.IsNullOrEmpty(model.Code))
                return new ApplyPromoCodeResultModel { Error = PromocodeError.NotFound };
            if (model.Products == null || model.Products.Length < 1)
                return new ApplyPromoCodeResultModel { Error = PromocodeError.None };
            var applyResult = _promocodeService.ApplyPromocodeToProducts(model.Code, model.Products.Select(x => new ProductCount { Count = x.Count, ProductId = x.ProductId }).ToArray());
            return applyResult.ToModel();
        }
    }
}
