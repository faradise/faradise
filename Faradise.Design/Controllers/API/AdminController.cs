﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.PromoCode;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Produces("application/json")]
    [Route("api/admin")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AdminController : APIController
    {
        private IAdminService _adminService;
        private IPromoCodeService _promoCodeService;
        public AdminController(IAdminService adminService, IPromoCodeService promoCodeService)
        {
            _adminService = adminService;
            _promoCodeService = promoCodeService;
        }

        [HttpPost("appoint-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AppointDesigners([FromBody] AppointDesignerModel model)
        {
            _adminService.AppointDesigners(model.ProjectId, model.DesignersIds);
            return true;
        }

        [HttpPost("remove-designer-from-pool")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool RemoveDesignerFromPool([FromBody] RemoveDesignerFromPoolModel model)
        {
            _adminService.RemoveDesignerFromPool(model.ProjectId, model.DesignerId);
            return true;
        }


        [HttpGet("get-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerInfo[] GetDesigners()
        {
            return _adminService.GetFullListDesigners()
                .ToArray();
        }

        [HttpGet("get-designer")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerModel GetDesigner(int designerId)
        {
            var model = _adminService.GetDesignerFullInfo(designerId);
            return model;
        }

        [HttpGet("get-designer-projects")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int[] GetDesignerProjects(int designerId)
        {
            return _adminService.GetDesignerProjects(designerId);             
        }

        [HttpPost("apply-designer")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ApplyDesigner([FromBody] ApplyDesignerModel model)
        {
            _adminService.ApplyDesigner(model.DesignerId, model.Level);
            return true;
        }

        [HttpPost("reject-designer")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool RejectDesigner([FromBody] int designerId)
        {
            _adminService.RejectDesigner(designerId);
            return true;
        }

        [HttpPost("mark-admin-viewed")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool MarkAdminViewed([FromBody] int projectId)
        {
            _adminService.MarkProjectAsViewed(projectId);
            return true;
        }

        [HttpPost("reset-admin-attention")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetAdminAttention([FromBody] int projectId)
        {
            _adminService.SetProjectAdminAttention(projectId, false);
            return true;
        }


        [HttpGet("get-projects")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AdminProjectInfo[] GetProjects()
        {
            return _adminService.GetProjectsInfo();
        }

        [HttpGet("get-project")]
        public AdminProjectModel GetProject(int projectId)
        {
            var projectInfo = _adminService.GetFullProjectInformation(projectId);
            return projectInfo;
        }

        [HttpPost("finish-project")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool FinishProject([FromBody] int projectId)
        {
            _adminService.FinishProject(projectId);
            return true;
        }

        [HttpGet("get-appointed-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerInfo[] GetAppointedDesigners([FromQuery] int projectId)
        {
            return _adminService.GetAppointedDesigners(projectId);
        }

        [HttpGet("get-rejected-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerRejectedInfo[] GetRejectedDesigners([FromQuery] int projectId)
        {
            return _adminService.GetRejectedDesigners(projectId);
        }

        [HttpGet("get-suitable-designers")]
        [APIAuthorize(Roles = Role.Authorized)]
        public DesignerInfo[] GetSuitableDesigners([FromQuery] int projectId)
        {
            return _adminService.GetSuitableDesigners(projectId);
        }

        [HttpPost("complete-visualizations")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool CompleteVisualizationForProject([FromBody] CompleteVisualizationModel model)
        {
            _adminService.CompleteVisualizationForProject(model.ProjectId, model.VisualizationLink);
            return true;
        }

        [HttpGet("get-visualizations")]
        [APIAuthorize(Roles = Role.Authorized)]
        public FullVisualizationModel[] GetVisualizationStatusForProject(int projectId)
        {
            return _adminService.GetVisualizationsForProject(projectId).Select(x =>
             new FullVisualizationModel()
             {
                 IsComplete = x.IsComplete,
                 IsSelected = x.IsSelected,
                 PaymentStatus = x.PaymentStatus,
                 Price = x.Price,
                 RoomId = x.RoomId,
                 VisualizationCode = x.VisualizationCode,
                 ProjectId = projectId
             }).ToArray();
        }

        [HttpPost("create-payment-for-project-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<string> CreatePaymentForProjectProductsOrder([FromBody] CreatePaymentForProjectModel model)
        {
            try
            {
                return await _adminService.AuthorizePaymentForProjectOrder(model.OrderId);
            }
            catch
            {
                return "price_missmatch_error";
            }
        }

        [HttpGet("get-orders-for-project")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ShortProductOrderModel[] GetOrdersForProject([FromQuery] int projectId)
        {
            var orders = _adminService.GetOrdersForProject(projectId);
            return orders.Select(x => new ShortProductOrderModel()
            {
                AdditionalPrice = x.AdditionalPrice,
                BasePrice = x.BasePrice,
                CreationTime = x.CreationTime,
                OrderId = x.OrderId,
                PaymentId = x.PaymentId,
                PaymentStatus = x.PaymentStatus,
                OrderStatus = x.OrderStatus,
                Name = x.Name,
                PaymentMethod = x.SelectedPaymentMethod,
                DeliveryPrice = x.DeliveryPrice
            }).ToArray();
        }

        [HttpGet("get-orders-information")]
        [APIAuthorize(Roles = Role.Authorized)]
        public FullProductProjectOrderModel GetOrderInformation([FromQuery] int orderId)
        {
            var order = _adminService.GetOrderInformation(orderId);
            var items = new List<ProductOrderItemsModel>();
            foreach (var goodsInRoom in order.Goods)
            {
                if (goodsInRoom.MarketplaceItems == null)
                    continue;
                foreach (var item in goodsInRoom.MarketplaceItems)
                    items.Add(new ProductOrderItemsModel() { RoomId = goodsInRoom.RoomId, ProductId = item.ProductId, Count = item.Count });
            }
            return new FullProductProjectOrderModel()
            {
                AdditionalPrice = order.AdditionalPrice,
                Adress = order.OrderDescription != null ? order.OrderDescription.Adress : string.Empty,
                BasePrice = order.BasePrice,
                City = order.OrderDescription != null ? order.OrderDescription.City : string.Empty,
                Description = order.OrderDescription != null ? order.OrderDescription.Description : string.Empty,
                Email = order.OrderDescription != null ? order.OrderDescription.Email : string.Empty,
                Name = order.OrderDescription != null ? order.OrderDescription.Name : string.Empty,
                OrderId = order.OrderId,
                PaymentId = order.PaymentId,
                PaymentLink = order.PaymentLink,
                ProjectId = order.ProjectId,
                CreationTime = order.CreateionTime,
                PaymentStatus = order.PaymentStatus,
                PlatformLink = order.PlatformLink,
                Phone = order.OrderDescription != null ? order.OrderDescription.Phone : string.Empty,
                Surname = order.OrderDescription != null ? order.OrderDescription.Surname : string.Empty,
                Items = items.ToArray(),
                DeliveryPrice = order.DeliveryPrice,
                OrderStatus = order.Status,
                PaymentMethod = order.PaymentMethod
            };
        }

        [HttpPost("finish-project-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool FinishProjectOrder([FromBody] int orderId)
        {
            _adminService.FinishProjectOrder(orderId);
            return true;
        }

        [HttpPost("cancel-project-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool CancelProjectOrder([FromBody] int orderId)
        {
            _adminService.CloseProjectOrder(orderId);
            return true;
        }

        [HttpPost("set-project-order-status")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool CancelProjectOrder([FromBody] SetOrderStatusModel model)
        {
            _adminService.SetProjectOrderStatus(model.OrderId, model.Status);
            return true;
        }

        [HttpPost("add-project-order-price")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddAdditionalPriceForProjectOrder([FromBody] SetAdditionalPriceForOrderModel model)
        {
            _adminService.SetAdditionalPriceForProjectOrder(model.OrderId, model.Price);
            return true;
        }

        [HttpPost("add-project-order-delivery-price")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddDeliveryPriceForProjectOrder([FromBody] SetAdditionalPriceForOrderModel model)
        {
            _adminService.SetDeliveryPriceForProjectOrder(model.OrderId, model.Price);
            return true;
        }

        [HttpPost("remove-goods-from-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void RemoveGoodsFromOrder([FromBody] RemoveGoodsFromOrderModel model)
        {
            _adminService.RemoveGoodsFromOrder(model.OrderId, new RoomGoods()
            {
                RoomId = model.RoomId,
                MarketplaceItems = new List<MarketplaceItem>() { new MarketplaceItem() { ProductId = model.ProductId, Count = model.Count, PaidFor = true } }
            });
        }

        [HttpPost("notify-pool-is-finish")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool NotifyPoolIsFinish([FromBody] int id)
        {
            _adminService.SetDesignerPoolAsFinished(id);
            return true;
        }

        [HttpGet("get-basket-orders")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ShortProductOrderModel[] GetBasketOrders([FromQuery] int from, [FromQuery] int count)
        {
            var orders = _adminService.GetBasketOrders(from, count);
            if (orders == null)
                return new ShortProductOrderModel[0];
            return orders.Select(x => new ShortProductOrderModel()
            {
                AdditionalPrice = x.AdditionalPrice,
                BasePrice = x.BasePrice,
                CreationTime = x.CreationTime,
                OrderId = x.OrderId,
                PaymentId = x.PaymentId,
                PaymentStatus = x.PaymentStatus,
                OrderStatus = x.OrderStatus,
                Name = x.Name,
                DeliveryPrice = x.DeliveryPrice,
                PaymentMethod = x.SelectedPaymentMethod
            }).ToArray();
        }

        [HttpGet("get-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public FullBasketOrderModel GetBasketOrder([FromQuery] int orderId)
        {
            var order = _adminService.GetBasketOrder(orderId);
            if (order == null)
                throw new ServiceException("order not found", "order_not_found");
            var model = new FullBasketOrderModel
            {
                AbsoluteDiscount = order.AbsoluteDiscount,
                AdditionalPrice = order.AdditionalPrice,
                SelectedPaymentMethod = order.SelectedPaymentMethod,
                DeliveryPrice = order.DeliveryPrice,
                Adress = order.OrderDescription.Adress,
                BasePrice = order.BasePrice,
                City = order.OrderDescription.City,
                CreationTime = order.CreateionTime,
                PromoCode = order.PromoCode,
                Description = order.OrderDescription.Description,
                Email = order.OrderDescription.Email,
                Name = order.OrderDescription.Name,
                OrderId = order.OrderId,
                OrderStatus = order.Status,
                PaymentId = order.PaymentId,
                PaymentLink = order.PaymentLink,
                PaymentStatus = order.PaymentStatus,
                Phone = order.OrderDescription.Phone,
                Surname = order.OrderDescription.Surname,
                PlatformLink = order.PlatformLink,
                Products = order.Products != null ? order.Products.Select(x => new Models.Admin.BasketProductModel
                {
                    Count = x.Count,
                    Name = x.Name,
                    Price = x.Price,
                    BasePrice = x.BasePrice,
                    ProductId = x.Id,
                    Vendor = x.Vendor ?? string.Empty,
                    VendorCode = x.VendorCode ?? string.Empty,
                    VendorLink = x.VendorLink ?? string.Empty
                }).ToArray() : new Models.Admin.BasketProductModel[0]
            };
            if (!string.IsNullOrEmpty(model.PromoCode))
            {
                var code = _promoCodeService.GetPromoCode(model.PromoCode);
                if (code != null)
                {
                    model.PromoCodePercents = code.PercentDiscount;
                    model.PromoCodeAbsolute = code.AbsoluteDiscount;
                }
            }
            return model;
        }

        [HttpPost("create-payment-for-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<string> CreatePaymentForBasketOrder([FromBody] int orderId)
        {
            try
            {
                return await _adminService.AuthorizePaymentForBasketOrder(orderId);
            }
            catch
            {
                return "price_missmatch_error";
            }
        }

        [HttpPost("finish-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool FinishBasketOrder([FromBody] int orderId)
        {
            _adminService.FinishBasketOrder(orderId);
            return true;
        }

        [HttpPost("cancel-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool CancelBasketOrder([FromBody] int orderId)
        {
            _adminService.CloseBasketOrder(orderId);
            return true;
        }

        [HttpPost("add-basket-price")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddAdditionalPriceForBasketOrder([FromBody] SetAdditionalPriceForOrderModel model)
        {
            _adminService.SetAdditionalPriceForBasketOrder(model.OrderId, model.Price);
            return true;
        }

        [HttpPost("add-basket-delivery-price")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddDeliveryPriceForBasketOrder([FromBody] SetAdditionalPriceForOrderModel model)
        {
            _adminService.SetDeliveryPriceForBasketOrder(model.OrderId, model.Price);
            return true;
        }

        [HttpPost("remove-products-from-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool RemoveProductsFromBasketOrder([FromBody] RemoveProductsFromBasketOrderModel model)
        {
            _adminService.RemoveProductFromBasketOrder(model.OrderId, model.ProductId, model.Count);
            return true;
        }

        [HttpPost("add-products-to-basket-order")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool AddProductsToBasketOrder([FromBody] AddProductsToBasketOrderModel model)
        {
            _adminService.AddProductToBasketOrder(model.OrderId, model.ProductId, model.Count);
            return true;
        }

        [HttpPost("refresh-basket-order-price")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool RefreshBasketOrderPrice([FromBody] int orderId)
        {
            _adminService.RefreshPriceForBasketOrder(orderId);
            return true;
        }

        [HttpPost("delete-project")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteProject([FromBody] int projectId)
        {
            _adminService.DeleteProject(projectId);
            return true;
        }

        [HttpPost("delete-user")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteUser([FromBody] int userId)
        {
            _adminService.DeleteUser(userId);
            return true;
        }

        [HttpPost("delete-room")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteRoom([FromBody] int roomId)
        {
            _adminService.DeleteRoom(roomId);
            return true;
        }

        [HttpGet("get-moderation-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ModerationCountsModel GetModerationProduct()
        {
            var model = _adminService.GetModerationProduct();
            return model;
        }

        [HttpGet("get-product-for-moderation")]
        [APIAuthorize(Roles = Role.Authorized)]
        public GetModerationProductModel GetProductForModeration(string type)
        {
            var model = _adminService.GetProductForModeration(type);
            return model;
        }

        [HttpPost("set-moderation-product")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetModerationProduct([FromBody] ModerationProductModel model)
        {
            _adminService.SetModerationProduct(model);
            return true;
        }

        [HttpPost("set-basket-order-status")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetBasketOrderStatus([FromBody] SetOrderStatusModel model)
        {
            _adminService.SetBasketOrderStatus(model.OrderId, model.Status);
            return true;
        }

        [HttpGet("products-stats")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProductsStats GetProductsStats()
        {
            return _adminService.GetProductsStats();
        }

        [HttpPost("change-basket-order-info")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangeBasketOrderInfo([FromBody] ChangeOrderDescriptionModel model)
        {
            var changes = new ChangeBasketOrder
            {
                Name = model.Name,
                Adress = model.Adress,
                City = model.City,
                Description = model.Description,
                Email = model.Email,
                OrderId = model.OrderId,
                Phone = model.Phone,
                Surname = model.Surname
            };
            _adminService.ChangeBasketOrder(changes);
            return true;
        }

        [HttpPost("change-basket-order-promocode")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangeBasketOrderPromocode([FromBody] ChangeBasketOrderPromocodeModel model)
        {
            _adminService.ChangePromoCodeForOrder(model.PromoCode, model.OrderId);
            return true;
        }

        [HttpPost("change-payment-method")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangePaymentMethod([FromBody] ChangePaymentMethodModel model)
        {
            _adminService.ChangePaymentMethod(model.OrderId, model.PaymentMethod);
            return true;
        }


        [HttpGet("get-promo-codes")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PromoCodeExport[] GetPromoCodes()
        {
            var model = _promoCodeService.GetPromoCodes();

            return model;
        }

        [HttpGet("get-promo-code")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PromoCodeExport GetPromoCode(string name)
        {
            var model = _promoCodeService.GetPromoCodeExport(name);

            return model;
        }

        [HttpGet("find-promo-code")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PromoCode FindPromoCode(string code)
        {
            var model = _promoCodeService.GetPromoCode(code);

            return model;
        }

        [HttpPost("create-promo-code")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool CreatePromoCode([FromBody] PromoCodeExport model)
        {
            _promoCodeService.CreatePromoCodeExport(model.Name, model.Count, model.MinPrice, model.PercentsDiscount, model.AbsoluteDisocunt, model.CodeType, model.StartDate, model.ExpireDate);
            return true;
        }

        [HttpGet("get-codes-csv")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PromoCode[] GetPromoCodesCsv(string code)
        {
            var model = _promoCodeService.GetPromoCodesForExport(code);

            return model;
        }

        [HttpGet("get-product-collection-blacklist")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<string> GetProductCollectionBlacklist()
        {
            return _adminService.GetBlacklist();
        }

        [HttpPost("add-word-to-product-collection-blacklist")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<string> AddWordToProductCollectionBlacklist([FromBody] string word)
        {
            _adminService.AddWordToBackList(word);
            return _adminService.GetBlacklist();
        }

        [HttpPost("remove-word-from-product-collection-blacklist")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<string> RemoveWordFromProductCollectionBlacklist([FromBody] string word)
        {
            _adminService.RemoveWordFromBlackList(word);
            return _adminService.GetBlacklist();
        }

        [HttpGet("get-collections-csv-for-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<byte[]> GetCollectionsCSVForCompany(int companyId)
        {
            var csv = await _adminService.GetCollectionsInCSVForCompany(companyId);
            return Encoding.UTF8.GetBytes(csv);
        }

        [HttpGet("get-prdouct-collections-for-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AdminCompanyProductCollectionsModel GetProductCollections(int companyId)
        {
            var collections = _adminService.GetCollectionsInfoForComapny(companyId);
            var collectionModels = collections != null ?
                collections.Select(x => new AdminProductCollectionShortModel
                {
                    Available = x.Available,
                    Id = x.Id,
                    KeyWord = x.KeyWord,
                    Name = x.Name,
                    ProductCount = x.ProductCount
                }).ToList() : new List<AdminProductCollectionShortModel>();
            return new AdminCompanyProductCollectionsModel { CompanyId = companyId, Collections = collectionModels };
        }

        [HttpGet("get-product-collection")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AdminProductCollectionFullModel GetProductCollection(int collectionId)
        {
            var collection = _adminService.GetCollection(collectionId);
            return collection != null ?
                new AdminProductCollectionFullModel
                {
                    Available = collection.Available,
                    Id = collection.Id,
                    KeyWord = collection.KeyWord,
                    Name = collection.Name,
                    Products = collection.Products != null
                                    ? collection.Products
                                        .Select(x => new AdminProductCollectionNameModel { Name = x.Name, ProductId = x.ProductId }).ToList()
                                        : new List<AdminProductCollectionNameModel>()
                } : null;
        }

        [HttpPost("change-product-collection")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ChangeProductCollection([FromBody] AdminProductCollectionShortModel model)
        {
            if (model == null)
                return false;
            _adminService.ChangeCollection(model.Id, model.Name, model.Available);
            return true;
        }

        [HttpPost("create-collections-for-company")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<bool> CreateCollectionsForCompany([FromBody] int companyId)
        {
            await _adminService.ManuallyCreateCollectionsForCompany(companyId);
            return true;
        }
    }
}
