﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Sale;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/sales")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APISaleController : APIController
    {
        private readonly ISaleService _saleService = null;

        public APISaleController(ISaleService saleService)
        {
            _saleService = saleService;
        }

        [HttpPost("create-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public SaleModel CreateSale([FromBody] CreateSaleModel model)
        {
            return _saleService.CreateSale(model.Name).ToModel();
        }

        [HttpPost("update-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public SaleModel UpdateSale([FromBody] SaleModel model)
        {
            _saleService.UpdateSale(new Sale
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                IsActive = model.IsActive,
                Percent = model.Percents,
                AllMarketplace = model.AllMarketplace,
                Categories = model.Categories,
                Companies = model.Companies,
                Products = model.Products
            });
            return _saleService.GetSale(model.Id).ToModel();
        }

        [HttpPost("delete-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteSale([FromBody] DeleteSaleModel model)
        {
            _saleService.DeleteSale(model.SaleId);
            return true;
        }

        [HttpPost("activate-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ActivateBadge([FromBody] ActivateSaleModel model)
        {
            _saleService.SetSaleActive(model.SaleId, true);
            return true;
        }

        [HttpPost("deactivate-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeactivateBadge([FromBody] ActivateSaleModel model)
        {
            _saleService.SetSaleActive(model.SaleId, false);
            return true;
        }

        [HttpGet("get-sale")]
        [APIAuthorize(Roles = Role.Authorized)]
        public SaleModel GetSale([FromQuery] int saleId)
        {
            return _saleService.GetSale(saleId).ToModel();
        }

        [HttpGet("get-all-sales")]
        [APIAuthorize(Roles = Role.Authorized)]
        public SaleShortModel[] GetAllSales()
        {
            return _saleService.GetSales().Select(x => x.ToModel()).ToArray();
        }
    }
}
