﻿using System.Linq;
using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/banner")]
    public class AdminBannerController : APIController
    {
        private readonly IDBBannerService _dbService;

        public AdminBannerController(IDBBannerService dbService)
        {
            _dbService = dbService;
        }


        [HttpGet("get-banners-list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BannerListModel GetBannersList()
        {
            var banners = _dbService.GetBanners();
            var bannerList = banners != null
                ? banners.Select(info => new BannerShortModel
                    {
                        Id = info.Id,
                        Name = info.Name,
                        Active = info.Active,
                        Order = info.Order,
                        Updated = info.Upated
                    })
                    .OrderByDescending(model => model.Active)
                    .ThenByDescending(model => model.Order)
                    .ThenBy(model => model.Updated)
                    .ToArray()
                : new BannerShortModel[0];
            return new BannerListModel { Banners = bannerList };
        }

        [HttpPost("create-banner")]
        [APIAuthorize(Roles = Role.Authorized)]
        public int CreateBanner([FromBody] BannerModel model)
        {
            return _dbService.CreateBanner(model);
        }

        [HttpGet("get-banner")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BannerModel GetBanner([FromQuery] int id)
        {
            return _dbService.GetBanner(id);
        }

        [HttpPost("update-banner")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool UpdateBanner([FromBody] BannerModel model)
        {
            return _dbService.UpdateBanner(model);
        }

        [HttpGet("toggle-banner-state")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void ToggleBannerState([FromQuery] int id)
        {
            _dbService.ToggleBannerState(id);
        }

        [HttpGet("delete-banner")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteBanner([FromQuery] int id)
        {
            _dbService.DeleteBanner(id);
        }
    }
}