﻿using Faradise.Design.Internal;
using Faradise.Design.Models.Payment.JSON;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers
{
    [Route("api/payment")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class PaymentController : APIController
    {
        [HttpPost("yandex-kassa-return-url")]
        public IActionResult YandexKassaNotification([FromBody] YandexKassaPaymentStatusNotification notification)
        {
            if (notification == null)
                return StatusCode(400);
            return StatusCode(200);
        }

        [HttpPost("yandex-kassa-return-url-test")]
        public IActionResult YandexKassaNotificationTest([FromBody] YandexKassaPaymentStatusNotification notification)
        {
            if (notification == null)
                return StatusCode(400);
            return StatusCode(200);
        }
    }
}
