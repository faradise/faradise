﻿using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Tilda;
using Faradise.Design.Controllers.Models;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.Tilda;
using Faradise.Design.Services;
using Faradise.Design.Services.Implementation.MailSending;
using Faradise.Design.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("tilda")]
    public class TildaWebhookController : APIController
    {
        private readonly IDBTildaService _dbTilda = null;
        private readonly IMailSendingService _mailer = null;
        private readonly ISMSService _smsService = null;

        public TildaWebhookController(IDBTildaService dbTilda, IMailSendingService mailer, ISMSService sms)
        {
            _dbTilda = dbTilda;
            _mailer = mailer;
            _smsService = sms;
        }

        [HttpGet("get-lottery-requests")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public async Task<TildaLotteryRequest[]> ExtractLotteryRequests()
        {
            return await _dbTilda.ExtractRequests(TildaMailType.Lottery);
        }

        [HttpPost("lottery-landing")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public async Task<IActionResult> AcceptLotteryForm([FromForm] TildaLotteryForm form)
        {
            if (string.IsNullOrEmpty(form.Name) || string.IsNullOrEmpty(form.Phone))
                return Ok();

            var phone = new string(form.Phone.Where(x => char.IsDigit(x) || x == '+').ToArray());
            var mail = form.Email.Trim().ToLower();

            var valid = await _dbTilda.CheckRequest(phone, mail, TildaMailType.Lottery);
            if (!valid)
                return Ok();

            var id = await _dbTilda.StoreRequest(form.Name, mail, phone, TildaMailType.Lottery);

            _mailer.SendEmail<LotteryTemplate>(new EmailModel
            {
                TemplateEngine = "velocity",
                TemplateModel = new LotteryTemplate { loteryid = id.ToString() },
                TemplateName = _mailer.LotteryTemplate,
                ToAddress = new[] { mail }
            });

            _smsService.TrySendSMS(
                phone.Replace("+7", "8"),
                $"Поздравляем! Ваш номер для участия в розыгрыше {id}. Ждите результатов 20 декабря. www.faradise.ru");

            return Ok();
        }
    }
}