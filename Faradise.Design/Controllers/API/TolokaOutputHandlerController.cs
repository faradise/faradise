﻿using System.IO;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Internal;
using Faradise.Design.Models.Toloka;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("toloka/accept")]
    public class TolokaOutputHandlerController : APIController
    {
        private readonly ITolokaAcceptorService _acceptor = null;

        public TolokaOutputHandlerController(ITolokaAcceptorService acceptor)
        {
            _acceptor = acceptor;
        }

        //[HttpPost("by-type")]
        //[DisableRequestSizeLimit]
        //public async Task<bool> AcceptTSVOutput([FromForm] UploadTSVModel model)
        //{
        //    string text;

        //    using (var stream = model.File.OpenReadStream())
        //    using (var sr = new StreamReader(stream))
        //        text = sr.ReadToEnd();

        //    if (string.IsNullOrEmpty(text))
        //        return false;

        //    var tolokaOutput = TolokaOutput.Parse(text);

        //    return await _acceptor.Accept(tolokaOutput, model.Type);
        //}
    }

    public class UploadTSVModel
    {
        public IFormFile File { get; set; }
        public TolokaOutputType Type { get; set; }
    }
}
