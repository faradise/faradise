﻿using Faradise.Design.Controllers.API.Models.User;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/user/")]
    public class UserController : APIController
    {
        private IUserService _userService = null;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Возвращает базовую информацию о пользователе
        /// </summary>
        /// <returns></returns>
        [HttpPost("authorize")]
        [APIAuthorize(Roles = Role.Authorized)]
        public AuthorizeModel Authorize()
        {
            var userId = ((User)HttpContext.Items[typeof(User)]).Id;
            var user = _userService.ExtractUserById(userId);

            string role = string.Empty;
            if (user.Role.ContainsRole(Role.Designer))
                role = Role.Designer.ToString();
            else if (user.Role.ContainsRole(Role.Client))
                role = Role.Client.ToString();
            else
                role = "Unassigned";

            var model = new AuthorizeModel()
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Phone = user.Phone,
                Role = role
            };

            return model;
        }

        /// <summary>
        /// Устанавливает email для текущего пользователя
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("setEmail")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetEmail([FromBody] EmailModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];

            _userService.AddEmail(user.Id, model.Email);
        }

        /// <summary>
        /// Устанавливает номер телефона для текущего пользователя
        /// </summary>
        [HttpPost("setPhone")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetPhone([FromBody] PhoneModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];

            _userService.AddUserPhone(user.Id, model.Phone);
        }

        /// <summary>
        /// Устанавливает email для текущего пользователя
        /// </summary>
        [HttpPost("setName")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void SetName([FromBody] NameModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];

            _userService.AddUserName(user.Id, model.Name);
        }
    }
}