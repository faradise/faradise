﻿using Faradise.Design.Controllers.API.Models.Chat;
using Faradise.Design.Controllers.API.Models.Messenger;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Faradise.Design.Services.Implementation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/messenger")]
    public class MessengerController : APIController
    {
        private ICDNService _cdnService = null;
        private IProjectDefinitionService _projectDefinitionService = null;
        private IProjectChatService _chatService = null;

        public MessengerController(ICDNService cdnService, IProjectDefinitionService projectDefinitionService, IProjectChatService chatService)
        {
            _cdnService = cdnService;
            _projectDefinitionService = projectDefinitionService;
            _chatService = chatService;
        }

        /// <summary>
        /// Загрузить фото и вернуть адрес фото
        /// </summary>
        /// <param name="model">Форма с фотографией</param>
        [HttpPost("send-photo")]
        [APIAuthorize(Roles = Role.Authorized)]
        public async Task<string> SendPhoto([FromForm] MessengerPhotoModel model)
        {
            var id = ((User)HttpContext.Items[typeof(User)]).Id;
            var path = await _cdnService.UploadData(model.Photo, _cdnService.GetPathForMessengerPhoto(model.GroupName));
            return path;
        }

        [HttpPost("confirm-user-on-group")]
        public bool ConfirmUserOnGroup(string groupId, string userId)
        {
            var project = _projectDefinitionService.GetProjectDefinition(int.Parse(groupId));
            if (project.ClientId == int.Parse(userId) || project.DesignerId == int.Parse(userId) || int.Parse(userId) < 0)
                return true;
            else
                return false;

            //return true;
        }
        
        [HttpPost("notify-about-message")]
        [APIAuthorize(Roles = Design.Models.Role.Unauthorized)]
        public void NotifyAboutMessage([FromBody] NotifyGroupModel model)
        {
            _chatService.NotifyAboutNewMessage(model.ProjectId, model.Users, model.IsFirstMessage);
        }

        /// <summary>
        /// Активировать оповещения из чата по СМС
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("activate-chat-notification")]
        [APIAuthorize(Roles = Design.Models.Role.Authorized)]
        public void EnableNotifyAboutMessage([FromBody] SetChatNotificationModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_projectDefinitionService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (user.Role.ContainsRole(Role.Client))
                _chatService.SetClientNotificationIsActive(model.ProjectId, true);
            else if (user.Role.ContainsRole(Role.Designer))
                _chatService.SetDesignerNotificationIsActive(model.ProjectId, true);
        }

        /// <summary>
        /// Деактивировать оповещение из чата по СМС
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("disable-chat-notification")]
        [APIAuthorize(Roles = Design.Models.Role.Authorized)]
        public void DisableNotificationAboutMessage([FromBody] SetChatNotificationModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_projectDefinitionService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (user.Role.ContainsRole(Role.Client))
                _chatService.SetClientNotificationIsActive(model.ProjectId, false);
            else if (user.Role.ContainsRole(Role.Designer))
                _chatService.SetDesignerNotificationIsActive(model.ProjectId, false);
        }

        /// <summary>
        /// Надо ли оповещать юзера из чата по СМС
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("get-chat-notification-state")]
        [APIAuthorize(Roles = Design.Models.Role.Authorized)]
        public bool GetNotificationAboutMessage(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_projectDefinitionService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            if (user.Role.ContainsRole(Role.Client))
                return _chatService.CheckClientNotifyISActive(projectId);
            else if (user.Role.ContainsRole(Role.Designer))
                return _chatService.CheckDesignerNotifyISActive(projectId);
            return false;
        }
    }
}
