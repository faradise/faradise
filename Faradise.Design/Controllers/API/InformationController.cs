﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Faradise.Design.Filters;
using Faradise.Design.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Produces("application/json")]
    [Route("api/Information")]
    public class InformationController : Controller
    {
        /// <summary>
        /// Получить 
        /// </summary>
        /// <returns></returns>
        [HttpGet("getEnumByName")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<string> GetEnum(string enumName)
        {
            var type = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsEnum && x.Name == enumName).FirstOrDefault();
            var result = new List<string>();
            Array typeValues = Enum.GetValues(type);
            foreach(var item in typeValues)
            {
                result.Add(item.ToString());
            }
            return result;
        }
    }
}