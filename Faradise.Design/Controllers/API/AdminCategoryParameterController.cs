﻿using System;
using System.Linq;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.CategoryParameters;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/categories")]
    public class AdminCategoryParameterController : APIController
    {
        private readonly IDBCategoryParameterService _dbService;
        private readonly ICDNService _cdnService;

        public AdminCategoryParameterController(IDBCategoryParameterService dbService, ICDNService cdnService)
        {
            _dbService = dbService;
            _cdnService = cdnService;
        }

        [HttpGet("list")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ParameterInfo[] CategoriesList()
        {
            return _dbService.GetAllParameters();
        }

        [HttpGet("category-parameters")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ParameterInfo[] ParametersByCategoryId([FromQuery] int id)
        {
            return _dbService.GetParametersByCategoryId(id);
        }


        [HttpPost("create-parameter")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void CreateParameter([FromBody] ParameterInfo model)
        {
            if (model.Synonyms[0] != null && model.Synonyms[0].Contains(","))
                model.Synonyms =
                    model.Synonyms[0].Split(',', StringSplitOptions.RemoveEmptyEntries)
                        .Select(s => s.Trim())
                        .ToList();
            _dbService.CreateParameter(model);
        }

        [HttpGet("delete-parameter")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void DeleteParameter([FromQuery] int id)
        {
            _dbService.DeleteParameter(id);
        }

        [HttpGet("actual-report")]
        public byte[] GetActualReportBytes()
        {
            var path = _cdnService.GetPathForFeedAnalyzerExcelFiles();
            var bytes = _cdnService.DownloadBytes(path + "/actual_feed_report.xlsx").Result;
            return bytes;
        }

        [HttpGet("get-parameter")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ParameterInfo GetParameter([FromQuery] int id)
        {
            return _dbService.GetParameter(id);
        }


        [HttpPost("update-parameter")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void UpdateBanner([FromBody] ParameterInfo model)
        {
            _dbService.UpdateParameter(model);
        }
    }
}