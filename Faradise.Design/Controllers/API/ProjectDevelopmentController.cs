﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models;
using Faradise.Design.Controllers.API.Models.ProjectDevelopment;
using Faradise.Design.Controllers.API.Models.Shared;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/project/development")]
    public class ProjectDevelopmentController : APIController
    {
        private readonly IProjectDevelopmentService _devService = null;
        private readonly ICDNService _cdnService = null;
        private readonly IProjectDefinitionService _defService = null;
        private readonly IArchiveService _archiveUpdateService = null;
        private readonly IPaymentService _paymentService = null;

        public ProjectDevelopmentController(IProjectDevelopmentService devService, ICDNService cdnService, IProjectDefinitionService defService, IArchiveService archiveUpdateService, IPaymentService paymentService)
        {
            _devService = devService;
            _cdnService = cdnService;
            _defService = defService;
            _paymentService = paymentService;
            _archiveUpdateService = archiveUpdateService;
        }

        /// <summary>
        /// Отказать от дизайнера 
        /// </summary>
        [HttpPost("abandon-designer")]
        [APIAuthorize(Roles = Role.Client)]
        public void AbandonDesigner([FromBody] AbandonDesignerModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _defService.AbandonDesigner(model.ProjectId, model.Description);
        }

        /// <summary>
        /// Получить краткие данные о комнатах. id и назначение.
        /// </summary>
        /// <param name="projectId"></param>
        [HttpGet("get-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public RoomPurposeModel[] GetRooms(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetRoomsShortInfos(projectId);
            return rooms != null ? rooms.Select(x => new RoomPurposeModel() { Purpose = x.Purpose, RoomId = x.RoomId }).ToArray() : new RoomPurposeModel[0];
        }


        /// <summary>
        /// Получить актуальную стадию проекта и информацию о ней
        /// </summary>
        /// <param name="projectId"></param>
        [HttpGet("get-current-stage")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProjectStageModel GetCurrentStage(int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var status = _devService.GetCurrentStageStatus(projectId);
            return status != null ? status.ToModel() : new ProjectStageModel();
        }

        /// <summary>
        /// Получить информацию о конкретной стадии проекта
        /// </summary>
        /// <param name="model"></param>
        [HttpGet("get-stage")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ProjectStageModel GetStage(GetDevelopStageModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var status = _devService.GetStatus(model.ProjectId, model.Stage);
            return status != null ? status.ToModel() : new ProjectStageModel();
        }

        /// <summary>
        /// Отметить стадию проекта как принятую ползьзователем
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("confirm-stage")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool SetUserStageConfirm([FromBody] ConfirmedDevelopStageModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            return _devService.SetStageStatus(model.ProjectId, model.Stage, user, true);
        }

        /// <summary>
        /// Пропустить стадию проекта (для пропуска неоплаченных этапов)
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("skip-stage")]
        [APIAuthorize(Roles = Role.Client)]
        public bool SkipStage([FromBody] ConfirmedDevelopStageModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            return _devService.SkipStage(model.ProjectId, user, model.Stage);
        }

        /// <summary>
        /// Загрузить фото с мудбордом для комнаты
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("upload-moodboard-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> UploadMoodboardPhoto([FromForm] UploadRoomFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForMoodboardPhoto(model.ProjectId));
            var fileId = _devService.AddMoodboardPhoto(model.ProjectId, model.RoomId, url);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Moodboard);
            return fileId;
        }

        /// <summary>
        /// Удалить фото мудборда комнаты
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("delete-moodboard-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public void DeleteMoodboardPhoto([FromBody] DeleteFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetMoodBoards(model.RoomId);
            if (rooms == null)
                throw new ServiceException($"Moodboard not found for room id {model.RoomId}", "moodboard_not_found");
            var photo = rooms.Photos != null ? rooms.Photos.FirstOrDefault(x => x.FileId == model.FileId) : null;
            if (photo == null)
                throw new ServiceException($"Moodboard photo not found for room id {model.RoomId}, photo id {photo.FileId}", "photo_not_found");
            _devService.DeleteMoodboardPhoto(model.ProjectId, model.FileId);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Moodboard);
        }

        /// <summary>
        /// Получить сслыку на скачку архива мудбордов
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-archive-moodboards")]
        [APIAuthorize(Roles = Role.Authorized)]
        public string GetArchiveMoodboards(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var moodBoards = _devService.GetMoodBoards(roomId);
            return moodBoards != null ? moodBoards.ArchiveUrl : string.Empty;
        }

        /// <summary>
        /// Получить мудборды для комнаты
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-moodboards")]
        [APIAuthorize(Roles = Role.Authorized)]
        public MoodboardModel GetMoodboards(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var moodboards = _devService.GetMoodBoards(roomId);
            return moodboards != null ? moodboards.ToModel() : null;
        }

        /// <summary>
        /// Получить коллажи для комнаты
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-collages")]
        [APIAuthorize(Roles = Role.Authorized)]
        public CollageModel GetCollages(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var collages = _devService.GetCollages(roomId);
            return collages != null ? collages.ToModel() : null;
        }

        /// <summary>
        /// Загрузить фото коллаж для комнаты
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("upload-collage-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> UploadCollagePhoto([FromForm] UploadRoomFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForCollagePhoto(model.ProjectId));
            var fileId = _devService.AddCollagePhoto(model.ProjectId, model.RoomId, url);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Collages);
            return fileId;
        }

        /// <summary>
        /// Получить сслыку на скачку архива коллажей
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-archive-collages")]
        [APIAuthorize(Roles = Role.Authorized)]
        public string GetArchiveCollages(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var collages = _devService.GetCollages(roomId);
            return collages != null ? collages.ArchiveUrl : string.Empty;
        }

        /// <summary>
        /// Удалить фото коллаж
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("delete-collage-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public void DeleteCollagePhoto([FromBody] DeleteFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetCollages(model.RoomId);
            if (rooms == null)
                throw new ServiceException($"Collage not found for room id {model.RoomId}", "collage_not_found");
            var photo = rooms.Photos != null ? rooms.Photos.FirstOrDefault(x => x.FileId == model.FileId) : null;
            if (photo == null)
                throw new ServiceException($"Collage photo not found for room id {model.RoomId}, photo id {photo.FileId}", "photo_not_found");
            _devService.DeleteCollagePhoto(model.ProjectId, model.FileId);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Collages);
        }

        /// <summary>
        /// Получить зонирование для комнаты
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-zones")]
        [APIAuthorize(Roles = Role.Authorized)]
        public ZoneModel GetZones(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var zones = _devService.GetZones(roomId);
            return zones != null ? zones.ToModel() : null;
        }

        /// <summary>
        /// Загрузить зонирование для комнаты(фото)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("upload-zone-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> UploadZonePhoto([FromForm] UploadRoomFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForZonePhoto(model.ProjectId));
            var fileId = _devService.AddZonePhoto(model.ProjectId, model.RoomId, url);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Zones);
            return fileId;
        }

        /// <summary>
        /// Получить сслыку на скачку архива зонирования
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-archive-zones")]
        [APIAuthorize(Roles = Role.Authorized)]
        public string GetArchiveZones(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var zones = _devService.GetZones(roomId);
            return zones != null ? zones.ArchiveUrl : string.Empty;
        }

        /// <summary>
        /// Удалить фото зонирования
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("delete-zone-photo")]
        [APIAuthorize(Roles = Role.Designer)]
        public void DeleteZonePhoto([FromBody] DeleteFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetZones(model.RoomId);
            if (rooms == null)
                throw new ServiceException($"Zones not found for room id {model.RoomId}", "zone_not_found");
            var photo = rooms.Photos != null ? rooms.Photos.FirstOrDefault(x => x.FileId == model.FileId) : null;
            if (photo == null)
                throw new ServiceException($"Zone photo not found for room id {model.RoomId}, photo id {photo.FileId}", "photo_not_found");
            _devService.DeleteZonePhoto(model.ProjectId, model.FileId);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Zones);
        }

        /// <summary>
        /// Получить планы комнаты
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet("get-plans")]
        [APIAuthorize(Roles = Role.Authorized)]
        public PlanModel GetPlans(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var plans = _devService.GetPlans(roomId);
            return plans != null ? plans.ToModel() : null;
        }

        /// <summary>
        /// Загрузить план комнаты
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("upload-plan-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public async Task<int> UploadPlanFile([FromForm] UploadRoomFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var url = await _cdnService.UploadData(model.File, _cdnService.GetPathForWorkPlanFiles(model.ProjectId));
            var fileId = _devService.AddPlanFile(model.ProjectId, model.RoomId, url);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Plans);
            return fileId;
        }

        /// <summary>
        /// Получить сслыку на скачку архива планов
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-archive-plans")]
        [APIAuthorize(Roles = Role.Authorized)]
        public string GetArchivePlans(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var plans = _devService.GetPlans(roomId);
            return plans != null ? plans.ArchiveUrl : string.Empty;
        }

        /// <summary>
        /// Удалить план комнаты
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("delete-plan-file")]
        [APIAuthorize(Roles = Role.Designer)]
        public void DeletePlanFile([FromBody] DeleteFileModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetPlans(model.RoomId);
            if (rooms == null)
                throw new ServiceException($"Plan not found for room id {model.RoomId}", "plan_not_found");
            var photo = rooms.Files != null ? rooms.Files.FirstOrDefault(x => x.FileId == model.FileId) : null;
            if (photo == null)
                throw new ServiceException($"Plan file not found for room id {model.RoomId}, file id {photo.FileId}", "file_not_found");
            _devService.DeletePlanFile(model.ProjectId, model.FileId);
            _archiveUpdateService.AddUpdateTask(model.ProjectId, model.RoomId, RoomArchiveType.Plans);
        }

        /// <summary>
        /// Получить всю визуализацию комнаты
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet("get-visulization")]
        [APIAuthorize(Roles = Role.Authorized)]
        public VisualizationModel GetVisualization(int projectId, int roomId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var visualizations = _devService.GetVisualizations(roomId);
            return visualizations != null ? visualizations.ToModel() : null;
        }

        /// <summary>
        /// Позвать админа в проект
        /// </summary>
        /// <param name="model"></param>
        [HttpPost("add-admin")]
        [APIAuthorize(Roles = Role.Authorized)]
        public void AddAdmin([FromBody] AddAdminToProjectModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _defService.SetProjectNeedAdminAttention(model.ProjectId, true);
            //TODO: добавить админа в чат?
        }

        [HttpPost("finish-room-plan")]
        [APIAuthorize(Roles = Role.Designer)]
        public void FinishRoomPlan([FromBody] FinishPlanModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _devService.SetPlanAsComplete(model.RoomId);
        }

        /// <summary>
        /// Заказать визуализацию для комнат и получить ссылку на оплату
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("order-room-visalization")]
        [APIAuthorize(Roles = Role.Client)]
        public async Task<string> OrderRoomVisualizationsAsync([FromBody] OrderVisualizationModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var stage = _devService.GetCurrentStageStatus(model.ProjectId);
            if (stage.Stage != DevelopmentStage.Visualization)
                throw new ServiceException($"Wrong stage", "wrong_stage");
            if (model.RoomsId == null || model.RoomsId.Length < 1)
                throw new ServiceException($"No rooms selected", "no_rooms_visualization");
            if (string.IsNullOrEmpty(user.Email))
                throw new ServiceException($"User has no email", "no_user_email");
            var price = _devService.GetVisualizationPriceForRooms(model.RoomsId);
            if (price.SumAmount == 0)
                throw new ServiceException($"selected visualization price is 0", "visualization_price_error");
            var roomIdsString = string.Empty;
            foreach (var r in model.RoomsId)
                roomIdsString += r.ToString() + " ";
            //TODO : определить return_url
            var reciept = KassaReceiptInfo.CreateForVisualization(price, user.Email, user.Phone);
            var payment = await _paymentService.CreateKassaPayment(Design.Models.Enums.PaymentTargetType.VisualizationFreelance, price.SumAmount, $"Visualization for rooms {roomIdsString}, project {model.ProjectId}", reciept, string.Empty);
            payment = await _paymentService.UpdateKassaPaymentRoute(payment.Id);
            if (payment == null)
                throw new ServiceException($"Payment error", "payment_error");
            _devService.SelectRoomVisualization(model.ProjectId, model.RoomsId, payment.Id);
            _devService.SetClientMadeDecision(model.ProjectId, DevelopmentStage.Visualization);
            return payment.PaymentRoute;
        }

        [HttpGet("get-ordered-visulization-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<int> GetOrderedRoomForVisualization([FromQuery] int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetRoomsShortInfos(projectId);
            var orderdRooms = new List<int>();
            foreach (var room in rooms)
            {
                var visualization = _devService.GetVisualizations(room.RoomId);
                if (visualization != null && visualization.IsSelected)
                {
                    orderdRooms.Add(room.RoomId);
                }
            }
            return orderdRooms;
        }

        [HttpPost("order-room-plans")]
        [APIAuthorize(Roles = Role.Client)]
        public async Task<string> OrderRoomPlans([FromBody] OrderRoomPlans model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var stage = _devService.GetCurrentStageStatus(model.ProjectId);
            if (stage.Stage != DevelopmentStage.Plan)
                throw new ServiceException($"Wrong stage", "wrong_stage");
            if (model.RoomsId == null || model.RoomsId.Length < 1)
                throw new ServiceException($"No rooms selected", "no_room_plans");
            if (string.IsNullOrEmpty(user.Email))
                throw new ServiceException($"User has no email", "no_user_email");
            var price = _devService.GetPlansPriceForRooms(model.RoomsId);
            if (price.SumAmount == 0)
                throw new ServiceException($"selected plan price is 0", "plan_price_error");
            var roomIdsString = string.Empty;
            foreach (var r in model.RoomsId)
                roomIdsString += r.ToString() + " ";
            //TODO : определить return_url
            var reciept = KassaReceiptInfo.CreateForPlan(price, user.Email, user.Phone);
            var payment = await _paymentService.CreateKassaPayment(Design.Models.Enums.PaymentTargetType.WorkPlans, price.SumAmount, $"Plans for rooms {roomIdsString}, project {model.ProjectId}", reciept, string.Empty);
            payment = await _paymentService.UpdateKassaPaymentRoute(payment.Id);
            if (payment == null)
                throw new ServiceException($"Payment error", "payment_error");
            _devService.SelectRoomPlans(model.ProjectId, model.RoomsId, payment.Id);
            _devService.SetClientMadeDecision(model.ProjectId, DevelopmentStage.Plan);
            return payment.PaymentRoute;
        }

        [HttpGet("get-ordered-workplans-rooms")]
        [APIAuthorize(Roles = Role.Authorized)]
        public List<int> GetOrderedRoomForWorkPlans([FromQuery] int projectId)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(projectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            var rooms = _devService.GetRoomsShortInfos(projectId);
            var orderdRooms = new List<int>();
            foreach (var room in rooms)
            {
                var plans = _devService.GetPlans(room.RoomId);
                if (plans != null && plans.IsSelected)
                {
                    orderdRooms.Add(room.RoomId);
                }
            }
            return orderdRooms;
        }

        [HttpPost("order-brigade")]
        [APIAuthorize(Roles = Role.Client)]
        public void OrderBriagde([FromBody] OrderBrigadeModel model)
        {
            var user = (User)HttpContext.Items[typeof(User)];
            if (!_defService.CheckUserOwnsService(model.ProjectId, user.Id))
                throw new ServiceException($"Client permission denied", "project_not_own_by_client");
            _devService.SetClientMadeDecision(model.ProjectId, DevelopmentStage.Brigade);
        }
    }
}