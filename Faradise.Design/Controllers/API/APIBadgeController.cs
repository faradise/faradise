﻿using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Badge;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/badges")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APIBadgeController : APIController
    {
        private readonly IBadgeService _badgeService;

        public APIBadgeController(IBadgeService badgeService)
        {
            _badgeService = badgeService;
        }

        [HttpPost("create-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BadgeModel CreateBadge([FromBody] CreateBadgeModel model)
        {
            return _badgeService.CreateNewBadge(model.Name).ToModel();
        }

        [HttpPost("update-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BadgeModel UpdateBadge([FromBody] BadgeModel model)
        {
            _badgeService.UpdateBadge(new Badge
            {
                Id = model.Id,
                Name = model.Name,
                Picture = model.Picture,
                IsActive = model.IsActive,
                Description = model.Description,
                AllMarketplace = model.AllMarketplace,
                Categories = model.Categories,
                Companies = model.Companies,
                Products = model.Products
            });
            return _badgeService.GetBadge(model.Id).ToModel();
        }

        [HttpPost("delete-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeleteBadge([FromBody] DeleteBadgeModel model)
        {
            _badgeService.DeleteBadge(model.BadgeId);
            return true;
        }

        [HttpPost("activate-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool ActivateBadge([FromBody] ActivateBadgeModel model)
        {
            _badgeService.SetBadgeActive(model.BadgeId, true);
            return true;
        }

        [HttpPost("deactivate-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public bool DeactivateBadge([FromBody] DeactivateBadgeModel model)
        {
            _badgeService.SetBadgeActive(model.BadgeId, false);
            return true;
        }

        [HttpGet("get-badge")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BadgeModel GetBadge([FromQuery] int badgeId)
        {
            return _badgeService.GetBadge(badgeId).ToModel();
        }

        [HttpGet("get-all-badges")]
        [APIAuthorize(Roles = Role.Authorized)]
        public BadgeShortModel[] GetAllBadges()
        {
            return _badgeService.GetBadges().Select(x => x.ToModel()).ToArray();
        }
    }
}
