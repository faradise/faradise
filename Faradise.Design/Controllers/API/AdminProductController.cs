﻿using System.Threading.Tasks;
using Faradise.Design.Filters;
using Faradise.Design.Internal;
using Faradise.Design.Models;
using Faradise.Design.Models.Admin.Product;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/product")]
    public class AdminProductController : APIController
    {
        private readonly IDBAdminMarketplaceService _dbMarketplace = null;

        public AdminProductController(IDBAdminMarketplaceService dbMarketplace)
        {
            _dbMarketplace = dbMarketplace;
        }

        [HttpGet("images-priority")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public async Task<ProductImages> GetImagesPriority([FromQuery] int productId)
        {
            return await _dbMarketplace.GetProductImages(productId);
        }

        [HttpPost("set-images-priority")]
        [APIAuthorize(Roles = Role.Unauthorized)]
        public async Task<bool> SetImagesPriority([FromBody] ProductImages model)
        {
            await _dbMarketplace.SetProductImagesPriority(model);

            return true;
        }
    }
}