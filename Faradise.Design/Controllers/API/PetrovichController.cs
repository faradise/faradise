﻿using Faradise.Design.Internal;
using Faradise.Design.Models.Petrovich;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace Faradise.Design.Controllers.API
{
    [Route("api/admin/petrovich")]
    public class PetrovichController : APIController
    {
        private IDBMarketplaceService _dbService = null;

        public PetrovichController(IDBMarketplaceService dbService)
        {
            _dbService = dbService;
        }


        [HttpPost("set-petrovich-categories")]
        public bool SetPetrovichCategories([FromForm] PetrovichFileModel model)
        {
            string text;

            using (var stream = model.File.OpenReadStream())
            using (var sr = new StreamReader(stream))
                text = sr.ReadToEnd();

            if (string.IsNullOrEmpty(text))
                return false;

            var petrovichListModel = PetrovichListModel.Parse(text);

            _dbService.MapPetrovichCategories(petrovichListModel, model.PetrovichId);
            return true;
        }

    }

    public class PetrovichFileModel
    {
        public int PetrovichId { get; set; }
        public IFormFile File { get; set; }
    }
}
