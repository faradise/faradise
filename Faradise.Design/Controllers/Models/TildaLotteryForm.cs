﻿namespace Faradise.Design.Controllers.Models
{
    public class TildaLotteryForm
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
