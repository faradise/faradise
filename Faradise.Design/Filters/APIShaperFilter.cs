﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace Faradise.Design.Filters
{
    public class APIShaperFilter : Attribute, IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var handle = await next();

            if (handle.Exception == null)
                HandleResult(handle);
        }

        private void HandleResult(ActionExecutedContext handle)
        {
            object val = true;

            if (handle.Result is ObjectResult result)
                val = result.Value;

            object res = null;

            if (val != null)
                res = new { code = "ok", result = val };
            else res = new { code = "ok" };

            handle.Result = new JsonResult(res);
        }
    }
}