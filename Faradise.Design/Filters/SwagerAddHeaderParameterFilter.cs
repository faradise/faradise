﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Models;

namespace Faradise.Design.Filters
{
    public class AddAuthTokenHeaderParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            var actionAuthAtribute = context.MethodInfo.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(APIAuthorize));
            Role actionRole = actionAuthAtribute != null ? (Role)actionAuthAtribute.NamedArguments[0].TypedValue.Value : Role.Unauthorized;
            if (actionRole != Role.Unauthorized)
            {
                string atrArgs = "Action Role = " + actionRole;
                operation.Parameters.Add(new HeaderParameter()
                {
                    Description = "Auth token for " + atrArgs,
                    Name = "token",
                    In = "header",
                    Type = "string",
                    Required = true
                });
            }
        }
    }

    class HeaderParameter : NonBodyParameter
    {
    }
}
