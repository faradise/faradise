﻿using Faradise.Design.Internal.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;

namespace Faradise.Design.Filters
{
    public class APIModelValidationFilter : Attribute, IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage)).ToArray();
                var path = context.HttpContext.Request.Path;

                throw new ModelValidationException(path, errors);
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        { }
    }
}
