﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using Faradise.Design.Models.Cache;

namespace Faradise.Design.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public class APIAuthorize : Attribute, IActionFilter
    {
        public Role Roles { get; set; }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Items.ContainsKey("authorized"))
                return;

            context.HttpContext.Items.Add("authorized", true);
            var filters = context.Filters.OfType<APIAuthorize>().Aggregate(Role.Empty, (x, y) => x |= y.Roles);

            if ((filters | Role.Unauthorized) == filters)
                return;

            var cache = (ICacheService) context.HttpContext.RequestServices.GetService(typeof(ICacheService));
            var userService = (IUserService)context.HttpContext.RequestServices.GetService(typeof(IUserService));
            var jwtService = (ITokenService)context.HttpContext.RequestServices.GetService(typeof(ITokenService));

            var token = context.HttpContext.Request.Headers["token"];
            var validatedToken = jwtService.GetClaims(token);
            if (validatedToken == null)
                return;

            var idString = validatedToken.FindFirst("id");
            var id = int.Parse(idString.Value);

            if (id <= 0)
                return;

            User user;

            var request = new CacheRequest("user", id);
            var cachedUser = cache.Extract<User>(request);
            if (cachedUser.Succeeded)
                user = cachedUser.Value;
            else
            {
                user = userService.ExtractUserById(id);
                if (user == null)
                    throw new AuthorizationException();
                cache.Put(request, user);
            }

            if (user == null)
                throw new AuthorizationException();

            if ((user.Role & filters) > 0)
                context.HttpContext.Items.Add(typeof(User), user);
            else throw new AuthorizationException(filters, user.Role);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        { }
    }
}
