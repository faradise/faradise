﻿using System;

namespace Faradise.Design.Internal.Exceptions
{
    public class ServiceException : Exception
    {
        public string Reason { get; private set; }

        public ServiceException(string message, string reason)
            : base(message)
        {
            Reason = reason;
        }
    }
}