﻿using System;
using System.Collections.Generic;

namespace Faradise.Design.Internal.Exceptions
{
    public class ModelValidationException : Exception
    {
        public string Route { get; private set; }
        public IEnumerable<string> Errors { get; private set; }

        public ModelValidationException(string route, IEnumerable<string> errors)
        {
            Route = route;
            Errors = errors;
        }
    }
}
