﻿using Faradise.Design.Models;
using System;

namespace Faradise.Design.Internal.Exceptions
{
    public class AuthorizationException : Exception
    {
        public AuthorizationException(Role expected, Role actual)
            : base(string.Format("Expected role: {0}, Actual role: {1}", expected, actual))
        {   }

        public AuthorizationException()
            : base("User was not found")
        {   }

        public AuthorizationException(string token)
            : base(string.Format("Bad token: {0}", token))
        {   }
    }
}