﻿using Faradise.Design.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Faradise.Design.Internal
{
    [APIShaperFilter]
    [APIModelValidationFilter]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class APIController : Controller
    {   }
}