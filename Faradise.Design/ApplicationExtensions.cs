﻿using Microsoft.AspNetCore.Builder;
using System;

namespace Faradise.Design
{
    public static class ApplicationExtensions
    {
        public static bool IsDevelopment(this Startup app)
        {
            return FaradiseEnvironment.Development;
        }

        public static bool IsStable(this Startup app)
        {
            return FaradiseEnvironment.Stable;
        }

        public static bool IsStaging(this Startup app)
        {
            return FaradiseEnvironment.Staging;
        }

        public static bool IsMaster(this Startup app)
        {
            return FaradiseEnvironment.Master;
        }

        public static bool IsIntegration(this Startup app)
        {
            return FaradiseEnvironment.Integration;
        }
    }
}
