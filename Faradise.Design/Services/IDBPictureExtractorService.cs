﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Faradise.Design.Services.Background;

namespace Faradise.Design.Services
{
    public interface IDBPictureExtractorService
    {
        Task UpdatePicture(int pictureId, string internalUrl);
        Task UpdatePictures(Dictionary<int, PictureUploadBackgroundService.UploadResult> results);
    }
}