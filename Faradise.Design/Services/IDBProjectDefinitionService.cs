﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDefinition;
using Faradise.Design.Models.ProjectDevelopment;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IDBProjectDefinitionService
    {
        ProjectDefinition GetProjectDefinition(int projectId);
        ProjectDefinition[] GetProjectsByUser(int userId);

        int CreateProjectDefinition(int clientId);
        void SetProjectName(int projectId, string name);
        void SetProjectCity(int projectId, string name);

        int GetDesigner(int projectId);
        bool AssignDesigner(int projectId, int userId);
        void DisbandDesigner(int projectId, string reason);
        void ConfirmDesigner(int projectId);

        bool CheckUserOwnsService(int projectId, int userId);
        void SetStage(int projectId, ProjectStage stage);
        void SetAbandonStage(int projectId, ProjectStage abandonStage);
        void CloseProject(int projectId);
        FullProject GetFullProject(int projectId);
        void FinishFill(int projectId);
        void SetProjectNeedAdminAttention(int projectId, bool needAttention);
        void MarkProjectAsChangedForAdmin(int projectId, bool isChanged);
        AdminProjectInfo[] GetProjectsInfo();

        void ArchiveProject(int projectId);
        void DeleteUser(int userId);
    }
}
