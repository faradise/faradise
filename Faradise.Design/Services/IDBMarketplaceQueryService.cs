﻿using System.Collections.Generic;
using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Services
{
    public interface IDBMarketplaceQueryService
    {
        ProductsQueryFilters GetQueryFilters(int categoryId);

        Product GetProductById(int productId);
        ProductsQuery GetProductsByFilters(ProductsQueryFiltersValues filters, ARPlatform platform, ARFormat format);
        Category[] GetCategories();
        Category[] GetSubcategories(int id);

        CategoryInfo[] GetCategoryPath(int categoryId);
        CategoryInfo[] GetProductCategoryPath(int productId);
        CategoryHierarchy[] GetCategoryHierarchy();

        int GetTotalProductsCount();
        int GetAvailableProductsCount();

        ARProductDefinition GetARDefinition(int productId, ARPlatform platform, ARFormat format);

        ProductInfo GetDailyProduct();
        Category[] GetCategoriesByIds(IEnumerable<int> categoriesIds);
    }
}