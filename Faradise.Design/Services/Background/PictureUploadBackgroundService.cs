﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Faradise.Design.Models.PictureUploads;
using Faradise.Design.Services.Backgroud;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Background
{
    public class PictureUploadBackgroundService : IBackgroundHostedService
    {
        private readonly IDBPictureExtractorService _dbService = null;
        private readonly IDBPictureExtractorQueryService _queryService = null;
        private readonly string _environment;

        public PictureUploadBackgroundService(IDBPictureExtractorService dbService, IDBPictureExtractorQueryService queryService)
        {
            _dbService = dbService;
            _queryService = queryService;
            _environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        }

        public async Task<bool> Process()
        {
            const int count = 100;

            var pictures = await _queryService.GetNextPicturesToUpload(count);

            if (!pictures.Any())
                return false;

            var uploadTasks = new Task<UploadResult>[pictures.Length];
            for (var p = 0; p < uploadTasks.Length; p++)
            {
                var picture = pictures[p];
                uploadTasks[p] = UploadOnePicture(picture);
            }

            Task.WaitAll(uploadTasks);

            var results = uploadTasks
                .ToDictionary(x => x.Result.PictureId, x => x.Result);

            await _dbService.UpdatePictures(results);

            return true;
        }

        private async Task<UploadResult> UploadOnePicture(PictureUploadTask uploadTask)
        {
            using (var http = new HttpClient())
            {
                var body = new
                {
                    url = uploadTask.PhotoUrl,
                    server = _environment,
                    partnerid = uploadTask.CompanyId,
                    productid = uploadTask.ProductId,
                    pictureid = uploadTask.PictureId
                };

                var s = JsonConvert.SerializeObject(body, Formatting.None);
                var content = new StringContent(s);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                using (var response = await http.PostAsync("https://faradise-design-images.azurewebsites.net/api/image-uploader?code=Ua/mVW07e3EEwrsc97hL8S4RBbkGhHbX2WVtoa6NhyWDCjGXyr1l6Q==", content))
                {
                    var result = new UploadResult
                    {
                        PictureId = uploadTask.PictureId,
                        Processed = response.StatusCode != HttpStatusCode.NotFound
                    };

                    if (!response.IsSuccessStatusCode)
                        return result;

                    var internalUrl = await response.Content.ReadAsStringAsync();
                    if (string.IsNullOrEmpty(internalUrl) || !internalUrl.Contains("url"))
                        return result;

                    var urlTag = JsonConvert.DeserializeObject<UrlResponse>(internalUrl);
                    result.Url = urlTag?.url;

                    return result;
                }
            }
        }

        public class UploadResult
        {
            public int PictureId;
            public string Url;
            public bool Processed;
        }

        private class UrlResponse
        {
            public string url;
        }
    }
}
