﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faradise.Design.Models.Cache;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Backgroud;

namespace Faradise.Design.Services.Background
{
    public class CacheWarmingBackgroundService : IBackgroundHostedService
    {
        private readonly ICacheService _cache = null;
        private readonly IDBMarketplaceQueryServiceProvider _db = null;
        private readonly IPersistentStorageService _persistantStorage = null;

        public CacheWarmingBackgroundService(ICacheService cache, IPersistentStorageService persistantStorage, IDBMarketplaceQueryServiceProvider db)
        {
            _db = db;
            _cache = cache;
            _persistantStorage = persistantStorage;
        }

        public async Task<bool> Process()
        {
            var azureAvailable = _persistantStorage.AzureSearchAvailable ?? false;
            if (!azureAvailable)
                return false;
            
            await WarmUpCategoryFilters();

            return true;
        }

        private async Task WarmUpCategoryFilters()
        {
            var actualCategoryIds = new Queue<int>();
            var categoryHierarchy = _db.GetCategoryHierarchy();

            FlatternCategories(categoryHierarchy, actualCategoryIds);

            foreach (var c in actualCategoryIds)
                RefreshFiltersCache(c);
            
            await Task.CompletedTask;
        }

        private void RefreshFiltersCache(int categoryId)
        {
            var filters = _db.GetQueryFilters(categoryId);

            var key = $"{nameof(_db.GetQueryFilters)}";
            var request = new CacheRequest(key, categoryId);

            _cache.Put(request, filters);
        }

        private void FlatternCategories(CategoryHierarchy[] categoryHierarchy, Queue<int> actualCategoryIds)
        {
            foreach (var c in categoryHierarchy)
            {
                actualCategoryIds.Enqueue(c.Id);
                FlatternCategories(c.Childs, actualCategoryIds);
            }
        }
    }
}
