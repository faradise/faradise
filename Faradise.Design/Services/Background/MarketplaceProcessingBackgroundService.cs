﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Sale;
using Faradise.Design.Services.Backgroud;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Background
{
    public class MarketplaceProcessingBackgroundService : IBackgroundHostedService
    {
        private readonly FaradiseDBContext _context = null;

        public MarketplaceProcessingBackgroundService(FaradiseDBContext context)
        {
            _context = context;
        }

        public async Task<bool> Process()
        {
            var processingRequired = await ProcessingRequired();
            if (processingRequired)
                await HandleProcessing();
            return processingRequired;
        }

        private async Task<bool> ProcessingRequired()
        {
            var lastUpdate = await _context.MarketplaceEntity
                .OrderBy(x => x.Id)
                .Select(x => x.SalesUpdated)
                .FirstAsync();

            var lastUpdatedSale = await _context.Sales.MaxAsync(x => x.Updated);

            if (lastUpdatedSale > lastUpdate)
                return true;

            var lastUpdatedBadge = await _context.Badges.MaxAsync(x => x.Updated);

            if (lastUpdatedBadge > lastUpdate)
                return true;

            var productsNeedUpdate = await _context.MarketplaceEntity
                .OrderBy(x => x.Id)
                .SelectMany(x => x.Categories)
                .SelectMany(y => y.CompanyCategories)
                .Where(y => y.Company.MarketplaceId != null)
                .SelectMany(y => y.Products)
                .Where(y => !y.IsRemoved && (y.Available == null || y.Available.Value))
                .AnyAsync(y => y.Processed == null || y.Processed < lastUpdate);

            return productsNeedUpdate;
        }

        private async Task HandleProcessing()
        {
            try
            {
                var sales = await _context.Sales
                    .ToArrayAsync();

                var badges = await _context.Badges
                    .ToArrayAsync();

                var hasSales = sales != null && sales.Any();
                var hasBadges = badges != null && badges.Any();

                if (!hasSales && !hasBadges)
                    return;

                var lastUpdate = DateTime.MinValue;
                if (hasSales)
                    lastUpdate = sales.Max(x => x.Updated);
                if (hasBadges)
                {
                    var badgeUpdate = badges.Max(x => x.Updated);
                    if (badgeUpdate > lastUpdate)
                        lastUpdate = badgeUpdate;
                }

                var salesPicker = new SalesMapper(sales);
                var badgesPicker = new BadgesMapper(badges);

                var processed = DateTime.UtcNow;

                while (true)
                {
                    var products = await _context.MarketplaceEntity
                        .OrderBy(x => x.Id)
                        .SelectMany(x => x.Categories)
                        .SelectMany(y => y.CompanyCategories)
                        .Where(y => y.Company.MarketplaceId != null)
                        .SelectMany(y => y.Products)
                        .Where(y => !y.IsRemoved && (y.Available == null || y.Available.Value))
                        .Where(y => y.Processed == null || y.Processed < lastUpdate)
                        .Select(x => new
                        {
                            Product = x,
                            Company = x.CompanyCategory.CompanyId,
                            Category = x.CompanyCategory.CategoryId
                        })
                        .Take(200)
                        .ToArrayAsync();

                    if (!products.Any())
                        break;

                    foreach (var p in products)
                    {
                        var sale = salesPicker.Pick(p.Product.Id, p.Company, p.Category ?? 0);
                        var badge = badgesPicker.Pick(p.Product.Id, p.Company, p.Category ?? 0);

                        if (p.Product.ActualPrice != null)
                        {
                            p.Product.Price = p.Product.ActualPrice.Value;
                            p.Product.ActualPrice = null;
                        }

                        if (sale != null)
                        {
                            p.Product.ActualPrice = p.Product.Price;
                            p.Product.Price = (int)(p.Product.ActualPrice * (1.0 - sale.Percents / 100.0));
                            p.Product.SaleId = sale.Id;
                        }
                        else p.Product.SaleId = null;

                        if (badge != null)
                            p.Product.BadgeId = badge.Id;
                        else p.Product.BadgeId = null;

                        p.Product.Processed = processed;
                    }

                    await _context.SaveChangesAsync();
                }

                _context.Sales.RemoveRange(_context.Sales.Where(x => x.IsDeleting));
                _context.Badges.RemoveRange(_context.Badges.Where(x => x.IsDeleting));

                var marketplace = await _context.MarketplaceEntity
                    .OrderBy(x => x.Id)
                    .FirstAsync();

                marketplace.SalesUpdated = processed;

                await _context.SaveChangesAsync();
            }
            catch
            {
                // ignored
            }
        }

        private class SalesMapper
        {
            private SaleEntity Marketplace { get; }

            private Dictionary<int, SaleEntity> Categories { get; } = new Dictionary<int, SaleEntity>();
            private Dictionary<int, SaleEntity> Brands { get; } = new Dictionary<int, SaleEntity>();
            private Dictionary<int, SaleEntity> Products { get; } = new Dictionary<int, SaleEntity>();

            public SalesMapper(SaleEntity[] sales)
            {
                if (sales == null)
                    return;

                foreach (var s in sales.Where(x => x.IsActive && !x.IsDeleting))
                {
                    if (string.IsNullOrEmpty(s.UpdateJson))
                        continue;

                    var jsoned = JsonConvert.DeserializeObject<SaleUpdateJson>(s.UpdateJson);

                    if (jsoned.AllMarkeplace)
                        Marketplace = s;
                    else if (jsoned.ProdyctIds != null && jsoned.ProdyctIds.Any())
                    {
                        foreach (var p in jsoned.ProdyctIds)
                        {
                            if (!Products.ContainsKey(p))
                                Products.Add(p, s);
                            else Products[p] = s;
                        }
                    }
                    else if (jsoned.CompaniesId != null && jsoned.CompaniesId.Any())
                    {
                        foreach (var c in jsoned.CompaniesId)
                        {
                            if (!Brands.ContainsKey(c))
                                Brands.Add(c, s);
                            else Brands[c] = s;
                        }
                    }
                    else if (jsoned.CategoryIds != null && jsoned.CategoryIds.Any())
                    {
                        foreach (var c in jsoned.CategoryIds)
                        {
                            if (!Categories.ContainsKey(c))
                                Categories.Add(c, s);
                            else Categories[c] = s;
                        }
                    }
                }
            }

            public SaleEntity Pick(int productId, int companyId, int categoryId)
            {
                if (Products.ContainsKey(productId))
                    return Products[productId];

                if (Brands.ContainsKey(companyId))
                    return Brands[companyId];

                if (Categories.ContainsKey(categoryId))
                    return Categories[categoryId];

                return Marketplace;
            }
        }

        private class BadgesMapper
        {
            private BadgeEntity Marketplace { get; }

            private Dictionary<int, BadgeEntity> Categories { get; } = new Dictionary<int, BadgeEntity>();
            private Dictionary<int, BadgeEntity> Brands { get; } = new Dictionary<int, BadgeEntity>();
            private Dictionary<int, BadgeEntity> Products { get; } = new Dictionary<int, BadgeEntity>();

            public BadgesMapper(BadgeEntity[] badges)
            {
                if (badges == null)
                    return;

                foreach (var s in badges.Where(x => x.IsActive && !x.IsDeleting))
                {
                    if (string.IsNullOrEmpty(s.UpdateJson))
                        continue;

                    var jsoned = JsonConvert.DeserializeObject<SaleUpdateJson>(s.UpdateJson);

                    if (jsoned.AllMarkeplace)
                        Marketplace = s;
                    else if (jsoned.ProdyctIds != null && jsoned.ProdyctIds.Any())
                    {
                        foreach (var p in jsoned.ProdyctIds)
                        {
                            if (!Products.ContainsKey(p))
                                Products.Add(p, s);
                            else Products[p] = s;
                        }
                    }
                    else if (jsoned.CompaniesId != null && jsoned.CompaniesId.Any())
                    {
                        foreach (var c in jsoned.CompaniesId)
                        {
                            if (!Brands.ContainsKey(c))
                                Brands.Add(c, s);
                            else Brands[c] = s;
                        }
                    }
                    else if (jsoned.CategoryIds != null && jsoned.CategoryIds.Any())
                    {
                        foreach (var c in jsoned.CategoryIds)
                        {
                            if (!Categories.ContainsKey(c))
                                Categories.Add(c, s);
                            else Categories[c] = s;
                        }
                    }
                }
            }

            public BadgeEntity Pick(int productId, int companyId, int categoryId)
            {
                if (Products.ContainsKey(productId))
                    return Products[productId];

                if (Brands.ContainsKey(companyId))
                    return Brands[companyId];

                if (Categories.ContainsKey(categoryId))
                    return Categories[categoryId];

                return Marketplace;
            }
        }
    }
}
