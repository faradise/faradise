﻿using Faradise.Design.Models.AzureSearch;
using Faradise.Design.Models.DAL;
using Faradise.Design.Services.AzureSearch;
using Faradise.Design.Services.Backgroud;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System;

namespace Faradise.Design.Services.Background
{
    public class IndexUpdaterService : IBackgroundHostedService
    {
        private readonly FaradiseDBContext _context;
        private readonly IPersistentStorageService _persistentStorage;
        private readonly IIndexerService _indexer;

        public IndexUpdaterService(FaradiseDBContext context, IPersistentStorageService persistentStorage, IIndexerService indexer)
        {
            _context = context;
            _indexer = indexer;
            _persistentStorage = persistentStorage;
        }

        public async Task CheckAzure()
        {
            if (FaradiseEnvironment.Development)
            {
                _persistentStorage.Put("AzureIsAvailable", false);
                return;
            }
            _persistentStorage.AzureSearchAvailable = false;

            var schema = GetSchema();

            var marketplace = await _context.MarketplaceEntity.FirstAsync();
            var actualSchema = marketplace.AzureProductSchema;
            var indexExists = await _indexer.CheckIndexExists("products");

            if (!indexExists || !string.Equals(schema, actualSchema))
            {
                marketplace.AzureProductSchema = schema;

                await _indexer.CreateIndex<AzureProduct>("products");
                await _context.SaveChangesAsync();
            }
            else _persistentStorage.AzureSearchAvailable = true;
        }

        public async Task<bool> Process()
        {
            if (FaradiseEnvironment.Development)
                return false;
            await _indexer.UploadProducts("products");

            _persistentStorage.AzureSearchAvailable = true;

            return true;
        }

        public string GetSchema()
        {
            var azureProduct = typeof(AzureProduct);
            var result = new StringBuilder("{");

            result.Append("title : " + azureProduct.Name + ", ");
            result.Append("properties : {");

            foreach (var prop in azureProduct.GetProperties())
            {
                result.Append(prop.Name + ": {");

                var attributes= Attribute.GetCustomAttributes(prop);
                foreach(var attribute in attributes)
                {
                    var strAttribute = attribute.ToString();
                    result.Append($"attribute:{strAttribute}"+", ");
                }

                result.Append("type : " + prop.PropertyType.Name.ToLower());
                result.Append(" }");
            }
            return result.ToString();
    }
    }
}
