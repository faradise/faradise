﻿using Faradise.Design.Models.DAL;
using Faradise.Design.Services.Backgroud;
using Faradise.Design.Services.Production.DB;
using System;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Background
{
    public class AutoFeedTaskManagerBackgroundService: IBackgroundHostedService
    {
        private readonly DBMarketplaceService _marketplace = null;

        private DateTime _nextCheckTime = DateTime.UtcNow;

        private readonly TimeSpan _minTimeBetweenAutoFeedUploadsForCompany = TimeSpan.FromHours(6);

        public AutoFeedTaskManagerBackgroundService(ICDNService cdn, FaradiseDBContext context)
        {
            _marketplace = new DBMarketplaceService(context);
        }

        public async Task<bool> Process()
        {
            _marketplace.CreateAutofeedTasks(1, _minTimeBetweenAutoFeedUploadsForCompany);
            return await Task.FromResult(true);
        }
    }
}
