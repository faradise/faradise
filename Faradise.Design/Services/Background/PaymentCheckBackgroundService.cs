﻿using System;
using System.Threading.Tasks;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;
using Faradise.Design.Services.Backgroud;

namespace Faradise.Design.Services.Background
{
    public class PaymentCheckBackgroundService : IBackgroundHostedService
    {
        private readonly IPaymentService _paymentService = null;
        private readonly IBasketService _basketService = null;

        public PaymentCheckBackgroundService(IPaymentService paymentService, IBasketService basketService)
        {
            _paymentService = paymentService;
            _basketService = basketService;
        }

        public async Task<bool> Process()
        {
            var pendingPayments = _paymentService.GetPendingPayments(PaymentMethod.YandexKassa);
            foreach (var paymentId in pendingPayments)
            {
                try
                {
                    var updatedPayment = await _paymentService.RequestAndUpdateKassaPaymentInfo(paymentId);
                    if (updatedPayment == null || updatedPayment.Status == Models.PaymentStatus.Pending)
                        continue;

                    switch (updatedPayment.Status)
                    {
                        case Models.PaymentStatus.Completed:
                            ProccessCompletedTransaction(updatedPayment);
                            break;
                        case Models.PaymentStatus.Canceled:
                            ProccessCanceledTransaction(updatedPayment);
                            break;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }

        private void ProccessCompletedTransaction(Payment payment)
        {
            if (payment.PaymentTarget != PaymentTargetType.BasketMarketplaceGoods)
                return;

            var orderId = _basketService.GetOrderIdByPayment(payment.Id);
            if (orderId == 0)
                return;

            _basketService.SetOrderStatus(orderId, OrderStatus.Paid);
        }

        private void ProccessCanceledTransaction(Payment payment)
        {
            if (payment.PaymentTarget != PaymentTargetType.BasketMarketplaceGoods)
                return;

            var orderId = _basketService.GetOrderIdByPayment(payment.Id);
            if (orderId == 0)
                return;

            _basketService.SetOrderStatus(orderId, OrderStatus.Canceled);
        }
    }
}
