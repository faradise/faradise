﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;
using Faradise.Design.Services.Backgroud;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Background
{
    public class TolokaTaskBackgroundService : IBackgroundHostedService
    {
        private ITolokaService _tolokaService = null;
        private ITolokaAcceptorService _acceptorService = null;

        public TolokaTaskBackgroundService(ITolokaService tolokaService, ITolokaAcceptorService acceptorService)
        {
            _tolokaService = tolokaService;
            _acceptorService = acceptorService;
        }

        public async Task<bool> Process()
        {
            var task = _tolokaService.GetUnprocessedTask();
            if (task == null)
                return false;

            _tolokaService.UpdateTaskStatus(task.Id, TolokaTaskStatus.Processing);

            var file = await _tolokaService.LoadTaskFromCDN(task.Url);
            var tolokaOutput = TolokaOutput.Parse(file);

            await _acceptorService.Accept(tolokaOutput, task.Type, task.Id);

            _tolokaService.UpdateTaskStatus(task.Id, TolokaTaskStatus.Processed);

            return true;
        }
    }
}
