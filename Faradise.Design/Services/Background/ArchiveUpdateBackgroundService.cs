﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.ArchiveUpdate;
using Faradise.Design.Models.Enums;
using Faradise.Design.Services.Backgroud;

namespace Faradise.Design.Services.Background
{
    public class ArchiveUpdateBackgroundService : IBackgroundHostedService
    {
        private readonly IProjectDevelopmentService _development = null;
        private readonly ICompressionService _compression = null;
        private readonly ICDNService _cdn = null;
        private readonly IArchiveService _archive = null;

        public ArchiveUpdateBackgroundService(
            IProjectDevelopmentService development, 
            ICompressionService compression, 
            ICDNService cdn, 
            IArchiveService archive)
        {
            _development = development;
            _compression = compression;
            _cdn = cdn;
            _archive = archive;
        }

        public async Task<bool> Process()
        {
            var archiveData = _archive.GetNextArchiveTask();
            if (archiveData == null)
                return false;
            
            try
            {
                var isSuccess = await UpdateArchiveAsync(archiveData);
                if (!isSuccess)
                    throw new ServiceException(
                        $"Some errors on archiveUpdate for room {archiveData.RoomId} in project id {archiveData.ProjectId}",
                        "archive_update_failed");
            }
            catch (Exception ex)
            {
                throw new ServiceException($"error on archiveUpdate, {ex}", "archive_update_failed");
            }

            return true;
        }

        private async Task<bool> UpdateArchiveAsync(ArchiveUpdateTask archiveData)
        {
            List<string> files = null;
            var archivePath = string.Empty;

            switch (archiveData.Type)
            {
                case RoomArchiveType.Collages:
                    var collages = _development.GetCollages(archiveData.RoomId);
                    files = collages?.Photos?.Select(x => x.Link).ToList();
                    archivePath = _cdn.GetPathForCollageArchive(archiveData.ProjectId);
                    break;
                case RoomArchiveType.Moodboard:
                    var moodboards = _development.GetMoodBoards(archiveData.RoomId);
                    files = moodboards?.Photos?.Select(x => x.Link).ToList();
                    archivePath = _cdn.GetPathForMoodboardArchive(archiveData.ProjectId);
                    break;
                case RoomArchiveType.Zones:
                    var zones = _development.GetZones(archiveData.RoomId);
                    files = zones?.Photos?.Select(x => x.Link).ToList();
                    archivePath = _cdn.GetPathForZoneArchive(archiveData.ProjectId);
                    break;
                case RoomArchiveType.Plans:
                    var plans = _development.GetPlans(archiveData.RoomId);
                    files = plans?.Files?.Select(x => x.Link).ToList();
                    archivePath = _cdn.GetPathForWorkPlanArchive(archiveData.ProjectId);
                    break;
            }

            if (string.IsNullOrEmpty(archivePath) || files == null || !files.Any())
                return false;
            
            var url = await _compression.CompressToZipAndUploadAsync(files, archivePath);
            if (string.IsNullOrEmpty(url))
                return false;

            switch (archiveData.Type)
            {
                case RoomArchiveType.Collages:
                    _development.SetCollageArchive(archiveData.RoomId, url);
                    break;
                case RoomArchiveType.Moodboard:
                    _development.SetMoodboardArchive(archiveData.RoomId, url);
                    break;
                case RoomArchiveType.Zones:
                    _development.SetZoneArchive(archiveData.RoomId, url);
                    break;
                case RoomArchiveType.Plans:
                    _development.SetPlanArchive(archiveData.RoomId, url);
                    break;
            }

            _archive.CompleteArchiveTask(archiveData.Id);

            return true;
        }
    }
}
