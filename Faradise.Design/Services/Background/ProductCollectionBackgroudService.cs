﻿using Faradise.Design.Services.Backgroud;
using System;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Background
{
    public class ProductCollectionBackgroudService : IBackgroundHostedService
    {
        private readonly IDBProductCollectionService _dbProductCollectionService = null;
        private readonly int _productsPerUpdate = 100;

        public ProductCollectionBackgroudService(IDBProductCollectionService dbProductCollectionService)
        {
            _dbProductCollectionService = dbProductCollectionService;
        }

        public async Task<bool> Process()
        {
            var companies = _dbProductCollectionService.GetCompaniesToUpdate();
            for(int c = 0; c < companies.Length; c++)
            {
                var company = companies[c];
                await _dbProductCollectionService.UpdateProductCollectionForCompany(company, _productsPerUpdate);
            }
            return true;
        }
    }
}
