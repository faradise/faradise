﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Extensions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Services.Backgroud;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace Faradise.Design.Services.Background
{
    public class FeedAnalyzerBackgroundService : IBackgroundHostedService
    {
        private readonly FaradiseDBContext _context;
        private readonly ICDNService _cdnService;
        private const string ReportName = "actual_feed_report";

        private Dictionary<int, MarketplaceCategoryEntity> CategoriesCache =
            new Dictionary<int, MarketplaceCategoryEntity>();

        private Dictionary<int, ProductProjection> ProductCache = new Dictionary<int, ProductProjection>();

        public FeedAnalyzerBackgroundService(FaradiseDBContext context,
            ICDNService cdnService)
        {
            _context = context;
            _cdnService = cdnService;
        }


        public async Task<bool> Process()
        {
            _context
                .MarketPlaceCategories
                .Include(entity => entity.Parameters)
                .ThenInclude(entity => entity.Parameter)
                .ToArray()
                .ForEach(entity => CategoriesCache.Add(entity.Id, entity));
            var marketplaceCategories =
                    CategoriesCache
                        .Select(entity => new
                        {
                            categoryId = entity.Value.Id,
                            categoryName = entity.Value.Name,
                            parameters = entity.Value.Parameters.Select(parameterEntity => new ParameterData
                            {
                                Id = parameterEntity.Parameter.Id,
                                Parameter = parameterEntity.Parameter.Name,
                                Synonyms = JsonConvert.DeserializeObject<string[]>(parameterEntity.Parameter.Synonyms)
                                    .ToList(),
                                GlobalParameter = parameterEntity.Parameter.GlobalParameter
                            }).ToList()
                        })
                        .ToArray()
                ;

            _context
                .Parameters
                .Where(entity => entity.GlobalParameter)
                .ForEach(globalParameter =>
                {
                    marketplaceCategories.ForEach(category =>
                    {
                        if (category.parameters.All(parameterData =>
                            parameterData.Id != globalParameter.Id))
                        {
                            category.parameters.Add(new ParameterData
                            {
                                Id = globalParameter.Id,
                                Parameter = globalParameter.Name,
                                Synonyms = JsonConvert.DeserializeObject<string[]>(globalParameter.Synonyms)
                                    .ToList(),
                                GlobalParameter = globalParameter.GlobalParameter
                            });
                        }
                    });
                })
                ;
            ;

            var categories = marketplaceCategories.Select(mpc =>
                    {
                        List<int> childCategories = new List<int>();
                        AppendChildCategories(mpc.categoryId);

                        void AppendChildCategories(int sourceId)
                        {
                            var underLayerCategories = CategoriesCache
                                .Where(entity => entity.Value.ParentId == sourceId).Select(entity => entity.Value.Id);

                            childCategories.Add(sourceId);

                            underLayerCategories.ForEach(AppendChildCategories);
                        }

                        return new ReportCategory
                        {
                            SubCategoriesIds = childCategories.ToArray(),
                            Parameters = mpc.parameters,
                            SourceCategoryId = mpc.categoryId,
                            SourceCategoryName = mpc.categoryName
                        };
                    })
                    .ToList()
                ;
            ;
            categories.ForEach(category => InheritParameters(category.SourceCategoryId, category));
            ;

            _context
                .Products
                .WhereSaleable()
                .Select(entity => new
                {
                    entity.Id,
                    projection = new ProductProjection
                    {
                        Data = entity.JSONNotes,
                        CategoryId = entity.CompanyCategory.Category.Id,
                        CompanyId = entity.CompanyId,
                        CompanyName = entity.CompanyCategory.Company.Name
                    }
                })
                .ToArray()
                .ForEach(arg => ProductCache.Add(arg.Id, arg.projection));

            void ProcessCategory(ReportCategory category)
            {
                var categoryProducts =
                        ProductCache
                            .Where(pair => category.SubCategoriesIds.Contains(pair.Value.CategoryId))
                            .Select(entity => new
                            {
                                data = entity.Value.Data,
                                partner = new
                                {
                                    Id = entity.Value.CompanyId,
                                    Name = entity.Value.CompanyName
                                }
                            })
                            .ToArray()
                    ;

                string[] DeserializeJsonNote(string data)
                {
                    var deserializedObject = JsonConvert.DeserializeObject<string[]>(data);

                    if (deserializedObject != null && deserializedObject.Any())
                        return deserializedObject
                            .Where(s => s.Contains(":"))
                            .Select(s => s.Split(":")[0])
                            .ToArray();
                    return Array.Empty<string>();
                }

                var preparedProducts =
                        categoryProducts
                            .Where(s => !string.IsNullOrEmpty(s.data))
                            .Select(entity => new
                            {
                                data = DeserializeJsonNote(entity.data),
                                partner = entity.partner
                            })
                            .ToArray()
                    ;

                foreach (var parameter in category.Parameters)
                {
                    parameter.ScanResult = new ScanResult
                    {
                        UsedParameter = parameter,
                        Result = preparedProducts.Count(products =>
                            products.data.Any(param => parameter.Synonyms.Contains(param.ToLower()))),
                        TotalProducts = categoryProducts.Length
                    };

                    preparedProducts
                        .GroupBy(enumerable => enumerable.partner.Id)
                        .Select(grouping => new PartnerScanResult
                        {
                            PartnerName = grouping.First().partner.Name,
                            PartnerId = grouping.Key,
                            PartnerProductsCount = categoryProducts.Count(arg => arg.partner.Id == grouping.Key),

                            Result = grouping.Count(products =>
                                products.data.Any(param => parameter.Synonyms.Contains(param.ToLower())))
                        })
                        .ForEach(result => parameter.ScanResult.PartnersLayout.Add(result.PartnerId, result));
                }
            }

            Parallel.ForEach(categories, ProcessCategory);
            var path = _cdnService.GetPathForFeedAnalyzerExcelFiles();
            await _cdnService.UploadData(CreateExcel(new Report {Created = DateTime.Now, Lists = categories}), path,
                $"{ReportName}", ".xlsx");
            return true;
        }

        private void InheritParameters(int sourceId, ReportCategory category)
        {
            var parentId = CategoriesCache[sourceId].ParentId;
            if (parentId == null)
                return;

            var parents = CategoriesCache
                .Where(entity => entity.Key == parentId)
                .ToArray();
            ;
            parents
                .SelectMany(entity => entity.Value.Parameters.Select(parameterEntity => parameterEntity.Parameter))
                .ForEach(parameterEntity =>
                {
                    if (category.Parameters.All(data => data.Id != parameterEntity.Id))
                    {
                        category.Parameters.Add(new ParameterData
                        {
                            Id = parameterEntity.Id,
                            Parameter = parameterEntity.Name,
                            Synonyms = JsonConvert.DeserializeObject<string[]>(parameterEntity.Synonyms)
                                .ToList(),
                            GlobalParameter = parameterEntity.GlobalParameter
                        });
                    }
                });
            parents.ForEach(entity => InheritParameters(entity.Value.Id, category));
        }

        private byte[] CreateExcel(Report report)
        {
            using (var excel = new ExcelPackage())
            {
                foreach (var category in report.Lists)
                {
                    var categoryName = FixName(category);
                    var sheet = excel.Workbook.Worksheets.Add(categoryName);
                    var partnerRowsCorrelation = new Dictionary<int, int>();

                    category.Parameters.SelectMany(data => data.ScanResult.PartnersLayout.Values)
                        .Select(result => new {result.PartnerId, result.PartnerName})
                        .DistinctBy(arg => arg.PartnerId)
                        .ForEach(
                            (partner, row) =>
                            {
                                sheet.Cells[row, 1].Value = $"{partner.PartnerName} ({partner.PartnerId})";
                                partnerRowsCorrelation.Add(partner.PartnerId, row);
                            }, 3);

                    sheet.Cells[2, 1].Value =
                        "Всего для категории";

                    category.Parameters.ForEach((parameter, columnIndex) =>
                    {
                        sheet.Cells[1, columnIndex].Value = parameter.Parameter;
                        sheet.Cells[2, columnIndex].Value =
                            $"{parameter.ScanResult.ResultPercent}% ({parameter.ScanResult.Result}/{parameter.ScanResult.TotalProducts})";


                        parameter.ScanResult.PartnersLayout.Values.ForEach(result =>
                        {
                            sheet.Cells[partnerRowsCorrelation[result.PartnerId], columnIndex].Value =
                                $"{result.ResultPercent}% ({result.Result}/{result.PartnerProductsCount})";
                        });
                    }, 2);
                }

                excel.Workbook.Worksheets.ForEach(worksheet => worksheet.DefaultColWidth = 25);
                return excel.GetAsByteArray();
            }
        }

        /// <summary>
        /// максимальная длина названия страницы в экселе ограничена 31 символом
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        private static string FixName(ReportCategory category)
        {
            return string.IsNullOrEmpty(category.SourceCategoryName)
                ? category.SourceCategoryId.ToString()
                : $"{new string(category.SourceCategoryName.Take(31 - (category.SourceCategoryId.ToString().Length + 2)).ToArray())}({category.SourceCategoryId})";
        }
    }

    public class ProductProjection
    {
        public string Data { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int CategoryId { get; set; }
    }


    public class Report
    {
        public List<ReportCategory> Lists { get; set; }
        public DateTime Created { get; set; }
    }

    public class ReportCategory
    {
        public int SourceCategoryId { get; set; }
        public string SourceCategoryName { get; set; }

        public int[] SubCategoriesIds { get; set; }
        public List<ParameterData> Parameters { get; set; } = new List<ParameterData>();
    }

    public class ScanResult
    {
        public ParameterData UsedParameter { get; set; }
        public int Result { get; set; }
        public int TotalProducts { get; set; }

        public double ResultPercent => Math.Round((double) Result / TotalProducts * 100, 1);

        public Dictionary<int, PartnerScanResult> PartnersLayout { get; set; } =
            new Dictionary<int, PartnerScanResult>();
    }

    public class PartnerScanResult
    {
        public int PartnerId { get; set; }
        public string PartnerName { get; set; }

        public int PartnerProductsCount { get; set; }
        public int Result { get; set; }
        public double ResultPercent => Math.Round((double) Result / PartnerProductsCount * 100, 1);
    }

    public class ParameterData
    {
        public int Id { get; set; }
        private List<string> _synonyms = new List<string>();
        public string Parameter { get; set; }

        public List<string> Synonyms
        {
            get
            {
                if (!_synonyms.Contains(Parameter))
                    _synonyms.Add(Parameter);
                return _synonyms
                    .Where(s => !string.IsNullOrWhiteSpace(s))
                    .Select(s => s.ToLower())
                    .ToList();
            }
            set => _synonyms = value;
        }

        public bool GlobalParameter { get; set; }

        public ScanResult ScanResult { get; set; }
    }
}