﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Marketplace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Marketplace.Xml;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Backgroud;
using Faradise.Design.Services.Production.DB;

namespace Faradise.Design.Services.Background
{
    public class CompanyFeedAcceptorBackgroundService : IBackgroundHostedService
    {
        private readonly DBMarketplaceService _marketplace = null;
        private readonly ICDNService _cdn = null;

        public CompanyFeedAcceptorBackgroundService(ICDNService cdn, FaradiseDBContext context)
        {
            context.ChangeTracker.LazyLoadingEnabled = false;

            _marketplace = new DBMarketplaceService(context);
            _cdn = cdn;

            RefreshProcessingUploads();
        }

        private void RefreshProcessingUploads()
        {
            _marketplace.RefreshProcessingUploads();
        }

        public async Task<bool> Process()
        {
            var updateTask = _marketplace.GetNextYmlUpdateTask();
            if (updateTask != null)
            {
                _marketplace.MarkYMLUpdateTaskAsProcessing(updateTask.Id);

                try
                {
                    var link = updateTask.YMLLink;

                    byte[] bytes = null;

                    if (updateTask.DownloadSource == YMLDownloadSource.CDN)
                    {
                        bytes = await _cdn.DownloadBytes(link);
                        if (bytes == null || bytes.Length == 0)
                            throw new ServiceException(
                                $"yml download failed for companyId {updateTask.CompanyId}",
                                "yml_download_failed");
                    }
                    else if (updateTask.DownloadSource == YMLDownloadSource.AutoFeed)
                    {
                        bytes = await DownloadFromRemoteSource(updateTask.YMLLink);
                        if (bytes == null || bytes.Length == 0)
                            throw new ServiceException(
                                $"yml download failed for companyId {updateTask.CompanyId}",
                                "yml_download_failed");
                    }

                    MarketplaceCompany company;

                    using (var stream = new MemoryStream(bytes))
                    {
                        switch (updateTask.FileType)
                        {
                            case UploadFiletype.YML:
                                company = MarketplaceParser.FromXml(stream);
                                break;
                            case UploadFiletype.EXCEL:
                                company = XlsxMarketplaceParser.Parse(stream).company;
                                break;
                            default:
                                throw new Exception("Unknown feed format");
                        }
                    }

                    company.Id = updateTask.CompanyId;

                    var data = new UpdateCompanyData
                    {
                        Id = company.Id,
                        Name = company.Name,
                        Url = company.Url,
                        Categories = company.Categories
                            .Select(x => x.ToService(company.Id))
                            .ToList(),
                        Products = company.Products
                            .Where(x => company.Categories.Any(y => y.Id == x.CategoryId))
                            .Select(x => x.ToService(company.Id))
                            .ToList()
                    };

                    _marketplace.UpdateCompany(data);
                    _marketplace.MarkYMLUpdateTaskAsCompleted(updateTask.Id);
                }
                catch (Exception ex)
                {
                    _marketplace.MarkYMLUpdateTaskAsFailed(updateTask.Id, ex.ToString());
                }
            }

            return true;
        }


        private static async Task<byte[]> DownloadFromRemoteSource(string url)
        {
            const int maxTrys = 50;

            for (var i = 0; i < maxTrys + 1; i++)
            {
                try
                {
                    using (var client = new HttpClient())
                    using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url)))
                    {
                        client.Timeout = TimeSpan.FromMinutes(20);

                        request.Headers.TryAddWithoutValidation(
                            "User-Agent",
                            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36");

                        request.Properties["RequestTimeout"] = client.Timeout;

                        using (var response = await client.SendAsync(request).ConfigureAwait(false))
                        {
                            if (!response.IsSuccessStatusCode)
                                return null;

                            using (var content = response.Content)
                            {
                                var bytes = await content.ReadAsByteArrayAsync();
                                if (bytes != null && bytes.Length > 0)
                                    return bytes;
                            }
                        }
                    }
                }
                catch
                {   }

                await Task.Delay(TimeSpan.FromSeconds(5));
            }

            throw new ServiceException($"Unable to download yml from {url}", "yml_download_failed");
        }
    }
}
