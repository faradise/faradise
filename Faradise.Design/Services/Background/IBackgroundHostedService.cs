﻿using System.Threading.Tasks;

namespace Faradise.Design.Services.Backgroud
{
    public interface IBackgroundHostedService
    {
        Task<bool> Process();
    }
}
