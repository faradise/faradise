﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Faradise.Design.Models.Feed.Google;
using Faradise.Design.Models.Feed.Yml;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Backgroud;
using Faradise.Design.Utilities.Feed;

namespace Faradise.Design.Services.Background
{
    public class FeedsBackgroundService : IBackgroundHostedService
    {
        private readonly TimeSpan _feedsUpdatePeriod = TimeSpan.FromHours(8);

        private readonly IDBFeedService _db = null;
        private readonly IDBMarketplaceQueryService _dbQuery = null;
        private readonly ICDNService _cdn = null;

        public FeedsBackgroundService(IDBFeedService db, IDBMarketplaceQueryService dbQuery, ICDNService cdn)
        {
            _db = db;
            _dbQuery = dbQuery;
            _cdn = cdn;
        }

        public async Task<bool> Process()
        {
            try
            {
                var updateRequired = await CheckFeedsUpdateRequired();
                if (!updateRequired)
                    return false;

                await ProcessFeedsUpdate();

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<bool> CheckFeedsUpdateRequired()
        {
            var lastUpdate = await _db.ExtractFeedsUpdateTime();
            return DateTime.UtcNow - lastUpdate > _feedsUpdatePeriod;
        }

        private async Task ProcessFeedsUpdate()
        {
            var feeds = CreateFeedContainers();

            var categories = ExtractCategories();
            var products = await ExtractProducts();
            var skippedCategories = await ExtractSkippedCategories();

            UpdateProductCategories(categories, products);

            foreach (var f in feeds)
            {
                var p = products.AsEnumerable();

                if (f.Cost == FeedCostType.From10K)
                {
                    p = p.Where(x => x.Price > 10000);
                    p = p.Where(x => x.CategoryId > 0 && !skippedCategories.Contains(x.CategoryId));
                }

                if (f.Availability == FeedAvailabilityType.Available)
                    p = p.Where(x => x.Available);

                f.AcceptCategories(categories.ToArray());
                f.AcceptProducts(p.ToArray());

                var filename = $"feed_{f.Format}_{f.Availability}_{f.Cost}_{DateTime.Now:yyyy-MM-dd-hh-mm}".ToLower();

                var file = f.SerializeFeed(filename);
                var url = await Upload(file, filename);

                await _db.StoreFeedLink(url, f.Format, f.Availability, f.Cost);

                f.Dispose();

                if (File.Exists(filename))
                    File.Delete(filename);
            }
        }

        public async Task<byte[]> GetFeedByProductIds(int[] productIds)
        {
            var feed = CreateFeed(FeedFormatType.Yml, FeedAvailabilityType.Full, FeedCostType.Full);
            var products =await ExtractProductsByIds(productIds);
            var categories= ExtractCategoriesByIds(products.Select(x => x.CategoryId).Distinct());
            var p = products.AsEnumerable();

            feed.AcceptCategories(categories.ToArray());
            feed.AcceptProducts(p.ToArray());

            var filename = $"feed_{feed.Format}_{feed.Availability}_{feed.Cost}_{DateTime.Now:yyyy-MM-dd-hh-mm}".ToLower();

            var file = feed.SerializeFeed(filename);
            var bytes= File.ReadAllBytes(filename);
            feed.Dispose();

            if (File.Exists(filename))
                File.Delete(filename);
            return bytes;
            //var url = await Upload(file, filename);

            //await _db.StoreFeedLink(url, f.Format, f.Availability, f.Cost);

            //f.Dispose();

            //if (File.Exists(filename))
            //    File.Delete(filename);


        }

        private List<Category> ExtractCategoriesByIds(IEnumerable<int> categoriesIds)
        {
            return _dbQuery.GetCategoriesByIds(categoriesIds).ToList();
        }

        private async Task<int[]> ExtractSkippedCategories()
        {
            var skip = new[] {14};

            return await _db.ExtractSkippedCategories(skip);
        }

        private static void UpdateProductCategories(List<Category> cats, List<FeedProduct> products)
        {
            var categories = cats.Select(x => new
            {
                x.Id,
                x.ParentId,
                x.Name
            })
            .ToArray();

            var categoryDictionary = categories.ToDictionary(x => x.Id, x => x);
            var paths = new Dictionary<int, string[]>();

            foreach (var p in products)
            {
                if (p.CategoryId == 0)
                {
                    p.Categories = new string[0];
                    continue;
                }

                if (!paths.ContainsKey(p.CategoryId))
                {
                    var list = new List<string>();
                    var category = p.CategoryId;

                    while (category > 0 && categoryDictionary.ContainsKey(category))
                    {
                        var c = categoryDictionary[category];
                        list.Insert(0, c.Name);
                        category = c.ParentId;
                    }

                    paths.Add(p.CategoryId, list.ToArray());
                }

                p.Categories = paths[p.CategoryId];
            }
        }

        private async Task<string> Upload(string file, string filename)
        {
            var bytes = File.ReadAllBytes(file);
            var url = await _cdn.UploadData(bytes, _cdn.GetPathForFeedYml(), filename, ".xml");

            return url;
        }

        private async Task<List<FeedProduct>> ExtractProducts()
        {
            const int step = 200;

            var count = await _db.ExtractProductsCount();
            var products = new List<FeedProduct>(count);
            var errors = 0;

            for (var i = 0; i < count / step + 1; i++)
            {
                try
                {
                    var query = await _db.ExtractFeedProducts(i * step, step);
                    products.AddRange(query);
                }
                catch (Exception)
                {
                    i--;
                    errors++;

                    if (errors > 50)
                        throw;
                }

                await Task.Delay(TimeSpan.FromMilliseconds(50));
            }

            return products;
        }
        private async Task<List<FeedProduct>> ExtractProductsByIds(int[] productsIds)
        {
            const int step = 200;

            var count = productsIds.Length;
            var products = new List<FeedProduct>(count);
            var errors = 0;

            for (var i = 0; i < count / step + 1; i++)
            {
                try
                {
                    var query = _db.ExtractFeedProductsByIds(productsIds.Skip(i*step).Take(step));
                    products.AddRange(query);
                }
                catch (Exception)
                {
                    i--;
                    errors++;

                    if (errors > 50)
                        throw;
                }

                await Task.Delay(TimeSpan.FromMilliseconds(50));
            }

            return products;
        }

        private List<Category> ExtractCategories()
        {
            return _dbQuery
                .GetCategories()
                .ToList();
        }

        private IEnumerable<Feed> CreateFeedContainers()
        {
            var formats = Enum.GetValues(typeof(FeedFormatType)).Cast<FeedFormatType>().ToArray();
            var availability = Enum.GetValues(typeof(FeedAvailabilityType)).Cast<FeedAvailabilityType>().ToArray();
            var costs = Enum.GetValues(typeof(FeedCostType)).Cast<FeedCostType>().ToArray();

            var feeds = new List<Feed>();

            foreach (var f in formats)
                foreach (var a in availability)
                    foreach (var c in costs)
                        feeds.Add(CreateFeed(f, a, c));
            
            return feeds;
        }

        private Feed CreateFeed(FeedFormatType format, FeedAvailabilityType availability, FeedCostType cost)
        {
            switch (format)
            {
                case FeedFormatType.Google:
                    return new GoogleFeed(availability, cost);
                case FeedFormatType.Yml:
                    return new YmlFeed(availability, cost);
                default:
                    throw new Exception($"Unsupported feed format - {format}");
            }
        }

        private abstract class Feed : IDisposable
        {
            public FeedFormatType Format { get; }
            public FeedAvailabilityType Availability { get; }
            public FeedCostType Cost { get; }

            protected Feed(FeedFormatType format, FeedAvailabilityType availability, FeedCostType cost)
            {
                Format = format;
                Availability = availability;
                Cost = cost;
            }

            public abstract void AcceptCategories(Category[] categories);
            public abstract void AcceptProducts(FeedProduct[] products);
            public abstract string SerializeFeed(string filename);

            public virtual void Dispose()
            {}
        }

        private class YmlFeed : Feed
        {
            private YmlCategory[] _categories = null; 
            private List<YmlOffer> _offers = null;
            private Category[] _dbCategories = new Category[0];
            private XmlDeliveryOption _shopDelivery = null;

            public override void AcceptCategories(Category[] categories)
            {
                _dbCategories = categories;
                _categories = categories
                    .Select(x => new YmlCategory
                    {
                        Id = x.Id.ToString(),
                        ParentId = x.ParentId > 0 ? x.ParentId.ToString() : null,
                        Name = x.Name
                    })
                    .ToArray();

                if (categories.Count() > 0)
                {
                    _shopDelivery = new XmlDeliveryOption
                    {
                        cost = _dbCategories.Max(x => x.DeliveryCost).ToString(),
                        days = _dbCategories.Max(x => x.DeliveryDays).ToString()
                    };
                }
            }

            public override void AcceptProducts(FeedProduct[] products)
            {
                _offers = new List<YmlOffer>(products.Length);

                foreach (var o in products)
                {
                    try
                    {
                        var offer = o.ToYml();

                        if (o.CategoryId == 0)
                            continue;

                        var category = _dbCategories.FirstOrDefault(x => x.Id == o.CategoryId);
                        if (category == null)
                            continue;

                        if (category.DeliveryDays > 0 && category.DeliveryCost > 0)
                        {
                            offer.DeliveryOptions = new[]
                            {
                                new XmlDeliveryOption
                                {
                                    cost = category.DeliveryCost.ToString(),
                                    days = category.DeliveryDays.ToString()
                                }
                            };
                        }
                        else offer.Delivery = false;

                        _offers.Add(offer);
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }

            public override string SerializeFeed(string filename)
            {
                var ymlCatalog = new YmlCatalog
                {
                    Shop = new YmlShop
                    {
                        Categories = _categories,
                        Offers = _offers.ToArray(),
                        DeliveryOptions = new[] { _shopDelivery }
                    }
                };

                using (var file = File.OpenWrite(filename))
                using (var textWriter = new StreamWriter(file))
                {
                    var xmlSerializer = new XmlSerializer(typeof(YmlCatalog));
                    var ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    xmlSerializer.Serialize(textWriter, ymlCatalog, ns);
                }

                return filename;
            }

            public override void Dispose()
            {
                base.Dispose();

                _categories = null;
                _dbCategories = null;
                _offers = null;
            }

            public YmlFeed(FeedAvailabilityType availability, FeedCostType cost) 
                : base(FeedFormatType.Yml, availability, cost)
            {}
        }

        private class GoogleFeed : Feed
        {
            private List<GoogleEntry> _entries = null;

            public override void AcceptCategories(Category[] categories)
            {}

            public override void AcceptProducts(FeedProduct[] products)
            {
                _entries = new List<GoogleEntry>(products.Length);

                foreach (var o in products)
                {
                    try
                    {
                        _entries.Add(o.ToGoogle());
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }

            public override string SerializeFeed(string filename)
            {
                var feed = new Utilities.Feed.Google.GoogleFeed
                {
                    Entries = _entries.ToArray()
                };

                var ns = new XmlSerializerNamespaces();
                ns.Add("g", "http://base.google.com/ns/1.0");

                using (var file = File.OpenWrite(filename))
                using (var textWriter = new StreamWriter(file))
                {
                    var xmlSerializer = new XmlSerializer(typeof(Utilities.Feed.Google.GoogleFeed), "http://www.w3.org/2005/Atom");
                    xmlSerializer.Serialize(textWriter, feed, ns);
                }

                return filename;
            }

            public override void Dispose()
            {
                base.Dispose();

                _entries = null;
            }

            public GoogleFeed(FeedAvailabilityType availability, FeedCostType cost) 
                : base(FeedFormatType.Google, availability, cost)
            {}
        }
    }
}