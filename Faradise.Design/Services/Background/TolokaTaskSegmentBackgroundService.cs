﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;
using Faradise.Design.Services.Backgroud;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Background
{
    public class TolokaTaskSegmentBackgroundService : IBackgroundHostedService
    {
        private ITolokaService _tolokaService = null;
        private ITolokaAcceptorService _acceptorService = null;

        public TolokaTaskSegmentBackgroundService(ITolokaService tolokaService, ITolokaAcceptorService acceptorService)
        {
            _tolokaService = tolokaService;
            _acceptorService = acceptorService;
        }

        public async Task<bool> Process()
        {
            var segment = _tolokaService.GetUnprocessedSegment();

            if (segment == null)
                return false;

            _tolokaService.UpdateSegmentStatus(segment.Id, TolokaTaskStatus.Processing);

            var file = await _tolokaService.LoadTaskSegmentFromCDN(segment.Url);
            var tolokaOutput = TolokaOutput.Parse(file);

            var result = await _acceptorService.AcceptSegment(tolokaOutput, segment.Type, segment.TaskId);

            if(result == true)
                _tolokaService.UpdateSegmentStatus(segment.Id, TolokaTaskStatus.Processed);

            return true;
        }

        public async Task ClearStatus()
        {
            await _tolokaService.ClearStatus();
        }
    }
}
