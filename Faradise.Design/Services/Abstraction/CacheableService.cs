﻿using Faradise.Design.Models.Cache;

namespace Faradise.Design.Services.Abstraction
{
    public abstract class CacheableService<TD>
    {
        private readonly TD _decoratee;
        private readonly ICacheService _cache;

        protected TD Decoratee { get => _decoratee; }

        protected CacheableService(TD decoratee, ICacheService cache)
        {
            _cache = cache;
            _decoratee = decoratee;
        }

        protected T HandleCaching<T>(string method, params object[] args)
        {
            var request = new CacheRequest(method, args);
            var cachedValue = _cache.Extract<T>(request);
            if (cachedValue.Succeeded)
                return cachedValue.Value;

            var decorateeMethod = _decoratee
                .GetType()
                .GetMethod(method);

            var decorateeResult = (T) decorateeMethod.Invoke(_decoratee, args);

            _cache.Put(request, decorateeResult);

            return decorateeResult;
        }
    }
}
