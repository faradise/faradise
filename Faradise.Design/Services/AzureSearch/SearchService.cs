﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.AzureSearch;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Configs;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Services.AzureSearch
{
    public class SearchService : ISearchService
    {
        private static string _searchServiceName = string.Empty;
        private static string _searchServiceAdminApiKey = string.Empty;
        private readonly FaradiseDBContext _context;

        public SearchService(IOptions<AzureSearchOptions> azureSearchOptions, FaradiseDBContext context)
        {
            _searchServiceName = azureSearchOptions.Value.SearchServiceName;
            _searchServiceAdminApiKey = azureSearchOptions.Value.SearchServiceAdminApiKey;

            _context = context;

            if (!FaradiseEnvironment.Integration)
            {
                if (string.IsNullOrEmpty(_searchServiceAdminApiKey) || string.IsNullOrEmpty(_searchServiceName))
                    throw new ServiceException("Incorrect options", "no_azure_options");
            }
        }

        private static SearchIndexClient CreateSearchIndexClient(string indexName)
        {
            string searchServiceName = _searchServiceName;
            string queryApiKey = _searchServiceAdminApiKey;

            SearchIndexClient indexClient =
                new SearchIndexClient(searchServiceName, indexName, new SearchCredentials(queryApiKey));
            return indexClient;
        }

        public List<T> FullTextSearch<T>(AzureFullTextSearchFilter filter) where T : class
        {
            var parameters = new SearchParameters()
            {
                QueryType = QueryType.Full,
                Top = filter.Count,
                Skip = filter.Offset,
            };

            var indexClient = CreateSearchIndexClient(filter.IndexName);
            var isId = int.TryParse(filter.Query, out _);
            if (isId)
            {
                var doc = indexClient.Documents.Get<T>(filter.Query);
                return new List<T>() { doc };
            }

            var formattedQuery = FormatStringQuery(filter.Query, filter.Flexibility);
            var result = indexClient.Documents.Search<T>(formattedQuery, parameters);
            var docs = result.Results.Select(x => x.Document).ToList();

            return docs;
        }

        private string FormatStringQuery(string query, int flexibility)
        {
            var queryArray = query.Split(" ");
            if (queryArray.Length > 1)
            {
                for (var i = 0; i < queryArray.Length; i++)
                    queryArray[i] = "+" + queryArray[i] + "~" + flexibility;
                query = string.Join(" ", queryArray);
                return query;
            }
            else return "+" + query + "~" + flexibility;

        }

        private SearchParameters GetSearchParametersByFilter(ProductsQueryFiltersValues filter,
            ARPlatform platform = ARPlatform.Browser, ARFormat format = ARFormat.None)
        {
            var parameters = new SearchParameters();
            parameters.IncludeTotalResultCount = true;
            parameters.Filter = string.Empty;

            if (filter.Offset > 0)
                parameters.Skip = filter.Offset;

            if (filter.Count > 0)
                parameters.Top = filter.Count;

            if (filter.Category > 0)
                parameters.Filter += $"MarketplaceCategory/any(t: t eq {filter.Category})";

            if (filter.SalesOnly)
            {
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += "(PreviousPrice gt Price or ActualPrice gt Price)";
                else
                    parameters.Filter += "and (PreviousPrice gt Price or ActualPrice gt Price)";
            }

            if (filter.PriceFrom != null)
            {
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"Price ge {filter.PriceFrom}";
                else
                    parameters.Filter += $"and Price ge {filter.PriceFrom}";
            }

            if (filter.PriceTo != null)
            {
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"Price le {filter.PriceTo}";
                else
                    parameters.Filter += $"and Price le {filter.PriceTo}";
            }

            if (filter.Rooms != null && filter.Rooms.Length > 0)
            {
                var expressions = filter.Rooms.Select(x => $"t eq {x}");
                var roomFilter = string.Join(" or ", expressions);
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"Rooms/any(t: {roomFilter})";
                else
                    parameters.Filter += $"and Rooms/any(t: {roomFilter})";
            }

            if (filter.Brands != null && filter.Brands.Length > 0)
            {
                var expressions = filter.Brands.Select(x => $"t eq {x}");
                var brandFilter = string.Join(" or ", expressions);
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"Brands/any(t: {brandFilter})";
                else
                    parameters.Filter += $"and Brands/any(t: {brandFilter})";
            }

            if (filter.Colors != null && filter.Colors.Length > 0)
            {
                var expressions = filter.Colors.Select(x => $"t eq {x}");
                var colorsFilter = string.Join(" or ", expressions);
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"Colors/any(t: {colorsFilter})";
                else
                    parameters.Filter += $"and Colors/any(t: {colorsFilter})";
            }


            if (filter.Ordering != FilterOrdering.None)
            {
                parameters.OrderBy = new List<string>();
                if (filter.Ordering == FilterOrdering.Ascending)
                    parameters.OrderBy.Add("Price asc");
                else if (filter.Ordering == FilterOrdering.Sale)
                    parameters.OrderBy.Add("SalePercent desc");
                else if (filter.Ordering == FilterOrdering.AR)
                    parameters.OrderBy.Add("ARSupport desc");
                else
                    parameters.OrderBy.Add("Price desc");

            }

            if (platform != ARPlatform.Browser || filter.AR)
            {
                var arType = GetARType(platform, format);
                if (string.IsNullOrEmpty(parameters.Filter))
                    parameters.Filter += $"ARSupport eq 1";
                else
                    parameters.Filter += $"and ARSupport eq 1";
            }

            AddMeasurementsToSearchParameters(filter.Measurements, parameters);

            parameters.QueryType = QueryType.Full;
            parameters.SearchMode = SearchMode.All;
            return parameters;
        }

        private void AddMeasurementsToSearchParameters(ProductMeasurements measurements, SearchParameters parameters)
        {
            if (measurements != null)
            {
                if (measurements.MaxHeight != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Height le {measurements.MaxHeight}";
                    else
                        parameters.Filter += $"and Height le {measurements.MaxHeight}";
                }

                if (measurements.MaxLength != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Length le {measurements.MaxLength}";
                    else
                        parameters.Filter += $"and Length le{measurements.MaxLength}";
                }

                if (measurements.MaxWidth != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Width le {measurements.MaxWidth}";
                    else
                        parameters.Filter += $"and Width le{measurements.MaxWidth}";
                }

                if (measurements.MinHeight != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Height ge {measurements.MinHeight}";
                    else
                        parameters.Filter += $"and Height ge {measurements.MinHeight}";
                }

                if (measurements.MinLength != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Length ge {measurements.MinLength}";
                    else
                        parameters.Filter += $"and Length ge{measurements.MinLength}";
                }

                if (measurements.MinWidth != null)
                {
                    if (string.IsNullOrEmpty(parameters.Filter))
                        parameters.Filter += $"Width ge {measurements.MinWidth}";
                    else
                        parameters.Filter += $"and Width ge{measurements.MinWidth}";
                }
            }
        }

        private int GetARType(ARPlatform platform, ARFormat format)
        {
            switch (platform)
            {
                case ARPlatform.Android when format == ARFormat.BFM:
                    return (int)ARModelType.AndroidBFM;
                case ARPlatform.Android when format == ARFormat.SFB:
                    return (int)ARModelType.AndroidSFB;
                case ARPlatform.IOS when format == ARFormat.BFM:
                    return (int)ARModelType.IosBFM;
                default:
                    return (int)ARModelType.IosSCN;
            }
        }

        public List<AzureProduct> GetById(string id)
        {
            var indexClient = CreateSearchIndexClient("products");
            var doc = indexClient.Documents.Get<AzureProduct>(id);
            return new List<AzureProduct>() { doc };
        }

        public DocumentSearchResult<AzureProduct> GetProductsByFilter(ProductsQueryFiltersValues filter,
            ARPlatform platform, ARFormat format, string indexName)
        {
            var indexClient = CreateSearchIndexClient(indexName);
            var formattedQuery = string.Empty;
            var parameters = GetSearchParametersByFilter(filter, platform, format);

            if (!string.IsNullOrEmpty(filter.SearchWords))
                formattedQuery = FormatStringQuery(filter.SearchWords, 1);

            return indexClient.Documents.Search<AzureProduct>(formattedQuery, parameters);
        }

        public ProductsQueryFilters GetQueryFilters(int categoryId, string indexName)
        {
            var queryFilters = new ProductsQueryFilters();
            var indexClient = CreateSearchIndexClient(indexName);
            var filter = new ProductsQueryFiltersValues { Count = 1 };

            if (categoryId != 0)
                filter.Category = categoryId;

            var parameters = GetSearchParametersByFilter(filter);

            parameters.Facets = new List<string> { "Rooms", "MarketplaceColors", "CompanyId" };
            queryFilters.Measurements = GetMeasurements(parameters, indexClient);

            parameters.OrderBy = new List<string> { "Price asc" };
            var resultMin = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (resultMin.Count == 0)
                queryFilters.MinPrice = 0;
            else
                queryFilters.MinPrice = resultMin.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Price desc" };
            var resultMax = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (resultMax.Count == 0)
                queryFilters.MaxPrice = 0;
            else
                queryFilters.MaxPrice = resultMax.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "ARSupport desc" };
            var resultAR = indexClient.Documents.Search<AzureProduct>("", parameters);

            queryFilters.Products = (int)resultMin.Count.Value;
            queryFilters.ContainsARProducts = resultAR.Results[0].Document.ARSupport > 0;

            var colorsIds = resultMin.Facets.Where(x => x.Key == "MarketplaceColors").SelectMany(x => x.Value)
                .Select(x => Convert.ToInt32(x.Value)).ToList();
            var roomsIds = resultMin.Facets.Where(x => x.Key == "Rooms").SelectMany(x => x.Value)
                .Select(x => Convert.ToInt32(x.Value)).ToList();
            var brandsIds = resultMin.Facets.Where(x => x.Key == "CompanyId").SelectMany(x => x.Value)
                .Select(x => Convert.ToInt32(x.Value)).ToList();

            queryFilters.Colors = GetColorsByIds(colorsIds);
            queryFilters.Rooms = GetRoomsByIds(roomsIds);
            queryFilters.Brands = GetBrandsByIds(brandsIds);

            return queryFilters;
        }

        private ProductMeasurements GetMeasurements(SearchParameters parameters, SearchIndexClient indexClient)
        {
            var result = new ProductMeasurements();

            parameters.OrderBy = new List<string> { "Width asc" };
            var widthMin = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (widthMin.Count == 0)
                result.MinWidth = 0;
            else
                result.MinWidth = widthMin.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Width desc" };
            var widthMax = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (widthMax.Count == 0)
                result.MaxWidth = 0;
            else
                result.MaxWidth = widthMax.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Height asc" };
            var heightMin = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (heightMin.Count == 0)
                result.MinHeight = 0;
            else
                result.MinHeight = heightMin.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Height desc" };
            var heightMax = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (heightMax.Count == 0)
                result.MaxHeight = 0;
            else
                result.MaxHeight = heightMax.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Length asc" };
            var lenghthMin = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (lenghthMin.Count == 0)
                result.MinLength = 0;
            else
                result.MinLength = lenghthMin.Results[0].Document.Price;

            parameters.OrderBy = new List<string> { "Length desc" };
            var lenghtMax = indexClient.Documents.Search<AzureProduct>("", parameters);
            if (lenghtMax.Count == 0)
                result.MaxLength = 0;
            else
                result.MaxLength = lenghtMax.Results[0].Document.Price;

            return result;
        }

        private List<int> GetChildIds(int categoryId)
        {
            return _context.MarketPlaceCategories.Where(x => x.ParentId == categoryId).Select(x => x.Id).ToList();
        }

        private List<ColorName> GetColorsByIds(List<int> colorsIds)
        {
            var colors = colorsIds.Join(_context.MarketplaceColors, x => x, y => y.Id, (x, y) => y)
                .Select(x => new ColorName
                {
                    Id = x.Id,
                    Name = x.Name,
                    Hex = x.Hex
                })
                .ToList();
            return colors;
        }

        private List<BrandName> GetBrandsByIds(List<int> brandsIds)
        {
            var brands = brandsIds.Join(_context.Companies, x => x, y => y.Id, (x, y) => y)
                .Select(x => new BrandName
                {
                    Id = x.Id,
                    Name = x.Name,
                })
                .ToList();
            return brands;
        }

        private List<RoomName> GetRoomsByIds(List<int> roomsIds)
        {
            var rooms = roomsIds.Join(_context.RoomNames, x => x, y => y.Id, (x, y) => y)
                .Select(x => new RoomName
                {
                    Id = x.Id,
                    Name = x.Name,
                })
                .ToList();
            return rooms;
        }
    }
}
