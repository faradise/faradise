﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.AzureSearch
{
    public class AzureFullTextSearchFilter
    {
        public int Count { get; set; }
        public int Offset { get; set; }
        public string Query { get; set; }
        public string IndexName { get; set; }
        public int Flexibility { get; set; }
    }
}
