﻿using Faradise.Design.Models.AzureSearch;
using Faradise.Design.Models.Marketplace;
using Microsoft.Azure.Search.Models;
using System.Collections.Generic;

namespace Faradise.Design.Services.AzureSearch
{
    public interface ISearchService
    {
        List<T> FullTextSearch<T>(AzureFullTextSearchFilter filter) where T:class;
        DocumentSearchResult<AzureProduct> GetProductsByFilter(ProductsQueryFiltersValues filter, ARPlatform platform, ARFormat format,string indexName);
        ProductsQueryFilters GetQueryFilters(int categoryId, string indexName);
        List<AzureProduct> GetById(string id);
    }
}