﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.AzureSearch;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Configs;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.AzureSearch
{
    public class IndexerService : IIndexerService, IDisposable
    {
        private static string _searchServiceName = string.Empty;
        private static string _searchServiceAdminApiKey = string.Empty;

        private DbContextOptions<FaradiseDBContext> _options;

        private Dictionary<int, int[]> _childCategories = new Dictionary<int, int[]>();

        private FaradiseDBContext _context;
        private readonly IPersistentStorageService _persistentStorageService;

        public IndexerService(IOptions<AzureSearchOptions> azureSearchOptions, IPersistentStorageService persistentStorageService, DbContextOptions<FaradiseDBContext> options)
        {
            _context = new FaradiseDBContext(options);
            _searchServiceName = azureSearchOptions.Value.SearchServiceName;
            _searchServiceAdminApiKey = azureSearchOptions.Value.SearchServiceAdminApiKey;

            if (!FaradiseEnvironment.Integration)
            {
                if (string.IsNullOrEmpty(_searchServiceAdminApiKey) || string.IsNullOrEmpty(_searchServiceName))
                    throw new ServiceException("Incorrect options", "no_azure_options");
            }
            
            _options = options;
            _persistentStorageService = persistentStorageService;
        }

        public async Task CreateIndex<T>(string name)
        {
            using (var searchService = new SearchServiceClient(
                searchServiceName: _searchServiceName,
                credentials: new SearchCredentials(_searchServiceAdminApiKey)))
            {

                var index = new Index(
                    name: name,
                    fields: FieldBuilder.BuildForType<T>());

                var exists = await searchService.Indexes.ExistsAsync(index.Name);
                if (exists)
                    await searchService.Indexes.DeleteAsync(index.Name);
                await searchService.Indexes.CreateAsync(index);
            }
        }
        
        public async Task<bool> CheckIndexExists(string index)
        {
            using (var searchService = new SearchServiceClient(
                searchServiceName: _searchServiceName,
                credentials: new SearchCredentials(_searchServiceAdminApiKey)))
            {
                return await searchService.Indexes.ExistsAsync(index);
            }
        }

        public async Task UploadProducts(string indexName)
        {
            var results = new List<DocumentIndexResult>();

            using (var indexClient = CreateSearchIndexClient(indexName))
            {
                var updateId = Guid.NewGuid().ToString();
                var products = await GetProductsToUpload(updateId);
                var step = 1000;
                var count = products.Length;

                for (var i = 0; i < count / step + 1; i++)
                {
                    var productsPart = products.Skip(step * i).Take(step);
                    if (productsPart.Any())
                    {
                        try
                        {
                            var result = UploadBatch(productsPart, indexClient);
                            results.Add(result);
                        }
                        catch (IndexBatchException e)
                        {
                            // Sometimes when your Search service is under load, indexing will fail for some of the documents in
                            // the batch. Depending on your application, you can take compensating actions like delaying and
                            // retrying. For this simple demo, we just log the failed document keys and continue.

                            Console.WriteLine(
                                "Failed to index some of the documents: {0}",
                                String.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));

                            await Task.Delay(TimeSpan.FromMilliseconds(100));
                            i--;
                        }
                    }
                }

                DeleteOldProducts(indexClient, updateId);

                var successCount = results.SelectMany(x => x.Results).Count(x => x.Succeeded);
                var unsuccessCount = results.SelectMany(x => x.Results).Count(x => !x.Succeeded);

                Console.WriteLine($"Успешно: {successCount}");
                Console.WriteLine($"Неуспешно: {unsuccessCount}");

                _childCategories = new Dictionary<int, int[]>();
            }
        }

        private async Task<AzureProduct[]> GetProductsToUpload(string updateId)
        {
            SetParentCategories();

            var productIds = await ExtractProductIds();
            
            var step = 200;
            var count = productIds.Count;
            var products = new List<AzureProduct>(productIds.Count);

            var batchUniqueId = Guid.NewGuid();

            await AddBatches(productIds, batchUniqueId);

            var errors = 0;

            for (var i = 0; i < count / step + 1; i++)
            {
                try
                {
                    var productsPart = await _context.Batches
                        .Where(z => z.BatchId == batchUniqueId)
                        .Skip(step * i)
                        .Take(step)
                        .Join(_context.Products, y => y.IdToQuery, x => x.Id, (y, x) => x)
                        .Select(x => new AzureProduct
                        {
                            Id = x.Id.ToString(),
                            Name = x.Name ?? string.Empty,
                            CompanyId = x.CompanyId,
                            CompanyCategoryId = x.CompanyCategoryId,
                            CompanyProductId = x.CompanyProductId ?? string.Empty,
                            MarketplaceCategory = _childCategories[x.CompanyCategory.CategoryId.Value],
                            IsRemoved = x.IsRemoved,
                            Description = x.Description ?? string.Empty,
                            Price = x.Price,
                            PreviousPrice = x.PreviousPrice,
                            ActualPrice = x.ActualPrice,
                            Rating = x.Rating,
                            Url = x.Url ?? string.Empty,
                            Available = x.Available,
                            Pickup = x.Pickup,
                            Delivery = x.Delivery,
                            DeliveryCost = x.DeliveryCost,
                            DeliveryDays = x.DeliveryDays ?? string.Empty,
                            BadgeId = x.BadgeId,
                            SaleId = x.SaleId,
                            Badge = x.Badge != null ? x.Badge.Picture ?? string.Empty : string.Empty,
                            BadgeDescription = x.Badge != null ? x.Badge.Description ?? string.Empty : string.Empty,
                            SaleDescription = x.Sale != null ? x.Sale.Description ?? string.Empty : string.Empty,
                            SalePercent = 100 - (x.PreviousPrice.HasValue && x.PreviousPrice > 0
                                              ? (x.Price * 100 / x.PreviousPrice.Value)
                                              : x.ActualPrice.HasValue && x.ActualPrice > 0
                                                  ? (x.Price * 100 / x.ActualPrice.Value)
                                                  : 100),

                            Vendor = x.Vendor ?? string.Empty,
                            VendorCode = x.VendorCode ?? string.Empty,
                            Warranty = x.Warranty,
                            Origin = x.Origin ?? string.Empty,
                            Size = x.Size ?? string.Empty,
                            Width = x.Width > 0 ? x.Width : null,
                            Height = x.Height > 0 ? x.Height : null,
                            Length = x.Length > 0 ? x.Height : null,

                            Pictures = x.Pictures
                                .OrderByDescending(y => y.Priority)
                                .Where(y => !string.IsNullOrEmpty(y.InternalUrl))
                                .Select(y => y.InternalUrl)
                                .ToArray(),

                            MarketplaceColors = x.ProductColors
                                .Where(y => y.CompanyColor.MarketplaceColorId.HasValue)
                                .Select(y => y.CompanyColor.MarketplaceColorId.Value)
                                .ToArray(),

                            ColorsNames = x.ProductColors
                                .Where(y => y.CompanyColor.MarketplaceColorId.HasValue)
                                .Select(y => y.CompanyColor.MarketplaceColor.Name ?? string.Empty)
                                .ToArray(),

                            JSONNotes = x.JSONNotes ?? string.Empty,

                            Rooms = x.CompanyCategory.Rooms
                                .Select(y => y.RoomNameId)
                                .ToArray(),

                            ARSupport = x.ARProduct != null ? 1 : 0,

                            ARFormat = x.ARProduct != null
                                ? GetARFormats(x.ARProduct.ARModels)
                                : new int[0],

                            LastUpdateId = updateId
                        })
                        .ToArrayAsync();

                    products.AddRange(productsPart);
                }
                catch
                {
                    i--;

                    errors++;
                    if (errors > 50)
                        throw;
                }
            }

            await _context.Database.ExecuteSqlCommandAsync("TRUNCATE TABLE [Batches]");
            await _context.SaveChangesAsync();

            _context.Dispose();
            _context = new FaradiseDBContext(_options);

            return products.ToArray();
        }

        private async Task AddBatches(List<int> productIds, Guid batchUniqueId)
        {
            var batches = productIds.Select(x => new BatchEntity
            {
                BatchId = batchUniqueId,
                IdToQuery = x
            });

            var step = 500;
            var count = batches.Count();

            for (var i = 0; i < count / step + 1; i++)
            {
                var part = batches
                    .Skip(i * step)
                    .Take(step);

                _context.Batches.AddRange(part);

                await _context.SaveChangesAsync();
            }
        }

        private async Task<List<int>> ExtractProductIds()
        {
            var count = await _context.Products
                .WhereSaleable()
                .CountAsync();

            var step = 1000;
            var ids = new List<int>(count);
            for (var i = 0; i < count / step + 1; i++)
            {
                var idstep = await _context.Products
                    .OrderBy(x => x.Id)
                    .WhereSaleable()
                    .Select(x => x.Id)
                    .Skip(i * step)
                    .Take(step)
                    .ToArrayAsync();

                ids.AddRange(idstep);
            }

            return ids;
        }

        private int[] GetARFormats(ICollection<ARModelEntity> aRModels)
        {
            var formats = new List<int>();
            if (aRModels.FirstOrDefault(x => x.Platform == ARPlatform.IOS && x.Format == ARFormat.BFM) != null)
                formats.Add((int) ARModelType.IosBFM);
            if (aRModels.FirstOrDefault(x => x.Platform == ARPlatform.IOS && x.Format == ARFormat.SCN) != null)
                formats.Add((int) ARModelType.IosSCN);
            if (aRModels.FirstOrDefault(x => x.Platform == ARPlatform.Android && x.Format == ARFormat.BFM) != null)
                formats.Add((int) ARModelType.AndroidBFM);
            if (aRModels.FirstOrDefault(x => x.Platform == ARPlatform.Android && x.Format == ARFormat.SFB) != null)
                formats.Add((int) ARModelType.AndroidSFB);
            return formats.ToArray();

        }

        private void SetParentCategories()
        {
            var categories = _context.MarketPlaceCategories.ToList();
            foreach (var category in categories)
            {
                var suitableCategories = GetSuitableCategories(categories, category.ParentId ?? 0);
                suitableCategories.Add(category.Id);
                _childCategories.Add(category.Id, suitableCategories.ToArray());
            }
        }

        private static List<int> GetSuitableCategories(List<MarketplaceCategoryEntity> categories, int parentId)
        {
            if (parentId == 0)
                return new List<int>();
            var suitableParentCategory = categories.FirstOrDefault(x => x.Id == parentId);

            var result = new List<int>();

            if (suitableParentCategory == null)
                return result;

            result.Add(suitableParentCategory.Id);
            var addResult = GetSuitableCategories(categories, suitableParentCategory.ParentId ?? 0);
            if (addResult.Any())
                result.AddRange(addResult);

            return result;
        }

        private static DocumentIndexResult UploadBatch(IEnumerable<AzureProduct> productsPart,
            ISearchIndexClient indexClient)
        {
            var actions = productsPart.Select(IndexAction.MergeOrUpload).ToArray();
            var batch = IndexBatch.New(actions);
            var result = indexClient.Documents.Index(batch);
            return result;
        }

        private void DeleteOldProducts(ISearchIndexClient indexClient, string uploadId)
        {
            var count = indexClient.Documents.Count();
            var step = 1000;
            var errors = 0;
            var results = new List<DocumentIndexResult>();

            for (var i = 0; i < count / step + 1; i++)
            {
                try
                {
                    var oldProducts = indexClient.Documents.Search<AzureProduct>("", new SearchParameters
                    {
                        Top = (int) step,
                        Skip = i * step,
                        OrderBy = new List<string> {"Price asc"},
                        Filter = $"LastUpdateId ne '{uploadId}'",
                    }).Results.Select(x => x.Document).ToList();

                    if (!oldProducts.Any())
                        break;

                    var actions = oldProducts.Select(x => IndexAction.Delete(x)).ToArray();
                    var batch = IndexBatch.New(actions);
                    var result = indexClient.Documents.Index(batch);

                    results.Add(result);
                }

                catch
                {
                    i--;

                    errors++;
                    if (errors > 50)
                        throw;
                }
            }

            Console.WriteLine($"Успешно:{results.Select(x => x.Results.Where(y => y.Succeeded)).Count()}");
        }
        
        public bool DeleteProduct(int productId)
        {
            using (var searchClient = CreateSearchIndexClient("products"))
            {
                var product = new AzureProduct {Id = productId.ToString()};
                var action = IndexAction.Delete(product);
                var batch = IndexBatch.New(new[] {action});
                var result = searchClient.Documents.Index(batch);
                return result.Results[0].Succeeded;
            }
        }

        private static SearchIndexClient CreateSearchIndexClient(string indexName)
        {
            var searchServiceName = _searchServiceName;
            var queryApiKey = _searchServiceAdminApiKey;

            var indexClient = new SearchIndexClient(searchServiceName, indexName, new SearchCredentials(queryApiKey));
            return indexClient;
        }

        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
        }
    }
}
