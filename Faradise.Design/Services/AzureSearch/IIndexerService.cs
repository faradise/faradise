﻿using System.Threading.Tasks;

namespace Faradise.Design.Services.AzureSearch
{
    public interface IIndexerService
    {
        Task CreateIndex<T>(string name);
        Task UploadProducts(string indexName);
        Task<bool> CheckIndexExists(string name);
    }
}