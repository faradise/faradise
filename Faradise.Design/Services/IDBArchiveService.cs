﻿using Faradise.Design.Models.ArchiveUpdate;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBArchiveService
    {
        void AddUpdateTask(ArchiveUpdateTask updateTask);
        ArchiveUpdateTask GetNextArchiveTask();
        void CompleteArchiveTask(int taskId);
    }
}
