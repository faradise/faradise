﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProductCompilation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBCompilationService
    {
        int CreateCompilation(CompilationSource source);
        bool UpdateCompilation(ProductCompilation compilation, CompilationSource source);
        void DeleteCompilation(int compilationId, CompilationSource source);
        ProductCompilationShort[] GetCompilations();
        ProductCompilation GetCompilation(int compilationId, CompilationSource source);
    }
}
