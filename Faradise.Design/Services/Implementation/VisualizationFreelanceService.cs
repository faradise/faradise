﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectPreparations;
using Faradise.Design.Models.VisualizationFreelance;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class VisualizationFreelanceService : IVisualizationFreelanceService
    {
        private readonly IDBVisualizationFreelanceService _visDb = null;
        private readonly IProjectDevelopmentService _devService = null;
        private readonly IProjectPreparationsService _prepService = null;
        private readonly IMarketplaceService _marketService = null;
        private readonly IMailSendingService _mailService = null;

        public VisualizationFreelanceService(IDBVisualizationFreelanceService visDb, IProjectPreparationsService prepService, IProjectDevelopmentService devService, IMarketplaceService marketService, IMailSendingService mailService)
        {
            _visDb = visDb;
            _devService = devService;
            _prepService = prepService;
            _marketService = marketService;
            _mailService = mailService;
        }

        public int AddFile(int projectId, int roomId, string fileLink, string filename)
        {
            return _visDb.AddFile(projectId, roomId, fileLink, filename);
        }

        public void ChangeDesigner(int projectId, int roomId, int designerId)
        {
            _visDb.ChangeDesigner(projectId, roomId, designerId);
        }

        public void CreateVisualizationTechSpec(int projectId, int roomId, int designerId)
        {
            _visDb.CreateVisualizationTechSpec(projectId, roomId, designerId);
        }

        public VisualisationTechSpec GetVisualizationTechSpec(int projectId, int roomId, int designerId)
        {
            if (!HasVisualizationTechSpec(projectId, roomId))
                CreateVisualizationTechSpec(projectId, roomId, designerId);
            return _visDb.GetVisualizationTechSpec(projectId, roomId);
        }

        public bool HasVisualizationTechSpec(int projectId, int roomId)
        {
            return _visDb.HasVisualizationTechSpec(projectId, roomId);
        }

        public void Publish(int projectId, int roomId, int designerId)
        {
            if (!HasVisualizationTechSpec(projectId, roomId))
                CreateVisualizationTechSpec(projectId, roomId, designerId);
            var products = _devService.GetMarketplaceGoods(roomId);
            if (products != null && products.MarketplaceItems != null)
            {
                var productIds = products.MarketplaceItems.Select(x => x.ProductId).Distinct().ToArray();
                _visDb.SetProducts(projectId, roomId, productIds);
            }
            var spec = GetVisualizationTechSpec(projectId, roomId, designerId);
            SendMailWithTechSpec(spec, projectId);
            _visDb.SetStatus(projectId, roomId, Models.VisualizationWorkStatus.Published);
        }

        private void SendMailWithTechSpec(VisualisationTechSpec spec, int projectId)
        {
            var room = _prepService.GetRooms(projectId).FirstOrDefault(x => x.RoomId == spec.RoomId);
            var styles = _prepService.GetStyles(projectId);
            var collages = _devService.GetCollages(spec.RoomId);
            var moodboard = _devService.GetMoodBoards(spec.RoomId);
            var zones = _devService.GetZones(spec.RoomId);
            var template = new VisualizationTechSpecTemplate()
            {
                 return_url = _mailService.AdminReturnUrl,
                projectid = projectId.ToString(),
                roomid = spec.RoomId.ToString(),
                purpose = room != null ? room.Purpose.Localize() : string.Empty,
                description = spec.Description,
                styles = styles != null ? styles.Select(x => x.Key.Localize()).ToArray() : new string[0],
                moodbaords = moodboard != null ? moodboard.Photos.Select(x => x.Link).ToArray() : new string[0],
                moodboardarchive = moodboard != null ? moodboard.ArchiveUrl : string.Empty,
                zones = zones != null ? zones.Photos.Select(x => x.Link).ToArray() : new string[0],
                zonearchive = zones != null ? zones.ArchiveUrl : string.Empty,
                collages = collages != null ? collages.Photos.Select(x => x.Link).ToArray() : new string[0],
                collagearchive = collages != null ? collages.ArchiveUrl : string.Empty,
                specfiles = spec.SpecFiles != null ? spec.SpecFiles.Select(x => new VisualizationTechSpecTemplate.SpecFile() { name = x.Name, link = x.Link }).ToArray() : null
            };
            if (room != null && room.Proportions != null)
            {
                template.roomproportions = new VisualizationTechSpecTemplate.Proportions()
                {
                    heightceiling = room.Proportions.HeightCeiling.ToString(),
                    heightdoor = room.Proportions.HeightDoor.ToString(),
                    sidea = room.Proportions.SideA.ToString(),
                    sideb = room.Proportions.SideB.ToString(),
                    widthdoor = room.Proportions.WidthDoor.ToString()
                };
            }
            if (spec.Products != null)
            {
                template.products = spec.Products.Select(x => new VisualizationTechSpecTemplate.Product() { id = x.ProductId.ToString(), name = x.ProductName.Replace("\"", string.Empty), outerlink = x.OuterUrl }).ToArray();
            }

            _mailService.SendEmail<VisualizationTechSpecTemplate>(new EmailModel()
            {
                TemplateModel = template,
                TemplateName = _mailService.VisualizationTechSpecPublishedTemplateId,
                ToAddress = new string[] { _mailService.AdminNotificationEmail },
                TemplateEngine = "velocity"
            });
        }

        public void RemoveFile(int projectId, int roomId, int fileId)
        {
            _visDb.RemoveFile(projectId, roomId, fileId);
        }

        public void SetDescription(int projectId, int roomId, string description)
        {
            _visDb.SetDescription(projectId, roomId, description);
        }

        public void Complete(int projectId, int roomId)
        {
            _visDb.SetStatus(projectId, roomId, VisualizationWorkStatus.Completed);
        }
    }
}
