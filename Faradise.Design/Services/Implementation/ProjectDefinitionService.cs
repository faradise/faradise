﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.ProjectDefinition;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class ProjectDefinitionService : IProjectDefinitionService
    {
        private readonly IDBProjectDefinitionService _dbService = null;
        private readonly IMailSendingService _mailService = null;
        private readonly IDesignerService _designerService = null;
        private readonly ISMSService _smsService = null;

        public ProjectDefinitionService(IDBProjectDefinitionService dbService, IDesignerService designerService, IMailSendingService mailService, ISMSService smsService)
        {
            _dbService = dbService;
            _mailService = mailService;
            _designerService = designerService;
            _smsService = smsService;
        }

        public void AssignDesigner(int projectId, int userId)
        {
            if (!_dbService.AssignDesigner(projectId, userId))
                return;
            var project = _dbService.GetProjectDefinition(projectId);
            var startStage = project.AbandonStage.HasValue ? project.AbandonStage.Value : ProjectStage.Preparetions;
            SetStage(projectId, startStage);

            var designer = _designerService.GetDesigner(userId);
            if (designer == null)
                return;
            if (!string.IsNullOrEmpty(designer.Base.Email))
            {
                _mailService.SendEmail<TemplateModel>(new EmailModel()
                {
                    TemplateModel = new TemplateModel() { return_url = _mailService.DesignerReturnUrl },
                    TemplateName = _mailService.DesignerAssignedTemplateId,
                    ToAddress = new string[] { designer.Base.Email }
                });
            }
            if (!string.IsNullOrEmpty(designer.Phone))
                _smsService.TrySendSMS(designer.Phone, _smsService.DesignerAssignedToNewProjectText);
        }

        public bool CheckUserOwnsService(int projectId, int userId)
        {
            return _dbService.CheckUserOwnsService(projectId, userId);
        }

        public int CreateProjectDefinition(int clientId)
        {
            return _dbService.CreateProjectDefinition(clientId);
        }

        public void AbandonDesigner(int projectId, string description)
        {
            var project = _dbService.GetProjectDefinition(projectId);
            var abandonedDesignerId = project.DesignerId;
            _dbService.DisbandDesigner(projectId, description);
            _dbService.SetAbandonStage(projectId, project.Stage);
            SetStage(projectId, ProjectStage.Description);

            var designer = _designerService.GetDesigner(abandonedDesignerId);
            _mailService.SendEmail<TemplateModel>(new MailSending.EmailModel()
            {
                ToAddress = new string[] { designer.Base.Email },
                TemplateName = _mailService.DesignerDeclinedByCleintTemplateId,
                TemplateModel = new TemplateModel() { return_url = _mailService.DesignerReturnUrl }
            });
        }

        public int GetDesigner(int projectId)
        {
            return _dbService.GetDesigner(projectId);
        }

        public ProjectDefinition GetProjectDefinition(int projectId)
        {
            return _dbService.GetProjectDefinition(projectId);
        }

        public ProjectDefinition[] GetProjectsByUser(int userId)
        {
            return _dbService.GetProjectsByUser(userId);
        }

        public void SetProjectCity(int projectId, string name)
        {
            _dbService.SetProjectCity(projectId, name);
        }

        public void SetProjectName(int projectId, string name)
        {
            _dbService.SetProjectName(projectId, name);
        }

        public void SetStage(int projectId, ProjectStage stage)
        {
            _dbService.SetStage(projectId, stage);
        }

        public void ConfirmDesigner(int projectId)
        {
            _dbService.ConfirmDesigner(projectId);
        }

        public void CloseProject(int projectId)
        {
            _dbService.CloseProject(projectId);
        }

        public void FinishFill(int projectId, User user)
        {
            var project = GetProjectDefinition(projectId);
            if (project.ProjectFill)
                return;


            _dbService.FinishFill(projectId);
            var emailTemplate = ClientIsReadyForDesignerPoolTemplate.BuildPoolIsLowForUser(project);
            emailTemplate.return_url = _mailService.AdminReturnUrl;
            _mailService.SendEmail<ClientIsReadyForDesignerPoolTemplate>(new MailSending.EmailModel()
            {
                ToAddress = new string[] { _mailService.AdminNotificationEmail },
                TemplateName = _mailService.ClientReadyForDesignerPoolTemplateId,
                TemplateModel = emailTemplate
            });
        }

        public void SetProjectNeedAdminAttention(int projectId, bool needAttention)
        {
            var definition = GetProjectDefinition(projectId);
            if (definition != null && definition.NeedAdmin != needAttention)
            {
                _dbService.SetProjectNeedAdminAttention(projectId, needAttention);
                if (needAttention)
                {
                    var template = new ProjectNeedAdminTemplate() { projectid = projectId.ToString(), return_url = _mailService.AdminReturnUrl };
                    _mailService.SendEmail<ProjectNeedAdminTemplate>(new EmailModel()
                    {
                        TemplateModel = template,
                        TemplateName = _mailService.ProjectNeedAdminTemplateId,
                        ToAddress = new string[] { _mailService.AdminNotificationEmail }
                    });
                }
            }
        }

        public void SetProjectAsViewedByAdmin(int projectId, bool isChanged)
        {
            _dbService.MarkProjectAsChangedForAdmin(projectId, isChanged);
        }
    }
}
