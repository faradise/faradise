﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.PromoCode;

namespace Faradise.Design.Services.Implementation
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IDBPromoCodeService _dbService = null;

        public PromoCodeService(IDBPromoCodeService dbService)
        {
            _dbService = dbService;
        }

        public PromoCodeApplyResult ApplyPromocodeToBasket(string code, int userId)
        {
            var promoCode = _dbService.GetPromoCode(code);
            var products = _dbService.GetProductsFromBasket(userId);
            var promoError = CheckPromoCode(promoCode, products.Where(x => x.SupportPromocode).Sum(p => p.PriceWithoutPromocode * p.Count));
            if (promoError != PromocodeError.None)
                return new PromoCodeApplyResult { Error = promoError };
            int absDiscount = 0;
            if (products != null)
            {
                foreach (var p in products)
                    p.PriceWithPromocode = p.SupportPromocode ? Math.Max((int)((1 - promoCode.PercentDiscount / 100.0f) * p.PriceWithoutPromocode), 0)
                        : p.PriceWithoutPromocode;
            }
            absDiscount = Math.Min(products.Where(x => x.SupportPromocode).ToArray().Sum(p => (p.PriceWithPromocode - 1) * p.Count), promoCode.AbsoluteDiscount);
            return new PromoCodeApplyResult
            {
                Error = PromocodeError.None,
                Products = products,
                StartDate = promoCode.StartDate,
                ExpireDate = promoCode.ExpireDate,
                PercentDiscount = promoCode.PercentDiscount,
                AbsoluteDiscount = absDiscount
            };
        }

        public PromoCodeApplyResult ApplyPromocodeToProducts(string code, ProductCount[] possibleProducts)
        {
            var promoCode = _dbService.GetPromoCode(code);
            var products = _dbService.GetProductsByIds(possibleProducts);
            var promoError = CheckPromoCode(promoCode, products.Where(x => x.SupportPromocode).Sum(p => p.PriceWithoutPromocode * p.Count));
            if (promoError != PromocodeError.None)
                return new PromoCodeApplyResult { Error = promoError };
            int absDiscount = 0;
            if (products != null)
            {
                foreach (var p in products)
                {
                    p.PriceWithPromocode = p.SupportPromocode ? Math.Max((int)((1 - promoCode.PercentDiscount / 100.0f) * p.PriceWithoutPromocode), 0) : p.PriceWithoutPromocode;
                }
            }
            absDiscount = Math.Min(products.Where(x => x.SupportPromocode).ToArray().Sum(p => p.PriceWithPromocode * p.Count), promoCode.AbsoluteDiscount);
            return new PromoCodeApplyResult
            {
                Error = PromocodeError.None,
                Products = products,
                StartDate = promoCode.StartDate,
                ExpireDate = promoCode.ExpireDate,
                PercentDiscount = promoCode.PercentDiscount,
                AbsoluteDiscount = absDiscount
            };
        }

        private int CalculateAbsoluteDiscount(int absoluteDiscount, ref PromocodeProduct[] products)
        {
            int sumDiscount = 0;
            var promoProducts = products.Where(x => x.SupportPromocode).OrderBy(x => x.PriceWithPromocode).ToArray();
            var allPrice = promoProducts.Sum(x => (x.PriceWithPromocode - 1) * x.Count);
            var maxDiscount = Math.Min(allPrice, absoluteDiscount);
            for (int p = 0; p < products.Length; p++)
            {
                if (!products[p].SupportPromocode)
                    continue;
                var weight = (products[p].PriceWithPromocode - 1) * products[p].Count / (float)allPrice;
                var absProductDiscount = (int) Math.Ceiling((weight * maxDiscount) / products[p].Count);
                products[p].PriceWithPromocode -= absProductDiscount;
                sumDiscount += absProductDiscount * products[p].Count;
            }
            if (sumDiscount > absoluteDiscount)
            {
                var prods = products.OrderBy(x => x.ProductId).OrderBy(x => x.Count).ToArray();
                foreach (var p in prods)
                {
                    var deltaPerOne = (sumDiscount - absoluteDiscount) / p.Count;
                    if (p.PriceWithPromocode - 1 > deltaPerOne)
                        p.PriceWithPromocode += deltaPerOne;
                    sumDiscount -= deltaPerOne * p.Count;
                    if (sumDiscount <= absoluteDiscount)
                        break;
                }
            }
            return sumDiscount;
        }

        public PromocodeError CheckPromocode(string code, int orderPrice)
        {
            var promoCode = _dbService.GetPromoCode(code);
            return CheckPromoCode(promoCode, orderPrice);
        }

        public void CreatePromoCodeExport(string name, int count, int minPrice, int percentsDiscount, int absoluteDiscount, PromoCodeType codeType, DateTime startDate, DateTime expireDate)
        {
            _dbService.CreatePromoCodeExport(name, count, minPrice, percentsDiscount, absoluteDiscount, codeType, startDate, expireDate);
        }

        public void DisablePromocode(string code)
        {
            _dbService.DisablePromocode(code);
        }

        public PromoCode GetPromoCode(string code)
        {
            return _dbService.GetPromoCode(code);
        }

        public PromoCodeExport GetPromoCodeExport(string exportname)
        {
            return _dbService.GetPromoCodeExport(exportname);
        }

        public PromoCodeExport[] GetPromoCodes()
        {
            return _dbService.GetPromoCodeExports();
        }

        public PromoCode[] GetPromoCodesForExport(string exportname)
        {
            return _dbService.GetPromoCodesForExport(exportname);
        }

        private PromocodeError CheckPromoCode(PromoCode promoCode, int orderPrice)
        {
            if (promoCode == null)
                return PromocodeError.NotFound;
            else if (promoCode.CodeType == PromoCodeType.Unique && promoCode.Used)
                return PromocodeError.Used;
            else if (promoCode.StartDate > DateTime.UtcNow)
                return PromocodeError.NotStarted;
            else if (promoCode.ExpireDate < DateTime.UtcNow)
                return PromocodeError.Expired;
            else if (orderPrice < 1)
                return PromocodeError.NoProductsForApply;
            else
                return promoCode.MinPrice > orderPrice ? PromocodeError.InsufficientPrice : PromocodeError.None;
        }
    }
}
