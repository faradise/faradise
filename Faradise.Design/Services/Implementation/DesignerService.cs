﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace Faradise.Design.Services.Implementation
{
    public class DesignerService : IDesignerService
    {
        private readonly IDBDesignerService _dBDesignerService = null;

        private readonly IMailSendingService _mailService = null;

        private readonly ISMSService _smsService = null;


        public DesignerService(IDBDesignerService dBDesignerService, IMailSendingService mailService, ISMSService smsService)
        {
            _dBDesignerService = dBDesignerService;
            _smsService = smsService;
            _mailService = mailService;
        }

        public bool CreateDesigner(int id)
        {
            var designer = new Designer { Id = id };
            return _dBDesignerService.CreateDesigner(designer.Id);
        }

        public Designer GetDesigner(int id)
        {
            var designer = _dBDesignerService.GetDesigner(id);
            return designer;
        }

        public DesignerBase GetBaseModel(int id)
        {
            var basic = _dBDesignerService.GetBaseModel(id);
            if (basic == null)
                throw new ServiceException($"Unable to load basic model from DB", "basicmodel_not_found");
            return basic;
        }
        
        public DesignerLegal GetLegalModel(int id)
        {
            var legal = _dBDesignerService.GetLegalModel(id);
            if (legal == null)
                throw new ServiceException($"Unable to load legal model from DB", "legalmodel_not_found");
            return legal;
        }

        public string GetPhotoLink(int id)
        {
            var photo = _dBDesignerService.GetPhotoLink(id);
            return photo;
        }

        public string GetPortfolioLink(int id)
        {
            
            var portfolio = _dBDesignerService.GetPortfolioLink(id);
            return portfolio;
        }

        public DesignerPossibility GetPossibilities(int id)
        {
            var possibilities = _dBDesignerService.GetPossibilities(id);
            if (possibilities == null)
                throw new ServiceException($"Unable to load possibilities from DB", "possibilities_not_found");
            return possibilities;
        }

        public DesignerStyle[] GetStyles(int id)
        {
            var styles = _dBDesignerService.GetStyles(id);
            //if (styles == null)
            //    throw new ServiceException($"Unable to load styles from DB", "styles_not_found");
            return styles;
        }

        public DesignerLevel GetLevel(int id)
        {
            var level = _dBDesignerService.GetTariff(id);
            return level;
        }

        public TestResults GetTest(int id)
        {
            var test = _dBDesignerService.GetTest(id);
            if (test == null)
                throw new ServiceException($"Unable to load test from DB", "test_not_found");
            return test;
        }

        public TestJobInfo GetTestJobInfo(int id)
        {
            var testInfo = _dBDesignerService.GetTestJobInfo(id);
            return testInfo;
        }

        public string GetPhone(int id)
        {
            return _dBDesignerService.GetPhone(id);
        }

        public void SetBaseModel(int id, DesignerBase baseModel)
        {
            _dBDesignerService.SetBaseModel(id, baseModel);
            AddBaseInfoToLegalInfo(id, baseModel.Name, baseModel.Surname, null);
        }

        public void SetLegalModel(int id, DesignerLegal legal)
        {
            _dBDesignerService.SetLegalModel(id, legal);
            if (_dBDesignerService.GetRegistrationStage(id) == DesignerRegistrationStage.TestApproved)
                _dBDesignerService.SetRegistrationStage(id, DesignerRegistrationStage.ReadyToWork);
        }

        public void SetPhotoLink(int id, string link)
        {
            _dBDesignerService.SetPhotoLink(id, link);
        }

        public void SetPortfolioLink(int id, string portfoliolink)
        {
            _dBDesignerService.SetPortfolioLink(id, portfoliolink);
        }

        public void SetPossibilities(int id, DesignerPossibility possibilities)
        {
            _dBDesignerService.SetPossibilities(id, possibilities);
        }

        public void SetStyles(int id, DesignerStyle[] styles)
        {
            _dBDesignerService.SetStyles(id, styles);
        }

        public void SetTariff(int id, DesignerLevel tariff)
        {
            _dBDesignerService.SetTariff(id, tariff);
        }

        public void SetTestApproved(int designerId, bool approved)
        {
            if (approved == true)
            {
                if (_dBDesignerService.GetRegistrationStage(designerId) == DesignerRegistrationStage.SendTestTask)
                {
                    _dBDesignerService.SetTestApproved(designerId, approved);
                    _dBDesignerService.SetRegistrationStage(designerId, DesignerRegistrationStage.TestApproved);
                    SendApprovedNotification(designerId);
                }
                else
                    throw new ServiceException("Registration stage not SendTestTask", "bad_registration_stage");
            }
            else
            {
                SendDeclinedNotification(designerId);
            }
        }

        private void SendApprovedNotification(int designerId)
        {
            var designer = _dBDesignerService.GetDesigner(designerId);
            if (designer == null)
                throw new ServiceException("Designer with id not found", "designer_not_found");

            _smsService.TrySendSMS(designer.Phone, _smsService.DesignerAprovedByAdminText);
            string templateId = string.Empty;
            switch (designer.Level)
            {
                case DesignerLevel.Novice:
                    templateId = _mailService.DesignerApprovedJunTemplateId;
                    break;
                case DesignerLevel.Specialist:
                    templateId = _mailService.DesignerApprovedMidTemplateId;
                    break;
                case DesignerLevel.Professional:
                    templateId = _mailService.DesignerApprovedProTemplateId;
                    break;
            }
            if (string.IsNullOrEmpty(templateId))
                return;
            _mailService.SendEmail<TemplateModel>(new EmailModel()
            {
                ToAddress = new string[] { designer.Base.Email },
                TemplateName = templateId,
                TemplateModel = new TemplateModel() { return_url = _mailService.DesignerReturnUrl }
            });
        }

        private void SendDeclinedNotification(int designerId)
        {
            var designer = _dBDesignerService.GetDesigner(designerId);
            if (designer == null)
                throw new ServiceException("Designer with id not found", "designer_not_found");

            _mailService.SendEmail<TemplateModel>(new EmailModel()
            {
                ToAddress = new string[] { designer.Base.Email },
                TemplateName = _mailService.DesignerDeclinedByAdminTemplateId,
                TemplateModel = new TemplateModel() { return_url = _mailService.DesignerReturnUrl }
            });
        }

        public void SetTest(int id, TestResults test)
        {
            _dBDesignerService.SetTest(id, test);
        }

        public void SetTestLink(int id, string testlink)
        {
            _dBDesignerService.SetTestLink(id, testlink);
            _dBDesignerService.SetRegistrationStage(id, DesignerRegistrationStage.SendTestTask);
        }

        public bool SetDesigner(int id, Designer model)
        {
            return _dBDesignerService.SetDesigner(id, model);
        }

        public void SetPhone(int id, string phone)
        {
            _dBDesignerService.SetPhone(id, phone);
            AddBaseInfoToLegalInfo(id, null, null, phone);
        }

        public void AddBaseInfoToLegalInfo(int id, string name, string surname, string phone)
        {
            _dBDesignerService.AddBaseInfoToLegalInfo(id, name, surname, phone);
        }

        public TestQuestionDescription[] GetDesignerQuestions()
        {
            var questions = _dBDesignerService.GetDesignerQuestions();
            if (questions == null || questions.Length == 0)
                throw new ServiceException($"Unable to load designer questions from DB", "no_questions_found");
            return questions;
        }

        public int CreatePortfolio(int id)
        {
            return _dBDesignerService.CreatePortfolioProject(id);
        }

        public Portfolio GetPortfolio(int id)
        {
            var portfolio = _dBDesignerService.GetPortfolio(id);
            if (portfolio == null)
                throw new ServiceException($"Unable to load portfolio from DB", "portfolio_not_found");
            return portfolio;
        }

        public void RemovePortfolio(int id, int portfolioId)
        {
            _dBDesignerService.RemovePortfolioProject(id, portfolioId);
        }

        public void SetPortfolio(int id, int portfolioId, string name, string description)
        {
            _dBDesignerService.SetPortfolio(id, portfolioId, name, description);
        }

        public void SetPortfolios(int id, PortfolioDescriptions model)
        {
            _dBDesignerService.SetPortfolios(id, model);
        }

        public void RemovePortfolioPhoto(int id, RemovePortfolioPhoto model)
        {
            _dBDesignerService.RemovePortfolioPhoto(id, model);
        }

        public int AddPortfolioPhoto(int id, int portfolioId, string path)
        {
            return _dBDesignerService.AddPortfolioPhoto(id, portfolioId, path);
        }

        public void AddAvatar(int id, string path)
        {
            _dBDesignerService.AddAvatar(id, path);
        }

        public void RemoveAvatar(int id)
        {
            _dBDesignerService.RemoveAvatar(id);
        }

        public Designer[] GetDesigners(int offset, int count)
        {
            return _dBDesignerService.GetDesigners(offset, count);
        }

        public DesignerRegistrationStage GetRegistrationStage(int id)
        {
            return _dBDesignerService.GetRegistrationStage(id);
        }

        public void DeleteDesigner(int designerId)
        {
            _dBDesignerService.DeleteDesigner(designerId);
        }

        public List<DesignerShortInfo> GetShortInfos()
        {
            return _dBDesignerService.GetDesignersShortInfos();
        }

        public int[] GetDesignerProjects(int designerId)
        {
            return _dBDesignerService.GetDesignerProjects(designerId);
        }
    }
}
