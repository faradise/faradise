﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Sale;

namespace Faradise.Design.Services.Implementation
{
    public class SaleService : ISaleService
    {
        private readonly IDBSaleService _dBSaleService;
        public SaleService(IDBSaleService dBSaleService)
        {
            _dBSaleService = dBSaleService;
        }

        public Sale CreateSale(string name)
        {
            return _dBSaleService.CreateSale(name);
        }

        public void DeleteSale(int saleId)
        {
            _dBSaleService.DeleteSale(saleId);
        }

        public Sale GetSale(int saleId)
        {
            return _dBSaleService.GetSale(saleId);
        }

        public SaleShortInfo[] GetSales()
        {
            return _dBSaleService.GetSales();
        }

        public void SetSaleActive(int saleId, bool isActive)
        {
            _dBSaleService.SetSaleActive(saleId, isActive);
        }

        public void UpdateSale(Sale sale)
        {
            _dBSaleService.UpdateSale(sale);
        }
    }
}
