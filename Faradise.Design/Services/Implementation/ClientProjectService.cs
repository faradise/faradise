﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Services.Implementation
{
    public class ClientProjectService : IClientProjectService
    {
        private readonly IDBClientProjectService _projectDb = null;
        private readonly IProjectDefinitionService _defService = null;

        public ClientProjectService(IDBClientProjectService projectDBService, IProjectDefinitionService defService)
        {
            _projectDb = projectDBService;
            _defService = defService;
        }

        public int CreateProjectDescription(int definitionId)
        {
            return _projectDb.CreateProjectDescription(definitionId);
        }

        public ProjectDescription GetProjectDescription(int projectId)
        {
            return _projectDb.GetProjectDescription(projectId);
        }

        public void SetDisignerRequirements(int projectId, DesignerRequirements designerRequirements)
        {
            var project = GetProjectDescription(projectId);
            if (project != null)
            {
                project.DesignerRequirements = designerRequirements;
                _projectDb.SaveProjectDescription(project);
            }
            else throw new ServiceException($"Unable to set project disigner description for projectId={projectId}", "project_not_found");
        }

        public void SetProjectBudget(int projectId, Budget budget)
        {
            var project = GetProjectDescription(projectId);
            if (project != null)
            {
                project.Budget = budget;
                _projectDb.SaveProjectDescription(project);
            }
            else throw new ServiceException($"Unable to set project budget for projectId={projectId}", "project_not_found");
        }

        public void SetProjectStyles(int projectId, StyleBinding[] styles)
        {
            var project = GetProjectDescription(projectId);
            if (project != null)
            {
                project.Styles = styles;
                _projectDb.SaveProjectDescription(project);
            }
            else throw new ServiceException($"Unable to set project styles for projectId={projectId}", "project_not_found");
        }

        public void SetProjectInformation(int projectId, ProjectTargetType objectType, Dictionary<Room, int> rooms, ReasonBinding[] reasons)
        {
            var project = GetProjectDescription(projectId);
            var roomList = new List<Room>();
            if (rooms != null)
            {
                foreach (var pair in rooms)
                    for (int r = 0; r < pair.Value; r++)
                        roomList.Add(pair.Key);
            }
            if (project != null)
            {
                project.Rooms = roomList;
                project.Reasons = reasons;
                project.ObjectType = objectType;
                _projectDb.SaveProjectDescription(project);
            }
            else throw new ServiceException($"Unable to set project info for projectId={projectId}", "project_not_found");
        }

        public TestQuestionDescription[] GetDesignerQuestions()
        {
            var questions = _projectDb.GetDesignerQuestions();
            if (questions == null || questions.Length == 0)
                throw new ServiceException($"Unable to load designer questions from DB", "questions_not_found");
            return questions;
        }

        public bool ValidationTest(TestResultsModel testing)
        {
            var testForm = GetDesignerQuestions();

            if(testing.Questions==null|| testing.Questions.Contains(null))
            {
                throw new ServiceException($"Empty value", "empty_value");
            }

            if (testing.Questions.Length != testForm.Length)
            {
                throw new ServiceException($"Wrong number of questions", "wrong_number_questions");
            }

            var validQuestionsIds = testForm.Select(x => x.Id).ToList();


            foreach (var answer in testing.Questions)
            {
                if (!validQuestionsIds.Contains(answer.Index))
                {
                    throw new ServiceException($"Non-existent question", "non_existent_question");
                }

                var validAnswersIds = testForm
                    .Where(x=>x.Id==answer.Index)
                    .FirstOrDefault()
                    .Answers
                    .Select(x=>x.Id)
                    .ToList();

                if (!validAnswersIds.Contains(answer.Answer))
                {
                    throw new ServiceException($"Non-existent answer", "non_existent_answer");
                }
            }

            return true;
        }
    }
}
