﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProductCompilation;

namespace Faradise.Design.Services.Implementation
{
    public class CompilationService : ICompilationService
    {
        private IDBCompilationService _dbCompilation;
        private IDBCompilationQueryService _queryCompilation;

        public CompilationService(IDBCompilationService dbCompilation, IDBCompilationQueryService queryCompilation)
        {
            _dbCompilation = dbCompilation;
            _queryCompilation = queryCompilation;
        }

        public int CreateCompilation(CompilationSource source)
        {
            return _dbCompilation.CreateCompilation(source);
        }

        public void DeleteCompilation(int compilationId, CompilationSource source)
        {
            _dbCompilation.DeleteCompilation(compilationId, source);
        }

        public ProductCompilation GetCompilation(int compilationId, CompilationSource source)
        {
            return _dbCompilation.GetCompilation(compilationId, source);
        }

        public ProductCompilationShort[] GetCompilations()
        {
            return _dbCompilation.GetCompilations();
        }

        public void UpdateCompilation(ProductCompilation compilation)
        {
            _dbCompilation.UpdateCompilation(compilation, compilation.Source);
        }
    }
}
