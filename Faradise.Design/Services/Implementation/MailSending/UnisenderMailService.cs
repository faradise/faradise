﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Emails.UniOne;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Faradise.Design.Models.Emails;
using Microsoft.Extensions.Logging;

namespace Faradise.Design.Services.Implementation.MailSending
{
    public class UnisenderMailService : IMailSendingService
    {
        private readonly string _uniOneSendUrl = string.Empty;
        private readonly string _uniOneApiKey = string.Empty;
        private readonly EmailOptions _config = null;
        private ILogger<UnisenderMailService> _logger = null;

        public string AdminNotificationEmail => _config.AdminNotificationEmail;

        public string DesignerPoolFinishedTemplateId => _config.DesignerPoolFinishedTemplateId;

        public string Stage3ClientGoesToNewStageTemplateId => _config.Stage3ClientGoesToNewStageTemplateId;

        public string DesignerDeclinedByAdminTemplateId => _config.DesignerDeclinedByAdminTemplateId;

        public string DesignerDeclinedByCleintTemplateId => _config.DesignerDeclinedByCleintTemplateId;

        public string DesignerPoolIsLowTemplateId => _config.DesignerPoolIsLowTemplateId;

        public string ClientReadyForDesignerPoolTemplateId => _config.ClientReadyForDesignerPoolTemplateId;

        public string VisualizationTechSpecPublishedTemplateId => _config.VisualizationTechSpecPublishedTemplateId;

        public string BrigadeTechSpecPublishedTemplateId => _config.BrigadeTechSpecPublishedTemplateId;

        public string NewProductOrderPublishedTemplateId => _config.NewProductOrderPublishedTemplateId;
        public string ProjectNeedAdminTemplateId => _config.ProjectNeedAdminTemplateId;
        public string DesignerAssignedTemplateId => _config.DesignerAssignedTemplateId;

        public string ClientReturnUrl => _config.ClientReturnUrl;
        public string DesignerReturnUrl => _config.DesignerReturnUrl;
        public string AdminReturnUrl => _config.AdminReturnUrl;

        public string ClientBasketOrderCreatedId => _config.ClientBasketOrderCreatedId;

        public string ClientBasketOrderPaymentLinkId => _config.ClientBasketOrderPaymentLinkId;

        public string ClientBasketOrderInWorkId => _config.ClientBasketOrderInWorkId;

        public string ClientBasketOrderCompletedId => _config.ClientBasketOrderCompletedId;

        public string BasketOrderNotificationEmail => _config.BasketOrderNotificationEmail;
        public string CustomerDevelopmentEmail => _config.CustomerDevelopmentEmail;

        public string AdminNewBasketOrderId => _config.AdminNewBasketOrderId;

        public string DesignerApprovedJunTemplateId => _config.DesignerApprovedJunTemplateId;
        public string DesignerApprovedMidTemplateId => _config.DesignerApprovedMidTemplateId;
        public string DesignerApprovedProTemplateId => _config.DesignerApprovedProTemplateId;

        public string CustomerDevelopmentTemplateId => _config.CustomerDevelopmentTemplateId;

        public string LotteryTemplate => _config.LotteryTemplate;

        public UnisenderMailService(IOptions<EmailOptions> options, ILogger<UnisenderMailService> logger)
        {
            _config = options.Value;
            _uniOneApiKey = _config.UniOneApiKey;
            _uniOneSendUrl = _config.UniOneSendUrl;
            _logger = logger;
        }

        public void SendMultipleEmails(EmailModel emails)
        {
            throw new NotImplementedException();
        }

        public async void SendEmail<T>(EmailModel email) where T: TemplateModel
        {
            if (email == null || email.ToAddress == null || email.ToAddress.Length < 1)
                throw new ServiceException("invalid email model", "email_error");
            using (var client = new HttpClient())
            {
                var uniOneJson = new UniOneMailModel<T>();
                uniOneJson.api_key = _uniOneApiKey;
                uniOneJson.username = _config.UniOneUserName;
                var message = new UniOneMessageModel<T>();
                message.template_id = email.TemplateName;
                if (!string.IsNullOrEmpty(email.TemplateEngine))
                    message.template_engine = email.TemplateEngine;
                message.recipients = new Recipients<T>[email.ToAddress.Length];
                for (int i = 0; i < email.ToAddress.Length; i++)
                {
                    message.recipients[i] = new Recipients<T>();
                    message.recipients[i].email = email.ToAddress[i];
                    message.recipients[i].substitutions = email.TemplateModel as T;
                }
                uniOneJson.message = message;
                var jsonString = JsonConvert.SerializeObject(uniOneJson);
                var response = await client.PostAsync(_uniOneSendUrl, new StringContent(jsonString, Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    string mails = string.Empty;
                    foreach (var m in message.recipients)
                        mails += " " + m;
                    _logger.LogError($"response http code {response.StatusCode} for mails {mails}", "unisender_error");
                }
                //Dictionary<string, string> htmlAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(response.Content.ReadAsStringAsync().Result);
                //if (htmlAttributes["status"] != "success")
                //    throw new ServiceException($"response status {htmlAttributes["status"]}", "email_error");
            }
        }

        /// <summary>
        /// Отправляет тестовое пиьсмо с параметрами, просто для проверки
        /// </summary>
        public void SendTestMail()
        {
            var testMail = new AdminNoDesignersTemplate();
            testMail.clientid = "Бутявка";
            testMail.leftinpool = "9000";
            testMail.projectid = "Домик";
            SendEmail<AdminNoDesignersTemplate>(new EmailModel()
            {
                ToAddress = new string[] { "zyndersan@gmail.com" },
                TemplateModel = testMail,
                TemplateName = "ed1b7796-b1c0-11e8-a22a-3a591afdfcc8"
            });
        }

        private Dictionary<string, string> GetEmailModelValue(TemplateModel model)
        {
            var result = new Dictionary<string, string>();
            Type modelType = model.GetType();
            var properties = modelType.GetProperties();

            foreach (var property in properties)
            {
                result.Add(property.Name, property.GetValue(model).ToString());
            };

            return result;
        }
    }
}
