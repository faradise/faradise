﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Faradise.Design.Services.Implementation.MailSending;
using System.Net;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Faradise.Design.Services.Configs;

namespace Faradise.Design.Services.Implementation
{
    public class MandrillMailSendingService : IMailSendingService
    {
        // данные мандрилы из конфига
        private string sendEmailApi= "https://mandrillapp.com/api/1.0/messages/send-template.json";
        private string mandrillaKey= "psRZ_6zbpLSJ3Wyjw9r7yg";

        public string AdminNotificationEmail => _options.AdminNotificationEmail;

        public string DesignerPoolFinishedTemplateId => _options.DesignerPoolFinishedTemplateId;

        public string Stage3ClientGoesToNewStageTemplateId => _options.Stage3ClientGoesToNewStageTemplateId;

        public string DesignerDeclinedByAdminTemplateId => _options.DesignerDeclinedByAdminTemplateId;

        public string DesignerDeclinedByCleintTemplateId => _options.DesignerDeclinedByCleintTemplateId;

        public string DesignerPoolIsLowTemplateId => _options.DesignerPoolIsLowTemplateId;

        public string ClientReadyForDesignerPoolTemplateId => _options.ClientReadyForDesignerPoolTemplateId;

        public string VisualizationTechSpecPublishedTemplateId => _options.VisualizationTechSpecPublishedTemplateId;

        public string BrigadeTechSpecPublishedTemplateId => _options.BrigadeTechSpecPublishedTemplateId;

        public string NewProductOrderPublishedTemplateId => _options.NewProductOrderPublishedTemplateId;
        public string ProjectNeedAdminTemplateId => _options.ProjectNeedAdminTemplateId;
        public string DesignerAssignedTemplateId => _options.DesignerAssignedTemplateId;

        public string ClientBasketOrderCreatedId => _options.ClientBasketOrderCreatedId;

        public string ClientBasketOrderPaymentLinkId => _options.ClientBasketOrderPaymentLinkId;

        public string ClientBasketOrderInWorkId => _options.ClientBasketOrderInWorkId;

        public string ClientBasketOrderCompletedId => _options.ClientBasketOrderCompletedId;


        public string ClientReturnUrl => _options.ClientReturnUrl;
        public string DesignerReturnUrl => _options.DesignerReturnUrl;
        public string AdminReturnUrl => _options.AdminReturnUrl;

        public string BasketOrderNotificationEmail => _options.AdminNotificationEmail;
        public string AdminNewBasketOrderId => _options.AdminNewBasketOrderId;
        public string CustomerDevelopmentEmail => _options.CustomerDevelopmentEmail;

        public string DesignerApprovedJunTemplateId => _options.DesignerApprovedJunTemplateId;
        public string DesignerApprovedMidTemplateId => _options.DesignerApprovedMidTemplateId;
        public string DesignerApprovedProTemplateId => _options.DesignerApprovedProTemplateId;

        public string CustomerDevelopmentTemplateId => _options.CustomerDevelopmentTemplateId;
        public string LotteryTemplate { get; }

        private readonly EmailOptions _options = null;

        public MandrillMailSendingService(IOptions<EmailOptions> options)
        {
            _options = options.Value;
        }

        public void SendEmail<T>(EmailModel email) where T : TemplateModel
        {
            var emailList = new EmailModel() { ToAddress = email.ToAddress, TemplateModel = email.TemplateModel, TemplateName = email.TemplateName };
            var message = PrepareMessage(emailList);
            var wc = new WebClient();
            var result = wc.UploadString(new Uri(sendEmailApi), message);
        }

        private string PrepareMessage(EmailModel email)
        {
            var message = new MandrillTemplateModel();
            message.key = mandrillaKey;
            message.template_name = email.TemplateName;
            message.message = new MandrillMessage()
            {
                from_email = "info@faradise.ru",
                from_name = "Faradise",
                to = email.ToAddress.Select(x => new MandrillAddress { email = x }).ToArray()
            };
            var properties = new List<MandrillTemplateStruct>(); ;
            var templateValues = GetEmailModelValue(email.TemplateModel);
            foreach(var entry in templateValues)
            {
                properties.Add(new MandrillTemplateStruct { name = entry.Key, content = entry.Value });
            }
            message.template_content = properties.ToArray();
            return JsonConvert.SerializeObject(message);

        }

        private Dictionary<string, string> GetEmailModelValue(TemplateModel model)
        {
            var result = new Dictionary<string, string>();
            Type modelType = model.GetType();
            var properties = modelType.GetProperties();
            
            foreach(var property in properties)
            {
                result.Add(property.Name, property.GetValue(model).ToString());
            };

            return result;
        }
    }
}
