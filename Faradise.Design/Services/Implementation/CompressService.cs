﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation
{
    public class CompressService : ICompressionService
    {
        class FileBinding
        {
            public string Name { get; set; }
            public byte[] Bytes { get; set; }
        }

        private readonly ICDNService _cdn = null;

        public CompressService(ICDNService cdn)
        {
            _cdn = cdn;
        }

        public async Task<string> CompressToZipAndUploadAsync(List<string> fileUrls, string uploadPath)
        {
            var downloadTasks = new Task<byte[]>[fileUrls.Count];
            for(int i = 0; i < downloadTasks.Length; i++)
                downloadTasks[i] = _cdn.DownloadBytes(fileUrls[i]);
            await Task.WhenAll(downloadTasks);
            var data = new List<FileBinding>(fileUrls.Count);
            for (int i = 0; i < downloadTasks.Length; i++)
            {
                if (downloadTasks[i].IsCompletedSuccessfully)
                {
                    var url = fileUrls[i];
                    data.Add(new FileBinding() { Name = fileUrls[i].Substring(url.LastIndexOf("/") + 1), Bytes = downloadTasks[i].Result });
                }
            }
            byte[] zipArray = null;
            using (MemoryStream zipStream = new MemoryStream())
            {
                using (ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Update, false))
                {
                    foreach (var item in data)
                    {
                        using (var dataStream = new MemoryStream(item.Bytes))
                        {
                            ZipArchiveEntry entry = zip.CreateEntry(item.Name);
                            using (Stream entryStream = entry.Open())
                            {
                                dataStream.CopyTo(entryStream);
                            }
                        }
                    }
                }
                zipArray = zipStream.ToArray();
            }
            var nameStart = uploadPath.LastIndexOf("/") + 1;
            var path = uploadPath.Remove(nameStart);
            var archiveName = uploadPath.Substring(nameStart).Replace(".zip", "");
            //await _cdn.DeleteFileAtPathAsync(_cdn.CreateUrl(uploadPath));
            var archiveUrl = await _cdn.UploadData(zipArray, path, archiveName, ".zip");
            //await _cdn.PurgeCacheAsync(archiveUrl);
            return archiveUrl;
        }
    }
}
