﻿using System;
using System.Text;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;

namespace Faradise.Design.Services.Toloka
{
    public class TolokaService : ITolokaService
    {
        private ICDNService _cdn = null;
        private IDBTolokaService _dbService = null;

        public TolokaService(ICDNService cdn, IDBTolokaService dbService)
        {
            _cdn = cdn;
            _dbService = dbService;
        }

        public TolokaTaskModel[] GetTasks()
        {
            return _dbService.GetTasks();
        }

        public TolokaTaskModel GetUnprocessedTask()
        {
            return _dbService.GetTaskByStatus(TolokaTaskStatus.Created);
        }

        public TolokaTaskSegmentModel GetUnprocessedSegment()
        {
            return _dbService.GetSegmentByStatus(TolokaTaskStatus.Created);
        }

        public void UpdateTaskStatus(int taskId, TolokaTaskStatus status)
        {
            _dbService.UpdateTaskStatus(taskId, status);
        }

        public void UpdateSegmentStatus(int segmentId, TolokaTaskStatus status)
        {
            _dbService.UpdateSegmentStatus(segmentId, status);
        }

        public async Task<bool> UploadTaskSegmentToCDN(string file, TolokaOutputType type, int TaskId)
        {
            if (string.IsNullOrEmpty(file))
                return true;

            var bytes = Encoding.UTF8.GetBytes(file);

            var dateCreate = DateTime.Now;
            var path = _cdn.GetPathForTolokaSegmentFile();
            var name = string.Format("{0}_{1}",
                type.ToString().ToLower(),
                dateCreate.ToString("o"));

            var url = await _cdn.UploadData(bytes, path, name, ".tsv");

            var segment = new TolokaTaskSegmentModel()
            {
                Url = url,
                Type = type,
                Status = TolokaTaskStatus.Created,
                TaskId = TaskId
            };

            _dbService.AddTaskSegment(segment);

            return true;
        }

        public async Task<bool> UploadTaskToCDN(UploadTSVModel model)
        {
            var dateCreate = DateTime.Now;
            var path = _cdn.GetPathForTolokaFile();
            var name = string.Format("{0}_{1}",
                model.Type.ToString().ToLower(),
                dateCreate.ToString("yyyy-MM-dd-hh:mm"));

            var url = await _cdn.UploadData(model.File, path, name);

            var task = new TolokaTaskModel()
            {
                Url = url,
                Created = dateCreate,
                Type = model.Type,
                Status = TolokaTaskStatus.Created
            };

            _dbService.AddTask(task);

            return true;
        }

        public async Task<string> LoadTaskFromCDN(string url)
        {
            return await _cdn.DownloadTextByUrl(url);
        }

        public async Task<bool> UploadTaskReportToCDN(string report, int taskId)
        {
            var task = _dbService.GetTaskById(taskId);

            if (!string.IsNullOrEmpty(task.ReportUrl))
            {
                var file = await _cdn.DownloadTextByUrl(task.ReportUrl);//LoadTaskReportFromCDN(taskId);

                await _cdn.DeleteFileAtPathAsync(task.ReportUrl); //DeleteTaskReportFromCDN(taskId);
                
                report = file + "\n" + report;
            }

            var path = _cdn.GetPathForTolokaReport();
            var name = string.Format("report_{0}", taskId);
            var bytes = Encoding.UTF8.GetBytes(report);

            var url = await _cdn.UploadData(bytes, path, name, ".tsv");

            _dbService.UpdateTaskReportUrl(taskId, url);

            return true;
        }

        public async Task<string> LoadTaskSegmentFromCDN(string url)
        {
            return await _cdn.DownloadTextByUrl(url);
        }

        public async Task<string> LoadTaskReportFromCDN(int TaskId)
        {
            var url = _cdn.CreateUrl(_cdn.GetPathForTolokaReport() + string.Format("report_{0}", TaskId) + ".tsv");
            return await _cdn.DownloadTextByUrl(url);
        }

        public async Task DeleteTaskReportFromCDN(int TaskId)
        {
            var url = _cdn.CreateUrl(_cdn.GetPathForTolokaReport() + string.Format("report_{0}", TaskId) + ".tsv");
            await _cdn.DeleteFileAtPathAsync(url);
        }

        public async Task ClearStatus()
        {
            _dbService.ClearStatus();
        }

        public TolokaTaskSegmentModel[] GetSegments(int taskId)
        {
            return _dbService.GetSegments(taskId);
        }

        public void DeleteTask(int taskId)
        {
            _dbService.DeleteTask(taskId);
        }

        public void RestartSegment(int segmentId)
        {
            _dbService.RestartSegment(segmentId);
        }
    }
}
