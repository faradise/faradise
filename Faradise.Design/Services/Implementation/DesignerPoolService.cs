﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DesignerPool;
using Faradise.Design.Models.Emails;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class DesignerPoolService : IDesignerPoolService
    {
        private readonly DesignerPoolOptions _poolOptions = null;
        private readonly IDBDesignerPoolService _dbPool = null;
        private readonly IDesignerService _designerService = null;
        private readonly IUserService _userService = null;
        private readonly IProjectDefinitionService _defService = null;
        private readonly IMailSendingService _mailService = null;
        private readonly ISMSService _smsService = null;

        public DesignerPoolService(IDBDesignerPoolService dbPool, IDesignerService designerService, IUserService userService, IProjectDefinitionService defService, IMailSendingService mailService, ISMSService smsService, IOptions<DesignerPoolOptions> poolOptions)
        {
            _dbPool = dbPool;
            _poolOptions = poolOptions.Value;
            _designerService = designerService;
            _userService = userService;
            _defService = defService;
            _mailService = mailService;
            _smsService = smsService;
        }

        public void AddDesignerToPool(int projectId, int userId)
        {
            var count = _dbPool.GetDesignerCount(projectId);
            if (count >= _poolOptions.MaxPoolSize)
                throw new ServiceException("Designer count reached limit", "designer_count_reached_limit");
            _dbPool.AddDesignerToPool(projectId, userId);
        }

        public DesignerDatingInfo[] GetDesignersPool(int projectId)
        {
            var designersIds = _dbPool.GetDesignerPool(projectId, _poolOptions.StackSize);
            if (designersIds == null)
                return new DesignerDatingInfo[0];
            var designerInfos = new List<DesignerDatingInfo>(designersIds.Length);
            foreach (var designerId in designersIds)
            {
                var designer = _designerService.GetDesigner(designerId);
                if (designer == null)
                    continue;
                var info = new DesignerDatingInfo()
                {
                    Id = designer.Id,
                    Name = designer.Base.Name + " " + designer.Base.Surname,
                    Description = designer.Base.About,
                    PortfolioLink = designer.PortfolioLink,
                    AvatarUrl = designer.PhotoLink,
                    Age = designer.Base != null ? designer.Base.Age : 0,
                    City = designer.Base != null ? designer.Base.City : string.Empty,
                    Gender = designer.Base != null ? designer.Base.Gender : Gender.Unknown,
                    PersonalСontrol = designer.Possibilities != null ? designer.Possibilities.PersonalСontrol : false
                };
                designerInfos.Add(info);
            }
            return designerInfos.ToArray();
        }

        public void RefreshDesignersPool(int projectId)
        {
            throw new NotImplementedException();
        }

        public void RemoveDesignerFromPool(int projectId, int userId, bool isRejected = true, string description = null)
        {
            _dbPool.RemoveDesignerFromPool(projectId, userId, isRejected, description);
            var left = _dbPool.GetDesignerCount(projectId);
            if (isRejected && left <= _poolOptions.NotificationBorder)
            {
                SendAdminAddDesignersNotifications(projectId, left);
            }
        }

        public void RemoveDesignersFromPool(int projectId, RemoveDesigner[] designers)
        {
            foreach (var designer in designers)
                _dbPool.RemoveDesignerFromPool(projectId, designer.DesignerId, true, designer.Description);
            var left = _dbPool.GetDesignerCount(projectId);
            if (left <= _poolOptions.NotificationBorder)
            {
                SendAdminAddDesignersNotifications(projectId, left);
            }
        }

        public int GetDesignersCount(int projectId)
        {
            return _dbPool.GetDesignerCount(projectId);
        }

        public void SetPoolIsFinished(int projectId)
        {
            var user = GetUserByProject(projectId);
            if (user == null)
                throw new ServiceException($"User owner for project id {projectId} not found ", "project__cleint_not_found");

            _smsService.TrySendSMS(user.Phone, _smsService.DesignerPoolFinishedText);

            _mailService.SendEmail<TemplateModel>(new MailSending.EmailModel()
            {
                ToAddress = new string[] { user.Email },
                TemplateModel = new TemplateModel() { return_url = _mailService.ClientReturnUrl },
                TemplateName = _mailService.DesignerPoolFinishedTemplateId
            });
        }

        private void SendAdminAddDesignersNotifications(int projectId, int leftInPool)
        {
            var project = _defService.GetProjectDefinition(projectId);
            if (project == null)
                throw new ServiceException($"Project with id {projectId} not found", "project_not_found");
            var user = _userService.ExtractUserById(project.ClientId);
            if (user == null)
                throw new ServiceException($"User with id {project.ClientId} not found", "user_not_found");

            _smsService.TrySendSMS(_smsService.AdminNotificationPhoneNumber, $"У пользователя {user.Name}, id {user.Id} нехватает дизайнеров на проекте {project.Name}");

            var emailTemplate = AdminNoDesignersTemplate.BuildPoolIsLowForUser(user, project, leftInPool);
            emailTemplate.return_url = _mailService.AdminReturnUrl;
            _mailService.SendEmail<AdminNoDesignersTemplate>(new EmailModel()
            {
                ToAddress = new string[] { _mailService.AdminNotificationEmail },
                TemplateModel = emailTemplate,
                TemplateName = _mailService.DesignerPoolIsLowTemplateId
            });
        }

        private User GetUserByProject(int projectId)
        {
            var project = _defService.GetProjectDefinition(projectId);
            if (project == null)
                throw new ServiceException($"Project with id {projectId} not found", "project_not_found");
            var user = _userService.ExtractUserById(project.ClientId);
            return user;
        }

        public DesignerInfo[] GetProjectSuitableDesigners(int projectId)
        {
            return _dbPool.GetProjectSuitableDesigners(projectId);
        }

        public DesignerRejectedInfo[] GetProjectRejectedDesigners(int projectId)
        {
            return _dbPool.GetProjectRejectedDesigners(projectId);
        }

        public DesignerInfo[] GetProjectAppointedDesigners(int projectId)
        {
            return _dbPool.GetProjectAppointedDesigners(projectId);
        }
    }
}
