﻿using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Faradise.Design.Services.Implementation
{
    public class PersistentStorageService : IPersistentStorageService
    {
        private IConnectionMultiplexer _multiplexer = null;
        private IDatabase _db = null;

        private readonly string _host = null;

        public bool? AzureSearchAvailable
        {
            get
            {
                if (Extract(nameof(AzureSearchAvailable), out bool val))
                    return val;
                return null;
            }
            set { Put(nameof(AzureSearchAvailable), value); }
        }

        public PersistentStorageService(IOptions<ServicesOptions> options)
        {
            _host = options.Value.Redis;

            _multiplexer = ConnectionMultiplexer.Connect(options.Value.Redis);
            _db = _multiplexer.GetDatabase();
        }

        public bool Extract<T>(string key, out T value)
        {
            try
            {
                var serializedResult = Get(key);
                if (serializedResult == null)
                {
                    value = default(T);
                    return false;
                }

                value = JsonConvert.DeserializeObject<T>(serializedResult);
                return true;
            }
            catch
            {
                value = default(T);
                return false;
            }
        }

        public bool Put<T>(string key, T value)
        {
            try
            {
                var serializedValue = JsonConvert.SerializeObject(value);
                return Save(key, serializedValue);
            }
            catch 
            {
                return false;
            }
        }

        private bool Save(string key, string value)
        {
            return _db.StringSet(key, value);
        }

        private string Get(string key)
        {
            var result = _db.StringGet(key);
            return result.ToString();
        }
    }
}
