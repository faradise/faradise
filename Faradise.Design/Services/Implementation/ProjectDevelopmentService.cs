﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectPreparations;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class ProjectDevelopmentService : IProjectDevelopmentService
    {
        private readonly IProjectDefinitionService _defService = null;
        private readonly IDBProjectDevelopmentService _prjDevelopmentDB = null;
        private readonly IUserService _userService = null;
        private readonly IMailSendingService _mailService = null;
        private IDBProjectOrderService _dBProjectOrderService = null;


        public ProjectDevelopmentService(IUserService userService, IProjectDefinitionService defService, IDBProjectDevelopmentService prjDevelopmentDB, IMailSendingService mailService, IDBProjectOrderService dBProjectOrderService)
        {
            _defService = defService;
            _prjDevelopmentDB = prjDevelopmentDB;
            _userService = userService;
            _mailService = mailService;
            _dBProjectOrderService = dBProjectOrderService;
        }

        public int AddCollagePhoto(int projectId, int roomId, string photoUrl)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Collage))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            return _prjDevelopmentDB.AddCollagePhoto(roomId, photoUrl);
        }

        public void AddMarketplaceGoods(RoomGoods goodsToAdd)
        {
            _prjDevelopmentDB.AddMarketplaceGoods(goodsToAdd);
        }

        public int AddMoodboardPhoto(int projectId, int roomId, string photoUrl)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Moodboard))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            return _prjDevelopmentDB.AddMoodboardPhoto(roomId, photoUrl);
        }

        public int AddPlanFile(int projectId, int roomId, string fileUrl)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Plan))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            return _prjDevelopmentDB.AddPlanFile(roomId, fileUrl);
        }

        public int AddZonePhoto(int projectId, int roomId, string photoUrl)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Zoning))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            return _prjDevelopmentDB.AddZonePhoto(roomId, photoUrl);
        }

        public void CreateProjectDevelopment(int projectId, List<RoomInfo> rooms)
        {
            _prjDevelopmentDB.CreateProjectDevelopment(projectId, rooms);
            _defService.SetStage(projectId, ProjectStage.Development);
        }

        public void DeleteCollagePhoto(int projectId, int photoId)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Collage))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            _prjDevelopmentDB.DeleteCollagePhoto(photoId);
        }

        public void DeleteMoodboardPhoto(int projectId, int photoId)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Moodboard))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            _prjDevelopmentDB.DeleteMoodboardPhoto(photoId);
        }

        public void DeletePlanFile(int projectId, int fileId)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Plan))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            _prjDevelopmentDB.DeletePlanFile(fileId);
        }

        public void DeleteZonePhoto(int projectId, int photoId)
        {
            if (!_prjDevelopmentDB.IsCurrentStage(projectId, DevelopmentStage.Zoning))
                throw new ServiceException($"Wrong stage", "wrong_stage");
            _prjDevelopmentDB.DeleteZonePhoto(photoId);
        }

        public Collage GetCollages(int roomId)
        {
            return _prjDevelopmentDB.GetCollages(roomId);
        }

        public DevelopmentStageStatus GetCurrentStageStatus(int projectId)
        {
            var stage = _prjDevelopmentDB.GetCurrentStageStatus(projectId);
            return GetStatus(projectId, stage);
        }

        public RoomGoods GetMarketplaceGoods(int roomId)
        {
            return _prjDevelopmentDB.GetMarketplaceGoods(roomId);
        }

        public Moodboard GetMoodBoards(int roomId)
        {
            return _prjDevelopmentDB.GetMoodBoards(roomId);
        }

        public Plan GetPlans(int roomId)
        {
            return _prjDevelopmentDB.GetPlans(roomId);
        }

        public ServicePriceInformation GetPlansPriceForRooms(int[] roomsIds)
        {
            ServicePriceInformation price = new ServicePriceInformation();
            foreach (var roomId in roomsIds)
            {
                var plan = _prjDevelopmentDB.GetPlans(roomId);
                if (plan.PaymentStatus.HasValue && plan.PaymentStatus != PaymentStatus.Canceled)
                    throw new ServiceException($"Plan for room {roomId} is already selected or have unfinished payment", "plan_can_not_be_slelected");
                price.SumAmount += plan.Price;
                price.Amount = plan.Price;
                price.Count++;
            }
            return price;
        }

        public RoomPurposeData[] GetRoomsShortInfos(int projectId)
        {
            return _prjDevelopmentDB.GetRoomsShortInfos(projectId);
        }

        public bool CheckStageHasContent(int projectId, DevelopmentStage stage)
        {
            switch (stage)
            {
                case DevelopmentStage.Moodboard:
                    return _prjDevelopmentDB.AllRoomsHasMoodboards(projectId);
                case DevelopmentStage.Collage:
                    return _prjDevelopmentDB.AllRoomsHasCollages(projectId);
                case DevelopmentStage.Zoning:
                    return _prjDevelopmentDB.AllRoomsHasZones(projectId);
                case DevelopmentStage.Visualization:
                    return _prjDevelopmentDB.AllSelectedRoomsHasVisuals(projectId);
                case DevelopmentStage.Plan:
                    return _prjDevelopmentDB.AllSelectedRoomsHasPlans(projectId);
            }
            return true;
        }

        public DevelopmentStageStatus GetStatus(int projectId, DevelopmentStage stage)
        {
            var hasUnfinishedWork = false;
            bool isPaid = true;
            bool needPayment = false;
            bool paymentFailed = false;
            if (stage == DevelopmentStage.Visualization)
            {
                var selectedVisuals = _prjDevelopmentDB.GetAllVisualizations(projectId);
                if (selectedVisuals.Any(x => x.IsSelected))
                {
                    hasUnfinishedWork = selectedVisuals.Any(x => x.IsSelected && !x.IsComplete);
                    isPaid = selectedVisuals.All(x => !x.IsSelected || (x.PaymentStatus.HasValue && x.PaymentStatus == PaymentStatus.Completed));
                    needPayment = selectedVisuals.Any(x => x.IsSelected && (!x.PaymentStatus.HasValue || x.PaymentStatus.Value == PaymentStatus.Canceled));
                    paymentFailed = selectedVisuals.Any(x => x.IsSelected && (x.PaymentStatus.HasValue && x.PaymentStatus.Value == PaymentStatus.Canceled));
                }
                else
                {
                    needPayment = true;
                    isPaid = false;
                }
            }
            else if (stage == DevelopmentStage.Plan)
            {
                var plans = _prjDevelopmentDB.GetAllPlans(projectId);
                if (plans.Any(x => x.IsSelected))
                {
                    hasUnfinishedWork = plans.Any(x => x.IsSelected && !x.IsComplete);
                    isPaid = plans.All(x => !x.IsSelected || (x.PaymentStatus.HasValue && x.PaymentStatus == PaymentStatus.Completed));
                    needPayment = plans.Any(x => x.IsSelected && (!x.PaymentStatus.HasValue || x.PaymentStatus.Value == PaymentStatus.Canceled));
                    paymentFailed = plans.Any(x => x.IsSelected && (x.PaymentStatus.HasValue && x.PaymentStatus.Value == PaymentStatus.Canceled));
                }
                else
                {
                    needPayment = true;
                    isPaid = false;
                }
            }
            var status = _prjDevelopmentDB.GetStatus(projectId, stage);
            status.HasUnfinishedWork = hasUnfinishedWork;
            status.IsPaid = isPaid;
            status.NeedPayment = needPayment;
            status.PaymentFailed = paymentFailed;
            return status;
        }

        public ServicePriceInformation GetVisualizationPriceForRooms(int[] roomsIds)
        {
            ServicePriceInformation price = new ServicePriceInformation();
            foreach (var roomId in roomsIds)
            {
                var visualization = _prjDevelopmentDB.GetVisualizations(roomId);
                if (visualization.PaymentStatus.HasValue && visualization.PaymentStatus != PaymentStatus.Canceled)
                    throw new ServiceException($"Visualization for room {roomId} is already selected or have unfinished payment", "visualization_can_not_be_slelected");
                price.SumAmount += visualization.Price;
                price.Amount = visualization.Price;
                price.Count++;
            }
            return price;
        }

        public Visualization GetVisualizations(int roomId)
        {
            return _prjDevelopmentDB.GetVisualizations(roomId);
        }

        public Zone GetZones(int roomId)
        {
            return _prjDevelopmentDB.GetZones(roomId);
        }

        public bool HasProjectDevelopment(int projectId)
        {
            return _prjDevelopmentDB.HasProjectDevelopment(projectId);
        }

        public void PayForOneOutsideItem(int roomId, string name, int count)
        {
            _prjDevelopmentDB.PayForOneOutsideItem(roomId, name, count);
        }

        public void RemoveMarketplaceGoods(RoomGoods goodsToRemove)
        {
            _prjDevelopmentDB.RemoveMarketplaceGoods(goodsToRemove);
        }

        public void SelectRoomPlans(int projectId, int[] roomIds, int paymentId)
        {
            foreach (var roomId in roomIds)
            {
                _prjDevelopmentDB.SetPlanAsSelected(roomId, paymentId);
            }
            _prjDevelopmentDB.UpdatePlansStageTime(projectId);
        }

        public void SelectRoomVisualization(int projectId, int[] roomIds, int paymentId)
        {
            foreach (var roomId in roomIds)
            {
                _prjDevelopmentDB.SetVisualizationAsSelected(roomId, paymentId);
            }
            _prjDevelopmentDB.UpdateVisualizationStageTime(projectId);
        }

        public void SetClientMadeDecision(int projectId, DevelopmentStage stage)
        {
            var currentStage = _prjDevelopmentDB.GetCurrentStageStatus(projectId);
            if (currentStage != stage)
                throw new ServiceException($"Project stage {stage} != current stage {currentStage}", "stage_is_not_current");
            _prjDevelopmentDB.SetClientMadeDecision(projectId, stage);
        }

        public void SetCollageArchive(int roomId, string url)
        {
            _prjDevelopmentDB.SetCollageArchive(roomId, url);
        }

        public void SetMoodboardArchive(int roomId, string url)
        {
            _prjDevelopmentDB.SetMoodboardArchive(roomId, url);
        }

        public void SetPlanArchive(int roomId, string url)
        {
            _prjDevelopmentDB.SetPlanArchive(roomId, url);
        }

        public void SetPlanAsComplete(int roomId)
        {
            _prjDevelopmentDB.SetPlanAsComplete(roomId);
        }

        public bool SetStageStatus(int projectId, DevelopmentStage stage, User user, bool isReadyForNextStage)
        {
            var currentStage = _prjDevelopmentDB.GetCurrentStageStatus(projectId);
            if (currentStage != stage)
                throw new ServiceException($"Project stage {stage} != current stage {currentStage}", "stage_is_not_current");
            var status = GetStatus(projectId, stage);
            if (status.HasUnfinishedWork)
                throw new ServiceException($"Project stage {stage} has unfinished work!", "stage_not_finished");
            if (user.Role.ContainsRole(Role.Client))
            {
                if (!status.DesignerIsReady)
                    throw new ServiceException($"Designer is not ready on stage {status.Stage}", "designer_is_not_ready");
                _prjDevelopmentDB.SetClientIsReady(projectId, status.Stage);
                GoToNextStage(projectId, currentStage, false, user);
                return true;
            } else if (user.Role.ContainsRole(Role.Designer))
            {
                if (!CheckStageHasContent(projectId, stage))
                    throw new ServiceException($"Project stage {stage} has unuploaded content", "stage_content_fill_error");
                _prjDevelopmentDB.SetDesignerIsReady(projectId, status.Stage);
                return false;
            }
            return false;
        }

        public void SetVisualizationAsComplete(int roomId)
        {
            _prjDevelopmentDB.SetVisualizationAsComplete(roomId);
        }

        public void SetVisualizationCode(int roomId, string visualizationCode)
        {
            _prjDevelopmentDB.SetVisualizationCode(roomId, visualizationCode);
        }

        public void SetZoneArchive(int roomId, string url)
        {
            _prjDevelopmentDB.SetZoneArchive(roomId, url);
        }

        public bool SkipStage(int projectId, User user, DevelopmentStage stage)
        {
            if (stage <= DevelopmentStage.Collage)
                throw new ServiceException($"You can skip stages only after collage stage", "early_skip_stage");
            var currentStage = _prjDevelopmentDB.GetCurrentStageStatus(projectId);
            if (currentStage != stage)
                throw new ServiceException($"Project stage {stage} != current stage {currentStage}", "stage_is_not_current");
            _prjDevelopmentDB.SetClientIsReady(projectId, stage);
            _prjDevelopmentDB.SetDesignerIsReady(projectId, stage);
            GoToNextStage(projectId, stage, true, user);
            return true;
        }

        private void GoToNextStage(int projectId, DevelopmentStage fromStage, bool skipped, User user)
        {
            _prjDevelopmentDB.GoToNextStage(projectId);
            if (skipped || fromStage > DevelopmentStage.Plan || !user.Role.ContainsRole(Role.Client))
                return;

            _mailService.SendEmail<TemplateModel>(new EmailModel()
            {
                TemplateModel = new TemplateModel() { return_url = _mailService.ClientReturnUrl },
                TemplateName = _mailService.Stage3ClientGoesToNewStageTemplateId,
                ToAddress = new string[] { user.Email }
            });
        }

        public ShortOrderInformation[] GetAllOrdersIdsForProject(int projectId)
        {
            return _dBProjectOrderService.GetAllOrdersIdsForProject(projectId);
        }

        public FullProjectProductsOrder GetOrderInformation(int orderId)
        {
            return _dBProjectOrderService.GetOrderInformation(orderId);
        }

        public void SetOrderAdditionalPrice(int orderId, int price)
        {
            _dBProjectOrderService.SetOrderAdditionalPrice(orderId, price);
        }

        public void IssueOrder(int projectId, OrderDescription description, ProductOrder[] orderedProducts, PaymentMethod method, int deliveryPrice)
        {
            var orderId = _dBProjectOrderService.CreateNewOrder(projectId, description, method, deliveryPrice);
            foreach (var product in orderedProducts)
            {
                _prjDevelopmentDB.AddProductsToOrder(product.RoomId, product.ProductId, product.Count, orderId);
            }
            _dBProjectOrderService.UpdateOrderBasePrice(orderId);
            var template = new NewProductOrderCreatedTemplate() { orderId = orderId.ToString(), projectid = projectId.ToString(), return_url = _mailService.AdminReturnUrl };
            _mailService.SendEmail<NewProductOrderCreatedTemplate>(new EmailModel()
            {
                TemplateModel = template,
                TemplateName = _mailService.NewProductOrderPublishedTemplateId,
                ToAddress = new string[] { _mailService.AdminNotificationEmail }
            });
        }

        public void SetOrderDeliveryPrice(int orderId, int price)
        {
            _dBProjectOrderService.SetOrderDeliveryPrice(orderId, price);
        }

        public void SetOrderStatus(int orderId, OrderStatus status)
        {
            _dBProjectOrderService.SetOrderStatus(orderId, status);
        }

        public void SetOrderPayment(int orderId, int paymentId)
        {
            _dBProjectOrderService.SetOrderPayment(orderId, paymentId);
        }
    }
}
