﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Emails;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class BasketService : IBasketService
    {
        private IDBBasketService _dbService = null;
        private IMailSendingService _mailService = null;
        private MarketplaceOptions _marketOptions = null;
        private ISMSService _smsService = null;
        private IPromoCodeService _promoCodeServive = null;

        public BasketService(IDBBasketService dbService, IMailSendingService mailService, IOptions<MarketplaceOptions> marketOptions, ISMSService smsService, IPromoCodeService promoCodeService)
        {
            _dbService = dbService;
            _mailService = mailService;
            _smsService = smsService;
            _marketOptions = marketOptions.Value;
            _promoCodeServive = promoCodeService;
        }

        public void AddProductToBasket(int userId, int productId, int count)
        {
            _dbService.AddProductToBasket(userId, productId, count);
        }

        public void AddProductsListToBasket(int userId, ProductCount[] products)
        {
            _dbService.AddProductsListToBasket(userId, products);
        }

        public void AssignPaymentToOrder(int orderId, Payment payment)
        {
            _dbService.AssignPaymentToOrder(orderId, payment);
            SetOrderStatus(orderId, OrderStatus.WaitingForPayment);
        }

        public int CreateOneClickOrder(int userId, int productId, int productCount, OneClickOrderDescription description, PaymentMethod method, int deliveryCost)
        {
            var orderId = _dbService.CreateOneClickOrder(userId, productId, productCount, description, method, deliveryCost);
            if (!string.IsNullOrEmpty(description.PromoCode))
                _promoCodeServive.DisablePromocode(description.PromoCode);
            RecalculateOrderBasePrice(orderId);
            SetOrderStatus(orderId, method != PaymentMethod.Tinkoff ? OrderStatus.Created : OrderStatus.WaitingForCreditApprove);
            return orderId;
        }

        public int CreateOrderFromBasket(int userId, OrderDescription description, PaymentMethod method, int deliveryCost)
        {
            var orderId = _dbService.CreateOrderFromBasket(userId, description, method, deliveryCost);
            if (!string.IsNullOrEmpty(description.PromoCode))
                _promoCodeServive.DisablePromocode(description.PromoCode);
            RecalculateOrderBasePrice(orderId);
            SetOrderStatus(orderId, method != PaymentMethod.Tinkoff ? OrderStatus.Created : OrderStatus.WaitingForCreditApprove);
            _dbService.ClearBasket(userId);
            return orderId;
        }

        public void FinishOrder(int orderId)
        {
            SetOrderStatus(orderId,OrderStatus.Completed);
        }
        
        public void CloseOrder(int orderId)
        {
            _dbService.CloseOrder(orderId);
        }

        public BasketProduct[] GetAllProductsInBasket(int userId)
        {
            return _dbService.GetAllProductsInBasket(userId);
        }

        public int GetOrderIdByPayment(int paymentId)
        {
            return _dbService.GetOrderIdByPayment(paymentId);
        }

        public FullBasketOrderInformation GetOrderInfo(int orderId)
        {
            return _dbService.GetOrderInfo(orderId);
        }

        public ShortOrderInformation[] GetOrders(int from, int count)
        {
            return _dbService.GetOrders(from, count);
        }

        public bool IsOrderOwnedByUser(int orderId, int userId)
        {
            return _dbService.IsOrderOwnedByUser(orderId, userId);
        }

        public int RecalculateOrderBasePrice(int orderId)
        {
            var orderInfo = GetOrderInfo(orderId);
            if (orderInfo == null)
                throw new ServiceException($"order {orderId} not found", "order_not_found");
            int price = 0;
            if (orderInfo.Products != null)
            {
                foreach (var product in orderInfo.Products)
                    price += product.Price * product.Count;
            }
            var basePrice = price - orderInfo.AbsoluteDiscount;
            _dbService.SetBasePrice(orderId, basePrice);
            return basePrice;
        }

        public void RemoveAllProductsFromBasket(int userId, int productId)
        {
            _dbService.RemoveAllProductsFromBasket(userId, productId);
        }

        public void RemoveProductFromBasket(int userId, int productId, int count)
        {
            _dbService.RemoveProductFromBasket(userId, productId, count);
        }

        public void RemoveProductsFromOrder(int orderId, int productId, int count)
        {
            _dbService.RemoveProductsFromOrder(orderId, productId, count);
            RecalculateOrderBasePrice(orderId);
        }

        public void AddProductToOrder(int orderId, int productId, int count)
        {
            _dbService.AddProductToOrder(orderId, productId, count);
            RecalculateOrderBasePrice(orderId);
        }

        public void SetOrderAdditionalPrice(int orderId, int price)
        {
            _dbService.SetAdditionalPrice(orderId, price);
        }

        public void SetOrderDeliveryPrice(int orderId, int price)
        {
            _dbService.SetDeliveryPrice(orderId, price);
        }

        public void SetOrderStatus(int orderId, OrderStatus status)
        {
            _dbService.SetOrderStatus(orderId, status);
            var fillInfo = _dbService.GetOrderInfo(orderId);
            if (fillInfo == null)
                throw new ServiceException("order not found", "order_not_found");
            if (fillInfo.Products == null || fillInfo.Products.Length < 1)
                throw new ServiceException("order have no products", "order_have_no_products");
            switch (status)
            {
                case OrderStatus.WaitingForCreditApprove:
                case OrderStatus.Created:
                    if (!string.IsNullOrEmpty(fillInfo.OrderDescription.Email))
                    {
                        var createdTemplate = new BasketOrderCreatedTemplate
                        {
                            clientname = fillInfo.OrderDescription.Name,
                            orderid = orderId.ToString(),
                            return_url = string.Empty,
                            sum = fillInfo.BasePrice.ToString(),
                            products = fillInfo.Products.Select(x => new BasketOrderCreatedTemplate.Product { name = x.Name.Replace("\"", string.Empty), price = x.Price.ToString(), count = x.Count.ToString() }).ToArray(),
                        };
                        if (fillInfo.AbsoluteDiscount > 0)
                        {
                            var pList = createdTemplate.products.ToList();
                            pList.Add(new BasketOrderCreatedTemplate.Product { name = "Скидка по промокоду", price = (-1 * fillInfo.AbsoluteDiscount).ToString(), count = string.Empty });
                            createdTemplate.products = pList.ToArray();
                        }
                        _mailService.SendEmail<BasketOrderCreatedTemplate>(new MailSending.EmailModel()
                        {
                            TemplateName = _mailService.ClientBasketOrderCreatedId,
                            TemplateModel = createdTemplate,
                            TemplateEngine = "velocity",
                            ToAddress = new string[] { fillInfo.OrderDescription.Email }
                        });
                    }
                    else
                    {
                        _smsService.TrySendSMS(fillInfo.OrderDescription.Phone, string.Format(_smsService.OneClickOrderText, fillInfo.OrderId));
                    }

                    var adminTemplate = new AdminNewBasketOrderTemplate
                    {
                        clientname = fillInfo.OrderDescription.Name,
                        orderid = orderId.ToString(),
                        return_url = string.Empty
                    };

                    _mailService.SendEmail<AdminNewBasketOrderTemplate>(new MailSending.EmailModel()
                    {
                        TemplateName = _mailService.AdminNewBasketOrderId,
                        TemplateModel = adminTemplate,
                        TemplateEngine = "velocity",
                        ToAddress = new string[] { _mailService.BasketOrderNotificationEmail }
                    });
                    break;
                case OrderStatus.WaitingForPayment:
                    if (string.IsNullOrEmpty(fillInfo.PlatformLink))
                        throw new ServiceException("payment link is empty", "payment_link_is_empty");
                    if (!string.IsNullOrEmpty(fillInfo.OrderDescription.Email))
                    {
                        var waitingForPaymentTemplate = new BasketOrderPaymentTemplate
                        {
                            clientname = fillInfo.OrderDescription.Name,
                            orderid = orderId.ToString(),
                            return_url = string.Empty,
                            sum = (fillInfo.BasePrice + fillInfo.DeliveryPrice + fillInfo.AdditionalPrice).ToString(),
                            deliverprice = fillInfo.DeliveryPrice.ToString(),
                            additionalprice = fillInfo.AdditionalPrice.ToString(),
                            paymentlink = fillInfo.PlatformLink,
                            products = fillInfo.Products.Select(x => new BasketOrderPaymentTemplate.Product { name = x.Name.Replace("\"", string.Empty), price = x.Price.ToString(), count = x.Count.ToString() }).ToArray(),
                        };
                        if (fillInfo.AbsoluteDiscount > 0)
                        {
                            var pList = waitingForPaymentTemplate.products.ToList();
                            pList.Add(new BasketOrderPaymentTemplate.Product { name = "Скидка по промокоду", price = (-1 * fillInfo.AbsoluteDiscount).ToString(), count = string.Empty });
                            waitingForPaymentTemplate.products = pList.ToArray();
                        }
                        _mailService.SendEmail<BasketOrderPaymentTemplate>(new MailSending.EmailModel()
                        {
                            TemplateName = _mailService.ClientBasketOrderPaymentLinkId,
                            TemplateModel = waitingForPaymentTemplate,
                            TemplateEngine = "velocity",
                            ToAddress = new string[] { fillInfo.OrderDescription.Email }
                        });
                    }
                    break;
                case OrderStatus.CreditApprovedInWork:
                case OrderStatus.InWork:
                    if (!string.IsNullOrEmpty(fillInfo.OrderDescription.Email))
                    {
                        var inworkTemplate = new BasketOrderInWorkTemplate
                        {
                            clientname = fillInfo.OrderDescription.Name,
                            orderid = fillInfo.OrderId.ToString(),
                            return_url = string.Empty,
                        };
                        _mailService.SendEmail<BasketOrderInWorkTemplate>(new MailSending.EmailModel()
                        {
                            TemplateName = _mailService.ClientBasketOrderInWorkId,
                            TemplateModel = inworkTemplate,
                            TemplateEngine = "velocity",
                            ToAddress = new string[] { fillInfo.OrderDescription.Email }
                        });
                    }
                    break;
                case OrderStatus.Completed:
                    if (!string.IsNullOrEmpty(fillInfo.OrderDescription.Email))
                    {
                        var completedTemplate = new BasketOrderIsCompleteTemplate
                        {
                            clientname = fillInfo.OrderDescription.Name,
                            orderid = fillInfo.OrderId.ToString(),
                            return_url = string.Empty
                        };
                        _mailService.SendEmail<BasketOrderIsCompleteTemplate>(new MailSending.EmailModel()
                        {
                            TemplateName = _mailService.ClientBasketOrderCompletedId,
                            TemplateModel = completedTemplate,
                            TemplateEngine = "velocity",
                            ToAddress = new string[] { fillInfo.OrderDescription.Email }
                        });
                    }
                    break;
            }
        }

        public void ClearBasket(int userId)
        {
            _dbService.ClearBasket(userId);
        }

        public void SetOrderIsReady(int orderId)
        {
            _dbService.SetOrderIsReady(orderId);
        }

        public void ClearPayment(int orderId)
        {
            _dbService.ClearPayment(orderId);
        }

        public void ChangeOrder(ChangeBasketOrder changes)
        {
            _dbService.ChangeOrder(changes);
        }

        public void SetBasketBin(int userId, ProductCount[] products)
        {
            _dbService.SetBasketBin(userId, products);
        }

        public int SaveBasket(ProductCount[] productsIds, int userId)
        {
            return _dbService.SaveBasket(productsIds, userId);
        }

        public BasketProduct[] GetAllProductsInSavedBasket(int basketId, int userId)
        {
            return _dbService.GetAllProductsInSavedBasket(basketId, userId);
        }

        public void AddProductToBasketFromSavedBasket(int user, int savedBasketId)
        {
            var products = _dbService.GetShortProductListFromSavedBasket(savedBasketId);
            foreach(var product in products)
            {
                AddProductToBasket(user, product.ProductId, product.Count);
            }
        }

        public SavedBasketShortInfo[] GetAllSavedBasketByUser(int userId)
        {
            return _dbService.GetAllSavedBasketsByUser(userId);
        }

        public void ChangePromoCodeForOrder(string promocode, int orderId)
        {
            _dbService.ChangePromoCodeForOrder(promocode, orderId);
            if (promocode != null)
                _promoCodeServive.DisablePromocode(promocode);
            RecalculateOrderBasePrice(orderId);
        }

        public void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod)
        {
            ClearPayment(orderId);
            _dbService.ChangePaymentMethod(orderId, paymentMethod);
        }


    }
}
