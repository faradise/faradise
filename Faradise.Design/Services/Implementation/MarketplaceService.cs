﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.YMLUpdater;
using Faradise.Design.Internal.Exceptions;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using System.IO;
using Faradise.Design.Marketplace;
using Faradise.Design.Marketplace.Xml;

namespace Faradise.Design.Services.Implementation
{
    public class MarketplaceService : IMarketplaceService
    {
        private readonly IDBMarketplaceService _dbMarketplaceService;
        private readonly IDBMarketplaceQueryService _dbQueryService;
        private readonly ICDNService _cdnService;

        public MarketplaceService(
            IDBMarketplaceService dbMarketplaceService, 
            ICDNService cdnService,
            IDBMarketplaceQueryService dbQueryService)
        {
            _dbQueryService = dbQueryService;
            _dbMarketplaceService = dbMarketplaceService;
            _cdnService = cdnService;
        }

        public List<Category> GetCategories()
        {
            return _dbMarketplaceService.GetCategories();
        }

        public List<Category> GetSubcategories(int id)
        {
            return _dbMarketplaceService.GetSubcategories(id);
        }

        public int AddCategory(Category category)
        {
            return _dbMarketplaceService.AddCategory(category);
        }

        public void EnableCategory(int categoryId)
        {
            _dbMarketplaceService.EnableCategory(categoryId);
        }

        public void DisableCategory(int categoryId)
        {
            _dbMarketplaceService.DisableCategory(categoryId);
        }

        public void DeleteCategory(int categoryId)
        {
            _dbMarketplaceService.DeleteCategory(categoryId);
        }

        public CompaniesModel GetAllCompanies(bool loadAdditinalInfo)
        {
            var companies = _dbMarketplaceService.GetAllCompanies(loadAdditinalInfo);
            return new CompaniesModel
            {
                Companies = companies,
                TotalCompanies = companies.Count,
                TotalProducts = companies.Sum(x => x.TotalProducts),
                MappedProducts = companies.Sum(x => x.MappedProducts),
            };
        }

        public CompanyModel GetCompanyById(int id)
        {
            var company = _dbMarketplaceService.GetCompanyById(id);
            return company.ToModel();
        }

        public void AddCompanyNote(int companyId, string note)
        {
            _dbMarketplaceService.AddCompanyNote(companyId, note);
        }

        public void DeleteCompanyNote(int companyId, int noteId)
        {
            _dbMarketplaceService.DeleteCompanyNote(companyId, noteId);
        }

        public void ChangeCompanyNote(int companyId, int noteId, string newnote)
        {
             _dbMarketplaceService.ChangeCompanyNote(companyId, noteId, newnote);
        }

        public bool EditCategory(ModifyCategoryModel category)
        {
           return _dbMarketplaceService.EditCategory(category.ToService());
        }

        public int CreateCompany()
        {
            return _dbMarketplaceService.CreateCompany();
        }

        public void EnableCompany(int companyId)
        {
            _dbMarketplaceService.EnableCompany(companyId);
        }

        public void DisableCompany(int companyId)
        {
            _dbMarketplaceService.DisableCompany(companyId);
        }

        public bool ModifyCompany(ModifyCompanyModel company)
        {
            var productionMask = CompanyProduction.Unknown;
            if (company.HasFurniture)
                productionMask |= CompanyProduction.Furniture;
            if (company.HasDesignFurniture)
                productionMask |= CompanyProduction.DesignFurniture;
            if (company.HasMaterials)
                productionMask |= CompanyProduction.Materials;
            if (company.HasElectric)
                productionMask |= CompanyProduction.Electronics;
            return _dbMarketplaceService.ModifyCompany(
                new Company
                {
                    Id = company.Id,
                    Name = company.Name,
                    Email = company.Email,
                    Url = company.Url,
                    CompanyProductionMask = productionMask,
                    ContactPersonEmail = company.ContactPersonEmail,
                    ContactPersonName = company.ContactPersonName,
                    ContactPersonPhone = company.ContactPersonPhone,
                    DeliveryInfo = company.DeliveryInfo,
                    LegalName = company.LegalName,
                    FeedUrl = company.FeedUrl,
                    DailyProductPriority = company.DailyProductPriority,
                    DontSupportPromocode = company.DontSupportPromocode
                });
        }

        public CompanyCategoriesModel GetCompanyCategories(int companyId)
        {
            return new CompanyCategoriesModel
            {
                CompanyId = companyId,
                Categories = _dbMarketplaceService
                    .GetCompanyCategories(companyId)
                    .Select(x => x.ToModel())
                    .ToList(),
            };
        }

        public CompanyCategoryModel GetCompanyCategory(int companyId, int companyCategoryId)
        {
            return _dbMarketplaceService.GetCompanyCategory(companyId, companyCategoryId).ToModel();
        }

        public bool MapCompanyCategories(CompanyCategoriesModel model)
        {
            var categories = model.Categories.Select(x => x.ToService(model.CompanyId)).ToList();
            return _dbMarketplaceService.MapCompanyCategories(categories, model.CompanyId);
        }

        public void UpdateCompany(MarketplaceCompany company)
        {
            var colors = company.Products
                .SelectMany(x => x.Color != null ? x.Color : new string[0])
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => x.Trim().ToLower())
                .Distinct();
            _dbMarketplaceService.UpdateCompanyColors(colors);
            var data = new UpdateCompanyData
            {
                Id = company.Id,
                Name = company.Name,
                Url = company.Url,
                Categories = company.Categories.Select(x => x.ToService(company.Id)).ToList(),
                Products = company.Products.Select(x => x.ToService(company.Id)).ToList()
            };
            _dbMarketplaceService.UpdateCompany(data);
        }

        public async Task ProcessYMLUploadForced(int taskId)
        {
            var updateTask = _dbMarketplaceService.GetYmlUpdateTask(taskId);
            if (updateTask != null)
            {
                MarkYMLUpdateTaskAsProcessing(updateTask.Id);
                try
                {
                    var link = updateTask.YMLLink;

                    var ymlBytes = await _cdnService.DownloadBytes(link);
                    if (ymlBytes == null || ymlBytes.Length == 0)
                        throw new ServiceException($"uml download failed for companyId {updateTask.CompanyId}", "yml_download_failed");

                    MarketplaceCompany company;
                    using (var stream = new MemoryStream(ymlBytes))
                    {
                        if (updateTask.FileType == UploadFiletype.YML)
                            company = MarketplaceParser.FromXml(stream);
                        else
                            company = XlsxMarketplaceParser.Parse(stream).company;
                    }

                    company.Id = updateTask.CompanyId;

                    UpdateCompany(company);
                    _dbMarketplaceService.MarkYMLUpdateTaskAsCompleted(updateTask.Id);
                }
                catch (Exception ex)
                {
                    MarkYMLUpdateTaskAsFailed(updateTask.Id, ex.ToString());
                }
            }
        }

        public Product GetProductById(int productId)
        {
            return _dbMarketplaceService.GetProductById(productId);
        }

        public int SaveCategoryImage(string url)
        {
            return _dbMarketplaceService.AddCategoryImage(url);
        }

        public Dictionary<int, string> GetCategoryImages()
        {
            return _dbMarketplaceService.GetCategoryImages();
        }

        public bool AddCategoryRoom(string roomName)
        {
            return _dbMarketplaceService.AddRoomName(roomName)!=0;
        }

        public bool EditCategoryRoom(RoomNameModel model)
        {
            return _dbMarketplaceService.EditRoomName(new RoomName { Id = model.RoomNameId, Name = model.Name });
        }

        public RoomNameModel[] GetCategoryRooms()
        {
            return _dbMarketplaceService.GetRoomNames().Select(x => new RoomNameModel { RoomNameId = x.Id, Name = x.Name }).ToArray();
        }

        public bool AddCompanyCategoryRoom(CompanyCategoryRoomModel model)
        {
            return _dbMarketplaceService.AddCompanyCategoryRoom(new CompanyCategoryRoom
            {
                CompanyId = model.CompanyId,
                CompanyCategoryId = model.CompanyCategoryId,
                RoomNameId = model.RoomNameId
            });
        }

        public bool DeleteCompanyCategoryRoom(CompanyCategoryRoomModel model)
        {
            return _dbMarketplaceService.DeleteCompanyCategoryRoom(new CompanyCategoryRoom
            {
                CompanyId = model.CompanyId,
                CompanyCategoryId = model.CompanyCategoryId,
                RoomNameId = model.RoomNameId
            });
        }

        public RoomNameModel[] GetRoomsInCompanyCategory(CompanyCategoryKeyModel companyCategoryKey)
        {
            return _dbMarketplaceService.GetCategoryRoomsByCompanyCategory(companyCategoryKey.CompanyId, companyCategoryKey.CompanyCategoryId)
                .Select(x => new RoomNameModel
                {
                    RoomNameId = x.Id,
                    Name = x.Name
                })
                .ToArray();
        }

        public bool AddMarketplaceColor(MarketplaceColorModel model)
        {
            return _dbMarketplaceService.AddMarketplaceColor(new MarketplaceColor { ColorId = model.ColorId, Name = model.Name, Hex = model.Hex }) != 0;
        }

        public bool EditMarketplaceColor(MarketplaceColorModel model)
        {
            return _dbMarketplaceService.EditMarketplaceColor(new MarketplaceColor { ColorId = model.ColorId, Name = model.Name, Hex=model.Hex });
        }

        public MarketplaceColorModel[] GetMarketplaceColors()
        {
            return _dbMarketplaceService.GetMarketplaceColors().Select(x => new MarketplaceColorModel { ColorId = x.ColorId, Name = x.Name, Hex=x.Hex }).ToArray(); ;
        }

        public List<CategoryInfo> GetCategoryPath(int categoryId)
        {
            return _dbMarketplaceService.GetCategoryPath(categoryId);
        }

        public bool AddAllCompanyCategoryRooms(CompanyCategoryKeyModel companyCategoryKey)
        {
            return _dbMarketplaceService.AddAllCompanyCategoryRooms(companyCategoryKey.CompanyId, companyCategoryKey.CompanyCategoryId);
        }

        public bool DeleteAllCompanyCategoryRooms(CompanyCategoryKeyModel companyCategoryKey)
        {
            return _dbMarketplaceService.DeleteAllCompanyCategoryRooms(companyCategoryKey.CompanyId, companyCategoryKey.CompanyCategoryId);
        }

        public int[] MapCompanyCategory(MapCategoryModel model)
        {
            return _dbMarketplaceService.MapCompanyCategory(new MapCategory
            {
                CompanyId = model.CompanyId,
                CompanyCategoryId = model.CompanyCategoryId,
                CategoryId = model.CategoryId
            });
        }

        public CompanyColorModel[] GetCompanyColors()
        {
            return _dbMarketplaceService.GetCompanyColors()
                .Select(x => new CompanyColorModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    MarketplaceColorId = x.MarketplaceColorId,
                    MarketplaceColorName = x.MarketplaceColorName
                }).ToArray();
        }

        public bool MapCompanyColor(MapColorModel model)
        {
            return _dbMarketplaceService.MapCompanyColor(model.CompanyColorId, model.MarketplaceColorId);
        }

        public int AddYMLUpdateTask(int companyId, string ymlLink, UploadFiletype filetype, int priority, YMLDownloadSource downloadSource)
        {
            return _dbMarketplaceService.AddYMLUpdateTask(companyId, ymlLink, filetype, priority, downloadSource);
        }

        public YMLUpdateTask GetNextYmlUpdateTask()
        {
            return _dbMarketplaceService.GetNextYmlUpdateTask();
        }

        public void DeleteYMLUpdateTask(int taskId)
        {
            _dbMarketplaceService.DeleteYMLUpdateTask(taskId);
        }

        public YMLUpdateTask[] GetAllPendingYMLUpdateTasks()
        {
            return _dbMarketplaceService.GetAllPendingYMLUpdateTasks();
        }

        public void MarkYMLUpdateTaskAsFailed(int taskId, string failMessage)
        {
            _dbMarketplaceService.MarkYMLUpdateTaskAsFailed(taskId, failMessage);
        }

        public void MarkYMLUpdateTaskAsProcessing(int taskId)
        {
            _dbMarketplaceService.MarkYMLUpdateTaskAsProcessing(taskId);
        }

        public List<OrderedProduct> GetProductsFromOrder(FullProjectProductsOrder order)
        {
            var products = new List<OrderedProduct>();
            foreach (var goods in order.Goods)
            {
                if (goods == null || goods.MarketplaceItems == null)
                    continue;
                foreach (var item in goods.MarketplaceItems)
                {
                    var product = _dbMarketplaceService.GetShortProductInfoById(item.ProductId);
                    products.Add(new OrderedProduct
                    {
                        Count = item.Count,
                        Id = product.Id,
                        Name = product.Name,
                        Price = product.Price,
                        Vendor = product.Vendor,
                        VendorCode = product.VendorCode,
                        VendorLink = product.VendorLink
                    });
                }
            }
            return products;
        }

        public List<OrderedProduct> GetProductsFromOrder(FullBasketOrderInformation order)
        {
            var products = new List<OrderedProduct>();
            foreach (var product in order.Products)
                products.Add(product);
            return products;
        }

        public void SetCompanyAutofeed(int companyId, bool isEnabled)
        {
            _dbMarketplaceService.SetCompanyAutofeedStatus(companyId, isEnabled);
        }

        public void MarkYMLUpdateTaskAsPending(int taskId)
        {
            _dbMarketplaceService.MarkYMLUpdateTaskAsPending(taskId);
        }

        public List<Product> GetProductsByVendorCode(string vendorCode)
        {
            return _dbMarketplaceService.GetProductsByVendorCode(vendorCode);
        }
    }
}
