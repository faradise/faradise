﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.ArchiveUpdate;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Services.Implementation
{
    public class ArchiveService : IArchiveService
    {
        private IDBArchiveService _dbService = null;

        public ArchiveService(IDBArchiveService dbService)
        {
            _dbService = dbService;
        }

        public void AddUpdateTask(int projectId, int roomId, RoomArchiveType updateType)
        {
            var updateTask = new ArchiveUpdateTask
            {
                ProjectId = projectId,
                RoomId = roomId,
                Type = updateType
            };
            _dbService.AddUpdateTask(updateTask);
        }

        public void CompleteArchiveTask(int taskId)
        {
            _dbService.CompleteArchiveTask(taskId);
        }

        public ArchiveUpdateTask GetNextArchiveTask()
        {
            return _dbService.GetNextArchiveTask();
        }
    }
}
