﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation
{
    public class ProjectChatService : IProjectChatService
    {
        private readonly IUserService _userService = null;
        private readonly IProjectDefinitionService _definitionService = null;
        private readonly ISMSService _smsService = null;
        private readonly IDBProjectChatService _dbChatService = null;

        public ProjectChatService(IDBProjectChatService dbChatService, IUserService userService, IProjectDefinitionService projectDefinitionService, ISMSService smsService)
        {
            _userService = userService;
            _definitionService = projectDefinitionService;
            _smsService = smsService;
            _dbChatService = dbChatService;
        }

        public bool CheckClientNotifyISActive(int projectId)
        {
            return _dbChatService.CheckClientNotifyISActive(projectId);
        }

        public bool CheckDesignerNotifyISActive(int projectId)
        {
            return _dbChatService.CheckDesignerNotifyISActive(projectId);
        }

        public void NotifyAboutNewMessage(int projectId, int[] userIdToNotify, bool[] isFirstMessageForUser)
        {
            var project = _definitionService.GetProjectDefinition(projectId);
            if (project == null)
                throw new ServiceException($"Project not found by Id {projectId}", "sms_send_failed");
            for (int i = 0; i < userIdToNotify.Length; i++)
            {
                var userId = userIdToNotify[i];
                var isFirstNotification = isFirstMessageForUser[i];
                var user = _userService.ExtractUserById(userId);
                if (user == null)
                    continue;
                string message = string.Empty;
                if (project.ClientId == userId)
                {
                    if (!CheckClientNotifyISActive(projectId))
                        continue;
                    message = isFirstNotification ? _smsService.NewDesignerMessageInChatText : _smsService.FirstDesignerMessageInChatText;
                }
                else if (project.DesignerId == userId)
                {
                    if (!CheckDesignerNotifyISActive(projectId))
                        continue;
                    message = _smsService.NewClientMessageInChatText;
                }
                if (!string.IsNullOrEmpty(message))
                    _smsService.TrySendSMS(user.Phone, message);
            }
        }

        public void SetClientNotificationIsActive(int projectId, bool isActive)
        {
            _dbChatService.SetClientNotificationIsActive(projectId, isActive);
        }

        public void SetDesignerNotificationIsActive(int projectId, bool isActive)
        {
            _dbChatService.SetDesignerNotificationIsActive(projectId, isActive);
        }


    }
}
