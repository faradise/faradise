﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.Brigade;
using Faradise.Design.Models.Emails;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services
{
    public class BrigadeService : IBrigadeService
    {
        private readonly IDBBrigadeService _dbService = null;
        private readonly IProjectDevelopmentService _devService = null;
        private readonly IMailSendingService _mailService = null;

        public BrigadeService(IDBBrigadeService dbService, IProjectDevelopmentService devService, IMailSendingService mailService)
        {
            _dbService = dbService;
            _devService = devService;
            _mailService = mailService;
        }

        public int AddFile(int projectId, string fileLink, string filename)
        {
            return _dbService.AddFile(projectId, fileLink, filename);
        }

        public void CreateTechSpec(int projectId,int designerId)
        {
            _dbService.CreateTechSpec(projectId, designerId);
        }

        public BrigadeTechSpec GetBrigadeTechSpec(int projectId, int designerId)
        {
            if (!HasTechSpec(projectId))
                CreateTechSpec(projectId, designerId);
            return _dbService.GetBrigadeTechSpec(projectId);
        }

        public bool HasTechSpec(int projectId)
        {
            return _dbService.HasTechSpec(projectId);
        }

        public void Publish(int projectId, int designerId)
        {
            var specs = GetBrigadeTechSpec(projectId, designerId);
            if (specs.Status <= BrigadeWorkStatus.Published)
                return;
            var roomInfos = ExtractBrigadeRoomInfos(projectId);
            specs = GetBrigadeTechSpec(projectId, designerId);
            SendMailWithTechSpec(specs);
            _dbService.SetStatus(projectId, BrigadeWorkStatus.Published);
        }

        private void SendMailWithTechSpec(BrigadeTechSpec spec)
        {
            var template = new BrigadeTechSpecTemplate()
            {
                 return_url = _mailService.AdminReturnUrl,
                projectid = spec.ProjectId.ToString(),
                description = spec.Description,
                specfiles = spec.SpecFiles != null ? spec.SpecFiles.Select(x => new BrigadeTechSpecTemplate.SpecFile() { name = x.Name, link = x.Link }).ToArray() : null
            };
            if (spec.RoomInfos != null)
            {
                template.rooms = new BrigadeTechSpecTemplate.Room[spec.RoomInfos.Length];
                for (int r = 0; r < spec.RoomInfos.Length; r++)
                {
                    template.rooms[r] = new BrigadeTechSpecTemplate.Room()
                    {
                        zones = spec.RoomInfos[r].Zones,
                        zonearchive = spec.RoomInfos[r].ZoneArchive,
                        collages = spec.RoomInfos[r].Collages,
                        collagearchive = spec.RoomInfos[r].CollageArchive,
                        plans = spec.RoomInfos[r].WorkPlans,
                        planarchive = spec.RoomInfos[r].WorkPlansArchive
                    };
                }
            }
            _mailService.SendEmail<VisualizationTechSpecTemplate>(new EmailModel()
            {
                TemplateModel = template,
                TemplateName = _mailService.BrigadeTechSpecPublishedTemplateId,
                ToAddress = new string[] { _mailService.AdminNotificationEmail },
                TemplateEngine = "velocity"
            });
        }

        private BrigadeRoomInfo[] ExtractBrigadeRoomInfos(int projectId)
        {
            var rooms = _devService.GetRoomsShortInfos(projectId);
            var brigadeRoomInfos = new List<BrigadeRoomInfo>();
            if (rooms != null)
            {
                foreach (var room in rooms)
                {
                    var info = new BrigadeRoomInfo() { Purpose = room.Purpose };
                    var zones = _devService.GetZones(room.RoomId);
                    if (zones != null && zones.Photos != null)
                    {
                        info.Zones = zones.Photos.Select(x => x.Link).ToArray();
                        info.ZoneArchive = zones.ArchiveUrl;
                    }
                    var collages = _devService.GetCollages(room.RoomId);
                    if (collages != null && collages.Photos != null)
                    {
                        info.Collages = collages.Photos.Select(x => x.Link).ToArray();
                        info.CollageArchive = collages.ArchiveUrl;
                    }
                    var workPlans = _devService.GetPlans(room.RoomId);
                    if (workPlans != null && workPlans.Files != null)
                    {
                        info.WorkPlans = workPlans.Files.Select(x => x.Link).ToArray();
                        info.WorkPlansArchive = workPlans.ArchiveUrl;
                    }
                    brigadeRoomInfos.Add(info);
                }
            }
            return brigadeRoomInfos.ToArray();
        }

        public void RemoveFile(int projectId, int fileId)
        {
            _dbService.RemoveFile(projectId, fileId);
        }

        public void SetDescription(int projectId, string description)
        {
            _dbService.SetDescription(projectId, description);
        }

        public void Complete(int projectId)
        {
            _dbService.SetStatus(projectId, BrigadeWorkStatus.Completed);
        }
    }
}
