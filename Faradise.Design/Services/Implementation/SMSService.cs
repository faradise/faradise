﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation
{
    public class SMSService : ISMSService
    {
        public string TestPhoneNumber => _options.TestPhoneNumber;
        public string AdminNotificationPhoneNumber => _options.AdminNotificationPhoneNumber;
        public string DesignerPoolFinishedText => _options.DesignerPoolFinishedText;
        public string NewDesignerMessageInChatText => _options.NewDesignerMessageInChatText;
        public string FirstDesignerMessageInChatText => _options.FirstDesignerMessageInChatText;
        public string DesignerAprovedByAdminText => _options.DesignerAprovedByAdminText;
        public string NewClientMessageInChatText => _options.NewClientMessageInChatText;
        public string DesignerAssignedToNewProjectText => _options.DesignerAssignedToNewProjectText;
        public string OneClickOrderText => _options.OneClickOrderText;

        private readonly SMSOptions _options = null;
        private readonly ILogger<SMSService> _logger = null;

        public SMSService(IOptions<SMSOptions> options, ILogger<SMSService> logger)
        {
            _options = options.Value;
            _logger = logger;
        }

        public string TrySendSMS(string phone, string message)
        {
            if (string.IsNullOrEmpty(phone) || string.IsNullOrEmpty(message))
                return "Message or phoe is empty";
            var path = _options.Host + "?login=" + _options.User + "&psw=" + _options.Password + "&phones=" + phone + "&charset=utf-8&mes=" + message;
            var wc = new WebClient();
            var response = wc.DownloadString(path);
            //var response = JsonConvert.DeserializeObject<SendSMSResponse>(str);
            if (!response.Contains("OK"))
                _logger.LogError($"sms not send to phone {phone}, response status code {response}");
            return response;
        }

        private class SendSMSResponse
        {
            [JsonProperty("statusCode")]
            public string StatusCode { get; set; }
        }
    }
}
