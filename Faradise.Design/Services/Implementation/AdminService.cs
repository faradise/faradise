﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProductCollection;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation.MailSending;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Implementation
{
    public class AdminService : IAdminService
    {
        private IDesignerPoolService _designerPoolService = null;
        private IDesignerService _designerService = null;
        private IDBProjectDefinitionService _dbProjectDefinitionService = null;
        private IDBProjectDevelopmentService _dBProjectDevelopmentService = null;
        private IDBProjectFlowService _dbProjectPreparationService = null;
        private IDBClientProjectService _dbProjectDescriptionService = null;
        private IPaymentService _paymentService = null;
        private IDBMarketplaceService _dbMarketplaceService = null;
        private IDBUserService _userService = null;
        private IBasketService _basketService = null;
        private IDBAdminMarketplaceService _dbAdminMarketplaceService = null;
        private IDBProjectOrderService _dBProjectOrderService = null;
        private MarketplaceOptions _marketOption = null;
        private IMarketplaceService _marketService = null;
        private IProductCollectionService _productCollectionService = null;

        public AdminService(
            IDesignerPoolService designerPoolService, 
            IDesignerService designerService, 
            IDBProjectDefinitionService dbProjectDefinitionService, 
            IDBProjectDevelopmentService dBProjectDevelopmentService,
            IDBProjectOrderService dBProjectOrderService,
            IDBProjectFlowService dbProjectPreparationsService, 
            IDBClientProjectService dbDescriptionService,
            IPaymentService paymentService,
            IDBMarketplaceService dbMarketplaceService,
            IDBUserService userService,
            IBasketService basketService,
            IDBAdminMarketplaceService adminMarketplaceService,
            IOptions<MarketplaceOptions> marketOption,
            IMarketplaceService marketService,
            IProductCollectionService productCollectionService)
        {
            _designerPoolService = designerPoolService;
            _designerService = designerService;
            _dbProjectDefinitionService = dbProjectDefinitionService;
            _dBProjectDevelopmentService = dBProjectDevelopmentService;
            _dbProjectPreparationService = dbProjectPreparationsService;
            _dbProjectDescriptionService = dbDescriptionService;
            _paymentService = paymentService;
            _dbMarketplaceService = dbMarketplaceService;
            _userService = userService;
            _basketService = basketService;
            _dbAdminMarketplaceService = adminMarketplaceService;
            _dBProjectOrderService = dBProjectOrderService;
            _marketOption = marketOption.Value;
            _marketService = marketService;
            _productCollectionService = productCollectionService;
        }

        public void ApplyDesigner(int designerId, DesignerLevel level)
        {
            _designerService.SetTariff(designerId, level);
            _designerService.SetTestApproved(designerId, true);
        }


        public void RejectDesigner(int designerId)
        {
            _designerService.SetTestApproved(designerId, false);
            _designerService.DeleteDesigner(designerId);
        }

        public void AppointDesigners(int projectId, int[] designersIds)
        {
            foreach(var designerId in designersIds)
            {
                _designerPoolService.AddDesignerToPool(projectId, designerId);
            }
        }

        public DesignerModel GetDesignerFullInfo(int id)
        {
            var designer = _designerService.GetDesigner(id);
            if (designer == null)
                throw new ServiceException($"Unable to load designer from DB", "designer_not_found");
            return designer.ToModel();
        }

        public int[] GetDesignerProjects(int designerId)
        {
            return _designerService.GetDesignerProjects(designerId);
        }

        public List<DesignerInfo> GetFullListDesigners()
        {
            var designersInfos = _designerService.GetShortInfos();
            return designersInfos.ToModel();
        }

        public AdminProjectInfo[] GetProjectsInfo()
        {
            var model = _dbProjectDefinitionService
                .GetProjectsInfo()
                .ToArray();

            return model;
        }

        public AdminProjectModel GetFullProjectInformation(int projectId)
        {
            var project = _dbProjectDefinitionService.GetFullProject(projectId);

            var model = new AdminProjectModel
            {
                AppointedDesignersCount = project.AppointedDesignersCount,
                City = project.City,
                ClientId = project.ClientId,
                ClientName = project.ClientName,
                ClientEmail = project.ClientEmail,
                ClientPhone = project.ClientPhone,
                DesignerId = project.DesignerId,
                Name = project.Name,
                Id = project.Id,
                Rooms = project.Rooms,
                RejectedDesignersCount = project.RejectedDesignersCount,
                DesignerRequirements = project.DesignerRequirements,
                TestAnswers = project.TestAnswers,
                Stage = project.Stage,
                Status = project.Status,
                IsFilled = project.IsFilled,
                FillTime = project.FillTime,
                NeedAdmin = project.NeedAdmin,
                HasChangesAfterLastAdminCheck = project.HasChangesAfterLastAdminCheck
            };

            var description = _dbProjectDescriptionService.GetProjectDescription(projectId);
            model.ObjectType = description.ObjectType;
            model.BudgetFurniture = description.Budget.Furniture;
            model.BudgetRenovation = description.Budget.Renovation;
            model.Styles = description.Styles.Select(x => x.ToModel()).ToList();
            model.Reasons = description.Reasons.Select(x => x.ToModel()).ToList();

            if (model.Stage < Models.ProjectStage.Preparetions)
                return model;

            model.PreparationStage = _dbProjectPreparationService.GetCurrentStage(projectId).Stage;
            model.RoomInfosAndProportions = _dbProjectPreparationService.GetRooms(projectId).Select(x => x.ToModel()).ToList();
            model.RoomAdditionsInfos = _dbProjectPreparationService.GetRoomAdditionalInfos(projectId).Select(x => x.ToModel()).ToList();
            model.Styles = _dbProjectPreparationService.GetProjectFlowStyles(projectId).Select(x => new StyleModel() { Description = x.Value, Name = x.Key }).ToList();
            model.PreparationBudget = _dbProjectPreparationService.GetBudget(projectId).ToModel();
            model.OldStuffPhotos = _dbProjectPreparationService.GetOldFurniture(projectId).ToModel();

            if (model.Stage < Models.ProjectStage.Development)
                return model;

            try
            {
                model.DevelopmentStage = _dBProjectDevelopmentService.GetCurrentStageStatus(projectId);
                model.Moodsboards = new List<Controllers.API.Models.ProjectDevelopment.MoodboardModel>();
                model.Collages = new List<Controllers.API.Models.ProjectDevelopment.CollageModel>();
                model.Zones = new List<Controllers.API.Models.ProjectDevelopment.ZoneModel>();
                var visualizations = GetVisualizationsForProject(project.Id);
                if (visualizations != null)
                {
                    model.Visualizations = visualizations.Select(x => new FullVisualizationModel()
                    {
                        IsComplete = x.IsComplete,
                        IsSelected = x.IsSelected,
                        PaymentStatus = x.PaymentStatus,
                        Price = x.Price,
                        ProjectId = project.Id,
                        RoomId = x.RoomId,
                        VisualizationCode = x.VisualizationCode
                    }).ToArray();
                }
                else
                    model.Visualizations = new FullVisualizationModel[0];
                model.WorkPlans = new List<Controllers.API.Models.ProjectDevelopment.PlanModel>();
                foreach (var room in model.RoomInfosAndProportions)
                {
                    var moodboard = _dBProjectDevelopmentService.GetMoodBoards(room.RoomId)?.ToModel();
                    if (moodboard != null)
                        model.Moodsboards.Add(moodboard);
                    var collage = _dBProjectDevelopmentService.GetCollages(room.RoomId)?.ToModel();
                    if (collage != null)
                        model.Collages.Add(collage);
                    var zoning = _dBProjectDevelopmentService.GetZones(room.RoomId)?.ToModel();
                    if (zoning != null)
                        model.Zones.Add(zoning);
                    var plans = _dBProjectDevelopmentService.GetPlans(room.RoomId)?.ToModel();
                    if (plans != null)
                        model.WorkPlans.Add(plans);
                }

                model.Products = GetProjectProductsInformation(model.RoomInfosAndProportions).ToArray();
            }
            catch { }

            return model;
        }

        public List<AdminProjectProductModel> GetProjectProductsInformation(List<RoomInfoModel> rooms)
        {
            List<AdminProjectProductModel> productModels = new List<AdminProjectProductModel>();

            foreach (var room in rooms)
            {
                var goods = _dBProjectDevelopmentService.GetMarketplaceGoods(room.RoomId);
                if (goods == null || goods.MarketplaceItems == null || goods.MarketplaceItems.Count < 1)
                    continue;

                foreach (var marketItem in goods.MarketplaceItems)
                {
                    Product product = _dbMarketplaceService.GetProductById(marketItem.ProductId);
                    if (product == null)
                        continue;

                    var adminProduct = new AdminProjectProductModel()
                    {
                        Id = product.Id,
                        Company = _dbMarketplaceService.GetCompanyNameById(product.CompanyId),
                        Count = marketItem.Count,
                        RoomName = room.Purpose,
                        RoomId = room.RoomId
                    };

                    productModels.Add(adminProduct);
                }
            }

            return productModels;
        }

        public void MarkProjectAsViewed(int projectId)
        {
            _dbProjectDefinitionService.MarkProjectAsChangedForAdmin(projectId, false);
        }

        public void SetProjectAdminAttention(int projectId, bool needAttention)
        {
            _dbProjectDefinitionService.SetProjectNeedAdminAttention(projectId, needAttention);
        }

        public void FinishProject(int projectId)
        {
            _dbProjectDefinitionService.CloseProject(projectId);
        }

        public DesignerInfo[] GetSuitableDesigners(int projectId)
        {
            return _designerPoolService.GetProjectSuitableDesigners(projectId);
        }

        public DesignerRejectedInfo[] GetRejectedDesigners(int projectId)
        {
            return _designerPoolService.GetProjectRejectedDesigners(projectId);
        }

        public DesignerInfo[] GetAppointedDesigners(int projectId)
        {
            return _designerPoolService.GetProjectAppointedDesigners(projectId);
        }

        public void CompleteVisualizationForProject(int projectId, string visualizationLink)
        {
            var visualizations = _dBProjectDevelopmentService.GetAllVisualizations(projectId);
            foreach (var visualization in visualizations)
            {
                if (visualization.IsSelected)
                {
                    _dBProjectDevelopmentService.SetVisualizationCode(visualization.RoomId, visualizationLink);
                    _dBProjectDevelopmentService.SetVisualizationAsComplete(visualization.RoomId);
                }
            }
            _dBProjectDevelopmentService.SetDesignerIsReady(projectId, DevelopmentStage.Visualization);
        }

        public async Task<string> AuthorizePaymentForProjectOrder(int orderId)
        {
            var order = _dBProjectOrderService.GetOrderInformation(orderId);

            var clientId = _dbProjectDefinitionService.GetProjectDefinition(order.ProjectId).ClientId;
            var user = _userService.GetUser(clientId);
            if (user == null || string.IsNullOrEmpty(user.Phone) || string.IsNullOrEmpty(user.Email))
                throw new ServiceException("user information error", "user_information_error");

            var oldBasePrice = order.BasePrice;
            var basePrice = _dBProjectOrderService.UpdateOrderBasePrice(orderId);
            if (basePrice != oldBasePrice)
                throw new ServiceException("Order price changed! Refresh page to see new price :3", "order_price_changed");

            if (order.PaymentId != null)
            {
                _paymentService.ClosePayment(order.PaymentId.Value);
                _dBProjectOrderService.ClearPayment(order.OrderId);
            }

            Payment payment = null;
            var products = _marketService.GetProductsFromOrder(order);
            var receipt = KassaReceiptInfo.CreateForProducts(products, order.DeliveryPrice, order.AdditionalPrice, 0, order.OrderDescription.Email, order.OrderDescription.Phone);
            var platformRoute = string.Empty;
            switch (order.PaymentMethod)
            {
                case PaymentMethod.YandexKassa:
                    platformRoute = _marketOption.PlatformPaymentRouteBase + $"/order/payroute/project/{order.OrderId}";
                    payment = await _paymentService.CreateKassaPayment(PaymentTargetType.MarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId} для проекта №{order.ProjectId}", receipt, platformRoute);
                    break;
                case PaymentMethod.Cash:
                    payment = _paymentService.CreateCashPayment(PaymentTargetType.MarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId} для проекта №{order.ProjectId}", receipt);
                    break;
                case PaymentMethod.Tinkoff:
                    payment = _paymentService.CreateTinkoffCreditPayment(PaymentTargetType.MarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId} для проекта №{order.ProjectId}", receipt);
                    break;
            }
            if (payment != null)
            {
                _dBProjectOrderService.SetOrderPayment(order.OrderId, payment.Id);
            }
            _dBProjectOrderService.SetOrderIsReady(orderId);
            return platformRoute;
        }

        public ShortOrderInformation[] GetOrdersForProject(int projectId)
        {
            return _dBProjectOrderService.GetAllOrdersIdsForProject(projectId);
        }

        public FullProjectProductsOrder GetOrderInformation(int orderId)
        {
            return _dBProjectOrderService.GetOrderInformation(orderId);
        }

        public void SetAdditionalPriceForProjectOrder(int orderId, int price)
        {
            _dBProjectOrderService.SetOrderAdditionalPrice(orderId, price);
        }

        public void SetDeliveryPriceForProjectOrder(int orderId, int price)
        {
            _dBProjectOrderService.SetOrderDeliveryPrice(orderId, price);
        }

        public void SetProjectOrderStatus(int orderId, OrderStatus status)
        {
            _dBProjectOrderService.SetOrderStatus(orderId, status);
        }

        public void FinishProjectOrder(int orderId)
        {
            _dBProjectOrderService.SetOrderStatus(orderId, OrderStatus.Completed);
        }

        public void CloseProjectOrder(int orderId)
        {
            _dBProjectOrderService.CloseOrder(orderId);
        }

        public Visualization[] GetVisualizationsForProject(int projectId)
        {
            return _dBProjectDevelopmentService.GetAllVisualizations(projectId).Where(x => x.IsSelected).ToArray();
        }

        public void RemoveGoodsFromOrder(int orderId, RoomGoods goods)
        {
            _dBProjectOrderService.RemoveGoodsFromOrder(orderId, goods);
        }

        public void SetDesignerPoolAsFinished(int projectId)
        {
            _designerPoolService.SetPoolIsFinished(projectId);
        }

        public void RemoveDesignerFromPool(int projectId, int designerId)
        {
            _designerPoolService.RemoveDesignerFromPool(projectId, designerId, false);
        }

        public ShortOrderInformation[] GetBasketOrders(int from, int count)
        {
            return _basketService.GetOrders(from, count);
        }

        public FullBasketOrderInformation GetBasketOrder(int orderId)
        {
            return _basketService.GetOrderInfo(orderId);
        }

        public async Task<string> AuthorizePaymentForBasketOrder(int orderId)
        {
            var order = _basketService.GetOrderInfo(orderId);
            if (order == null)
                throw new ServiceException($"order {orderId} not found", "order_not_found");
            var oldPrice = order.BasePrice;
            var price = _basketService.RecalculateOrderBasePrice(orderId);
            if (price != oldPrice)
                throw new ServiceException($"order {orderId} changed price from {oldPrice} to {price}. Refresh page for continue", "order_changed_price");
            if (price == 0)
                throw new ServiceException($"Empty order", "order_is_empty");
            var email = order.OrderDescription.Email;
            if (email == null)
                throw new ServiceException($"Email not specified", "email_is_empty");

            if (order.PaymentId != null)
            {
                _paymentService.ClosePayment(order.PaymentId.Value);
                _basketService.ClearPayment(order.OrderId);
            }

            Payment payment = null;
            var products = _marketService.GetProductsFromOrder(order);
            var receipt = KassaReceiptInfo.CreateForProducts(products, order.DeliveryPrice, order.AdditionalPrice, order.AbsoluteDiscount, order.OrderDescription.Email, order.OrderDescription.Phone);
            var platformRoute = string.Empty;
            switch (order.SelectedPaymentMethod)
            {
                case PaymentMethod.YandexKassa:
                    platformRoute = _marketOption.PlatformPaymentRouteBase + $"/order/payroute/market/{order.OrderId}";
                    payment = await _paymentService.CreateKassaPayment(PaymentTargetType.BasketMarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId}", receipt, platformRoute);
                    break;
                case PaymentMethod.Cash:
                    payment = _paymentService.CreateCashPayment(PaymentTargetType.BasketMarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId}", receipt);
                    break;
                case PaymentMethod.Tinkoff:
                    payment = _paymentService.CreateTinkoffCreditPayment(PaymentTargetType.BasketMarketplaceGoods, order.DeliveryPrice + order.AdditionalPrice + order.BasePrice, $"Заказ товаров №{order.OrderId}", receipt);
                    break;
            }
            if (payment != null)
            {
                _basketService.AssignPaymentToOrder(order.OrderId, payment);
            }
            _basketService.SetOrderIsReady(orderId);
            return platformRoute;
        }

        public void RemoveProductFromBasketOrder(int orderId, int productId, int count)
        {
            _basketService.RemoveProductsFromOrder(orderId, productId, count);
        }

        public void AddProductToBasketOrder(int orderId, int productId, int count)
        {
            _basketService.AddProductToOrder(orderId, productId, count);
        }

        public void SetAdditionalPriceForBasketOrder(int orderId, int price)
        {
            _basketService.SetOrderAdditionalPrice(orderId, price);
        }

        public void SetDeliveryPriceForBasketOrder(int orderId, int price)
        {
            _basketService.SetOrderDeliveryPrice(orderId, price);
        }

        public void DeleteProject(int projectId)
        {
            _dbProjectDefinitionService.ArchiveProject(projectId);
        }

        public void DeleteUser(int userId)
        {
            _dbProjectDefinitionService.DeleteUser(userId);
        }

        public void DeleteRoom(int roomId)
        {
            _dbProjectPreparationService.DeleteRoom(roomId);
        }

        public void FinishBasketOrder(int orderId)
        {
            _basketService.FinishOrder(orderId);
        }

        public void CloseBasketOrder(int orderId)
        {
            _basketService.CloseOrder(orderId);
        }

        public ModerationCountsModel GetModerationProduct()
        {
            return _dbAdminMarketplaceService.GetModerationProduct();
        }

        public GetModerationProductModel GetProductForModeration(string type)
        {
            return _dbAdminMarketplaceService.GetProductForModeration(type);
        }

        public void SetModerationProduct(ModerationProductModel model)
        {
            _dbAdminMarketplaceService.SetModerationProduct(model);
        }

        public void SetBasketOrderStatus(int orderId, OrderStatus status)
        {
            _basketService.SetOrderStatus(orderId, status);
        }

        public ProductsStats GetProductsStats()
        {
            return _dbAdminMarketplaceService.GetProductsStats();
        }

        public List<GetCategoryProductsModel> GetCategoryProducts(int companyId, int companyCategoryId)
        {
            //var fullProductList = new List<GetCategoryProductsModel>();
            //int step = 100;
            //int offset = 0;
            //int safeCounter = 1000;
            //do
            //{
            //    safeCounter--;
            //    var products = _dbMarketplaceService.GetCategoryProducts(companyId, companyCategoryId, offset, step);
            //    if (products.Count > 0)
            //    {
            //        fullProductList.AddRange(products);
            //        offset += step;
            //    }
            //    else
            //        break;
            //} while (safeCounter > 0);
            //return fullProductList;
            return _dbMarketplaceService.GetCategoryProducts(companyId, companyCategoryId);
        }

        public bool ChangeProductCategory(int productId, int categoryId)
        {
            return _dbMarketplaceService.ChangeProductCategory(productId, categoryId);
        }

        public void ChangeBasketOrder(ChangeBasketOrder changes)
        {
            _basketService.ChangeOrder(changes);
        }

        public ARProductFullInfo GetArProductFullInfo(int arProductId)
        {
            return _dbAdminMarketplaceService.GetArProductFullInfo(arProductId);
        }

        public void MapARProductToProduct(int? productId, int arProductId)
        {
            _dbAdminMarketplaceService.MapARProductToProduct(productId, arProductId);
        }

        public ARProductShortInfo[] GetArProductList()
        {
            return _dbAdminMarketplaceService.GetArProductList();
        }

        public PossibleDailyProduct[] GetPossibleDailyProducts(int offset, int count, float companyKoef, float categoryKoef, float saleKoef)
        {
            return _dbAdminMarketplaceService.GetPossibleDailyProducts(offset, count, companyKoef, categoryKoef, saleKoef);
        }

        public DailyProduct GetDailyProduct()
        {
            return _dbAdminMarketplaceService.GetDailyProduct();
        }

        public void SetDailyProduct(int productId)
        {
            _dbAdminMarketplaceService.SetDailyProduct(productId);
        }

        public DailyProduct[] GetPreviousDailyProducts()
        {
            return _dbAdminMarketplaceService.GetPreviousDailyProducts();
        }

        public void RefreshPriceForBasketOrder(int orderId)
        {
            _basketService.RecalculateOrderBasePrice(orderId);
        }

        public void ChangePromoCodeForOrder(string promocode, int orderId)
        {
            _basketService.ChangePromoCodeForOrder(promocode, orderId);
        }

        public void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod)
        {
            _basketService.ChangePaymentMethod(orderId, paymentMethod);
        }

        public List<string> GetBlacklist()
        {
            return _productCollectionService.GetBlacklist();
        }

        public void AddWordToBackList(string word)
        {
            _productCollectionService.AddWordToBackList(word);
        }

        public void RemoveWordFromBlackList(string word)
        {
            _productCollectionService.RemoveWordFromBlackList(word);
        }

        public async Task<string> GetCollectionsInCSVForCompany(int companyId)
        {
            return await _productCollectionService.GetCollectionsInCSVForCompany(companyId);
        }

        public List<ProductCollection> GetProductCollectionsForCompany(int companyId)
        {
            return _productCollectionService.GetCollectionsForCompany(companyId);
        }

        public List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId)
        {
            return _productCollectionService.GetCollectionsInfoForComapny(companyId);
        }

        public ProductCollection GetCollection(int collectionId)
        {
            return _productCollectionService.GetCollection(collectionId);
        }

        public void ChangeCollection(int collectionId, string name, bool isAvalable)
        {
            _productCollectionService.ChangeCollection(collectionId, name, isAvalable);
        }

        public async Task ManuallyCreateCollectionsForCompany(int companyId)
        {
            await _productCollectionService.UpdateProductCollectionForCompany(companyId, 100);
        }
    }
}
