﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Faradise.Design.Models.ProductCollection;

namespace Faradise.Design.Services.Implementation
{
    public class ProductCollectionService : IProductCollectionService
    {
        private readonly IDBProductCollectionService _dbService;

        public ProductCollectionService(IDBProductCollectionService dbService)
        {
            _dbService = dbService;
        }

        public async Task<string> GetCollectionsInCSVForCompany(int companyId)
        {
            var collections = await FindProductCollectionsForCompany(companyId, 1000);
            string csvCollection = "Collection;ProductName;ProductId";
            foreach (var nameToCollection in collections)
            {
                var collection = nameToCollection.Value;
                if (collection.Count < 2)
                    continue;
                foreach (var product in collection.Distinct())
                {
                    csvCollection += '\n';
                    csvCollection += nameToCollection.Key + ";" + product.Name + ";" + product.ProductId.ToString();
                }
            }
            return csvCollection;
        }

        public List<string> GetBlacklist()
        {
            return _dbService.GetBlackList();
        }

        public void AddWordToBackList(string word)
        {
            _dbService.AddWordToBackList(word);
        }

        public void RemoveWordFromBlackList(string word)
        {
            _dbService.RemoveWordFromBlackList(word);
        }

        public async Task<Dictionary<string, List<ProductName>>> FindProductCollectionsForCompany(int companyId, int take)
        {
            return await _dbService.FindProductCollectionsForCompany(companyId, take);
        }

        public async Task UpdateProductCollectionForCompany(int productId, int take)
        {
            await _dbService.UpdateProductCollectionForCompany(productId, take);
        }

        public List<ProductCollection> GetCollectionsForCompany(int companyId)
        {
            return _dbService.GetCollectionsForCompany(companyId);
        }

        public void ChangeCollection(int collectionId, string name, bool isAvalable)
        {
            _dbService.ChangeCollection(collectionId, name, isAvalable);
        }

        public List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId)
        {
            return _dbService.GetCollectionsInfoForComapny(companyId);
        }

        public ProductCollection GetCollection(int collectionId)
        {
            return _dbService.GetCollection(collectionId);
        }
    }
}
