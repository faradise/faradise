﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.Badge;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Services.Implementation
{
    public class BadgeService : IBadgeService
    {
        private readonly IDBBadgeService _dbBadgeService = null;

        public BadgeService ( IDBBadgeService dbBadgeService)
        {
            _dbBadgeService = dbBadgeService;
        }

        public Badge CreateNewBadge(string name)
        {
            return _dbBadgeService.CreateNewBadge(name);
        }

        public void DeleteBadge(int badgeId)
        {
            _dbBadgeService.DeleteBadge(badgeId);
        }

        public Badge GetBadge(int badgeId)
        {
            return _dbBadgeService.GetBadge(badgeId);
        }

        public BadgeShortInfo[] GetBadges()
        {
            return _dbBadgeService.GetBadges();
        }

        public void SetBadgeActive(int badgeId, bool isActive)
        {
            _dbBadgeService.SetBadgeActive(badgeId, isActive);
        }

        public void UpdateBadge(Badge updateInfo)
        {
            _dbBadgeService.UpdateBadge(updateInfo);
        }
    }
}
