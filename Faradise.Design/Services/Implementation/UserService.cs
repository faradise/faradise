﻿using Faradise.Design.Models;
using Faradise.Design.Internal.Exceptions;

namespace Faradise.Design.Services.Implementation
{
    public class UserService : IUserService
    {
        private IDBUserService _dbUserService = null;
        private ITokenService _tokenService = null;
        private IDesignerService _designerService = null;

        public UserService(IDBUserService dbUserService, ITokenService tokenService, IDesignerService designerService)
        {
            _dbUserService = dbUserService;
            _tokenService = tokenService;
            _designerService = designerService;
        }
                
        public User CreateUser(Role role)
        {
            var user = new User()
            {
                Role = role
            };

            user = _dbUserService.CreateUser(user.Role);

            if (role == (Role.Designer | Role.Authorized))
                _designerService.CreateDesigner(user.Id);

            return user;
        }

        public User ExtractUserById(int id)
        {
            var user = _dbUserService.GetUser(id);
            return user;
        }

        public User ExtractUserByAlias(string aliasString, AliasType aliasType)
        {
            var alias = _dbUserService.GetAlias(aliasType, aliasString);
            if(alias == null)
                return null;

            var user = _dbUserService.GetUser(alias.UserId);
            return user;
        }
                
        public void AddAlias(int id, AliasType type, string value)
        {
            Alias model = new Alias { UserId = id, Type = type, Value = value };
            if (_dbUserService.GetAlias(model.Type, model.Value) == null)
            {
                _dbUserService.CreateAlias(model);
                if (type == AliasType.Phone)
                    AddUserPhone(id, value);
            }
            else
                throw new ServiceException(string.Format("This {0} already used", type), "already_in_use");
        }

        public void AddRole(int id, Role role)
        {
            var user = _dbUserService.GetUser(id);
            if (user == null)
                throw new ServiceException("User not found", "bad_userid");

            user.Role = user.Role | role;
            _dbUserService.SetUserRole(user.Id, user.Role);
        }

        public void AddEmail(int id, string email)
        {
            _dbUserService.SetUserEmail(id, email);
        }

        public void AddUserName(int id, string userName)
        {
            _dbUserService.SetUserName(id, userName);
        }

        public void AddUserPhone(int id, string userPhone)
        {
            _dbUserService.SetUserPhone(id, userPhone);

            var user = ExtractUserById(id);
            if (user.Role == (Role.Designer | Role.Authorized))
                _designerService.SetPhone(id, userPhone);
        }


        public bool CheckPhoneInUse(string fixedPhone)
        {
            var exists = _dbUserService.GetAlias(AliasType.Phone, fixedPhone);
            return exists != null;
        }
    }
}
