﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectPreparations;

namespace Faradise.Design.Services.Implementation
{
    public class ProjectPreparationsService : IProjectPreparationsService
    {
        private readonly IDBProjectFlowService _dbProjectFlow = null;
        private readonly IProjectDefinitionService _defService = null;
        private readonly IProjectDevelopmentService _devService = null;

        public ProjectPreparationsService(IDBProjectFlowService dbProjectService, IProjectDefinitionService defService, IProjectDevelopmentService devService)
        {
            _dbProjectFlow = dbProjectService;
            _defService = defService;
            _devService = devService;
        }

        public int AddOldFurnishPhoto(int projectId, string photoUrl)
        {
            return _dbProjectFlow.AddOldFurnishToSavePhoto(projectId, photoUrl);
        }

        public int AddOldFurniturePhoto(int projectId, string photoUrl)
        {
            return _dbProjectFlow.AddOldFurnitureToSavePhoto(projectId, photoUrl);
        }

        public void SetPlan(int roomId, string photoUrl)
        {
            _dbProjectFlow.SetPlanPhoto(roomId, photoUrl);
        }

        public int AddRoomPhoto(int roomId, string photoUrl)
        {
            return _dbProjectFlow.AddRoomPhoto(roomId, photoUrl);
        }

        public void ConfirmDesigner(int projectId)
        {
            _defService.ConfirmDesigner(projectId);
            if (!_dbProjectFlow.HasProjectFlow(projectId))
            {
                _dbProjectFlow.CreateProjectFlow(projectId);
            }
        }

        public void DeleteOldFurnishPhoto(int photoId)
        {
            _dbProjectFlow.DeleteOldFurnishPhoto(photoId);
        }

        public void DeleteOldFurniturePhoto(int photoId)
        {
            _dbProjectFlow.DeleteOldFurniturePhoto(photoId);
        }

        public void DeletePlan(int roomId)
        {
            _dbProjectFlow.DeletePlan(roomId);
        }

        public void DeleteRoomPhoto(int photoId)
        {
            _dbProjectFlow.DeleteRoomPhoto(photoId);
        }

        public ClientDesignerBudget GetBudget(int projectId)
        {
            return _dbProjectFlow.GetBudget(projectId);
        }

        public StageStatus GetCurrentStage(int projectId)
        {
            return _dbProjectFlow.GetCurrentStage(projectId);
        }

        public OldFurniture GetOldFurniture(int projectId)
        {
            return _dbProjectFlow.GetOldFurniture(projectId);
        }

        public List<AdditionalInformationForRoom> GetRoomAdditionalInfos(int projectId)
        {
            return _dbProjectFlow.GetRoomAdditionalInfos(projectId);
        }

        public List<RoomInfo> GetRooms(int projectId)
        {
            return _dbProjectFlow.GetRooms(projectId);
        }

        public Dictionary<ProjectStyle, string> GetStyles(int projectId)
        {
            return _dbProjectFlow.GetProjectFlowStyles(projectId);
        }

        public void SetAdditionalInformation(int projectId, List<AdditionalInformationForRoom> rooms)
        {
            _dbProjectFlow.SetAdditionalInformation(rooms);
        }

        public void SetBudgetForProject(int projectId, int furnitureBudget, int renovationBudget)
        {
            _dbProjectFlow.SetProjectFlowBudget(projectId, furnitureBudget, renovationBudget);
        }

        public void SetInfoAboutFavoriteStyles(int projectId, Dictionary<ProjectStyle, string> styles)
        {
            _dbProjectFlow.SetProjectFlowStyles(projectId, styles);
        }

        public void SetInfoAboutProjectRooms(int projectId, List<ShortRoomInfo> rooms)
        {
            if (rooms == null)
                return;
            var roomProportions = new Dictionary<int, RoomProportions>(rooms.Count);
            foreach (var r in rooms)
                roomProportions.Add(r.RoomId, r.Proportions);
            _dbProjectFlow.SetProjectFlowRoomProportions(roomProportions);
        }

        public bool SetStageStatus(int projectId, CooperateStage stage, Role role, bool isReadyForNextStage)
        {
            var currentStage = GetCurrentStage(projectId);
            if (stage != currentStage.Stage)
                throw new ServiceException($"Current stage is {currentStage.Stage}, not {stage}!", "project_wrong_stage");
            if (role.ContainsRole(Role.Designer) && isReadyForNextStage)
            {
                if (!currentStage.ClientIsReady)
                    throw new ServiceException($"Waiting for client!", "project_client_not_ready");
                if (currentStage.Stage == CooperateStage.WhatToSave)
                {
                    if (!_devService.HasProjectDevelopment(projectId))
                    {
                        var rooms = GetRooms(projectId);
                        if (rooms == null)
                            throw new ServiceException($"rooms for project with id {projectId} not found", "rooms_not_found");
                        _devService.CreateProjectDevelopment(projectId, rooms);
                    }
                    return true;
                }
                var next = _dbProjectFlow.GoToNextStage(projectId);
                return (int)next > (int)stage;
            }
            else if (role.ContainsRole(Role.Client))
                _dbProjectFlow.SetClientReady(projectId);
            return false;
        }

        public bool CheckIfStageIsCurrent(int projectId, CooperateStage stage)
        {
            return GetCurrentStage(projectId).Stage == stage;
        }

        public void StartStagePreparations(int projectId)
        {
            if (!_dbProjectFlow.HasProjectFlow(projectId))
            {
                _dbProjectFlow.CreateProjectFlow(projectId);
            }
        }

        public void SkipStage(int projectId, CooperateStage stage)
        {
            var currentStage = GetCurrentStage(projectId);
            if (stage != currentStage.Stage)
                throw new ServiceException($"Current stage is {currentStage.Stage}, not {stage}!", "project_wrong_stage");
            if (!CanSkipThatStage(stage))
                throw new ServiceException($"Cant skip that stage!", "project_wrong_stage");
            _dbProjectFlow.SetClientReady(projectId);
            if (currentStage.Stage == CooperateStage.WhatToSave)
            {
                if (!_devService.HasProjectDevelopment(projectId))
                {
                    var rooms = GetRooms(projectId);
                    if (rooms == null)
                        throw new ServiceException($"rooms for project with id {projectId} not found", "rooms_not_found");
                    _devService.CreateProjectDevelopment(projectId, rooms);
                }
                return;
            }
            _dbProjectFlow.GoToNextStage(projectId);
        }

        private bool CanSkipThatStage(CooperateStage stage)
        {
            return stage == CooperateStage.WhatToSave;
        }
    }
}
