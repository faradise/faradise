﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.Payment.JSON;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Implementation
{
    public class PaymentService : IPaymentService
    {
        private IDBPaymentService _dbPaymentService = null;
        private readonly string _authorization = string.Empty;
        private readonly string _redirectUrl = string.Empty;

        public PaymentService(IDBPaymentService dbPaymentService, IOptions<YandexKassaOptions> options)
        {
            _dbPaymentService = dbPaymentService;
            _authorization = Convert.ToBase64String(Encoding.UTF8.GetBytes(options.Value.ShopId + ":" + options.Value.ShopKey));
            _redirectUrl = options.Value.RedirectUrl;
        }

        public void ClosePayment(int paymentId)
        {
            _dbPaymentService.ClosePayment(paymentId);
        }

        public async Task<Payment> CreateKassaPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt, string platformRoute)
        {
            if (receipt == null || (string.IsNullOrEmpty(receipt.Email) && string.IsNullOrEmpty(receipt.Phone)))
                throw new ServiceException("No contact data provided for receipt", "no_receipt_contact_data");
            var payment = new Payment()
            {
                PaymentTarget = paymentTarget,
                Amount = amount.ToString(),
                Currency = "RUB",
                Status = PaymentStatus.Created,
                Description = description,
                Method = PaymentMethod.YandexKassa,
                PlatformRoute = platformRoute,
                Reciept = JsonConvert.SerializeObject(receipt)
            };
            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            return payment;
            //return await UpdateKassaPaymentRoute(paymentId);
        }

        public async Task<Payment> UpdateKassaPaymentRoute(int paymentId)
        {
            var payment = GetPayment(paymentId);
            if (payment.Method != PaymentMethod.YandexKassa || payment.Status == PaymentStatus.Completed)
                throw new ServiceException("payment_update_failed", "update available only for yandex kassa payments");
            if (!string.IsNullOrEmpty(payment.YandexId) && payment.Status == PaymentStatus.Pending)
                return payment;
            if (string.IsNullOrEmpty(payment.Reciept))
                throw new ServiceException("payment_update_failed", "reciept is null for payment");
            var receipt = JsonConvert.DeserializeObject<KassaReceiptInfo>(payment.Reciept);
            var idempotenceKey = Guid.NewGuid().ToString();
            using (var httpClient = new HttpClient())
            {
                var msg = new HttpRequestMessage(HttpMethod.Post, "https://payment.yandex.net/api/v3/payments");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _authorization);
                msg.Headers.Add("Idempotence-Key", idempotenceKey);

                var kassaReceiptItems = new YandexCreatePayment.Item[receipt.Items.Count];
                for (int i = 0; i < receipt.Items.Count; i++)
                {
                    var item = receipt.Items[i];
                    kassaReceiptItems[i] = new YandexCreatePayment.Item()
                    {
                        amount = new YandexCreatePayment.Amount() { currency = "RUB", value = item.Amount.ToString() },
                        description = item.Description,
                        quantity = item.Quantity.ToString(),
                        vat_code = 1,
                        payment_subject = item.PaymentSubject,
                        payment_mode = "full_payment"
                    };
                }

                var createModel = new YandexCreatePayment()
                {
                    description = payment.Description,
                    amount = new YandexCreatePayment.Amount()
                    {
                        value = payment.Amount.ToString(),
                        currency = payment.Currency
                    },
                    confirmation = new YandexCreatePayment.Confurmation()
                    {
                        return_url = _redirectUrl,
                        type = "redirect",
                    },
                    capture = true,
                    receipt = new YandexCreatePayment.Receipt()
                    {
                        items = kassaReceiptItems,
                        tax_system_code = 2
                    }
                };

                if (string.IsNullOrEmpty(receipt.Email))
                    createModel.receipt.phone = receipt.Phone;
                else
                    createModel.receipt.email = receipt.Email;

                var jsonString = JsonConvert.SerializeObject(createModel);
                if (jsonString != null && !string.IsNullOrEmpty(jsonString))
                {
                    msg.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                }
                else
                    return null;
                var response = await httpClient.SendAsync(msg);
                if (!response.IsSuccessStatusCode)
                {
                    string errorJson = string.Empty;
                    if (response.Content != null)
                        errorJson = await response.Content.ReadAsStringAsync();
                    throw new ServiceException($"Yandex Kassa create payment error for {payment.Description}, code {(int)response.StatusCode}, phrase {response.ReasonPhrase}, json: {errorJson}", "yandex_kassa_create_payment_failed");
                }
                var responseJson = response.Content.ReadAsStringAsync().Result;
                var createInfo = JsonConvert.DeserializeObject<YandexCreatePaymentResponse>(responseJson);
                var status = YandexToInnerStatus(createInfo.status);
                if (status == PaymentStatus.Canceled)
                    throw new ServiceException($"Yandex Kassa canceled payment for {payment.Description}, code {(int)response.StatusCode}, phrase {response.ReasonPhrase}", "yandex_kassa_cancel_payment");
                payment.IdempotenceKey = idempotenceKey;
                payment.PaymentRoute = createInfo.confirmation.confirmation_url;
                payment.RawJson = responseJson;
                payment.YandexId = createInfo.id;
                payment.Status = status;
                payment.Id = paymentId;
                _dbPaymentService.UpdatePayment(payment);
                return payment;
            }

        }

        public Payment CreateCashPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt)
        {
            if (receipt == null || (string.IsNullOrEmpty(receipt.Email) && string.IsNullOrEmpty(receipt.Phone)))
                throw new ServiceException("No contact data provided for receipt", "no_receipt_contact_data");
            var idempotenceKey = Guid.NewGuid().ToString();
            var payment = new Payment()
            {
                IdempotenceKey = idempotenceKey,
                PaymentTarget = paymentTarget,
                Amount = amount.ToString(),
                Currency = "RUB",
                Description = description,
                Status = PaymentStatus.Manual,
                Method = PaymentMethod.Cash,
                Reciept = JsonConvert.SerializeObject(receipt)
            };
            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            return payment;
        }

        public Payment CreateTinkoffCreditPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt)
        {
            if (receipt == null || (string.IsNullOrEmpty(receipt.Email) && string.IsNullOrEmpty(receipt.Phone)))
                throw new ServiceException("No contact data provided for receipt", "no_receipt_contact_data");
            var idempotenceKey = Guid.NewGuid().ToString();
            var payment = new Payment()
            {
                IdempotenceKey = idempotenceKey,
                PaymentTarget = paymentTarget,
                Amount = amount.ToString(),
                Currency = "RUB",
                Description = description,
                Status = PaymentStatus.Manual,
                Method = PaymentMethod.Tinkoff,
                Reciept = JsonConvert.SerializeObject(receipt)
            };
            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            return payment;
        }

        public Payment GetPayment(int paymentId)
        {
            return _dbPaymentService.GetPayment(paymentId);
        }

        public void SetPaymentStatus(int paymentId, PaymentStatus status)
        {
            _dbPaymentService.SetPaymentStatus(paymentId, status);
        }

        public async Task<Payment> RequestAndUpdateKassaPaymentInfo(int paymentId)
        {
            var payment = GetPayment(paymentId);
            if (payment == null)
                throw new ServiceException($"Payment id {paymentId} not found", "payment_not_found");
            using (var httpClient = new HttpClient())
            {
                var msg = new HttpRequestMessage(HttpMethod.Get, "https://payment.yandex.net/api/v3/payments/" + payment.YandexId);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _authorization);
                var response = await httpClient.SendAsync(msg);
                if (!response.IsSuccessStatusCode)
                    return null;
                var responseJson = response.Content.ReadAsStringAsync().Result;
                var paymentInfo = JsonConvert.DeserializeObject<YandexKassaPaymentInfo>(responseJson);
                var status = YandexToInnerStatus(paymentInfo.status);
                if (status != payment.Status)
                    return _dbPaymentService.SetPaymentStatus(payment.Id, status);
                else
                    return payment;
            }
        }

        private PaymentStatus YandexToInnerStatus(string yandexStatus)
        {
            if (yandexStatus == "pending")
                return PaymentStatus.Pending;
            else if (yandexStatus == "waiting_for_capture")
                return PaymentStatus.Pending;
            else if (yandexStatus == "succeeded")
                return PaymentStatus.Completed;
            else if (yandexStatus == "canceled")
                return PaymentStatus.Canceled;
            throw new ServiceException($"Yandex Kassa return unrecognised pay status {yandexStatus}", "yandex_status_urecocgnised");
        }

        public int[] GetPendingPayments(PaymentMethod method)
        {
            return _dbPaymentService.GetPendingPaymentsIds(method);
        }
    }
}
