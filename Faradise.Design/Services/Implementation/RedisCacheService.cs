﻿using System;
using System.Linq;
using Faradise.Design.Models.Cache;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Faradise.Design.Services.Implementation
{
    public class RedisCacheService : ICacheService, IDisposable
    {
        private const int LIFETIME = 30;

        private IConnectionMultiplexer _redis = null;
        private IDatabase _db = null;

        public RedisCacheService(IOptions<ServicesOptions> options)
        {
            _redis = ConnectionMultiplexer.Connect(options.Value.Redis);
            _db = _redis.GetDatabase();
        }

        private static string GenerateKey(CacheRequest request)
        {
            if (request.Arguments.Length > 0)
            {
                var stringArgs = request.Arguments
                    .Select(x => JsonConvert.SerializeObject(x, Formatting.None))
                    .ToArray();

                return $"{request.Key}_{string.Join('/', stringArgs)}";
            }

            return request.Key;
        }

        public bool Put<T>(CacheRequest request, T result, bool extend = false)
        {
            try
            {
                var key = GenerateKey(request);
                var value = JsonConvert.SerializeObject(result);

                return Save(key, value, extend);
            }
            catch
            {
                return false;
            }
        }

        public CacheValue<T> Extract<T>(CacheRequest request)
        {
            try
            {
                var key = GenerateKey(request);

                var serializedResult = Get(key);
                if (serializedResult == null)
                    CacheValue<T>.Miss();

                var result = JsonConvert.DeserializeObject<T>(serializedResult);
                
                return CacheValue<T>.Ok(result);
            }
            catch
            {
                return CacheValue<T>.Miss();
            }
        }

        private bool Save(string key, string value, bool extend)
        {
            var rkey = (RedisKey) key;

            var stored = _db.StringSet(rkey, value);
            if (!stored)
                return false;

            _db.KeyExpire(rkey, TimeSpan.FromMinutes(LIFETIME));

            return true;
        }

        private string Get(string key)
        {
            var rkey = (RedisKey)key;

            var result = _db.StringGet(rkey);

            return result.ToString();
        }

        public void Dispose()
        {
            if (_redis != null)
                _redis.Dispose();
        }

        public void Drop()
        {
            var endpoints = _redis.GetEndPoints();
            foreach (var e in endpoints)
            {
                var s = _redis.GetServer(e);
                var keys = s.Keys();

                foreach (var k in keys)
                    _db.KeyDelete(k);
            }
        }
    }
}
