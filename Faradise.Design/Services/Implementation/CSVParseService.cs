﻿using Faradise.Design.DAL;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Utilities.Parser;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation
{
    public class CSVParseService
    {
        private FaradiseDBContext _context = null;

        public CSVParseService(FaradiseDBContext context)
        {
            _context = context;
        }

        public void Parse()
        {
            var parser = new ARProductCSVParser();
            var wc = new WebClient();
            var data = wc.DownloadData("http://csv.faradise.ru/csv.php?key=d41d8cd98f00b204e9800998ecf8427e");
            var stream = new MemoryStream(data);
            var models = parser.Parse(stream);
            UpdateARProducts(models);
        }

        public void CreateDBEntities(List<ARProduct> models)
        {
            var entities = models.Select(x => new ARProductEntity
            {
                FaradiseProductId = x.FaradiseProductId,
                FaradiseColorId = x.FaradiseColorId,
                ProductId = null,
                Name = x.Name,
                Color = x.Color,
                VendorCode = x.VendorCode,
                Render = x.Render,
                Vertical = x.Vertiacal,

            }).ToList();
            _context.ARProducts.AddRange(entities);
            _context.SaveChanges();
            var arModels = new List<ARModelEntity>();
            models.ForEach(x => arModels.AddRange(GetModels(x, entities.First(y => y.FaradiseColorId==x.FaradiseColorId).Id)));
            _context.ARModels.AddRange(arModels);
            _context.SaveChanges();

            
        }

        private void UpdateARProducts(List<ARProduct> updatedProducts)
        {
            const int step = 1000;
            var colorsIds = new List<int>();
            for (var i = 0; i*1000 < updatedProducts.Count; i++)
            {
                var part = updatedProducts
                    .Skip(i * step)
                    .Take(step)
                    .ToList();

                // В продуктах уникальный FaradiseColorId
                var existedProducts = part.Join(_context.ARProducts.Include("ARModels"), x => x.FaradiseColorId, y => y.FaradiseColorId, (x, y) => UpdateProduct(x,y)).ToArray();
                colorsIds.AddRange(existedProducts.Select(x => x.FaradiseColorId).ToArray());
                 _context.ARProducts.UpdateRange(existedProducts);

                _context.SaveChanges();
            }
            DeleteOldProducts(updatedProducts.Select(x => x.FaradiseColorId).ToArray());
            updatedProducts.RemoveAll(x => colorsIds.Contains(x.FaradiseColorId));
            if(updatedProducts.Any())
                CreateDBEntities(updatedProducts);
           

        }

        private void DeleteOldProducts(int[] colorsIds)
        {
            var allProductsIds = _context.ARProducts.Select(x=>x.FaradiseColorId).ToList();
            var oldProductsIds = allProductsIds.Except(colorsIds).ToList();
            var oldProducts = oldProductsIds.Join(_context.ARProducts, x => x, y => y.FaradiseColorId, (x, y) => y).ToList();
            _context.ARProducts.RemoveRange(oldProducts);
            _context.SaveChanges();
        }

        private ARProductEntity UpdateProduct(ARProduct newData, ARProductEntity entity)
        {
            entity.FaradiseProductId = newData.FaradiseProductId;
            entity.FaradiseColorId = newData.FaradiseColorId;
            entity.Name = newData.Name;
            entity.Color = newData.Color;
            entity.VendorCode = newData.VendorCode;
            entity.Render = newData.Render;
            entity.Vertical = newData.Vertiacal;
            var oldArModels = entity.ARModels;
            UpdateARModels(oldArModels, newData, entity.Id);
            return entity;
        }

        private void UpdateARModels(ICollection<ARModelEntity> oldArModels, ARProduct product, int ARProductId)
        {
            var androidBFM = oldArModels.FirstOrDefault(x => x.Platform == ARPlatform.Android && x.Format == ARFormat.BFM);
            if(androidBFM==null)
            {
                androidBFM = CreateARModelEntity(ARProductId, ARPlatform.Android, ARFormat.BFM, product.AndroidBFM);
                _context.ARModels.Add(androidBFM);
            }
            else
             UpdateARModelData(androidBFM, product.AndroidBFM);

            var androidSFB = oldArModels.FirstOrDefault(x => x.Platform == ARPlatform.Android && x.Format == ARFormat.SFB);
            if (androidSFB == null)
            {
                androidSFB = CreateARModelEntity(ARProductId, ARPlatform.Android, ARFormat.SFB, product.AndroidSFB);
                _context.ARModels.Add(androidSFB);
            }
            else
                UpdateARModelData(androidSFB, product.AndroidSFB);

            var iosBFM = oldArModels.FirstOrDefault(x => x.Platform == ARPlatform.IOS && x.Format == ARFormat.BFM);
            if (iosBFM == null)
            {
                iosBFM = CreateARModelEntity(ARProductId, ARPlatform.IOS, ARFormat.BFM, product.IosBFM);
                _context.ARModels.Add(iosBFM);
            }
            else
                UpdateARModelData(iosBFM, product.IosBFM);

            var iosSCN = oldArModels.FirstOrDefault(x => x.Platform == ARPlatform.IOS && x.Format == ARFormat.SCN);
            if (iosSCN == null)
            {
                iosSCN = CreateARModelEntity(ARProductId, ARPlatform.IOS, ARFormat.SCN, product.IosSCN);
                _context.ARModels.Add(iosSCN);
            }
            else
                UpdateARModelData(iosSCN, product.IosSCN);

            _context.SaveChanges();
        }

        private void UpdateARModelData(ARModelEntity entity, ARParsedModel newModel)
        {
            entity.Model = newModel.GeometryUrl;
            entity.Diffuse = newModel.DiffuseUrl;
            entity.Normal = newModel.NormalUrl;
            entity.Metallic = newModel.MetalicUrl;
            entity.Roughness = newModel.RoughnessUrl;
            entity.MetallicRoughness = newModel.MetallicRoughnessUrl;
        }


        private List<ARModelEntity> GetModels(ARProduct product, int id)
        {
            var androidBFM = CreateARModelEntity(id, ARPlatform.Android, ARFormat.BFM, product.AndroidBFM);
            var androidSFB = CreateARModelEntity(id, ARPlatform.Android, ARFormat.SFB, product.AndroidSFB);
            var iosBFM= CreateARModelEntity(id, ARPlatform.IOS, ARFormat.BFM, product.IosBFM);
            var iosSCN = CreateARModelEntity(id, ARPlatform.IOS, ARFormat.SCN, product.IosSCN);

            return new List<ARModelEntity> { androidBFM, androidSFB, iosBFM, iosSCN };
        }

        private ARModelEntity CreateARModelEntity(int ARProductId, ARPlatform platform, ARFormat format, ARParsedModel model)
        {
            return new ARModelEntity
            {
                ARProductId = ARProductId,
                Platform = platform,
                Format = format,
                Model = model.GeometryUrl,
                Diffuse = model.DiffuseUrl,
                Normal = model.NormalUrl,
                Metallic = model.MetalicUrl,
                Roughness = model.RoughnessUrl,
                MetallicRoughness = model.MetallicRoughnessUrl
            };
        }
    }
}
