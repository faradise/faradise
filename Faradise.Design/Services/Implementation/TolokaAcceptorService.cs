﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Toloka;
using Faradise.Design.Models.Toloka.Acceptors;

namespace Faradise.Design.Services.Implementation
{
    public class TolokaAcceptorService : ITolokaAcceptorService
    {
        private readonly FaradiseDBContext _context = null;
        private ICDNService _cdn = null;
        private readonly IServiceProvider _provider = null;
        private ITolokaService _tolokaService = null;

        public TolokaAcceptorService(FaradiseDBContext context, ICDNService cdn, IServiceProvider provider, ITolokaService tolokaService)
        {
            _context = context;
            _cdn = cdn;
            _provider = provider;
            _tolokaService = tolokaService;
        }

        public async Task<bool> Accept(TolokaOutput output, TolokaOutputType type, int taskId)
        {
            var acceptorType = typeof(TolokaAcceptor)
                .Assembly
                .GetTypes()
                .Select(x => new {Type = x, Attribute = x.GetCustomAttribute<TolokaAcceptorTargetAttribute>()})
                .Where(x => x.Attribute != null)
                .FirstOrDefault(x => x.Attribute.Type == type);

            if (acceptorType == null)
                return false;

            var acceptor = (TolokaAcceptor)Activator.CreateInstance(acceptorType.Type);
            acceptor.ResolveServices(_provider);
            await acceptor.Accept(output, type, taskId, SaveSegment);
            
            return true;
        }

        public async Task<bool> AcceptSegment(TolokaOutput output, TolokaOutputType type, int taskId)
        {
            var acceptorType = typeof(TolokaAcceptor)
                .Assembly
                .GetTypes()
                .Select(x => new { Type = x, Attribute = x.GetCustomAttribute<TolokaAcceptorTargetAttribute>() })
                .Where(x => x.Attribute != null)
                .FirstOrDefault(x => x.Attribute.Type == type);

            if (acceptorType == null)
                return false;

            var acceptor = (TolokaAcceptor)Activator.CreateInstance(acceptorType.Type);
            acceptor.ResolveServices(_provider);
            var report = await acceptor.AcceptSegment(output, ExtractProduct, PostprocessProduct);
            await _tolokaService.UploadTaskReportToCDN(report, taskId);
            return true;
        }

        private ProductEntity ExtractProduct(int id)
        {
            return _context.Products.Find(id);
        }

        private void PostprocessProduct(ProductEntity entity)
        {
            _context.SaveChanges();
        }

        private async Task<bool> SaveSegment(string file, TolokaOutputType type, int taskId)
        {
            return await _tolokaService.UploadTaskSegmentToCDN(file, type, taskId);
        }
    }
}
