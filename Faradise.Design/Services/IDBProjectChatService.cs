﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBProjectChatService
    {
        void SetClientNotificationIsActive(int projectId, bool isActive);
        void SetDesignerNotificationIsActive(int projectId, bool isActive);
        bool CheckClientNotifyISActive(int projectId);
        bool CheckDesignerNotifyISActive(int projectId);
    }
}
