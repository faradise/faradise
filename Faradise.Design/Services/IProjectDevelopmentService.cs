﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectPreparations;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IProjectDevelopmentService
    {
        void CreateProjectDevelopment(int projectId, List<RoomInfo> rooms);
        bool HasProjectDevelopment(int projectId);

        RoomPurposeData[] GetRoomsShortInfos(int projectId);

        Moodboard GetMoodBoards(int roomId);
        int AddMoodboardPhoto(int projectId, int roomId, string photoUrl);
        void DeleteMoodboardPhoto(int projectId, int photoId);
        void SetMoodboardArchive(int roomId, string url);

        Zone GetZones(int roomId);
        int AddZonePhoto(int projectId, int roomId, string photoUrl);
        void DeleteZonePhoto(int projectId, int photoId);
        void SetZoneArchive(int roomId, string url);

        Collage GetCollages(int roomId);
        int AddCollagePhoto(int projectId, int roomId, string photoUrl);
        void DeleteCollagePhoto(int projectId, int photoId);
        void SetCollageArchive(int roomId, string url);

        Plan GetPlans(int roomId);
        int AddPlanFile(int projectId, int roomId, string fileUrl);
        void DeletePlanFile(int projectId, int fileId);
        void SetPlanArchive(int roomId, string url);

        Visualization GetVisualizations(int roomId);
        void SetVisualizationCode(int roomId, string visualizationCode);

        RoomGoods GetMarketplaceGoods(int roomId);
        void AddMarketplaceGoods(RoomGoods goodsToAdd);
        void RemoveMarketplaceGoods(RoomGoods goodsToRemove);

        DevelopmentStageStatus GetStatus(int projectId, DevelopmentStage stage);
        bool SetStageStatus(int projectId, DevelopmentStage stage, User user, bool isReadyForNextStage);
        bool SkipStage(int projectId, User user, DevelopmentStage stage);
        bool CheckStageHasContent(int projectId, DevelopmentStage stage);
        void SetClientMadeDecision(int projectId, DevelopmentStage stage);
        DevelopmentStageStatus GetCurrentStageStatus(int projectId);

        void SelectRoomVisualization(int projectId, int[] roomIds, int paymentId);
        ServicePriceInformation GetVisualizationPriceForRooms(int[] roomsIds);

        void SelectRoomPlans(int projectId, int[] roomIds, int paymentId);
        ServicePriceInformation GetPlansPriceForRooms(int[] roomsIds);

        void SetVisualizationAsComplete(int roomId);
        void SetPlanAsComplete(int roomId);

        ShortOrderInformation[] GetAllOrdersIdsForProject(int projectId);

        void IssueOrder(int projectId, OrderDescription description, ProductOrder[] orderedProducts, PaymentMethod method, int deliveryPrice);
        FullProjectProductsOrder GetOrderInformation(int orderId);
        void SetOrderAdditionalPrice(int orderId, int price);
        void SetOrderDeliveryPrice(int orderId, int price);
        void SetOrderStatus(int orderId, OrderStatus status);
        void SetOrderPayment(int orderId, int paymentId);
    }
}
