﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;

namespace Faradise.Design.Services.Mock
{
    public class MockPaymentService : IPaymentService
    {
        private IDBPaymentService _dbPaymentService = null;

        public MockPaymentService(IDBPaymentService dbPaymentService)
        {
            _dbPaymentService = dbPaymentService;
        }

        public void ClosePayment(int paymentId)
        {
            _dbPaymentService.ClosePayment(paymentId);
        }

        public Payment CreateCashPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt)
        {
            var payment = new Payment()
            {
                Amount = amount.ToString(),
                Currency = "RUB",
                Description = description,
                IdempotenceKey = Guid.NewGuid().ToString(),
                PaymentTarget = paymentTarget,
                Status = PaymentStatus.Manual,
                Method = PaymentMethod.Cash,
                Reciept = Newtonsoft.Json.JsonConvert.SerializeObject(receipt)
            };
            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            return payment;
        }

        public async Task<Payment> CreateKassaPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt, string platformRoute)
        {
            var payment = new Payment()
            {
                Amount = amount.ToString(),
                Currency = "RUB",
                Description = description,
                IdempotenceKey = Guid.NewGuid().ToString(),
                PaymentRoute = "https://money.yandex.ru/payments/external/confirmation",
                PaymentTarget = paymentTarget,
                RawJson = string.Empty,
                Status = PaymentStatus.Pending,
                PlatformRoute = platformRoute,
                Reciept = Newtonsoft.Json.JsonConvert.SerializeObject(receipt),
                Method = PaymentMethod.YandexKassa
            };

            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            _dbPaymentService.UpdatePayment(payment);

            return await Task.FromResult(payment);
        }

        public Payment CreateTinkoffCreditPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt)
        {
            var payment = new Payment()
            {
                Amount = amount.ToString(),
                Currency = "RUB",
                Description = description,
                IdempotenceKey = Guid.NewGuid().ToString(),
                PaymentTarget = paymentTarget,
                Status = PaymentStatus.Manual,
                Method = PaymentMethod.Tinkoff,
                Reciept = Newtonsoft.Json.JsonConvert.SerializeObject(receipt)
            };
            var paymentId = _dbPaymentService.CreatePayment(payment);
            payment.Id = paymentId;
            return payment;
        }

        public Payment GetPayment(int paymentId)
        {
            return _dbPaymentService.GetPayment(paymentId);
        }

        public int[] GetPendingPayments(PaymentMethod method)
        {
            return _dbPaymentService.GetPendingPaymentsIds(method);
        }

        public async Task<Payment> RequestAndUpdateKassaPaymentInfo(int paymentId)
        {
            var payment = GetPayment(paymentId);
            if (payment == null)
                throw new ServiceException($"Payment id {paymentId} not found", "payment_not_found");

            await Task.Delay(3000);
            return _dbPaymentService.SetPaymentStatus(payment.Id,  PaymentStatus.Completed);
        }

        public void SetPaymentStatus(int paymentId, PaymentStatus status)
        {
            _dbPaymentService.SetPaymentStatus(paymentId, status);
        }

        public async Task<Payment> UpdateKassaPaymentRoute(int paymentId)
        {
            var payment = GetPayment(paymentId);
            if (payment.Method != PaymentMethod.YandexKassa || payment.Status == PaymentStatus.Completed)
                throw new ServiceException("payment_update_failed", "update available only for yandex kassa payments");
            if (!string.IsNullOrEmpty(payment.YandexId) && payment.Status == PaymentStatus.Pending)
                return payment;
            if (string.IsNullOrEmpty(payment.Reciept))
                throw new ServiceException("payment_update_failed", "reciept is null for payment");
            await Task.Delay(3000);
            payment.YandexId = "super_puper_id";
            payment.IdempotenceKey = Guid.NewGuid().ToString();
            _dbPaymentService.UpdatePayment(payment);
            return payment;
        }
    }
}
