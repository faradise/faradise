﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Mock
{
    public class MockSMSService : ISMSService
    {
        public string TestPhoneNumber => string.Empty;
        public string AdminNotificationPhoneNumber => string.Empty;
        public string DesignerPoolFinishedText => string.Empty;
        public string NewDesignerMessageInChatText => string.Empty;
        public string FirstDesignerMessageInChatText => string.Empty;
        public string DesignerAprovedByAdminText => string.Empty;
        public string NewClientMessageInChatText => string.Empty;
        public string DesignerAssignedToNewProjectText => string.Empty;
        public string OneClickOrderText => string.Empty;

        public string TrySendSMS(string phone, string message)
        {
            return "200";
        }
    }
}
