﻿using Faradise.Design.Models;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;

namespace Faradise.Design.Services.Mock
{
    public class MockValidationService : IValidationService
    {
        private readonly SMSOptions _configuration = null;
        private readonly IDBUserService _dbService = null;

        public MockValidationService(IOptions<SMSOptions> options, IDBUserService dbService)
        {
            _configuration = options.Value;
            _dbService = dbService;
        }

        public string SendValidationCode(string address)
        {
            var rnd = new Random();

            // TODO:: В данный момент мы не будем отправлять коды на смс
            // а используем предзаписанный

            var code = "0000";

            //var code = rnd.Next(1000, 9999).ToString();

            //var response = TrySendCode(address, code);
            //if (response != "200")
            //    throw new ServiceException("", "");

            _dbService.StoreVerificationCode(new VerificationCode { Code = code, Phone = address, Expires = DateTime.Now.Add(TimeSpan.FromMinutes(15)) });

            return code;
        }

        public bool VerifyValidationCode(string address, string code)
        {
            var storedCode = _dbService.GetVerificationCode(address);
            if (storedCode == null)
                return false;

            if (storedCode.Expires < DateTime.Now)
                return false;

            if (!storedCode.Code.Equals(code, StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }

        private string TrySendCode(string address, string code)
        {
            var path = _configuration.Host + "?login=" + _configuration.User + "&psw=" + _configuration.Password + "&phones=" + address + "&charset=utf-8&mes=" + code;
            var wc = new WebClient();
            var response = JsonConvert.DeserializeObject<SendCodeResponse>(wc.DownloadString(path));
            return response.StatusCode;
        }

        private class SendCodeResponse
        {
            [JsonProperty("statusCode")]
            public string StatusCode { get; set; }
        }
    }
}
