﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Services.Implementation.MailSending;

namespace Faradise.Design.Services.Mock
{
    public class MockMailSendingService : IMailSendingService
    {
        public string AdminNotificationEmail => string.Empty;

        public string DesignerPoolFinishedTemplateId => string.Empty;

        public string Stage3ClientGoesToNewStageTemplateId => string.Empty;

        public string DesignerAprovedByAdminTemplateId => string.Empty;

        public string DesignerDeclinedByAdminTemplateId => string.Empty;

        public string DesignerDeclinedByCleintTemplateId => string.Empty;

        public string DesignerPoolIsLowTemplateId => string.Empty;

        public string ClientReadyForDesignerPoolTemplateId => string.Empty;

        public string VisualizationTechSpecPublishedTemplateId => string.Empty;

        public string BrigadeTechSpecPublishedTemplateId => string.Empty;

        public string NewProductOrderPublishedTemplateId => string.Empty;

        public string ClientReturnUrl => string.Empty;

        public string DesignerReturnUrl => string.Empty;

        public string AdminReturnUrl => string.Empty;

        public string ProjectNeedAdminTemplateId => string.Empty;

        public string DesignerAssignedTemplateId => string.Empty;

        public string ClientBasketOrderCreatedId => string.Empty;

        public string ClientBasketOrderPaymentLinkId => string.Empty;

        public string ClientBasketOrderInWorkId => string.Empty;

        public string ClientBasketOrderCompletedId => string.Empty;

        public string BasketOrderNotificationEmail => string.Empty;

        public string AdminNewBasketOrderId => string.Empty;

        public string DesignerApprovedJunTemplateId => string.Empty;

        public string DesignerApprovedMidTemplateId => string.Empty;

        public string DesignerApprovedProTemplateId => string.Empty;

        public string CustomerDevelopmentEmail => string.Empty;

        public string CustomerDevelopmentTemplateId => string.Empty;

        public string LotteryTemplate => string.Empty;

        public void SendEmail<T>(EmailModel email) where T : TemplateModel
        {
            
        }
    }

}
