﻿using Faradise.Design.Services.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface ISMSService
    {
        string TestPhoneNumber { get; }
        string AdminNotificationPhoneNumber { get; }
        string DesignerPoolFinishedText { get; }
        string NewDesignerMessageInChatText { get; }
        string FirstDesignerMessageInChatText { get; }
        string DesignerAprovedByAdminText { get; }
        string NewClientMessageInChatText { get; }
        string DesignerAssignedToNewProjectText { get; }
        string OneClickOrderText { get; }

        string TrySendSMS(string phone, string message);
    }
}
