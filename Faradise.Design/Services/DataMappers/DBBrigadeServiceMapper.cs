﻿using Faradise.Design.Models;
using Faradise.Design.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.DataMappers
{
    public static class DBBrigadeServiceMapper
    {
        public static TechSpecFile ToService(this BrigadeTechSpecFileEntity entity)
        {
            return new TechSpecFile
            {
                FileId = entity.Id,
                Name = entity.Name,
                Link = entity.Url
            };
        }
    }
}
