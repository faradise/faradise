﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.AzureSearch;
using Faradise.Design.Models.Marketplace;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.DataMappers
{
    public static class AzureSearchDataMapper
    {
        public static ShortProductModel ToModel(this AzureProduct product)
        {
            var model = new ShortProductModel
            {
                Id = Convert.ToInt32(product.Id),
                Description = product.Description,
                PreviousPrice = product.PreviousPrice,
                Price = product.Price,
                Name = product.Name,
                Badge = product.Badge,
                BadgeDescription = product.Description,
                SaleDescription = product.SaleDescription
            };

            if (product.Pictures.Length > 0)
                model.PhotoUrl = product.Pictures[0];

            if (product.Pictures.Length > 1)
                model.AdditionalPhotoUrl = product.Pictures[1];

            

            return model;

        }

        public static ProductsQuery ToService(this DocumentSearchResult<AzureProduct> searchResult)
        {
            return new ProductsQuery
            {
                AllCount = (int)searchResult.Count,
                Products = ToService(searchResult.Results)
            };
        }

        public static ProductsQuery ToService(this List<AzureProduct> searchResult)
        {
            return new ProductsQuery
            {
                AllCount = (int)searchResult.Count,
                Products = searchResult.Select(x=>x.ToService()).ToArray()
            };
        }

        public static ProductInfo ToService(this AzureProduct product)
        {
            return new ProductInfo
            {
                Id = Convert.ToInt32(product.Id),
                PhotoUrl = product.Pictures.ToArray(),
                Description = product.Description,
                Name = product.Name,
                Price = product.Price,
                PreviousPrice = product.PreviousPrice ?? product.ActualPrice,
                Vendor = product.Vendor,
                VendorCode = product.VendorCode,
                VendorLink = product.Url,
                Badge = product.Badge,
                BadgeDescription = product.BadgeDescription,
                SaleDescription = product.SaleDescription,
                ARSupport = Convert.ToBoolean(product.ARSupport)
            };
        }

        public static ProductInfo[] ToService(this IList<SearchResult<AzureProduct>> products)
        {
            return products.Select(x => x.Document).Select(x => new ProductInfo
            {
                Id = Convert.ToInt32(x.Id),
                PhotoUrl = x.Pictures.ToArray(),
                Description = x.Description,
                Name = x.Name,
                Price = x.Price,
                PreviousPrice = x.PreviousPrice??x.ActualPrice,
                Vendor = x.Vendor,
                VendorCode = x.VendorCode,
                VendorLink = x.Url,
                Badge=x.Badge,
                BadgeDescription=x.BadgeDescription,
                SaleDescription=x.SaleDescription,
                ARSupport = Convert.ToBoolean(x.ARSupport)
            }).ToArray();
        }

        

    }
}
