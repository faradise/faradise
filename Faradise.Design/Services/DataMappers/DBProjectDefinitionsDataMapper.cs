﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.DAL;
using Faradise.Design.Models.ProjectDefinition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.DataMappers
{
    public static class DBProjectDefinitionsDataMapper
    {
        public static ProjectDefinition ToService(this ProjectDefinitionEntity entity)
        {
            return new ProjectDefinition()
            {
                Id = entity.Id,
                City = entity.City,
                ClientId = entity.ClientId,
                DesignerConfirmed = entity.DesignerId != null,
                DesignerId = entity.DesignerId != null ? entity.DesignerId.Value : 0,
                AbandonStage = entity.AbandonStage,
                Name = entity.Name,
                Stage = entity.Stage,
                Status = entity.Status,
                ProjectFill = entity.IsProjectFill,
                HasChangesAfterLastAdminCheck = entity.HasChangesAfterLastAdminCheck,
                NeedAdmin = entity.NeedAdmin,
                ProjectFillDate = entity.ProjectFillDate,
            };
        }

        


    }
}
