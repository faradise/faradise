﻿using Faradise.Design.DAL;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDefinition;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.DataMappers;
using System;

namespace Faradise.Design.Services.DataMappers
{
    public static class DBClientProjectMapper
    {
        public static ProjectDescription ToService(this ProjectDescriptionEntity project)
        {
            return new ProjectDescription()
            {
                ProjectId = project.Id,
                ObjectType = project.ObjectType,
                Rooms = project.Rooms != null ? project.Rooms.Select(x=>x.Room).ToList(): new List<Room>(),
                Reasons = project.Reasons != null ? project.Reasons.ToList().ToService() : new ReasonBinding[0],
                Styles = project.Styles != null ? project.Styles.Select(x => x.ToData()).ToArray() : new StyleBinding[0],
                Budget = new Budget()
                {
                    Furniture = project.FurnitureBudget,
                    Renovation = project.RenovationBudget
                },

                DesignerRequirements = project.DesignerRequirements != null ? project.DesignerRequirements.ToData(project.TestAnswers.ToList()) : null,
            };
        }

        public static void FillEntity(this ProjectDescriptionEntity oldProject, ProjectDescription project)
        {
            oldProject.Id = project.ProjectId;
            oldProject.ObjectType = project.ObjectType;
            oldProject.Rooms = project.Rooms != null ?project.Rooms.Select(x=>new ProjectDescriptionRoomEntity { Room = x }).ToList(): new List<ProjectDescriptionRoomEntity>();
            oldProject.Reasons = project.Reasons != null ? project.Reasons.Select(x => x.ToEntity()).ToList() : new List<ProjectDescriptionReasonEntity>();
            oldProject.Styles = project.Styles != null ? project.Styles.Select(x => x.ToEntity()).ToList() : new List<ProjectDescriptionStyleEntity>();
            oldProject.FurnitureBudget = project.Budget != null ? project.Budget.Furniture : 0;
            oldProject.RenovationBudget = project.Budget != null ? project.Budget.Renovation : 0;
            oldProject.DesignerRequirements = project.DesignerRequirements != null ? project.DesignerRequirements.ToEntity() : null;
            oldProject.TestAnswers = project.DesignerRequirements != null && project.DesignerRequirements.Testing != null ? project.DesignerRequirements.Testing.ToEntity() : new List<TestResultsAnswerEntity>();
         }

        public static ProjectDescriptionStyleEntity ToEntity(this StyleBinding x)
        {
            return new ProjectDescriptionStyleEntity()
            {
                Style = x.Name,
                Description = x.Description
            };
        }

        public static ProjectDescriptionReasonEntity ToEntity(this ReasonBinding x)
        {
            return new ProjectDescriptionReasonEntity()
            {
                Reason = x.Name,
                Description = x.Description
            };
        }

        public static List<TestResultsAnswerEntity> ToEntity(this TestResults testing)
        {
            var result = new List<TestResultsAnswerEntity>();
            foreach (var testAnswer in testing.Questions)
            {
                result.Add(new TestResultsAnswerEntity() { QuestionId = testAnswer.Index, AnswerId = testAnswer.Answer });
            }
            return result;
        }

        public static DesignerRequirementsEntity ToEntity(this DesignerRequirements designerRequirements)
        {
            return new DesignerRequirementsEntity()
            {
                Gender = designerRequirements.Gender,
                AgeFrom = designerRequirements.AgeFrom,
                AgeTo = designerRequirements.AgeTo,
                PersonalMeetingRequired = designerRequirements.NeedPersonalMeeting,
                PersonalMeetingAddress = designerRequirements.MeetingAdress,
                PersonalControlRequired = designerRequirements.NeedPersonalСontrol,
            };
        }

        public static List<ProjectDescriptionRoomEntity> ToEntity(this Dictionary<Room, int> rooms)
        {
            var result = new List<ProjectDescriptionRoomEntity>();
            foreach (var room in rooms)
            {
                result.Add(new ProjectDescriptionRoomEntity() { Room = room.Key});
            }
            return result;
        }

        public static ReasonBinding[] ToService(this List<ProjectDescriptionReasonEntity> reasons)
        {
            return reasons.Select(x => x.ToService()).ToArray();
        }

        public static ReasonBinding ToService(this ProjectDescriptionReasonEntity reason)
        {
            return new ReasonBinding()
            {
                Name = reason.Reason,
                Description = reason.Description
            };
        }

        public static StyleBinding ToData(this ProjectDescriptionStyleEntity reason)
        {
            return new StyleBinding()
            {
                Name = reason.Style,
                Description = reason.Description
            };
        }

        public static ProjectStyle ToEnumService(this ProjectDescriptionStyleEntity style)
        {
            return style.Style;
        }

        public static ProjectReason ToEnumService(this ProjectDescriptionReasonEntity reason)
        {
            return reason.Reason;
        }

        public static DesignerRequirements ToData(this DesignerRequirementsEntity designer, List<TestResultsAnswerEntity> test)
        {
            return new DesignerRequirements()
            {
                Gender = designer.Gender,
                AgeFrom = designer.AgeFrom,
                AgeTo = designer.AgeTo,
                NeedPersonalMeeting = designer.PersonalMeetingRequired,
                MeetingAdress = designer.PersonalMeetingAddress,
                NeedPersonalСontrol = designer.PersonalControlRequired,
                Testing = test != null ? test.ToService() : new TestResults() { Questions = new TestResultsAnswer[0] }
            };
        }

        public static TestResults ToService(this List<TestResultsAnswerEntity> test)
        {
            if (test == null)
                return new TestResults() { Questions = new TestResultsAnswer[0] };
            return new TestResults()
            {
                Questions = test.Select(x => new TestResultsAnswer() { Index = x.QuestionId, Answer = x.AnswerId }).ToArray()
            };
        }

        public static void AddInfo(this FullProject project, ProjectDefinitionEntity entity)
        {
            project.Id = entity.Id;
            project.Name = entity.Name;
            project.City = entity.City;
            project.BudgetFurniture = entity.Description.FurnitureBudget;
            project.BudgetRenovation = entity.Description.RenovationBudget;
            project.DesignerIsApproved = entity.DesignerConfirmed;
            project.AppointedDesignersCount = entity.DesignerPool.Where(x => x.Status == DesignerStatusInPool.Appointed).Count();
            project.RejectedDesignersCount = entity.DesignerPool.Where(x => x.Status == DesignerStatusInPool.Rejected).Count();
            project.DesignerId = entity.DesignerId == null ? 0 : entity.DesignerId.Value;
            project.ClientId = entity.ClientId;
            project.ClientName = entity.User.Name;
            project.ClientPhone = entity.User.Phone;
            project.ClientEmail = entity.User.Email;
            project.Stage = entity.Stage;
            project.Status = entity.Status;
            project.ObjectType = entity.Description.ObjectType;
            project.TestAnswers = entity.Description.TestAnswers.ToList().ToService();
            project.Rooms = entity.Description.Rooms != null ? entity.Description.Rooms.Select(x => x.Room).ToList() : new List<Room>();
            project.Styles = entity.Description.Styles != null ? entity.Description.Styles.Select(x => x.ToData()).ToList() : new List<StyleBinding>();
            project.Reasons = entity.Description.Reasons != null ? entity.Description.Reasons.Select(x => x.ToService()).ToList() : new List<ReasonBinding>();
            project.DesignerRequirements = entity.Description.DesignerRequirements != null ? entity.Description.DesignerRequirements.ToData(entity.Description.TestAnswers.ToList()) : null;
            project.IsFilled = entity.IsProjectFill;
            project.FillTime = entity.ProjectFillDate.HasValue ? entity.ProjectFillDate.Value : DateTime.MinValue;
            project.NeedAdmin = entity.NeedAdmin;
            project.HasChangesAfterLastAdminCheck = entity.HasChangesAfterLastAdminCheck;
        }
    }
}
