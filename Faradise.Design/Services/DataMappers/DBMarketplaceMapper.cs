﻿using Faradise.Design.DAL;

namespace Faradise.Design.Services.DataMappers
{
    public static class DBMarketplaceMapper
    {
        public static ProductPictureEntity ToPictureEntity(this string picture, int productId)
        {
            return new ProductPictureEntity
            {
                ProductId = productId,
                Url = picture
            };
        }
    }
}
