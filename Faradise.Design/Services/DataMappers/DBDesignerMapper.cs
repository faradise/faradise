﻿using Faradise.Design.DAL;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.ProjectDescription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.DataMappers
{
    public static class DBDesignerMapper
    {
        public static Designer ToFullServiceModel(this DesignerEntity designer, UserEntity user, ICollection<DesignerPortfolioProjectEntity>projects)
        {
            var model = new Designer
            {
                Id = designer.Id,
                Base = designer.ToService(user),
                Stage = designer.Stage,
                Phone = user.Phone,
                PortfolioLink = designer.PortfolioLink,
                Portfolio = projects.ToService(),
                TestJob = designer.TestJobInfo.ToService(),
                Styles = designer.Styles != null ? designer.Styles.ToService() : null,
                PhotoLink = designer.PhotoLink,
                Level = designer.Level,
                Possibilities = designer.Possibilities.ToService(),
                Testing = designer.TestResults.ToService(),
                LegalModel = new DesignerLegal
                {
                    LegalType = designer.LegalType,
                    CompanyLegalInfo = designer.CompanyLegalInfo.ToService(),
                    PersonalLegalInfo = designer.PersonalLegalInfo.ToService(user.Phone)
                }
            };
            if (model.Possibilities == null)
                model.Possibilities = new DesignerPossibility();
            return model;
        }

        public static DesignerBase ToService(this DesignerEntity designer, UserEntity user)
        {
            return new DesignerBase
            {
                About = designer.About,
                Age = designer.Age,
                City = designer.City,
                Email = user.Email,
                Gender = designer.Gender,
                Name = designer.Name,
                //Phone = designer.User.Phone,
                Surname = designer.Surname,
                WorkExperience = designer.WorkExperience,
                Stage=designer.Stage
            };
        }

        public static CompanyLegalInfo ToService(this DesignerCompanyLegalInfoEntity info)
        {
            if (info == null)
                return new CompanyLegalInfo();
            return new CompanyLegalInfo()
            {
                BankName = info.BankName,
                BIC = info.BIC,
                CheckingAccount = info.CheckingAccount,
                INN = info.INN,
                KPP = info.KPP,
                Name = info.Name,
                NameSoleExecutiveBody = info.NameSoleExecutiveBody,
                OGRN = info.OGRN,
                SoleExecutiveBody = info.SoleExecutiveBody
            };
        }

        public static PersonalLegalInfo ToService(this DesignerPersonalLegalInfoEntity info, string phone)
        {
            if (info == null)
                return new PersonalLegalInfo();
            return new PersonalLegalInfo()
            {
                ActualAddress = info.ActualAddress,
                DateOfIssue = info.DateOfIssue,
                DepartmentCode = info.DepartmentCode,
                Email = info.Email,
                MiddleName = info.MiddleName,
                Name = info.Name,
                PassportIssuedBy = info.PassportIssuedBy,
                PassportSeries = info.PassportSeries,
                RegistrationAddress = info.RegistrationAddress,
                Surname = info.Surname,
                Phone = phone
            };
        }

        public static Portfolio ToService(this ICollection<DesignerPortfolioProjectEntity> projects)
        {
            if (projects == null || !projects.Any())
                return new Portfolio { Portfolios = new PortfolioProject[0] };

            return new Portfolio()
            {
                Portfolios = projects.Select(x => new PortfolioProject()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    //PhotoLinks = x.Photos != null ? new PortfolioPhoto[0] : new PortfolioPhoto[0]
                    PhotoLinks = x.Photos != null ? x.Photos.Select(y => new PortfolioPhoto()
                    {
                        Id = y.Id,
                        PhotoLink = y.Url
                    })
                    .ToArray() : new PortfolioPhoto[0]

                }).ToArray()
            };
        }

        public static DesignerPossibility ToService(this DesignerPossibilityEntity possibility)
        {
            if (possibility == null)
                return null;
            return new DesignerPossibility()
            {
                Blueprints = possibility.Blueprints,
                GoToPlaceInAnotherCity = possibility.GoToPlaceInAnotherCity,
                GoToPlaceInYourCity = possibility.GoToPlaceInYourCity,
                PersonalMeeting = possibility.PersonalMeeting,
                PersonalСontrol = possibility.PersonalСontrol,
                RoomMeasurements = possibility.RoomMeasurements,
                SkypeMeeting = possibility.SkypeMeeting
            };
        }

        public static DesignerStyle[] ToService(this ICollection<DesignerStyleEntity> styles)
        {
            if (styles == null)
                return null;
            return styles.Select(x => new DesignerStyle()
            {
                Name = x.Name,
                Description = x.Description
            }).ToArray();
        }

        public static TestResults ToService(this ICollection<DesignerTestResultsAnswerEntity> answers)
        {
            if (answers == null)
                return new TestResults();
            return new TestResults()
            {
                Questions = answers.Select(x => new TestResultsAnswer()
                {
                    Index = x.QuestionId,
                    Answer = x.AnswerId
                }).ToArray()
            };
        }

        public static DesignerTestResultsAnswerEntity[] ToService(this TestResults answers, int designerId)
        {
            return answers.Questions.Select(x => new DesignerTestResultsAnswerEntity()
            {
                DesignerId = designerId,
                AnswerId = x.Answer,
                QuestionId = x.Index

            }).ToArray();
        }

        public static void ToEntity(this DesignerBase designerBase, DesignerEntity designer)
        {
            designer.About = designerBase.About;
            designer.Age = designerBase.Age;
            designer.City = designerBase.City;
            designer.User.Email = designerBase.Email.ToLower();
            designer.Gender = designerBase.Gender;
            designer.Name = designerBase.Name;
            //designer.User.Phone = designerBase.Phone;
            designer.Surname = designerBase.Surname;
            designer.WorkExperience = designerBase.WorkExperience;
            designer.Stage = designerBase.Stage;

        }

        public static void ToEntity(this DesignerLegal legalModel, DesignerEntity designer)
        {
            if(legalModel.LegalType==Models.LegalType.Company)
            {
                if (designer.CompanyLegalInfo == null)
                    designer.CompanyLegalInfo = new DesignerCompanyLegalInfoEntity();
                designer.CompanyLegalInfo.BankName = legalModel.CompanyLegalInfo.BankName;
                designer.CompanyLegalInfo.BIC = legalModel.CompanyLegalInfo.BIC;
                designer.CompanyLegalInfo.CheckingAccount = legalModel.CompanyLegalInfo.CheckingAccount;
                designer.CompanyLegalInfo.INN = legalModel.CompanyLegalInfo.INN;
                designer.CompanyLegalInfo.KPP = legalModel.CompanyLegalInfo.KPP;
                designer.CompanyLegalInfo.Name = legalModel.CompanyLegalInfo.Name;
                designer.CompanyLegalInfo.NameSoleExecutiveBody = legalModel.CompanyLegalInfo.NameSoleExecutiveBody;
                designer.CompanyLegalInfo.OGRN = legalModel.CompanyLegalInfo.OGRN;
                designer.CompanyLegalInfo.SoleExecutiveBody = legalModel.CompanyLegalInfo.SoleExecutiveBody;
            }

            else
            {
                if (designer.PersonalLegalInfo == null)
                    designer.PersonalLegalInfo = new DesignerPersonalLegalInfoEntity();
                designer.PersonalLegalInfo.ActualAddress = legalModel.PersonalLegalInfo.ActualAddress;
                designer.PersonalLegalInfo.DateOfIssue = legalModel.PersonalLegalInfo.DateOfIssue;
                designer.PersonalLegalInfo.DepartmentCode = legalModel.PersonalLegalInfo.DepartmentCode;
                designer.PersonalLegalInfo.Email = legalModel.PersonalLegalInfo.Email.ToLower();
                designer.PersonalLegalInfo.MiddleName = legalModel.PersonalLegalInfo.MiddleName;
                designer.PersonalLegalInfo.Name = legalModel.PersonalLegalInfo.Name;
                designer.PersonalLegalInfo.PassportIssuedBy = legalModel.PersonalLegalInfo.PassportIssuedBy;
                designer.PersonalLegalInfo.PassportSeries = legalModel.PersonalLegalInfo.PassportSeries;
                designer.PersonalLegalInfo.RegistrationAddress = legalModel.PersonalLegalInfo.RegistrationAddress;
                designer.PersonalLegalInfo.Surname = legalModel.PersonalLegalInfo.Surname;
            }
        }

        public static void ToEntity (this DesignerPossibility possibility, DesignerPossibilityEntity designerPossibility)
        {
            designerPossibility.Blueprints = possibility.Blueprints;
            designerPossibility.GoToPlaceInAnotherCity = possibility.GoToPlaceInAnotherCity;
            designerPossibility.GoToPlaceInYourCity = possibility.GoToPlaceInYourCity;
            designerPossibility.PersonalMeeting = possibility.PersonalMeeting;
            designerPossibility.PersonalСontrol = possibility.PersonalСontrol;
            designerPossibility.RoomMeasurements = possibility.RoomMeasurements;
            designerPossibility.SkypeMeeting = possibility.SkypeMeeting;
        }

        public static TestJobInfo ToService(this DesignerTestJobInfoEntity info)
        {
            if (info == null)
                return null;
            return new TestJobInfo
            {
                TestLink = info.TestLink,
                TestTime = info.TestTime,
                TestApproved = info.TestApproved
            };
        }

    }
}
