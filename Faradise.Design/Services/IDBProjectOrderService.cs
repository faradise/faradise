﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBProjectOrderService
    {
        int CreateNewOrder(int projectId, OrderDescription description, PaymentMethod method, int deliveryCost);
        int UpdateOrderBasePrice(int orderId);
        void SetOrderPayment(int orderId, int paymentId);
        void ClearPayment(int orderId);

        ShortOrderInformation[] GetAllOrdersIdsForProject(int projectId);
        FullProjectProductsOrder GetOrderInformation(int orderId);
        void SetOrderAdditionalPrice(int orderId, int price);
        void SetOrderDeliveryPrice(int orderId, int price);
        void SetOrderStatus(int orderId, OrderStatus status);
        void RemoveGoodsFromOrder(int orderId, RoomGoods goodsToRemove);
        void CloseOrder(int orderId);
        void SetOrderIsReady(int orderId);
    }
}
