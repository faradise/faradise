﻿using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDefinition;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IProjectDefinitionService
    {
        ProjectDefinition GetProjectDefinition(int projectId);
        ProjectDefinition[] GetProjectsByUser(int userId);

        int CreateProjectDefinition(int clientId);
        void SetProjectName(int projectId, string name);
        void SetProjectCity(int projectId, string name);

        int GetDesigner(int projectId);
        void AssignDesigner(int projectId, int designerId);
        void AbandonDesigner(int projectId, string description);
        void ConfirmDesigner(int projectId);

        bool CheckUserOwnsService(int projectId, int userId);
        void SetStage(int projectId, ProjectStage stage);
        void CloseProject(int projectId);
        void FinishFill(int projectId, User user);
        void SetProjectNeedAdminAttention(int projectId, bool needAttention);
        void SetProjectAsViewedByAdmin(int projectId, bool isChanged);
    }
}
