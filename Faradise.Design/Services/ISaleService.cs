﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Sale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface ISaleService
    {
        Sale CreateSale(string name);
        void UpdateSale(Sale sale);
        void DeleteSale(int saleId);
        SaleShortInfo[] GetSales();
        Sale GetSale(int saleId);
        void SetSaleActive(int saleId, bool isActive);
    }
}
