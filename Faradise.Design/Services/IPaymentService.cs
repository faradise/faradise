﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IPaymentService
    {
        Task<Payment> CreateKassaPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt, string platformRoute);
        Payment CreateCashPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt);
        Payment CreateTinkoffCreditPayment(PaymentTargetType paymentTarget, float amount, string description, KassaReceiptInfo receipt);
        Payment GetPayment(int paymentId);
        Task<Payment> RequestAndUpdateKassaPaymentInfo(int paymentId);
        void SetPaymentStatus(int paymentId, PaymentStatus status);
        int[] GetPendingPayments(PaymentMethod method);
        Task<Payment> UpdateKassaPaymentRoute(int paymentId);
        void ClosePayment(int paymentId);
    }
}
