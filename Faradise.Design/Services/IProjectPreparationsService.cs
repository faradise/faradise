﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectPreparations;
using Faradise.Design.Models.ProjectDescription;

namespace Faradise.Design.Services
{
    public interface IProjectPreparationsService
    {
        void SetInfoAboutFavoriteStyles(int projectId, Dictionary<ProjectStyle,string> styles);
        void SetInfoAboutProjectRooms(int projectId, List<ShortRoomInfo> rooms);
        void SetAdditionalInformation(int projectId, List<AdditionalInformationForRoom> rooms);
        void SetBudgetForProject(int projectId, int furnitureBudget, int renovationBudget);

        int AddRoomPhoto(int roomId, string photoUrl);
        int AddOldFurniturePhoto(int projectId, string photoUrl);
        int AddOldFurnishPhoto(int projectId, string photoUrl);
        void SetPlan(int roomId, string photoUrl);

        void DeleteRoomPhoto(int photoId);
        void DeleteOldFurniturePhoto(int photoId);
        void DeleteOldFurnishPhoto(int photoId);
        void DeletePlan(int roomId);

        List<RoomInfo> GetRooms(int projectId);
        Dictionary<ProjectStyle, string> GetStyles(int projectId);
        ClientDesignerBudget GetBudget(int projectId);
        List<AdditionalInformationForRoom> GetRoomAdditionalInfos(int projectId);
        OldFurniture GetOldFurniture(int projectId);

        bool SetStageStatus(int projectId, CooperateStage stage, Role role, bool isReadyForNextStage);
        StageStatus GetCurrentStage(int projectId);
        bool CheckIfStageIsCurrent(int projectId, CooperateStage stage);
        void ConfirmDesigner(int projectId);

        void StartStagePreparations(int ptojectId);

        void SkipStage(int projectId, CooperateStage stage);
    }
}
