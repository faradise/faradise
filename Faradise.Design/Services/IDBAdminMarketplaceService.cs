﻿using System.Threading.Tasks;
using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.Admin.Product;
using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Services
{
    public interface IDBAdminMarketplaceService
    {
        ModerationCountsModel GetModerationProduct();
        GetModerationProductModel GetProductForModeration(string type);

        void SetModerationProduct(ModerationProductModel model);

        ProductsStats GetProductsStats();
        ARProductFullInfo GetArProductFullInfo(int arProductId);
        void MapARProductToProduct(int? productId, int arProductId);
        ARProductShortInfo[] GetArProductList();
        PossibleDailyProduct[] GetPossibleDailyProducts(int offset, int count, float companyKoef, float categoryKoef, float saleKoef);
        DailyProduct GetDailyProduct();
        void SetDailyProduct(int productId);
        DailyProduct[] GetPreviousDailyProducts();

        Task SetProductImagesPriority(ProductImages model);
        Task<ProductImages> GetProductImages(int productId);
    }
}
