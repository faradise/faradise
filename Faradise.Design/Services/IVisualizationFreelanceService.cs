﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models;
using Faradise.Design.Models.VisualizationFreelance;

namespace Faradise.Design.Services
{
    public interface IVisualizationFreelanceService
    {
        VisualisationTechSpec GetVisualizationTechSpec(int projectId, int roomId, int designerId);
        bool HasVisualizationTechSpec(int projectId, int roomId);
        void CreateVisualizationTechSpec(int projectId, int roomId, int designerId);
        void SetDescription(int projectId, int roomId, string description);
        int AddFile(int projectId, int roomId, string fileLink, string filename);
        void RemoveFile(int projectId, int roomId, int fileId);
        void ChangeDesigner(int projectId, int roomId, int designerId);
        void Publish(int projectId, int roomId, int designerId);
        void Complete(int projectId, int roomId);
    }
}
