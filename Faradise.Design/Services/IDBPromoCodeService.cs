﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.PromoCode;
using System;

namespace Faradise.Design.Services
{
    public interface IDBPromoCodeService
    {
        PromoCode GetPromoCode(string code);
        PromocodeProduct[] GetProductsByIds(ProductCount[] products);
        PromocodeProduct[] GetProductsFromBasket(int userId);
        void DisablePromocode(string code);
        PromoCodeExport[] GetPromoCodeExports();
        PromoCodeExport GetPromoCodeExport(string exportname);
        PromoCode[] GetPromoCodesForExport(string exportname);
        void CreatePromoCodeExport(string name, int count, int minPrice, int percentsDiscount, int absoluteDiscount, PromoCodeType codeType, DateTime startDate, DateTime expireDate);
        void CreatePromoCodeForExport(string exportname, int count);
    }
}
