﻿using Faradise.Design.Models.CategoryParameters;

namespace Faradise.Design.Services
{
    public interface IDBCategoryParameterService
    {
        ParameterInfo[] GetAllParameters();
        void CreateParameter(ParameterInfo parameter);
        void DeleteParameter(int id);
        ParameterInfo[] GetParametersByCategoryId(int id);
        ParameterInfo GetParameter(int id);
        void UpdateParameter(ParameterInfo model);
    }
}