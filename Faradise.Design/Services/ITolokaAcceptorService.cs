﻿using System;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Toloka;

namespace Faradise.Design.Services
{
    public interface ITolokaAcceptorService
    {
        Task<bool> Accept(TolokaOutput output, TolokaOutputType type, int taskId);
        Task<bool> AcceptSegment(TolokaOutput output, TolokaOutputType type, int taskId);
    }
}
