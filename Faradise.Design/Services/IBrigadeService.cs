﻿using Faradise.Design.Models.Brigade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IBrigadeService
    {
        BrigadeTechSpec GetBrigadeTechSpec(int projectId, int designerId);
        bool HasTechSpec(int projectId);
        void CreateTechSpec(int projectId, int designerId);
        void SetDescription(int projectId, string description);
        int AddFile(int projectId, string fileLink, string filename);
        void RemoveFile(int projectId, int fileId);
        void Publish(int projectId, int designerId);
        void Complete(int projectId);
    }
}
