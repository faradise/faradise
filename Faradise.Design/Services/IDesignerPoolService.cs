﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.DesignerPool;

namespace Faradise.Design.Services
{
    public interface IDesignerPoolService
    {
        DesignerDatingInfo[] GetDesignersPool(int projectId);
        void AddDesignerToPool(int projectId, int userId);
        void RemoveDesignerFromPool(int projectId, int userId, bool isRejected = true, string description = null);
        void RemoveDesignersFromPool(int projectId, RemoveDesigner[] designers);
        void RefreshDesignersPool(int projectId);
        int GetDesignersCount(int projectId);
        void SetPoolIsFinished(int projectId);

        DesignerInfo[] GetProjectSuitableDesigners(int projectId);
        DesignerRejectedInfo[] GetProjectRejectedDesigners(int projectId);
        DesignerInfo[] GetProjectAppointedDesigners(int projectId);
    }
}
