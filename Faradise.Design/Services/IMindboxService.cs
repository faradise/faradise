﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IMindboxService
    {
        string GenerateTicketForUser(string userPhone);
    }
}
