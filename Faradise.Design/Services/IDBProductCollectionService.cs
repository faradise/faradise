﻿using Faradise.Design.Models.ProductCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBProductCollectionService
    {
        Task<List<ProductName>> GetProductsForCompanyAsync(int companyId, int skipProducts, int takeProducts);
        List<ProductName> GetProductsForCompany(int companyId, int skipProducts, int takeProducts);
        List<string> GetBlackList();
        void AddWordToBackList(string word);
        void RemoveWordFromBlackList(string word);
        Task<Dictionary<string, List<ProductName>>> FindProductCollectionsForCompany(int companyId,int takeProducts);
        Task UpdateProductCollectionForCompany(int companyId, int takeProducts);
        List<ProductCollection> GetCollectionsForCompany(int companyId);
        List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId);
        ProductCollection GetCollection(int collectionId);
        void ChangeCollection(int collectionId, string name, bool isAvalable);
        int[] GetCompaniesToUpdate();
    }
}
