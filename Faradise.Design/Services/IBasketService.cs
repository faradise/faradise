﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IBasketService
    {
        void AddProductToBasket(int userId, int productId, int count);
        void RemoveProductFromBasket(int userId, int productId, int count);
        void RemoveAllProductsFromBasket(int userId, int productId);
        BasketProduct[] GetAllProductsInBasket(int userId);
        void SetBasketBin(int userId, ProductCount[] products);
        void AddProductsListToBasket(int userId, ProductCount[] products);

        int CreateOrderFromBasket(int userId, OrderDescription description, PaymentMethod method, int deliveryCost);
        int CreateOneClickOrder(int userId, int productId, int productCount, OneClickOrderDescription description, PaymentMethod method, int deliveryCost);
        void AssignPaymentToOrder(int orderId, Payment payment);
        void RemoveProductsFromOrder(int orderId, int productId, int count);
        void AddProductToOrder(int orderId, int productId, int count);
        ShortOrderInformation[] GetOrders(int from, int count);
        FullBasketOrderInformation GetOrderInfo(int orderId);

        void SetOrderAdditionalPrice(int orderId, int price);
        void SetOrderDeliveryPrice(int orderId, int price);
        int RecalculateOrderBasePrice(int orderId);
        void FinishOrder(int orderId);
        void CloseOrder(int orderId);
        void SetOrderStatus(int orderId, OrderStatus status);
        void SetOrderIsReady(int orderId);
        void ChangeOrder(ChangeBasketOrder changes);
        void ChangePromoCodeForOrder(string promocode, int orderId);
        void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod);

        bool IsOrderOwnedByUser(int orderId, int userId);
        int GetOrderIdByPayment(int paymentId);
        void ClearBasket(int userId);
        void ClearPayment(int orderId);
        int SaveBasket(ProductCount[] productsIds, int userId);
        BasketProduct[] GetAllProductsInSavedBasket(int basketId, int userId);
        void AddProductToBasketFromSavedBasket(int user, int savedBasketId);
        SavedBasketShortInfo[] GetAllSavedBasketByUser(int userId);
    }
}
