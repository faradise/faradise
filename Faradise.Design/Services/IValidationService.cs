﻿namespace Faradise.Design.Services
{
    public interface IValidationService
    {
        string SendValidationCode(string address);
        bool VerifyValidationCode(string address, string code);
    }
}