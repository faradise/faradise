﻿using Faradise.Design.Models.Badge;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IBadgeService
    {
        Badge CreateNewBadge(string name);
        void UpdateBadge(Badge updateInfo);
        void DeleteBadge(int badgeId);
        BadgeShortInfo[] GetBadges();
        Badge GetBadge(int badgeId);
        void SetBadgeActive(int badgeId, bool isActive);
    }
}
