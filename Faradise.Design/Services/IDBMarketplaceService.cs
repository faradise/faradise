﻿using System;
using System.Collections.Generic;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Petrovich;
using Faradise.Design.Models.YMLUpdater;

namespace Faradise.Design.Services
{
    public interface IDBMarketplaceService
    {
        List<Category> GetCategories();
        List<CompanyInfo> GetAllCompanies(bool loadAdditinalInfo);
        Company GetCompanyById(int Id);
        string GetCompanyNameById(int id);
        List<CompanyCategory> GetCompanyCategories(int companyId);
        CompanyCategory GetCompanyCategory(int companyId, int companyCategoryId);
        Product GetProductById(int productId);
        List<Product> GetProductsByVendorCode(string vendorCode);
        List<GetCategoryProductsModel> GetCategoryProducts(int companyId, int companyCategoryId);
        List<Category> GetSubcategories(int id);
        ProductInfo GetShortProductInfoById(int productId);
        List<CategoryInfo> GetCategoryPath(int categoryId);

        CompanyColor[] GetCompanyColors();
        RoomName[] GetRoomNames();
        MarketplaceColor[] GetMarketplaceColors();

        RoomName[] GetCategoryRoomsByCompanyCategory(int companyId, int companyCategoryId);
        
        int AddCategory(Category category);
        void EnableCategory(int categoryId);
        void DisableCategory(int categoryId);
        bool EditCategory(Category category);
        void DeleteCategory(int categoryId);
        bool ChangeProductCategory(int productId, int categoryId);

        int CreateCompany();
        void EnableCompany(int companyId);
        void DisableCompany(int companyId);
        bool ModifyCompany(Company company);
        void AddCompanyNote(int companyId, string note);
        void DeleteCompanyNote(int companyId, int noteId);
        void ChangeCompanyNote(int companyId, int noteId, string newNote);
        bool MapCompanyCategories(List<MapCategory> categories, int companyId);
        void UpdateCompany(UpdateCompanyData data);
        

        int AddCategoryImage(string url);
        Dictionary<int, string> GetCategoryImages();
        int AddRoomName(string roomName);
        bool EditRoomName(RoomName room);
        
        bool AddCompanyCategoryRoom(CompanyCategoryRoom room);
        bool DeleteCompanyCategoryRoom(CompanyCategoryRoom room);
        int AddMarketplaceColor(MarketplaceColor model);
        bool EditMarketplaceColor(MarketplaceColor marketplaceColor);
        void UpdateCompanyColors(IEnumerable<string> colors);
        bool AddAllCompanyCategoryRooms(int companyId, int companyCategoryId);
        bool DeleteAllCompanyCategoryRooms(int companyId, int companyCategoryId);
        int[] MapCompanyCategory(MapCategory mapCategory);
        bool MapPetrovichCategories(PetrovichListModel petrovichList, int companyId);


        bool MapCompanyColor(int companyColorId, int marketplaceColorId);

        int AddYMLUpdateTask(int companyId, string ymlLink, UploadFiletype filetype, int priority, YMLDownloadSource downloadSource);
        YMLUpdateTask GetNextYmlUpdateTask();
        YMLUpdateTask GetYmlUpdateTask(int taskId);
        YMLUpdateTask[] GetAllPendingYMLUpdateTasks();
        void DeleteYMLUpdateTask(int taskId);
        void MarkYMLUpdateTaskAsFailed(int taskId, string failMessage);
        void MarkYMLUpdateTaskAsProcessing(int taskId);
        void MarkYMLUpdateTaskAsCompleted(int taskId);
        void MarkYMLUpdateTaskAsPending(int taskId);
        List<CategoryInfo> GetProductCategoryPath(int productId);
        void CreateAutofeedTasks(int withpriority, TimeSpan minTimeAfterLastUpdate);
        void RefreshProcessingUploads();

        void SetCompanyAutofeedStatus(int companyId, bool isEnabled);
        CompanyLastFeedInfo GetCompanyLastFeedInfo(int companyId);
    }
}
