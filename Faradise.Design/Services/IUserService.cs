﻿using Faradise.Design.Models;

namespace Faradise.Design.Services
{
    public interface IUserService
    {
        User CreateUser(Role role);
        User ExtractUserById(int id);
        User ExtractUserByAlias(string aliasString, AliasType aliasType);

        void AddAlias(int id, AliasType type, string value);
        void AddRole(int id, Role role);
        void AddEmail(int id, string email);

        void AddUserName(int id, string userName);
        void AddUserPhone(int id, string userPhone);

        bool CheckPhoneInUse(string fixedPhone);
    }
}
