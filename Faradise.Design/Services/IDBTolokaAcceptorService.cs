﻿using Faradise.Design.DAL;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IDBTolokaAcceptorService
    {
        MarketplaceColorEntity[] GetMarketplaceColors();
        CompanyColorEntity[] GetCompanyColors(MarketplaceColorEntity[] marketplaceColors);
        ProductEntity[] GetProducts(List<int> ids);
        void SetProducts();
        string SetColors(ProductEntity product, List<int> colors, MarketplaceColorEntity[] marketplaceColors, CompanyColorEntity[] companyColors);
        string SetDescriptions(string ids, string descriptions);
    }
}
