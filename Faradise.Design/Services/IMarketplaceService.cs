﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Enums;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.YMLUpdater;

namespace Faradise.Design.Services
{
    public interface IMarketplaceService
    {
        List<Category> GetCategories();
        List<Category> GetSubcategories(int id);
        List<CategoryInfo> GetCategoryPath(int categoryId);
        int AddCategory(Category category);
        void EnableCategory(int categoryId);
        void DisableCategory(int categoryId);
        void DeleteCategory(int categoryId);
        CompaniesModel GetAllCompanies(bool loadAdditinalInfo);
        CompanyModel GetCompanyById(int id);
        bool EditCategory(ModifyCategoryModel category);
        bool ModifyCompany(ModifyCompanyModel company);
        int CreateCompany();
        void EnableCompany(int companyId);
        void DisableCompany(int companyId);
        void AddCompanyNote(int companyId, string note);
        void DeleteCompanyNote(int companyId, int noteId);
        void ChangeCompanyNote(int companyId, int noteId, string newnote);
        CompanyCategoriesModel GetCompanyCategories(int companyId);
        bool MapCompanyCategories(CompanyCategoriesModel model);
        void UpdateCompany(MarketplaceCompany company);
        
        Product GetProductById(int productId);
        List<Product> GetProductsByVendorCode(string vendorCode);
        
        int SaveCategoryImage(string url);
        Dictionary<int, string> GetCategoryImages();
        bool AddCategoryRoom(string roomName);
        bool EditCategoryRoom(RoomNameModel model);
        RoomNameModel[] GetCategoryRooms();
        bool AddCompanyCategoryRoom(CompanyCategoryRoomModel model);
        bool DeleteCompanyCategoryRoom(CompanyCategoryRoomModel model);
        RoomNameModel[] GetRoomsInCompanyCategory(CompanyCategoryKeyModel companyCategoryKey);
        bool AddMarketplaceColor(MarketplaceColorModel model);
        bool EditMarketplaceColor(MarketplaceColorModel model);
        MarketplaceColorModel[] GetMarketplaceColors();
        bool DeleteAllCompanyCategoryRooms(CompanyCategoryKeyModel companyCategoryKey);
        bool AddAllCompanyCategoryRooms(CompanyCategoryKeyModel companyCategoryKey);
        int[] MapCompanyCategory(MapCategoryModel model);
        CompanyColorModel[] GetCompanyColors();
        bool MapCompanyColor(MapColorModel model);
        CompanyCategoryModel GetCompanyCategory(int companyId, int companyCategoryId);

        int AddYMLUpdateTask(int companyId, string ymlLink, UploadFiletype filetype, int priority, YMLDownloadSource downloadSource);
        YMLUpdateTask GetNextYmlUpdateTask();
        YMLUpdateTask[] GetAllPendingYMLUpdateTasks();
        void DeleteYMLUpdateTask(int taskId);
        void MarkYMLUpdateTaskAsFailed(int taskId, string failMessage);
        void MarkYMLUpdateTaskAsProcessing(int taskId);
        void MarkYMLUpdateTaskAsPending(int taskId);
        Task ProcessYMLUploadForced(int taskId);

        void SetCompanyAutofeed(int companyId, bool isEnabled);

        List<OrderedProduct> GetProductsFromOrder(FullProjectProductsOrder order);
        List<OrderedProduct> GetProductsFromOrder(FullBasketOrderInformation order);
    }
}
