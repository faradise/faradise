﻿namespace Faradise.Design.Services.Configs
{
    public class CDNOptions
    {
        public string Host { get; set; }
        public string Container { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}
