﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Configs
{
    public class MarketplaceOptions
    {
        public string ProjectOrderRedirectUrl { get; set; }
        public string BasketOrderRedirectUrl { get; set; }
        public string PlatformPaymentRouteBase { get; set; }
        public string SavedBasketsRouteBase { get; set; }
    }
}
