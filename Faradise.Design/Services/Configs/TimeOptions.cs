﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Configs
{
    public class TimeOptions
    {
        public int RoomMoodboardTime { get; set; }
        public int RoomCollageTime { get; set; }
        public int RoomZoningTime { get; set; }
        public int RoomVisulizationTime { get; set; }
        public int RoomPlanTime { get; set; }
    }
}
