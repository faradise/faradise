﻿namespace Faradise.Design.Services.Configs
{
    public class AzureSearchOptions
    {
        public string SearchServiceAdminApiKey { get; set; }
        public string SearchServiceName { get; set; }
    }
}
