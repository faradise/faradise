﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Configs
{
    public class YandexKassaOptions
    {
        public string ShopId { get; set; }
        public string ShopKey { get; set; }
        public string RedirectUrl { get; set; }
    }
}
