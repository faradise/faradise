﻿namespace Faradise.Design.Services.Configs
{
    public class EmailOptions
    {
        public string UniOneApiKey { get; set; }
        public string UniOneUserName { get; set; }
        public string UniOneSendUrl { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string OrderSenderEmail { get; set; }

        public string AdminNotificationEmail { get; set; }
        public string BasketOrderNotificationEmail { get; set; }
        public string CustomerDevelopmentEmail { get; set; }

        public string DesignerPoolFinishedTemplateId { get; set; }

        public string Stage3ClientGoesToNewStageTemplateId { get; set; }

        public string DesignerDeclinedByAdminTemplateId { get; set; }

        public string DesignerDeclinedByCleintTemplateId { get; set; }

        public string DesignerPoolIsLowTemplateId { get; set; }

        public string ClientReadyForDesignerPoolTemplateId { get; set; }

        public string VisualizationTechSpecPublishedTemplateId { get; set; }
        public string BrigadeTechSpecPublishedTemplateId { get; set; }
        public string NewProductOrderPublishedTemplateId { get; set; }
        public string ProjectNeedAdminTemplateId { get; set; }
        public string DesignerAssignedTemplateId { get; set; }

        public string ClientBasketOrderCreatedId { get; set; }
        public string ClientBasketOrderPaymentLinkId { get; set; }
        public string ClientBasketOrderInWorkId { get; set; }
        public string ClientBasketOrderCompletedId { get; set; }
        public string AdminNewBasketOrderId { get; set; }

        public string ClientReturnUrl { get; set; }
        public string DesignerReturnUrl { get; set; }
        public string AdminReturnUrl { get; set; }

        public string DesignerApprovedJunTemplateId { get; set; }
        public string DesignerApprovedMidTemplateId { get; set; }
        public string DesignerApprovedProTemplateId { get; set; }

        public string CustomerDevelopmentTemplateId { get; set; }

        public string LotteryTemplate { get; set; }
    }
}
