﻿namespace Faradise.Design.Services.Configs
{
    public class SMSOptions
    {
        public string Host { get; set; }
        public string Login { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Container { get; set; }
        public string TestPhoneNumber { get; set; }

        public string AdminNotificationPhoneNumber { get; set; }

        public string DesignerPoolFinishedText { get; set; }
        public string NewDesignerMessageInChatText { get; set; }
        public string FirstDesignerMessageInChatText { get; set; }
        public string DesignerAprovedByAdminText { get; set; }
        public string NewClientMessageInChatText { get; set; }
        public string DesignerAssignedToNewProjectText { get; set; }
        public string OneClickOrderText { get; set; }
    }
}