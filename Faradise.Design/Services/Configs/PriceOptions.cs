﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Configs
{
    public class PriceOptions
    {
        public int RoomVisualizationPrice { get; set; }
        public int RoomPlanPrice { get; set; }
    }
}
