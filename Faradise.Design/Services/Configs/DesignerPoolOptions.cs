﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Configs
{
    public class DesignerPoolOptions
    {
        public int StackSize { get; set; }
        public int NotificationBorder { get; set; }
        public int MaxPoolSize { get; set; }
    }
}
