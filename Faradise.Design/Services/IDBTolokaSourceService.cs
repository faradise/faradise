﻿using Faradise.Design.Models.Toloka.Input;

namespace Faradise.Design.Services
{
    public interface IDBTolokaSourceService
    {
        TolokaInputSize[] ExtractSizeSource(int category, int offset, int count);
        TolokaInputProducts[] ExtractProducts(int category, int offset, int count, string vendor);
        TolokaInputPhoto[] ExtractPhotoSource(int category, int offset, int count, int countPhoto);
        TolokaInputPhoto[] ExtractPhotoColorSource(int category, int offset, int count, int countPhoto);
        TolokaInputProducts[] ExtractDescriptionSource(int category, int offset, int count, string vendor, int maxLetter);
    }
}
