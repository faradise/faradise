﻿using Faradise.Design.Models;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IDesignerService
    {
        bool CreateDesigner(int id);
        Designer GetDesigner(int id);
        Designer[] GetDesigners(int offset, int count);
        bool SetDesigner(int id, Designer model);

        void SetBaseModel(int id, DesignerBase baseModel);
        void SetPortfolioLink(int id, string portfoliolink);
        void SetTestLink(int id, string testlink);
        void SetStyles(int id, DesignerStyle[] styles);
        void SetTestApproved(int id, bool approved);
        void SetPhotoLink(int id, string link);
        void SetTariff(int id, DesignerLevel tariff);
        void SetPossibilities(int id, DesignerPossibility possibilities);
        void SetTest(int id, TestResults test);
        void SetLegalModel(int id, DesignerLegal legal);
        void SetPhone(int id, string phone);
        void AddBaseInfoToLegalInfo(int id, string name, string surname, string phone);

        DesignerBase GetBaseModel(int id);
        DesignerRegistrationStage GetRegistrationStage(int id);
        string GetPortfolioLink(int id);
        TestJobInfo GetTestJobInfo(int id);
        DesignerStyle[] GetStyles(int id);
        string GetPhotoLink(int id);
        DesignerLevel GetLevel(int id);
        DesignerPossibility GetPossibilities(int id);
        TestResults GetTest(int id);
        DesignerLegal GetLegalModel(int id);
        string GetPhone(int id);
        int[] GetDesignerProjects(int designerId);

        TestQuestionDescription[] GetDesignerQuestions();

        int CreatePortfolio(int id);
        void RemovePortfolio(int id, int portfolioId);
        Portfolio GetPortfolio(int id);
        void SetPortfolio(int id, int portfolioId, string name, string description);
        void SetPortfolios(int id, PortfolioDescriptions model);
        void RemovePortfolioPhoto(int id, RemovePortfolioPhoto model);
        int AddPortfolioPhoto(int id, int portfolioId, string path);

        void AddAvatar(int id, string path);
        void RemoveAvatar(int id);

        void DeleteDesigner(int designerId);
        List<DesignerShortInfo> GetShortInfos();
    }
}
