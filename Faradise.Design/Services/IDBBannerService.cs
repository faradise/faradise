﻿using Faradise.Design.Controllers.API.Models.Banner;

namespace Faradise.Design.Services
{
    public interface IDBBannerService
    {
        BannerModel[] GetBanners();
        int CreateBanner(BannerModel model);
        BannerModel GetBanner(int id);
        bool UpdateBanner(BannerModel model);
        void ToggleBannerState(int id);
        void DeleteBanner(int id);
    }
}