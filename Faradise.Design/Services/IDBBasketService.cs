﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBBasketService
    {
        void AddProductToBasket(int userId, int productId, int count);
        void RemoveProductFromBasket(int userId, int productId, int count);
        void RemoveAllProductsFromBasket(int userId, int productId);
        void SetBasketBin(int userId, ProductCount[] products);
        void AddProductsListToBasket(int userId, ProductCount[] products);
        BasketProduct[] GetAllProductsInBasket(int userId);

        int CreateOneClickOrder(int userId, int productId, int productCount, OneClickOrderDescription description, PaymentMethod method, int deliveryCost);
        int CreateOrderFromBasket(int userId, OrderDescription description, PaymentMethod method, int deliveryCost);
        void ChangeOrder(ChangeBasketOrder changes);
        void AssignPaymentToOrder(int orderId, Payment payment);
        void ClearPayment(int orderId);
        void SetOrderStatus(int orderId, OrderStatus status);
        void RemoveProductsFromOrder(int otderId, int productId, int count);
        void AddProductToOrder(int orderId, int productId, int count);
        ShortOrderInformation[] GetOrders(int from, int count);
        FullBasketOrderInformation GetOrderInfo(int orderId);
        PaymentMethod GetOrderPaymentMethod(int orderId);

        void SetBasePrice(int orderId, int price);
        void SetAdditionalPrice(int orderId, int price);
        void SetDeliveryPrice(int orderId, int price);
        void CloseOrder(int orderId);
        void ChangePromoCodeForOrder(string promocode, int orderId);

        bool IsOrderOwnedByUser(int orderId, int userId);
        void ClearBasket(int userId);
        int GetOrderIdByPayment(int paymentId);

        int[] GetProductsIdsInBasket(int userId);
        bool CheckProductInBasket(int userId, int productId);
        void SetOrderIsReady(int orderId);
        int SaveBasket(ProductCount[] productsIds, int userId);
        BasketProduct[] GetAllProductsInSavedBasket(int basketId, int userId);
        ProductCount[] GetShortProductListFromSavedBasket(int savedBasketId);
        SavedBasketShortInfo[] GetAllSavedBasketsByUser(int userId);
        void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod);
    }
}
