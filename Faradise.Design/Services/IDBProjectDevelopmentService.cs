﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectPreparations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBProjectDevelopmentService
    {
        void CreateProjectDevelopment(int projectId, List<RoomInfo> rooms);
        bool HasProjectDevelopment(int projectId);

        RoomPurposeData[] GetRoomsShortInfos(int projectId);

        Moodboard GetMoodBoards(int roomId);
        int AddMoodboardPhoto(int roomId, string photoUrl);
        void DeleteMoodboardPhoto(int photoId);
        void SetMoodboardArchive(int roomId, string url);

        Collage GetCollages(int roomId);
        int AddCollagePhoto(int roomId, string photoUrl);
        void DeleteCollagePhoto(int photoId);
        void SetCollageArchive(int roomId, string url);

        Zone GetZones(int roomId);
        int AddZonePhoto(int roomId, string photoUrl);
        void DeleteZonePhoto(int photoId);
        void SetZoneArchive(int roomId, string url);

        Plan[] GetAllPlans(int projectId);

        Plan GetPlans(int roomId);
        int AddPlanFile(int roomId, string fileUrl);
        void DeletePlanFile(int fileId);
        void SetPlanArchive(int roomId, string url);

        void SetPlanAsComplete(int roomId);
        void SetPlanAsSelected(int roomId, int paymentId);

        Visualization[] GetAllVisualizations(int projectId);

        Visualization GetVisualizations(int roomId);
        void SetVisualizationCode(int roomId, string visualizationCode);

        void SetVisualizationAsComplete(int roomId);
        void SetVisualizationAsSelected(int roomId, int paymentId);

        RoomGoods GetMarketplaceGoods(int roomId);
        void AddMarketplaceGoods(RoomGoods goodsToAdd);
        void RemoveMarketplaceGoods(RoomGoods goodsToRemove);

        DevelopmentStageStatus GetStatus(int projectId, DevelopmentStage stage);
        void SetClientIsReady(int projectId, DevelopmentStage stage);
        void SetDesignerIsReady(int projectId, DevelopmentStage stage);
        void SetClientMadeDecision(int projectId, DevelopmentStage stage);
        DevelopmentStage GoToNextStage(int projectId);
        DevelopmentStage GetCurrentStageStatus(int projectId);

        void AddProductsToOrder(int roomId, int itemId, int count, int orderId);
        void PayForOneOutsideItem(int roomId, string name, int count);
        void UpdateVisualizationStageTime(int projectId);
        void UpdatePlansStageTime(int projectId);
        
        bool AllRoomsHasMoodboards(int projectId);
        bool AllRoomsHasCollages(int projectId);
        bool AllRoomsHasZones(int projectId);
        bool AllSelectedRoomsHasVisuals(int projectId);
        bool AllSelectedRoomsHasPlans(int projectId);
        bool IsCurrentStage(int projectId, DevelopmentStage stage);
    }
}
