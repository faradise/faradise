﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBTolokaService
    {
        void AddTask(TolokaTaskModel model);
        void AddTaskSegment(TolokaTaskSegmentModel model);
        TolokaTaskModel[] GetTasks();
        TolokaTaskModel GetTaskByStatus(TolokaTaskStatus status);
        TolokaTaskModel GetTaskById(int taskId);
        TolokaTaskSegmentModel GetSegmentByStatus(TolokaTaskStatus status);
        void UpdateTaskStatus(int taskId, TolokaTaskStatus status);
        void UpdateSegmentStatus(int segmentId, TolokaTaskStatus status);
        void UpdateTaskReportUrl(int taskId, string reportUrl);

        TolokaTaskSegmentModel[] GetSegments(int taskId);
        void DeleteTask(int taskId);
        void RestartSegment(int segmentId);

        void ClearStatus();
    }
}
