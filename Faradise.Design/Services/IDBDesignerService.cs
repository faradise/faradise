﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IDBDesignerService
    {
        bool CreateDesigner(int id);
        Designer GetDesigner(int id);
        Designer[] GetDesigners(int offset, int count); 
        bool SetDesigner(int id, Designer model);

        bool SetBaseModel(int id, DesignerBase baseModel);
        bool SetRegistrationStage(int id, DesignerRegistrationStage stage);
        bool SetPortfolioLink(int id, string portfoliolink);
        bool SetTestLink(int id, string testlink);
        bool SetStyles(int id, DesignerStyle[] styles);
        bool SetTestApproved(int id, bool approved);
        bool SetPhotoLink(int id, string link);
        bool SetTariff(int id, DesignerLevel tariff);
        bool SetPossibilities(int id, DesignerPossibility possibilities);
        bool SetTest(int id, TestResults test);
        bool SetLegalModel(int id, DesignerLegal legal);
        void SetPhone(int id, string phone);
        void AddBaseInfoToLegalInfo(int id, string name, string surname, string phone);

        DesignerBase GetBaseModel(int id);
        void DeleteDesigner(int designerId);
        DesignerRegistrationStage GetRegistrationStage(int id);
        string GetPortfolioLink(int id);
        TestJobInfo GetTestJobInfo(int id);
        DesignerStyle[] GetStyles(int id);
        string GetPhotoLink(int id);
        DesignerLevel GetTariff(int id);
        DesignerPossibility GetPossibilities(int id);
        TestResults GetTest(int id);
        DesignerLegal GetLegalModel(int id);
        string GetPhone(int id);
        int[] GetDesignerProjects(int designerId);

        TestQuestionDescription[] GetDesignerQuestions();

        int CreatePortfolioProject(int id);
        void RemovePortfolioProject(int id, int portfolioId);
        List<DesignerShortInfo> GetDesignersShortInfos();
        Portfolio GetPortfolio(int id);
        bool SetPortfolio(int id, int portfolioId, string name, string description);
        void SetPortfolios(int id, PortfolioDescriptions model);
        int AddPortfolioPhoto(int id, int portfolioId, string path);
        void RemovePortfolioPhoto(int id, RemovePortfolioPhoto model);

        void AddAvatar(int id, string path);
        void RemoveAvatar(int id);
    }
}
