﻿using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Controllers.API.Models.Designer;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Controllers.API.Models.ProjectPreparations;
using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProductCollection;
using Faradise.Design.Models.ProjectDevelopment;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IAdminService
    {
        void AppointDesigners(int projectId, int[] designersIds);
        void RemoveDesignerFromPool(int projectId, int designerId);
        List<DesignerInfo> GetFullListDesigners();
        DesignerModel GetDesignerFullInfo(int designerId);
        int[] GetDesignerProjects(int designerId);
        AdminProjectModel GetFullProjectInformation(int projectId);
        List<AdminProjectProductModel> GetProjectProductsInformation(List<RoomInfoModel> rooms);
        void ApplyDesigner(int designerId, DesignerLevel level);
        void RejectDesigner(int designerId);
        void MarkProjectAsViewed(int projectId);
        void SetProjectAdminAttention(int projectId, bool needAttention);
        void FinishProject(int projectId);
        DesignerInfo[] GetSuitableDesigners(int projectId);
        DesignerRejectedInfo[] GetRejectedDesigners(int projectId);
        DesignerInfo[] GetAppointedDesigners(int projectId);
        void SetDesignerPoolAsFinished(int projectId);

        AdminProjectInfo[] GetProjectsInfo();
        void CompleteVisualizationForProject(int projectId, string visualizationLink);
        Task<string> AuthorizePaymentForProjectOrder(int orderId);
        ShortOrderInformation[] GetOrdersForProject(int projectId);
        FullProjectProductsOrder GetOrderInformation(int orderId);
        void SetAdditionalPriceForProjectOrder(int orderId, int price);
        Visualization[] GetVisualizationsForProject(int projectId);
        void SetProjectOrderStatus(int orderId, OrderStatus status);
        void FinishProjectOrder(int orderId);
        void CloseProjectOrder(int orderId);
        void SetDeliveryPriceForProjectOrder(int orderId, int price);
        void RemoveGoodsFromOrder(int orderId, RoomGoods goods);

        ShortOrderInformation[] GetBasketOrders(int from, int count);
        FullBasketOrderInformation GetBasketOrder(int orderId);
        Task<string> AuthorizePaymentForBasketOrder(int orderId);
        void RemoveProductFromBasketOrder(int orderId, int productId, int count);
        void AddProductToBasketOrder(int orderId, int productId, int count);
        void RefreshPriceForBasketOrder(int orderId);
        void SetAdditionalPriceForBasketOrder(int orderId, int price);
        void SetDeliveryPriceForBasketOrder(int orderId, int price);
        void DeleteProject(int projectId);
        void DeleteUser(int userId);
        void DeleteRoom(int roomId);

        void SetBasketOrderStatus(int orderId, OrderStatus status);
        void FinishBasketOrder(int orderId);
        void CloseBasketOrder(int orderId);

        ModerationCountsModel GetModerationProduct();
        GetModerationProductModel GetProductForModeration(string type);
        void SetModerationProduct(ModerationProductModel model);

        ProductsStats GetProductsStats();

        void ChangeBasketOrder(ChangeBasketOrder changes);
        List<GetCategoryProductsModel> GetCategoryProducts(int companyId, int companyCategoryId);
        bool ChangeProductCategory(int productId, int categoryId);

        ARProductFullInfo GetArProductFullInfo(int arProductId);
        void MapARProductToProduct(int? productId, int arProductId);
        ARProductShortInfo[] GetArProductList();
        PossibleDailyProduct[] GetPossibleDailyProducts(int offset, int count, float companyKoef, float categoryKoef, float saleKoef);
        DailyProduct GetDailyProduct();
        void SetDailyProduct(int productId);
        DailyProduct[] GetPreviousDailyProducts();
        void ChangePromoCodeForOrder(string promocode, int orderId);
        void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod);

        List<string> GetBlacklist();
        void AddWordToBackList(string word);
        void RemoveWordFromBlackList(string word);
        Task<string> GetCollectionsInCSVForCompany(int companyId);
        List<ProductCollection> GetProductCollectionsForCompany(int companyId);
        List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId);
        ProductCollection GetCollection(int collectionId);
        void ChangeCollection(int collectionId, string name, bool isAvalable);
        Task ManuallyCreateCollectionsForCompany(int companyId);
    }
}
