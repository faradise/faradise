﻿using Faradise.Design.Models.ProjectDescription;

namespace Faradise.Design.Services
{
    public interface IDBClientProjectService
    {
        ProjectDescription GetProjectDescription(int projectId);
        void SaveProjectDescription(ProjectDescription project);
        int CreateProjectDescription(int definitionId);
        TestQuestionDescription[] GetDesignerQuestions();
    }
}
