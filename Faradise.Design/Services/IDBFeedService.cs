﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faradise.Design.Utilities.Feed;

namespace Faradise.Design.Services
{
    public interface IDBFeedService
    {
        Task<int> ExtractProductsCount();
        Task<IEnumerable<FeedProduct>> ExtractFeedProducts(int skip, int take);
        Task<DateTime> ExtractFeedsUpdateTime();

        Task StoreFeedLink(string url, FeedFormatType format, FeedAvailabilityType availability, FeedCostType costType);
        Task<string> ExtractFeedLink(FeedFormatType format, FeedAvailabilityType availability, FeedCostType costType);

        Task<int[]> ExtractSkippedCategories(int[] skip);
        FeedProduct[] ExtractFeedProductsByIds(IEnumerable<int> enumerable);
    }
}
