﻿using Faradise.Design.Models.ProductCollection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IProductCollectionService
    {
        List<string> GetBlacklist();
        void AddWordToBackList(string word);
        void RemoveWordFromBlackList(string word);
        Task<string> GetCollectionsInCSVForCompany(int companyId);
        Task<Dictionary<string, List<ProductName>>> FindProductCollectionsForCompany(int companyId, int take);
        Task UpdateProductCollectionForCompany(int companyId, int take);
        List<ProductCollection> GetCollectionsForCompany(int companyId);
        List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId);
        ProductCollection GetCollection(int collectionId);
        void ChangeCollection(int collectionId, string name, bool isAvalable);
    }
}
