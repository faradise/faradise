﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.PromoCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IPromoCodeService
    {
        PromocodeError CheckPromocode(string code, int orderPrice);
        PromoCodeApplyResult ApplyPromocodeToProducts(string code, ProductCount[] products);
        PromoCodeApplyResult ApplyPromocodeToBasket(string code, int userId);
        PromoCode GetPromoCode(string code);
        PromoCodeExport[] GetPromoCodes();
        PromoCodeExport GetPromoCodeExport(string exportname);
        PromoCode[] GetPromoCodesForExport(string exportname);
        void DisablePromocode(string code);
        void CreatePromoCodeExport(string name, int count, int minPrice, int percentsDiscount, int absoluteDiscount, PromoCodeType codeType, DateTime startDate, DateTime expireDate);
    }
}
