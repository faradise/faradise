﻿using Faradise.Design.Controllers.API;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface ITolokaService
    {
        Task<bool> UploadTaskToCDN(UploadTSVModel model);
        Task<bool> UploadTaskSegmentToCDN(string file, TolokaOutputType type, int TaskId);
        Task<bool> UploadTaskReportToCDN(string report, int TaskId);

        Task<string> LoadTaskFromCDN(string url);
        Task<string> LoadTaskSegmentFromCDN(string url);
        Task<string> LoadTaskReportFromCDN(int TaskId);
        Task DeleteTaskReportFromCDN(int TaskId);

        TolokaTaskModel[] GetTasks();
        TolokaTaskSegmentModel[] GetSegments(int taskId);
        void DeleteTask(int taskId);
        void RestartSegment(int segmentId);

        TolokaTaskModel GetUnprocessedTask();
        TolokaTaskSegmentModel GetUnprocessedSegment();
        void UpdateTaskStatus(int taskId, TolokaTaskStatus status);
        void UpdateSegmentStatus(int segmentId, TolokaTaskStatus status);

        Task ClearStatus();
    }
}
