﻿using Faradise.Design.Models.Cache;

namespace Faradise.Design.Services
{
    public interface ICacheService
    {
        bool Put<T>(CacheRequest request, T result, bool extend = false);
        CacheValue<T> Extract<T>(CacheRequest request);
        void Drop();
    }
}
