﻿namespace Faradise.Design.Services
{
    public interface IPersistentStorageService
    {
        bool? AzureSearchAvailable { get; set; }

        bool Put<T>(string key, T value);
        bool Extract<T>(string key, out T value);
    }
}
