﻿using Faradise.Design.Models;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IDBUserService
    {
        User GetUser(int id);
        User CreateUser(Role role);

        void SetUserRole(int id, Role role);
        void SetUserEmail(int id, string email);
        void SetUserName(int id, string name);
        void SetUserPhone(int id, string phone);

        void CreateAlias(Alias alias);
        Alias GetAlias(AliasType type, string value);
        List<Alias> GetAliases(int userId);

        void StoreVerificationCode(VerificationCode code);
        VerificationCode GetVerificationCode(string phone);
    }
}
