﻿using Faradise.Design.Controllers.API.Models.Banner;

namespace Faradise.Design.Services
{
    public interface IDBBannerQueryService
    {
        Banner GetBanner(int id);
        Banner[] GetAllActiveBanners();
    }
}