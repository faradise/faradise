﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models;
using Faradise.Design.Models.ProjectDescription;
using System.Collections.Generic;

namespace Faradise.Design.Services
{
    public interface IClientProjectService
    {
        ProjectDescription GetProjectDescription(int projectId);

        int CreateProjectDescription(int definitionId);
        void SetProjectInformation(int projectId, ProjectTargetType objectType, Dictionary<Room, int> rooms, ReasonBinding[] reasons);
        void SetProjectStyles(int projectId, StyleBinding[] styles);
        void SetProjectBudget(int projectId, Budget budget);
        void SetDisignerRequirements(int projectId, DesignerRequirements designerRequirements);
        TestQuestionDescription[] GetDesignerQuestions();
        bool ValidationTest(TestResultsModel testing);
    }
}
