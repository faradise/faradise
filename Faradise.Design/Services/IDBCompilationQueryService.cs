﻿using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.Services
{
    public interface IDBCompilationQueryService
    {
        ProductsQuery GetProductForCompilation(string compilationName, int offset, int count);
    }
}
