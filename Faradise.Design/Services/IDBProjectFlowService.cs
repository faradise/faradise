﻿using Faradise.Design.Models;
using Faradise.Design.Models.ProjectPreparations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBProjectFlowService
    {
        void SetProjectFlowRoomProportions(Dictionary<int, RoomProportions> proportions);
        void SetProjectFlowStyles(int projectId, Dictionary<ProjectStyle, string> styles);
        CooperateStage GoToNextStage(int projectId);
        void SetProjectFlowBudget(int projectId, int furnitureBudget, int renovationBudget);
        void SetAdditionalInformation(List<AdditionalInformationForRoom> information);

        int AddRoomPhoto(int roomId, string url);
        void SetPlanPhoto(int roomId, string url);
        int AddOldFurnitureToSavePhoto(int projectId, string url);
        int AddOldFurnishToSavePhoto(int projectId, string url);

        void DeleteRoomPhoto(int photoId);
        void DeleteOldFurniturePhoto(int photoId);
        void DeleteOldFurnishPhoto(int photoId);
        void DeletePlan(int roomId);
        void DeleteRoom(int roomId);

        List<RoomInfo> GetRooms(int projectId);
        Dictionary<ProjectStyle, string> GetProjectFlowStyles(int projectId);
        ClientDesignerBudget GetBudget(int projectId);
        List<AdditionalInformationForRoom> GetRoomAdditionalInfos(int projectId);
        OldFurniture GetOldFurniture(int projectId);

        StageStatus GetCurrentStage(int projectId);

        void SetClientReady(int projectId);
        int CreateProjectFlow(int projectId);
        bool HasProjectFlow(int projectId);
    }
}
