﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface ICompressionService
    {
        Task<string> CompressToZipAndUploadAsync(List<string> fileUrls, string uploadPath);
    }
}
