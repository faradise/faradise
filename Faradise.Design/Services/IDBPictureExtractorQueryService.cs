﻿using Faradise.Design.Models.PictureUploads;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBPictureExtractorQueryService
    {
        Task<PictureUploadTask[]> GetNextPicturesToUpload(int count);
    }
}
