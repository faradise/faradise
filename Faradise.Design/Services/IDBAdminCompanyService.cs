﻿using Faradise.Design.Models.Admin.Company;

namespace Faradise.Design.Services
{
    public interface IDBAdminCompanyService
    {
        AdminCompanyCategory[] ExtractCompanyCategories(int companyId);
        AdminCompanyCategoryItem ExtractCompanyCategory(int companyId, int categoryId);
    }
}
