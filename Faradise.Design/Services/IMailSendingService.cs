﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IMailSendingService
    {
        void SendEmail<T>(EmailModel email) where T : TemplateModel;

        string AdminNotificationEmail { get; }
        string BasketOrderNotificationEmail { get;}
        string CustomerDevelopmentEmail { get; }

        string DesignerPoolFinishedTemplateId { get; }
        string Stage3ClientGoesToNewStageTemplateId { get; }
        string DesignerDeclinedByAdminTemplateId { get; }
        string DesignerDeclinedByCleintTemplateId { get; }
        string DesignerPoolIsLowTemplateId { get; }
        string ClientReadyForDesignerPoolTemplateId { get; }
        string VisualizationTechSpecPublishedTemplateId { get; }
        string BrigadeTechSpecPublishedTemplateId { get; }
        string NewProductOrderPublishedTemplateId { get; }
        string ProjectNeedAdminTemplateId { get; }
        string DesignerAssignedTemplateId { get; }

        string ClientBasketOrderCreatedId {get;}
        string ClientBasketOrderPaymentLinkId {get;}
        string ClientBasketOrderInWorkId {get;}
        string ClientBasketOrderCompletedId {get;}
        string AdminNewBasketOrderId { get; }

        string ClientReturnUrl { get; }
        string DesignerReturnUrl { get; }
        string AdminReturnUrl { get; }

        string DesignerApprovedJunTemplateId { get; }
        string DesignerApprovedMidTemplateId { get; }
        string DesignerApprovedProTemplateId { get; }

        string CustomerDevelopmentTemplateId { get; }

        string LotteryTemplate { get; }
    }
}
