﻿using System;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Toloka;

namespace Faradise.Design.Services
{
    public interface ICDNService
    {
        void Initialize();
        string GetPathForMoodboardArchive(int projectId);
        string GetPathForWorkPlanArchive(int projectId);
        string GetPathForZoneArchive(int projectId);
        string GetPathForCollageArchive(int projectId);
        string GetPathForVisualisationArchive(int projectId);

        string GetPathForRoomPlan(int projectId);
        string GetPathForRoomPhoto(int projectId);
        string GetPathForOldFurniturePhoto(int projectId);
        string GetPathForOldFurnishPhoto(int projectId);
        string GetPathForMoodboardPhoto(int projectId);
        string GetPathForFreelanceVisualizationFile(int projectId);
        string GetPathForCollagePhoto(int projectId);
        string GetPathForZonePhoto(int projectId);
        string GetPathForVisualisationFiles(int projectId);
        string GetPathForWorkPlanFiles(int projectId);
        string GetPathForBrigadeFiles(int projectId);

        string GetPathForDesignerAvatar(int id);
        string GetPathForDesignerPortfolioPhoto(int id, int portfolioId);
        string GetPathForCategotyPhotos();

        string GetPathForYMLUpload();
        string GetPathForFeedYml();
        string GetPathForFeedGoogle();

        string GetPathForTolokaReport();
        string GetPathForTolokaFile();
        string GetPathForTolokaSegmentFile();
        string GetPathForFeedAnalyzerExcelFiles();


        string GetPathForMessengerPhoto(string groupName);

        Task<byte[]> DownloadBytes(string path);
        Task<string> DownloadText(string path);
        Task<string> DownloadTextByUrl(string url);
        Task<string> UploadData(byte[] data, string path, string extension);
        Task<string> UploadData(byte[] data, string path, string name, string extension);
        Task<string> UploadData(IFormFile file, string path);
        Task<string> UploadData(IFormFile file, string path, string name);
        Task<string> UploadPhoto(IFormFile file, string path, int maxSideLenght);

        Task<List<string>> GetContainerFileNames(string path);
        Task DeleteFileAtPathAsync(string fullPath);
        string CreateUrl(string path);
        Task PurgeCacheAsync(string fullPath);
    }
}