﻿using System.Collections.Generic;
using Faradise.Design.Controllers.API.Models.CorruptedPictures;

namespace Faradise.Design.Services
{
    public interface IDBCorruptedPicturesService
    {
        List<CorruptedPartner> GetAllInvalidPartners();
    }
}