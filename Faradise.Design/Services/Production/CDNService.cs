﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Services.Configs;
using Faradise.Design.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Faradise.Design.Services
{
    public class CDNService : ICDNService
    {
        private const string DELETE_FOLDER = "_deleted/";
        private readonly CDNOptions _options = null;

        private string _token = string.Empty;
        private string _storage = string.Empty;
        private DateTime _expireTokenTime = DateTime.MaxValue;

        public CDNService(IOptions<CDNOptions> options)
        {
            _options = options.Value;
        }

        public void Initialize()
        {
            LoginAsync();
        }

        private bool NeedLogin()
        {
            return string.IsNullOrEmpty(_token) || _expireTokenTime >= DateTime.UtcNow;
        }

        public async Task LoginAsync()
        {
            using (var http = new HttpClient())
            {
                var content = new ByteArrayContent(new byte[0]);
                content.Headers.Add("X-Auth-User", _options.User);
                content.Headers.Add("X-Auth-Key", _options.Password);

                using (var response = await http.PostAsync(_options.Host, content))
                {
                    var tokenValues = response.Headers.GetValues("X-Auth-Token");
                    var storageValues = response.Headers.GetValues("X-Storage-Url");
                    var expireInSeconds = response.Headers.GetValues("X-Expire-Auth-Token").First();
                    double seconds = 0;
                    if (double.TryParse(expireInSeconds, out seconds))
                        _expireTokenTime = DateTime.UtcNow.AddSeconds(seconds / 2);
                    else
                        _expireTokenTime = DateTime.MaxValue;
                    _token = tokenValues.First();
                    _storage = storageValues.First();
                }
            }
        }

        public async Task<byte[]> DownloadBytes(string path)
        {
            await LoginAsync();
            var url = CreateUrl(path);
            using (var http = new HttpClient())
            using (var response = await http.GetAsync(url))
            {
                var bytes = await response.Content.ReadAsByteArrayAsync();
                return bytes;
            }
        }

        public async Task<string> DownloadText(string path)
        {
            var url = CreateUrl(path);
            using (var http = new HttpClient())
            using (var response = await http.GetAsync(url))
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }
        }

        public async Task<string> DownloadTextByUrl(string url)
        {
            await LoginAsync();
            using (var http = new HttpClient())
            using (var response = await http.GetAsync(url))
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }
        }

        public string CreateUrl(string path)
        {
            var prefixPath = _storage + _options.Container;
            if (path.StartsWith(prefixPath))
                return path;
            return prefixPath + path;
        }

        public string GetPathForRoomPlan(int projectId)
        {
            return $"/projects/{projectId}/photos/plans/";
        }

        public string GetPathForRoomPhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/rooms/";
        }

        public string GetPathForOldFurniturePhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/old_furniture/";
        }

        public string GetPathForOldFurnishPhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/old_furnish/";
        }

        public string GetPathForDesignerAvatar(int id)
        {
            return $"/designers/{id}/avatar/";
        }

        public string GetPathForDesignerPortfolioPhoto(int id, int portfolioId)
        {
            return $"/designers/{id}/portfolios/{portfolioId}/";
        }

        public string GetPathForMoodboardPhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/moodboard/";
        }

        public string GetPathForCollagePhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/collage/";
        }

        public string GetPathForZonePhoto(int projectId)
        {
            return $"/projects/{projectId}/photos/zonning/";
        }

        public string GetPathForVisualisationFiles(int projectId)
        {
            return $"/projects/{projectId}/visualization/";
        }

        public string GetPathForWorkPlanFiles(int projectId)
        {
            return $"/projects/{projectId}/workplans/";
        }

        public string GetPathForFreelanceVisualizationFile(int projectId)
        {
            return $"/freelance/{projectId}/visualization/";
        }

        public string GetPathForBrigadeFiles(int projectId)
        {
            return $"/freelance/{projectId}/brigade/";
        }

        public string GetPathForFeedYml()
        {
            return string.Format("/feeds/{0}/yml/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForFeedGoogle()
        {
            return string.Format("/feeds/{0}/google/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForTolokaReport()
        {
            return string.Format("/toloka/reports/{0}/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForFeedAnalyzerExcelFiles()
        {
            return string.Format("/excel/reports/{0}/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForTolokaFile()
        {
            return string.Format("/toloka/{0}/tasks/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForTolokaSegmentFile()
        {
            return string.Format("/toloka/{0}/segments/", Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
        }

        public string GetPathForMessengerPhoto(string groupName)
        {
            return $"/messenger/{groupName}/";
        }

        public async Task<string> UploadData(byte[] data, string path, string extension)
        {
            return await UploadData(data, path, Guid.NewGuid().ToString(), extension);
        }

        public async Task<string> UploadData(byte[] data, string path, string name, string extension)
        {
            await LoginAsync();
            var url = CreateUrl(path);
            using (var http = new HttpClient())
            {
                var filename = name + extension;
                url += filename;
                var content = new ByteArrayContent(data);
                content.Headers.Add("X-Auth-Token", _token);
                content.Headers.Add("Content-type", "application/octet-stream");
                content.Headers.Add("X-Filename", filename);
                content.Headers.Add("X-Object-Meta-Sendmefile-Allow-Overwrite", "yes");
                using (var response = await http.PutAsync(url, content))
                {
                    if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(url))
                        return url;
                    else
                    {
                        throw new ServiceException(
                            $"cdn file upload failed for file {filename}. StatusCode: {(int) response.StatusCode}. ReasonPhrase: {response.ReasonPhrase}. Put url: {url}",
                            "file_cdn_upload_failed");
                        return string.Empty;
                    }
                }
            }
        }

        public async Task<string> UploadData(IFormFile file, string path)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                if (memoryStream == null || memoryStream.Length == 0)
                    throw new ServiceException("Can not open file or file is empty", "file_open_error");
                var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                return await UploadData(memoryStream.ToArray(), path, extension);
            }
        }

        public async Task<string> UploadData(IFormFile file, string path, string name)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                if (memoryStream == null || memoryStream.Length == 0)
                    throw new ServiceException("Can not open file or file is empty", "file_open_error");
                var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                return await UploadData(memoryStream.ToArray(), path, name, extension);
            }
        }

        public async Task<string> UploadPhoto(IFormFile file, string path, int maxSideLenght)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                if (memoryStream == null || memoryStream.Length == 0)
                    throw new ServiceException("Can not open file or file is empty", "file_open_error");

                var newStream = memoryStream.Resize(maxSideLenght);

                var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                return await UploadData(newStream.ToArray(), path, extension);
            }
        }

        public async Task DeleteFileAtPathAsync(string fullPath)
        {
            //var lastEsc = fullPath.LastIndexOf("/");
            //if (lastEsc == -1)
            //    throw new ServiceException($"Bad file path {fullPath}", "file_delete_failed");
            //var fileName = fullPath.Substring(lastEsc + 1);
            //var deletedFolder = fullPath.Remove(lastEsc) + DELETE_FOLDER;

            //var bytes = await DownloadBytes(fullPath);
            //await UploadData(bytes, deletedFolder, fullPath.Substring(fullPath.LastIndexOf('.')));
            await LoginAsync();
            using (var http = new HttpClient())
            {
                var msg = new HttpRequestMessage(HttpMethod.Delete, fullPath);
                msg.Headers.Add("X-Auth-Token", _token);
                using (var response = await http.SendAsync(msg))
                {
                    if (!response.IsSuccessStatusCode)
                        throw new ServiceException($"Unable to delete file {fullPath} from cdn", "file_delete_failed");
                }
            }
        }

        public async Task<List<string>> GetContainerFileNames(string path)
        {
            await LoginAsync();
            var url = CreateUrl(path);
            using (var http = new HttpClient())
            using (var response = await http.GetAsync(url))
            {
                var str = await response.Content.ReadAsStringAsync();
                return new List<string>();
            }
        }

        public string GetPathForMoodboardArchive(int projectId)
        {
            return $"/projects/{projectId}/photos/moodboard/archive/allmoodbaords_{GetTimeStamp()}.zip";
        }

        public string GetPathForWorkPlanArchive(int projectId)
        {
            return $"/projects/{projectId}/workplans/archive/allworkplans_{GetTimeStamp()}.zip";
        }

        public string GetPathForZoneArchive(int projectId)
        {
            return $"/projects/{projectId}/photos/zonning/archive/allzones_{GetTimeStamp()}.zip";
        }

        public string GetPathForCollageArchive(int projectId)
        {
            return $"/projects/{projectId}/photos/collage/archive/allcollages_{GetTimeStamp()}.zip";
        }

        public string GetPathForVisualisationArchive(int projectId)
        {
            return $"/projects/{projectId}/visualization/archive/allvisualizations_{GetTimeStamp()}.zip";
        }


        public async Task PurgeCacheAsync(string fullPath)
        {
            await LoginAsync();
            var url = CreateUrl(fullPath);
            using (var http = new HttpClient())
            {
                var msg = new HttpRequestMessage(HttpMethod.Put, "https://api.selcdn.ru/v1/cdn");
                msg.Content = new StringContent(fullPath);
                msg.Headers.Add("X-Auth-Token", _token);
                var req = msg.RequestUri;
                var reqs = msg.ToString();
                using (var response = await http.SendAsync(msg))
                {
                    if (!response.IsSuccessStatusCode)
                        throw new ServiceException($"Unable to earase cache file {fullPath} from cdn",
                            "file_delete_failed");
                }
            }
        }

        private string GetTimeStamp()
        {
            return DateTime.UtcNow.ToString("dd MMM HH:mm:ss:fff");
        }

        public string GetPathForCategotyPhotos()
        {
            return "/categories/";
        }

        public string GetPathForYMLUpload()
        {
            return "/service/uml_uploads/";
        }
    }
}