﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net;

namespace Faradise.Design.Services
{
    public class ValidationService : IValidationService
    {
        private readonly IDBUserService _dbService = null;
        private readonly ISMSService _smsService = null;

        // TODO:: Отрефакторить. Вынести способ отправки токена в отдельный сервис.

        public ValidationService(IDBUserService dbService, ISMSService smsService)
        {
            _dbService = dbService;
            _smsService = smsService;
        }

        public string SendValidationCode(string address)
        {
            var rnd = new Random();

            // TODO:: В данный момент мы не будем отправлять коды на смс
            // а используем предзаписанный

            //if (System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "staging"|| System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "master")
            //    address = _options.TestPhoneNumber;


            //var code = "0000";
            var storedCode = _dbService.GetVerificationCode(address);
            if(storedCode!=null)
            {
                var nowTime = DateTime.Now;
                var difference = (nowTime - storedCode.CreationDate).TotalSeconds;
                if(difference<30)
                    throw new ServiceException($"sms was sent recently to adress {address}", "sms_service_error");
            }
            var code = rnd.Next(1000, 9999).ToString();

            if (address == "79990000000")
            {
                code = "0000";
            }
            else
            {
                var response = _smsService.TrySendSMS(address, $"Ваш код Faradise: {code}");
                //if (response != "200")
                if (!response.Contains("OK"))
                    throw new ServiceException($"sms not send to adress {address}", "sms_service_error");
            }

            _dbService.StoreVerificationCode(new VerificationCode { Code = code, Phone = address, Expires = DateTime.Now.Add(TimeSpan.FromMinutes(15)), CreationDate=DateTime.Now });

            return code;
        }

        public bool VerifyValidationCode(string address, string code)
        {
            if (address == "79990000000" && code == "0000")
                return true;
            var storedCode = _dbService.GetVerificationCode(address);
            if (storedCode == null)
                return false;

            if (storedCode.Expires < DateTime.Now)
                return false;

            if (!storedCode.Code.Equals(code, StringComparison.OrdinalIgnoreCase))
                return false;

            return true;
        }
    }
}
