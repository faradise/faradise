﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Extensions;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Models.ProjectPreparations;
using Faradise.Design.Services.Configs;
using Microsoft.Extensions.Options;

namespace Faradise.Design.Services.Production.DB
{
    public class DBProjectDevelopmentService : IDBProjectDevelopmentService
    {

        private readonly FaradiseDBContext _context;
        private TimeOptions _timeOptions = null;
        private PriceOptions _priceOptions = null;

        public DBProjectDevelopmentService (FaradiseDBContext db, IOptions<TimeOptions> timeOptions, IOptions<PriceOptions> priceOptions)
        {
            _context = db;
            _timeOptions = timeOptions.Value;
            _priceOptions = priceOptions.Value;
        }

        public int AddCollagePhoto(int roomId, string photoUrl)
        {
            var photo = new RoomCollagePhotoEntity { RoomId = roomId, Url = photoUrl };
           _context.RoomCollagePhotos.Add(photo);
            _context.SaveChanges();
            return photo.Id;
        }

        public void SetCollageArchive(int roomId, string url)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException($"room id {roomId} not found", "room_not_found");
            room.CollagePhotoArchiveUrl = url;
            _context.SaveChanges();
        }

        public void AddMarketplaceGoods(RoomGoods goodsToAdd)
        {
            var items = new List<RoomGoodsEntity>();
            foreach(var goods in goodsToAdd.MarketplaceItems)
            {
                for(var i=0;i<goods.Count; i++)
                {
                    var entity = new RoomGoodsEntity
                    {
                        RoomId = goodsToAdd.RoomId,
                        ProductId = goods.ProductId,
                        PaidFor = goods.PaidFor
                    };
                    items.Add(entity);
                }
            };
            _context.RoomGoods.AddRange(items);
            _context.SaveChanges();
         }

        public int AddMoodboardPhoto(int roomId, string photoUrl)
        {
            var photo = new RoomMoodboardPhotoEntity { RoomId = roomId, Url = photoUrl };
            _context.RoomMoodboardPhotos.Add(photo);
            _context.SaveChanges();
            return photo.Id;
        }

        public int AddPlanFile(int roomId, string fileUrl)
        {
            var photo = new RoomPlanPhotoEntity { PlanId = roomId, Url = fileUrl };
            _context.RoomPlanPhotos.Add(photo);
            _context.SaveChanges();
            return photo.Id;
        }

        public int AddZonePhoto(int roomId, string photoUrl)
        {
            var photo = new RoomZonePhotoEntity { RoomId = roomId, Url = photoUrl };
            _context.RoomZonePhotos.Add(photo);
            _context.SaveChanges();
            return photo.Id;
        }

        public void CreateProjectDevelopment(int projectId, List<RoomInfo> rooms)
        {
            var data = new ProjectDevelopmentEntity() { Id = projectId };
            data.CurrentStage = DevelopmentStage.Moodboard;
            _context.ProjectDevelopments.Add(data);
            _context.SaveChanges();
            foreach (var room in rooms)
            {
                _context.RoomVisualizations.Add(new RoomVisualizationEntity()
                {
                    RoomId = room.RoomId,
                    Price = _priceOptions.RoomVisualizationPrice
                });
                _context.RoomPlans.Add(new RoomPlanEntity()
                {
                    RoomId = room.RoomId,
                    Price = _priceOptions.RoomPlanPrice
                });
                _context.SaveChanges();
            }
            var stepArray = Enum.GetValues(typeof(DevelopmentStage)).Cast<DevelopmentStage>().OrderBy(x => x).ToArray();
            foreach (var step in stepArray)
            {
                int time = 0;
                var stageData = new ProjectDevelopmentStageDataEntity()
                {
                    ProjectId = projectId,
                    Stage = step,
                    ClientIsReady = false,
                    StartTime = DateTime.UtcNow,
                    DesignerIsReady = false,
                    TotalLength = time
                };
                _context.ProjectDevelopmentStageData.Add(stageData);
                _context.SaveChanges();
            };
        }

        public void DeleteCollagePhoto(int photoId)
        {
            var photo = _context.RoomCollagePhotos.Find(photoId);
            if (photo == null)
                throw new ServiceException($"photo id {photoId} not found", "photo_not_found");
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public void DeleteMoodboardPhoto(int photoId)
        {
            var photo = _context.RoomMoodboardPhotos.Find(photoId);
            if (photo == null)
                throw new ServiceException($"photo id {photoId} not found", "photo_not_found");
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public void DeletePlanFile(int fileId)
        {
            var photo = _context.RoomPlanPhotos.Find(fileId);
            if (photo == null)
                throw new ServiceException($"photo id {fileId} not found", "photo_not_found");
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public void DeleteZonePhoto(int photoId)
        {
            var photo = _context.RoomZonePhotos.Find(photoId);
            if (photo == null)
                throw new ServiceException($"photo id {photoId} not found", "photo_not_found");
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public Plan[] GetAllPlans(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms.ToList();
            var plans = new List<Plan>();
            foreach(var plan in rooms.Select(x=>x.Plan).ToList())
            {
                plans.Add(new Plan
                {
                    RoomId = plan.RoomId,
                    IsComplete = plan.IsComplete,
                    IsSelected = plan.IsSelected,
                    Price = plan.Price,
                    ArchiveUrl = plan.ArchiveUrl,
                    PaymentStatus = plan.Payment?.Status,
                    Files = plan.Photos.Where(x => x.IsActive).Select(x => new RoomFile { FileId = x.Id, Link = x.Url }).ToArray()
                });
            };
            return plans.ToArray();
        }

        public Visualization[] GetAllVisualizations(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms.ToList();
            var visualizations = new List<Visualization>();
            foreach (var visualization in rooms.Select(x => x.Visualization).ToList())
            {
                visualizations.Add(new Visualization
                {
                    RoomId = visualization.RoomId,
                    IsComplete = visualization.IsComplete,
                    IsSelected = visualization.IsSelected,
                    Price = visualization.Price,
                    VisualizationCode = visualization.VisualizationCode,
                    PaymentStatus = visualization.Payment?.Status
                });
            };
            return visualizations.ToArray();
        }

        public Collage GetCollages(int roomId)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "room_not_found");
            var photos = _context.RoomCollagePhotos.Where(x => x.RoomId == roomId && x.IsActive);
            return new Collage
            {
                RoomId = roomId,
                ArchiveUrl = room.CollagePhotoArchiveUrl,
                Photos = photos != null ? photos.Select(x => new RoomFile { FileId = x.Id, Link = x.Url }).ToArray() : new RoomFile[0]
            };
        }

        public DevelopmentStage GetCurrentStageStatus(int projectId)
        {
            var project = _context.ProjectDevelopments.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            return project.CurrentStage;
        }

        public Moodboard GetMoodBoards(int roomId)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "room_not_found");
            var photos = _context.RoomMoodboardPhotos.Where(x => x.RoomId == roomId && x.IsActive);
            return new Moodboard
            {
                RoomId = roomId,
                ArchiveUrl = room.MoodboardPhotoArchiveUrl,
                Photos = photos != null ? photos.Select(x => new RoomFile { FileId = x.Id, Link = x.Url }).ToArray() : new RoomFile[0]
            };
        }

        public Plan GetPlans(int roomId)
        {
            var plan = _context.RoomPlans.Find(roomId);
            if (plan == null)
                throw new ServiceException("plan_not_found", "plan_not_found");
            var photos = _context.RoomPlanPhotos.Where(x => x.PlanId == roomId && x.IsActive);
            return new Plan
            {
                RoomId = roomId,
                ArchiveUrl = plan.ArchiveUrl,
                IsComplete = plan.IsComplete,
                PaymentStatus = plan.Payment?.Status,
                IsSelected = plan.IsSelected,
                Price = plan.Price,
                Files = photos != null ? photos.Select(x => new RoomFile { FileId = x.Id, Link = x.Url }).ToArray() : new RoomFile[0]
            };
        }

        public RoomPurposeData[] GetRoomsShortInfos(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return new RoomPurposeData[0];
            return rooms.Select(x => new RoomPurposeData { RoomId = x.Id, Purpose = x.Room }).ToArray();
        }

        public DevelopmentStageStatus GetStatus(int projectId, DevelopmentStage stage)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId == projectId && x.Stage == stage);
            if (stageData == null)
                throw new ServiceException("stage_not_found", "stage_not_found");
            var timeLeft = stageData.TotalLength - (int)(DateTime.UtcNow - stageData.StartTime).TotalSeconds;
            if (timeLeft < 0)
                timeLeft = 0;
            return new DevelopmentStageStatus
            {
                Stage = stage,
                ClientIsReady = stageData.ClientIsReady,
                DesignerIsReady = stageData.DesignerIsReady,
                TimeLeft = timeLeft,
                UserMadeDecision=stageData.UserMadeDecision
            };
        }

        public Visualization GetVisualizations(int roomId)
        {
            var visualization = _context.RoomVisualizations.Find(roomId);
            if(visualization==null)
                throw new ServiceException("visualization_not_found", "visualization_not_found");
            return new Visualization
            {
                RoomId = roomId,
                VisualizationCode = visualization.VisualizationCode,
                IsComplete = visualization.IsComplete,
                IsSelected = visualization.IsSelected,
                PaymentStatus = visualization.Payment?.Status,
                Price = visualization.Price,
            };
        }

        public Zone GetZones(int roomId)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "room_not_found");
            var photos = _context.RoomZonePhotos.Where(x=>x.RoomId==roomId && x.IsActive).ToList();
            return new Zone
            {
                RoomId = roomId,
                ArchiveUrl = room.ZonePhotoArchiveUrl,
                Photos=photos.Select(x=>new RoomFile { FileId=x.Id, Link=x.Url}).ToArray()
            };
        }

        public DevelopmentStage GoToNextStage(int projectId)
        {
            var project = _context.ProjectDevelopments.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var current = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.Stage == project.CurrentStage&&x.ProjectId==projectId);
            if (current.Stage >= DevelopmentStage.WaitForFinish)
                return current.Stage;
            var nextStage = current.Stage.Next();
            var nextStageData= _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.Stage == nextStage && x.ProjectId == projectId);
            project.CurrentStage = nextStage;
            nextStageData.StartTime = DateTime.UtcNow;
            nextStageData.ClientIsReady = false;
            nextStageData.DesignerIsReady = false;
            int totalTime = 0;
            var roomsData = _context.ProjectDescriptionRooms.Where(x => x.ProjectId == projectId).Select(x=>new { x.Visualization, x.Plan }).ToList()  ;
            switch (nextStage)
            {
                case DevelopmentStage.Moodboard:
                    totalTime = _timeOptions.RoomMoodboardTime * roomsData.Count();
                    break;
                case DevelopmentStage.Collage:
                    totalTime = _timeOptions.RoomCollageTime * roomsData.Count();
                    break;
                case DevelopmentStage.Zoning:
                    totalTime = _timeOptions.RoomZoningTime * roomsData.Count();
                    break;
                case DevelopmentStage.Visualization:
                    totalTime = _timeOptions.RoomVisulizationTime * roomsData.Where(x=>x.Visualization.IsSelected).Count();
                    break;
                case DevelopmentStage.Plan:
                    totalTime = _timeOptions.RoomPlanTime * roomsData.Where(x => x.Plan.IsSelected).Count();
                    break;
                default:
                    totalTime = 0;
                    break;
            }
            nextStageData.TotalLength = totalTime;
            _context.SaveChanges();
            return nextStageData.Stage;
        }

        public bool HasProjectDevelopment(int projectId)
        {
            return _context.ProjectDevelopments.Find(projectId) != null;
        }

        public void PayForOneOutsideItem(int roomId, string name, int count)
        {
            throw new NotImplementedException();
        }


        public void RemoveMarketplaceGoods(RoomGoods goodsToRemove)
        {
            foreach(var goods in goodsToRemove.MarketplaceItems)
            {
                for(var i=0;i<goods.Count;i++)
                {
                    var entity = _context.RoomGoods.FirstOrDefault(x=>x.RoomId==goodsToRemove.RoomId&&x.ProductId==goods.ProductId && x.PaidFor == goods.PaidFor);
                    _context.RoomGoods.Remove(entity);
                    _context.SaveChanges();
                    
                }
            }
        }

        public void SetClientIsReady(int projectId, DevelopmentStage stage)
        {
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId == projectId && x.Stage == stage);
            if(stageData==null)
                throw new ServiceException($"stageData with project id {projectId} not found", "stageData_not_found");
            stageData.ClientIsReady = true;
            _context.SaveChanges();
        }


        public void SetDesignerIsReady(int projectId, DevelopmentStage stage)
        {
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId == projectId && x.Stage == stage);
            if (stageData == null)
                throw new ServiceException($"stageData with project id {projectId} not found", "stageData_not_found");
            stageData.DesignerIsReady = true;
            _context.SaveChanges();
        }

        public void SetMoodboardArchive(int roomId, string url)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "room_not_found");
            room.MoodboardPhotoArchiveUrl = url;
            _context.SaveChanges();
        }

        public void SetPlanArchive(int roomId, string url)
        {
            var plan = _context.RoomPlans.Find(roomId);
            if (plan == null)
                throw new ServiceException("plan_not_found", "plan_not_found");
            plan.ArchiveUrl = url;
            _context.SaveChanges();
        }

        public void SetPlanAsComplete(int roomId)
        {
            var plan = _context.RoomPlans.Find(roomId);
            if (plan == null)
                throw new ServiceException("plan_not_found", "plan_not_found");
            plan.IsComplete = true;
            _context.SaveChanges();
        }

        public void SetPlanAsSelected(int roomId, int paymentId)
        {
            var plan = _context.RoomPlans.Find(roomId);
            if (plan == null)
                throw new ServiceException("plan_not_found", "plan_not_found");
            plan.IsSelected = true;
            plan.PaymentId = paymentId;
            _context.SaveChanges();
        }

        public void SetVisualizationAsComplete(int roomId)
        {
            var visualization = _context.RoomVisualizations.Find(roomId);
            if (visualization == null)
                throw new ServiceException("visualization_not_found", "visualization_not_found");
            visualization.IsComplete = true;
            _context.SaveChanges();
        }

        public void SetVisualizationAsSelected(int roomId, int paymentId)
        {
            var visualization = _context.RoomVisualizations.Find(roomId);
            if (visualization == null)
                throw new ServiceException("visualization_not_found", "visualization_not_found");
            visualization.IsSelected = true;
            visualization.PaymentId = paymentId;
            _context.SaveChanges();
        }

        public void SetZoneArchive(int roomId, string url)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "room_not_found");
            room.ZonePhotoArchiveUrl = url;
            _context.SaveChanges();
        }

        public RoomGoods GetMarketplaceGoods(int roomId)
        {
            var goods = _context.RoomGoods.Where(x => x.RoomId == roomId).ToList();
            var marketplaceItems = new List<MarketplaceItem>();
            var groupedItems = goods.GroupBy(x => new { k1 = x.ProductId, k2 = x.PaidFor }).Select(x=>new MarketplaceItem { ProductId = x.Key.k1, PaidFor = x.Key.k2, Count = x.Count() });
            return new RoomGoods
            {
                RoomId = roomId,
                MarketplaceItems = groupedItems.ToList(),
                OutsideItems = new List<OutsideItem>()
            };
        }

        public void SetClientMadeDecision(int projectId, DevelopmentStage stage)
        {
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId==projectId && x.Stage == stage);
            if(stageData==null)
                throw new ServiceException($"project with id {projectId} not found", "project_not_found");
            stageData.UserMadeDecision = true;
            _context.SaveChanges();
        }

        public void UpdateVisualizationStageTime(int projectId)
        {
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId == projectId && x.Stage == DevelopmentStage.Visualization);
            if (stageData == null)
                throw new ServiceException($"stageData with project id {projectId} not found", "stageData_not_found");
            stageData.TotalLength = _timeOptions.RoomVisulizationTime *  _context.RoomVisualizations.Count(x => x.Room.ProjectId == projectId && x.IsSelected);
            _context.SaveChanges();
        }

        public void UpdatePlansStageTime(int projectId)
        {
            var stageData = _context.ProjectDevelopmentStageData.FirstOrDefault(x => x.ProjectId == projectId && x.Stage == DevelopmentStage.Plan);
            if (stageData == null)
                throw new ServiceException($"stageData with project id {projectId} not found", "stageData_not_found");
            stageData.TotalLength = _timeOptions.RoomPlanTime * _context.RoomPlans.Count(x => x.Room.ProjectId == projectId && x.IsSelected);
            _context.SaveChanges();
        }

        public void SetVisualizationCode(int roomId, string visualizationCode)
        {
            var visualization = _context.RoomVisualizations.Find(roomId);
            if (visualization == null)
                throw new ServiceException($"visualization with id {roomId} not found", "visualization_not_found");
            visualization.VisualizationCode = visualizationCode;
            _context.SaveChanges();
        }

        public int CreateNewOrder(int projectId, OrderDescription description)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException($"project id {projectId} is not found", "project_not_found");
            var order = new ProjectMarketplaceOrderEntity()
            {
                CreationTime = DateTime.UtcNow,
                ProjectId = projectId,
                UserId = project.ClientId,
                Description = description.Description,
                Email = description.Email,
                Adress = description.Adress,
                City = description.City,
                Name = description.Name,
                Surname = description.Surname,
                Phone = description.Phone
            };
            _context.ProjectMarketplaceOrders.Add(order);
            _context.SaveChanges();
            return order.Id;
        }

        public int UpdateOrderBasePrice(int orderId)
        {
            var orderedItems = _context.RoomGoods.Where(x => x.OrderId == orderId).Select(x => x.ProductId);
            int price = 0;
            foreach (var orderItem in orderedItems)
            {
                var product = _context.Products.Find(orderItem);
                if (product != null)
                    price += product.Price;
            }
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found","order_not_found");
            order.BasePrice = price;

            _context.SaveChanges();
            return order.BasePrice;
        }

        public void SetOrderPayment(int orderId, int paymentId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.PaymentId = paymentId;
            _context.SaveChanges();
        }

        public void AddProductsToOrder(int roomId, int itemId, int count, int orderId)
        {
            for (var i = 0; i < count; i++)
            {
                var item = _context.RoomGoods.FirstOrDefault(x => x.RoomId == roomId && x.ProductId == itemId && !x.PaidFor);
                if (item == null)
                    throw new ServiceException($"there is no unpaid item with id {itemId} not found", "marketplace_item_not_found");
                item.PaidFor = true;
                item.OrderId = orderId;
                _context.SaveChanges();
            };
        }

        public ShortOrderInformation[] GetAllOrdersIdsForProject(int projectId)
        {
            return _context.ProjectMarketplaceOrders.Where(x => x.ProjectId == projectId).Select
                (x => new ShortOrderInformation() {
                    CreationTime = x.CreationTime,
                    OrderId = x.Id,
                    BasePrice = x.BasePrice,
                     AdditionalPrice = x.AdditionalPrice,
                    PaymentId = x.PaymentId,
                    PaymentStatus = x.Payment != null ? x.Payment.Status : PaymentStatus.Pending
                }).ToArray();
        }

        public FullProjectProductsOrder GetOrderInformation(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            return new FullProjectProductsOrder()
            {
                OrderId = order.Id,
                BasePrice = order.BasePrice,
                AdditionalPrice = order.AdditionalPrice,
                PaymentStatus = order.Payment != null ? order.Payment.Status : PaymentStatus.Pending,
                PaymentId = order.PaymentId,
                ProjectId = order.ProjectId,
                PaymentLink=order.Payment?.PaymentRoute,
                PlatformLink = order.Payment?.PlatformRoute,
                CreateionTime = order.CreationTime,
                OrderDescription = new OrderDescription()
                {
                    Adress = order.Adress,
                    City = order.City,
                    Description = order.Description,
                    Email = order.Email,
                    Name = order.Name,
                    Phone = order.Phone,
                    Surname = order.Surname
                },
                Goods = _context.RoomGoods.Where(x => x.OrderId == orderId).
                            GroupBy(room => room.RoomId).
                            Select(x => new RoomGoods()
                            {
                                RoomId = x.Key,
                                MarketplaceItems = x.GroupBy(productGroup => productGroup.ProductId).Select(item => new MarketplaceItem()
                                {
                                    ProductId = item.Key,
                                    Count = item.Count(),
                                    PaidFor = true
                                }).ToList(),
                                OutsideItems = new List<OutsideItem>()
                            }).ToArray()
            };
        }

        public void RemoveGoodsFromOrder(int orderId, RoomGoods goodsToRemove)
        {
            foreach (var goods in goodsToRemove.MarketplaceItems)
            {
                for (var i = 0; i < goods.Count; i++)
                {
                    var entity = _context.RoomGoods.FirstOrDefault(x => x.RoomId == goodsToRemove.RoomId && x.ProductId == goods.ProductId && x.OrderId == orderId);
                    if (entity != null)
                    {
                        entity.OrderId = null;
                        entity.PaidFor = false;
                    }
                    _context.SaveChanges();
                }
            }
        }

        public void FinishOrder(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.IsFinished = true;
        }

        public void SetOrderAdditionalPrice(int orderId, int price)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.AdditionalPrice = price;
            _context.SaveChanges();
        }

        public bool AllRoomsHasMoodboards(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return true;
            return rooms.All(room => room.MoodboardsPhotos.Any(x => x.IsActive));
        }

        public bool AllRoomsHasCollages(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return true;
            return rooms.All(room => room.CollagesPhotos.Any(x => x.IsActive));
        }

        public bool AllRoomsHasZones(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return true;
            return rooms.All(room => room.ZonePhotos.Any(x => x.IsActive));
        }

        public bool AllSelectedRoomsHasVisuals(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return true;
            return rooms.Select(x => new { x.Visualization.IsSelected, x.Visualization.IsComplete }).All(x => !x.IsSelected || x.IsComplete);
        }

        public bool AllSelectedRoomsHasPlans(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var rooms = project.Rooms;
            if (rooms == null)
                return true;
            return rooms.Select(x => new { x.Plan.IsSelected, x.Plan.IsComplete }).All(x => !x.IsSelected || x.IsComplete);
        }

        public bool IsCurrentStage(int projectId, DevelopmentStage stage)
        {
            var project = _context.ProjectDevelopments.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            return project.CurrentStage == stage;
        }
    }
}
