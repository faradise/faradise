﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.DAL;
using Faradise.Design.DAL.Extensions;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Utilities.Expander;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBMarketplaceQueryService : IDBMarketplaceQueryServiceProvider
    {
        private readonly FaradiseDBQueryContext _context;

        public DBMarketplaceQueryService(FaradiseDBQueryContext context)
        {
            _context = context;
        }

        public ProductInfo GetDailyProduct()
        {
            var product = _context.DailyProducts
                .OrderByDescending(x => x.StartDate)
                .Select(x => x.Product)
                .WhereSaleable()
                .Take(1)
                .SelectProductInfo(ARPlatform.Browser, ARFormat.None)
                .FirstOrDefault();
            return product;
        }

        public ProductsQueryFilters GetQueryFilters(int categoryId)
        {
            var suitableCategories = _context.GetSuitableCategories(categoryId);

            var query = _context.MarketplaceEntity
                .AsExpandable()
                .OrderBy(x => x.Id)
                .Select(x => new
                {
                    Products = x.Categories
                        .Where(y => suitableCategories == null || suitableCategories.Contains(y.Id))
                        .SelectMany(y => y.CompanyCategories)
                        .SelectMany(y => y.Products)
                        .Where(y => ProductEntity.FilterAvailable(y))
                })
                .Select(x => new
                {
                    Count = x.Products.Count(),

                    MinPrice = x.Products.Min(y => (int?)y.Price),
                    MaxPrice = x.Products.Max(y => (int?)y.Price),

                    MinWidth = x.Products.Min(entity => entity.Width),
                    MaxWidth = x.Products.Max(entity => entity.Width),

                    MinHeight = x.Products.Min(entity => entity.Height),
                    MaxHeight = x.Products.Max(entity => entity.Height),

                    MinLength = x.Products.Min(entity => entity.Length),
                    MaxLength = x.Products.Max(entity => entity.Length),

                    AR = x.Products.Any(y => y.ARProduct != null),

                    Brands = x.Products
                        .Select(y => y.CompanyCategory.Company)
                        .Distinct(),

                    Rooms = x.Products
                        .SelectMany(y => y.CompanyCategory.Rooms.Select(z => z.RoomName))
                        .Distinct()
                })
                .Select(x => new ProductsQueryFilters
                {
                    Products = x.Count,
                    MinPrice = x.MinPrice ?? 0,
                    MaxPrice = x.MaxPrice ?? 0,

                    Brands = x.Brands
                        .Select(y => new BrandName { Id = y.Id, Name = y.Name })
                        .ToList(),

                    Rooms = x.Rooms
                        .Select(y => new RoomName { Id = y.Id, Name = y.Name })
                        .ToList(),

                    ContainsARProducts = x.AR,

                    Measurements = new ProductMeasurements
                    {
                        MinHeight = x.MinHeight,
                        MaxHeight = x.MaxHeight,
                        MaxLength = x.MaxLength,
                        MinLength = x.MinLength,
                        MinWidth = x.MinWidth,
                        MaxWidth = x.MaxWidth
                    }
                })
                .First();

            query.Colors = _context.MarketplaceColors
                .Select(x => new ColorName { Id = x.Id, Name = x.Name, Hex = x.Hex })
                .ToList();

            return query;
        }

        public Product GetProductById(int productId)
        {
            var result = _context.Products
                .Where(x => x.Id == productId)
                .SelectProduct()
                .FirstOrDefault();

            return result;
        }

        public ProductsQuery GetProductsByFilters(ProductsQueryFiltersValues filters, ARPlatform platform, ARFormat format)
        {
            var suitable = _context.GetSuitableCategories(filters.Category ?? 0);
            var search = string.IsNullOrEmpty(filters.SearchWords) ? null : $"%{filters.SearchWords}%";

            var query = _context.Products
                .WhereSaleable()
                .ApplyMeasurementFilter(filters.Measurements)
                .ApplySaleFilter(filters.SalesOnly)
                .ApplyPriceFilters(filters.PriceFrom, filters.PriceTo)
                .ApplyARFilter(platform != ARPlatform.Browser || filters.AR, platform, format)
                .ApplyColorFilter(filters.Colors)
                .ApplySearchFilter(search)
                .ApplyCategoryFilter(suitable)
                .ApplyBrandFilter(filters.Brands)
                .OrderBy(filters.Ordering, platform, format);

            return new ProductsQuery
            {
                AllCount = query.Count(),
                Products = query
                    .Skip(filters.Offset)
                    .Take(filters.Count)
                    .SelectProductInfo(platform, format)
                    .ToArray()
            };
        }

        public Category[] GetCategories()
        {
            return _context.MarketPlaceCategories
                .Where(x => !string.IsNullOrEmpty(x.Name))
                .Select(x => MarketplaceCategoryEntity.AsCategory(x))
                .OrderByDescending(x => x.Priority)
                .ThenBy(x => x.Id)
                .ToArray();
        }

        public Category[] GetCategoriesByIds(IEnumerable<int> categoriesIds)
        {
            return categoriesIds.Join(_context.MarketPlaceCategories, x=>x, y=>y.Id,(x,y)=>y)
                .Where(x => !string.IsNullOrEmpty(x.Name))
                .Select(x => MarketplaceCategoryEntity.AsCategory(x))
                .OrderByDescending(x => x.Priority)
                .ThenBy(x => x.Id)
                .ToArray();
        }

        public Category[] GetSubcategories(int id)
        {
            var exists = _context.MarketplaceEntity
                .SelectMany(x => x.Categories)
                .Any(x => x.Id == id);

            if (!exists)
                throw new ServiceException($"category with id {id} was not found", "category_not_found");

            var result = _context.MarketplaceEntity
                .SelectMany(x => x.Categories)
                .Where(x => x.Id == id)
                .SelectMany(x => x.Childs)
                .Select(x => MarketplaceCategoryEntity.AsCategory(x))
                .OrderByDescending(x => x.Priority)
                .ThenBy(x => x.Id)
                .ToArray();

            return result;
        }

        public CategoryInfo[] GetCategoryPath(int categoryId)
        {
            var category = _context.MarketplaceEntity
                .SelectMany(x => x.Categories)
                .Where(x => x.Id == categoryId)
                .Include(x => x.Parent)
                .FirstOrDefault();

            if (category == null)
                throw new ServiceException($"category with id {categoryId} was not found", "category_not_found");

            var result = new List<CategoryInfo> {new CategoryInfo {Id = category.Id, Name = category.Name}};

            while (category.ParentId.HasValue)
            {
                category = category.Parent;
                result.Insert(0, new CategoryInfo {Id = category.Id, Name = category.Name});
            }

            return result.ToArray();
        }

        public CategoryInfo[] GetProductCategoryPath(int productId)
        {
            var category = _context.Products
                .Where(x => x.Id == productId)
                .Where(x => x.CompanyCategory.CategoryId != null)
                .Select(x => x.CompanyCategory)
                .Select(x => x.Category)
                .Include(x => x.Parent)
                .FirstOrDefault();

            var result = new List<CategoryInfo>();

            while (category != null)
            {
                result.Insert(0, new CategoryInfo {Id = category.Id, Name = category.Name});
                category = category.Parent;
            }

            return result.ToArray();
        }

        public CategoryHierarchy[] GetCategoryHierarchy()
        {
            var entities = _context.MarketplaceEntity
                .AsExpandable()
                .SelectMany(x => x.Categories)
                .Select(x => new
                {
                    Category = x,
                    HaveProducts = x.CompanyCategories
                        .SelectMany(y => y.Products)
                        .Where(y => ProductEntity.FilterAvailable(y))
                        //.Where(y => ProductEntity.FilterPictures(y))
                        .Any()
                })
                .OrderByDescending(x => x.Category.Priority)
                .ThenBy(x => x.Category.Id)
                .ToList();

            // delete categories which does not have products and child categories
            
            var clean = false;
            while (!clean)
            {
                clean = true;

                for (var i = 0; i < entities.Count; i++)
                {
                    var c = entities[i];
                    if (!c.HaveProducts)
                    {
                        if (!entities.Any(x => x.Category.ParentId == c.Category.Id))
                        {
                            entities.RemoveAt(i);
                            i--;

                            clean = false;
                        }
                    }
                }
            }

            foreach (var c in entities)
            {
                if (c.Category.ParentId != null)
                {
                    var parent = entities.FirstOrDefault(x => x.Category.Id == c.Category.ParentId.Value);
                    if (parent != null)
                    {
                        if (parent.Category.Childs == null)
                            parent.Category.Childs = new List<MarketplaceCategoryEntity>();
                        parent.Category.Childs.Add(c.Category);
                    }
                }
            }

            var categories = entities
                .Select(x => x.Category)
                .Where(x => x.ParentId == null)
                .Select(ToModel)
                .ToArray();

            return categories;
        }

        public int GetAvailableProductsCount()
        {
            return _context.Products
                .WhereAvailable()
                .WhereHavePictures()
                .Count();
        }

        public ARProductDefinition GetARDefinition(int productId, ARPlatform platform, ARFormat format)
        {
            var arProduct = _context.ARProducts
                .Include(x => x.ARModels)
                .FirstOrDefault(x => x.ProductId == productId);

            var arModel = arProduct?.ARModels.FirstOrDefault(x => x.Platform == platform && x.Format == format);
            if (arModel == null)
                return null;

            var definition = new ARProductDefinition
            {
                ProductId = productId,

                MapDiffuse = arModel.Diffuse,
                MapNormal = arModel.Normal,
                MapMetallic = arModel.Metallic,
                MapRoughness = arModel.Roughness,
                MapMetallicRoughness = arModel.MetallicRoughness,
                Model = arModel.Model,
                Vertical = arProduct.Vertical
            };

            return definition;
        }

        public int GetTotalProductsCount()
        {
            return _context.Products
                .WhereValid()
                .Count();
        }

        #region Mappers

        private static CategoryHierarchy ToModel(MarketplaceCategoryEntity category)
        {
            return new CategoryHierarchy
            {
                Id = category.Id,
                Name = category.Name,
                ImageUrl = category.ImageUrl,
                Childs = category.Childs != null
                    ? category.Childs
                        .Where(x => !string.IsNullOrEmpty(x.Name))
                        .Select(ToModel)
                        .ToArray()
                    : new CategoryHierarchy[0]
            };
        }

        #endregion
    }
}