﻿using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.DAL.Extensions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka.Input;

namespace Faradise.Design.Services.Production.DB
{
    public class DBTolokaSourceService : IDBTolokaSourceService
    {
        private readonly FaradiseDBContext _context = null;

        public DBTolokaSourceService(FaradiseDBQueryContext context)
        {
            _context = context;
        }

        public TolokaInputSize[] ExtractSizeSource(int category, int offset, int count)
        {
            var suitableCategories = _context.GetSuitableCategories(category);

            return _context.Products
                .WhereAvailable()
                .Where(x => category != 0 ? suitableCategories == null || suitableCategories.Contains(x.CompanyCategory.CategoryId ?? 0) : true)
                .Where(x => !x.IsRemoved && (x.Available == null || x.Available.Value))
                .Where(x => x.Width == null && x.Height == null && x.Length == null)
                .Where(x => !string.IsNullOrEmpty(x.Url))
                .Skip(offset)
                .Take(count)
                .Select(x => new TolokaInputSize
                {
                    Id = x.Id,
                    Url = x.Url

                })
                .ToArray();
        }

        public TolokaInputPhoto[] ExtractPhotoSource(int category, int offset, int count, int countPhoto)
        {
            var suitableCategories = _context.GetSuitableCategories(category);
                        
                return _context.Products
                    .WhereAvailable()
                    .Where(x => x.ModerationStatus == null || !x.ModerationStatus.Value.HasFlag(ProductModerationStatus.Photo))
                    .Where(x => suitableCategories == null || !suitableCategories.Contains(x.CompanyCategory.CategoryId ?? 0))
                    .Where(x => !x.IsRemoved && (x.Available == null || x.Available.Value))
                    .Where(x => x.Pictures.Count >= countPhoto)
                    .Skip(offset)
                    .Take(count)
                    .Select(x => new TolokaInputPhoto
                    {
                        Id = x.Id,

                        PhotoUrls = x.Pictures.Select(y => y.Url).ToArray()
                    })
                    .ToArray();
        }

        public TolokaInputPhoto[] ExtractPhotoColorSource(int category, int offset, int count, int countPhoto)
        {
            var suitableCategories = _context.GetSuitableCategories(category);

            return _context.Products
                .WhereAvailable()
                .Where(x => suitableCategories == null || !suitableCategories.Contains(x.CompanyCategory.CategoryId ?? 0))
                .Where(x => x.ProductColors.Count == 0 || x.ModerationStatus == null || !x.ModerationStatus.Value.HasFlag(ProductModerationStatus.Color))
                .Where(x => !x.IsRemoved && (x.Available == null || x.Available.Value))
                .Where(x => x.Pictures.Count >= countPhoto)
                .Skip(offset)
                .Take(count)
                .Select(x => new TolokaInputPhoto
                {
                    Id = x.Id,

                    PhotoUrls = x.Pictures.Select(y => y.Url).ToArray()
                })
                .ToArray();
        }

        public TolokaInputProducts[] ExtractProducts(int category, int offset, int count, string vendor)
        {
            var suitableCategories = _context.GetSuitableCategories(category);

            return _context.Products
                .WhereAvailable()
                .Where(x => category != 0 ? suitableCategories == null || suitableCategories.Contains(x.CompanyCategory.CategoryId ?? 0) : true)
                .Where(x => !x.IsRemoved && (x.Available == null || x.Available.Value))
                .Where(x => !string.IsNullOrEmpty(vendor) ? x.Vendor.Contains(vendor) : true )
                .Skip(offset)
                .Take(count)
                .Select(x => new TolokaInputProducts
                {
                    Id = x.Id,
                    Url = x.Url,
                    Name = x.Name
                })
                .ToArray();
        }

        public TolokaInputProducts[] ExtractDescriptionSource(int category, int offset, int count, string vendor, int maxLetter)
        {
            var suitableCategories = _context.GetSuitableCategories(category);

            return _context.Products
                .WhereAvailable()
                .Where(x => category != 0 ? suitableCategories == null || suitableCategories.Contains(x.CompanyCategory.CategoryId ?? 0) : true)
                .Where(x => !x.IsRemoved && (x.Available == null || x.Available.Value))
                .Where(x => !string.IsNullOrEmpty(vendor) ? x.Vendor.Contains(vendor) : true)
                .Where(x => x.Description != null && x.Description.Count() <= maxLetter)
                .Skip(offset)
                .Take(count)
                .Select(x => new TolokaInputProducts
                {
                    Id = x.Id,
                    Url = x.Url,
                    Name = x.Name
                })
                .ToArray();
        }
    }
}
