﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Services.Production.DB
{
    public class DBTolokaAcceptorService : IDBTolokaAcceptorService
    {
        private readonly FaradiseDBContext _context = null;

        public DBTolokaAcceptorService(FaradiseDBContext context)
        {
            _context = context;
        }

        public MarketplaceColorEntity[] GetMarketplaceColors()
        {
            var marketplaceColors = _context.MarketplaceColors.ToArray();
            return marketplaceColors;
        }

        public CompanyColorEntity[] GetCompanyColors(MarketplaceColorEntity[] marketplaceColors)
        {
            List<CompanyColorEntity> companyColors = new List<CompanyColorEntity>();

            foreach(var mColor in marketplaceColors)
            {
                var companyColor = _context.CompanyColors.FirstOrDefault(x => x.Name.ToLower() == mColor.Name.ToLower());
                if (companyColor != null)
                    companyColors.Add(companyColor);
            }

            return companyColors.ToArray();
        }

        public ProductEntity[] GetProducts(List<int> ids)
        {
            var products = _context.Products.Where(x => ids.Contains(x.Id)).ToArray();
            return products;
        }

        public void SetProducts()
        {
            _context.SaveChanges();
        }

        public string SetColors(ProductEntity product, List<int> colors, MarketplaceColorEntity[] marketplaceColors, CompanyColorEntity[] companyColors)
        {
            string result = string.Empty;

            _context.ProductColors.RemoveRange(product.ProductColors);

            foreach (var colorId in colors)
            {
                var marketplaceColor = marketplaceColors.FirstOrDefault(x => x.Id == colorId);
                if (marketplaceColor == null)
                    marketplaceColor = _context.MarketplaceColors.FirstOrDefault(x => x.Id == colorId);
                if (marketplaceColor == null)
                {
                    result += " " + colorId + " - no mColor /";
                    continue; //throw new ServiceException("Marketplace color is null", "marketplce_color_null");
                }

                var companyColor = companyColors.FirstOrDefault(x => x.Name.ToLower() == marketplaceColor.Name.ToLower());
                if (companyColor == null)
                    companyColor = _context.CompanyColors.FirstOrDefault(x => x.Name.ToLower() == marketplaceColor.Name.ToLower());
                if (companyColor == null)
                {
                    companyColor = new CompanyColorEntity() { Name = marketplaceColor.Name.ToLower(), MarketplaceColorId = marketplaceColor.Id, MarketplaceColor = marketplaceColor };
                    _context.CompanyColors.Add(companyColor);
                    _context.SaveChanges();

                    companyColor = _context.CompanyColors.FirstOrDefault(x => x.Name.ToLower() == marketplaceColor.Name.ToLower());
                    if (companyColor == null)
                    {
                        result += " " + colorId + " - no cColor /";
                        continue; //throw new ServiceException("Company color is null", "company_color_null");
                    }
                }

                var productColor = _context.ProductColors.FirstOrDefault(x => x.CompanyColorId == companyColor.Id && x.ProductId == product.Id);
                if (productColor == null)
                {
                    productColor = new ProductColorEntity() { ProductId = product.Id, Product = product, CompanyColorId = companyColor.Id, CompanyColor = companyColor };
                    _context.ProductColors.Add(productColor);
                    _context.SaveChanges();

                    productColor = _context.ProductColors.FirstOrDefault(x => x.CompanyColorId == companyColor.Id && x.ProductId == product.Id);
                    if (productColor == null)
                        continue;  //throw new ServiceException("Product color is null", "product_color_null");
                }

                productColor.IsModerated = true;
                product.ProductColors.Add(productColor);
                result += " " + colorId + " - Ok /";
            }

            _context.SaveChanges();

            return result;
        }

        public string SetDescriptions(string ids, string descriptions)
        {
            var listIds = JsonConvert.DeserializeObject<int[]>(ids);

            var products = _context.Products
                .Where(x => listIds.Contains(x.Id))
                .ToArray();

            var productsId = "";
            foreach (var product in products)
            {
                product.Description = descriptions;
                productsId += product.Id + ", ";

                if (product.ModerationStatus == null)
                    product.ModerationStatus = ProductModerationStatus.Description;
                else product.ModerationStatus |= ProductModerationStatus.Description;
            }

            _context.SaveChanges();

            return productsId;
        }
    }    
}
