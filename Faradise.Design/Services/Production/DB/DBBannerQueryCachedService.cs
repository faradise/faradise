﻿using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.Services.Abstraction;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBannerQueryCachedService : CacheableService<IDBBannerQueryService>, IDBBannerQueryService
    {
        public DBBannerQueryCachedService(DBBannerQueryService decoratee, ICacheService cache) : base(decoratee, cache)
        {
        }

        public Banner GetBanner(int id)
        {
            return HandleCaching<Banner>(nameof(GetBanner), id);
        }

        public Banner[] GetAllActiveBanners()
        {
            return HandleCaching<Banner[]>(nameof(GetAllActiveBanners));
        }
    }
}