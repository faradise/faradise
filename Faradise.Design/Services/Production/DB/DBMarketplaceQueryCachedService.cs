﻿using System.Collections.Generic;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Abstraction;

namespace Faradise.Design.Services.Production.DB
{
    public class DBMarketplaceQueryCachedService : CacheableService<IDBMarketplaceQueryService>, IDBMarketplaceQueryService
    {
        public DBMarketplaceQueryCachedService(IDBMarketplaceQueryServiceProvider queryService, ICacheService cacheService)
            : base(queryService, cacheService)
        {   }

        public ProductInfo GetDailyProduct()
        {
            return HandleCaching<ProductInfo>(nameof(GetDailyProduct));
        }

        public ProductsQueryFilters GetQueryFilters(int categoryId)
        {
            return HandleCaching<ProductsQueryFilters>(nameof(GetQueryFilters), categoryId);
        }

        public Product GetProductById(int productId)
        {
            return Decoratee.GetProductById(productId);
        }

        public ProductsQuery GetProductsByFilters(ProductsQueryFiltersValues filters, ARPlatform platform, ARFormat format)
        {
            if (!string.IsNullOrEmpty(filters.SearchWords))
                return Decoratee.GetProductsByFilters(filters, platform, format);

            return HandleCaching<ProductsQuery>(nameof(GetProductsByFilters), filters, platform, format);
        }

        public Category[] GetCategories()
        {
            return HandleCaching<Category[]>(nameof(GetCategories));
        }

        public Category[] GetSubcategories(int id)
        {
            return HandleCaching<Category[]>(nameof(GetSubcategories), id);
        }

        public CategoryInfo[] GetCategoryPath(int categoryId)
        {
            return HandleCaching<CategoryInfo[]>(nameof(GetCategoryPath), categoryId);
        }

        public CategoryInfo[] GetProductCategoryPath(int productId)
        {
            return HandleCaching<CategoryInfo[]>(nameof(GetProductCategoryPath), productId);
        }

        public CategoryHierarchy[] GetCategoryHierarchy()
        {
            return HandleCaching<CategoryHierarchy[]>(nameof(GetCategoryHierarchy));
        }

        public int GetTotalProductsCount()
        {
            return HandleCaching<int>(nameof(GetTotalProductsCount));
        }

        public int GetAvailableProductsCount()
        {
            return HandleCaching<int>(nameof(GetAvailableProductsCount));
        }

        public ARProductDefinition GetARDefinition(int productId, ARPlatform platform, ARFormat format)
        {
            return HandleCaching<ARProductDefinition>(nameof(GetARDefinition), productId, platform, format);
        }

        public Category[] GetCategoriesByIds(IEnumerable<int> categoriesIds)
        {
            return HandleCaching<Category[]>(nameof(GetCategoriesByIds), categoriesIds);
        }
    }
}
