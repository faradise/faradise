﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Production.DB
{
    public class DBProjectChatService : IDBProjectChatService
    {
        private readonly FaradiseDBContext _context;

        public DBProjectChatService(FaradiseDBContext db)
        {
            _context = db;
        }

        public bool CheckClientNotifyISActive(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
            {
                throw new ServiceException("project_not_found", "project_not_found");
            }
            var group = _context.ChatGroups.Find(projectId);
            if (group == null)
            {
                group = new MessageGroupEntity { GroupId = projectId };
                _context.ChatGroups.Add(group);
                _context.SaveChanges();
            }
            return group.NotifyClient;
        }

        public bool CheckDesignerNotifyISActive(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
            {
                throw new ServiceException("project_not_found", "project_not_found");
            }
            var group = _context.ChatGroups.Find(projectId);
            if (group == null)
            {
                group = new MessageGroupEntity { GroupId = projectId };
                _context.ChatGroups.Add(group);
                _context.SaveChanges();
            }
            return group.NotifyDesigner;
        }

        public void SetClientNotificationIsActive(int projectId, bool isActive)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
            {
                throw new ServiceException("project_not_found", "project_not_found");
            }
            var group = _context.ChatGroups.Find(projectId);
            if (group == null)
            {
                group = new MessageGroupEntity { GroupId = projectId };
                _context.ChatGroups.Add(group);
                _context.SaveChanges();
            }
            group.NotifyClient = isActive;
            _context.SaveChanges();

        }

        public void SetDesignerNotificationIsActive(int projectId, bool isActive)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
            {
                throw new ServiceException("project_not_found", "project_not_found");
            }
            var group = _context.ChatGroups.Find(projectId);
            if (group == null)
            {
                group = new MessageGroupEntity { GroupId = projectId };
                _context.ChatGroups.Add(group);
                _context.SaveChanges();
            }
            group.NotifyDesigner = isActive;
            _context.SaveChanges();
        }
    }
}
