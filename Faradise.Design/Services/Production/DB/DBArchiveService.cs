﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Models.ArchiveUpdate;
using Faradise.Design.Models.DAL;

namespace Faradise.Design.Services.Production.DB
{
    public class DBArchiveService : IDBArchiveService
    {
        private FaradiseDBContext _context = null;

        public DBArchiveService(FaradiseDBContext context)
        {
            _context = context;
        }

        public void AddUpdateTask(ArchiveUpdateTask updateTask)
        {
            var oldTask = _context.ArchiveUpdateTasks.FirstOrDefault(x => x.ProjectId == updateTask.ProjectId && x.RoomId == updateTask.RoomId && x.Type == x.Type);
            if (oldTask != null)
                return;
            _context.ArchiveUpdateTasks.Add(new DAL.ArchiveUpdateTaskEntity
            {
                ProjectId = updateTask.ProjectId,
                RoomId = updateTask.RoomId,
                Type = updateTask.Type
            });
            _context.SaveChanges();
        }

        public void CompleteArchiveTask(int taskId)
        {
            var oldTask = _context.ArchiveUpdateTasks.Find(taskId);
            if (oldTask == null)
                return;
            _context.ArchiveUpdateTasks.Remove(oldTask);
            _context.SaveChanges();
        }

        public ArchiveUpdateTask GetNextArchiveTask()
        {
            var taskEntity = _context.ArchiveUpdateTasks.FirstOrDefault();
            if (taskEntity == null)
                return null;
            return new ArchiveUpdateTask
            {
                Id = taskEntity.Id,
                ProjectId = taskEntity.ProjectId,
                RoomId = taskEntity.RoomId,
                Type = taskEntity.Type
            };
        }
    }
}
