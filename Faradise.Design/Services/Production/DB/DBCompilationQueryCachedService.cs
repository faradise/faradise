﻿using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.Abstraction;

namespace Faradise.Design.Services.Production.DB
{
    public class DBCompilationQueryCachedService : CacheableService<IDBCompilationQueryService>, IDBCompilationQueryService
    {
        public DBCompilationQueryCachedService(DBCompilationQueryService queryService, ICacheService cacheService)
            : base(queryService, cacheService)
        {   }

        public ProductsQuery GetProductForCompilation(string compilationName, int offset, int count)
        {
            return HandleCaching<ProductsQuery>(nameof(GetProductForCompilation), compilationName, offset, count);
        }
    }
}
