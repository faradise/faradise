﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Brigade;
using Faradise.Design.Models.DAL;
using Faradise.Design.Services.DataMappers;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBrigadeService : IDBBrigadeService
    {
        private readonly FaradiseDBContext _context;

        public DBBrigadeService(FaradiseDBContext db)
        {
            _context = db;
        }

        public int AddFile(int projectId, string fileLink, string filename)
        {
            var specExist = _context.BrigadeTechSpecs.Any(x=>x.ProjectId==projectId);
            if (!specExist)
                throw new ServiceException($"Brigade specification not found for project {projectId}", "brigade_techSpec_not_found");
            var file = new BrigadeTechSpecFileEntity()
            {
               BrigadeTechSpecId=projectId,
               Name=filename,
               Url=fileLink
            };
            _context.BrigadeTechSpecFiles.Add(file);
            _context.SaveChanges();
            return file.Id;
        }

        public void CreateTechSpec(int projectId, int designerId)
        {
            var spec = new BrigadeTechSpecEntity
            {
                ProjectId = projectId,
                DesignerId = designerId,
                Description = string.Empty,
                Status = BrigadeWorkStatus.Created,
                PublishTime = DateTime.MaxValue
            };
            _context.BrigadeTechSpecs.Add(spec);
            _context.SaveChanges();
        }

        public BrigadeTechSpec GetBrigadeTechSpec(int projectId)
        {
            var brigadeSpec = _context.BrigadeTechSpecs.Find(projectId);
            if (brigadeSpec== null)
                throw new ServiceException($"Brigade specification not found for project {projectId}", "brigade_techSpec_not_found");
            var files = brigadeSpec.SpecFiles.ToArray();
            var roomInfos = GetRoomInfos(projectId);
            return new BrigadeTechSpec
            {
                ProjectId = brigadeSpec.ProjectId,
                Description = brigadeSpec.Description,
                PublishTime = brigadeSpec.PublishTime,
                DesignerId = brigadeSpec.DesignerId,
                Status = brigadeSpec.Status,
                SpecFiles = files.Select(x => x.ToService()).ToArray(),
                RoomInfos = roomInfos
            };
        }

        public BrigadeRoomInfo[] GetRoomInfos(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project== null)
                throw new ServiceException($"project {projectId} not found ", "project_not_found");
            var infos = project.Rooms.Select(x => new BrigadeRoomInfo
            {
                Purpose = x.Room,
                Zones = x.ZonePhotos.Select(y => y.Url).ToArray(),
                ZoneArchive = x.ZonePhotoArchiveUrl,
                Collages = x.CollagesPhotos.Select(y => y.Url).ToArray(),
                CollageArchive = x.CollagePhotoArchiveUrl,
                WorkPlans = x.Plan.Photos.Select(y => y.Url).ToArray(),
                WorkPlansArchive = x.PlanUrl
            });
            return infos.ToArray();
        }

        public bool HasTechSpec(int projectId)
        {
            return _context.BrigadeTechSpecs.Any(x => x.ProjectId == projectId);
        }

        public void RemoveFile(int projectId, int fileId)
        {
            var file = _context.BrigadeTechSpecFiles.Find(fileId);
            if(file== null)
                throw new ServiceException($"project {projectId} not found ", "project_not_found");
            _context.BrigadeTechSpecFiles.Remove(file);
            _context.SaveChanges();
        }

        public void SetDescription(int projectId, string description)
        {
            var brigadeSpec = _context.BrigadeTechSpecs.Find(projectId);
            if (brigadeSpec == null)
                throw new ServiceException($"Brigade specification not found for project {projectId}", "brigade_techSpec_not_found");
            brigadeSpec.Description = description;
            _context.SaveChanges();
        }

        public void SetRoomInfos(int projectId, BrigadeRoomInfo[] roomInfos)
        {
            throw new NotImplementedException();
        }

        public void SetStatus(int projectId, BrigadeWorkStatus status)
        {
            var brigadeSpec = _context.BrigadeTechSpecs.Find(projectId);
            if (brigadeSpec == null)
                throw new ServiceException($"Brigade specification not found for project {projectId}", "brigade_techSpec_not_found");
            brigadeSpec.Status = status;
            _context.SaveChanges();
        }
    }
}
