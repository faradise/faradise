﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Designer;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Services.DataMappers;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBDesignerService : IDBDesignerService
    {
        private readonly FaradiseDBContext _context;

        public DBDesignerService(FaradiseDBContext db)
        {
            _context = db;
            SetDefaultTest();
        }

        private void SetDefaultTest()
        {
            var testQuestions = _context.TestQuestionDescriptions.Where(x => x.For == TestType.Designer).ToList();
            if (!testQuestions.Any())
            {
                var questions = new TestQuestionDescription[]
                {
                    new TestQuestionDescription(0, "Вы не видите свой интерьер без:")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(0, "Вау-эффекта, неожиданных цветовых сочетаний, комбинации несовместимых предметов и фактур;"),
                            new TestAnswerDescription(1, "Лаконичных форм, чистых линий и сдержанных цветов"),
                            new TestAnswerDescription(2, "Натуральных материалов близких к природным фактурам и естественные оттенков.")
                        }
                    },
                    new TestQuestionDescription(1, "Какие поверхности кажутся вам более приятными на ощупь?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(3, "Все гладкое и блестящее"),
                            new TestAnswerDescription(4, "Все натуральное — дерево без лака, керамика без глазури"),
                            new TestAnswerDescription(5, "Оригинальное сочетания материалов: бетон и кожа, фарфор и метал")
                        }
                    },
                    new TestQuestionDescription(2, "Какие цвета вы предпочитаете?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(6, "Нейтральную цветовую гамму, где в основе такие цвета, как белый, бежевый, серый, черный"),
                            new TestAnswerDescription(7, "Буйство красок, необычные цветовые сочетания"),
                            new TestAnswerDescription(8, "Теплые природные тона и нежные пастельные оттенки")
                        }
                    },
                    new TestQuestionDescription(3, "Что бы вы никогда не сделали у себя дома?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(9, "Золотой унитаз"),
                            new TestAnswerDescription(10, "Глянцевые наливные полы"),
                            new TestAnswerDescription(11, "Зеркало в деревянной раме с витиеватым узоро")
                        }
                    },
                    new TestQuestionDescription(4, "Что бы вы хотели получить в подарок на новоселье?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(12, "Простое, но стильное, будь то лаконичная белая ваза или современная картина"),
                            new TestAnswerDescription(13, "Подхваты для штор или латунный антикварный поднос"),
                            new TestAnswerDescription(14, "Нечто необычное, цепляющее взгляд - диковинный предмет или яркий постер в стиле поп-арт")
                        }
                    }
                };

                foreach (var question in questions)
                {
                    var qEntity = new TestQuestionDescriptionEntity
                    {
                        Text = question.Text,
                        For = TestType.Designer,
                    };
                    _context.TestQuestionDescriptions.Add(qEntity);
                    _context.SaveChanges();
                    foreach (var answer in question.Answers)
                    {
                        var aEntity = new TestAnswerDescriptionEntity
                        {
                            QuestionId = qEntity.Id,
                            For = TestType.Designer,
                            Text = answer.Text
                        };
                        _context.TestAnswerDescriptions.Add(aEntity);
                    }
                    _context.SaveChanges();
                }
            };
        }

        public void AddAvatar(int userId, string path)
        {
            var designer = _context.Designers.Find(userId);
            if(designer==null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.PhotoLink = path;
            _context.SaveChanges();
        }

        public int AddPortfolioPhoto(int id, int portfolioId, string path)
        {
            var portfolio = _context.DesignerPortfolioProjects.Find(portfolioId);
            if (portfolio == null)
                throw new ServiceException("portfolio not found", "portfolio_not_found");
            var photo = new DesignerPortfolioProjectPhotoEntity
            {
                PortfolioProjectId = portfolioId,
                Url = path
            };
            _context.DesignerPortfolioProjectPhotos.Add(photo);
            _context.SaveChanges();
            return photo.Id;

        }

        public bool CreateDesigner(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException("user not found", "user_not_found");
            var designer = new DesignerEntity { Id = id };
            _context.Designers.Add(designer);
            _context.SaveChanges();
            return true;
        }

        public int CreatePortfolioProject(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            var portfolioProject = new DesignerPortfolioProjectEntity { DesignerId = id };
            _context.DesignerPortfolioProjects.Add(portfolioProject);
            _context.SaveChanges();
            return portfolioProject.Id;
        }

        public bool GetApproval(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.TestApproved;
        }

        public DesignerBase GetBaseModel(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            var user = designer.User;
            return designer.ToService(user);
        }

        public Designer GetDesigner(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");

            var designerModel = new Designer
            {
                Id = id,
                Base = designer.ToService(designer.User),
                LegalModel = new DesignerLegal()
                {
                    LegalType = designer.LegalType,
                    CompanyLegalInfo = designer.CompanyLegalInfo != null ? designer.CompanyLegalInfo.ToService() : null,
                    PersonalLegalInfo = designer.PersonalLegalInfo != null ? designer.PersonalLegalInfo.ToService(designer.User.Phone) : null

                },
                Level = designer.Level,
                PhotoLink = designer.PhotoLink,
                Portfolio = designer.Projects.ToService(),
                Possibilities = designer.Possibilities.ToService(),
                Styles = designer.Styles.ToService(),
                Testing = designer.TestResults.ToService(),
                Stage = designer.Stage,
                PortfolioLink = designer.PortfolioLink,
                Phone = designer.User.Phone,
                TestJob = designer.TestJobInfo.ToService()
            };

            if (designerModel.Possibilities == null)
                designerModel.Possibilities = new DesignerPossibility();
            return designerModel;
        }

        public TestQuestionDescription[] GetDesignerQuestions()
        {
            return _context.TestQuestionDescriptions
                .Where(x => x.For == TestType.Designer)
                .GroupJoin(
                _context.TestAnswerDescriptions,
                q=>q.Id,
                a=>a.QuestionId,
                (q,a)=>new TestQuestionDescription()
                {
                    Id = q.Id,
                    Text = q.Text,
                    Answers=a.Select(y => new TestAnswerDescription { Id = y.Id, Text = y.Text }).ToArray()
                }).ToArray();

        }

        public DesignerLegal GetLegalModel(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return new DesignerLegal
            {
                LegalType = designer.LegalType,
                CompanyLegalInfo = designer.CompanyLegalInfo != null ? designer.CompanyLegalInfo.ToService() : null,
                PersonalLegalInfo = designer.PersonalLegalInfo != null ? designer.PersonalLegalInfo.ToService(designer.User.Phone) : null
            };
        }

        public string GetPhotoLink(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.PhotoLink;
        }

        public Portfolio GetPortfolio(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.Projects.ToService();
        }


        public DesignerPossibility GetPossibilities(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.Possibilities.ToService();
        }

        public DesignerStyle[] GetStyles(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.Styles.ToService();
        }

        public DesignerLevel GetTariff(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.Level;
        }

        public TestResults GetTest(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.TestResults.ToService();
        }

        public string GetTestLink(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.TestJobLink;
        }

        public void RemoveAvatar(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.PhotoLink = String.Empty;
            _context.SaveChanges();
        }

        public void RemovePortfolioProject(int id, int portfolioId)
        {
            var portfolioProject = _context.DesignerPortfolioProjects.Find(portfolioId);
            if (portfolioProject == null)
                throw new ServiceException("portfolioProject not found", "portfolioProject_not_found");
            if(portfolioProject.DesignerId!=id)
                throw new ServiceException("user not owner", "user_not_owner");
            _context.DesignerPortfolioProjects.Remove(portfolioProject);
            _context.SaveChanges();
        }

        public void RemovePortfolioPhoto(int id, RemovePortfolioPhoto model)
        {
            var portfolioPhoto = _context.DesignerPortfolioProjectPhotos.Find(model.PhotoId);
            if (portfolioPhoto == null)
                throw new ServiceException("portfolioPhoto not found", "portfolioPhoto_not_found");
            if (portfolioPhoto.Portfolio.DesignerId != id)
                throw new ServiceException("user not owner", "user_not_owner");
            _context.DesignerPortfolioProjectPhotos.Remove(portfolioPhoto);
            _context.SaveChanges();
        }

        public bool SetBaseModel(int id, DesignerBase baseModel)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            baseModel.ToEntity(designer);
            _context.SaveChanges();
            return true;
        }

        public bool SetDesigner(int projectId, Designer model)
        {
            var designer = _context.Designers.Find(model.Id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            
            return true;

        }

        public bool SetLegalModel(int id, DesignerLegal legal)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.LegalType = legal.LegalType;
            legal.ToEntity(designer);
            _context.SaveChanges();
            return true;
        }

        public bool SetPhotoLink(int id, string link)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.PhotoLink = link;
            _context.SaveChanges();
            return true;
        }

        public bool SetPortfolio(int id, int portfolioId, string name, string description)
        {
            var portfolioProject = _context.DesignerPortfolioProjects.Find(portfolioId);
            if (portfolioProject == null)
                throw new ServiceException("portfolioProject not found", "portfolioProject_not_found");
            if (portfolioProject.DesignerId != id)
                throw new ServiceException("user not owner", "user_not_owner");
            portfolioProject.Name = name;
            portfolioProject.Description = description;
            _context.SaveChanges();
            return true;

        }

        public bool SetPossibilities(int id, DesignerPossibility possibilities)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            if (designer.Possibilities == null)
                designer.Possibilities = new DesignerPossibilityEntity();
            possibilities.ToEntity(designer.Possibilities);
            _context.SaveChanges();
            return true;
        }

        public bool SetStyles(int id, DesignerStyle[] styles)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            var oldStyles = _context.DesignerStyles.Where(x => x.DesignerId == id).ToList();
            if (oldStyles != null)
                _context.DesignerStyles.RemoveRange(oldStyles);
            var newStyles = styles.Select(x => new DesignerStyleEntity()
            {
                DesignerId = id,
                Description = x.Description,
                Name = x.Name
            });
            _context.DesignerStyles.AddRange(newStyles);
            _context.SaveChanges();
            return true;
        }

        public bool SetTariff(int id, DesignerLevel tariff)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.Level = tariff;
            _context.SaveChanges();
            return true;
        }

        public bool SetTest(int id, TestResults test)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            if (designer.TestResults != null)
                _context.DesignerTestResultsAnswers.RemoveRange(designer.TestResults);
            _context.DesignerTestResultsAnswers.AddRange(test.ToService(id));
            _context.SaveChanges();
            return true;
        }

        public bool SetTestApproved(int id, bool approved)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            if (designer.TestJobInfo == null)
                designer.TestJobInfo = new DesignerTestJobInfoEntity();
            designer.TestJobInfo.TestApproved = approved;
            _context.SaveChanges();
            return true;
        }

        public bool SetTestLink(int id, string testlink)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            if (designer.TestJobInfo == null)
                designer.TestJobInfo = new DesignerTestJobInfoEntity();
            designer.TestJobInfo.TestLink = testlink;
            designer.TestJobInfo.TestTime = DateTime.UtcNow;
            _context.SaveChanges();
            return true;
        }

        public Designer[] GetDesigners(int offset, int count)
        {
            var designers=_context.Designers
                .Skip(offset)
                .Take(count)
                .Include(x=>x.Projects)
                .ToArray();
            var conv=designers.Select(x => x.ToFullServiceModel(x.User, x.Projects)).ToArray();
            return conv;
        }

        public string GetPortfolioLink(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.PortfolioLink;
        }

        public TestJobInfo GetTestJobInfo(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            return designer.TestJobInfo.ToService();
        }

        public void SetPortfolios(int id, PortfolioDescriptions model)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            var projects = designer.Projects.ToList();
            foreach (var description in model.Descriptions)
            {
                try
                {
                    var portfolio = projects.FirstOrDefault(x => x.Id == description.Id);
                    if (portfolio != null)
                    {
                        portfolio.Name = description.Name;
                        portfolio.Description = description.Description;
                    }
                    else
                    {
                        throw new ServiceException("Portfolio not found", "portfolio_not_found");
                    }
                }
                catch
                {
                    continue;
                }
                    
            }
            _context.SaveChanges();
        }

        public bool SetPortfolioLink(int id, string portfoliolink)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("designer not found", "designer_not_found");
            designer.PortfolioLink = portfoliolink;
            _context.SaveChanges();
            return true;
        }

        public void SetPhone(int id, string phone)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException("Designer not found", "designer_not_found");
            user.Phone = phone;
            _context.SaveChanges();
        }

        public void AddBaseInfoToLegalInfo(int id, string name, string surname, string phone)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("Designer not found", "designer_not_found");

            if (designer.LegalType == LegalType.Personal)
            {
                var personalInfo = designer.PersonalLegalInfo;
                if (personalInfo == null)
                    personalInfo = new DesignerPersonalLegalInfoEntity();

                if (name != null)
                    personalInfo.Name = name;

                if (surname != null)
                    personalInfo.Surname = surname;
            }
            if (phone != null)
                designer.User.Phone = phone;
            _context.SaveChanges();
        }

        public string GetPhone(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException("Designer not found", "designer_not_found");
            return user.Phone;
        }

        public bool SetRegistrationStage(int id, DesignerRegistrationStage stage)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("Designer not found", "designer_not_found");
            designer.Stage = stage;
            _context.SaveChanges();
            return true;
        }

        public DesignerRegistrationStage GetRegistrationStage(int id)
        {
            var designer = _context.Designers.Find(id);
            if (designer == null)
                throw new ServiceException("Designer not found", "designer_not_found");
            return designer.Stage;
        }

        public List<DesignerShortInfo> GetDesignersShortInfos()
        {
            var designers = _context.Designers.Select(x => new DesignerShortInfo
            {
                Id = x.Id,
                Name = x.Name,
                Surname = x.Surname,
                Email=x.User.Email,
                Phone=x.User.Phone,
                AppointedProjectsCount = x.Pools.Count(),
                ProjectsInWorkCount = x.ProjectsInWork.Count(),
                RejectedProjectsCount = x.Pools.Where(y => y.Status == Models.Enums.DesignerStatusInPool.Rejected).Count(),
                Stage = x.Stage
            }).ToList();
            return designers;
        }

        public void DeleteDesigner(int designerId)
        {
            var existing = _context.Users.Find(designerId);
            if (existing != null)
            {
                _context.Users.Remove(existing);
                _context.SaveChanges();
            }
        }

        public int[] GetDesignerProjects(int designerId)
        {
            var projects = _context.ProjectDefinitions
                .Where(x => x.DesignerId == designerId)
                .Select(x => x.Id)
                .ToArray();
            return projects;
        }
    }
}