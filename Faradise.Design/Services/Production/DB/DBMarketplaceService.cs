﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.DAL;
using Faradise.Design.Extensions;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Petrovich;
using Faradise.Design.Models.YMLUpdater;
using Faradise.Design.Services.DataMappers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Production.DB
{
    public class DBMarketplaceService : IDBMarketplaceService
    {
        private readonly FaradiseDBContext _context;

        public DBMarketplaceService(FaradiseDBContext db)
        {
            _context = db;
        }

        public int AddCategory(Category category)
        {
            var entity = new MarketplaceCategoryEntity { Name = category.Name };

            entity.MarketplaceId = _context.MarketplaceEntity.FirstOrDefault().Id;

            _context.MarketPlaceCategories.Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }

        public string GetCompanyNameById(int id)
        {
            var company = _context.Companies.Select(x => new { x.Id, x.Name }).FirstOrDefault(x => x.Id == id);
            return company != null ? company.Name : string.Empty;
        }


        public void EnableCategory(int categoryId)
        {
            var category = _context.MarketPlaceCategories.Find(categoryId);
            if (category == null)
                throw new ServiceException("category_not_found", "category_not_found");

            category.MarketplaceId = _context.MarketplaceEntity.FirstOrDefault().Id;
            _context.SaveChanges();
        }

        public void DisableCategory(int categoryId)
        {
            var category = _context.MarketPlaceCategories.Find(categoryId);
            if (category == null)
                throw new ServiceException("category_not_found", "category_not_found");

            category.MarketplaceId = null;
            _context.SaveChanges();
        }

        public bool ModifyCompany(Company company)
        {
            var entity = _context.Companies.Find(company.Id);
            if (entity == null)
                throw new ServiceException("company_not_found", "company_not_found");

            entity.Name = company.Name;
            entity.Email = company.Email;
            entity.Url = company.Url;
            entity.LegalName = company.LegalName ?? string.Empty;
            entity.CompanyProductionMask = (int)company.CompanyProductionMask;
            entity.ContactPersonEmail = company.ContactPersonEmail ?? string.Empty;
            entity.ContactPersonName = company.ContactPersonName ?? string.Empty;
            entity.ContactPersonPhone = company.ContactPersonPhone ?? string.Empty;
            entity.DontSupportPromocode = company.DontSupportPromocode;
            entity.DeliveryInfo = company.DeliveryInfo ?? string.Empty;
            entity.FeedUrl = company.FeedUrl;
            entity.EnableAutofeed = company.EnableAutofeed;
            entity.DailyProductPriority = company.DailyProductPriority;
            _context.SaveChanges();

            return true;
        }

        public void AddCompanyNote(int companyId, string note)
        {
            var entity = _context.Companies.Find(companyId);
            if (entity == null)
                throw new ServiceException("company_not_found", "company_not_found");
            var notes = ExtractNotes(entity);
            notes.Add(note);
            entity.NotesJson = JsonConvert.SerializeObject(notes);
            _context.SaveChanges();
        }

        public void DeleteCompanyNote(int companyId, int noteId)
        {
            var entity = _context.Companies.Find(companyId);
            if (entity == null)
                throw new ServiceException("company_not_found", "company_not_found");

            var notes = ExtractNotes(entity);
            if (notes.Count <= noteId)
                return;

            notes.RemoveAt(noteId);
            entity.NotesJson = JsonConvert.SerializeObject(notes);

            _context.SaveChanges();
        }

        public void ChangeCompanyNote(int companyId, int noteId, string newNote)
        {
            var entity = _context.Companies.Find(companyId);
            if (entity == null)
                throw new ServiceException("company_not_found", "company_not_found");

            var notes = ExtractNotes(entity);
            if (notes.Count <= noteId)
                return;

            notes[noteId] = newNote;
            entity.NotesJson = JsonConvert.SerializeObject(notes);

            _context.SaveChanges();
        }

        private static List<string> ExtractNotes(CompanyEntity entity)
        {
            List<string> notes = null;

            if (!string.IsNullOrEmpty(entity.NotesJson))
                notes = JsonConvert.DeserializeObject<List<string>>(entity.NotesJson);

            return notes ?? new List<string>();
        }

        public void DeleteCategory(int categoryId)
        {
            var category = _context.MarketPlaceCategories.Find(categoryId);
            if (category == null)
                throw new ServiceException("category_not_found", "category_not_found");

            _context.MarketPlaceCategories.Remove(category);
            _context.SaveChanges();
        }

        public List<CompanyInfo> GetAllCompanies(bool loadAdditinalInfo)
        {
            return _context.Companies.Select(x => new { MarketCompany = x, Feed = _context.YmlUpdates.Where(f => f.CompanyId == x.Id).OrderByDescending(f => f.StatusChangeTime).FirstOrDefault() })//.Join(_context.YmlUpdates, c => c.Id, t => t.CompanyId, (c, t) => new { MarketCompany = c, UpdateTask = t })
                .Select(x => new CompanyInfo
                {
                    Id = x.MarketCompany.Id,
                    Name = x.MarketCompany.Name,

                    IsEnabled = x.MarketCompany.MarketplaceId != null ? true : false,

                    TotalCategories = x.MarketCompany.CompanyCategories.Count,

                    MappedCategories = x.MarketCompany.CompanyCategories
                        .Count(c => c.CategoryId != null),

                    TotalProducts = x.MarketCompany.CompanyCategories
                        .SelectMany(c => c.Products)
                        .Count(p => !p.IsRemoved),

                    MappedProducts = x.MarketCompany.CompanyCategories
                        .Where(c => c.CategoryId != null)
                        .SelectMany(c => c.Products)
                        .Count(p => !p.IsRemoved),

                    NotInTradeCategories = loadAdditinalInfo ? x.MarketCompany.CompanyCategories.Where(c => c.CategoryId == null).SelectMany(p => p.Products).Select(p => new { p.IsRemoved, p.Available, p.Price, p.Name }).Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name)) : false,
                    MapNotToLeafCategories = loadAdditinalInfo ? x.MarketCompany.CompanyCategories.Where(c => c.CategoryId != null && c.Category.Childs.Any()).SelectMany(p => p.Products).Select(p => new { p.IsRemoved, p.Available, p.Price, p.Name }).Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name)) : false,
                    LastFeed = x.Feed != null ? new CompanyLastFeedInfo() { LastFeedStatus = x.Feed.Status, LastFeedStatusChangeTime = x.Feed.StatusChangeTime, Source = x.Feed.DownloadSource } : null
                })
                .ToList();
        }

        public List<Category> GetCategories()
        {
            var categories = _context.MarketPlaceCategories
                .Select(x => MarketplaceCategoryEntity.AsCategory(x))
                .OrderByDescending(x => x.Priority)
                .ThenBy(x => x.Id)
                .ToList();

            return categories;
        }

        public List<Category> GetSubcategories(int id)
        {
            var exists = _context.MarketPlaceCategories.Any(x => x.Id == id);

            if (!exists)
                throw new ServiceException($"category with id {id} was not found", "category_not_found");

            var result = _context.MarketPlaceCategories
                .Where(x => x.Id == id)
                .SelectMany(x => x.Childs)
                .Select(x => MarketplaceCategoryEntity.AsCategory(x))
                .OrderByDescending(x => x.Priority)
                .ThenBy(x => x.Id)
                .ToList();

            return result;
        }

        public Company GetCompanyById(int id)
        {
            var company = _context.Companies
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    Company = x,
                    AllCategories = x.CompanyCategories,
                    MapedCategories = x.CompanyCategories.Where(c => c.CategoryId != null),
                    CanTradeProducts = x.CompanyCategories.SelectMany(p => p.Products).Where(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name)),
                    ProductsInSale =  x.CompanyCategories.SelectMany(p => p.Products).Where(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name) && p.PreviousPrice != null && p.Price < p.PreviousPrice)
                })
                .Select(x => new Company
                {
                    Id = x.Company.Id,
                    Name = x.Company.Name,
                    Email = x.Company.Email,
                    Url = x.Company.Url,
                    LegalName = x.Company.LegalName,
                    NotesJson = x.Company.NotesJson,
                    DeliveryInfo = x.Company.DeliveryInfo,
                    ContactPersonEmail = x.Company.ContactPersonEmail,
                    ContactPersonName = x.Company.ContactPersonName,
                    CompanyProductionMask = (CompanyProduction)x.Company.CompanyProductionMask,
                    ContactPersonPhone = x.Company.ContactPersonPhone,
                    DailyProductPriority = x.Company.DailyProductPriority,
                    DontSupportPromocode = x.Company.DontSupportPromocode,

                    AllProducts = x.AllCategories.SelectMany(p => p.Products).Count(),
                    AllCategories = x.AllCategories.Count(),

                    CanTradeCategories = x.AllCategories.Count(c => c.Products.Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name))),
                    CanTradeProducts = x.CanTradeProducts.Count(),

                    InTradeCategories = x.MapedCategories.Count(c => c.Products.Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name))),
                    InTradeProducts = x.MapedCategories.SelectMany(p => p.Products).Count(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name)),

                    NotInTradeCategories = x.AllCategories.Count(c => c.CategoryId == null && c.Products.Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name))),
                    NotInTradeProducts = x.AllCategories.Where(c => c.CategoryId == null).SelectMany(p => p.Products).Count(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name)),

                    SalesCategories = x.AllCategories.Count(c => c.Products.Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && p.Price < p.PreviousPrice && !string.IsNullOrEmpty(p.Name))),
                    SalesProducts = x.ProductsInSale.Count(),

                    ProductsNotInLeafs = x.MapedCategories.Where(c => c.Category.Childs.Any()).Any(c => c.Products.Any(p => !p.IsRemoved && (p.Available == null || p.Available.Value) && p.Price > 0 && !string.IsNullOrEmpty(p.Name))),

                    MinSale = x.ProductsInSale.Max(p => (p.Price * 100)/ p.PreviousPrice),
                    MaxSale = x.ProductsInSale.Min(p => (p.Price * 100)/ p.PreviousPrice),

                    IsEnabled = x.Company.MarketplaceId != null ? true : false,
                    EnableAutofeed = x.Company.EnableAutofeed,
                    FeedUrl = x.Company.FeedUrl
                })
                .FirstOrDefault();

            if (company == null)
                throw new ServiceException("company_not_found", "company_not_found");

            return company;
        }

        public bool EditCategory(Category category)
        {
            var categoryEntity = _context.MarketPlaceCategories.Find(category.Id);
            if (categoryEntity == null)
                throw new ServiceException("category_not_found", "category_not_found");

            categoryEntity.Name = category.Name;
            categoryEntity.InHeader = category.InHeader;
            categoryEntity.IsPopular = category.IsPopular;
            categoryEntity.ImageUrl = category.ImageUrl;
            categoryEntity.DeliveryCost = category.DeliveryCost;
            categoryEntity.DeliveryDays = category.DeliveryDays;
            categoryEntity.Priority = category.Priority;
            categoryEntity.DailyProductPriority = category.DailyProductPriority;
            categoryEntity.ParentId = category.ParentId > 0
                ? new int?(category.ParentId)
                : null;

            if (category.SelectedParametersIds == null || category.SelectedParametersIds.Length == 0)
            {
                categoryEntity.Parameters.ForEach(entity => _context.CategoryParameters.Remove(entity));
            }
            else
            {
                categoryEntity.Parameters =
                    category.SelectedParametersIds
                        .Except(categoryEntity.Parameters.Select(entity => entity.CategoryId))
                        .Select(id => new CategoryParameterEntity
                        {
                            ParameterId = id, CategoryId = categoryEntity.Id
                        }).ToList();
            }
            _context.SaveChanges();

            return true;
        }

        public int CreateCompany()
        {
            var company = new CompanyEntity();

            company.MarketplaceId = _context.MarketplaceEntity.FirstOrDefault().Id;

            _context.Companies.Add(company);
            _context.SaveChanges();

            return company.Id;
        }

        public void EnableCompany(int companyId)
        {
            var company = _context.Companies.FirstOrDefault(x => x.Id == companyId);
            if (company == null)
                throw new ServiceException("company_not_found", "company_not_found");

            company.MarketplaceId = _context.MarketplaceEntity.FirstOrDefault().Id;
            _context.SaveChanges();
        }

        public void DisableCompany(int companyId)
        {
            var company = _context.Companies.FirstOrDefault(x => x.Id == companyId);
            if (company == null)
                throw new ServiceException("category_not_found", "category_not_found");

            company.MarketplaceId = null;
            _context.SaveChanges();
        }

        public List<CompanyCategory> GetCompanyCategories(int companyId)
        {
            var categories = _context.CompanyCategories
                .Where(x => x.CompanyId == companyId)
                .Select(x => new CompanyCategory
                {
                    CompanyId = companyId,
                    CategoryId = x.CategoryId ?? 0,
                    CompanyCategoryId = x.CompanyCategoryId,
                    ParentCompanyCategoryId = x.ParentCompanyCategoryId ?? 0,
                    CompanyCategoryName = x.Name,
                    ProductsCount = x.Products.Count,
                    Rooms = x.Rooms.Select(y => new RoomName { Id = y.RoomNameId, Name = y.RoomName.Name }).ToArray()
                })
                .ToList();

            return categories;
        }

        public bool ChangeProductCategory(int productId, int categoryId)
        {
            var product = _context.Products
                .Where(x => x.Id == productId)
                .FirstOrDefault();
                        
            var categoryName = _context.MarketPlaceCategories.Find(categoryId).Name;

            var companyCategories = _context.Companies
                .Where(x => x.Id == product.CompanyId)
                .Select(x => x.CompanyCategories)
                .FirstOrDefault()
                .ToList();
            
            if (companyCategories == null)
                throw new ServiceException("company_categories_not_found", "company_categories_not_found");

            bool isExists = false;
            int companyCategoryId = 0;

            foreach (var cat in companyCategories)
            {
                if (string.Equals(cat.Name.ToLower(), categoryName.ToLower()))
                {
                    isExists = true;
                    companyCategoryId = cat.CompanyCategoryId;
                }
            }

            if(isExists == true)
            {
                product.CompanyCategoryId = companyCategoryId;
            }
            else
            {
                int maxId = companyCategories.Max(x => x.CompanyCategoryId);

                var companyCategory = new CompanyCategoryEntity() { CompanyCategoryId = ++maxId, CompanyId = product.CompanyId, Name = categoryName, CategoryId = categoryId };
                
                _context.CompanyCategories.Add(companyCategory);

                product.CompanyCategoryId = companyCategory.CompanyCategoryId;
            }

            _context.SaveChanges();

            return true;
        }

        public CompanyCategory GetCompanyCategory(int companyId, int companyCategoryId)
        {
            var entity = _context.CompanyCategories
                .Where(x => x.CompanyId == companyId && x.CompanyCategoryId == companyCategoryId)
                .Include("Parent")
                .FirstOrDefault();

            if (entity == null)
                throw new ServiceException("category_not_found", "category_not_found");

            var depth = 0;
            AddDepth(entity, ref depth);

            return new CompanyCategory
            {
                CompanyId = companyId,
                CategoryId = entity.CategoryId ?? 0,
                CompanyCategoryId = entity.CompanyCategoryId,
                ParentCompanyCategoryId = entity.ParentCompanyCategoryId ?? 0,
                CompanyCategoryName = entity.Name,
                ProductsCount = entity.Products.Count,
                Rooms = entity.Rooms.Select(y => new RoomName { Id = y.RoomNameId, Name = y.RoomName.Name }).ToArray(),
                Depth = depth
            };

        }

        private static void AddDepth(CompanyCategoryEntity entity, ref int depth)
        {
            while (true)
            {
                if (entity.Parent == null)
                    return;

                depth += 1;

                entity = entity.Parent;
            }
        }

        public int[] MapCompanyCategory(MapCategory category)
        {
            var entity = _context.CompanyCategories.Where(x=>x.CompanyId==category.CompanyId&&x.CompanyCategoryId==category.CompanyCategoryId).Include("Childs").FirstOrDefault();
            if (entity == null)
                throw new ServiceException("category_not_found", "category_not_found");

            var nextId = category.CategoryId == 0
                ? new int?()
                : new int?(category.CategoryId);

            entity.CategoryId = nextId;

            var mapped = new List<int> { entity.CompanyCategoryId };
            MapChildCategory(entity.Childs.ToList(), nextId, mapped);
            _context.SaveChanges();

            return mapped.ToArray();
        }

        private void MapChildCategory(List<CompanyCategoryEntity> childs, int? categoryId, List<int> mapped)
        {
            foreach (var child in childs)
            {
                mapped.Add(child.CompanyCategoryId);
                child.CategoryId = categoryId;
                MapChildCategory(child.Childs.ToList(), categoryId, mapped);
            }
        }

        public bool MapCompanyCategories(List<MapCategory> mappings, int companyId)
        {
            var existingCategoryEntities = _context.CompanyCategories
                .Where(x => x.CompanyId == companyId)
                .ToList();

            Parallel.ForEach(mappings, mapping =>
            {
                var entity = existingCategoryEntities
                    .FirstOrDefault(x => x.CompanyCategoryId == mapping.CompanyCategoryId);

                if (entity == null)
                    return;

                if (!entity.CategoryId.HasValue && mapping.CategoryId == 0)
                    return;

                if (entity.CategoryId == mapping.CategoryId)
                    return;

                if (mapping.CategoryId == 0)
                    entity.CategoryId = null;
                else entity.CategoryId = mapping.CategoryId;
            });

            _context.SaveChanges();

            return true;
        }

        public bool MapPetrovichCategories(PetrovichListModel petrovichList, int companyId)
        {
            var existingCategoryEntities = _context.CompanyCategories
                .Where(x => x.CompanyId == companyId)
                .ToList();

            var marketplaceCategories = _context.MarketPlaceCategories.ToList();

            foreach(var row in petrovichList.PetrovichModels)
            {
                var companyCategory = existingCategoryEntities.FirstOrDefault(x => x.Name.ToLower() == row.CompanyCategory.ToLower());

                if (companyCategory == null)
                    continue;

                var marketplaceCategory = marketplaceCategories.FirstOrDefault(x => x.Name?.ToLower() == row.MarketplaceCategory.ToLower());
                if(marketplaceCategory == null)
                {
                    MarketplaceCategoryEntity marketplaceCategoryParent = null;
                    if (string.IsNullOrEmpty(row.MarketplaceCategoryParent))
                    {
                        marketplaceCategoryParent = marketplaceCategories.FirstOrDefault(x => x.Name?.ToLower() == "Все для ремонта".ToLower());//создать категорию "все для ремонта"
                    }
                    else
                    {
                        marketplaceCategoryParent = marketplaceCategories.FirstOrDefault(x => x.Name?.ToLower() == row.MarketplaceCategoryParent.ToLower());
                    }

                    marketplaceCategory = new MarketplaceCategoryEntity { Name = row.MarketplaceCategory, ParentId = marketplaceCategoryParent.Id };
                    marketplaceCategory.MarketplaceId = _context.MarketplaceEntity.FirstOrDefault().Id;
                    _context.MarketPlaceCategories.Add(marketplaceCategory);
                    _context.SaveChanges();
                    marketplaceCategories.Add(marketplaceCategory);
                }

                companyCategory.CategoryId = marketplaceCategory.Id;
            }
            _context.SaveChanges();
            return true;
        }

        public void UpdateCompany(UpdateCompanyData data)
        {
            var exists = _context.Companies.Any(x => x.Id == data.Id);
            if (!exists)
                throw new ServiceException("company_not_found", "company_not_found");

            var existingProducts = ExtractCompanyProducts(data.Id);

            var existingCategories = _context.CompanyCategories
                .Where(x => x.CompanyId == data.Id)
                .ToList();

            UpdateCompanyCategories(existingCategories, data.Categories);

            DeleteOldCompanyProducts(existingProducts, data.Products);
            UpdateCompanyProducts(existingProducts, data.Products);

            _context.SaveChanges();
        }

        private List<ProductEntity> ExtractCompanyProducts(int companyId)
        {
            var count = _context.Products
                .Where(x => x.CompanyId == companyId)
                .Count();

            var step = 100;
            var list = new List<ProductEntity>(count);

            var errors = 0;
            for (var i = 0; i < count / step + 1; i++)
            {
                try
                {
                    var products = _context.Products
                        .Where(x => x.CompanyId == companyId)
                        .Include(x => x.Pictures)
                        .Include(x => x.ProductColors)
                        .Include(x => x.Sale)
                        .Skip(i * step)
                        .Take(step)
                        .ToArray();

                    list.AddRange(products);
                }
                catch
                {
                    errors++;
                    if (errors > 50)
                        throw;
                    i--;
                }
            }

            return list;
        }

        private void UpdateCompanyProducts(List<ProductEntity> existingProducts, List<Product> updatedProducts)
        {
            const int step = 100;

            var updatedColors = updatedProducts
                .Where(x => x.Color != null && x.Color.Any())
                .SelectMany(x => x.Color)
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => x.Trim().ToLower())
                .Distinct()
                .ToArray();

            var existingColors = _context.CompanyColors
                .ToArray()
                .ToDictionary(x => x.Name, x => x);

            foreach (var u in updatedColors)
            {
                if (existingColors.ContainsKey(u))
                    continue;

                var newColor = new CompanyColorEntity { Name = u };
                existingColors.Add(u, newColor);

                _context.CompanyColors.Add(newColor);
            }

            _context.SaveChanges();

            var errors = 0;

            for (var i = 0; i < updatedProducts.Count / step + 1; i++)
            {
                try
                {
                    var part = updatedProducts
                        .Skip(i * step)
                        .Take(step)
                        .ToList();

                    part.ForEach(update =>
                    {
                        var entity = existingProducts
                            .FirstOrDefault(x =>
                                x.CompanyId == update.CompanyId &&
                                x.CompanyProductId == update.CompanyProductId);

                        if (entity == null)
                        {
                            entity = new ProductEntity
                            {
                                CompanyId = update.CompanyId,
                                CompanyProductId = update.CompanyProductId,
                                CompanyCategoryId = update.CompanyCategoryId,
                                Pictures = new List<ProductPictureEntity>(),
                                ProductColors = new List<ProductColorEntity>()
                            };

                            _context.Products.Add(entity);
                        }

                        entity.IsRemoved = false;
                        entity.Processed = null;

                        if (CanUpdate(entity, ProductModerationStatus.Description))
                        {
                            entity.Name = update.Name;
                            entity.Description = update.Description;
                        }

                        if (entity.Sale != null && entity.Sale.Percents > 0)
                        {
                            entity.ActualPrice = update.Price;
                            entity.PreviousPrice = update.PreviousPrice;
                            entity.Price = (int) (update.Price * (1.0 - entity.Sale.Percents / 100.0));
                        }
                        else
                        {
                            entity.Price = update.Price;
                            entity.PreviousPrice = update.PreviousPrice;
                            entity.ActualPrice = null;
                        }

                        entity.Url = update.Url;
                        entity.Available = update.Available;
                        entity.Pickup = update.Pickup;

                        entity.Delivery = update.Delivery;
                        entity.DeliveryCost = update.DeliveryCost;
                        entity.DeliveryDays = update.DeliveryDays;

                        entity.Vendor = update.Vendor;
                        entity.VendorCode = update.VendorCode;

                        entity.Warranty = update.Warranty;
                        entity.Origin = update.Origin;

                        if (CanUpdate(entity, ProductModerationStatus.Size))
                        {
                            entity.Size = update.Size;
                            entity.Width = update.Width;
                            entity.Height = update.Height;
                            entity.Length = update.Length;
                        }

                        entity.JSONNotes = JsonConvert.SerializeObject(update.Notes);

                        var updatedPictures = update.Pictures != null
                            ? update.Pictures
                                .Where(x => !string.IsNullOrEmpty(x))
                                .Select(x => x.ToPictureEntity(entity.Id))
                                .ToArray()
                            : new ProductPictureEntity[0];

                        if (CanUpdate(entity, ProductModerationStatus.Photo))
                        {
                            if (!updatedPictures.Any())
                                entity.Pictures = new List<ProductPictureEntity>();
                            else
                            {
                                foreach (var newPicture in updatedPictures)
                                {
                                    if (entity.Pictures.Any(x => string.Equals(x.Url, newPicture.Url)))
                                        continue;
                                    entity.Pictures.Add(newPicture);
                                }

                                for (var j = 0; j < entity.Pictures.Count; j++)
                                {
                                    var ex = entity.Pictures.ElementAt(j);
                                    if (updatedPictures.Any(x => string.Equals(x.Url, ex.Url)))
                                        continue;

                                    entity.Pictures.Remove(ex);
                                    j--;
                                }
                            }
                        }

                        //if (CanUpdate(entity, ProductModerationStatus.Color))
                        //{
                        //    var updatedProductColors = update.Color != null
                        //        ? update.Color
                        //            .Where(x => !string.IsNullOrEmpty(x))
                        //            .Select(x => x.Trim())
                        //            .Select(x => x.ToLower())
                        //            .ToArray()
                        //        : new string[0];

                        //    if (!updatedProductColors.Any())
                        //        entity.ProductColors = new List<ProductColorEntity>();
                        //    else
                        //    {
                        //        foreach (var c in updatedProductColors)
                        //        {
                        //            var ex = existingColors[c];
                        //            if (entity.ProductColors.All(x => x.CompanyColorId != ex.Id))
                        //                entity.ProductColors.Add(new ProductColorEntity {CompanyColorId = ex.Id});
                        //        }

                        //        for (var j = 0; j < entity.ProductColors.Count; j++)
                        //        {
                        //            var existing = entity.ProductColors.ElementAt(j);
                        //            var ex = existingColors.Values.FirstOrDefault(x => x.Id == existing.CompanyColorId);

                        //            if ((ex != null && updatedProductColors.Any(x => string.Equals(x, ex.Name)) ||
                        //                 existing.IsModerated == true))
                        //                continue;

                        //            entity.ProductColors.Remove(existing);
                        //            j--;
                        //        }
                        //    }
                        //}

                    });

                    _context.SaveChanges();
                }
                catch
                {
                    errors++;
                    if (errors > 100)
                        throw;
                    i--;
                }
            }
        }

        private static bool CanUpdate(ProductEntity entity, ProductModerationStatus target)
        {
            if (entity.ModerationStatus == null)
                return true;

            return !entity.ModerationStatus.Value.HasFlag(target);
        }

        private static void DeleteOldCompanyProducts(List<ProductEntity> existingProducts, List<Product> updatedProducts)
        {
            existingProducts.ForEach(existing =>
            {
                if (updatedProducts.All(x => x.CompanyProductId != existing.CompanyProductId))
                    existing.IsRemoved = true;
            });
        }

        private void UpdateCompanyCategories(List<CompanyCategoryEntity> existingCategories, List<CompanyCategory> updatedCategories)
        {
            foreach (var updated in updatedCategories)
            {
                var existing = existingCategories.FirstOrDefault(x => x.CompanyCategoryId == updated.CompanyCategoryId);
                if (existing == null)
                {
                    existing = new CompanyCategoryEntity
                    {
                        CompanyId = updated.CompanyId,
                        CompanyCategoryId = updated.CompanyCategoryId
                    };

                    _context.CompanyCategories.Add(existing);

                    existingCategories.Add(existing);
                }

                existing.Name = updated.CompanyCategoryName;
                existing.ParentCompanyCategoryId = updated.ParentCompanyCategoryId;
            }

            foreach (var category in existingCategories)
            {
                if (!category.ParentCompanyCategoryId.HasValue)
                    continue;

                if (category.ParentCompanyCategoryId == category.CompanyCategoryId)
                    category.ParentCompanyCategoryId = null;

                if (existingCategories.All(x => x.CompanyCategoryId != category.ParentCompanyCategoryId))
                    category.ParentCompanyCategoryId = null;
            }

            _context.SaveChanges();
        }

        public Product GetProductById(int productId)
        {
            var result = _context.Products
                .Where(x => x.Id == productId)
                .SelectProduct()
                .FirstOrDefault();

            return result;
        }

        public List<GetCategoryProductsModel> GetCategoryProducts(int companyId, int companyCategoryId)
        {
            var result = _context.CompanyCategories
                .Where(x => x.CompanyCategoryId == companyCategoryId)
                .Where(x => x.CompanyId == companyId)
                .Select(x => x.Products.Select(y => new GetCategoryProductsModel()
                {
                    Id = y.Id,
                    Seller=y.CompanyCategory.Company.Name,
                    Vendor = y.Vendor,
                    VendorCode = y.VendorCode,
                    Url = y.Url,
                    Name = y.Name,
                    Pictures = new[] {y.Pictures.FirstOrDefault() != null ? y.Pictures.FirstOrDefault().Url : ""},
                    CompanyCategoryId = y.CompanyCategoryId,
                    CompanyId = y.CompanyId,
                    CategoryName = y.CompanyCategory.Category.Name,
                    CompanyCategoryName = y.CompanyCategory.Name,
                    Height = y.Height,
                    Width = y.Width,
                    Length = y.Length
                }).ToList()).FirstOrDefault();

            return result;
        }

        public ProductInfo GetShortProductInfoById(int productId)
        {
            return _context.Products
                .Where(x => x.Id == productId)
                .SelectProductInfo(ARPlatform.Browser, ARFormat.None)
                .FirstOrDefault();
        }

        public int AddCategoryImage(string url)
        {
            var image = new CategoryImageEntity { Url = url };

            _context.CategoryImages.Add(image);
            _context.SaveChanges();

            return image.Id;
        }

        public Dictionary<int, string> GetCategoryImages()
        {
            return _context.CategoryImages.ToDictionary(x => x.Id, x => x.Url);
        }

        public int AddRoomName(string roomName)
        {
            if (_context.RoomNames.Any(x => x.Name == roomName))
                throw new ServiceException("room already exist", "room_already_exist");

            var entity = new RoomNameEntity
            {
                Name = roomName
            };
            _context.RoomNames.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public bool EditRoomName(RoomName room)
        {
            var entity = _context.RoomNames.Find(room.Id);
            if (entity == null)
                throw new ServiceException("room not exist", "room_not_exist");
            entity.Name = room.Name;
            _context.SaveChanges();
            return true;

        }

        public RoomName[] GetRoomNames()
        {
            var rooms = _context.RoomNames.ToList();
            return rooms.Select(x => new RoomName { Id = x.Id, Name = x.Name }).ToArray();
        }

        public bool AddCompanyCategoryRoom(CompanyCategoryRoom room)
        {
            var entity = _context.CompanyCategoryRooms.FirstOrDefault(x => x.CompanyCategoryId == room.CompanyCategoryId && x.CompanyId == room.CompanyId && x.RoomNameId == room.RoomNameId);
            if (entity != null)
                throw new ServiceException("room already exist", "room_already_exist");
            entity = new CompanyCategoryRoomEntity
            {
                CompanyId = room.CompanyId,
                CompanyCategoryId = room.CompanyCategoryId,
                RoomNameId = room.RoomNameId,
            };
            _context.CompanyCategoryRooms.Add(entity);
            _context.SaveChanges();
            return true;
        }


        public bool DeleteCompanyCategoryRoom(CompanyCategoryRoom room)
        {
            var entity = _context.CompanyCategoryRooms.FirstOrDefault(x => x.CompanyCategoryId == room.CompanyCategoryId && x.CompanyId == room.CompanyId && x.RoomNameId == room.RoomNameId);
            if (entity == null)
                throw new ServiceException("room not found", "room_not_found");
            _context.CompanyCategoryRooms.Remove(entity);
            _context.SaveChanges();
            return true;
        }

        public RoomName[] GetCategoryRoomsByCompanyCategory(int companyId, int companyCategoryId)
        {
            var rooms = _context.CompanyCategoryRooms.Include("RoomName").Where(x => x.CompanyId == companyId && x.CompanyCategoryId == companyCategoryId).ToList();
            return rooms.Select(x => new RoomName { Id = x.RoomName.Id, Name = x.RoomName.Name }).ToArray();
        }

        public int AddMarketplaceColor(MarketplaceColor model)
        {
            if (_context.MarketplaceColors.Any(x => x.Name == model.Name))
                throw new ServiceException("color already exist", "color_already_exist");

            var entity = new MarketplaceColorEntity
            {
                Name = model.Name,
                Hex = model.Hex
            };

            _context.MarketplaceColors.Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }

        public bool EditMarketplaceColor(MarketplaceColor marketplaceColor)
        {
            var entity = _context.MarketplaceColors.Find(marketplaceColor.ColorId);
            if (entity == null)
                throw new ServiceException("room not exist", "room_not_exist");
            entity.Name = marketplaceColor.Name;
            entity.Hex = marketplaceColor.Hex;
            _context.SaveChanges();
            return true;
        }

        public MarketplaceColor[] GetMarketplaceColors()
        {
            var colors = _context
                .MarketplaceColors
                .ToList();

            return colors.Select(x => new MarketplaceColor
            {
                ColorId = x.Id,
                Name = x.Name,
                Hex = x.Hex
            })
            .ToArray();
        }

        public List<CategoryInfo> GetCategoryPath(int categoryId)
        {
            var category = _context.MarketPlaceCategories
                .Where(x => x.Id == categoryId)
                .Include(x => x.Parent)
                .FirstOrDefault();

            if (category == null)
                throw new ServiceException($"category with id {categoryId} was not found", "category_not_found");

            var result = new List<CategoryInfo> { new CategoryInfo { Id = category.Id, Name = category.Name } };

            while (category.ParentId.HasValue)
            {
                category = category.Parent;
                result.Insert(0, new CategoryInfo { Id = category.Id, Name = category.Name });
            }

            return result;
        }

        public void UpdateCompanyColors(IEnumerable<string> colors)
        {
            var dbColors = _context.CompanyColors.Select(x => x.Name).ToArray();
            var newColors = colors
                .Where(x => !dbColors.Contains(x))
                .Select(x => new CompanyColorEntity { Name = x, MarketplaceColorId = null })
                .ToList();

            if (!newColors.Any())
                return;

            _context.CompanyColors.AddRange(newColors);
            _context.SaveChanges();
        }

        public CompanyColor[] GetCompanyColors()
        {
            return _context.CompanyColors
                .Select(x => new CompanyColor
                {
                    Id = x.Id,
                    Name = x.Name,
                    MarketplaceColorId = x.MarketplaceColorId,
                    MarketplaceColorName = x.MarketplaceColor != null ? x.MarketplaceColor.Name : string.Empty

                })
                .ToArray();
        }        

        public bool MapCompanyColor(int companyColorId, int marketplaceColorId)
        {
            var color = _context.CompanyColors.Find(companyColorId);
            if(color==null)
                throw new ServiceException($"company color with id {companyColorId} was not found", "company_color_not_found");
            color.MarketplaceColorId = marketplaceColorId;
            _context.SaveChanges();
            return true;
        }

        public bool AddAllCompanyCategoryRooms(int companyId, int companyCategoryId)
        {
            var roomsIds = _context.RoomNames.Select(x => x.Id).ToArray();
            if(roomsIds==null||!roomsIds.Any())
                throw new ServiceException("rooms not found", "rooms_not_found");
            var rooms = roomsIds.Select(x => new CompanyCategoryRoomEntity
            {
                CompanyId = companyId,
                CompanyCategoryId = companyCategoryId,
                RoomNameId = x
            });
            _context.CompanyCategoryRooms.AddRange(rooms);
            _context.SaveChanges();
            return true;
        }

        public bool DeleteAllCompanyCategoryRooms(int companyId, int companyCategoryId)
        {
            var rooms = _context.CompanyCategoryRooms.Where(x => x.CompanyId == companyId && x.CompanyCategoryId == companyCategoryId).ToArray();

            if (!rooms.Any())
                return true;

            _context.CompanyCategoryRooms.RemoveRange(rooms);
            _context.SaveChanges();

            return true;
        }

        public int AddYMLUpdateTask(int companyId, string ymlLink, UploadFiletype type, int priority, YMLDownloadSource downloadSource)
        {
            var timeNow = DateTime.UtcNow;
            var taskEntity = new YMLUpdateTaskEntity { Priority = priority, YMLLink = ymlLink, Filetype = type, CreateTime = timeNow, StatusChangeTime = timeNow, CompanyId = companyId, Status = YMLUpdateTaskStatus.Pending, DownloadSource = downloadSource };
            _context.YmlUpdates.Add(taskEntity);
            _context.SaveChanges();
            return taskEntity.Id;
        }


        public YMLUpdateTask GetNextYmlUpdateTask()
        {
            var entity = _context.YmlUpdates
                .Where(x => x.Status == YMLUpdateTaskStatus.Pending)
                .OrderByDescending(x => x.Priority)
                .FirstOrDefault();

            if (entity != null)
            {
                return new YMLUpdateTask
                {
                    Id = entity.Id,
                    CompanyId = entity.CompanyId,
                    CreateTime = entity.CreateTime,
                    YMLLink = entity.YMLLink,
                    Priority = entity.Priority,
                    Status = entity.Status,
                    FailMessage = entity.FailMessage,
                    FileType = entity.Filetype,
                    DownloadSource = entity.DownloadSource
                };
            }

            return null;
        }

        public void DeleteYMLUpdateTask(int taskId)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return;

            _context.YmlUpdates.Remove(taskEntity);

            _context.SaveChanges();
        }

        public YMLUpdateTask[] GetAllPendingYMLUpdateTasks()
        {
            return _context.YmlUpdates.OrderByDescending(x => x.Priority).Select(x => new YMLUpdateTask
            {
                Id = x.Id,
                CompanyId = x.CompanyId,
                CreateTime = x.CreateTime,
                YMLLink = x.YMLLink,
                Priority = x.Priority,
                Status = x.Status,
                FailMessage = x.FailMessage,
                 FileType = x.Filetype,
                  DownloadSource = x.DownloadSource
            }).ToArray();
        }

        public void MarkYMLUpdateTaskAsFailed(int taskId, string errorMessage)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return;

            taskEntity.Status = YMLUpdateTaskStatus.Failed;
            taskEntity.StatusChangeTime = DateTime.UtcNow;
            taskEntity.FailMessage = errorMessage;

            _context.SaveChanges();
        }

        public void MarkYMLUpdateTaskAsProcessing(int taskId)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return;

            taskEntity.Status = YMLUpdateTaskStatus.InProcess;
            taskEntity.StatusChangeTime = DateTime.UtcNow;
            taskEntity.FailMessage = string.Empty;

            _context.SaveChanges();
        }

        public List<CategoryInfo> GetProductCategoryPath(int productId)
        {
            var category = _context.Products
                .Where(x => x.Id == productId)
                .Where(x => x.CompanyCategory.CategoryId != null)
                .Select(x => x.CompanyCategory)
                .Select(x => x.Category)
                .Include(x => x.Parent)
                .FirstOrDefault();

            var result = new List<CategoryInfo>();

            while (category != null)
            {
                result.Insert(0, new CategoryInfo { Id = category.Id, Name = category.Name });
                category = category.Parent;
            }

            return result;
        }

        public YMLUpdateTask GetYmlUpdateTask(int taskId)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return null;

            return new YMLUpdateTask
            {
                Id = taskEntity.Id,
                CompanyId = taskEntity.CompanyId,
                CreateTime = taskEntity.CreateTime,
                YMLLink = taskEntity.YMLLink,
                Priority = taskEntity.Priority,
                Status = taskEntity.Status,
                FailMessage = taskEntity.FailMessage,
                FileType = taskEntity.Filetype,
                DownloadSource = taskEntity.DownloadSource
            };
        }

        public void MarkYMLUpdateTaskAsCompleted(int taskId)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return;

            taskEntity.Status = YMLUpdateTaskStatus.Succeeded;
            taskEntity.StatusChangeTime = DateTime.UtcNow;

            _context.SaveChanges();
        }

        public void SetCompanyAutofeedStatus(int companyId, bool isEnabled)
        {
            var company = _context.Companies.FirstOrDefault(x => x.Id == companyId);
            if (company == null)
                throw new ServiceException("company_not_found", "company_not_found");

            company.EnableAutofeed = isEnabled;
            _context.SaveChanges();
        }

        public void CreateAutofeedTasks(int withpriority, TimeSpan minTimeAfterLastUpdate)
        {
            var timeNow = DateTime.UtcNow;
            var minHours = minTimeAfterLastUpdate.TotalHours;
            foreach (var feed in _context.Companies.Select(x => new { x.Id, x.FeedUrl, x.AutofeedComplitionTime, x.EnableAutofeed }).Where(x => x.EnableAutofeed && !string.IsNullOrEmpty(x.FeedUrl) && EF.Functions.DateDiffHour(x.AutofeedComplitionTime, timeNow) >= minHours ))
            {
                var uploadTask = _context.YmlUpdates.FirstOrDefault(x => x.CompanyId == feed.Id && x.Status == YMLUpdateTaskStatus.Pending && x.DownloadSource == YMLDownloadSource.AutoFeed);
                if (uploadTask == null)
                {
                    _context.YmlUpdates.Add(new YMLUpdateTaskEntity
                    {
                        CompanyId = feed.Id,
                        CreateTime = timeNow,
                        Filetype = UploadFiletype.YML,
                        DownloadSource = YMLDownloadSource.AutoFeed,
                        Priority = withpriority,
                        Status = YMLUpdateTaskStatus.Pending,
                        StatusChangeTime = timeNow,
                        YMLLink = feed.FeedUrl
                    });
                    var company = _context.Companies.Find(feed.Id);
                    if (company != null)
                    { 
                        company.AutofeedComplitionTime = timeNow;
                    }
                }
                else
                    uploadTask.YMLLink = feed.FeedUrl;
            }
            _context.SaveChanges();
        }

        public void RefreshProcessingUploads()
        {
            var uploads = _context.YmlUpdates.Where(x => x.Status == YMLUpdateTaskStatus.InProcess);
            var timeNow = DateTime.UtcNow;
            foreach (var u in uploads)
            {
                u.Status = YMLUpdateTaskStatus.Pending;
                u.StatusChangeTime = timeNow;
            }
            _context.SaveChanges();
        }

        public CompanyLastFeedInfo GetCompanyLastFeedInfo(int companyId)
        {
            var feed = _context.YmlUpdates.Where(x => x.CompanyId == companyId).OrderByDescending(x => x.StatusChangeTime).FirstOrDefault();
            if (feed == null)
                return null;
            else
                return new CompanyLastFeedInfo { Source = feed.DownloadSource, LastFeedStatus = feed.Status, LastFeedStatusChangeTime = feed.StatusChangeTime };
        }

        public List<Product> GetProductsByVendorCode(string vendorCode)
        {
            var code = vendorCode.Trim().ToLower();
            return _context.Products.Where(x => x.VendorCode.Trim().ToLower() == code).SelectProduct().ToList();
        }


        public void MarkYMLUpdateTaskAsPending(int taskId)
        {
            var taskEntity = _context.YmlUpdates.Find(taskId);
            if (taskEntity == null)
                return;

            taskEntity.Status = YMLUpdateTaskStatus.Pending;
            taskEntity.StatusChangeTime = DateTime.UtcNow;
            taskEntity.FailMessage = string.Empty;

            _context.SaveChanges();
        }
    }
}