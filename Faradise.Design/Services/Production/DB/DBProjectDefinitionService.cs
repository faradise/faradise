﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDefinition;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Models.ProjectDevelopment;
using Faradise.Design.Services.DataMappers;

namespace Faradise.Design.Services.Production.DB
{
    public class DBProjectDefinitionService : IDBProjectDefinitionService
    {
        private readonly FaradiseDBContext _context;

        public DBProjectDefinitionService(FaradiseDBContext db)
        {
            _context = db;
        }

        public bool AssignDesigner(int projectId, int userId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if(project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            if (project.DesignerId == userId)
                return false;
            project.DesignerId = userId;
            var designerInPool = _context.DesignerPools.FirstOrDefault(x => x.ProjectId == projectId && x.DesignerId == userId);
            if(designerInPool==null)
                throw new ServiceException("designer_not_found_in_pool", "designer_not_found_in_pool");
            designerInPool.Status = DesignerStatusInPool.Approved;
            _context.SaveChanges();
            return true;
        }

        public bool CheckUserOwnsService(int projectId, int userId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            if (project.ClientId == userId || project.DesignerId == userId)
                return true;
            return false;
        }

        public void CloseProject(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.Status = ProjectStatus.Closed;
            _context.SaveChanges();
        }

        public void ConfirmDesigner(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.DesignerConfirmed = true;
            _context.SaveChanges();
        }

        public int CreateProjectDefinition(int clientId)
        {
            var project = new ProjectDefinitionEntity()
            {
                ClientId = clientId,
                Stage = ProjectStage.Description,
                Status=ProjectStatus.InWork,
                CreationDate=DateTime.UtcNow
            };
            _context.ProjectDefinitions.Add(project);
            _context.SaveChanges();
            _context.ChatGroups.Add(new MessageGroupEntity { GroupId = project.Id });
            _context.SaveChanges();
            return project.Id;
        }

        public void DisbandDesigner(int projectId, string reason)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            var designerInPool = _context.DesignerPools.FirstOrDefault(x => x.ProjectId == projectId && x.DesignerId ==project.DesignerId );
            if (designerInPool == null)
                throw new ServiceException("designer_not_found_in_pool", "designer_not_found_in_pool");
            designerInPool.Status = DesignerStatusInPool.Rejected;
            designerInPool.RejectReason = reason;
            project.DesignerConfirmed = false;
            project.DesignerId = null;
            _context.SaveChanges();
        }

        public void FinishFill(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.IsProjectFill = true;
            project.ProjectFillDate = DateTime.UtcNow;
            _context.SaveChanges();
        }

        public int GetDesigner(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            return project.DesignerId.HasValue?project.DesignerId.Value:0;
        }

        public FullProject GetFullProject(int projectId)
        {
            var projectDefinition = _context.ProjectDefinitions.Find(projectId);
            if (projectDefinition == null||projectDefinition.Archived)
                throw new ServiceException("project_not_found", "project_not_found");
            var project = new FullProject();
            project.AddInfo(projectDefinition);
            return project;
        }

        public AdminProjectInfo[] GetProjectsInfo()
        {
            var result = 
                from definition in _context.ProjectDefinitions
                where !definition.Archived
                join description in _context.ProjectFlowDescriptions 
                on definition.Id equals description.ProjectId into desc
                from description in desc.DefaultIfEmpty()
                join development in _context.ProjectDevelopments 
                on definition.Id equals development.Id into dev
                from development in dev.DefaultIfEmpty()
                select new AdminProjectInfo
                {
                    Id = definition.Id,
                    Name = definition.Name,
                    
                    ClientName = definition.User.Name,
                    ClientEmail = definition.User.Email,
                    ClientPhone = definition.User.Phone,
                    CreationDate=definition.CreationDate,

                    Filled = definition.IsProjectFill,

                    AppointedDesigners = definition.DesignerPool.Count(d => d.Status == DesignerStatusInPool.Appointed),
                    RejectedDesigners = definition.DesignerPool.Count(d => d.Status == DesignerStatusInPool.Rejected),

                    DesignerId = definition.DesignerId,
                    ClientId = definition.ClientId,

                    NeedAdmin = definition.NeedAdmin,
                    HasChangesAfterLastAdminCheck = definition.HasChangesAfterLastAdminCheck,

                    Status = definition.Status,
                    Stage = definition.Stage,

                    CooperateStage = description == null ? CooperateStage.Meeting : description.Stage,
                    DevelopmentStage = development == null ? DevelopmentStage.Moodboard : development.CurrentStage,
                    OrderCount = _context.ProjectMarketplaceOrders.Count(x => x.ProjectId == definition.Id)
                };

            return result.ToArray();
        }

        public ProjectDefinition GetProjectDefinition(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null||project.Archived)
                throw new ServiceException("project_not_found", "project_not_found");
            return project.ToService();
        }

        public ProjectDefinition[] GetProjectsByUser(int userId)
        {
            var user = _context.Users.Find(userId);
            if (user == null||user.Archived)
                throw new ServiceException("project_not_found", "project_not_found");
            var projects = _context.ProjectDefinitions.Where(x => (x.ClientId == userId || x.DesignerId == userId)&&!x.Archived).ToList();
            return projects != null ? projects.Select(x => x.ToService()).ToArray() : null;
        }

        public void SetAbandonStage(int projectId, ProjectStage abandonStage)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.AbandonStage = abandonStage;
            _context.SaveChanges();
        }

        public void SetProjectCity(int projectId, string cityName)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.City = cityName;
            _context.SaveChanges();
        }

        public void SetProjectName(int projectId, string name)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.Name = name;
            _context.SaveChanges();
        }

        public void SetStage(int projectId, ProjectStage stage)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.Stage = stage;
            _context.SaveChanges();
        }

        public void SetProjectNeedAdminAttention(int projectId, bool needAdmin)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.NeedAdmin = needAdmin;
            project.HasChangesAfterLastAdminCheck = needAdmin;
            _context.SaveChanges();
        }

        public void MarkProjectAsChangedForAdmin(int projectId, bool isChanged)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.HasChangesAfterLastAdminCheck = isChanged;
            _context.SaveChanges();
        }

        public void DeleteUser(int userId)
        {
            var user = _context.Users.Find(userId);
            if (user == null)
                throw new ServiceException($"User with id {userId} was not found", "user_not_found");

            _context.Users.Remove(user);

            _context.SaveChanges();
        }

        public void ArchiveProject(int projectId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");
            project.Archived = true;
            _context.SaveChanges();
        }

        
    }
}
