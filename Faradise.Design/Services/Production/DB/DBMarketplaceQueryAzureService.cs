﻿using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.AzureSearch;
using Faradise.Design.Services.DataMappers;
using System;
using System.Collections.Generic;

namespace Faradise.Design.Services.Production.DB
{
    public class DBMarketplaceQueryAzureService: IDBMarketplaceQueryServiceProvider
    {
        private readonly DBMarketplaceQueryService _queryService;
        private readonly ISearchService _azureSearchService;
        private readonly IPersistentStorageService _persistentStorageService;

        public DBMarketplaceQueryAzureService(DBMarketplaceQueryService queryService, ISearchService azureSearchService, IPersistentStorageService persistentStorageService)
        {
            _azureSearchService = azureSearchService;
            _queryService = queryService;
            _persistentStorageService = persistentStorageService;
        }

        public ProductsQueryFilters GetQueryFilters(int categoryId)
        {
            try
            {
                var azureAvailable = _persistentStorageService.AzureSearchAvailable ?? false;

                if (!azureAvailable)
                    throw new Exception("IndexerService not IsAvailadble");

                return _azureSearchService.GetQueryFilters(categoryId, "products");
            }
            catch
            {
                return _queryService.GetQueryFilters(categoryId);
            }
        }

        public Product GetProductById(int productId)
        {
            return _queryService.GetProductById(productId);
        }

        public ProductInfo GetDailyProduct()
        {
            return _queryService.GetDailyProduct();
        }

        public ProductsQuery GetProductsByFilters(ProductsQueryFiltersValues filters, ARPlatform platform, ARFormat format)
        {
            try
            {
                var azureAvailable = _persistentStorageService.AzureSearchAvailable ?? false;

                if (!azureAvailable)
                    throw new Exception("IndexerService not IsAvailable");

                var isId = int.TryParse(filters.SearchWords, out int id);
                if (isId)
                    return _azureSearchService.GetById(filters.SearchWords).ToService();
                
                return _azureSearchService.GetProductsByFilter(filters, platform, format, "products").ToService();
            }

            catch
            {
                return _queryService.GetProductsByFilters(filters, platform, format);
            }
        }

        public Category[] GetCategories()
        {
            return _queryService.GetCategories();
        }

        public Category[] GetSubcategories(int id)
        {
            return _queryService.GetSubcategories(id);
        }

        public CategoryInfo[] GetCategoryPath(int categoryId)
        {
            return _queryService.GetCategoryPath(categoryId);
        }

        public CategoryInfo[] GetProductCategoryPath(int productId)
        {
            return _queryService.GetProductCategoryPath(productId);
        }

        public CategoryHierarchy[] GetCategoryHierarchy()
        {
            return _queryService.GetCategoryHierarchy();
        }

        public int GetAvailableProductsCount()
        {
            return _queryService.GetAvailableProductsCount();
        }

        public ARProductDefinition GetARDefinition(int productId, ARPlatform platform, ARFormat format)
        {
            return _queryService.GetARDefinition(productId, platform, format);
        }

        public int GetTotalProductsCount()
        {
            return _queryService.GetTotalProductsCount();
        }

        public Category[] GetCategoriesByIds(IEnumerable<int> categoriesIds)
        {
            return _queryService.GetCategoriesByIds(categoriesIds);
        }
    }
}
