﻿using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.ProjectDescription;
using Faradise.Design.Services.DataMappers;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBClientProjectService : IDBClientProjectService
    {
        private readonly FaradiseDBContext _context;

        public DBClientProjectService(FaradiseDBContext db)
        {
            _context = db;
            SetDefaultTest();
        }

        private void SetDefaultTest()
        {
            var testQuestions = _context.TestQuestionDescriptions.Where(x => x.For == TestType.Client).ToList();
            if (!testQuestions.Any())
            {
                var questions = new TestQuestionDescription[]
                {
                    new TestQuestionDescription(0, "Вы не видите свой интерьер без:")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(0, "Вау-эффекта, неожиданных цветовых сочетаний, комбинации несовместимых предметов и фактур;"),
                            new TestAnswerDescription(1, "Лаконичных форм, чистых линий и сдержанных цветов"),
                            new TestAnswerDescription(2, "Натуральных материалов близких к природным фактурам и естественные оттенков.")
                        }
                    },
                    new TestQuestionDescription(1, "Какие поверхности кажутся вам более приятными на ощупь?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(3, "Все гладкое и блестящее"),
                            new TestAnswerDescription(4, "Все натуральное — дерево без лака, керамика без глазури"),
                            new TestAnswerDescription(5, "Оригинальное сочетания материалов: бетон и кожа, фарфор и метал")
                        }
                    },
                    new TestQuestionDescription(2, "Какие цвета вы предпочитаете?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(6, "Нейтральную цветовую гамму, где в основе такие цвета, как белый, бежевый, серый, черный"),
                            new TestAnswerDescription(7, "Буйство красок, необычные цветовые сочетания"),
                            new TestAnswerDescription(8, "Теплые природные тона и нежные пастельные оттенки")
                        }
                    },
                    new TestQuestionDescription(3, "Что бы вы никогда не сделали у себя дома?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(9, "Золотой унитаз"),
                            new TestAnswerDescription(10, "Глянцевые наливные полы"),
                            new TestAnswerDescription(11, "Зеркало в деревянной раме с витиеватым узоро")
                        }
                    },
                    new TestQuestionDescription(4, "Что бы вы хотели получить в подарок на новоселье?")
                    {
                        Answers = new TestAnswerDescription[]
                        {
                            new TestAnswerDescription(12, "Простое, но стильное, будь то лаконичная белая ваза или современная картина"),
                            new TestAnswerDescription(13, "Подхваты для штор или латунный антикварный поднос"),
                            new TestAnswerDescription(14, "Нечто необычное, цепляющее взгляд - диковинный предмет или яркий постер в стиле поп-арт")
                        }
                    }
                };

                foreach (var question in questions)
                {
                    var qEntity = new TestQuestionDescriptionEntity
                    {
                        Text = question.Text,
                        For = TestType.Client,
                    };
                    _context.TestQuestionDescriptions.Add(qEntity);
                    _context.SaveChanges();
                    foreach (var answer in question.Answers)
                    {
                        var aEntity = new TestAnswerDescriptionEntity
                        {
                            QuestionId = qEntity.Id,
                            For = TestType.Client,
                            Text = answer.Text
                        };
                        _context.TestAnswerDescriptions.Add(aEntity);
                    }
                    _context.SaveChanges();
                }
            };
        }

        public ProjectDescription GetProjectDescription(int projectId)
        {
            var project = _context.ProjectDescriptions.Include("Definition").FirstOrDefault(x => x.Id == projectId);
            if (project == null||project.Definition.Archived)
                throw new ServiceException("project_not_found", "project_not_found");
            return project.ToService();
        }

        public void SaveProjectDescription(ProjectDescription project)
        {
            var oldProject = _context.ProjectDescriptions.FirstOrDefault(x => x.Id == project.ProjectId);
            if (oldProject == null)
                oldProject = new ProjectDescriptionEntity();
            oldProject.FillEntity(project);
            _context.SaveChanges();
        }

        public int CreateProjectDescription(int definitionId)
        {
            var project = new ProjectDescriptionEntity() { Id = definitionId };
            _context.ProjectDescriptions.Add(project);
            _context.SaveChanges();
            return project.Id;
        }


        public TestQuestionDescription[] GetDesignerQuestions()
        {
            var questions = _context.TestQuestionDescriptions.Where(x => x.For == TestType.Client)
                .Select(x => new TestQuestionDescription(x.Id, x.Text))
                .ToArray();

            foreach(var question in questions)
            {
                question.Answers = _context.TestAnswerDescriptions
                    .Where(x => x.QuestionId == question.Id)
                    .Select(x => new TestAnswerDescription(x.Id, x.Text))
                    .ToArray();
            }

            return questions;
        }
    }
}
