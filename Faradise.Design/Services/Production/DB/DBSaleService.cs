﻿using System;
using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Sale;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Production.DB
{
    public class DBSaleService : IDBSaleService
    {
        private readonly FaradiseDBContext _context = null;

        public DBSaleService(FaradiseDBContext context)
        {
            _context = context;
        }

        public Sale CreateSale(string name)
        {
            var entity = new SaleEntity { Name = name, Updated =  DateTime.UtcNow };
            _context.Sales.Add(entity);
            _context.SaveChanges();
            return new Sale { Id = entity.Id, Name = name, IsActive = entity.IsActive };
        }

        public void DeleteSale(int saleId)
        {
            var entity = _context.Sales.Find(saleId);
            if (entity == null)
                throw new ServiceException($"sale with id {saleId} not found", "sale_not_found");
            entity.IsActive = false;
            entity.Updated = DateTime.UtcNow;
            entity.UpdateJson = JsonConvert.SerializeObject(new SaleUpdateJson { RemoveAfterUpdate = true });
            entity.IsDeleting = true;
            _context.SaveChanges();
        }

        public Sale GetSale(int saleId)
        {
            return _context.Sales.Where(x => x.Id == saleId)
                .Select(x => new Sale
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    IsActive = x.IsActive,
                    Percent = x.Percents,
                    UpdateJson = x.UpdateJson
                }).FirstOrDefault();
        }

        public SaleShortInfo[] GetSales()
        {
            return _context.Sales.Where(x => !x.IsDeleting).Select(x =>
                new SaleShortInfo
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    Name = x.Name
                }
            ).ToArray();
        }

        public void SetSaleActive(int saleId, bool isActive)
        {
            var entity = _context.Sales.Find(saleId);
            if (entity == null)
                throw new ServiceException($"sale with id {saleId} not found", "sale_not_found");
            entity.Updated = DateTime.UtcNow;
            entity.IsActive = isActive;
            _context.SaveChanges();
        }

        public void UpdateSale(Sale updateInfo)
        {
            if (updateInfo == null)
                return;
            var entity = _context.Sales.Find(updateInfo.Id);
            if (entity == null)
                throw new ServiceException($"sale with id {updateInfo.Id} not found", "sale_not_found");
            entity.Name = updateInfo.Name;
            entity.Percents = updateInfo.Percent;
            entity.Updated = DateTime.UtcNow;
            entity.Description = updateInfo.Description;
            var updateJson = new SaleUpdateJson
            {
                AllMarkeplace = updateInfo.AllMarketplace,
                CategoryIds = updateInfo.Categories,
                CompaniesId = updateInfo.Companies,
                ProdyctIds = updateInfo.Products
            };
            entity.UpdateJson = JsonConvert.SerializeObject(updateJson);
            _context.SaveChanges();
        }
    }
}
