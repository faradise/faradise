﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.Payment;
using Faradise.Design.Models.ProjectDevelopment;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBasketService : IDBBasketService
    {
        private readonly FaradiseDBContext _context;

        public DBBasketService(FaradiseDBContext db)
        {
            _context = db;
        }

        public void AddProductToBasket(int userId, int productId, int count)
        {
            if(!_context.Users.Any(x=>x.Id==userId))
                throw new ServiceException($"User not found", "user_not_found");
            if (!_context.Products.Any(x => x.Id ==productId))
                throw new ServiceException($"Product not found", "product_not_found");

            var oldProduct = _context.BasketProducts.Where(x => x.UserId == userId && x.ProductId == productId).FirstOrDefault();
            if (oldProduct == null)
            {
                _context.Add(new BasketProductEntity
                {
                    ProductId = productId,
                    UserId = userId,
                    Count = count
                });
            }
            else
            {
                oldProduct.Count += count;
            }
            _context.SaveChanges();
        }

        public void AssignPaymentToOrder(int orderId, Payment payment)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if(order==null)
                throw new ServiceException($"Order not found", "order_not_found");
            if(!_context.Payments.Any(x=>x.Id==payment.Id))
                throw new ServiceException($"Payment not found", "payment_not_found");

            order.PaymentId = payment.Id;
            _context.SaveChanges();
        }

        public void ClearBasket(int userId)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var products = _context.BasketProducts.Where(x => x.UserId == userId);
            if (products == null || !products.Any())
                return;
            _context.BasketProducts.RemoveRange(products);
            _context.SaveChanges();
        }

        public void SetBasketBin(int userId, ProductCount[] products)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var oldProducts = _context.BasketProducts.Where(x => x.UserId == userId);
            if (products == null || !products.Any())
            {
                _context.BasketProducts.RemoveRange(oldProducts);
                _context.SaveChanges();
                return;
            }
            var utcNow = DateTime.UtcNow;
            foreach (var p in products)
            {
                var oldProduct = oldProducts.FirstOrDefault(x => x.ProductId == p.ProductId);
                if (oldProduct != null)
                    oldProduct.Count = p.Count;
                else
                    _context.BasketProducts.Add(new BasketProductEntity { ProductId = p.ProductId, Count = p.Count, UserId = userId, DateAdded = utcNow });
            }
            var productsToRemove = oldProducts.Where(x => products.All(p => p.ProductId != x.ProductId));
            _context.BasketProducts.RemoveRange(productsToRemove);
            _context.SaveChanges();
        }

        public void AddProductsListToBasket(int userId, ProductCount[] products)
        {
            if (products.Length < 1)
                return;
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var oldProducts = _context.BasketProducts.Where(x => x.UserId == userId);
            var utcNow = DateTime.UtcNow;
            bool contextChanged = false;
            foreach (var p in products)
            {
                if (p.Count < 1)
                    continue;
                if (!_context.Products.Any(x => x.Id == p.ProductId))
                    throw new ServiceException($"Product not found", "product_not_found");
                var oldProduct = oldProducts.FirstOrDefault(x => x.ProductId == p.ProductId);
                if (oldProduct != null)
                    oldProduct.Count += p.Count;
                else
                    _context.BasketProducts.Add(new BasketProductEntity { ProductId = p.ProductId, Count = p.Count, UserId = userId, DateAdded = utcNow });
                contextChanged = true;
            }
            if (contextChanged)
                _context.SaveChanges();
        }

        public int CreateOneClickOrder(int userId, int productId, int productCount, OneClickOrderDescription description, PaymentMethod method, int deliveryCost)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");

            var order = new MarketplaceOrderEntity
            {
                UserId = userId,
                PaymentId = null,
                CreationTime = DateTime.UtcNow,
                Name = description.Name,
                Phone = description.Phone,
                Email = description.Email,
                Status = OrderStatus.Created,
                IsActive = true,
                SelectedPaymentMethod = method,
                DeliveryPrice = deliveryCost,
                PromoCode = description.PromoCode
            };

            _context.MarketplaceOrders.Add(order);
            _context.SaveChanges();

            var product = _context.Products.Where(x => x.Id == productId).Select(x => new { ProductId = x.Id, CanUseCode = !x.CompanyCategory.Company.DontSupportPromocode }).FirstOrDefault();
            if (product == null)
                throw new ServiceException($"Product id {productId} not found", "product_not_found");

            var code = _context.PromoCodes.FirstOrDefault(x => x.Code == description.PromoCode);

            var orderProducts = new MarketplaceOrderProductEntity
            {
                OrderId = order.Id,
                ProductId = productId,
                Count = productCount,
                PromoCodeDiscount = code != null && product.CanUseCode ? code.Export.Percents : 0,
                SupportPromoCode = code != null && product.CanUseCode
            };
            _context.MarketplaceOrderProducts.AddRange(orderProducts);
            _context.SaveChanges();
            return order.Id;
        }

        public int CreateOrderFromBasket(int userId, OrderDescription description, PaymentMethod method, int deliveryCost)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var order = new MarketplaceOrderEntity
            {
                UserId = userId,
                PaymentId = null,
                CreationTime = DateTime.UtcNow,
                City = description.City,
                Name = description.Name,
                Surname = description.Surname,
                Phone = description.Phone,
                Email = description.Email,
                Address = description.Adress,
                Description = description.Description,
                Status = OrderStatus.Created,
                IsActive = true,
                SelectedPaymentMethod = method,
                DeliveryPrice = deliveryCost,
                PromoCode = description.PromoCode
            };

            var products = _context.BasketProducts.Where(x => x.UserId == userId).Select(x => new { Count = x.Count, ProductId = x.Product.Id, CanUseCode = !x.Product.CompanyCategory.Company.DontSupportPromocode}).ToList();
            if(products==null||!products.Any())
                throw new ServiceException($"No products in basket", "no_products_in_basket");
            _context.MarketplaceOrders.Add(order);
            _context.SaveChanges();

            var code = _context.PromoCodes.FirstOrDefault(x => x.Code == description.PromoCode);

            var orderProducts = products.Select(x => new MarketplaceOrderProductEntity
            {
                OrderId = order.Id,
                ProductId = x.ProductId,
                Count = x.Count,
                PromoCodeDiscount = code != null && x.CanUseCode ? code.Export.Percents : 0,
                SupportPromoCode = code != null && x.CanUseCode
            });
            _context.MarketplaceOrderProducts.AddRange(orderProducts);
            _context.SaveChanges();
            return order.Id;
        }

        public void ChangePromoCodeForOrder(string promocode, int orderId)
        {
            var orderEntity = _context.MarketplaceOrders.FirstOrDefault(x => x.Id == orderId);
            if (orderEntity == null)
                return;
            if (orderEntity.Status > OrderStatus.Created)
                throw new ServiceException($"promocode change only available in created status", "order_status_error");
            var code = !string.IsNullOrEmpty(promocode) ? _context.PromoCodes.FirstOrDefault(x => x.Code == promocode) : null;
            if (!string.IsNullOrEmpty(promocode) && code == null)
                throw new ServiceException($"promocode with code {promocode} not found", "promocode_not_found");
            orderEntity.PromoCode = promocode;
            var products = _context.MarketplaceOrderProducts.Include("Product").Where(x => x.OrderId == orderId).ToList();
            foreach (var p in products)
            {
                var supportCode = code != null && !p.Product.CompanyCategory.Company.DontSupportPromocode;
                p.PromoCodeDiscount = supportCode ? code.Export.Percents : 0;
                p.SupportPromoCode = supportCode;
            }
            _context.SaveChanges();
        }

        public void CloseOrder(int orderId)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.IsActive = false;
            order.Status = order.SelectedPaymentMethod == PaymentMethod.Tinkoff ? OrderStatus.CanceledCredit : OrderStatus.Canceled;
            _context.SaveChanges();
        }
        
        public BasketProduct[] GetAllProductsInBasket(int userId)
        {

            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var products = _context.BasketProducts.Where(x => x.UserId == userId).Include("Product").Select(x => new BasketProduct
            {
                Product = new ProductInfo
                {
                    Id = x.Product.Id,
                    PhotoUrl = x.Product.Pictures
                        .Where(y => y.Processed == null || y.InternalUrl != null)
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .Take(2)
                        .ToArray(),
                    Description = x.Product.Description,
                    Name = x.Product.Name,
                    Price = x.Product.Price,
                    PreviousPrice = x.Product.PreviousPrice ?? x.Product.ActualPrice,
                    Seller=x.Product.CompanyCategory.Company.Name,
                    Vendor = x.Product.Vendor,
                    VendorCode = x.Product.VendorCode
                },
                Count = x.Count,
            }).ToArray();
            return products;
        }

        public int[] GetProductsIdsInBasket(int userId)
        {
            return _context.BasketProducts
                .Where(x => x.UserId == userId)
                .Select(x => x.ProductId)
                .ToArray();
        }

        public int GetOrderIdByPayment(int paymentId)
        {
            var order = _context.MarketplaceOrders.Select(x => new { x.Id, x.PaymentId }).FirstOrDefault(x => x.PaymentId == paymentId);
            if (order == null)
                return 0;
            return order.Id;
        }

        public FullBasketOrderInformation GetOrderInfo(int orderId)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            var products = _context.MarketplaceOrderProducts.Include("Product").Where(x => x.OrderId == orderId).ToList();
            var info = new FullBasketOrderInformation
            {
                OrderId = order.Id,
                OrderDescription = new OrderDescription
                {
                    Adress = order.Address,
                    City = order.City,
                    Description = order.Description,
                    Email = order.Email,
                    Name = order.Name,
                    Surname = order.Surname,
                    Phone = order.Phone
                },
                AdditionalPrice = order.AdditionalPrice,
                BasePrice = order.BasePrice,
                CreateionTime = order.CreationTime,
                PromoCode = order.PromoCode,
                IsFinished = (order.Status == OrderStatus.Completed),
                PaymentId = order.PaymentId,
                PaymentLink = order.Payment != null ? order.Payment.PaymentRoute : string.Empty,
                PlatformLink = order.Payment != null ? order.Payment.PlatformRoute : string.Empty,
                PaymentStatus = order.Payment?.Status ?? PaymentStatus.Pending,
                IsReady = order.IsReady,
                Status = order.Status,
                SelectedPaymentMethod = order.SelectedPaymentMethod,
                DeliveryPrice = order.DeliveryPrice,
                Products = products.Select(x => new OrderedProduct
                {
                    Id = x.Product.Id,
                    Name = x.Product.Name,
                    Price = (int)((1 - x.PromoCodeDiscount / 100.0f) * x.Product.Price),
                    SupportPromoCode = x.SupportPromoCode,
                    BasePrice = x.Product.Price,
                    Count = x.Count,
                    VendorLink = x.Product.Url,
                    VendorCode = x.Product.VendorCode,
                    Vendor = x.Product.Vendor
                }).ToArray()
            };
            info.AbsoluteDiscount = UpdateAbsoluteDiscount(info);
            //if (!string.IsNullOrEmpty(order.PromoCode))
            //{
            //    var code = _context.PromoCodes.FirstOrDefault(x => x.Code == order.PromoCode);
            //    if (code != null && code.Export.Absolute > 0)
            //    {
            //        var promoProducts = info.Products.Where(x => x.SupportPromoCode).ToArray();
            //        var allPrice = promoProducts.Sum(x => (x.Price - 1) * x.Count);
            //        var maxDiscount = Math.Min(allPrice, code.Export.Absolute);
            //        for (int p = 0; p < info.Products.Length; p++)
            //        {
            //            if (!info.Products[p].SupportPromoCode)
            //                continue;
            //            var weight = (info.Products[p].Price - 1) * info.Products[p].Count / (float)allPrice;
            //            var absProductDiscount = (int)((weight * maxDiscount) / info.Products[p].Count);
            //            info.Products[p].Price -= absProductDiscount;
            //        }
            //    }
            //}
            return info;
        }

        private int UpdateAbsoluteDiscount(FullBasketOrderInformation order)
        {
            if (!string.IsNullOrEmpty(order.PromoCode))
            {
                var code = _context.PromoCodes.FirstOrDefault(x => x.Code == order.PromoCode);
                if (code != null && code.Export.Absolute > 0)
                {
                    var possibleDiscountSum = order.Products.Where(x => x.SupportPromoCode).Sum(x => (x.Price - 1) * x.Count);
                    return Math.Min(code.Export.Absolute, possibleDiscountSum);
                }
            }
            return 0;
        }

        public ShortOrderInformation[] GetOrders(int from, int count)
        {
            return _context.MarketplaceOrders.OrderBy(x => x.Status).Include("Payment").Skip(from).Take(count).Select(x => new ShortOrderInformation
            {
                OrderId = x.Id,
                AdditionalPrice = x.AdditionalPrice,
                BasePrice = x.BasePrice,
                CreationTime = x.CreationTime,
                OrderStatus = x.Status,
                PaymentId = x.PaymentId,
                PaymentStatus = x.Payment != null ? x.Payment.Status : PaymentStatus.Pending,
                Name = x.Description != null ? x.Name : string.Empty,
                SelectedPaymentMethod = x.SelectedPaymentMethod,
                DeliveryPrice = x.DeliveryPrice
            }).ToArray();

        }

        public bool IsOrderOwnedByUser(int orderId, int userId)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order==null)
                throw new ServiceException($"Order not found", "order_not_found");
            return order.UserId == userId;
        }

        public void RemoveAllProductsFromBasket(int userId, int productId)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var products = _context.BasketProducts.Where(x => x.UserId == userId && x.ProductId == productId).ToList();
            if (products == null || !products.Any())
                return;
            _context.BasketProducts.RemoveRange(products);
            _context.SaveChanges();

        }

        public void RemoveProductFromBasket(int userId, int productId, int count)
        {
            if (!_context.Users.Any(x => x.Id == userId))
                throw new ServiceException($"User not found", "user_not_found");
            var product = _context.BasketProducts.Where(x => x.UserId == userId && x.ProductId == productId).FirstOrDefault();
            if(product==null)
                throw new ServiceException($"product in basket not found", "product_in_basket_not_found");
            product.Count = product.Count - count;
            if (product.Count <= 0)
                _context.BasketProducts.Remove(product);
            _context.SaveChanges();
        }

        public void RemoveProductsFromOrder(int orderId, int productId, int count)
        {
            var order = _context.MarketplaceOrders.FirstOrDefault(x => x.Id == orderId);
            if (order == null)
                throw new ServiceException($"User not found", "user_not_found");
            if (order.Status > OrderStatus.Created)
                throw new ServiceException($"Product changes only available for created orders", "cant_change_order");
            var product = _context.MarketplaceOrderProducts.Where(x => x.OrderId==orderId && x.ProductId == productId).FirstOrDefault();
            if (product == null)
                throw new ServiceException($"product in order not found", "product_in_order_not_found");
            product.Count = product.Count - count;
            if (product.Count <= 0)
                _context.MarketplaceOrderProducts.Remove(product);
            _context.SaveChanges();
        }

        public void AddProductToOrder(int orderId, int productId, int count)
        {
            var order = _context.MarketplaceOrders.FirstOrDefault(x => x.Id == orderId);
            if (order == null)
                throw new ServiceException($"User not found", "user_not_found");
            if (order.Status > OrderStatus.Created)
                throw new ServiceException($"Product changes only available for created orders", "cant_change_order");
            var oldProduct = _context.MarketplaceOrderProducts.Where(x => x.OrderId == orderId && x.ProductId == productId).FirstOrDefault();
            if (oldProduct != null)
            {
                oldProduct.Count += count;
            }
            else
            {
                var productEntity = _context.Products.FirstOrDefault(x => x.Id == productId);
                if (productEntity == null)
                    throw new ServiceException($"product with id {productId} not found", "product_not_found");
                var orderProductEntity = new MarketplaceOrderProductEntity
                {
                    Count = count,
                    OrderId = orderId,
                    ProductId = productId,
                };
                if (!string.IsNullOrEmpty(order.PromoCode))
                {
                    var code = _context.PromoCodes.FirstOrDefault(x => x.Code == order.PromoCode);
                    orderProductEntity.PromoCodeDiscount = code != null && !productEntity.CompanyCategory.Company.DontSupportPromocode ? code.Export.Percents : 0;
                    orderProductEntity.SupportPromoCode = code != null && !productEntity.CompanyCategory.Company.DontSupportPromocode;
                }
                _context.MarketplaceOrderProducts.Add(orderProductEntity);
            }
            _context.SaveChanges();
        }

        public void SetAdditionalPrice(int orderId, int price)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.AdditionalPrice = price;
            _context.SaveChanges();
        }

        public void SetBasePrice(int orderId, int price)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.BasePrice = price;
            _context.SaveChanges();
        }

        public void SetOrderStatus(int orderId, OrderStatus status)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.Status = status;

            _context.SaveChanges();
        }

        public bool CheckProductInBasket(int userId, int productId)
        {
            return _context.BasketProducts
                .Any(x => x.UserId == userId && x.ProductId == productId);
        }

        public PaymentMethod GetOrderPaymentMethod(int orderId)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            return order.SelectedPaymentMethod;
        }

        public void SetDeliveryPrice(int orderId, int price)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.DeliveryPrice = price;
            _context.SaveChanges();
        }

        public void SetOrderIsReady(int orderId)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.IsReady = true;
            _context.SaveChanges();
        }

        public void ClearPayment(int orderId)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null || order.PaymentId == null)
                return;
            order.PaymentId = null;
            _context.SaveChanges();
        }

        public void ChangeOrder(ChangeBasketOrder changes)
        {
            var order = _context.MarketplaceOrders.Find(changes.OrderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            order.City = changes.City;
            order.Address = changes.Adress;
            order.Name = changes.Name;
            order.Surname = changes.Surname;
            order.Phone = changes.Phone;
            order.Description = changes.Description;
            order.Email = changes.Email;
            _context.SaveChanges();
        }

        public int SaveBasket(ProductCount[] products, int userId)
        {
            var json = JsonConvert.SerializeObject(products);
            var savedBasketEntity = new SavedBasketEntity
            {
                UserOwnerId=userId,
                JSONProductsIds = json
            };
            _context.SavedBaskets.Add(savedBasketEntity);
            _context.SaveChanges();
            return savedBasketEntity.Id;
        }

        public BasketProduct[] GetAllProductsInSavedBasket(int basketId, int userId)
        {
            AddViewSavedBasket(basketId, userId);
            var basket = _context.SavedBaskets.Find(basketId);
            if(basket==null)
                throw new ServiceException("basket not found", "basket_not_found");
            var productsIds = JsonConvert.DeserializeObject<ProductCount[]>(basket.JSONProductsIds);
            var products = new List<BasketProduct>();
            foreach(var productId in productsIds)
            {
                var product = _context.Products.Find(productId.ProductId);
                if (product == null)
                    continue;
                var basketProduct = new BasketProduct
                {
                    Product = new ProductInfo
                    {
                        Id = product.Id,
                        PhotoUrl = product.Pictures
                         .Where(y => y.Processed == null || y.InternalUrl != null)
                         .OrderByDescending(y => y.Priority)
                         .Select(y => y.InternalUrl ?? y.Url)
                         .Take(2)
                         .ToArray(),
                        Description = product.Description,
                        Name = product.Name,
                        Price = product.Price,
                        PreviousPrice = product.PreviousPrice ?? product.ActualPrice,
                        Seller = product.CompanyCategory.Company.Name,
                        Vendor = product.Vendor,
                        VendorCode = product.VendorCode
                    },
                    Count = productId.Count
                };
                products.Add(basketProduct);
            }
            return products.ToArray();
        }

        public void AddViewSavedBasket(int savedBasketId, int userId)
        {
            var view = _context.ViewsSavedBaskets.FirstOrDefault(x => x.UserId == userId && x.SavedBasketId == savedBasketId);
            if (view == null)
            {
                view = new ViewSavedBasketEntity
                {
                    UserId = userId,
                    SavedBasketId = savedBasketId,
                    Count=1
                };
                _context.ViewsSavedBaskets.Add(view);
            }
            else
                view.Count += 1;
            _context.SaveChanges();
        }

        public ProductCount[] GetShortProductListFromSavedBasket(int savedBasketId)
        {
            var basket = _context.SavedBaskets.Find(savedBasketId);
            if (basket == null)
                throw new ServiceException("basket not found", "basket_not_found");
            return JsonConvert.DeserializeObject<ProductCount[]>(basket.JSONProductsIds);
        }

        public SavedBasketShortInfo[] GetAllSavedBasketsByUser(int userId)
        {
            var baskets = _context.SavedBaskets.Where(x => x.UserOwnerId == userId)
                .Select(x => new SavedBasketShortInfo
                {
                    Id = x.Id,
                    OwnerId = userId,
                    Views = x.Views.Select(y => new BasketView
                    {
                        UserId = y.UserId,
                        ViewsCount = y.Count
                    }).ToArray()
                }).ToArray();
            return baskets;

        }

        public void ChangePaymentMethod(int orderId, PaymentMethod paymentMethod)
        {
            var order = _context.MarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order not found", "order_not_found");
            if (order.SelectedPaymentMethod != paymentMethod)
            {
                order.SelectedPaymentMethod = paymentMethod;
                _context.SaveChanges();
            }
        }
    }
}