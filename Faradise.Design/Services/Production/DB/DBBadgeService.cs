﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Badge;
using Faradise.Design.Models.DAL;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBadgeService : IDBBadgeService
    {
        private FaradiseDBContext _context = null;

        public DBBadgeService(FaradiseDBContext context)
        {
            _context = context;
        }

        public Badge CreateNewBadge(string name)
        {
            var entity = new BadgeEntity { Name = name, Updated = DateTime.UtcNow };
            _context.Badges.Add(entity);
            _context.SaveChanges();
            return new Badge { Id = entity.Id, Name = name, IsActive = entity.IsActive };
        }

        public void DeleteBadge(int badgeId)
        {
            var entity = _context.Badges.Find(badgeId);
            if (entity == null)
                throw new ServiceException($"badge with id {badgeId} not found", "badge_not_found");
            entity.IsActive = false;
            entity.IsDeleting = true;
            entity.Updated = DateTime.UtcNow;
            entity.UpdateJson = JsonConvert.SerializeObject(new BadgeUpdateJson { RemoveAfterUpdate = true });
            _context.SaveChanges();
        }

        public Badge GetBadge(int badgeId)
        {
            return _context.Badges
                .Where(x => x.Id == badgeId)
                .Select(x => new Badge
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsActive = x.IsActive,
                    Description = x.Description,
                    UpdateJson = x.UpdateJson,
                    Picture = x.Picture,
                }).FirstOrDefault();
        }

        public BadgeShortInfo[] GetBadges()
        {
            return _context.Badges.Where(x => !x.IsDeleting).Select(x => new BadgeShortInfo
            {
                Id = x.Id,
                IsActive = x.IsActive,
                Name = x.Name
            })
                .ToArray();
        }

        public void SetBadgeActive(int badgeId, bool isActive)
        {
            var entity = _context.Badges.Find(badgeId);
            if (entity == null)
                throw new ServiceException($"badge with id {badgeId} not found", "badge_not_found");
            entity.IsActive = isActive;
            _context.SaveChanges();
        }

        public void UpdateBadge(Badge updateInfo)
        {
            if (updateInfo == null)
                return;
            var entity = _context.Badges.Find(updateInfo.Id);
            if (entity == null)
                throw new ServiceException($"badge with id {updateInfo.Id} not found", "badge_not_found");
            entity.Name = updateInfo.Name;
            entity.Description = updateInfo.Description;
            entity.Picture = updateInfo.Picture;
            entity.Updated = DateTime.UtcNow;
            var updateJson = new BadgeUpdateJson
            {
                AllMarkeplace = updateInfo.AllMarketplace,
                CategoryId = updateInfo.Categories,
                CompaniesId = updateInfo.Companies,
                ProdyctIds = updateInfo.Products
            };
            entity.UpdateJson = JsonConvert.SerializeObject(updateJson);
            _context.SaveChanges();
        }
    }
}
