﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Admin.Models;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.Admin.Product;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Services.DataMappers;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBAdminMarketplaceService : IDBAdminMarketplaceService
    {
        private readonly FaradiseDBContext _context;

        public DBAdminMarketplaceService(FaradiseDBContext db)
        {
            _context = db;
        }

        public ARProductFullInfo GetArProductFullInfo(int arProductId)
        {
            var arProduct = _context.ARProducts.Find(arProductId);
            if (arProduct == null)
                throw new ServiceException($"no ar product with id {arProductId} found", "ar_product_not_found");
            var product = new ARProductFullInfo
            {
                Id = arProduct.Id,
                FaradiseProductId = arProduct.FaradiseProductId,
                Color = arProduct.Color,
                FaradiseColorId = arProduct.FaradiseColorId,
                Name = arProduct.Name,
                ProductId = arProduct.ProductId,
                ProductName = arProduct.ProductId != null ? _context.Products.FirstOrDefault(x => x.Id == arProduct.ProductId).Name : string.Empty,
                Render = arProduct.Render,
                VendorCode = arProduct.VendorCode,
                Vertiacal = arProduct.Vertical
            };
            var productModels = arProduct.ARModels.ToArray();
            if (productModels.Length > 0)
            {
                var androidBFM = productModels.FirstOrDefault(x => x.Format == ARFormat.BFM && x.Platform == ARPlatform.Android);
                if (androidBFM != null)
                    product.AndroidBFM = new ARParsedModel
                    {
                        DiffuseUrl = androidBFM.Diffuse,
                        GeometryUrl = androidBFM.Model,
                        MetalicUrl = androidBFM.Metallic,
                        MetallicRoughnessUrl = androidBFM.MetallicRoughness,
                        NormalUrl = androidBFM.Normal,
                        RoughnessUrl = androidBFM.Roughness
                    };
                var androidSFB = productModels.FirstOrDefault(x => x.Format == ARFormat.SFB && x.Platform == ARPlatform.Android);
                if (androidSFB != null)
                    product.AndroidSFB = new ARParsedModel
                    {
                        DiffuseUrl = androidSFB.Diffuse,
                        GeometryUrl = androidSFB.Model,
                        MetalicUrl = androidSFB.Metallic,
                        MetallicRoughnessUrl = androidSFB.MetallicRoughness,
                        NormalUrl = androidSFB.Normal,
                        RoughnessUrl = androidSFB.Roughness
                    };
                var iosBFM = productModels.FirstOrDefault(x => x.Format == ARFormat.BFM && x.Platform == ARPlatform.IOS);
                if (iosBFM != null)
                    product.IosBFM = new ARParsedModel
                    {
                        DiffuseUrl = iosBFM.Diffuse,
                        GeometryUrl = iosBFM.Model,
                        MetalicUrl = iosBFM.Metallic,
                        MetallicRoughnessUrl = iosBFM.MetallicRoughness,
                        NormalUrl = iosBFM.Normal,
                        RoughnessUrl = iosBFM.Roughness
                    };
                var iosSCN = productModels.FirstOrDefault(x => x.Format == ARFormat.SCN && x.Platform == ARPlatform.IOS);
                if (iosSCN != null)
                    product.IosSCN = new ARParsedModel
                    {
                        DiffuseUrl = iosSCN.Diffuse,
                        GeometryUrl = iosSCN.Model,
                        MetalicUrl = iosSCN.Metallic,
                        MetallicRoughnessUrl = iosSCN.MetallicRoughness,
                        NormalUrl = iosSCN.Normal,
                        RoughnessUrl = iosSCN.Roughness
                    };
            }
            return product;
        }

        public void MapARProductToProduct(int? productId, int arProductId)
        {
            if (productId != null)
            {
                var products = _context.Products.Select(x => x.Id).Where(x => x == productId);
                if (products == null || products.Count() < 1)
                    throw new ServiceException($"no product with id {productId} found", "product_not_found");
            }
            var arProduct = _context.ARProducts.Find(arProductId);
            if (arProduct == null)
                throw new ServiceException($"no ar product with id {arProductId} found", "ar_product_not_found");
            arProduct.ProductId = productId;
            _context.SaveChanges();
        }

        public ARProductShortInfo[] GetArProductList()
        {
            return _context.ARProducts.Select(x => new { x.Id, x.FaradiseProductId, x.Name, x.Color, x.Product })
                .Select(x => new ARProductShortInfo
                {
                    Id = x.Id,
                    Color = x.Color,
                    FaradiseName = x.Name,
                    FaradiseId = x.FaradiseProductId,
                    ProductId = x.Product != null ? x.Product.Id : 0,
                    ProductName = x.Product != null ? x.Product.Name : string.Empty
                }).ToArray();
        }

        public ModerationCountsModel GetModerationProduct()
        {
            var needColor = _context.Products.Count(x => !x.ProductColors.Any());
            var needRating = _context.Products.Count(x => x.Rating == null);

            return new ModerationCountsModel() { ProductWithoutColorCount = needColor, ProductWithoutRatingCount = needRating };
        }

        public GetModerationProductModel GetProductForModeration(string type)
        {
            ProductEntity model;
            if(type == "color")
                model = _context.Products.FirstOrDefault(x => !x.ProductColors.Any());
            else
                model = _context.Products.FirstOrDefault(x => x.Rating == null);

            if (model == null)
                throw new ServiceException("Product for moderation is null", "model_is_null");

            List<string> photoUrls = new List<string>();
            foreach(var picture in model.Pictures)
            {
                photoUrls.Add(picture.Url);
            }

            List<int> colors = new List<int>();
            List<int> marketplaceColors = new List<int>();
            foreach (var color in model.ProductColors)
            {
                colors.Add(color.Id);
                if(color.CompanyColor?.MarketplaceColorId != null)
                    marketplaceColors.Add(color.CompanyColor.MarketplaceColorId.Value);
            }

            var productModel = new GetModerationProductModel() {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                PhotoUrls = photoUrls.ToArray(),
                Price = model.Price,
                Colors = colors.ToArray(),
                MarketplaceColors = marketplaceColors.ToArray()
            };

            return productModel;
        }

        public ProductsStats GetProductsStats()
        {
            var stats = new ProductsStats
            {
                TotalProducts = _context.Products.WhereAvailable().Count(),

                Name = _context.Products.WhereAvailable().Count(y => y.Name != null && y.Name != string.Empty),

                Description = _context.Products.WhereAvailable().Count(y => y.Description != null && y.Description != string.Empty),

                PreviousPrice = _context.Products.WhereAvailable().Count(y => y.PreviousPrice != null),
                Url = _context.Products.WhereAvailable().Count(y => y.Url != null),

                Pickup = _context.Products.WhereAvailable().Count(y => y.Pickup != null),
                Delivery = _context.Products.WhereAvailable().Count(y => y.Delivery != null),
                DeliveryOptions = _context.Products.WhereAvailable().Count(y => y.DeliveryCost != null),

                Vendor = _context.Products.WhereAvailable().Count(y => y.Vendor != null && y.Vendor != string.Empty),
                Warranty = _context.Products.WhereAvailable().Count(y => y.Warranty != null),

                Origin = _context.Products.WhereAvailable().Count(y => y.Origin != null),
                Size = _context.Products.WhereAvailable().Count(y => y.Size != null && y.Size != string.Empty),

                Notes = _context.Products.WhereAvailable().Count(y => y.JSONNotes != null && y.JSONNotes != string.Empty),

                Pictures = _context.Products.WhereAvailable().Count(y => y.Pictures.Any()),

                Colors = _context.Products.WhereAvailable().Count(y => y.ProductColors.Any())
            };

            return stats;
        }

        public void SetModerationProduct(ModerationProductModel model)
        {
            var product = _context.Products.FirstOrDefault(x => x.Id == model.ProductId);
            if(product == null)
                throw new ServiceException("Moderation product is null", "model_is_null");


            //color?

            foreach ( var colorId in model.ColorId)
            {
                var marketplaceColor = _context.MarketplaceColors.FirstOrDefault(x => x.Id == colorId);
                if (marketplaceColor == null)
                    throw new ServiceException("Marketplace color is null", "marketplce_color_null");

                var companyColor = _context.CompanyColors.FirstOrDefault(x => x.Name.ToLower() == marketplaceColor.Name.ToLower());
                if (companyColor == null)
                {
                    companyColor = new CompanyColorEntity() { Name = marketplaceColor.Name.ToLower(), MarketplaceColorId = marketplaceColor.Id, MarketplaceColor = marketplaceColor };
                    _context.CompanyColors.Add(companyColor);
                    _context.SaveChanges();

                    companyColor = _context.CompanyColors.FirstOrDefault(x => x.Name.ToLower() == marketplaceColor.Name.ToLower());
                    if (companyColor == null)
                        throw new ServiceException("Company color is null", "company_color_null");
                }

                var color = product.ProductColors.FirstOrDefault(x => x.CompanyColorId == companyColor.Id);
                if (color == null)
                {
                    var productColor = _context.ProductColors.FirstOrDefault(x => x.CompanyColorId == companyColor.Id && x.ProductId == product.Id);
                    if (productColor == null)
                    {
                        productColor = new ProductColorEntity() { ProductId = product.Id, Product = product, CompanyColorId = companyColor.Id, CompanyColor = companyColor };
                        _context.ProductColors.Add(productColor);
                        _context.SaveChanges();

                        productColor = _context.ProductColors.FirstOrDefault(x => x.CompanyColorId == companyColor.Id && x.ProductId == product.Id);
                        if (productColor == null)
                            throw new ServiceException("Product color is null", "product_color_null");
                    }

                    productColor.IsModerated = true;
                    product.ProductColors.Add(productColor);
                }
            }

            product.Description = model.Description;
            product.Rating = model.Rating;
            _context.SaveChanges();
        }

        public PossibleDailyProduct[] GetPossibleDailyProducts(int offset, int count, float companyKoef,
            float categoryKoef, float saleKoef)
        {
            return _context.Products
                .WhereAvailable()
                .WhereHavePictures()
                .OrderByDescending(p => p.CompanyCategory.Company.DailyProductPriority * companyKoef
                                        + p.CompanyCategory.Category.DailyProductPriority * categoryKoef
                                        + saleKoef * (100.0f - (p.PreviousPrice.HasValue && p.PreviousPrice.Value > 0
                                                          ? (p.Price * 100.0f / p.PreviousPrice.Value)
                                                          : ((p.ActualPrice.HasValue && p.ActualPrice.Value > 0)
                                                              ? (p.Price * 100.0f / p.ActualPrice.Value)
                                                              : 100.0f))))
                .Skip(offset)
                .Take(count)
                .Select(x => new PossibleDailyProduct
                {
                    Id = x.Id,
                    Name = x.Name,
                    Price = x.Price,
                    PreviousPrice = x.PreviousPrice ?? x.ActualPrice,

                    PhotoUrl = x.Pictures
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .FirstOrDefault(),

                    CompanyName = x.CompanyCategory.Company.Name,
                    TotalPriority = x.CompanyCategory.Company.DailyProductPriority * companyKoef
                                    + x.CompanyCategory.Category.DailyProductPriority * categoryKoef
                                    + saleKoef * (100.0f - (x.PreviousPrice.HasValue && x.PreviousPrice.Value > 0
                                                      ? (x.Price * 100.0f / x.PreviousPrice.Value)
                                                      : ((x.ActualPrice.HasValue && x.ActualPrice.Value > 0)
                                                          ? (x.Price * 100.0f / x.ActualPrice.Value)
                                                          : 100.0f)))
                })
                .ToArray();
        }

        public DailyProduct GetDailyProduct()
        {
            var productIdAndTime = _context.DailyProducts.OrderByDescending(x => x.StartDate).Take(1)
                .Select(x => new {x.ProductId, x.StartDate}).FirstOrDefault();
            if (productIdAndTime == null)
                return null;
            var product = _context.Products.Find(productIdAndTime.ProductId);

            return new DailyProduct
            {
                Product = new PossibleDailyProduct
                {
                    Id = product.Id,
                    Name = product.Name,
                    Price = product.Price,
                    PreviousPrice = product.PreviousPrice ?? product.ActualPrice,

                    PhotoUrl = product.Pictures
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .FirstOrDefault(),

                    CompanyName = product.CompanyCategory.Company.Name
                },
                OfferTime = productIdAndTime.StartDate
            };
        }

        public void SetDailyProduct(int productId)
        {
            _context.DailyProducts.Add(new DailyProductEntity { StartDate = DateTime.UtcNow, ProductId = productId });
            _context.SaveChanges();
        }

        public DailyProduct[] GetPreviousDailyProducts()
        {
            return _context.DailyProducts
                .OrderByDescending(x => x.StartDate)
                .Select(x => new DailyProduct
                {
                    OfferTime = x.StartDate,
                    Product = new PossibleDailyProduct
                    {
                        Id = x.ProductId,
                        Name = x.Product.Name,
                        Price = x.Product.Price,
                        PreviousPrice = x.Product.PreviousPrice ?? x.Product.ActualPrice,

                        PhotoUrl = x.Product.Pictures
                            .OrderByDescending(y => y.Priority)
                            .Select(y => y.InternalUrl ?? y.Url)
                            .FirstOrDefault(),

                        CompanyName = x.Product.CompanyCategory.Company.Name
                    }
                }).ToArray();
        }

        public async Task SetProductImagesPriority(ProductImages model)
        {
            var pictures = await _context.Products
                .Where(x => x.Id == model.ProductId)
                .SelectMany(x => x.Pictures)
                .ToArrayAsync();

            foreach (var i in model.Images)
            {
                var picture = pictures.FirstOrDefault(x => x.Id == i.ImageId);
                if (picture != null)
                    picture.Priority = i.Priority;
            }

            await _context.SaveChangesAsync();
        }

        public async Task<ProductImages> GetProductImages(int productId)
        {
            var p = await _context.Products
                .Include(x => x.Pictures)
                .Where(x => x.Id == productId)
                .FirstAsync();

            return new ProductImages
            {
                ProductId = p.Id,
                Name = p.Name,
                Images = p.Pictures
                    .OrderByDescending(y => y.Priority)
                    .Select(y => new ProductImagePriority
                    {
                        ImageId = y.Id,
                        ImageUrl = y.InternalUrl ?? y.Url,
                        Priority = y.Priority
                    })
                    .ToArray()
            };
        }
    }
}
