﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;

namespace Faradise.Design.Services.Production.DB
{
    public class DBUserService : IDBUserService
    {
        private readonly FaradiseDBContext _context;

        public DBUserService(FaradiseDBContext db)
        {
            _context = db;
        }

        public void CreateAlias(Alias alias)
        {
            var dbAlias = new UserAliasEntity()
            {
                UserId=alias.UserId,
                Value = alias.Value,
                AliasType = alias.Type
            };
            _context.Aliases.Add(dbAlias);
            _context.SaveChanges();
        }

        public User CreateUser(Role role)
        {
            var user = new UserEntity()
            {
                Role = role,
                CreationDate = DateTime.UtcNow
            };
            _context.Users.Add(user);
            _context.SaveChanges();

            return new User { Id = user.Id, Role = role };
        }

        public Alias GetAlias(AliasType type, string value)
        {
            var alias = _context.Aliases.Where(x => x.AliasType == type && x.Value == value).FirstOrDefault();
            if (alias == null)
                return null;
            return new Alias { Id = alias.Id, Type = alias.AliasType, Value = alias.Value, UserId=alias.UserId };
        }

        public List<Alias> GetAliases(int userId)
        {
            return _context.Aliases
                .Where(x => x.UserId == userId)
                .Select(x => new Alias { Id = x.Id, Type = x.AliasType, Value = x.Value, UserId = x.UserId })
                .ToList();
        }

        public User GetUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                return null;
            return new User { Id = user.Id, Role = user.Role, Email = user.Email, Phone = user.Phone, Name = user.Name };
        }

        public string GetUserEmail(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException($"User with id {id} was not found", "user_not_found");
            return user.Email;
        }

        public VerificationCode GetVerificationCode(string phone)
        {
            var codes = _context.VerificationCodes.ToList();
            var code = _context.VerificationCodes.LastOrDefault(x => x.Number == phone);
            // TODO:: Нужно как-то удалять обработанный код из базы
            if (code == null)
                return null;
            return new VerificationCode { Code = code.Code, Phone = code.Number, Expires = code.Expires, CreationDate=code.CreationDate };

        }

        public void SetUserEmail(int id, string email)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException($"User with id {id} was not found", "user_not_found");
            user.Email = email.ToLower();
            _context.SaveChanges();
        }

        public void SetUserRole(int id, Role role)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException($"User with id {id} was not found", "user_not_found");
            user.Role = role;
            _context.SaveChanges();
        }

        public void StoreVerificationCode(VerificationCode code)
        {
            var verifCode = new VerificationCodeEntity
            {
                Code = code.Code,
                Number = code.Phone,
                Expires = code.Expires,
                CreationDate=code.CreationDate
            };

            _context.VerificationCodes.Add(verifCode);
            _context.SaveChanges();
        }

        public void SetUserName(int id, string name)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException($"User with id {id} was not found", "user_not_found");

            user.Name = name;

            _context.SaveChanges();
        }

        public void SetUserPhone(int id, string phone)
        {
            var user = _context.Users.Find(id);
            if (user == null)
                throw new ServiceException($"User with id {id} was not found", "user_not_found");

            user.Phone = phone;

            _context.SaveChanges();
        }
    }
}
