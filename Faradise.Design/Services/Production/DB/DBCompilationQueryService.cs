﻿using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Utilities.Expander;

namespace Faradise.Design.Services.Production.DB
{
    public class DBCompilationQueryService : IDBCompilationQueryService
    {
        private readonly FaradiseDBQueryContext _context;

        public DBCompilationQueryService(FaradiseDBQueryContext context)
        {
            _context = context;
        }

        public ProductsQuery GetProductForCompilation(string compilationName, int offset, int count)
        {
            var name = compilationName.ToLower();
            var productCompilation = _context.ProductCompilations
                .Select(x => new { x.Name, x.Id })
                .FirstOrDefault(x => x.Name.ToLower() == name);

            if (productCompilation != null)
                return GetProductsFromProductCompilation(productCompilation.Id, offset, count);

            var categoryCompilation = _context.CategoryCompilations
                .Select(x => new { x.Name, x.Id })
                .FirstOrDefault(x => x.Name.ToLower() == name);

            if (categoryCompilation != null)
                return GetProductsFromCategoryCompilation(categoryCompilation.Id, offset, count);

            return GetRandomProducts(offset, count);
        }

        private ProductsQuery GetRandomProducts(int offset, int count)
        {
            return _context.MarketplaceEntity
                .AsExpandable()
                .Where(x => x.Id > 0)
                .Select(x => new
                {
                    Products = x.Categories
                        .SelectMany(y => y.CompanyCategories)
                        .SelectMany(y => y.Products)
                        .Where(y => ProductEntity.FilterAvailable(y))
                })
                .Select(z => new ProductsQuery
                {
                    AllCount = z.Products.Count(),
                    Products = z.Products
                        .OrderBy(y => y.Id)
                        .Skip(offset)
                        .Take(count)
                        .Select(x => ProductEntity.AsProductInfo(x))
                        .ToArray()
                })
                .FirstOrDefault();
        }

        private ProductsQuery GetProductsFromProductCompilation(int compilationId, int offset, int count)
        {
            var query = _context.ProductCompilations
                .AsExpandable()
                .Where(x => x.Id == compilationId)
                .Select(x => new
                {
                    Products = x.CompilationToProducts
                        .OrderBy(c => c.Order)
                        .Select(y => y.Product)
                        .Where(y => ProductEntity.FilterAvailable(y))
                })
                .Select(z => new ProductsQuery
                {
                    AllCount = z.Products.Count(),
                    Products = z.Products
                        .Skip(offset)
                        .Take(count)
                        .Select(x => ProductEntity.AsProductInfo(x))
                        .ToArray()
                })
                .FirstOrDefault();

            return query;
        }

        private ProductsQuery GetProductsFromCategoryCompilation(int compilationId, int offset, int count)
        {
            var query = _context.CategoryCompilations
                .Where(x => x.Id == compilationId)
                .Select(x => new
                {
                    Products = x.CompilationToCategory
                        .Select(y => y.Category)
                        .SelectMany(y => y.CompanyCategories)
                        .SelectMany(y => y.Products)
                        .Where(y => ProductEntity.FilterAvailable(y))
                })
                .Select(z => new ProductsQuery
                {
                    AllCount = z.Products.Count(),
                    Products = z.Products
                        .OrderBy(y => y.Price)
                        .Skip(offset)
                        .Take(count)
                        .Select(x => ProductEntity.AsProductInfo(x))
                        .ToArray()
                })
                .FirstOrDefault();

            return query;
        }
    }
}
