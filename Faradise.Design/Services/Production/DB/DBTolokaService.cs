﻿using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Toloka;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Production.DB
{
    public class DBTolokaService : IDBTolokaService
    {
        private readonly FaradiseDBContext _context = null;

        public DBTolokaService(FaradiseDBContext context)
        {
            _context = context;
        }

        public void AddTask(TolokaTaskModel model)
        {
            var task = new TolokaTaskEntity() { Url = model.Url, Status = model.Status, Type = model.Type, Created = model.Created };
            _context.TolokaTasks.Add(task);
            _context.SaveChanges();
        }

        public void AddTaskSegment(TolokaTaskSegmentModel model)
        {
            var segment = new TolokaTaskSegmentEntity() { TaskId = model.TaskId, Url = model.Url, Type = model.Type, Status = model.Status };
            _context.TolokaTaskSegments.Add(segment);
            _context.SaveChanges();
        }

        public TolokaTaskModel[] GetTasks()
        {
            var model = _context.TolokaTasks
                .Include(x => x.Segments)
                .Select(x => new TolokaTaskModel()
            {
                Id = x.Id,
                Created = x.Created,
                Status = x.Status,
                Type = x.Type,
                Url = x.Url,
                ReportUrl = x.ReportUrl,
                SegmentsCount = x.Segments.Count(),
                ProcessedSegmentsCount = x.Segments.Where(y => y.Status == TolokaTaskStatus.Processed).Count()
            }).ToArray();
            return model;
        }

        public TolokaTaskModel GetTaskByStatus(TolokaTaskStatus status)
        {
            var entity = _context.TolokaTasks.FirstOrDefault(x => x.Status == status);

            if (entity == null)
                return null;

            return new TolokaTaskModel()
            {
                Id = entity.Id,
                Status = entity.Status,
                Created = entity.Created,
                Type = entity.Type,
                Url = entity.Url,
                ReportUrl = entity.ReportUrl
            };
        }

        public TolokaTaskModel GetTaskById(int taskId)
        {
            var entity = _context.TolokaTasks.FirstOrDefault(x => x.Id == taskId);

            if (entity == null)
                return null;

            return new TolokaTaskModel()
            {
                Id = entity.Id,
                Status = entity.Status,
                Created = entity.Created,
                Type = entity.Type,
                Url = entity.Url,
                ReportUrl = entity.ReportUrl
            };
        }

        public void UpdateTaskStatus(int taskId, TolokaTaskStatus status)
        {
            var entity = _context.TolokaTasks.FirstOrDefault(x => x.Id == taskId);
            entity.Status = status;
            _context.SaveChanges();
        }

        public void UpdateSegmentStatus(int segmentId, TolokaTaskStatus status)
        {
            var entity = _context.TolokaTaskSegments.FirstOrDefault(x => x.Id == segmentId);
            entity.Status = status;
            _context.SaveChanges();
        }

        public TolokaTaskSegmentModel GetSegmentByStatus(TolokaTaskStatus status)
        {
            var entity = _context.TolokaTaskSegments.FirstOrDefault(x => x.Status == status);

            if (entity == null)
                return null;

            return new TolokaTaskSegmentModel()
            {
                Id = entity.Id,
                Status = entity.Status,
                Type = entity.Type,
                Url = entity.Url,
                TaskId = entity.TaskId
            };
        }

        public void ClearStatus()
        {
            var segments = _context.TolokaTaskSegments
                .Where(x => x.Status == TolokaTaskStatus.Processing)
                .ToList();

            segments.ForEach(x => x.Status = TolokaTaskStatus.Created);

            var tasks = _context.TolokaTasks
                .Include(x => x.Segments)
                .Where(x => x.Status == TolokaTaskStatus.Processing)
                .ToList();

            tasks.ForEach(x => x.Status = TolokaTaskStatus.Created);
            _context.TolokaTaskSegments.RemoveRange(tasks.SelectMany(y => y.Segments));
            _context.SaveChanges();
        }

        public void UpdateTaskReportUrl(int taskId, string reportUrl)
        {
            var entity = _context.TolokaTasks.FirstOrDefault(x => x.Id == taskId);
            if (entity != null)
            {
                entity.ReportUrl = reportUrl;
                _context.SaveChanges();
            }
        }

        public TolokaTaskSegmentModel[] GetSegments(int taskId)
        {
            var model = _context.TolokaTaskSegments
                .Where(x => x.TaskId == taskId)
                .Select(x => new TolokaTaskSegmentModel()
                {
                    Id = x.Id,
                    Status = x.Status,
                    Type = x.Type,
                    Url = x.Url
                }).ToArray();
            return model;
        }

        public void DeleteTask(int taskId)
        {
            var task = _context.TolokaTasks.FirstOrDefault(x => x.Id == taskId);

            if (task != null)
            {
                _context.TolokaTasks.Remove(task);
                _context.SaveChanges();
            }
        }

        public void RestartSegment(int segmentId)
        {
            var segment = _context.TolokaTaskSegments.FirstOrDefault(x => x.Id == segmentId);

            if(segment != null)
            {
                segment.Status = TolokaTaskStatus.Created;
                _context.SaveChanges();
            }
        }
    }
}
