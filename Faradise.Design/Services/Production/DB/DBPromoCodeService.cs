﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.PromoCode;

namespace Faradise.Design.Services.Production.DB
{
    public class DBPromoCodeService : IDBPromoCodeService
    {
        private readonly FaradiseDBContext _context;

        public DBPromoCodeService(FaradiseDBContext db)
        {
            _context = db;
        }

        public PromoCode GetPromoCode(string code)
        {
            var select = _context.PromoCodes.Where(x => x.Code == code).Select(x => new { Code = x, Export = x.Export }).FirstOrDefault();
            if (select == null)
                return null;
            return new PromoCode
            {
                Code = code,
                PercentDiscount = select.Export.Percents,
                AbsoluteDiscount = select.Export.Absolute,
                CodeType = select.Export.PromoCodeType,
                ExpireDate = select.Export.ExpireDate,
                StartDate = select.Export.StartDate,
                MinPrice = select.Export.MinPrice,
                Used = select.Code.Used,
                ExportName = select.Export.Name
            };
        }

        public void DisablePromocode(string code)
        {
            var promoCodeEntity = _context.PromoCodes.FirstOrDefault(x => x.Code == code);
            if (promoCodeEntity == null)
                throw new ServiceException($"Prmocode {code} not found", "promocode_not_found");
            if (promoCodeEntity.Export.PromoCodeType == PromoCodeType.Named)
                return;
            promoCodeEntity.Used = true;
            promoCodeEntity.UseDate = DateTime.UtcNow;
            _context.SaveChanges();
        }

        public PromocodeProduct[] GetProductsByIds(ProductCount[] products)
        {
            return _context.Products
                .Join(products, p2 => p2.Id, p1 => p1.ProductId, (p1, p2) => new { p1, p2 })
                .Select(x => new PromocodeProduct
                {
                    ProductId = x.p2.ProductId,
                    Count = x.p2.Count,
                    PriceWithoutPromocode = x.p1.Price,
                    ProductName = x.p1.Name,
                    SupportPromocode = !x.p1.CompanyCategory.Company.DontSupportPromocode
                }).ToArray();
        }

        public PromocodeProduct[] GetProductsFromBasket(int userId)
        {
            return _context.BasketProducts
                .Where(x => x.UserId == userId)
                .Select(x => new { x.Product, x.Count })
                .Select(x => new PromocodeProduct
                {
                    ProductId = x.Product.Id,
                    Count = x.Count,
                    PriceWithoutPromocode = x.Product.Price,
                    ProductName = x.Product.Name,
                    SupportPromocode = !x.Product.CompanyCategory.Company.DontSupportPromocode
                }).ToArray();
        }

        public PromoCodeExport[] GetPromoCodeExports()
        {
            return _context.PromoCodeExports.Select(x =>
            new PromoCodeExport
            {
                CodeType = x.PromoCodeType,
                StartDate = x.StartDate,
                Count = x.Codes != null ? x.Codes.Count() : 0,
                CreateDate = x.CreationDate,
                ExpireDate = x.ExpireDate,
                Name = x.Name,
                Used = x.Codes != null ? x.Codes.Count(c => c.Used) : 0,
                PercentsDiscount = x.Percents,
                AbsoluteDisocunt = x.Absolute,
                MinPrice = x.MinPrice
            }).ToArray();
        }

        public PromoCodeExport GetPromoCodeExport(string exportname)
        {
            var entity = _context.PromoCodeExports.FirstOrDefault(x => x.Name == exportname);

            if (entity == null)
            {
                return null;
            }
            else
            {
                return new PromoCodeExport
                {
                    CodeType = entity.PromoCodeType,
                    StartDate = entity.StartDate,
                    MinPrice = entity.MinPrice,
                    Count = entity.Codes != null ? entity.Codes.Count() : 0,
                    CreateDate = entity.CreationDate,
                    ExpireDate = entity.ExpireDate,
                    Name = entity.Name,
                    Used = entity.Codes != null ? entity.Codes.Count(c => c.Used) : 0,
                    PercentsDiscount = entity.Percents,
                    AbsoluteDisocunt = entity.Absolute
                };
            }
        }

        private string CreatePromoString(int num)
        {
            var b32codeArray = Utilities.Encode.ToFaradiseAlphabed.Encode(num);
            var dateNum = Int32.Parse(DateTime.UtcNow.ToString("ddMMyy"));
            var b32date = Utilities.Encode.Base36.Encode(dateNum).ToUpper();
            return b32date + "-" + b32codeArray;
        }

        private void AddPromoCodes(List<PromoCodeEntity> codes, string exportName, PromoCodeType codeType)
        {
            _context.PromoCodes.AddRange(codes);
            _context.SaveChanges();
            foreach (var c in codes)
                c.Code = codeType == PromoCodeType.Unique ? CreatePromoString(c.Id) : exportName;
            _context.SaveChanges();
        }

        private void CreatePromocodes(int count, int exportId, string exportName, PromoCodeType codeType)
        {
            var perPush = 1000;
            var codes = new List<PromoCodeEntity>(perPush);
            for (var pushed = 0; pushed < count; pushed++)
            {
                codes.Add(new PromoCodeEntity { Used = false, ExportId = exportId });
                if (codes.Count > perPush)
                {
                    AddPromoCodes(codes, exportName, codeType);
                    codes.Clear();
                }
            }
            if (codes.Count > 0)
                AddPromoCodes(codes, exportName, codeType);
        }

        public void CreatePromoCodeExport(string name, int count, int minPrice, int percentsDiscount, int absoluteDiscount, PromoCodeType codeType, DateTime startDate, DateTime expireDate)
        {
            if (_context.PromoCodeExports.Any(x => x.Name == name))
                throw new ServiceException($"promocode export name {name} is taken", "promocode_export_name_is_taken");
            var exportEntity = new PromoCodeExportEntity
            {
                CreationDate = DateTime.UtcNow,
                ExpireDate = expireDate,
                Name = name,
                MinPrice = minPrice,
                Percents = percentsDiscount,
                Absolute = absoluteDiscount,
                PromoCodeType = codeType,
                StartDate = startDate
            };
            _context.PromoCodeExports.Add(exportEntity);
            _context.SaveChanges();
            if (codeType == PromoCodeType.Named)
                count = 1;
            CreatePromocodes(count, exportEntity.Id, exportEntity.Name, exportEntity.PromoCodeType);
        }

        public void CreatePromoCodeForExport(string exportname, int count)
        {
            var exportEntity = _context.PromoCodeExports.FirstOrDefault(x => x.Name == exportname);
            if (exportEntity == null)
                throw new ServiceException($"promocode export {exportname} not found", "promocode_export_not_found");
            CreatePromocodes(count, exportEntity.Id, exportEntity.Name, exportEntity.PromoCodeType);
        }

        public PromoCode[] GetPromoCodesForExport(string exportname)
        {
            return _context.PromoCodeExports
                .Where(x => x.Name == exportname)
                .Take(1)
                .SelectMany(x => x.Codes)
                .Select(x => new PromoCode
                {
                    Code = x.Code,
                    CodeType = x.Export.PromoCodeType,
                    ExpireDate = x.Export.ExpireDate,
                    ExportName = x.Export.Name,
                    PercentDiscount = x.Export.Percents,
                    AbsoluteDiscount = x.Export.Absolute,
                    MinPrice = x.Export.MinPrice,
                    StartDate = x.Export.StartDate,
                    Used = x.Used
                }).ToArray();
        }
    }
}
