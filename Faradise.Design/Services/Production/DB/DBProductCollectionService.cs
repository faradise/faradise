﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.ProductCollection;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Production.DB
{
    public class DBProductCollectionService : IDBProductCollectionService
    {

        private char[] _splitChars = new char[] { ' ', '-', ',', '.', ':', '\t' };
        private Regex _isDigital = new Regex(@"^\d+$");
        private Regex _replaceBrackets = new Regex(@"\(.+\)");
        private readonly FaradiseDBContext _context;

        public DBProductCollectionService(FaradiseDBContext context)
        {
            _context = context;
        }

        public void AddWordToBackList(string word)
        {
            var blacklistJson = _context.MarketplaceEntity.FirstOrDefault().CollectionBlacklist;
            var blacklist = string.IsNullOrEmpty(blacklistJson) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(blacklistJson);
            blacklist.Add(word.ToLower());
            _context.MarketplaceEntity.FirstOrDefault().CollectionBlacklist = JsonConvert.SerializeObject(blacklist);
            _context.SaveChanges();
        }

        public async Task<Dictionary<string, List<ProductName>>> FindProductCollectionsForCompany(int companyId, int takeProducts)
        {
            List<ProductName> products = new List<ProductName>();
            var count = await _context.Products
                .Where(x => x.CompanyId == companyId)
                .WhereAvailable()
                .CountAsync();

            int errors = 0;
            for (int skip = 0; skip < count; skip += takeProducts)
            {
                try
                {
                    var productPart = await GetProductsForCompanyAsync(companyId, skip, takeProducts);
                    products.AddRange(productPart);
                }
                catch (Exception)
                {
                    skip -= takeProducts;
                    errors++;
                    if (errors > 50)
                        throw;
                }
            }

            var blacklist = GetBlackList();
            var collections = new Dictionary<string, List<ProductName>>();
            foreach (var p in products)
            {
                var name = _replaceBrackets.Replace(p.Name, "");
                var tags = name.Split(_splitChars, StringSplitOptions.RemoveEmptyEntries);
                tags = tags.Except(blacklist).ToArray();
                foreach (var t in tags)
                {
                    var checkTag = t.Replace("x", "").Replace("х", "");
                    if (t.Length < 2 || _isDigital.IsMatch(checkTag))
                        continue;
                    var lTag = t.ToLower();
                    List<ProductName> collection = null;
                    if (collections.TryGetValue(lTag, out collection))
                    {
                        collection.Add(new ProductName() { ProductId = p.ProductId, Name = p.Name });
                    }
                    else
                    {
                        collections.Add(lTag, new List<ProductName> { new ProductName() { ProductId = p.ProductId, Name = p.Name } });
                    }
                }
            }

            collections = collections
                .Where(x => x.Value.Count > 1)
                .ToDictionary(x => x.Key, y => y.Value);

            return collections;
        }

        public List<string> GetBlackList()
        {
            var blacklistJson = _context.MarketplaceEntity.FirstOrDefault().CollectionBlacklist;
            var blacklist = string.IsNullOrEmpty(blacklistJson) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(blacklistJson);
            return blacklist;
        }

        public List<ProductName> GetProductsForCompany(int companyId, int skipProducts, int takeProducts)
        {
             return _context.Products
                .Where(x => x.CompanyId == companyId)
                .WhereAvailable()
                .Skip(skipProducts)
                .Take(takeProducts)
                .Select(x => new ProductName() { ProductId = x.Id, Name = x.Name })
                .ToList();
        }

        public async Task<List<ProductName>> GetProductsForCompanyAsync(int companyId, int skipProducts, int takeProducts)
        {
            return await _context.Products
                .Where(x => x.CompanyId == companyId)
                .WhereAvailable()
                .Skip(skipProducts)
                .Take(takeProducts)
                .Select(x => new ProductName() { ProductId = x.Id, Name = x.Name })
                .ToListAsync();
        }

        public void RemoveWordFromBlackList(string word)
        {
            var blacklistJson = _context.MarketplaceEntity.FirstOrDefault().CollectionBlacklist;
            var blacklist = string.IsNullOrEmpty(blacklistJson) ? new List<string>() : JsonConvert.DeserializeObject<List<string>>(blacklistJson);
            blacklist.Remove(word.ToLower());
            _context.MarketplaceEntity.FirstOrDefault().CollectionBlacklist = JsonConvert.SerializeObject(blacklist);
            _context.SaveChanges();
        }

        public async Task UpdateProductCollectionForCompany(int companyId, int takeProducts)
        {
            var foundCollections = await FindProductCollectionsForCompany(companyId, takeProducts);
            int collectionsPerIterations = 10;
            for (int i = 0; i < foundCollections.Count; i+= collectionsPerIterations)
            {
                var actualCollections = foundCollections
                    .Skip(i)
                    .Take(collectionsPerIterations)
                    .ToDictionary(x => x.Key, y => y.Value);

                //1. Находим старые коллекции, которые нужно обновить в соответствии с новым списком продуктов
                var oldCollections = await _context.ProductCollections
                    .Where(x => x.CompanyId == companyId)
                    .Join(actualCollections, x => x.KeyWord, y => y.Key,
                    (x, y) => new
                    {
                        CollectionId = x.Id,
                        Keyword = x.KeyWord,
                        OldProducts = x.ToProducts.Select(p => p.ProductId),
                        TargetProducts = y.Value.Select(p => p.ProductId),
                        Available = x.Available,
                        ManualDisable = x.ManualDisable,
                        CollectionEntity = x
                    }).ToArrayAsync();

                foreach (var oldCollection in oldCollections)
                {
                    //Добавляем недостающие продукты в коллекцию
                    var newProducts = oldCollection.TargetProducts.Except(oldCollection.OldProducts);
                    var newProductsEntities = newProducts.Select(x => new ProductToCollectionEntity
                    {
                        CollectionId = oldCollection.CollectionId,
                        ProductId = x
                    });
                    _context.ProductsToCollections
                        .AddRange(newProductsEntities);

                    //Удаляем отсутсвующие продукты в коллекцию
                    var toRemoveProducts = oldCollection.OldProducts.Except(oldCollection.TargetProducts);
                    foreach (var remove in toRemoveProducts)
                    {
                        var deleteDummy = new ProductToCollectionEntity { CollectionId = oldCollection.CollectionId, ProductId = remove };
                        _context.ProductsToCollections.Attach(deleteDummy);
                        _context.Entry(deleteDummy).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    }
                }

                //2. Активируем старые коллекции, в которых теперь достаточно объектов, не выключенные вручную
                var toActivate = oldCollections.Where(x => !x.Available && !x.ManualDisable && x.TargetProducts.Count() > 1);
                foreach (var collection in toActivate)
                    collection.CollectionEntity.Available = true;

                //3. Создаем новые коллекции
                var newCollections = actualCollections.Keys.Except(oldCollections.Select(x => x.Keyword)).Select(x => new ProductCollectionEntity { KeyWord = x, Name = x, CompanyId = companyId, Available = true }).ToArray();
                foreach (var collection in newCollections)
                    _context.ProductCollections.Add(collection);
                await _context.SaveChangesAsync();

                foreach (var collection in newCollections)
                {
                    var products = actualCollections[collection.KeyWord];
                    _context.ProductsToCollections
                        .AddRange(products.
                        Select(x => new ProductToCollectionEntity
                        {
                            CollectionId = collection.Id,
                            ProductId = x.ProductId
                        }));
                }
                await _context.SaveChangesAsync();
            }


            //4. Находим уже несуществуюющие коллекции, выключаем их
            var allCollectionNames = foundCollections.Keys;
            var disableCollections = await _context.ProductCollections.Where(x => x.CompanyId == companyId && x.Available).Where(x => allCollectionNames.All(n => n != x.KeyWord)).ToArrayAsync();
            foreach (var disable in disableCollections)
                disable.Available = false;
        }

        public List<ProductCollection> GetCollectionsForCompany(int companyId)
        {
            return _context.ProductCollections.Where(x => x.CompanyId == companyId)
                .Select(x => new ProductCollection
                {
                    Id = x.Id,
                    Available = x.Available,
                    KeyWord = x.KeyWord,
                    Name = x.Name,
                    Products = x.ToProducts.Select(p => p.Product).Select(p => new ProductName { Name = p.Name, ProductId = p.Id }).ToList()
                }).ToList();
        }

        public List<ProductCollectionInfo> GetCollectionsInfoForComapny(int companyId)
        {
            return _context.ProductCollections.Where(x => x.CompanyId == companyId)
                .Select(x => new ProductCollectionInfo
                {
                    Id = x.Id,
                    Available = x.Available,
                    KeyWord = x.KeyWord,
                    Name = x.Name,
                    ProductCount = x.ToProducts.Count
                }).ToList();
        }

        public void ChangeCollection(int collectionId, string name,  bool isAvalable)
        {
            var collection = _context.ProductCollections.FirstOrDefault(x => x.Id == collectionId);
            if (collection != null)
            {
                collection.Name = name;
                if (collection.Available != isAvalable)
                {
                    collection.Available = isAvalable;
                    collection.ManualDisable = !isAvalable;
                }
                _context.SaveChanges();
            }
        }

        public ProductCollection GetCollection(int collectionId)
        {
            var collection = _context.ProductCollections.Where(x => x.Id == collectionId)
                .Select(x => new ProductCollection
                {
                    Id = x.Id,
                    Available = x.Available,
                    KeyWord = x.KeyWord,
                    Name = x.Name,
                    Products = x.ToProducts.Select(p => p.Product).Select(p => new ProductName { Name = p.Name, ProductId = p.Id }).ToList()
                }).FirstOrDefault();
            if (collection == null)
                throw new ServiceException($"collection with id {collectionId} not found", "collection_not_found");
            return collection;
        }

        public int[] GetCompaniesToUpdate()
        {
            return _context.Companies.Select(x => x.Id).ToArray();
        }
    }
}
