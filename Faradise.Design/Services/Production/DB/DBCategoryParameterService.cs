﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.DAL;
using Faradise.Design.Models.CategoryParameters;
using Faradise.Design.Models.DAL;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Faradise.Design.Services.Production.DB
{
    public class DBCategoryParameterService : IDBCategoryParameterService
    {
        private readonly FaradiseDBContext _context;

        public DBCategoryParameterService(FaradiseDBContext context)
        {
            _context = context;
        }

        public ParameterInfo[] GetAllParameters()
        {
            return _context
                .Parameters
                .Select(entity => new ParameterInfo
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Type = entity.Type,
                    GlobalParameter = entity.GlobalParameter,
                    Synonyms = JsonConvert.DeserializeObject<List<string>>(entity.Synonyms),
                    Categories = entity.Categories.Select(parameterEntity => new CategoryParameterInfo
                    {
                        CategoryId = parameterEntity.CategoryId,
                        CategoryName = parameterEntity.Category.Name
                    }).ToList()
                }).ToArray();
        }

        public void CreateParameter(ParameterInfo parameter)
        {
            var entity = new ParameterEntity
            {
                Name = parameter.Name,
                Type = parameter.Type,
                GlobalParameter = parameter.GlobalParameter,
                Synonyms = JsonConvert.SerializeObject(parameter.Synonyms),
                Categories = parameter.SelectedCategories == null
                    ? new List<CategoryParameterEntity>()
                    : parameter.SelectedCategories
                        .Select(categoryId => new CategoryParameterEntity {CategoryId = categoryId}).ToList()
            };
            _context.Parameters.Add(entity);
            _context.SaveChanges();
        }

        public void DeleteParameter(int id)
        {
            _context.Entry(new ParameterEntity {Id = id}).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public ParameterInfo[] GetParametersByCategoryId(int id)
        {
            return
                _context
                    .Parameters
                    .Where(entity =>
                        entity.Categories.Select(parameterEntity => parameterEntity.CategoryId).Contains(id))
                    .ToArray()
                    .Select(entity => entity.ToModel())
                    .ToArray()
                ;
        }

        public ParameterInfo GetParameter(int id)
        {
            return _context
                .Parameters
                .Find(id)
                .ToModel();
        }

        public void UpdateParameter(ParameterInfo model)
        {
            var existingEntity = _context.Parameters.Find(model.Id);
            var dboModel = model.ToDbo();
            existingEntity.Name = dboModel.Name;
            existingEntity.GlobalParameter = dboModel.GlobalParameter;
            existingEntity.Synonyms = dboModel.Synonyms;
            existingEntity.Type = dboModel.Type;
            _context.CategoryParameters.RemoveRange(existingEntity.Categories);
            _context.SaveChanges();
            if (model.SelectedCategories != null && model.SelectedCategories.Any())
            {
                existingEntity.Categories = model.SelectedCategories.Select(id => new CategoryParameterEntity
                {
                    CategoryId = id,
                    ParameterId = existingEntity.Id
                }).ToList();
                _context.SaveChanges();
            }
        }
    }
}