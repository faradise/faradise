﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Utilities.Feed;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBFeedService : IDBFeedService
    {
        private readonly FaradiseDBContext _context;

        public DBFeedService(FaradiseDBContext db)
        {
            _context = db;
        }

        public async Task<int> ExtractProductsCount()
        {
            return await _context.Products
                .WhereValid()
                .CountAsync();
        }

        public async Task<IEnumerable<FeedProduct>> ExtractFeedProducts(int skip, int take)
        {
            var products = await _context.Products
                .WhereValid()
                .OrderBy(x => x.Id)
                .Skip(skip)
                .Take(take)
                .Select(x => new FeedProduct
                {
                    Id = x.Id,
                    Available = 
                        x.CompanyCategory.CategoryId != null &&
                        x.CompanyCategory.Company.MarketplaceId != null &&
                        x.CompanyCategory.Category.MarketplaceId != null &&
                        !x.IsRemoved &&
                        (
                            x.Available == null ||
                            x.Available.Value
                        ),

                    Price = x.Price,
                    PreviousPrice = x.PreviousPrice ?? x.ActualPrice,

                    CategoryId = x.CompanyCategory.CategoryId ?? 0,

                    Pictures = x.Pictures
                        .Where(y => y.Processed == null || y.InternalUrl != null)
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .ToArray(),

                    Name = x.Name,
                    Description = x.Description,

                    Width = x.Width,
                    Height = x.Height,
                    Length = x.Length,

                    Color = x.ProductColors
                        .OrderBy(y => y.Id)
                        .Select(y => y.CompanyColor)
                        .Where(y => y.MarketplaceColor != null)
                        .Select(y => y.MarketplaceColor.Name)
                        .FirstOrDefault(),

                    Vendor = x.Vendor ?? x.CompanyCategory.Company.Name,
                    VendorCode = x.VendorCode,

                    Warranty = x.Warranty
                })
                .ToArrayAsync();

            return products;
        }

        public FeedProduct[] ExtractFeedProductsByIds(IEnumerable<int> productIds)
        {
            var products = _context.Products
                .Join(productIds, y => y.Id, x => x, (y, x) => y)
                .Select(x => new FeedProduct
                {
                    Id = x.Id,
                    Available =
                        x.CompanyCategory.CategoryId != null &&
                        x.CompanyCategory.Company.MarketplaceId != null &&
                        x.CompanyCategory.Category.MarketplaceId != null &&
                        !x.IsRemoved &&
                        (
                            x.Available == null ||
                            x.Available.Value
                        ),

                    Price = x.Price,
                    PreviousPrice = x.PreviousPrice ?? x.ActualPrice,

                    CategoryId = x.CompanyCategory.CategoryId ?? 0,

                    Pictures = x.Pictures
                        .Where(y => y.Processed == null || y.InternalUrl != null)
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .ToArray(),

                    Name = x.Name,
                    Description = x.Description,

                    Width = x.Width,
                    Height = x.Height,
                    Length = x.Length,

                    Color = x.ProductColors
                        .OrderBy(y => y.Id)
                        .Select(y => y.CompanyColor)
                        .Where(y => y.MarketplaceColor != null)
                        .Select(y => y.MarketplaceColor.Name)
                        .FirstOrDefault(),

                    Vendor = x.Vendor ?? x.CompanyCategory.Company.Name,
                    VendorCode = x.VendorCode,

                    Warranty = x.Warranty
                })
                .ToArray();
            return products;
        }

        public async Task<DateTime> ExtractFeedsUpdateTime()
        {
            return await _context.MarketplaceEntity
                .OrderBy(x => x.Id)
                .Select(x => x.FeedUpdated)
                .FirstAsync();
        }

        public async Task StoreFeedLink(string url, FeedFormatType format, FeedAvailabilityType availability, FeedCostType costType)
        {
            var m = await _context.MarketplaceEntity
                .OrderBy(x => x.Id)
                .Include(x => x.Feeds)
                .FirstOrDefaultAsync();

            var existing = m.Feeds.FirstOrDefault(x => x.Format == format && x.Availability == availability && x.Cost == costType);
            if (existing == null)
            {
                existing = new MarketplaceFeedEntity
                {
                    MarketplaceId = m.Id,
                    Format = format,
                    Availability = availability,
                    Cost = costType
                };

                _context.MarketplaceFeeds.Add(existing);
            }

            existing.Url = url;

            m.FeedUpdated = DateTime.UtcNow;

            await _context.SaveChangesAsync();
        }

        public async Task<string> ExtractFeedLink(FeedFormatType format, FeedAvailabilityType availability, FeedCostType costType)
        {
            var feed = await _context.MarketplaceEntity
                .OrderBy(x => x.Id)
                .SelectMany(x => x.Feeds)
                .FirstOrDefaultAsync(x => x.Format == format && x.Availability == availability && x.Cost == costType);

            return feed?.Url;
        }

        public async Task<int[]> ExtractSkippedCategories(int[] skip)
        {
            var categories = await _context.MarketPlaceCategories
                .Include(x => x.Childs)
                .ToDictionaryAsync(x => x.Id, x => x);

            var skipped = new List<int>();

            ProcessSkip(skip, skipped, categories);

            return skipped.ToArray();
        }

        private static void ProcessSkip(IEnumerable<int> skip, ICollection<int> skipped, IReadOnlyDictionary<int, MarketplaceCategoryEntity> categories)
        {
            foreach (var s in skip)
            {
                skipped.Add(s);

                if (!categories.ContainsKey(s))
                    continue;

                var cat = categories[s];
                if (cat?.Childs == null)
                    continue;

                var childs = categories[s].Childs.Select(x => x.Id);
                ProcessSkip(childs, skipped, categories);
            }
        }
    }
}
