﻿//using System.Linq;
//using System.Threading.Tasks;
//using Faradise.Design.DAL;
//using Faradise.Design.Models.PictureUploads;
//using Microsoft.EntityFrameworkCore;

//namespace Faradise.Design.Services.Production.DB
//{
//    public class DBPictureExtractorQuerryService : IDBPictureExtractorQueryService
//    {
//        private readonly FaradiseDBQueryContext _context;

//        public DBPictureExtractorQuerryService(FaradiseDBQueryContext context)
//        {
//            _context = context;
//        }

//        public async Task<PictureUploadTask[]> GetNextPicturesToUpload(int count)
//        {
//            return await _context.Products
//                .WhereAvailable()
//                .SelectMany(x => x.Pictures)
//                .Where(x => x.InternalUrl == null && x.Url != null)
//                .Select(x => new PictureUploadTask
//                {
//                    PictureId = x.Id,
//                    PhotoUrl = x.Url,
//                    CompanyId = x.Product.CompanyId,
//                    ProductId = x.ProductId
//                })
//                .Take(count)
//                .ToArrayAsync();
//        }
//    }
//}
