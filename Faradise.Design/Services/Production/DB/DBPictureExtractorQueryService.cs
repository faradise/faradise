﻿using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Models.PictureUploads;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBPictureExtractorQueryService : IDBPictureExtractorQueryService
    {
        private readonly FaradiseDBQueryContext _context = null;

        public DBPictureExtractorQueryService(FaradiseDBQueryContext context)
        {
            _context = context;
        }

        public async Task<PictureUploadTask[]> GetNextPicturesToUpload(int count)
        {
            return await _context
                .Products
                .WhereAvailable()
                .SelectMany(x => x.Pictures)
                .Where(x => x.Processed == null)
                .Where(x => x.InternalUrl == null && x.Url != null)
                .Select(x => new PictureUploadTask
                {
                    PictureId = x.Id,
                    PhotoUrl = x.Url,
                    CompanyId = x.Product.CompanyId,
                    ProductId = x.ProductId
                })
                .Take(count)
                .ToArrayAsync();
        }
    }
}
