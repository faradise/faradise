﻿using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using System.Linq;

namespace Faradise.Design.Services.Production.DB
{
    public class DBDesignerPoolService : IDBDesignerPoolService
    {
        private readonly FaradiseDBContext _context;

        public DBDesignerPoolService(FaradiseDBContext db)
        {
            _context = db;
        }

        public void AddDesignerToPool(int projectId, int userId)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException("project_not_found", "project_not_found");

            var designer = _context.Designers.Find(userId);
            if (designer == null)
                throw new ServiceException("designer_not_found", "designer_not_found");

            var poolEntry = new DesignerPoolEntity
            {
                ProjectId = projectId,
                DesignerId = userId,
                Status = DesignerStatusInPool.Appointed
            };

            _context.DesignerPools.Add(poolEntry);
            _context.SaveChanges();
        }

        public int GetDesignerCount(int projectId)
        {
            return _context.DesignerPools.Count(x => x.ProjectId == projectId && x.Status != DesignerStatusInPool.Rejected);
        }

        public int[] GetDesignerPool(int projectId, int maxCount)
        {
            return _context.DesignerPools
                .Where(x => x.ProjectId == projectId && x.Status != DesignerStatusInPool.Rejected)
                .Select(x => x.DesignerId)
                .ToArray();
        }

        public DesignerInfo[] GetProjectAppointedDesigners(int projectId)
        {
            return _context.DesignerPools
                .Where(x => x.ProjectId == projectId)
                .Where(x => x.Status == DesignerStatusInPool.Appointed)
                .Select(x => new DesignerInfo
                {
                    Id = x.Designer.Id,
                    Name = x.Designer.Name,
                    Surname = x.Designer.Surname,
                    AppointedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.DesignerId && d.Status == DesignerStatusInPool.Appointed),
                    RejectedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.DesignerId && d.Status == DesignerStatusInPool.Rejected),
                    ProjectsInWorkCount = x.Designer.ProjectsInWork.Count(),
                    Stage = x.Designer.Stage
                })
                .ToArray();
        }

        public DesignerRejectedInfo[] GetProjectRejectedDesigners(int projectId)
        {
            DesignerRejectedInfo[] designers = _context.DesignerPools
                .Where(x => x.ProjectId == projectId)
                .Where(x => x.Status == DesignerStatusInPool.Rejected)
                .Select(x => new DesignerRejectedInfo
                {
                    Id = x.Designer.Id,
                    Name = x.Designer.Name,
                    Surname = x.Designer.Surname,
                    AppointedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.DesignerId && d.Status == DesignerStatusInPool.Appointed),
                    RejectedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.DesignerId && d.Status == DesignerStatusInPool.Rejected),
                    ProjectsInWorkCount = x.Designer.ProjectsInWork.Count(),
                    Stage = x.Designer.Stage,
                    RejectReason = x.RejectReason
                })
                .ToArray();
            return designers;
        }

        public DesignerInfo[] GetProjectSuitableDesigners(int projectId)
        {
            return _context.Designers
                .Where(x => x.Stage == DesignerRegistrationStage.ReadyToWork)
                .Where(x => !_context.DesignerPools.Any(p => p.DesignerId == x.Id && p.ProjectId == projectId))
                .Select(x => new DesignerInfo
                {
                    Id = x.Id,
                    Name = x.Name,
                    Surname = x.Surname,
                    AppointedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.Id && d.Status == DesignerStatusInPool.Appointed),
                    RejectedProjectsCount = _context.DesignerPools.Count(d => d.DesignerId == x.Id && d.Status == DesignerStatusInPool.Rejected),
                    ProjectsInWorkCount = x.ProjectsInWork.Count(),
                    Stage = x.Stage
                })
                .ToArray();
        }

        public void RemoveDesignerFromPool(int projectId, int userId, bool isRejected = true, string description = null)
        {
            var designerInPool = _context
                .DesignerPools
                .SingleOrDefault(x => x.ProjectId == projectId && x.DesignerId == userId);

            if (designerInPool == null)
                throw new ServiceException("designert_not_in_pool", "designert_not_in_pool");

            if (isRejected)
            {
                designerInPool.Status = DesignerStatusInPool.Rejected;
                designerInPool.RejectReason = description;
            }
            else
                _context.DesignerPools.Remove(designerInPool);

            _context.SaveChanges();
        }
    }
}
