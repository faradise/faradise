﻿using System.Linq;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Marketplace.Xml;
using Faradise.Design.Models.Admin.Company;
using Faradise.Design.Models.DAL;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBAdminCompanyService : IDBAdminCompanyService
    {
        private readonly FaradiseDBContext _context = null;

        public DBAdminCompanyService(FaradiseDBContext context)
        {
            _context = context;
        }

        public AdminCompanyCategory[] ExtractCompanyCategories(int companyId)
        {
            return _context.CompanyCategories
                .AsNoTracking()
                .Where(x => x.CompanyId == companyId)
                .Select(x => new AdminCompanyCategory
                {
                    CompanyCategoryId = x.CompanyCategoryId,
                    ParentCompanyCategoryId = x.ParentCompanyCategoryId ?? 0,

                    Products = x.Products
                        .Where(y => !y.IsRemoved && (y.Available == null || y.Available.Value))
                        .Where(y => y.Price > 0)
                        .Count(y => y.Name != null),

                    CategoryId = x.CategoryId,
                    BadCategoryBinding = x.Category != null && x.Category.Childs.Any(y => y.MarketplaceId != null)
                })
                .ToArray();
        }

        public AdminCompanyCategoryItem ExtractCompanyCategory(int companyId, int categoryId)
        {
            var c = _context.CompanyCategories
                .Where(x => x.CompanyId == companyId && x.CompanyCategoryId == categoryId)
                .Select(x => new
                {
                    x.CompanyId,
                    x.CategoryId,
                    x.Name,
                    Products = x.Products
                        .Where(y => !y.IsRemoved && (y.Available == null || y.Available.Value))
                        .Where(y => y.Price > 0)
                        .Count(y => y.Name != null),
                    Rooms = x.Rooms
                        .Select(y => y.RoomName)
                        .Select(y => new RoomNameModel {RoomNameId = y.Id, Name = y.Name})
                        .ToArray(),
                    x.Parent,

                    BadCategoryBinding = x.Category != null && x.Category.Childs.Any(y => y.MarketplaceId != null)
                })
                .First();

            var depth = 0;
            var parent = c.Parent;

            while (parent != null)
            {
                depth++;
                parent = parent.Parent;
            }

            return new AdminCompanyCategoryItem
            {
                CompanyId = c.CompanyId,
                CategoryId = c.CategoryId ?? 0,
                Name = c.Name,
                Depth = depth,
                Products = c.Products,
                Rooms = c.Rooms,
                BadCategoryBinding = c.BadCategoryBinding
            };
        }
    }
}
