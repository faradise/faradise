﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProductCompilation;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBCompilationService : IDBCompilationService
    {
        private readonly FaradiseDBContext _context;

        public DBCompilationService(FaradiseDBContext context)
        {
            _context = context;
        }

        private int CreateProductCompilation()
        {
            var entity = new ProductCompilationEntity { Name = "p_unnamed_" + Guid.NewGuid() };
            _context.ProductCompilations.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        private int CreateCategoryCompilation()
        {
            var entity = new CategoryCompilationEntity { Name = "c_unnamed_" + Guid.NewGuid() };
            _context.CategoryCompilations.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        private void DeleteCategotyCompilation(int compilationId)
        {
            var entity = _context.CategoryCompilations.Find(compilationId);
            if (entity == null)
                throw new ServiceException("no compilation_category found for id {compilationId}", "categoty_compilation_not_found");
            _context.CategoryCompilations.Remove(entity);
            _context.SaveChanges();
        }

        private void DeleteProductCompilation(int compilationId)
        {
            var entity = _context.ProductCompilations.Find(compilationId);
            if (entity == null)
                throw new ServiceException("no compilation_product found for id {compilationId}", "product_compilation_not_found");
            _context.ProductCompilations.Remove(entity);
            _context.SaveChanges();
        }

        private ProductCompilation GetProductCompilation(int compilationId)
        {
            var compilation = _context.ProductCompilations
                .Where(x => x.Id == compilationId)
                .Select(x => new ProductCompilation
                {
                    Id = x.Id,
                    Name = x.Name,
                    Products = x.CompilationToProducts.OrderBy(c => c.Order).Select(toProduct => toProduct.ProductId).ToArray(),
                    Source = CompilationSource.ProductList
                })
                .FirstOrDefault();

            if (compilation == null)
                throw new ServiceException("no compilation_product found for id {compilationId}",
                    "product_compilation_not_found");

            return compilation;
        }

        private ProductCompilation GetCategoriesCompilation(int compilationId)
        {
            var compilation = _context.CategoryCompilations
                .Where(x => x.Id == compilationId)
                .Select(x => new ProductCompilation
                {
                    Id = x.Id,
                    Name = x.Name,
                    Categories = x.CompilationToCategory.Select(toCategory => toCategory.CategoryId).ToArray(),
                    Source = CompilationSource.Categories
                })
                .FirstOrDefault();

            if (compilation == null)
                throw new ServiceException("no compilation_category found for id {compilationId}",
                    "categoty_compilation_not_found");

            return compilation;
        }

        private bool UpdateCategoryCompilation(ProductCompilation compilation)
        {
            if (compilation == null)
                return false;

            var entity = _context.CategoryCompilations.Find(compilation.Id);
            if (entity == null)
                throw new ServiceException("no compilation_category found for id {compilationId}", "categoty_compilation_not_found");

            var newName = compilation.Name.ToLower();
            if (entity.Name != newName)
            {
                if (_context.ProductCompilations.Any(x => x.Name == newName))
                    throw new ServiceException("name not uniq", "name_not_uniq");
                if (_context.CategoryCompilations.Any(x => x.Name == newName))
                    throw new ServiceException("name not uniq", "name_not_uniq");
                entity.Name = newName;
            }

            var addList = compilation.Categories != null 
                ? compilation.Categories
                    .Distinct()
                    .ToList() 
                : new List<int>();

            foreach (var chain in entity.CompilationToCategory)
            {
                if (addList.Any(x => x == chain.CategoryId))
                {
                    addList.Remove(chain.CategoryId);
                    continue;
                }

                _context.CompilationsToCategories.Remove(chain);
            }

            if (addList.Count > 0)
            {
                foreach (var categoryId in addList)
                {
                    _context.CompilationsToCategories.Add(new CompilationToCategoryEntity
                    {
                        CompilationId = entity.Id,
                        CategoryId = categoryId
                    });
                }
            }

            _context.SaveChanges();

            return true;
        }

        private bool UpdateProductCompilation(ProductCompilation compilation)
        {
            if (compilation == null)
                return false;

            var entity = _context.ProductCompilations
                .Include(x => x.CompilationToProducts)
                .FirstOrDefault(x => x.Id == compilation.Id);

            if (entity == null)
                throw new ServiceException("no compilation_product found for id {compilationId}", "product_compilation_not_found");

            var newName = compilation.Name.ToLower();
            if (entity.Name != newName)
            {
                if (_context.ProductCompilations.Any(x => x.Name == newName))
                    throw new ServiceException("name not uniq", "name_not_uniq");
                if (_context.CategoryCompilations.Any(x => x.Name == newName))
                    throw new ServiceException("name not uniq", "name_not_uniq");
                entity.Name = newName;
            }

            var addList = compilation.Products != null 
                ? compilation.Products
                    .Distinct()
                    .ToList() 
                : new List<int>();

            foreach (var chain in entity.CompilationToProducts)
            {
                if (addList.Any(x => x == chain.ProductId))
                {
                    addList.Remove(chain.ProductId);
                    continue;
                }

                _context.CompilationsToProducts.Remove(chain);
            }

            if (addList.Count > 0)
            {
                foreach (var pId in addList)
                {
                    entity.CompilationToProducts.Add(new CompilationToProductEntity
                    {
                        CompilationId = entity.Id,
                        ProductId = pId
                    });
                }
            }

            if (compilation.Products != null)
            {
                for (var i = 0; i < compilation.Products.Length; i++)
                {
                    var ex = entity.CompilationToProducts.FirstOrDefault(x => x.ProductId == compilation.Products[i]);
                    if (ex != null)
                        ex.Order = i;
                }
            }
            
            _context.SaveChanges();

            return true;
        }

        public int CreateCompilation(CompilationSource source)
        {
            switch (source)
            {
                case CompilationSource.ProductList:
                    return CreateProductCompilation();
                case CompilationSource.Categories:
                    return CreateCategoryCompilation();
                default:
                    return 0;
            }
        }

        public bool UpdateCompilation(ProductCompilation compilation, CompilationSource source)
        {
            switch (source)
            {
                case CompilationSource.ProductList:
                    return UpdateProductCompilation(compilation);
                case CompilationSource.Categories:
                    return UpdateCategoryCompilation(compilation);
                default:
                    return false;
            }
        }

        public void DeleteCompilation(int compilationId, CompilationSource source)
        {
            switch (source)
            {
                case CompilationSource.ProductList:
                    DeleteProductCompilation(compilationId);
                    break;
                case CompilationSource.Categories:
                    DeleteCategotyCompilation(compilationId);
                    break;
            }
        }

        public ProductCompilationShort[] GetCompilations()
        {
            return _context.CategoryCompilations
                .Select(x => new ProductCompilationShort
                {
                    Id = x.Id,
                    Name = x.Name,
                    Source = CompilationSource.Categories
                })
                .Concat(_context.ProductCompilations
                    .Select(x => new ProductCompilationShort
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Source = CompilationSource.ProductList

                    }))
                .ToArray();
        }

        public ProductCompilation GetCompilation(int compilationId, CompilationSource source)
        {
            switch (source)
            {
                case CompilationSource.ProductList:
                    return GetProductCompilation(compilationId);
                case CompilationSource.Categories:
                    return GetCategoriesCompilation(compilationId);
                default:
                    return null;
            }
        }
    }
}
