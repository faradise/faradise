﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Extensions;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.ProjectPreparations;

namespace Faradise.Design.Services.Production.DB
{
    

    public class DBProjectFlowService: IDBProjectFlowService
    {
        private readonly FaradiseDBContext _context;

        public DBProjectFlowService(FaradiseDBContext db)
        {
            _context = db;
        }

        // Устанавливать, а не добавлять
        public void SetAdditionalInformation(List<AdditionalInformationForRoom> information)
        {
            foreach(var room in information)
            {

                var addInfo = room.Questions.Select(x => new ProjectFlowRoomAdditionalInformationEntity()
                {
                    RoomId = room.RoomId,
                    QuestionIndex = x.Index,
                    Answer = x.Answer
                })
                .ToList();
                _context.ProjectFlowRoomAdditionalInformations.AddRange(addInfo);
            }

            _context.SaveChanges();
        }

        public void DeleteRoom(int roomId)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new ServiceException("room_not_found", "roomt_not_found");
            _context.ProjectDescriptionRooms.Remove(room);
            _context.SaveChanges();
        }

        public int AddOldFurnishToSavePhoto(int projectId, string url)
        {
            var oldFurnish = new ProjectFlowOldFurnishPhotoEntity()
            {
                ProjectId = projectId,
                Url = url
            };

            _context.ProjectFlowOldFurnishPhotos.Add(oldFurnish);
            _context.SaveChanges();

            return oldFurnish.Id;
        }

        public int AddOldFurnitureToSavePhoto(int projectId, string url)
        {
            var oldFurniture = new ProjectFlowOldFurniturePhotoEntity()
            {
                ProjectId = projectId,
                Url = url
            };

            _context.ProjectFlowOldFurniturePhotos.Add(oldFurniture);
            _context.SaveChanges();

            return oldFurniture.Id;
        }

        public void SetPlanPhoto(int roomId, string url)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new NotImplementedException();
            room.PlanUrl = url;
            _context.SaveChanges();

        }

        public void SetProjectFlowBudget(int projectId, int furnitureBudget, int renovationBudget)
        {
            var project = _context.ProjectFlowDescriptions.FirstOrDefault(x=>x.ProjectId==projectId);
            if (project == null)
                throw new NotImplementedException();
            project.FurnitureBudget = furnitureBudget;
            project.RenovationBudget = renovationBudget;
            _context.SaveChanges();
        }


        public void SetProjectFlowRoomProportions(Dictionary<int, RoomProportions> proportions)
        {
            foreach(var room in proportions)
            {
                var proportion = _context.ProjectFlowRoomsProportions.FirstOrDefault(x => x.RoomId == room.Key);
                if (proportion == null)
                {
                    proportion = new ProjectFlowRoomProportionsEntity();
                    _context.ProjectFlowRoomsProportions.Add(proportion);
                }
                proportion.RoomId = room.Key;
                proportion.SideA = room.Value.SideA;
                proportion.SideB = room.Value.SideB;
                proportion.HeightDoor = room.Value.HeightDoor;
                proportion.WidthDoor = room.Value.WidthDoor;
                proportion.HeightCeiling = room.Value.HeightCeiling;
                
            }
            _context.SaveChanges();
        }

        public void SetProjectFlowStyles(int projectId, Dictionary<ProjectStyle, string> styles)
        {
            var oldStyles = _context.ProjectFlowStyles.Where(x => x.ProjectId == projectId).ToList();
            _context.ProjectFlowStyles.RemoveRange(oldStyles);
            var selectedStyles = styles.Select(x => new ProjectFlowStyleEntity()
            {
                ProjectId = projectId,
                Style = x.Key,
                Comment = x.Value
            }).ToList();
            _context.ProjectFlowStyles.AddRange(selectedStyles);
            _context.SaveChanges();
        }

        public int AddRoomPhoto(int roomId, string url)
        {
            var photo = new ProjectFlowRoomPhotoEntity()
            {
                RoomId = roomId,
                Url = url
            };
            _context.ProjectFlowRoomPhotos.Add(photo);
            _context.SaveChanges();

            return photo.Id;
        }

        public void DeleteOldFurnishPhoto(int photoId)
        {
            var photo = _context.ProjectFlowOldFurnishPhotos.Find(photoId);
            if (photo == null)
                throw new NotImplementedException();
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public void DeleteOldFurniturePhoto(int photoId)
        {
            var photo = _context.ProjectFlowOldFurniturePhotos.Find(photoId);
            if (photo == null)
                throw new NotImplementedException();
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public void DeletePlan(int roomId)
        {
            var room = _context.ProjectDescriptionRooms.Find(roomId);
            if (room == null)
                throw new NotImplementedException();
            var oldPhoto = new ProjectFlowPlanOldPhotoEntity()
            {
                RoomId = room.Id,
                Url = room.PlanUrl
            };
            _context.ProjectFlowPlanOldPhotos.Add(oldPhoto);
            _context.SaveChanges();
            room.PlanUrl= string.Empty;
            _context.SaveChanges();
        }

        public void DeleteRoomPhoto(int photoId)
        {
            var photo = _context.ProjectFlowRoomPhotos.Find(photoId);
            if (photo == null)
                throw new NotImplementedException();
            photo.IsActive = false;
            _context.SaveChanges();
        }

        public ClientDesignerBudget GetBudget(int projectId)
        {
            var project = _context.ProjectFlowDescriptions.FirstOrDefault(x => x.ProjectId == projectId);
            if (project == null)
                throw new NotImplementedException();
            return new ClientDesignerBudget { FurnitureBudget = project.FurnitureBudget, RenovationBudget = project.RenovationBudget };
        }

        public OldFurniture GetOldFurniture(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new NotImplementedException();
            var furnishPhotos = _context.ProjectFlowOldFurnishPhotos.Where(x => x.ProjectId == projectId && x.IsActive).Select(x=>new Photo { PhotoId = x.Id, Link = x.Url }).ToList();
            var furniturePhotos= _context.ProjectFlowOldFurniturePhotos.Where(x => x.ProjectId == projectId && x.IsActive).Select(x => new Photo { PhotoId = x.Id, Link = x.Url }).ToList();
            return new OldFurniture() { FurnishPhotos = furnishPhotos, FurniturePhotos = furniturePhotos };

        }

        public Dictionary<ProjectStyle, string> GetProjectFlowStyles(int projectId)
        {
            var styles = _context.ProjectFlowStyles.Where(x => x.ProjectId == projectId)
                .Select(x => new KeyValuePair<ProjectStyle, string>(x.Style, x.Comment))
                .ToList();
            return new Dictionary<ProjectStyle, string>(styles);
        }

        public List<AdditionalInformationForRoom> GetRoomAdditionalInfos(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new NotImplementedException();
            var roomsIds = _context.ProjectDescriptionRooms.Where(x=>x.ProjectId==projectId).Select(x => x.Id).ToList();
            var result = new List<AdditionalInformationForRoom>();

            foreach(var roomId in roomsIds)
            {
                var questions = _context.ProjectFlowRoomAdditionalInformations.Where(x => x.RoomId == roomId)
                    .Select(x => new AdditionalQuestion
                    {
                        Index = x.QuestionIndex,
                        Answer = x.Answer
                    })
                    .ToList();
                result.Add(new AdditionalInformationForRoom { RoomId = roomId, Questions = questions });
            }

            return result;
        }

        public List<RoomInfo> GetRooms(int projectId)
        {
            var project = _context.ProjectDescriptions.Find(projectId);
            if (project == null)
                throw new NotImplementedException();
            var rooms = _context.ProjectDescriptionRooms
                .Where(x => x.ProjectId == projectId)
                .Select(x => new RoomInfo()
                {
                    RoomId = x.Id,
                    Purpose = x.Room,
                    Photos = x.Photos.Where(y=>y.IsActive).Select(y => new Photo { PhotoId = y.Id, Link = y.Url }).ToList(),
                    Plan = x.PlanUrl,
                    Proportions = x.Proportions == null ? new RoomProportions() : new RoomProportions
                    {
                        SideA = x.Proportions.SideA,
                        SideB = x.Proportions.SideB,
                        HeightDoor = x.Proportions.HeightDoor,
                        WidthDoor = x.Proportions.WidthDoor,
                        HeightCeiling = x.Proportions.HeightCeiling
                    }
                })
                .ToList();
            return rooms;

        }

        public CooperateStage GoToNextStage(int projectId)
        {
            var project = _context.ProjectFlowDescriptions.FirstOrDefault(x => x.ProjectId == projectId);
            if (project == null)
                throw new NotImplementedException();
            project.Stage = project.Stage.Next();
            project.ClientIsReady = false;
            project.DesignerIsReady = false;
            _context.SaveChanges();
            return project.Stage;
        }

        public StageStatus GetCurrentStage(int projectId)
        {
            var projectFlow = _context.ProjectFlowDescriptions.FirstOrDefault(x => x.ProjectId == projectId);
            if (projectFlow == null)
                throw new NotImplementedException();
            return new StageStatus { Stage = projectFlow.Stage, ClientIsReady = projectFlow.ClientIsReady, DesignerIsReady = projectFlow.DesignerIsReady };
        }

        public void SetClientReady(int projectId)
        {
            var projectFlow = _context.ProjectFlowDescriptions.FirstOrDefault(x => x.ProjectId == projectId);
            if (projectFlow == null)
                throw new NotImplementedException();
            projectFlow.ClientIsReady = true;
            _context.SaveChanges();
        }

        public int CreateProjectFlow(int projectId)
        {
            var project = _context.ProjectDescriptions.FirstOrDefault(x => x.Id == projectId);
            if (project == null)
                throw new NotImplementedException();
            var oldProject = _context.ProjectFlowDescriptions.FirstOrDefault(x => x.ProjectId == projectId);
            if (oldProject != null)
            {
                oldProject.Stage = CooperateStage.Meeting;
                oldProject.DesignerIsReady = false;
                oldProject.ClientIsReady = false;
                _context.SaveChanges();
                return oldProject.Id;
            }
            var projectFlow = new ProjectFlowDescriptionEntity()
            {
                ProjectId = projectId,
                FurnitureBudget = project.FurnitureBudget,
                RenovationBudget = project.RenovationBudget,
                Stage = CooperateStage.Meeting,
                ClientIsReady = false,
                DesignerIsReady = false,
            };
            _context.ProjectFlowDescriptions.Add(projectFlow);

            var flowStyles = project.Styles.Select(x => new ProjectFlowStyleEntity() { ProjectId = projectId, Style = x.Style, Comment = x.Description });
            _context.ProjectFlowStyles.AddRange(flowStyles);

            _context.SaveChanges();
            return projectFlow.Id;
        }

        public bool HasProjectFlow(int projectId)
        {
            return _context.ProjectFlowDescriptions.Any(x => x.ProjectId == projectId);
        }
    }
}
