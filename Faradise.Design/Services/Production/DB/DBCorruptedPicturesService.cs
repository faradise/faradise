﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Controllers.API.Models.CorruptedPictures;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;

namespace Faradise.Design.Services.Production.DB
{
    public class DBCorruptedPicturesService : IDBCorruptedPicturesService
    {
        private readonly FaradiseDBContext _context;

        public DBCorruptedPicturesService(FaradiseDBContext context)
        {
            _context = context;
        }

        public List<CorruptedPartner> GetAllInvalidPartners()
        {
            return
                _context.Products
                    .WhereAvailable()
                    .SelectMany(entity => entity.Pictures)
                    .Where(entity =>
                        entity.Processed.HasValue
                        && string.IsNullOrEmpty(entity.InternalUrl))
                    .Select(entity => new
                    {
                        partnerName = entity.Product.CompanyCategory.Company.Name,
                        partnerId = entity.Product.CompanyCategory.Company.Id,
                        productName = entity.Product.Name,
                        productId = entity.Product.Id,
                        pictureUrl = entity.Url,
                    })
                    .ToList()
                    .GroupBy(data => data.partnerId)
                    .Select(partnerGroup => new CorruptedPartner
                    {
                        PartnerId = partnerGroup.Key,
                        PartnerName = partnerGroup.First().partnerName,
                        CorruptedProducts = partnerGroup
                            .GroupBy(partner => partner.productId)
                            .Select(products =>
                                new CorruptedProduct
                                {
                                    ProductId = products.Key,
                                    ProductName = products.First().productName,
                                    CorruptedPictures = products.Select(data => data.pictureUrl).ToList()
                                }).ToList()
                    }).ToList()
                ;
        }
    }
}