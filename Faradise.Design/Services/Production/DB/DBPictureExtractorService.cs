﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Models.DAL;
using System.Threading.Tasks;
using Faradise.Design.Services.Background;
using Microsoft.EntityFrameworkCore;
using ServiceStack;

namespace Faradise.Design.Services.Production.DB
{
    public class DBPictureExtractorService : IDBPictureExtractorService
    {
        private readonly FaradiseDBContext _context;

        public DBPictureExtractorService(FaradiseDBContext context)
        {
            _context = context;
        }

        public async Task UpdatePicture(int pictureId, string internalUrl)
        {
            var pictureEntity = await _context.ProductsPictures.FindAsync(pictureId);
            if (pictureEntity == null)
                return;

            pictureEntity.InternalUrl = internalUrl;

            await _context.SaveChangesAsync();
        }

        public async Task UpdatePictures(Dictionary<int, PictureUploadBackgroundService.UploadResult> results)
        {
            var keys = results.Keys.ToArray();
            var pictures = await _context.ProductsPictures
                .Where(x => keys.Contains(x.Id))
                .ToArrayAsync();

            foreach (var p in pictures)
            {
                if (!results.ContainsKey(p.Id))
                    continue;

                var r = results[p.Id];
                if (!r.Processed)
                    continue;

                p.InternalUrl = r.Url;
                p.Processed = DateTime.UtcNow;
            }

            await _context.SaveChangesAsync();
        }
    }
}
