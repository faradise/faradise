﻿using System;
using System.Linq;
using System.Reflection;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.DAL;
using Faradise.Design.Extensions;
using Faradise.Design.Models.DAL;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBannerService : IDBBannerService
    {
        private readonly FaradiseDBContext _context;

        public DBBannerService(FaradiseDBContext context)
        {
            _context = context;
        }

        public BannerModel[] GetBanners()
        {
            return _context
                .Banners
                .Select(entity => entity.ToModel())
                .ToArray();
        }

        public int CreateBanner(BannerModel model)
        {
            BannerEntity bannerEntity = model.ToDbo();
            bannerEntity.Updated = DateTime.Now;
            _context.Banners.Add(bannerEntity);
            _context.SaveChanges();
            return bannerEntity.Id;
        }

        public BannerModel GetBanner(int id)
        {
            return _context.Banners
                .Find(id)
                .ToModel();
        }

        public bool UpdateBanner(BannerModel model)
        {
            SetNullables(model);
            var existingEntity = _context.Banners.Find(model.Id);
            _context.Entry(existingEntity).CurrentValues.SetValues(model.ToDbo());
            existingEntity.Updated = DateTime.Now;
            _context.SaveChanges();
            return true;
        }

        private static void SetNullables(BannerModel model)
        {
            Func<PropertyInfo, bool> nullable = info => info.PropertyType.IsGenericType
                                                                    && info.PropertyType.GetGenericTypeDefinition() ==
                                                                    typeof(Nullable<>);

            typeof(BannerModel).GetProperties().Where(nullable).ForEach(info =>
            {
                var val = info.GetValue(model);
                if (val != null && val.Equals(0))
                    info.SetValue(model, null);
            });
        }

        public void ToggleBannerState(int id)
        {
            var existingEntity = _context.Banners.Find(id);
            existingEntity.Active = !existingEntity.Active;
            existingEntity.Updated = DateTime.Now;
            _context.SaveChanges();
        }

        public void DeleteBanner(int id)
        {
            _context.Entry(new BannerEntity {Id = id}).State = EntityState.Deleted;
            _context.SaveChanges();
        }
    }
}