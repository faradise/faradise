﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;

namespace Faradise.Design.Services.Production.DB
{
    public class DBPaymentService : IDBPaymentService
    {
        private readonly FaradiseDBContext _dbContext = null;

        public DBPaymentService(FaradiseDBContext db)
        {
            _dbContext = db;
        }

        public void ClosePayment(int paymentId)
        {
            var entity = _dbContext.Payments.Find(paymentId);
            if (entity == null)
                return;
            entity.Status = PaymentStatus.Canceled;
            _dbContext.SaveChanges();
        }

        public int CreatePayment(Payment payment)
        {
            if (payment == null)
                throw new ServiceException("Cant create null payment in DB", "payment_is_null");
            var entity = new PaymentEntity()
            {
                Amount = payment.Amount,
                Currency = payment.Currency,
                Description = payment.Description,
                IdempotenceKey = payment.IdempotenceKey,
                PaymentRoute = payment.PaymentRoute,
                PaymentTarget = payment.PaymentTarget,
                RawJson = payment.RawJson,
                Status = payment.Status,
                YandexId = payment.YandexId,
                Method = payment.Method,
                PlatformRoute = payment.PlatformRoute,
                Reciept = payment.Reciept
            };
            _dbContext.Payments.Add(entity);
            _dbContext.SaveChanges();
            return entity.Id;
        }

        public Payment GetPayment(int paymentId)
        {
            var entity = _dbContext.Payments.Find(paymentId);
            if (entity == null)
                throw new ServiceException($"Payment with id {paymentId} not found", "payment_not_found");
            return new Payment()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                Currency = entity.Currency,
                Description = entity.Description,
                IdempotenceKey = entity.IdempotenceKey,
                PaymentRoute = entity.PaymentRoute,
                PaymentTarget = entity.PaymentTarget,
                RawJson = entity.RawJson,
                Status = entity.Status,
                YandexId = entity.YandexId,
                Method = entity.Method,
                PlatformRoute = entity.PlatformRoute,
                Reciept = entity.Reciept
            };
        }

        public int[] GetPendingPaymentsIds(PaymentMethod method)
        {
            return _dbContext.Payments.Where(x => x.Method == method && x.Status == PaymentStatus.Pending).Select(x => x.Id).ToArray();
        }

        public Payment SetPaymentStatus(int paymentId, PaymentStatus status)
        {
            var entity = _dbContext.Payments.Find(paymentId);
            if (entity == null)
                throw new ServiceException($"Payment with id {paymentId} not found", "payment_not_found");
            entity.Status = status;
            _dbContext.SaveChanges();
            return new Payment()
            {
                Id = entity.Id,
                Amount = entity.Amount,
                Currency = entity.Currency,
                Description = entity.Description,
                IdempotenceKey = entity.IdempotenceKey,
                PaymentRoute = entity.PaymentRoute,
                PaymentTarget = entity.PaymentTarget,
                RawJson = entity.RawJson,
                Status = entity.Status,
                YandexId = entity.YandexId,
                Method = entity.Method,
                PlatformRoute = entity.PlatformRoute,
                Reciept = entity.Reciept
            };
        }

        public Payment UpdatePayment(Payment payment)
        {
            var entity = _dbContext.Payments.Find(payment.Id);
            if (entity == null)
                throw new ServiceException($"Payment with id {payment.Id} not found", "payment_not_found");
            entity.IdempotenceKey = payment.IdempotenceKey;
            entity.PaymentRoute = payment.PaymentRoute;
            entity.RawJson = payment.RawJson;
            entity.Status = payment.Status;
            entity.YandexId = payment.YandexId;
            _dbContext.SaveChanges();
            return payment;
        }
    }
}
