﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.VisualizationFreelance;

namespace Faradise.Design.Services.Production.DB
{
    public class DBVisualizationFreelanceService : IDBVisualizationFreelanceService
    {
        private readonly FaradiseDBContext _context;

        public DBVisualizationFreelanceService(FaradiseDBContext db)
        {
            _context = db;
        }

        public int AddFile(int projectId, int roomId, string fileLink, string filename)
        {
            var techSpec = _context.VisualizationTechSpecs.Find(roomId);
            if (techSpec == null)
                throw new ServiceException($"Visualization specification not found for project {projectId} room {roomId}", "visualization_techSpec_not_found");
            var entity = new TechSpecFileEntity() { Name = filename, Url = fileLink, TechSpecId = techSpec.RoomId };
            _context.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public void ChangeDesigner(int projectId, int roomId, int designerId)
        {
            var techSpec = _context.VisualizationTechSpecs.Find(roomId);
            if (techSpec == null)
                throw new ServiceException($"Visualization specification not found for project {projectId} room {roomId}", "visualization_techSpec_not_found");
            techSpec.DesignerId = designerId;
            _context.SaveChanges();
        }

        public void CreateVisualizationTechSpec(int projectId, int roomId, int designerId)
        {
            var entity = new VisualizationTechSpecEntity()
            {
                DesignerId = designerId,
                PublishTime = DateTime.MinValue,
                RoomId = roomId,
                Status = VisualizationWorkStatus.Created
            };
            _context.Add(entity);
            _context.SaveChanges();
        }

        public VisualisationTechSpec GetVisualizationTechSpec(int projectId, int roomId)
        {
            var entity = _context.VisualizationTechSpecs.FirstOrDefault(x => x.RoomId == roomId);
            if (entity == null)
                return null;
            return new VisualisationTechSpec()
            {
                Description = entity.Description,
                DesignerId = entity.DesignerId.HasValue ? entity.DesignerId.Value : 0,
                Products = entity.Products != null ? entity.Products.Select(x => new TechSpecProduct() { ProductId = x.ProductId, ProductName = x.Product.Name, OuterUrl = x.Product.Url }).ToArray() : new TechSpecProduct[0],
                PublishTime = entity.PublishTime,
                RoomId = entity.RoomId,
                SpecFiles = entity.Files != null ? entity.Files.Select(x => new TechSpecFile() { Name = x.Name, Link = x.Url, FileId = x.Id }).ToArray() : new TechSpecFile[0],
                Status = entity.Status

            };
        }

        public bool HasVisualizationTechSpec(int projectId, int roomId)
        {
            return _context.VisualizationTechSpecs.Any(x => x.RoomId == roomId);
        }

        public void RemoveFile(int projectId, int roomId, int fileId)
        {
            var file = _context.VisualizationTechSpecFiles.Find(fileId);
            if (file == null)
                return;
            _context.VisualizationTechSpecFiles.Remove(file);
            _context.SaveChanges();
        }

        public void SetDescription(int projectId, int roomId, string description)
        {
            var entity = _context.VisualizationTechSpecs.Find(roomId);
            if (entity == null)
                throw new ServiceException($"Visualization specification not found for project {projectId} room {roomId}", "visualization_techSpec_not_found");
            entity.Description = description;
            _context.SaveChanges();
        }

        public void SetProducts(int projectId, int roomId, int[] products)
        {
            var entity = _context.VisualizationTechSpecs.Find(roomId);
            if (entity.Files != null && entity.Files.Any())
                _context.RemoveRange(entity.Files);
            _context.VisualizationProducts.AddRange(products.
                Select(x => _context.Products.Find(x)).
                Select(x =>
                new VisualizationProductEntity() { ProductId = x.Id, VisualizationTechSpecId = entity.RoomId }));
            _context.SaveChanges();
        }

        public void SetStatus(int projectId, int roomId, VisualizationWorkStatus status)
        {
            var entity = _context.VisualizationTechSpecs.Find(roomId);
            if (entity == null)
                throw new ServiceException($"Visualization specification not found for project {projectId} room {roomId}", "visualization_techSpec_not_found");
            entity.Status = status;
            _context.SaveChanges();
        }
    }
}
