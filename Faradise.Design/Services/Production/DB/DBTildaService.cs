﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Tilda;
using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Tilda;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Services.Production.DB
{
    public class DBTildaService : IDBTildaService
    {
        private readonly FaradiseDBContext _dbContext = null;

        public DBTildaService(FaradiseDBContext context)
        {
            _dbContext = context;
        }

        public async Task<bool> CheckRequest(string phone, string mail, TildaMailType type)
        {
            var existing = await _dbContext.TildaMails.AnyAsync(x => x.Phone == phone && x.Email == mail && x.Type == type);

            return !existing;
        }

        public async Task<int> StoreRequest(string name, string mail, string phone, TildaMailType type)
        {
            var entity = new TildaMailEntity
            {
                Name = name,
                Email = mail,
                Phone = phone,
                Type = type,
                Created = DateTime.UtcNow
            };

            _dbContext.TildaMails.Add(entity);

            await _dbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<TildaLotteryRequest[]> ExtractRequests(TildaMailType type)
        {
            return await _dbContext.TildaMails
                .Where(x => x.Type == type)
                .Select(x => new TildaLotteryRequest
                {
                    Id = x.Id,
                    Email = x.Email,
                    Phone = x.Phone,
                    Name = x.Name,
                    Created = x.Created
                })
                .ToArrayAsync();
        }
    }
}
