﻿using Faradise.Design.DAL;
using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models;
using Faradise.Design.Models.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation
{
    public class DBProjectOrderService : IDBProjectOrderService
    {
        private readonly FaradiseDBContext _context;

        public DBProjectOrderService(FaradiseDBContext db)
        {
            _context = db;
        }

        public int CreateNewOrder(int projectId, OrderDescription description, PaymentMethod method, int deliveryCost)
        {
            var project = _context.ProjectDefinitions.Find(projectId);
            if (project == null)
                throw new ServiceException($"project id {projectId} is not found", "project_not_found");
            var order = new ProjectMarketplaceOrderEntity()
            {
                CreationTime = DateTime.UtcNow,
                ProjectId = projectId,
                UserId = project.ClientId,
                Description = description.Description,
                Email = description.Email,
                Adress = description.Adress,
                City = description.City,
                Name = description.Name,
                Surname = description.Surname,
                Phone = description.Phone,
                SelectedPaymentMethod = method,
                DeliveryPrice = deliveryCost,
                Status = method == PaymentMethod.Tinkoff ? OrderStatus.WaitingForCreditApprove : OrderStatus.Created
            };
            _context.ProjectMarketplaceOrders.Add(order);
            _context.SaveChanges();
            return order.Id;
        }

        public int UpdateOrderBasePrice(int orderId)
        {
            var orderedItems = _context.RoomGoods.Where(x => x.OrderId == orderId).Select(x => x.ProductId);
            int price = 0;
            foreach (var orderItem in orderedItems)
            {
                var product = _context.Products.Find(orderItem);
                if (product != null)
                    price += product.Price;
            }
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.BasePrice = price;

            _context.SaveChanges();
            return order.BasePrice;
        }

        public void SetOrderPayment(int orderId, int paymentId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.PaymentId = paymentId;
            _context.SaveChanges();
        }

        public void AddProductsToOrder(int roomId, int itemId, int count, int orderId)
        {
            for (var i = 0; i < count; i++)
            {
                var item = _context.RoomGoods.FirstOrDefault(x => x.RoomId == roomId && x.ProductId == itemId && !x.PaidFor);
                if (item == null)
                    throw new ServiceException($"there is no unpaid item with id {itemId} not found", "marketplace_item_not_found");
                item.PaidFor = true;
                item.OrderId = orderId;
                _context.SaveChanges();
            };
        }

        public ShortOrderInformation[] GetAllOrdersIdsForProject(int projectId)
        {
            return _context.ProjectMarketplaceOrders.Where(x => x.ProjectId == projectId).Select
                (x => new ShortOrderInformation()
                {
                    CreationTime = x.CreationTime,
                    OrderId = x.Id,
                    BasePrice = x.BasePrice,
                    AdditionalPrice = x.AdditionalPrice,
                    DeliveryPrice = x.DeliveryPrice,
                    SelectedPaymentMethod = x.SelectedPaymentMethod,
                    PaymentId = x.PaymentId,
                    PaymentStatus = x.Payment != null ? x.Payment.Status : PaymentStatus.Pending,
                    OrderStatus = x.Status
                }).ToArray();
        }

        public FullProjectProductsOrder GetOrderInformation(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            return new FullProjectProductsOrder()
            {
                OrderId = order.Id,
                BasePrice = order.BasePrice,
                AdditionalPrice = order.AdditionalPrice,
                DeliveryPrice = order.DeliveryPrice,
                PaymentStatus = order.Payment != null ? order.Payment.Status : PaymentStatus.Pending,
                Status = order.Status,
                PaymentMethod = order.SelectedPaymentMethod,
                PaymentId = order.PaymentId,
                ProjectId = order.ProjectId,
                PaymentLink = order.Payment?.PaymentRoute,
                PlatformLink = order.Payment?.PlatformRoute,
                CreateionTime = order.CreationTime,
                 IsReady = order.IsReady,
                  IsFinished = order.IsFinished,
                OrderDescription = new OrderDescription()
                {
                    Adress = order.Adress,
                    City = order.City,
                    Description = order.Description,
                    Email = order.Email,
                    Name = order.Name,
                    Phone = order.Phone,
                    Surname = order.Surname
                },
                Goods = _context.RoomGoods.Where(x => x.OrderId == orderId).
                            GroupBy(room => room.RoomId).
                            Select(x => new RoomGoods()
                            {
                                RoomId = x.Key,
                                MarketplaceItems = x.GroupBy(productGroup => productGroup.ProductId).Select(item => new MarketplaceItem()
                                {
                                    ProductId = item.Key,
                                    Count = item.Count(),
                                    PaidFor = true
                                }).ToList(),
                                OutsideItems = new List<OutsideItem>()
                            }).ToArray()
            };
        }

        public void RemoveGoodsFromOrder(int orderId, RoomGoods goodsToRemove)
        {
            foreach (var goods in goodsToRemove.MarketplaceItems)
            {
                for (var i = 0; i < goods.Count; i++)
                {
                    var entity = _context.RoomGoods.FirstOrDefault(x => x.RoomId == goodsToRemove.RoomId && x.ProductId == goods.ProductId && x.OrderId == orderId);
                    if (entity != null)
                    {
                        entity.OrderId = null;
                        entity.PaidFor = false;
                    }
                    _context.SaveChanges();
                }
            }
        }

        public void CloseOrder(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.IsFinished = true;
            order.Status = order.SelectedPaymentMethod == PaymentMethod.Tinkoff ? OrderStatus.CanceledCredit : OrderStatus.Canceled;
            _context.SaveChanges();
        }

        public void SetOrderAdditionalPrice(int orderId, int price)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.AdditionalPrice = price;
            _context.SaveChanges();
        }

        public void SetOrderDeliveryPrice(int orderId, int price)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.DeliveryPrice = price;
            _context.SaveChanges();
        }

        public void SetOrderStatus(int orderId, OrderStatus status)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.Status = status;
            _context.SaveChanges();
        }

        public void SetOrderIsReady(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null)
                throw new ServiceException($"Order id {orderId} not found", "order_not_found");
            order.IsReady = true;
            _context.SaveChanges();
        }

        public void ClearPayment(int orderId)
        {
            var order = _context.ProjectMarketplaceOrders.Find(orderId);
            if (order == null || order.PaymentId == null)
                return;
            order.PaymentId = null;
            _context.SaveChanges();
        }
    }
}
