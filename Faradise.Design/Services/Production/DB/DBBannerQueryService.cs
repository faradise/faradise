﻿using System.Linq;
using Faradise.Design.Controllers.API.DataMappers;
using Faradise.Design.Controllers.API.Models.Banner;
using Faradise.Design.DAL;

namespace Faradise.Design.Services.Production.DB
{
    public class DBBannerQueryService : IDBBannerQueryService
    {
        private readonly FaradiseDBQueryContext _context;

        public DBBannerQueryService(FaradiseDBQueryContext context)
        {
            _context = context;
        }

        public Banner GetBanner(int id)
        {
            return _context
                .Banners
                .Find(id)
                .ToExternal();
        }

        public Banner[] GetAllActiveBanners()
        {
            return _context
                .Banners
                .Where(entity => entity.Active)
                .Select(entity => entity.ToExternal())
                .ToArray();
        }
    }
}