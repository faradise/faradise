﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBPaymentService
    {
        int CreatePayment(Payment payment);
        Payment GetPayment(int paymentId);
        Payment SetPaymentStatus(int paymentId, PaymentStatus status);
        int[] GetPendingPaymentsIds(PaymentMethod method);
        Payment UpdatePayment(Payment payment);
        void ClosePayment(int paymentId);
    }
}
