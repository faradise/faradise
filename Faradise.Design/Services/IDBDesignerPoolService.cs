﻿using Faradise.Design.Controllers.API;
using Faradise.Design.Controllers.API.Models.Admin;
using Faradise.Design.Models.DesignerPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBDesignerPoolService
    {
        int[] GetDesignerPool(int projectId, int maxCount);
        void AddDesignerToPool(int projectId, int userId);
        void RemoveDesignerFromPool(int projectId, int userId, bool isRejected, string description);
        int GetDesignerCount(int projectId);
        DesignerInfo[] GetProjectSuitableDesigners(int projectId);
        DesignerRejectedInfo[] GetProjectRejectedDesigners(int projectId);
        DesignerInfo[] GetProjectAppointedDesigners(int projectId);
    }
}
