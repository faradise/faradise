﻿using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Tilda;
using Faradise.Design.Models.Tilda;

namespace Faradise.Design.Services
{
    public interface IDBTildaService
    {
        Task<bool> CheckRequest(string phone, string mail, TildaMailType type);
        Task<int> StoreRequest(string name, string mail, string phone, TildaMailType type);
        Task<TildaLotteryRequest[]> ExtractRequests(TildaMailType type);
    }
}
