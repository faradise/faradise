﻿using Faradise.Design.Models;
using Faradise.Design.Models.VisualizationFreelance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services
{
    public interface IDBVisualizationFreelanceService
    {
        VisualisationTechSpec GetVisualizationTechSpec(int projectId, int roomId);
        bool HasVisualizationTechSpec(int projectId, int roomId);
        void CreateVisualizationTechSpec(int projectId, int roomId, int designerId);
        void SetDescription(int projectId, int roomId, string description);
        int AddFile(int projectId, int roomId, string fileLink, string filename);
        void RemoveFile(int projectId, int roomId, int fileId);
        void ChangeDesigner(int projectId, int roomId, int designerId);
        void SetStatus(int projectId, int roomId, VisualizationWorkStatus status);
        void SetProducts(int projectId, int roomId, int[] products);
    }
}
