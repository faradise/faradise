﻿using System;

namespace Faradise.Design
{
    public static class FaradiseEnvironment
    {
        private static string Deployment { get { return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"); } }

        public static bool Development { get { return Deployment.Equals("develop", StringComparison.OrdinalIgnoreCase); } }
        public static bool Stable { get { return Deployment.Equals("stable", StringComparison.OrdinalIgnoreCase); } }
        public static bool Staging { get { return Deployment.Equals("staging", StringComparison.OrdinalIgnoreCase); } }
        public static bool Master { get { return Deployment.Equals("master", StringComparison.OrdinalIgnoreCase); } }
        public static bool Integration { get { return Deployment.Equals("integration", StringComparison.OrdinalIgnoreCase); } }
    }
}
