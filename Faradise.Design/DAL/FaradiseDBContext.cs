﻿using Faradise.Design.DAL;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.Models.DAL
{
    public class FaradiseDBContext : DbContext
    {
        public FaradiseDBContext(DbContextOptions<FaradiseDBContext> options)
            : base(options)
        { }

        protected FaradiseDBContext(DbContextOptions options)
            : base(options) { }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<UserAliasEntity> Aliases { get; set; }
        public DbSet<VerificationCodeEntity> VerificationCodes { get; set; }

        public DbSet<ProjectDefinitionEntity> ProjectDefinitions { get; set; }
        public DbSet<ProjectDescriptionEntity> ProjectDescriptions { get; set; }
        public DbSet<DesignerRequirementsEntity> DesignerRequirements { get; set; }
        public DbSet<ProjectDescriptionReasonEntity> ProjectDescriptionsReasons { get; set; }
        public DbSet<ProjectDescriptionStyleEntity> ProjectDescriptionStyles { get; set; }
        public DbSet<ProjectDescriptionRoomEntity> ProjectDescriptionRooms { get; set; }

        public DbSet<TestResultsAnswerEntity> TestResultAnswers { get; set; }
        public DbSet<TestAnswerDescriptionEntity> TestAnswerDescriptions { get; set; }
        public DbSet<TestQuestionDescriptionEntity> TestQuestionDescriptions { get; set; }

        public DbSet<ProjectFlowDescriptionEntity> ProjectFlowDescriptions { get; set; }
        public DbSet<ProjectFlowOldFurnishPhotoEntity> ProjectFlowOldFurnishPhotos { get; set; }
        public DbSet<ProjectFlowOldFurniturePhotoEntity> ProjectFlowOldFurniturePhotos { get; set; }
        public DbSet<ProjectFlowRoomAdditionalInformationEntity> ProjectFlowRoomAdditionalInformations { get; set; }
        public DbSet<ProjectFlowRoomPhotoEntity> ProjectFlowRoomPhotos { get; set; }
        public DbSet<ProjectFlowRoomProportionsEntity> ProjectFlowRoomsProportions { get; set; }
        public DbSet<ProjectFlowStyleEntity> ProjectFlowStyles { get; set; }
        public DbSet<ProjectFlowPlanOldPhotoEntity> ProjectFlowPlanOldPhotos { get; set; }

        public DbSet<DesignerEntity> Designers { get; set; }
        public DbSet<DesignerCompanyLegalInfoEntity> DesignerCompanyLegalInfos { get; set; }
        public DbSet<DesignerPersonalLegalInfoEntity> DesignerPersonalLegalInfos { get; set; }
        public DbSet<DesignerPortfolioProjectEntity> DesignerPortfolioProjects { get; set; }
        public DbSet<DesignerPortfolioProjectPhotoEntity> DesignerPortfolioProjectPhotos { get; set; }
        public DbSet<DesignerPossibilityEntity> DesignerPossibilities { get; set; }
        public DbSet<DesignerStyleEntity> DesignerStyles { get; set; }
        public DbSet<DesignerTestResultsAnswerEntity> DesignerTestResultsAnswers { get; set; }
        public DbSet<DesignerTestJobInfoEntity> DesignerTestJobInfos { get; set; }
        public DbSet<DesignerPoolEntity> DesignerPools { get; set; }

        public DbSet<MarketplaceCategoryEntity> MarketPlaceCategories { get; set; }
        public DbSet<CompanyEntity> Companies { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<CompanyCategoryEntity> CompanyCategories { get; set; }
        public DbSet<ProductPictureEntity> ProductsPictures { get; set; }
        public DbSet<RoomNameEntity> RoomNames { get; set; }
        public DbSet<CompanyCategoryRoomEntity> CompanyCategoryRooms { get; set; }
        public DbSet<MarketplaceColorEntity> MarketplaceColors { get; set; }
        public DbSet<CompanyColorEntity>CompanyColors { get; set; }
        public DbSet<ProductColorEntity> ProductColors { get; set; }

        public DbSet<RoomCollagePhotoEntity>RoomCollagePhotos { get; set; }
        public DbSet<RoomGoodsEntity> RoomGoods { get; set; }
        public DbSet<RoomMoodboardPhotoEntity> RoomMoodboardPhotos { get; set; }
        public DbSet<RoomPlanEntity>RoomPlans { get; set; }
        public DbSet<RoomPlanPhotoEntity>RoomPlanPhotos { get; set; }
        public DbSet<RoomVisualizationEntity> RoomVisualizations { get; set; }
        public DbSet<RoomVisualizationPhotoEntity> RoomVisualizationPhotos { get; set; }
        public DbSet<RoomZonePhotoEntity> RoomZonePhotos { get; set; }
        public DbSet<ProjectDevelopmentEntity> ProjectDevelopments { get; set; }
        public DbSet<ProjectDevelopmentStageDataEntity> ProjectDevelopmentStageData { get; set; }
        public DbSet<CategoryImageEntity> CategoryImages { get; set; }

        public DbSet<VisualizationTechSpecEntity> VisualizationTechSpecs { get; set; }
        public DbSet<TechSpecFileEntity> VisualizationTechSpecFiles { get; set; }
        public DbSet<VisualizationProductEntity> VisualizationProducts { get; set; }

        public DbSet<MessageGroupEntity> ChatGroups { get; set; }
        public DbSet<ChatMessageEntity> ChatMessages { get; set; }
        public DbSet<ChatMessagePhotoEntity>ChatMessagePhotos { get; set; }
        public DbSet<ChatGroupUserEntity>ChatUsers { get; set; }

        public DbSet<PaymentEntity> Payments { get; set; }
        public DbSet<ProjectMarketplaceOrderEntity> ProjectMarketplaceOrders { get; set; }

        public DbSet<BrigadeTechSpecEntity>BrigadeTechSpecs { get; set; }
        public DbSet<BrigadeTechSpecFileEntity>BrigadeTechSpecFiles { get; set; }

        public DbSet<MarketplaceOrderEntity> MarketplaceOrders { get; set; }
        public DbSet<MarketplaceOrderProductEntity> MarketplaceOrderProducts { get; set; }
        public DbSet<BasketProductEntity> BasketProducts { get; set; }
        public DbSet<SavedBasketEntity> SavedBaskets { get; set; }
        public DbSet<ViewSavedBasketEntity> ViewsSavedBaskets { get; set; }

        public DbSet<YMLUpdateTaskEntity> YmlUpdates { get; set; }

        public DbSet<SaleEntity> Sales { get; set; }
        public DbSet<BadgeEntity> Badges { get; set; }

        public DbSet<MarketplaceEntity> MarketplaceEntity { get; set; }
        public DbSet<MarketplaceFeedEntity> MarketplaceFeeds { get; set; }

        public DbSet<ProductCompilationEntity> ProductCompilations { get; set; }
        public DbSet<CategoryCompilationEntity> CategoryCompilations { get; set; }

        public DbSet<CompilationToProductEntity> CompilationsToProducts { get; set; }
        public DbSet<CompilationToCategoryEntity> CompilationsToCategories { get; set; }

        public DbSet<ARProductEntity> ARProducts { get; set; }
        public DbSet<ARModelEntity> ARModels { get; set; }

        public DbSet<BatchEntity> Batches { get; set; }

        public DbSet<DailyProductEntity> DailyProducts { get; set; }

        public DbSet<ArchiveUpdateTaskEntity> ArchiveUpdateTasks { get; set; }

        public DbSet<PromoCodeExportEntity> PromoCodeExports { get; set; }
        public DbSet<PromoCodeEntity> PromoCodes { get; set; }

        public DbSet<TildaMailEntity> TildaMails { get; set; }
        public DbSet<BannerEntity> Banners { get; set; }
        public DbSet<ParameterEntity> Parameters { get; set; }
        public DbSet<CategoryParameterEntity> CategoryParameters { get; set; }

        public DbSet<TolokaTaskEntity> TolokaTasks { get; set; }
        public DbSet<TolokaTaskSegmentEntity> TolokaTaskSegments { get; set; }

        public DbSet<ProductCollectionEntity> ProductCollections { get; set; }
        public DbSet<ProductToCollectionEntity> ProductsToCollections { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BatchEntity>()
                .Property(e => e.Id).UseSqlServerIdentityColumn();

            modelBuilder.Entity<TestResultsAnswerEntity>()
                .HasKey(c => new { c.ProjectId, c.QuestionId });

            modelBuilder.Entity<DesignerEntity>()
                .HasMany(x => x.Pools)
                .WithOne(x => x.Designer)
                .HasForeignKey(x => x.DesignerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyCategoryEntity>()
                .HasKey(x => new { x.CompanyId, x.CompanyCategoryId });

            modelBuilder.Entity<CompanyCategoryEntity>()
                .HasOne(x => x.Company)
                .WithMany(x => x.CompanyCategories)
                .HasForeignKey(x => x.CompanyId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProjectMarketplaceOrderEntity>()
                .HasOne(x => x.Payment)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.PaymentId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ProductEntity>()
                .HasOne(x => x.CompanyCategory)
                .WithMany(x => x.Products)
                .HasForeignKey(x => new { x.CompanyId, x.CompanyCategoryId })
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CompanyCategoryEntity>()
                .HasOne(x => x.Category)
                .WithMany(x => x.CompanyCategories)
                .HasForeignKey(x => x.CategoryId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<CompanyCategoryEntity>()
                .HasOne(x => x.Parent)
                .WithMany(x => x.Childs)
                .HasForeignKey(x => new { x.CompanyId, x.ParentCompanyCategoryId })
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<MarketplaceCategoryEntity>()
                .HasOne(x => x.Parent)
                .WithMany(x => x.Childs)
                .HasForeignKey(x => x.ParentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CompanyEntity>()
                .HasOne(x => x.Marketplace)
                .WithMany(x => x.Companies)
                .HasForeignKey(x => x.MarketplaceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<MarketplaceCategoryEntity>()
                .HasOne(x => x.Marketplace)
                .WithMany(x => x.Categories)
                .HasForeignKey(x => x.MarketplaceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ProductColorEntity>()
                .HasOne(x => x.Product)
                .WithMany(x => x.ProductColors)
                .HasForeignKey(x => x.ProductId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductColorEntity>()
                .HasOne(x => x.CompanyColor)
                .WithMany(x => x.ProductColors)
                .HasForeignKey(x => x.CompanyColorId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CompanyColorEntity>()
                .HasOne(x => x.MarketplaceColor)
                .WithMany(x => x.CompanyColors)
                .HasForeignKey(x => x.MarketplaceColorId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);


            modelBuilder.Entity<CompilationToProductEntity>()
                .HasKey(x => new { x.CompilationId, x.ProductId });

            modelBuilder.Entity<CompilationToProductEntity>()
               .HasOne(x => x.Compilation)
               .WithMany(x => x.CompilationToProducts)
               .HasForeignKey(x => x.CompilationId)
               .IsRequired(true)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CompilationToProductEntity>()
                .HasOne(x => x.Product)
                .WithMany(x => x.CompilationsToProduct)
                .HasForeignKey(x => x.ProductId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CompilationToCategoryEntity>()
                .HasKey(x => new { x.CompilationId, x.CategoryId });

            modelBuilder.Entity<CompilationToCategoryEntity>()
                .HasOne(x => x.Compilation)
                .WithMany(x => x.CompilationToCategory)
                .HasForeignKey(x => x.CompilationId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CompilationToCategoryEntity>()
                .HasOne(x => x.Category)
                .WithMany(x => x.CompilationsToCategory)
                .HasForeignKey(x => x.CategoryId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductEntity>()
                .HasOne(x => x.Sale)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.SaleId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ProductEntity>()
                .HasOne(x => x.Badge)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.BadgeId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ARProductEntity>()
                .HasOne(x => x.Product)
                .WithOne(x => x.ARProduct)
                .HasForeignKey<ARProductEntity>(x => x.ProductId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ARModelEntity>()
                .HasOne(x => x.ARProduct)
                .WithMany(x => x.ARModels)
                .HasForeignKey(x => x.ARProductId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<MarketplaceFeedEntity>()
                .HasOne(x => x.Marketplace)
                .WithMany(x => x.Feeds)
                .HasForeignKey(x => x.MarketplaceId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DailyProductEntity>()
               .HasOne(x => x.Product)
               .WithMany(x => x.DailyProducts)
               .HasForeignKey(x => x.ProductId)
               .IsRequired(true)
               .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<TolokaTaskSegmentEntity>()
                .HasOne(x => x.Task)
                .WithMany(x => x.Segments)
                .HasForeignKey(x => x.TaskId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<PromoCodeEntity>()
                .HasOne(x => x.Export)
                .WithMany(x => x.Codes)
                .HasForeignKey(x => x.ExportId)
                .IsRequired(true);

            modelBuilder.Entity<CategoryParameterEntity>()
                .HasKey(bc => new { bc.ParameterId, bc.CategoryId });

            modelBuilder.Entity<CategoryParameterEntity>()
                .HasOne(bc => bc.Parameter)
                .WithMany(b => b.Categories)
                .HasForeignKey(bc => bc.ParameterId);

            modelBuilder.Entity<CategoryParameterEntity>()
                .HasOne(bc => bc.Category)
                .WithMany(c => c.Parameters)
                .HasForeignKey(bc => bc.CategoryId);

            modelBuilder.Entity<ProductCollectionEntity>()
                .HasOne(x => x.Company)
                .WithMany(x => x.ProductCollections)
                .HasForeignKey(x => x.CompanyId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ProductToCollectionEntity>()
                .HasKey(x => new { x.CollectionId, x.ProductId });

            modelBuilder.Entity<ProductToCollectionEntity>()
                .HasOne(x => x.Product)
                .WithMany(x => x.ToCollections)
                .HasForeignKey(x => x.ProductId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ProductToCollectionEntity>()
                .HasOne(x => x.Collection)
                .WithMany(x => x.ToProducts)
                .HasForeignKey(x => x.CollectionId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }
    }
}
