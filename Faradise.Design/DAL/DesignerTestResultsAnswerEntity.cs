﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerTestResultsAnswerEntity
    {
        public int Id { get; set; }
        public int DesignerId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }

        [ForeignKey("QuestionId")]
        public virtual TestQuestionDescriptionEntity Question { get; set; }

        [ForeignKey("AnswerId")]
        public virtual TestAnswerDescriptionEntity Answer { get; set; }
    }
}
