﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Utilities.Expander;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Faradise.Design.DAL
{
    public class ProductEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public string CompanyProductId { get; set; }

        public bool IsRemoved { get; set; } = false;

        public string Description { get; set; }

        public int Price { get; set; }
        public int? PreviousPrice { get; set; }
        public int? ActualPrice { get; set; }

        public int? Rating { get; set; }

        public string Url { get; set; }
        public bool? Available { get; set; }

        public bool? Pickup { get; set; }
        public bool? Delivery { get; set; }
        public int? DeliveryCost { get; set; }
        public string DeliveryDays { get; set; }

        public string Vendor { get; set; }
        public string VendorCode { get; set; }
        public bool? Warranty { get; set; }

        public string Origin { get; set; }

        public string Size { get; set; }

        public float? Width { get; set; }
        public float? Height { get; set; }
        public float? Length { get; set; }

        public ProductModerationStatus? ModerationStatus { get; set; }

        public string JSONNotes { get; set; } = string.Empty;

        public int? BadgeId { get; set; }
        public int? SaleId { get; set; }


        public DateTime? Processed { get; set; }

        public virtual CompanyCategoryEntity CompanyCategory { get; set; }

        public virtual ICollection<ProductColorEntity> ProductColors { get; set; }
        public virtual ICollection<ProductPictureEntity> Pictures { get; set; }

        public virtual ICollection<CompilationToProductEntity> CompilationsToProduct { get; set; }
        public virtual ICollection<ProductToCollectionEntity> ToCollections { get; set; }

        public virtual ICollection<DailyProductEntity> DailyProducts { get; set; }

        public virtual SaleEntity Sale { get; set; }
        public virtual BadgeEntity Badge { get; set; }

        public virtual ARProductEntity ARProduct { get; set; }

        public DateTime LastUpdatedDate { get; set; } = DateTime.UtcNow;

        private static Expression<Func<ProductEntity, ProductInfo>> AsProductInfoExpression
        {
            get
            {
                return x => new ProductInfo
                {
                    Id = x.Id,
                    Badge = x.Badge != null ? x.Badge.Picture : null,
                    BadgeDescription = x.Badge != null ? x.Badge.Description : null,
                    SaleDescription = x.Sale != null ? x.Sale.Description : null,
                    Name = x.Name,
                    Description = x.Description,

                    Price = x.Price,
                    PreviousPrice = x.PreviousPrice ?? x.ActualPrice,

                    PhotoUrl = x.Pictures
                        .Where(y => y.InternalUrl != null)
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .Take(2)
                        .ToArray(),

                    Seller = x.CompanyCategory.Company.Name,

                    Vendor = x.Vendor ?? x.CompanyCategory.Company.Name,
                    VendorCode = x.VendorCode,

                    ARSupport = x.ARProduct != null
                };
            }
        }

        private static Expression<Func<ProductEntity, Product>> AsProductExpression
        {
            get
            {
                return x => new Product
                {
                    Id = x.Id,

                    Badge = x.Badge != null ? x.Badge.Picture : null,
                    BadgeDescription = x.Badge != null ? x.Badge.Description : null,
                    SaleDescription = x.Sale != null ? x.Sale.Description : null,
                    Name = x.Name,
                    Description = x.Description,

                    CompanyId = x.CompanyId,
                    CompanyCategoryId = x.CompanyCategoryId,
                    CompanyProductId = x.CompanyProductId,
                    CategoryId = x.CompanyCategory.CategoryId ?? 0,

                    Price = x.Price,
                    PreviousPrice = x.PreviousPrice ?? x.ActualPrice,

                    Url = x.Url,

                    Pictures = x.Pictures
                        .Where(y => y.InternalUrl != null)
                        .OrderByDescending(y => y.Priority)
                        .Select(y => y.InternalUrl ?? y.Url)
                        .ToArray(),

                    Available =
                        x.CompanyCategory.CategoryId != null &&
                        x.CompanyCategory.Company.MarketplaceId != null &&
                        x.CompanyCategory.Category.MarketplaceId != null &&
                        !x.IsRemoved &&
                        (
                            x.Available == null ||
                            x.Available.Value
                        ),

                    Pickup = x.Pickup,
                    Delivery = x.Delivery,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,

                    Seller = x.CompanyCategory.Company.Name,
                    Vendor = x.Vendor ?? x.CompanyCategory.Company.Name,
                    VendorCode = x.VendorCode,

                    Warranty = x.Warranty,

                    Color = x.ProductColors
                        .Select(y => y.CompanyColor.Name)
                        .ToArray(),

                    Origin = x.Origin,
                    Size = x.Size,

                    Width = x.Width,
                    Height = x.Height,
                    Length = x.Length,

                    Notes = x.JSONNotes != null ? JsonConvert.DeserializeObject<string[]>(x.JSONNotes) : new string[0],

                    CategoryDeliveryCost =
                        x.CompanyCategory.Category != null ? x.CompanyCategory.Category.DeliveryCost : 0,
                    CategoryDeliveryDays =
                        x.CompanyCategory.Category != null ? x.CompanyCategory.Category.DeliveryDays : 0
                };
            }
        }

        private static Expression<Func<ProductEntity, bool>> FilterAvailableExpression
        {
            get
            {
                return x => x.CompanyCategory.CategoryId != null
                            && x.CompanyCategory.Category.MarketplaceId != null
                            && x.CompanyCategory.Company.MarketplaceId != null
                            && !x.IsRemoved
                            && (x.Available == null || x.Available == true)
                            && x.Price > 0
                            && !string.IsNullOrEmpty(x.Name)
                            && (x.Delivery == null || x.Delivery == true);
            }
        }

        private static Expression<Func<ProductEntity, ProductMeasurements, bool>> MeasurementsValidExpression
            =>
                (product, measurements) =>
                    measurements == null ||
                    ((measurements.MinHeight == null || product.Height >= measurements.MinHeight)
                     && (measurements.MaxHeight == null || product.Height <= measurements.MaxHeight)
                     && (measurements.MinLength == null || product.Length >= measurements.MinLength)
                     && (measurements.MaxLength == null || product.Length <= measurements.MaxLength)
                     && (measurements.MinWidth == null || product.Width >= measurements.MinWidth)
                     && (measurements.MaxWidth == null || product.Width <= measurements.MaxWidth));


        private static Expression<Func<ProductEntity, bool>> FilterValidExpression
        {
            get
            {
                return x => x.CompanyCategory.CategoryId != null
                            && x.Price > 0
                            && !string.IsNullOrEmpty(x.Name)
                            && (x.Delivery == null || x.Delivery == true);
            }
        }

        private static Expression<Func<ProductEntity, bool>> FilterPicturesExpression
        {
            get { return x => x.Pictures.Any(y => y.Processed.HasValue && !string.IsNullOrEmpty(y.InternalUrl)); }
        }

        [ReplaceWithExpression(PropertyName = nameof(AsProductInfoExpression))]
        public static ProductInfo AsProductInfo(ProductEntity entity)
        {
            return AsProductInfoExpression.Compile().Invoke(entity);
        }

        [ReplaceWithExpression(PropertyName = nameof(AsProductExpression))]
        public static Product AsProduct(ProductEntity entity)
        {
            return AsProductExpression.Compile().Invoke(entity);
        }

        [ReplaceWithExpression(PropertyName = nameof(FilterAvailableExpression))]
        public static bool FilterAvailable(ProductEntity entity)
        {
            return FilterAvailableExpression.Compile().Invoke(entity);
        }

        [ReplaceWithExpression(PropertyName = nameof(MeasurementsValidExpression))]
        public static bool MeasurementsValid(ProductEntity entity, ProductMeasurements measurements)
        {
            return MeasurementsValidExpression.Compile().Invoke(entity, measurements);
        }

        [ReplaceWithExpression(PropertyName = nameof(FilterValidExpression))]
        public static bool FilterValid(ProductEntity entity)
        {
            return FilterValidExpression.Compile().Invoke(entity);
        }

        [ReplaceWithExpression(PropertyName = nameof(FilterPicturesExpression))]
        public static bool FilterPictures(ProductEntity entity)
        {
            return FilterPicturesExpression.Compile().Invoke(entity);
        }
    }

    public static class ProductEntityExtensions
    {
        public static IQueryable<ProductEntity> WhereValid(this IQueryable<ProductEntity> products)
        {
            return products
                .AsExpandable()
                .Where(x => ProductEntity.FilterValid(x));
        }

        public static IQueryable<ProductEntity> ApplyMeasurementFilter(this IQueryable<ProductEntity> products, ProductMeasurements filter)
            => products
                .AsExpandable()
                .Where(product => ProductEntity.MeasurementsValid(product, filter));

        public static IQueryable<ProductEntity> WhereAvailable(this IQueryable<ProductEntity> products)
        {
            return products
                .AsExpandable()
                .Where(x => ProductEntity.FilterAvailable(x));
        }

        public static IQueryable<ProductEntity> WhereSaleable(this IQueryable<ProductEntity> products)
        {
            return products
                .AsExpandable()
                .Where(x => ProductEntity.FilterAvailable(x))
                .Where(x => ProductEntity.FilterValid(x));
        }

        public static IQueryable<ProductEntity> WhereHavePictures(this IQueryable<ProductEntity> products)
        {
            if (FaradiseEnvironment.Integration)
                return products;

            return products
                .AsExpandable()
                .Where(x => ProductEntity.FilterPictures(x));
        }

        public static IQueryable<Product> SelectProduct(this IQueryable<ProductEntity> products)
        {
            return products
                .AsExpandable()
                .Select(x => ProductEntity.AsProduct(x));
        }

        public static IQueryable<ProductInfo> SelectProductInfo(this IQueryable<ProductEntity> products, ARPlatform platform, ARFormat format)
        {
            return products
                .AsExpandable()
                .Select(x => ProductEntity.AsProductInfo(x));
        }

        public static IQueryable<ProductEntity> ApplySaleFilter(this IQueryable<ProductEntity> source, bool sales)
        {
            return sales
                ? source.Where(x => x.PreviousPrice.Value > x.Price || x.ActualPrice.Value > x.Price)
                : source;
        }

        public static IQueryable<ProductEntity> ApplyPriceFilters(this IQueryable<ProductEntity> source, int? from, int? to)
        {
            if (from.HasValue)
                source = source.Where(x => x.Price >= from);
            if (to.HasValue)
                source = source.Where(x => x.Price <= to);

            return source;
        }

        public static IQueryable<ProductEntity> ApplyARFilter(this IQueryable<ProductEntity> source, bool ar, ARPlatform platform, ARFormat format)
        {
            if (!ar)
                return source;
            return source.Where(x => x.ARProduct != null &&
                                     x.ARProduct.ARModels.Any(y => y.Platform == platform &&
                                                                   y.Format == format));
        }

        public static IQueryable<ProductEntity> ApplyColorFilter(this IQueryable<ProductEntity> source, int[] colors)
        {
            if (colors == null || !colors.Any())
                return source;

            return source.Where(x =>
                x.ProductColors.Select(y => y.CompanyColor).Any(y => colors.Contains(y.MarketplaceColorId ?? 0)));
        }

        public static IQueryable<ProductEntity> ApplySearchFilter(this IQueryable<ProductEntity> source, string freetext)
        {
            if (string.IsNullOrEmpty(freetext))
                return source;

            return source.Where(x =>
                EF.Functions.FreeText(x.Name, freetext) ||
                EF.Functions.FreeText(x.Description, freetext));
        }

        public static IQueryable<ProductEntity> ApplyCategoryFilter(this IQueryable<ProductEntity> source, int[] categories)
        {
            if (categories == null || !categories.Any())
                return source;
            return source.Where(x => categories.Contains(x.CompanyCategory.CategoryId ?? 0));
        }

        public static IQueryable<ProductEntity> ApplyBrandFilter(this IQueryable<ProductEntity> source, int[] brands)
        {
            if (brands == null || !brands.Any())
                return source;
            return source.Where(x => brands.Contains(x.CompanyId));
        }

        public static IOrderedQueryable<ProductEntity> OrderBy(this IQueryable<ProductEntity> source, FilterOrdering ordering, ARPlatform platform, ARFormat format)
        {
            switch (ordering)
            {
                case FilterOrdering.AR:
                    return source
                        .OrderByDescending(x => x.ARProduct.ARModels.Count())
                        .ThenBy(x => x.Price);
                case FilterOrdering.Ascending:
                    return source.OrderBy(x => x.Price);
                case FilterOrdering.Descending:
                    return source.OrderByDescending(x => x.Price);
                case FilterOrdering.Popularity:
                    return source.OrderByDescending(x => x.Rating);
                case FilterOrdering.Sale:
                    return source.OrderBy(x => x.Price * 100 / (x.PreviousPrice ?? x.ActualPrice ?? 1));
                default: return source.OrderByDescending(x => x.Id);
            }
        }
    }
}