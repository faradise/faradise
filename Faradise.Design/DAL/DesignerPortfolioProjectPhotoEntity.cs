﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerPortfolioProjectPhotoEntity
    {
        public int Id { get; set; }
        public int PortfolioProjectId { get; set; }
        public string Url { get; set; }

        [ForeignKey("PortfolioProjectId")]
        public virtual DesignerPortfolioProjectEntity Portfolio { get; set; }
        

    }
}
