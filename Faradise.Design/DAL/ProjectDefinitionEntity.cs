﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDefinition;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class ProjectDefinitionEntity
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? DesignerId { get; set; }
        public bool DesignerConfirmed { get; set; } = false;
        public string Name { get; set; }
        public string City { get; set; }
        public ProjectStage Stage { get; set; }
        public ProjectStatus Status { get; set; }
        public ProjectStage? AbandonStage { get; set; }
        public bool IsProjectFill { get; set; }
        public DateTime? ProjectFillDate { get; set; }
        public bool NeedAdmin { get; set; }
        public bool HasChangesAfterLastAdminCheck { get; set; }
        public virtual ProjectDescriptionEntity Description { get; set; }
        public virtual ICollection<DesignerPoolEntity> DesignerPool { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.MinValue;
        public bool Archived { get; set; }

        [ForeignKey("ClientId")]
        public virtual UserEntity User { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }
    }
}
