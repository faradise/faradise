﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectFlowRoomAdditionalInformationEntity
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int QuestionIndex { get; set; }
        public string Answer { get; set; }

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }
    }
}
