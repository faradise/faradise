﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class BrigadeTechSpecFileEntity
    {
        public int Id { get; set; }
        public int BrigadeTechSpecId { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }

        [ForeignKey("BrigadeTechSpecId")]
        public virtual BrigadeTechSpecEntity TechSpec { get; set; }
    }
}
