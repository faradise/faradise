﻿using Faradise.Design.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class ProjectDescriptionRoomEntity
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public Room Room { get; set; }
        public string PlanUrl { get; set; }
        public string CollagePhotoArchiveUrl { get; set; }
        public string MoodboardPhotoArchiveUrl { get; set; }
        public string ZonePhotoArchiveUrl { get; set; }
        public virtual RoomPlanEntity Plan { get; set; }
        public virtual RoomVisualizationEntity Visualization {get;set;}
        public virtual ICollection<RoomCollagePhotoEntity> CollagesPhotos { get; set; }
        public virtual ICollection<RoomMoodboardPhotoEntity>MoodboardsPhotos { get; set; }
        public virtual ICollection<RoomZonePhotoEntity>ZonePhotos { get; set; }


        [ForeignKey("ProjectId")]
        public virtual ProjectDescriptionEntity Project { get; set; }

        public virtual ICollection<ProjectFlowRoomPhotoEntity> Photos { get; set; }
        public virtual ProjectFlowRoomProportionsEntity Proportions { get; set; }
    }
}
