﻿using System.Collections.Generic;
using System.Linq;
using Faradise.Design.Models.DAL;

namespace Faradise.Design.DAL.Extensions
{
    public static class FaradiseDBContextExtensions
    {
        public static int[] GetSuitableCategories(this FaradiseDBContext context, int parent)
        {
            return context.GetSuitableCategories(new[] { parent });
        }

        public static int[] GetSuitableCategories(this FaradiseDBContext context, int[] parents)
        {
            if (parents == null)
                return null;

            if (parents.Length == 1 && parents[0] == 0)
                return null;

            var categories = context.MarketPlaceCategories
                .Select(x => new CategoryParentInfo
                {
                    Key = x.Id,
                    Parent = x.ParentId ?? 0
                })
                .ToArray();

            var childs = new List<int>();

            foreach (var c in parents)
                ProcessChilds(c, childs, categories);

            return childs.ToArray();
        }

        private static void ProcessChilds(int category, ICollection<int> result, CategoryParentInfo[] categories)
        {
            result.Add(category);

            var next = categories.Where(x => x.Parent == category);
            foreach (var c in next)
                ProcessChilds(c.Key, result, categories);
        }

        private class CategoryParentInfo
        {
            public int Key { get; set; }
            public int Parent { get; set; }
        }
    }
}
