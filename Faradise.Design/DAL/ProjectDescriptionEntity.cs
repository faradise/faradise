﻿using Faradise.Design.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class ProjectDescriptionEntity
    {
        [Key]
        public int Id { get; set; }
        public int FurnitureBudget { get; set; }
        public int RenovationBudget { get; set; }

        public ProjectTargetType ObjectType { get; set; }


        public virtual ICollection<TestResultsAnswerEntity> TestAnswers { get; set; }

        public virtual ICollection<ProjectDescriptionRoomEntity> Rooms { get; set; }

        public virtual ICollection<ProjectDescriptionStyleEntity> Styles { get; set; }
        
        public virtual ICollection<ProjectDescriptionReasonEntity> Reasons { get; set; }

        public virtual DesignerRequirementsEntity DesignerRequirements { get; set; }

        [ForeignKey("Id")]
        public virtual ProjectDefinitionEntity Definition { get; set; }
    }
}