﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class TolokaTaskEntity
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public TolokaOutputType Type { get; set; }
        public TolokaTaskStatus Status { get; set; }
        public DateTime Created { get; set; }
        public string ReportUrl { get; set; }
        
        public virtual ICollection<TolokaTaskSegmentEntity> Segments { get; set; } 
    }
}
