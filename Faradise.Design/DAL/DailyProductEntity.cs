﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DailyProductEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public DateTime StartDate { get; set; }

        public virtual ProductEntity Product { get; set; }
    }
}
