﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class ProductPictureEntity
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string InternalUrl { get; set; }
        public int ProductId { get; set; }
        public int Priority { get; set; } = 0;

        public DateTime? Processed { get; set; }

        [ForeignKey("ProductId")]
        public virtual ProductEntity Product { get; set; }
    }
}
