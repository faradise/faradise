﻿namespace Faradise.Design.DAL
{
    public class ProductColorEntity
    {
        public int Id { get; set; }

        public int CompanyColorId { get; set; }
        public int ProductId { get; set; }

		public bool? IsModerated { get; set; }
        public virtual ProductEntity Product { get; set; }
        public virtual CompanyColorEntity CompanyColor { get; set; }
    }
}
