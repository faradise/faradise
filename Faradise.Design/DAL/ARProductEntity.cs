﻿using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class ARProductEntity
    {
        public int Id { get; set; }

        public int FaradiseProductId { get; set; }
        public int FaradiseColorId { get; set; }

        public int? ProductId { get; set; }

        public string Name { get; set; }
        public string Color { get; set; }
        public string VendorCode { get; set; }

        public bool Vertical { get; set; }

        public string Render { get; set; }

        public virtual ProductEntity Product { get; set; }
        public virtual ICollection<ARModelEntity> ARModels { get; set; }
    }
}
