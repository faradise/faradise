﻿using System;
using Faradise.Design.Models.Tilda;

namespace Faradise.Design.DAL
{
    public class TildaMailEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public TildaMailType Type { get; set; }

        public DateTime Created { get; set; }
    }
}
