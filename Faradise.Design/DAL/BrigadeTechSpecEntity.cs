﻿using Faradise.Design.Models;
using Faradise.Design.Models.Brigade;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class BrigadeTechSpecEntity
    {
        [Key]
        public int ProjectId {get;set;}
        public int DesignerId { get; set; }
        public string Description { get; set; }
        public DateTime PublishTime { get; set; }
        public BrigadeWorkStatus Status { get; set; }
        public virtual ICollection<BrigadeTechSpecFileEntity> SpecFiles { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDevelopmentEntity Project { get; set; }
    }
}
