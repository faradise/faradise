﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomMoodboardPhotoEntity
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; } = true;

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }
    }
}
