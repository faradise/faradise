﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class PromoCodeEntity
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime UseDate { get; set; }
        public bool Used { get; set; }
        public int ExportId { get; set; }

        public virtual PromoCodeExportEntity Export { get; set; }
    }
}
