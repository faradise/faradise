﻿using Faradise.Design.Models.DAL;
using Microsoft.EntityFrameworkCore;

namespace Faradise.Design.DAL
{
    public class FaradiseDBQueryContext : FaradiseDBContext
    {
        public FaradiseDBQueryContext(DbContextOptions<FaradiseDBQueryContext> options)
            : base(options)
        { }
    }
}
