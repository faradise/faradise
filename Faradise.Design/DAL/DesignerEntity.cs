﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public Gender Gender { get; set; }
        public string About { get; set; }
        public WorkExperience WorkExperience { get; set; }
        public string PortfolioLink { get; set; }
        public string TestJobLink { get; set; }
        public bool TestApproved { get; set; }
        public string PhotoLink { get; set; }
        public DesignerLevel Level { get; set; }
        public LegalType LegalType { get; set; }
        public DesignerRegistrationStage Stage { get; set; }

        public virtual ICollection<DesignerPortfolioProjectEntity> Projects { get; set; }
        public virtual ICollection<DesignerStyleEntity> Styles { get; set; }
        public virtual ICollection<DesignerTestResultsAnswerEntity> TestResults { get; set; }
        public virtual DesignerCompanyLegalInfoEntity CompanyLegalInfo { get; set; }
        public virtual DesignerPersonalLegalInfoEntity PersonalLegalInfo { get; set; }
        public virtual DesignerPossibilityEntity Possibilities { get; set; }
        public virtual DesignerTestJobInfoEntity TestJobInfo { get; set; }
        public virtual ICollection<ProjectDefinitionEntity> ProjectsInWork { get; set; }
        public virtual ICollection<DesignerPoolEntity> Pools { get; set; }

        [ForeignKey("Id")]
        public virtual UserEntity User { get; set; }
        
    }
}
