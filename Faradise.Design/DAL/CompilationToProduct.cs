﻿namespace Faradise.Design.DAL
{
    public class CompilationToProductEntity
    {
        public int CompilationId { get; set; }
        public int ProductId { get; set; }
        public int Order { get; set; }

        public virtual ProductCompilationEntity Compilation { get; set; }
        public virtual ProductEntity Product { get; set; }
    }
}
