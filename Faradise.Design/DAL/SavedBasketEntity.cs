﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class SavedBasketEntity
    {
        public int Id { get; set; }
        public int UserOwnerId { get; set; }
        public string JSONProductsIds { get; set; }
        public virtual ICollection<ViewSavedBasketEntity>Views { get; set; }
    }
}
