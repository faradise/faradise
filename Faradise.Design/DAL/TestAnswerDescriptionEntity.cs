﻿using Faradise.Design.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class TestAnswerDescriptionEntity
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public TestType For { get; set; }

    }
}
