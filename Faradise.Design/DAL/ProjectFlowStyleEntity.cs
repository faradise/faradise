﻿using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectFlowStyleEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public ProjectStyle Style { get; set; }
        public string Comment { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDefinitionEntity Project { get; set; }
    }
}
