﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProductToCollectionEntity
    {
        public int CollectionId { get; set; }
        public int ProductId { get; set; }

        public virtual ProductEntity Product { get; set; }
        public virtual ProductCollectionEntity Collection { get; set; }
    }
}
