﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class MarketplaceOrderProductEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        public int Count { get; set; }
        public int PromoCodeDiscount { get; set; }
        public bool SupportPromoCode { get; set; }
        
        [ForeignKey("ProductId")]
        public virtual ProductEntity Product { get; set; }

        [ForeignKey("OrderId")]
        public virtual MarketplaceOrderEntity Order { get; set; }
    }
}
