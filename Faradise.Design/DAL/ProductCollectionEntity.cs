﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProductCollectionEntity
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string KeyWord { get; set; }
        public bool Available { get; set; }
        public bool ManualDisable { get; set; }

        public virtual ICollection<ProductToCollectionEntity> ToProducts { get; set; }
        public virtual CompanyEntity Company { get;set;}
    }
}
