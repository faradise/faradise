﻿using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class MarketplaceColorEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Hex { get; set; }

        public virtual ICollection<CompanyColorEntity> CompanyColors { get; set; }
    }
}
