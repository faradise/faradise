﻿using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectDevelopmentEntity
    {
        [Key]
        public int Id { get; set; }
        public DevelopmentStage CurrentStage { get; set; }
        public virtual ICollection<ProjectDevelopmentStageDataEntity> ProjectDevelopmentStageData { get; set; }

        [ForeignKey("Id")]
        public virtual ProjectDefinitionEntity ProjectDefinition { get; set; }
    }
}
