﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomPlanPhotoEntity
    {
        public int Id { get; set; }
        public int PlanId { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; } = true;

        [ForeignKey("PlanId")]
        public virtual RoomPlanEntity Plan { get; set; }
    }
}
