﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomNameEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
