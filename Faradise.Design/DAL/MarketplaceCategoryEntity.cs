﻿ using Faradise.Design.Models.Marketplace;
using Faradise.Design.Utilities.Expander;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Faradise.Design.DAL
{
    public class MarketplaceCategoryEntity
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        
        public int? MarketplaceId { get; set; }

        public string Name { get; set; }
        public bool IsPopular { get; set; }
        public bool InHeader { get; set; }
        public string ImageUrl { get; set; }

        public int DeliveryDays { get; set; }
        public int DeliveryCost { get; set; }

        public int Priority { get; set; }
        public int DailyProductPriority { get; set; }

        public virtual MarketplaceCategoryEntity Parent { get; set; }

        public virtual MarketplaceEntity Marketplace { get; set; }

        public virtual ICollection<MarketplaceCategoryEntity> Childs { get; set; }

        public virtual ICollection<CompanyCategoryEntity> CompanyCategories { get; set; }
        public virtual ICollection<CompilationToCategoryEntity> CompilationsToCategory { get; set; }
        public virtual ICollection<CategoryParameterEntity> Parameters { get; set; }

        private static Expression<Func<MarketplaceCategoryEntity, Category>> AsCategoryExpression
        {
            get
            {
                return x => new Category
                {
                    Id = x.Id,
                    ParentId = x.ParentId ?? 0,
                    Name = x.Name,
                    InHeader = x.InHeader,
                    IsPopular = x.IsPopular,
                    ImageUrl = x.ImageUrl,
                    DeliveryCost = x.DeliveryCost,
                    DeliveryDays = x.DeliveryDays,
                    Priority = x.Priority,
                    IsEnabled = x.MarketplaceId != null ? true : false,
                    DailyProductPriority = x.DailyProductPriority
                };
            }
        }

        [ReplaceWithExpression(PropertyName = nameof(AsCategoryExpression))]
        public static Category AsCategory(MarketplaceCategoryEntity entity)
        {
            return AsCategoryExpression.Compile().Invoke(entity);
        }
    }
}
