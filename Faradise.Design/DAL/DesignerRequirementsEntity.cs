﻿using Faradise.Design.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class DesignerRequirementsEntity
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public Gender Gender { get; set; }

        public int AgeFrom { get; set; }
        public int AgeTo { get; set; }

        public bool PersonalMeetingRequired { get; set; }
        public string PersonalMeetingAddress { get; set; }

        public bool PersonalControlRequired { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDescriptionEntity Project { get; set; }
    }
}
