﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class MessageGroupEntity
    {
        [Key]
        public int GroupId { get; set; }
        public string LastServerNotificationTime { get; set; } = JsonConvert.SerializeObject(new Dictionary<int, DateTime>());
        public bool NotifyDesigner { get; set; } = true;
        public bool NotifyClient { get; set; } = true;

        public virtual ICollection<ChatMessageEntity> Messages { get; set; }

        [ForeignKey("GroupId")]
        public virtual ProjectDefinitionEntity Project { get; set; }
    }
}
