﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerPoolEntity
    {
        public int Id { get; set; }
        public int DesignerId { get; set; } = 0;
        public int ProjectId { get; set; }
        public DesignerStatusInPool Status { get; set; }
        public string RejectReason { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDefinitionEntity Project { get; set; }
    }
}
