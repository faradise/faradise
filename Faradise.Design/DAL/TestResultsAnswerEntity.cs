﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class TestResultsAnswerEntity
    {
        public int ProjectId { get; set; }

        public int QuestionId { get; set; }
        public int AnswerId { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDescriptionEntity ProjectDescription { get; set; }

        [ForeignKey("QuestionId")]
        public virtual TestQuestionDescriptionEntity Question { get; set; }

        [ForeignKey("AnswerId")]
        public virtual TestAnswerDescriptionEntity Answer { get; set; }
    }
}
