﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomGoodsEntity
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int ProductId { get; set; }
        public bool PaidFor { get; set; }
        public int? OrderId { get; set; }

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }

        [ForeignKey("OrderId")]
        public virtual ProjectMarketplaceOrderEntity Order { get; set; }
    }
}
