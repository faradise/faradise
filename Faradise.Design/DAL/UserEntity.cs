﻿using Faradise.Design.Models;
using System;

namespace Faradise.Design.DAL
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
        public string Phone { get; internal set; }
        public DateTime CreationDate { get; set; } = DateTime.MinValue;
        public bool Archived { get; set; } 
    }
}
