﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class CompanyColorEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? MarketplaceColorId { get; set; }

        public virtual MarketplaceColorEntity MarketplaceColor { get; set; }
        public virtual ICollection<ProductColorEntity> ProductColors { get; set; }
    }
}
