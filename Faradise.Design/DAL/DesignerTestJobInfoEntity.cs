﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerTestJobInfoEntity
    {
        [Key]
        public int DesignerId { get; set; }
        public string TestLink { get; set; }
        public DateTime TestTime { get; set; }
        public bool TestApproved { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }
    }
}
