﻿using Faradise.Design.Models;
using Faradise.Design.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.DAL
{
    public class PaymentEntity
    {
        [Key]
        public int Id { get; set; }
        public string IdempotenceKey { get; set; }
        public string YandexId { get; set; }
        public string RawJson { get; set; }
        public string PaymentRoute { get; set; }
        public PaymentTargetType PaymentTarget { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public PaymentStatus Status { get; set; }
        public PaymentMethod Method { get; set; }
        public string PlatformRoute { get; set; }
        public string Reciept { get; set; }
        public virtual ICollection<ProjectMarketplaceOrderEntity> Orders { get; set; }
    }
}
