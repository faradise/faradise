﻿using Faradise.Design.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class ProjectDescriptionReasonEntity
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public ProjectReason Reason { get; set; }
        public string Description { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDescriptionEntity Project { get; set; }
    }
}
