﻿using System;

namespace Faradise.Design.Models.DAL
{
    public class VerificationCodeEntity
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Code { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.MinValue;
        public DateTime Expires { get; set; }
    }
}