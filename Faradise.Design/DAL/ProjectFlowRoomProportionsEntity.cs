﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectFlowRoomProportionsEntity
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public int SideA { get; set; }
        public int SideB { get; set; }
        public int HeightDoor { get; set; }
        public int WidthDoor { get; set; }
        public int HeightCeiling { get; set; }

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }
    }
}
