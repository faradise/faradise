﻿using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectDevelopmentStageDataEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public DevelopmentStage Stage { get; set; }
        public bool DesignerIsReady { get; set; }
        public bool ClientIsReady { get; set; }
        public bool UserMadeDecision { get; set; }
        public DateTime StartTime { get; set; }
        public int TotalLength { get; set; }

        [ForeignKey("ProjectId")]
        public virtual ProjectDevelopmentEntity ProjectDevelopment { get; set; }
    }
}
