﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.DAL
{
    public class CompanyCategoryEntity
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }

        public int? ParentCompanyCategoryId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<CompanyCategoryEntity> Childs { get; set; }

        public int? CategoryId { get; set; }

        [ForeignKey("CompanyId,ParentCompanyCategoryId")]
        public virtual CompanyCategoryEntity Parent { get; set; }=null;

        public virtual CompanyEntity Company { get; set; }
        public virtual MarketplaceCategoryEntity Category { get; set; }

        public virtual ICollection<ProductEntity> Products { get; set; }
        public virtual ICollection<CompanyCategoryRoomEntity>Rooms { get; set; }
    }
}