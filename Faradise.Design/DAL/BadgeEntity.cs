﻿using System;
using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class BadgeEntity
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleting { get; set; }
        public DateTime Updated { get; set; }
        public string UpdateJson { get; set; }

        public string Description { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }

        public virtual ICollection<ProductEntity> Products { get; set; }
   }
}
