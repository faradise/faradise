﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class CompanyCategoryRoomEntity
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public int RoomNameId { get; set; }

        [ForeignKey("CompanyId, CompanyCategoryId")]
        public virtual CompanyCategoryEntity CompanyCategory { get; set; }

        [ForeignKey("RoomNameId")]
        public virtual RoomNameEntity RoomName {get;set;}
    }
}
