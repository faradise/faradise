﻿using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectFlowDescriptionEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int FurnitureBudget { get; set; }
        public int RenovationBudget { get; set; }
        public CooperateStage Stage { get; set; }
        public bool ClientIsReady { get; set; } = false;
        public bool DesignerIsReady { get; set; } = false;


        [ForeignKey("ProjectId")]
        public virtual ProjectDefinitionEntity Project { get; set; }
    }
}
