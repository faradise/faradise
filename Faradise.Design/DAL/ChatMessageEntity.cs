﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ChatMessageEntity
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string Type { get; set; }
        public int? SenderId { get; set; }
        public string Text { get; set; }
        public DateTime TimeCreate { get; set; }

        [ForeignKey("SenderId")]
        public virtual UserEntity User { get; set; }

        [ForeignKey("GroupId")]
        public virtual MessageGroupEntity MessageGroup { get; set; }


    }
}
