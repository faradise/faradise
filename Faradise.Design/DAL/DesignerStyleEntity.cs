﻿using Faradise.Design.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerStyleEntity
    {
        public int Id { get; set; }
        public int DesignerId { get; set; }
        public ProjectStyle Name { get; set; }
        public string Description { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }
    }
}
