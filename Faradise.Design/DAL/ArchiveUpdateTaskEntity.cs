﻿using Faradise.Design.Models.Enums;

namespace Faradise.Design.DAL
{
    public class ArchiveUpdateTaskEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public RoomArchiveType Type { get; set; }
    }
}
