﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerPossibilityEntity
    {
        [Key]
        public int DesignerId { get; set; }
        public bool GoToPlaceInYourCity { get; set; }
        public bool GoToPlaceInAnotherCity { get; set; }
        public bool PersonalСontrol { get; set; }
        public bool PersonalMeeting { get; set; }
        public bool SkypeMeeting { get; set; }
        public bool RoomMeasurements { get; set; }
        public bool Blueprints { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }
    }
}
