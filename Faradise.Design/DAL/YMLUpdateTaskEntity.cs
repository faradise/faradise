﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class YMLUpdateTaskEntity
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string YMLLink { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime StatusChangeTime { get; set; }
        public int Priority { get; set; }
        public YMLUpdateTaskStatus Status { get; set; }
        public string FailMessage { get; set; }
        public UploadFiletype Filetype { get; set; }
        public YMLDownloadSource DownloadSource { get; set; }
    }
}
