﻿using Faradise.Design.DAL;
using System.ComponentModel.DataAnnotations.Schema;

namespace Faradise.Design.Models.DAL
{
    public class UserAliasEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Value { get; set; }
        public AliasType AliasType { get; set; }

        [ForeignKey("UserId")]
        public virtual UserEntity User { get; set; }
    }
}
