﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class CompanyEntity
    {
        public int Id { get; set; }
        public int? MarketplaceId { get; set; }

        public string Name { get; set; } = String.Empty;
        public string Url { get; set; } = String.Empty;
        public string Email { get; set; } = String.Empty;

        public int CompanyProductionMask { get; set; } = 0;

        public string LegalName { get; set; } = String.Empty;
        public string ContactPersonName { get; set; } = String.Empty;
        public string ContactPersonPhone { get; set; } = String.Empty;
        public string ContactPersonEmail { get; set; } = String.Empty;
        public string DeliveryInfo { get; set; } = String.Empty;
        public string NotesJson { get; set; } = String.Empty;
        public string FeedUrl { get; set; } = String.Empty;
        public bool EnableAutofeed { get; set; } = false;
        public DateTime AutofeedComplitionTime { get; set; }
        public bool DontSupportPromocode { get; set; }

        public int DailyProductPriority { get; set; }

        public virtual ICollection <CompanyCategoryEntity> CompanyCategories { get; set; }
        public virtual MarketplaceEntity Marketplace { get; set; }
        public virtual ICollection<ProductCollectionEntity> ProductCollections { get; set; }
    }
}