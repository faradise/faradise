﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class CompilationToCategoryEntity
    {
        public int CompilationId { get; set; }
        public int CategoryId { get; set; }

        public virtual CategoryCompilationEntity Compilation { get; set; }
        public virtual MarketplaceCategoryEntity Category { get; set; }
    }
}
