﻿using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class ProductCompilationEntity
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;

        public virtual ICollection<CompilationToProductEntity> CompilationToProducts { get; set; }
    }
}
