﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class TechSpecFileEntity
    {
        public int Id { get; set; }
        public int TechSpecId { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }

        [ForeignKey("TechSpecId")]
        public virtual VisualizationTechSpecEntity TechSpec { get; set; }
    }
}
