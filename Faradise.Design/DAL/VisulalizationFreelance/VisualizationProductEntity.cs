﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class VisualizationProductEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int VisualizationTechSpecId { get; set; }

        [ForeignKey("VisualizationTechSpecId")]
        public virtual VisualizationTechSpecEntity VisualizationTechSpec { get; set; }
        [ForeignKey("ProductId")]
        public virtual ProductEntity Product { get; set; }
    }
}
