﻿using Faradise.Design.Models;
using Faradise.Design.Models.VisualizationFreelance;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class VisualizationTechSpecEntity
    {
        [Key]
        public int RoomId { get; set; }
        public int? DesignerId { get; set; }
        public string Description { get; set; }
        public DateTime PublishTime { get; set; }
        public VisualizationWorkStatus Status { get; set; }


        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }

        public virtual ICollection<TechSpecFileEntity> Files { get; set; }
        public virtual ICollection<VisualizationProductEntity> Products { get; set; }
    }
}
