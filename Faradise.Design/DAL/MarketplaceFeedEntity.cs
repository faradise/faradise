﻿using Faradise.Design.Utilities.Feed;

namespace Faradise.Design.DAL
{
    public class MarketplaceFeedEntity
    {
        public int Id { get; set; }
        public int MarketplaceId { get; set; }

        public FeedFormatType Format { get; set; }
        public FeedAvailabilityType Availability { get; set; }
        public FeedCostType Cost { get; set; }

        public string Url { get; set; }

        public virtual MarketplaceEntity Marketplace { get; set; }
    }
}
