﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Faradise.Design.Models.Enums;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Faradise.Design.DAL
{
    public class ParameterEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Synonyms { get; set; }
        public bool GlobalParameter { get; set; }
        public ParameterType Type { get; set; }
        public virtual ICollection<CategoryParameterEntity> Categories { get; set; }
    }
}