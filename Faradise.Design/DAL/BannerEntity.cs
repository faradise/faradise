﻿using System;
using Faradise.Design.Controllers.API.Models.Admin.Marketpace;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.DAL
{
    public class BannerEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PictureWeb { get; set; }
        public string PictureMobile { get; set; }
        public string PictureMobileBackground { get; set; }

        //1
        public string CompilationName { get; set; }

        //2
        public int? ProductId { get; set; }

        //3
        public int? CategoryId { get; set; }
        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }
        public int? BrandId { get; set; }
        public FilterOrdering? Ordering { get; set; }
        public bool? DiscountFilter { get; set; }

        //4
        public string Url { get; set; }


        public DateTime Updated { get; set; }
        public bool Active { get; set; }

        public int Order { get; set; }
    }
}