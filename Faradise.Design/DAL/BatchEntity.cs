﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class BatchEntity
    {
        [Key]
        public int Id { get; set; }
        public Guid BatchId { get; set; }
        public int IdToQuery { get; set; }
    }
}
