﻿using Faradise.Design.Models.Marketplace;

namespace Faradise.Design.DAL
{
    public class ARModelEntity
    {
        public int Id { get; set; }
        public int ARProductId { get; set; }

        public ARPlatform Platform { get; set; }
        public ARFormat Format { get; set; }

        public string Model { get; set; }

        public string Diffuse { get; set; }
        public string Normal { get; set; }
        public string Metallic { get; set; }
        public string Roughness { get; set; }
        public string MetallicRoughness { get; set; }

        public virtual ARProductEntity ARProduct { get; set; }
    }
}
