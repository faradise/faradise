﻿using System.ComponentModel;

namespace Faradise.Design.DAL
{
    public class CategoryParameterEntity
    {
        public int CategoryId { get; set; }
        public virtual MarketplaceCategoryEntity Category { get; set; }
        public int ParameterId { get; set; }
        public virtual ParameterEntity Parameter { get; set; }

    }
}