﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class MarketplaceOrderEntity
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? PaymentId { get; set; }
        public DateTime CreationTime { get; set; }
        public int BasePrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int DeliveryPrice { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime DatePayment { get; set; }
        public OrderStatus Status { get; set; }
        public bool IsActive { get; set; }
        public PaymentMethod SelectedPaymentMethod { get; set; }
        public bool IsReady { get; set; }
        public string PromoCode { get; set; }

        [ForeignKey("PaymentId")]
        public virtual PaymentEntity Payment { get; set; }
    }
}
