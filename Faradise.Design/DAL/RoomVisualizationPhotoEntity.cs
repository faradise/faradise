﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomVisualizationPhotoEntity
    {
        public int Id { get; set; }
        public int VisualizationId { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; } = true;

        [ForeignKey("VisualizationId")]
        public virtual RoomVisualizationEntity Visualization { get; set; }
    }
}
