﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class DesignerPortfolioProjectEntity
    {
        public int Id { get; set; }
        public int DesignerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<DesignerPortfolioProjectPhotoEntity> Photos { get; set; }

        [ForeignKey("DesignerId")]
        public virtual DesignerEntity Designer { get; set; }
    }
}
