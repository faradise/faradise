﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.DAL
{
    public class MarketplaceEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CompanyEntity> Companies { get; set; }
        public virtual ICollection<MarketplaceCategoryEntity> Categories { get; set; }

        public DateTime SalesUpdated { get; set; }
        public DateTime FeedUpdated { get; set; }

        public string AzureProductSchema { get; set; }
        public string CollectionBlacklist { get; set; }

        public virtual ICollection<MarketplaceFeedEntity> Feeds { get; set; }
    }
}
