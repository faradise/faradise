﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ViewSavedBasketEntity
    {
        public int Id { get; set; }
        public int UserId {get;set;}
        public int SavedBasketId { get; set; }
        public int Count { get; set; }

        [ForeignKey("SavedBasketId")]
        public virtual SavedBasketEntity SavedBasket { get; set; }
    }
}
