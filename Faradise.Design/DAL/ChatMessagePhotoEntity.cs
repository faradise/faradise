﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ChatMessagePhotoEntity
    {
        public int Id { get; set; }
        public int MessageId { get; set; }
        public string Link { get; set; }

        [ForeignKey("MessageId")]
        public virtual ChatMessageEntity Message { get; set; }
    }
}
