﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class ProjectFlowOldFurniturePhotoEntity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; } = true;

        [ForeignKey("ProjectId")]
        public virtual ProjectDefinitionEntity Project { get; set; }
    }
}
