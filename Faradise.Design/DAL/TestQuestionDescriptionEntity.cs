﻿using Faradise.Design.Models;
using System.Collections.Generic;

namespace Faradise.Design.DAL
{
    public class TestQuestionDescriptionEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public TestType For {get;set;}

    }
}
