﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.DAL
{
    public class RoomPlanEntity
    {
        [Key]
        public int RoomId { get; set; }
        public int Price { get; set; }
        public bool IsSelected { get; set; }
        public bool IsComplete { get; set; }
        public string ArchiveUrl { get; set; }
        public virtual ICollection<RoomPlanPhotoEntity> Photos { get; set; }
        public int? PaymentId { get; set; }

        [ForeignKey("RoomId")]
        public virtual ProjectDescriptionRoomEntity Room { get; set; }

        [ForeignKey("PaymentId")]
        public virtual PaymentEntity Payment { get; set; } 
    }
}
