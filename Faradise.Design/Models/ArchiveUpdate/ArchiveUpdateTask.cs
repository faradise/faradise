﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ArchiveUpdate
{
    public class ArchiveUpdateTask
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int RoomId { get; set; }
        public RoomArchiveType Type { get; set; }

        //public static ArchiveUpdateTask CreateForMoodboards(int projectId, int roomId)
        //{
        //    return new ArchiveUpdateTask() { ProjectId = projectId, RoomId = roomId, Type = RoomArchiveType.Moodboard };
        //}

        //public static ArchiveUpdateTask CreateForCollages(int projectId, int roomId)
        //{
        //    return new ArchiveUpdateTask() { ProjectId = projectId, RoomId = roomId, Type = RoomArchiveType.Collages };
        //}

        //public static ArchiveUpdateTask CreateForZones(int projectId, int roomId)
        //{
        //    return new ArchiveUpdateTask() { ProjectId = projectId, RoomId = roomId, Type = RoomArchiveType.Zones };
        //}

        //public static ArchiveUpdateTask CreateForPlans(int projectId, int roomId)
        //{
        //    return new ArchiveUpdateTask() { ProjectId = projectId, RoomId = roomId, Type = RoomArchiveType.Plans };
        //}

        //public static ArchiveUpdateTask CreateForVisuals(int projectId, int roomId)
        //{
        //    return new ArchiveUpdateTask() { ProjectId = projectId, RoomId = roomId, Type = RoomArchiveType.Visualization };
        //}
    }
}
