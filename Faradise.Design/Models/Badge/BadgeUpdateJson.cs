﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Badge
{
    public class BadgeUpdateJson
    {
        public int[] ProdyctIds { get; set; }
        public int[] CategoryId { get; set; }
        public int[] CompaniesId { get; set; }
        public bool AllMarkeplace { get; set; }
        public bool RemoveAfterUpdate;
    }
}
