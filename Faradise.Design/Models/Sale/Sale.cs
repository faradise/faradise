﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Sale
{
    public class Sale
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Percent { get; set; }
        public bool IsActive { get; set; }
        public bool AllMarketplace { get; set; }
        public int[] Categories { get; set; }
        public int[] Products { get; set; }
        public int[] Companies { get; set; }
        public string UpdateJson { get; set; }
        public string Description { get; set; }
    }
}
