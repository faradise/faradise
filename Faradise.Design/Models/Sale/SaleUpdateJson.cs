﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Sale
{
    public class SaleUpdateJson
    {
        public int[] ProdyctIds { get; set; }
        public int[] CategoryIds { get; set; }
        public int[] CompaniesId { get; set; }
        public bool AllMarkeplace { get; set; }
        public bool RemoveAfterUpdate;
    }
}
