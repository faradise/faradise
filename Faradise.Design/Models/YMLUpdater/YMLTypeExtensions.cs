﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.YMLUpdater
{
    public static class YMLTypeExtensions
    {
        public static UploadFiletype MapFileExtension(string extension)
        {
            if (extension.Contains("xls"))
                return UploadFiletype.EXCEL;
            else
                return UploadFiletype.YML;
        }
    }
}
