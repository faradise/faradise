﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.YMLUpdater
{
    public class YMLUpdateTask
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string YMLLink { get; set; }
        public DateTime CreateTime { get; set; }
        public int Priority { get; set; }
        public YMLUpdateTaskStatus Status { get; set; }
        public string FailMessage { get; set; }
        public UploadFiletype FileType { get; set; } 
        public YMLDownloadSource DownloadSource { get; set; }
    }
}
