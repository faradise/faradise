﻿using System.Collections.Generic;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class AdditionalInformationForRoom
    {
        public int RoomId { get; set; }
        public List<AdditionalQuestion> Questions { get; set; }
    }
}
