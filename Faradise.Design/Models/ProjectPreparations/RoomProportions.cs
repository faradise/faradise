﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class RoomProportions
    {
        public int SideA { get; set; }
        public int SideB { get; set; }
        public int HeightDoor { get; set; }
        public int WidthDoor { get; set; }
        public int HeightCeiling { get; set; }
    }
}
