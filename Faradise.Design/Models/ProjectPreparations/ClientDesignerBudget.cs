﻿namespace Faradise.Design.Models.ProjectPreparations
{
    public class ClientDesignerBudget
    {
        public int FurnitureBudget { get; set; }
        public int RenovationBudget { get; set; }
    }
}
