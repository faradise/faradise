﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class StageStatus
    {
        public CooperateStage Stage { get; set; }
        public bool DesignerIsReady { get; set; }
        public bool ClientIsReady { get; set; }
    }
}
