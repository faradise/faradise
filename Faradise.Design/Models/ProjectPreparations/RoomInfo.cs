﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class RoomInfo
    {
        public int RoomId { get; set; }
        public Room Purpose { get; set; }
        public List<Photo> Photos { get; set; }
        public string Plan { get; set; }
        public RoomProportions Proportions { get; set; }
    }
}
