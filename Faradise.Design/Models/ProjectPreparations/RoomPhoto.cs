﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class Photo
    {
        public int PhotoId { get; set; }
        public string Link { get; set; }
    }
}
