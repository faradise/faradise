﻿
namespace Faradise.Design.Models.ProjectPreparations
{
    public class ShortRoomInfo
    {
        public int RoomId { get; set; }
        public RoomProportions Proportions { get; set; }
    }
}
