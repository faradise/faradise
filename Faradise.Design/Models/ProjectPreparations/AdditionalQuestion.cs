﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class AdditionalQuestion
    {
        public int Index { get; set; }
        public string Answer { get; set; }
    }
}
