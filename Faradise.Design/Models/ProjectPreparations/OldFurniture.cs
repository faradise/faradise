﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectPreparations
{
    public class OldFurniture
    {       
        public List<Photo> FurniturePhotos { get; set; }
        public List<Photo> FurnishPhotos { get; set; }
    }
}
