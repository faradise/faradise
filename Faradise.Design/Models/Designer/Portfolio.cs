﻿namespace Faradise.Design.Models.Designer
{
    public class Portfolio
    {
        public Portfolio()
        {
            Portfolios = new PortfolioProject[0];
        }

        public PortfolioProject[] Portfolios { get; set; }
    }
}
