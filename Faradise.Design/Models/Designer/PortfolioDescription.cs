﻿namespace Faradise.Design.Models.Designer
{
    public class PortfolioDescription
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название проекта портфолио
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание проекта портфолио
        /// </summary>
        public string Description { get; set; }
    }
}
