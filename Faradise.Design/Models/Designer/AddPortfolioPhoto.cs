﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Faradise.Design.Models.Designer
{
    public class AddPortfolioPhoto
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        public int PortfolioId { get; set; }

        /// <summary>
        /// Фото файл
        /// </summary>
        public IFormFile Photo { get; set; }
    }
}
