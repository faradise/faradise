﻿namespace Faradise.Design.Models.Designer
{
    public class PersonalLegalInfo
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Телефон дизайнера
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Серия и номер паспорта
        /// </summary>
        public string PassportSeries { get; set; }

        /// <summary>
        /// Кем выдан
        /// </summary>
        public string PassportIssuedBy { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public string DateOfIssue { get; set; }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public string DepartmentCode { get; set; }

        /// <summary>
        /// Адрес регистрации
        /// </summary>
        public string RegistrationAddress { get; set; }

        /// <summary>
        /// Фактический адрес
        /// </summary>
        public string ActualAddress { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }
    }
}
