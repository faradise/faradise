﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;

namespace Faradise.Design.Models.Designer
{
    public class Designer
    {
        public Designer()
        {
            Base = new DesignerBase();
            Portfolio = new Portfolio();
            Styles = new DesignerStyle[0];
            Possibilities = new DesignerPossibility();
            Testing = new TestResults();
            LegalModel = new DesignerLegal();
            TestJob = new TestJobInfo();
        }

        /// <summary>
        /// Id дизайнера
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Стадия регистрации
        /// </summary>
        public DesignerRegistrationStage Stage { get; set; }

        /// <summary>
        /// Основная информация
        /// </summary>
        public DesignerBase Base { get; set; }

        /// <summary>
        /// Телефон дизайнера
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Ссылка на портфолио
        /// </summary>
        public string PortfolioLink { get; set; }

        /// <summary>
        /// Портфолио
        /// </summary>
        public Portfolio Portfolio { get; set; }

        /// <summary>
        /// Тестовое задание
        /// </summary>
        public TestJobInfo TestJob { get; set; }

        /// <summary>
        /// Список стилей
        /// </summary>
        public DesignerStyle[] Styles { get; set; }

        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string PhotoLink { get; set; }

        /// <summary>
        /// Уровень (ступень) дизайнера
        /// </summary>
        public DesignerLevel Level { get; set; }

        /// <summary>
        /// Возможности
        /// </summary>
        public DesignerPossibility Possibilities { get; set; }

        /// <summary>
        /// Резултаты теста подбора дизайнера
        /// </summary>
        public TestResults Testing { get; set; }

        /// <summary>
        /// Юридические данные
        /// </summary>
        public DesignerLegal LegalModel { get; set; }
    }
}
