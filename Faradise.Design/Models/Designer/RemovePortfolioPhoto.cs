﻿namespace Faradise.Design.Models.Designer
{
    public class RemovePortfolioPhoto
    {
        /// <summary>
        /// Id портфолио
        /// </summary>
        public int PortfolioId { get; set; }

        /// <summary>
        /// Id фото
        /// </summary>
        public int PhotoId { get; set; }
    }
}
