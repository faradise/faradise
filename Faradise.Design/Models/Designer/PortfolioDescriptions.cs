﻿namespace Faradise.Design.Models.Designer
{
    public class PortfolioDescriptions
    {
        public PortfolioDescription[] Descriptions { get; set; }
    }
}
