﻿namespace Faradise.Design.Models.Designer
{
    public class DesignerStyle
    {
        /// <summary>
        /// Тип стиля
        /// </summary>
        public ProjectStyle Name { get; set; }

        /// <summary>
        /// Дополнительное описание
        /// </summary>
        public string Description { get; set; }
    }
}
