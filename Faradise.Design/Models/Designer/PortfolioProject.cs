﻿namespace Faradise.Design.Models.Designer
{
    public class PortfolioProject
    {
        public PortfolioProject()
        {
            PhotoLinks = new PortfolioPhoto[0];
        }

        /// <summary>
        /// Id портфолио
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название проекта портфолио
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание проекта портфолио
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Фотографии проекта портфолио
        /// </summary>
        public PortfolioPhoto[] PhotoLinks { get; set; }
    }
}
