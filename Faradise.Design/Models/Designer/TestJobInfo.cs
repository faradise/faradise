﻿using System;

namespace Faradise.Design.Models.Designer
{
    public class TestJobInfo
    {
        /// <summary>
        /// Ссылка на тестовое задание
        /// </summary>
        public string TestLink { get; set; }

        /// <summary>
        /// Время отправки тестового задания
        /// </summary>
        public DateTime TestTime { get; set; }

        /// <summary>
        /// Заявка одобрена
        /// </summary>
        public bool TestApproved { get; set; }
    }
}
