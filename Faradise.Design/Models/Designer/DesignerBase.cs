﻿using Faradise.Design.Models.Enums;

namespace Faradise.Design.Models.Designer
{
    public class DesignerBase
    {
        /// <summary>
        /// Имя дизайнера
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Фамилия дизайнера
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Возраст дизайнера
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Город дизайнера
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Пол дизайнера
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Email дизайнера
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// О себе
        /// </summary>
        public string About { get; set; }

        /// <summary>
        /// Опыт работы
        /// </summary>
        public WorkExperience WorkExperience { get; set; }

        public DesignerRegistrationStage Stage { get; set; }
    }
}
