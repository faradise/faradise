﻿namespace Faradise.Design.Models.Designer
{
    public class CompanyLegalInfo
    {
        /// <summary>
        /// Название организации
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ОГРН (или ОГРНИП)
        /// </summary>
        public string OGRN { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string INN { get; set; }

        /// <summary>
        /// КПП
        /// </summary>
        public string KPP { get; set; }

        /// <summary>
        /// Название банка
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// Расчетный счет
        /// </summary>
        public string CheckingAccount { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        public string BIC { get; set; }

        /// <summary>
        /// Название единоличного исполнительного органа
        /// </summary>
        public string SoleExecutiveBody { get; set; }

        /// <summary>
        /// ФИО единоличного исполнительного органа
        /// </summary>
        public string NameSoleExecutiveBody { get; set; }
    }
}
