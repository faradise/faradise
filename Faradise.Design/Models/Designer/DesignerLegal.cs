﻿using Faradise.Design.Controllers.API.Models.Designer;

namespace Faradise.Design.Models.Designer
{
    public class DesignerLegal
    {
        public DesignerLegal()
        {
            CompanyLegalInfo = new CompanyLegalInfo();
            PersonalLegalInfo = new PersonalLegalInfo();
        }
        /// <summary>
        /// Юридическое или физическое лицо
        /// </summary>
        public LegalType LegalType { get; set; }

        /// <summary>
        /// Юридическое лицо
        /// </summary>
        public CompanyLegalInfo CompanyLegalInfo { get; set; }

        /// <summary>
        /// Физическое лицо
        /// </summary>
        public PersonalLegalInfo PersonalLegalInfo { get; set; }
    }
}
