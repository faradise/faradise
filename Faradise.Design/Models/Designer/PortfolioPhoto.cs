﻿namespace Faradise.Design.Models.Designer
{
    public class PortfolioPhoto
    {
        /// <summary>
        /// Id фото
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Фотографии проекта портфолио
        /// </summary>
        public string PhotoLink { get; set; }
    }
}
