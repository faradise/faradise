﻿namespace Faradise.Design.Models.Designer
{
    public class DesignerPossibility
    {
        /// <summary>
        /// Возможность выезжать на место в своем городе
        /// </summary>
        public bool GoToPlaceInYourCity { get; set; }

        /// <summary>
        /// Возможность выезжать на место в другом городе
        /// </summary>
        public bool GoToPlaceInAnotherCity { get; set; }

        /// <summary>
        /// Возможность авторского надзора
        /// </summary>
        public bool PersonalСontrol { get; set; }

        /// <summary>
        /// Возможность организации очной встречи
        /// </summary>
        public bool PersonalMeeting { get; set; }

        /// <summary>
        /// Возможность организации skype-встречи
        /// </summary>
        public bool SkypeMeeting { get; set; }

        /// <summary>
        /// Я могу выехать и сделать замеры помещения
        /// </summary>
        public bool RoomMeasurements { get; set; }

        /// <summary>
        /// Делаете ли вы рабочие чертежи?
        /// </summary>
        public bool Blueprints { get; set; }
    }
}
