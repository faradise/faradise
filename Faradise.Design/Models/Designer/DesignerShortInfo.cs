﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Designer
{
    public class DesignerShortInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int AppointedProjectsCount { get; set; }
        public int ProjectsInWorkCount { get; set; }
        public int RejectedProjectsCount { get; set; }
        public DesignerRegistrationStage Stage { get; set; }
    }
}
