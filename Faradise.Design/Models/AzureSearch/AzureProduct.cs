﻿using System;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Spatial;
using Newtonsoft.Json;

namespace Faradise.Design.Models.AzureSearch
{
    public class AzureProduct
    {
        [System.ComponentModel.DataAnnotations.Key]
        [IsSearchable,IsFilterable]
        public string Id { get; set; }
        [IsSearchable]
        public string Name { get; set; }
        [IsFilterable, IsFacetable]
        public int CompanyId { get; set; }
        [IsFilterable]
        public int CompanyCategoryId { get; set;  }
        [IsFilterable]
        public string CompanyProductId { get; set; }
        [IsFilterable, IsFacetable]
        public int[] MarketplaceCategory { get; set; }
        [IsFilterable, IsFacetable]
        public bool IsRemoved { get; set; } = false;
        [IsSearchable]
        [Analyzer(AnalyzerName.AsString.RuMicrosoft)]
        [JsonProperty("description_ru")]
        public string Description { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public int Price { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public int? PreviousPrice { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public int? ActualPrice { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public int? Rating { get; set; }
        [IsFilterable, IsFacetable]
        public string Url { get; set; }
        [IsFilterable, IsFacetable]
        public bool? Available { get; set; }
        [IsFilterable, IsFacetable]
        public bool? Pickup { get; set; }
        [IsFilterable, IsFacetable]
        public bool? Delivery { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public int? DeliveryCost { get; set; }
        [IsSearchable, IsFilterable, IsFacetable]
        public string DeliveryDays { get; set; }
        [IsFilterable, IsFacetable]
        public string Vendor { get; set; }
        [IsSearchable, IsFilterable, IsFacetable]
        public string VendorCode { get; set; }
        [IsFilterable, IsFacetable]
        public bool? Warranty { get; set; }
        [IsSearchable, IsFilterable, IsFacetable]
        public string Origin { get; set; }
        [IsSearchable, IsFilterable, IsFacetable]
        public string Size { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public double? Width { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public double? Height { get; set; }
        [IsFilterable, IsSortable, IsFacetable]
        public double? Length { get; set; }
        public int? BadgeId { get; set; }
        public int? SaleId { get; set; }
        public string Badge { get; set; }
        public string BadgeDescription { get; set; }
        public string SaleDescription { get; set; }
        public int SalePercent { get; set; }
        [IsSearchable]
        public string[] ColorsNames { get; set; }

        [IsFilterable, IsSortable]
        public int ARSupport { get; set; }
        [IsFilterable]
        public int[] ARFormat { get; set; }
        public string[] Pictures { get; set; }
        [IsFilterable, IsFacetable]
        public int[] MarketplaceColors { get; set; }
        [IsSearchable]
        public string JSONNotes { get; set; } = string.Empty;
        [IsFilterable, IsFacetable]
        public int[] Rooms { get; set; }

        [IsFilterable]
        public string LastUpdateId { get; set; }
    }
}
