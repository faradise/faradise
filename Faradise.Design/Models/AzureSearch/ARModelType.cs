﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.AzureSearch
{
    public enum ARModelType
    {
        IosBFM=0,
        IosSCN=1,
        AndroidBFM=2,
        AndroidSFB=3
    }
}
