﻿using Faradise.Design.Controllers.API.Models.ProjectDescription;
using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDescription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDefinition
{
    public class FullProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int BudgetFurniture { get; set; }
        public int BudgetRenovation { get; set; }
        public bool DesignerIsApproved { get; set; }
        public int AppointedDesignersCount { get; set; }
        public int RejectedDesignersCount { get; set; }
        public int DesignerId { get; set; }
        public int ClientId { get; set; }

        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhone { get; set; }
        public ProjectStage Stage { get; set; }
        public ProjectStatus Status { get; set; }
        public ProjectTargetType ObjectType { get; set; }
        public TestResults TestAnswers { get; set; }
        public bool NeedAdmin { get; set; }
        public bool HasChangesAfterLastAdminCheck { get; set; }
        public DateTime FillTime { get; set; }

        public bool IsFilled { get; set; }

        public List<Room> Rooms { get; set; }

        public List<StyleBinding> Styles { get; set; }

        public List<ReasonBinding> Reasons { get; set; }

        public DesignerRequirements DesignerRequirements { get; set; }
    }
}
