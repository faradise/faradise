﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDefinition
{
    public class ProjectShortInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int BudgetFurniture { get; set; }
        public int BudgetRenovation { get; set; }
        public bool DesignerIsApproved { get; set; }
        public int AppointedDesignersCount { get; set; }
        public int RejectedDesignersCount { get; set; }
        public int DesignerId { get; set; }
        public bool IsProjectFill { get; set; }
        public int ClientId { get; set; }
        public ProjectStage Stage { get; set; }
        public ProjectStatus Status { get; set; }
        public bool NeedAdmin { get; set; }
    }
}
