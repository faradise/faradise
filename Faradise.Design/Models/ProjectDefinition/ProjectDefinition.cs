﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDefinition
{
    public class ProjectDefinition
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int DesignerId { get; set; }
        public bool DesignerConfirmed { get; set; }
        public int ClientId { get; set; }
        public ProjectStage Stage { get; set; }
        public ProjectStage? AbandonStage { get; set; }
        public ProjectStatus Status { get; set; }
        public bool ProjectFill { get; set; }
        public DateTime? ProjectFillDate { get; set; }
        public bool NeedAdmin { get; set; }
        public bool HasChangesAfterLastAdminCheck { get; set; }
    }
}
