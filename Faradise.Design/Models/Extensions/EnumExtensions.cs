﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public static class EnumExtensions
    {
        public static bool ContainsRole(this Role flags, Role role)
        {
            return (flags & role) == role;
        }
    }
}
