﻿namespace Faradise.Design.Models.Cache
{
    public class CacheRequest
    {
        public string Key { get; private set; }
        public object[] Arguments { get; private set; }

        public CacheRequest(string key, params object[] args)
        {
            Key = key;
            Arguments = args;
        }
    }
}
