﻿namespace Faradise.Design.Models.Cache
{
    public class CacheValue<T>
    {
        public bool Succeeded { get; private set; }
        public T Value { get; private set; }

        public static CacheValue<T> Ok(T value)
        {
            return new CacheValue<T>
            {
                Succeeded = true,
                Value = value
            };
        }

        public static CacheValue<T> Miss()
        {
            return new CacheValue<T>
            {
                Succeeded = false,
                Value = default(T)
            };
        }
    }
}
