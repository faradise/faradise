﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation.MailSending
{
    public class MandrillTemplateModel
    {
        public string key { get; set; }
        public string template_name { get; set; } = "Test";
        public MandrillTemplateStruct[] template_content { get; set; }
        public MandrillMessage message { get; set; }
        public string async { get; set; } = "false";
        public string send_at { get; set; } = "2018-01-01 00:00:00";
    }

    public struct MandrillTemplateStruct
    {
        public string name { get; set; }
        public string content { get; set; }
    }

    public class MandrillMessage
    {
        public string from_email { get; set; }
        public string from_name { get; set; }
        public MandrillAddress[] to { get; set; }
    }

    public class MandrillAddress
    {
        public string email { get; set; }

    }
}
