﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Services.Implementation.MailSending
{
    public class EmailModel
    {
        public string[] ToAddress { get; set; }
        public string TemplateName { get; set; }
        public TemplateModel TemplateModel { get; set; }
        public string TemplateEngine { get; set; }
    }

    public class TemplateModel
    {
        public string return_url { get; set; }
    }
}
