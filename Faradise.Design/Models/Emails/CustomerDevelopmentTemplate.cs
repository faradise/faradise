﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class CustomerDevelopmentTemplate : TemplateModel
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string clientid { get; set; }
    }
}
