﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails.UniOne
{
    public class UniOneMailModel<T> where T : TemplateModel
    {
        public string api_key;
        public string username;
        public UniOneMessageModel<T> message;
    }

    public class UniOneMessageModel<T> where T : TemplateModel
    {
        public string template_id;
        public string template_engine;
        public string global_substitutions;
        public string html;
        public string subject;
        public string from_email;
        public string from_name;
        public Recipients<T>[] recipients;
    }

    public class Recipients<T> where T : TemplateModel
    {
        public string email;
        public string to_name;
        public T substitutions;
    }
}
