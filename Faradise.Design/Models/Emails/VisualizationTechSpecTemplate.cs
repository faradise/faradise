﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class VisualizationTechSpecTemplate : TemplateModel
    {
        public class Product
        {
            public string id { get; set; }
            public string name { get; set; }
            public string outerlink { get; set; }
        }

        public class SpecFile
        {
            public string name { get; set; }
            public string link { get; set; }
        }

        public class Proportions
        {
            public string sidea { get; set; }
            public string sideb { get; set; }
            public string heightdoor { get; set; }
            public string widthdoor { get; set; }
            public string heightceiling { get; set; }
        }

        public string projectid { get; set; }
        public string roomid { get; set; }
        public string purpose { get; set; }
        public string description { get; set; }
        public SpecFile[] specfiles { get; set; }
        public string[] styles { get; set; }
        public Proportions roomproportions { get; set; }
        public string[] moodbaords { get; set; }
        public string moodboardarchive { get; set; }
        public string[] collages { get; set; }
        public string collagearchive { get; set; }
        public string[] zones { get; set; }
        public string zonearchive { get; set; }
        public Product[] products { get; set; }
    }
}
