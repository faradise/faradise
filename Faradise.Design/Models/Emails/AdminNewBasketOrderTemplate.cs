﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class AdminNewBasketOrderTemplate : TemplateModel
    {
        public string orderid { get; set; }
        public string clientname { get; set; }
    }
}
