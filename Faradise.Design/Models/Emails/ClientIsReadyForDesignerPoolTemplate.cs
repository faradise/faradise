﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class ClientIsReadyForDesignerPoolTemplate : TemplateModel
    {
        public string projectid { get; set; }

        public static ClientIsReadyForDesignerPoolTemplate BuildPoolIsLowForUser(ProjectDefinition.ProjectDefinition project)
        {
            if (project == null)
                return null;
            return new ClientIsReadyForDesignerPoolTemplate()
            {
                projectid = project.Id.ToString()
            };
        }
    }
}
