﻿using Faradise.Design.Services.Implementation.MailSending;

namespace Faradise.Design.Models.Emails
{
    public class LotteryTemplate : TemplateModel
    {
        public string loteryid { get; set; }
    }
}
