﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class BasketOrderPaymentTemplate : TemplateModel
    {
        public string clientname { get; set; }
        public string orderid { get; set; }
        public string paymentlink { get; set; }
        public string deliverprice { get; set; }
        public string additionalprice { get; set; }
        public string sum { get; set; }
        public Product[] products { get; set; }

        public class Product
        {
            public string name { get; set; }
            public string count { get; set; }
            public string price { get; set; }
        }
    }
}
