﻿using Faradise.Design.Services.Implementation.MailSending;

namespace Faradise.Design.Models.Emails
{
    public class AdminNoDesignersTemplate : TemplateModel
    {
        public string clientid { get; set; }
        public string projectid { get; set; }
        public string leftinpool { get; set; }

        public static AdminNoDesignersTemplate BuildPoolIsLowForUser(User user, ProjectDefinition.ProjectDefinition project, int leftDesigners)
        {
            if (user == null || project == null)
                return null;
            return new AdminNoDesignersTemplate()
            {
                clientid = user.Id.ToString(),
                projectid = project.Id.ToString(),
                leftinpool = leftDesigners.ToString()
            };
        }
    }
}
