﻿using Faradise.Design.Services.Implementation.MailSending;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Emails
{
    public class BrigadeTechSpecTemplate : TemplateModel
    {
        public class SpecFile
        {
            public string name { get; set; }
            public string link { get; set; }
        }

        public class Room
        {
            public string[] plans { get; set; }
            public string planarchive { get; set; }
            public string[] collages { get; set; }
            public string collagearchive { get; set; }
            public string[] zones { get; set; }
            public string zonearchive { get; set; }
        }

        public string projectid { get; set; }
        public string description { get; set; }
        public SpecFile[] specfiles { get; set; }
        public Room[] rooms { get; set; }

    }
}
