﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class StyleBinding
    {
        public ProjectStyle Name { get; set; }
        public string Description { get; set; }
    }
}
