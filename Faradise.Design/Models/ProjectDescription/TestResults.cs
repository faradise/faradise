﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class TestResults
    {
        public TestResultsAnswer[] Questions { get; set; }
    }
}
