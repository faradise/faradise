﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class Budget
    {
        public int Furniture { get; set; }
        public int Renovation { get; set; }
    }
}
