﻿using System.Collections.Generic;

namespace Faradise.Design.Models.ProjectDescription
{
    public class ProjectDescription
    {
        public int ProjectId { get; set; }
        public ProjectTargetType ObjectType { get; set; }
        public List<Room> Rooms { get; set; }
        public ReasonBinding[] Reasons { get; set; }
        public StyleBinding[] Styles { get; set; }
        public Budget Budget { get; set; }
        public DesignerRequirements DesignerRequirements { get; set; }
    }
}
