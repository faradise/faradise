﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class TestQuestionDescription
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public TestAnswerDescription[] Answers { get; set; }

        public TestQuestionDescription() { } 

        public TestQuestionDescription(int id, string text)
        {
            Text = text;
            Id = id;
        }
    }
}
