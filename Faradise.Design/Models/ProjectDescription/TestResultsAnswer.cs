﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class TestResultsAnswer
    {
        public int Index { get; set; }
        public int Answer { get; set; }
    }
}
