﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class ReasonBinding
    {
        public ProjectReason Name { get; set; }
        public string Description { get; set; }
    }
}
