﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class TestAnswerDescription
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public TestAnswerDescription() { }

        public TestAnswerDescription(int id, string text)
        {
            Text = text;
            Id = id;
        }
    }
}
