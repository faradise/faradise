﻿namespace Faradise.Design.Models.ProjectDescription
{
    public class DesignerRequirements
    {
        public Gender Gender { get; set; }
        public int AgeFrom { get; set; }
        public int AgeTo { get; set; }
        public bool NeedPersonalMeeting { get; set; }
        public string MeetingAdress { get; set; }
        public bool NeedPersonalСontrol { get; set; }
        public TestResults Testing { get; set; }
    }
}
