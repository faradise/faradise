﻿namespace Faradise.Design.Models.Admin.Company
{
    public class AdminCompanyCategory
    {
        public int CompanyCategoryId { get; set; }
        public int ParentCompanyCategoryId { get; set; }

        public int Products { get; set; }

        public int? CategoryId { get; set; }
        public bool BadCategoryBinding { get; set; }
    }
}
