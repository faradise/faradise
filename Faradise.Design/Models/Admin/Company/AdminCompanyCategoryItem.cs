﻿using Faradise.Design.Controllers.API.Models.Admin.Marketpace;

namespace Faradise.Design.Models.Admin.Company
{
    public class AdminCompanyCategoryItem
    {
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public int Depth { get; set; }
        public int Products { get; set; }

        public bool BadCategoryBinding { get; set; }

        public RoomNameModel[] Rooms { get; set; }
    }
}
