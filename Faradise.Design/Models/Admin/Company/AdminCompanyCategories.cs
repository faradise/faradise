﻿namespace Faradise.Design.Models.Admin.Company
{
    public class AdminCompanyCategories
    {
        public int CompanyId { get; set; }
        public AdminCompanyCategory[] CompanyCategories { get; set; }
    }
}
