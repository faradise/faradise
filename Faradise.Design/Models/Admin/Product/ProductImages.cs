﻿namespace Faradise.Design.Models.Admin.Product
{
    public class ProductImages
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public ProductImagePriority[] Images { get; set; }
    }
}
