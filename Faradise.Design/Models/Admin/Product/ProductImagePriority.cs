﻿namespace Faradise.Design.Models.Admin.Product
{
    public class ProductImagePriority
    {
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
        public int Priority { get; set; }
    }
}
