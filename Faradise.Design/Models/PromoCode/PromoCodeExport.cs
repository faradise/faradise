﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.PromoCode
{
    public class PromoCodeExport
    {
        public string Name { get; set; }
        public int MinPrice { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public int PercentsDiscount;
        public int AbsoluteDisocunt;
        public PromoCodeType CodeType;
        public int Used { get; set; }
        public int Count { get; set; }
    }
}
