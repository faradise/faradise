﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.PromoCode
{
    public class PromoCode
    {
        public string Code;
        public string ExportName;
        public int PercentDiscount;
        public int AbsoluteDiscount;
        public PromoCodeType CodeType;
        public bool Used;
        public int MinPrice;
        public DateTime StartDate;
        public DateTime ExpireDate;
    }
}
