﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.PromoCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.PromoCode
{
    public class PromoCodeApplyResult
    {
        public PromocodeError Error { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime StartDate { get; set; }
        public int PercentDiscount { get; set; }
        public int AbsoluteDiscount { get; set; }
        public PromocodeProduct[] Products { get; set; }
    }
}
