﻿using System.Collections.Generic;
using Faradise.Design.Marketplace.Models;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Models.CategoryParameters
{
    public class ParameterInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Synonyms { get; set; } = new List<string>();
        public ParameterType Type { get; set; }
        public bool GlobalParameter { get; set; }
        public int[] SelectedCategories { get; set; }
        public List<CategoryParameterInfo> Categories { get; set; } = new List<CategoryParameterInfo>();
    }
}