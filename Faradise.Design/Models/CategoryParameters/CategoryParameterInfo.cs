﻿using System.Collections.Generic;

namespace Faradise.Design.Models.CategoryParameters
{
    public class CategoryParameterInfo
    {
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }

        //public string CategoryName { get; set; }
        //public int CategoryId { get; set; }
        //public int ParameterCount => Parameters.Count;
        //public List<ParameterInfo> Parameters { get; set; }
    }
}