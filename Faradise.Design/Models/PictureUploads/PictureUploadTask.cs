﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.PictureUploads
{
    public class PictureUploadTask
    {
        public int PictureId { get; set; }
        public int ProductId { get; set; }
        public int CompanyId { get; set; }
        public string PhotoUrl { get; set; }
    }
}
