﻿using Faradise.Design.Models.ProjectPreparations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.VisualizationFreelance
{
    public class VisualisationTechSpec
    {
        public int RoomId { get; set; }
        public int DesignerId { get; set; }
        public string Description { get; set; }
        public DateTime PublishTime { get; set; }
        public TechSpecFile[] SpecFiles { get; set; }
        public VisualizationWorkStatus Status { get; set; }
        public TechSpecProduct[] Products { get; set; }
    }
}
