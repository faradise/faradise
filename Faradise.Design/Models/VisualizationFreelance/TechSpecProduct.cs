﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.VisualizationFreelance
{
    public class TechSpecProduct
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string OuterUrl { get; set; }
    }
}
