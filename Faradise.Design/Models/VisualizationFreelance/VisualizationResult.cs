﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.VisualizationFreelance
{
    public class VisualizationResult
    {
        public int RoomId { get; set; }
        public int ProjectId { get; set; }
        public RoomFile[] Files { get; set; }
        public string Description { get; set; }
    }
}
