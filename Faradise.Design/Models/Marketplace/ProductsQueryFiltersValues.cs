﻿using Faradise.Design.Controllers.API.Models.Marketplace;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Models.Marketplace
{
    public class ProductsQueryFiltersValues
    {
        public int Offset { get; set; }
        public int Count { get; set; }

        public FilterOrdering Ordering { get; set; }

        public string SearchWords { get; set; }

        public int? Category { get; set; }

        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }

        public bool AR { get; set; }

        public int[] Brands { get; set; }
        public int[] Rooms { get; set; }
        public int[] Colors { get; set; }

        public bool SalesOnly { get; set; }
        public ProductMeasurements Measurements { get; set; }
    }
}
