﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class BasketView
    {
        public int UserId { get; set; }
        public int ViewsCount { get; set; }
    }
}
