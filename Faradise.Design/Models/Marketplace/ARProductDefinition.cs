﻿namespace Faradise.Design.Models.Marketplace
{
    public class ARProductDefinition
    {
        public int ProductId { get; set; }

        public string Model { get; set; }

        public string MapDiffuse { get; set; }
        public string MapNormal { get; set; }
        public string MapMetallic { get; set; }
        public string MapRoughness { get; set; }
        public string MapMetallicRoughness { get; set; }

        public bool Vertical { get; set; }
    }
}
