﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public CompanyProduction CompanyProductionMask { get; set; }
        public string LegalName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhone { get; set; }
        public string ContactPersonEmail { get; set; }
        public string DeliveryInfo { get; set; }
        public string NotesJson { get; set; }
        public string FeedUrl { get; set; }
        public bool EnableAutofeed { get; set; }

        public bool IsEnabled { get; set; }

        public int AllProducts { get; set; }
        public int AllCategories { get; set; }

        public int CanTradeCategories { get; set; }
        public int CanTradeProducts { get; set; }

        public int InTradeCategories { get; set; }
        public int InTradeProducts { get; set; }

        public int NotInTradeCategories { get; set; }
        public int NotInTradeProducts { get; set; }

        public int SalesCategories { get; set; }
        public int SalesProducts { get; set; }

        public bool ProductsNotInLeafs { get; set; }

        public int? MinSale { get; set; }
        public int? MaxSale { get; set; }

        public int DailyProductPriority { get; set; }
        public bool DontSupportPromocode { get; set; }
    }
}
