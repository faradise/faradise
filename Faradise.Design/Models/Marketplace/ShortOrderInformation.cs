﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class ShortOrderInformation
    {
        public int OrderId { get; set; }
        public int BasePrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int DeliveryPrice { get; set; }
        public int? PaymentId { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime CreationTime { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentMethod SelectedPaymentMethod { get; set; }
        public string Name { get; set; }
    }
}
