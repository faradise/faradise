﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class CompanyCategoryRoom
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public int RoomNameId { get; set; }
    }
}
