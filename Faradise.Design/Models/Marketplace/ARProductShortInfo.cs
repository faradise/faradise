﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class ARProductShortInfo
    {
        public int Id { get; set; }
        public string FaradiseName { get; set; }
        public int FaradiseId { get; set; }
        public string Color { get; set; }
        public string ProductName { get; set; }
        public int? ProductId { get; set; }
    }
}
