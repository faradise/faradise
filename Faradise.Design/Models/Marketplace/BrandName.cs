﻿namespace Faradise.Design.Models.Marketplace
{
    public class BrandName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
