﻿namespace Faradise.Design.Models.Marketplace
{
    public class ARProductSupport
    {
        public bool AndroidBFM { get; set; }
        public bool AndroidSFB { get; set; }

        public bool IOSBFM { get; set; }
        public bool IOSSCN { get; set; }
    }
}
