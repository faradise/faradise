﻿namespace Faradise.Design.Models.Marketplace
{
    public enum ARFormat
    {
        None = 0,
        BFM,
        SCN,
        SFB
    }
}
