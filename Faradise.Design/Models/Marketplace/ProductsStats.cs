﻿namespace Faradise.Design.Models.Marketplace
{
    public class ProductsStats
    {
        public int TotalProducts { get; set; }

        public int Name { get; set; }
        public int Description { get; set; }
        public int PreviousPrice { get; set; }
        public int Url { get; set; }
        public int Pickup { get; set; }
        public int Delivery { get; set; }
        public int DeliveryOptions { get; set; }
        public int Vendor { get; set; }
        public int Warranty { get; set; }
        public int Origin { get; set; }
        public int Size { get; set; }
        public int Notes { get; set; }
        public int Pictures { get; set; }
        public int Colors { get; set; }
    }
}
