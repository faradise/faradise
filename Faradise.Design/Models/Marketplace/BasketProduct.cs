﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class BasketProduct
    {
        public ProductInfo Product { get; set; }
        public int Count { get; set; }
    }
}
