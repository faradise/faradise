﻿namespace Faradise.Design.Models.Marketplace
{
    public class CategoryName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
