﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class UpdateCompanyData
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Url { get; set; }

        public List<CompanyCategory> Categories { get; set; }
        public List<Product> Products { get; set; }
    }
}
