﻿namespace Faradise.Design.Models.Marketplace
{
    public class RoomName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
