﻿namespace Faradise.Design.Models.Marketplace
{
    public class ProductsQuery
    {
        public int AllCount { get; set; }
        public ProductInfo[] Products { get; set; }
    }
}
