﻿namespace Faradise.Design.Models.Marketplace
{
    public class CompanyCategory
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public string CompanyCategoryName { get; set; }
        public int ParentCompanyCategoryId { get; set; }
        public RoomName[] Rooms {get;set;}
        public int Depth { get; set; }
        public int ProductsCount { get; set; }

        public int CategoryId { get; set; }
    }
}
