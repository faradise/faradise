﻿namespace Faradise.Design.Models.Marketplace
{
    public enum ARPlatform
    {
        Browser,
        IOS,
        Android
    }
}
