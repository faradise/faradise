﻿using Faradise.Design.Models.Enums;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class FullProjectProductsOrder
    {
        public int OrderId { get; set; }
        public int ProjectId { get; set; }
        public OrderDescription OrderDescription { get; set; }
        public int BasePrice { get; set; }
        public int AdditionalPrice { get; set; }
        public int DeliveryPrice { get; set; }
        public RoomGoods[] Goods { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public int? PaymentId { get; set; }
        public string PaymentLink { get; set; }
        public string PlatformLink { get; set; }
        public DateTime CreateionTime { get; set; }
        public bool IsFinished { get; set; }
        public OrderStatus Status { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        public bool IsReady { get; set; }
    }
}
