﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class SavedBasketShortInfo
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public BasketView[] Views { get; set; }
    }
}
