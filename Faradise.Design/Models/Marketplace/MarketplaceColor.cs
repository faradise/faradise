﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class MarketplaceColor
    {
        public int ColorId { get; set; }
        public string Name { get; set; }
        public string Hex { get; set; }
    }
}
