﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class PossibleDailyProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public int Price { get; set; }
        public int? PreviousPrice { get; set; }
        public string PhotoUrl { get; set; }
        public float CompPriority { get; set; }
        public float CatPriority { get; set; }
        public float SalePriority { get; set; }
        public float TotalPriority { get; set; }
    }
}
