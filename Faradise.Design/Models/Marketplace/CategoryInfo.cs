﻿namespace Faradise.Design.Models.Marketplace
{
    public class CategoryInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
