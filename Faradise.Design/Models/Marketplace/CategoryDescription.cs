﻿using System.Collections.Generic;

namespace Faradise.Design.Models.Marketplace
{
    public class CategoryDescription
    {
        public Category Category { get; set; }
        public List<Category> Root { get; set; }
        public List<Category> Childs { get; set; }
    }
}
