﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class MapCategory
    {
        public int CompanyId { get; set; }
        public int CompanyCategoryId { get; set; }
        public int CategoryId { get; set; }
    }
}
