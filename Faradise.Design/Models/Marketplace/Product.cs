﻿using Faradise.Design.Controllers.API.Models.Marketplace;

namespace Faradise.Design.Models.Marketplace
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public int CompanyCategoryId { get; set; }
        public string CompanyProductId { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }
        public int? PreviousPrice { get; set; }
        
        public string Url { get; set; }
        public string[] Pictures { get; set; }

        public bool? Available { get; set; }

        public bool? Pickup { get; set; }
        public bool? Delivery { get; set; }
        public int? DeliveryCost { get; set; }
        public string DeliveryDays { get; set; }

        public string Seller { get; set; }
        public string Vendor { get; set; }
        public string VendorCode { get; set; }
        public bool? Warranty { get; set; }

        public string[] Color { get; set; }
        public string Origin { get; set; }

        public string Size { get; set; }

        public float? Width { get; set; }
        public float? Height { get; set; }
        public float? Length { get; set; }

        public string[] Notes { get; set; }

        public string Badge { get; set; }
        public string BadgeDescription { get; set; }
        public string SaleDescription { get; set; }

        public int CategoryDeliveryCost { get; set; }
        public int CategoryDeliveryDays { get; set; }

        public ARProductSupport ARSupport { get; set; } = new ARProductSupport
        {
            AndroidBFM = false,
            AndroidSFB = false,
            IOSBFM = false,
            IOSSCN = false
        };
    }
}
