﻿using System.Collections.Generic;
using Faradise.Design.Controllers.API.Models.Marketplace;

namespace Faradise.Design.Models.Marketplace
{
    public class ProductsQueryFilters
    {
        public int Products { get; set; }

        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }

        public List<ColorName> Colors { get; set; } = new List<ColorName>();
        public List<BrandName> Brands { get; set; } = new List<BrandName>();
        public List<RoomName> Rooms { get; set; } = new List<RoomName>();

        public bool ContainsARProducts { get; set; }

        public ProductMeasurements Measurements { get; set; }
    }
}
