﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class DailyProduct
    {
        public DateTime OfferTime { get; set; }
        public PossibleDailyProduct Product { get; set; }
    }
}
