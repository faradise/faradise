﻿namespace Faradise.Design.Models.Marketplace
{
    public class ColorName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Hex { get; set; }
    }
}
