﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class CategoryHierarchy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        
        public CategoryHierarchy[] Childs { get; set; }
    }
}
