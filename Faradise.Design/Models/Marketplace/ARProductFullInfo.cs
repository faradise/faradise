﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Marketplace
{
    public class ARProductFullInfo
    {
        public int Id { get; set; }
        public int FaradiseProductId { get; set; }
        public int FaradiseColorId { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string VendorCode { get; set; }
        public bool Vertiacal { get; set; }
        public string Render { get; set; }

        public ARParsedModel IosBFM { get; set; }
        public ARParsedModel IosSCN { get; set; }

        public ARParsedModel AndroidBFM { get; set; }
        public ARParsedModel AndroidSFB { get; set; }
    }
}
