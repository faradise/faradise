﻿using System;

namespace Faradise.Design.Models
{
    public class VerificationCode
    {
        public string Phone;
        public string Code;
        public DateTime Expires;
        public DateTime CreationDate;
    }
}
