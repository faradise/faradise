﻿namespace Faradise.Design.Models
{
    public class Alias
    {
        public int Id;
        public int UserId;
        public AliasType Type;
        public string Value;
    }
}
