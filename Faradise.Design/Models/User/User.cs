﻿namespace Faradise.Design.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Phone { get; set; }

        public Role Role { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return Id.ToString();
        }
    }
}
