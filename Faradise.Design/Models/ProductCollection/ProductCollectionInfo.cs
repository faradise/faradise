﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProductCollection
{
    public class ProductCollectionInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string KeyWord { get; set; }
        public bool Available { get; set; }
        public int ProductCount { get; set; }
    }
}
