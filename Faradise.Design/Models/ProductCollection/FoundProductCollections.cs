﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProductCollection
{
    public class FoundProductCollections
    {
        public Dictionary<string, List<ProductName>> Collections { get; set; }
    }
}
