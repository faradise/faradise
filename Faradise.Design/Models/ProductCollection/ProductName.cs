﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProductCollection
{
    public class ProductName
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
