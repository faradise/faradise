﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Enums;
using System;

namespace Faradise.Design.Models.Toloka
{
    public class TolokaTaskModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public TolokaOutputType Type { get; set; }
        public TolokaTaskStatus Status { get; set; }
        public DateTime Created { get; set; }

        public int SegmentsCount { get; set; }
        public int ProcessedSegmentsCount { get; set; }

        public string ReportUrl { get; set; }
    }
}
