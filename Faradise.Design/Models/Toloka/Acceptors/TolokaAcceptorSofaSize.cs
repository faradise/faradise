﻿using System;
using System.Collections.Generic;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using System.Linq;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.SofaSize)]
    public class TolokaAcceptorSofaSize : TolokaAcceptor
    {
        public TolokaAcceptorSofaSize() 
            : base("input:id", ProductModerationStatus.Size)
        {  }

        //protected override void ProcessProduct(ProductEntity product, IEnumerable<TolokaOutputRow> rows, Action<int, bool, string> report)
        //{
        //    var outputWidth = rows.AsFloat("OUTPUT:product_width");
        //    var outputLength = rows.AsFloat("OUTPUT:product_length");
        //    var outputHeight = rows.AsFloat("OUTPUT:product_height");
        //    var outputDepth = rows.AsFloat("OUTPUT:product_depth");
        //    var outputDiameter = rows.AsFloat("OUTPUT:product_diameter");

        //    var url = rows.AsString("INPUT:link");
        //    var id = rows.AsInteger("INPUT:id");

        //    //outputWidth = Normalize(outputWidth);
        //    //outputLength = Normalize(outputLength);
        //    //outputHeight = Normalize(outputHeight);
        //    //outputDepth = Normalize(outputDepth);

        //    float? width;
        //    bool widthTrue;
            
        //    width = (outputWidth[0] == outputWidth[1]) ? outputWidth[0] : null;

        //    if (width == null)
        //        width = (outputWidth[1] == outputWidth[2]) ? outputWidth[1] : null;

        //    if (width == null)
        //        width = (outputWidth[0] == outputWidth[2]) ? outputWidth[0] : null;
        //    widthTrue = width != null ? true : false;

        //    float? length;
        //    bool lengthTrue;
        //    length = outputLength[0] == outputLength[1] ? outputLength[0] : null;

        //    if (length == null)
        //        length = outputLength[1] == outputLength[2] ? outputLength[1] : null;

        //    if (length == null)
        //        length = outputLength[0] == outputLength[2] ? outputLength[0] : null;
        //    lengthTrue = length != null ? true : false;

        //    float? height;
        //    bool heightTrue;
        //    height = outputHeight[0] == outputHeight[1] ? outputHeight[0] : null;

        //    if (height == null)
        //        height = outputHeight[1] == outputHeight[2] ? outputHeight[1] : null;

        //    if (height == null)
        //        height = outputHeight[0] == outputHeight[2] ? outputHeight[0] : null;
        //    heightTrue = height != null ? true : false;

        //    float? depth;
        //    bool depthTrue;
        //    depth = outputDepth[0] == outputDepth[1] ? outputDepth[0] : null;

        //    if (depth == null)
        //        depth = outputDepth[1] == outputDepth[2] ? outputDepth[1] : null;

        //    if (depth == null)
        //        depth = outputDepth[0] == outputDepth[2] ? outputDepth[0] : null;
        //    depthTrue = depth != null ? true : false;

        //    float? diameter;
        //    bool diameterTrue;
        //    diameter = outputDiameter[0] == outputDiameter[1] ? outputDiameter[0] : null;

        //    if (diameter == null)
        //        diameter = outputDiameter[1] == outputDiameter[2] ? outputDiameter[1] : null;

        //    if (diameter == null)
        //        diameter = outputDiameter[0] == outputDiameter[2] ? outputDiameter[0] : null;
        //    diameterTrue = diameter != null ? true : false;

        //    float finalHeight = 0;
        //    float finalWidth = 0;
        //    float finalLength = 0;

        //    if(height != null)
        //    {
        //        finalHeight = height.Value;                
        //    }
        //    finalWidth = GetWidthValue(width, length, depth);
        //    finalLength = GetLengthValue(width, length, depth);

        //    if(finalWidth != 0 && diameter != null && finalLength == 0)
        //    {
        //        finalLength = finalWidth;
        //        finalWidth = diameter.Value;
        //    }
        //    else if(diameter != null)
        //    {
        //        finalWidth = diameter.Value;
        //    }

        //    var result = "Width: " + finalWidth + " Length: " + finalLength + " Height: " + finalHeight + "\t" + url[0];
        //    bool isOk = (finalWidth != 0 && finalLength != 0)
        //        || (finalHeight != 0 && finalLength != 0)
        //        || (finalWidth != 0 && finalHeight != 0)
        //        || (diameter != null) ? true : false;

        //    if (isOk)
        //    {
        //        if (finalWidth != 0)
        //            product.Width = finalWidth;

        //        if (finalHeight != 0)
        //            product.Height = finalHeight;

        //        if (finalLength != 0)
        //            product.Length = finalLength;
        //    }

        //    report(id[0].Value, isOk, result);
        //}

        private float?[] Normalize(float?[] input)
        {
            if (input[0] != null)
                input[0] = input[0].Value > 1000 ? input[0].Value / 10 : input[0].Value;

            if (input[1] != null)
                input[1] = input[1].Value > 1000 ? input[1].Value / 10 : input[1].Value;

            if (input[2] != null)
                input[2] = input[2].Value > 1000 ? input[2].Value / 10 : input[2].Value;

            if (input[0] != null)
                input[0] = input[0].Value < 1 ? input[0].Value * 100 : input[0].Value;

            if (input[1] != null)
                input[1] = input[1].Value < 1 ? input[1].Value * 100 : input[1].Value;

            if (input[2] != null)
                input[2] = input[2].Value < 1 ? input[2].Value * 100 : input[2].Value;

            return input;
        }

        private float GetWidthValue(float? a, float? b, float? c)
        {
            List<float?> list = new List<float?>();
            if (a != null)
                list.Add(a);
            if (b != null)
                list.Add(b);
            if (c != null)
                list.Add(c);

            var sortedList = from u in list
                             orderby u.Value descending
                             select u;

            var l = sortedList.ToList();
            if (l.Count >= 1)
                return l[0].Value;
            else
                return 0;
        }

        private float GetLengthValue(float? a, float? b, float? c)
        {
            List<float?> list = new List<float?>();
            if (a != null)
                list.Add(a);
            if (b != null)
                list.Add(b);
            if (c != null)
                list.Add(c);
                        
            var sortedList = from u in list
                             orderby u.Value descending
                             select u;

            var l = sortedList.ToList();
            if (l.Count >= 2)
                return l[1].Value;
            else
                return 0;
        }

        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            throw new NotImplementedException();
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            throw new NotImplementedException();
        }

        protected override string CreateHeader()
        {
            throw new NotImplementedException();
        }
    }
}
