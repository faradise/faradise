﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.AskonaDescription)]
    public class TolokaAcceptorDescriptions : TolokaAcceptor
    {
        private IDBTolokaAcceptorService _dbTolokaService = null;

        public TolokaAcceptorDescriptions()
            : base("input:id", ProductModerationStatus.Description)
        { }

        public override void ResolveServices(IServiceProvider provider)
        {
            _dbTolokaService = (IDBTolokaAcceptorService)provider.GetService(typeof(IDBTolokaAcceptorService));
        }

        protected override string CreateHeader()
        {
            return string.Format("INPUT:id\tINPUT:ids\tOUTPUT:result");
        }
        
        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            var descriptions = row.AsString("OUTPUT:description");
            var ids = row.AsString("INPUT:ids");
            var id = row.AsInteger("INPUT:id");

            var description = FixDescription(descriptions[0]);

            bool isSmall = description.ToCharArray().Length <= 50 ? true : false;
            bool firstIsLower = Char.IsLower(description.ToCharArray()[0]);

            if (!(isSmall || firstIsLower))
            {
                return string.Format("{0}\t{1}\t{2}", id[0].Value, ids[0], description);
            }
            return string.Empty;
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            var descriptions = row.AsString("OUTPUT:result");
            var ids = row.AsString("INPUT:ids");

            var result = _dbTolokaService.SetDescriptions(ids[0], descriptions[0]);
            if (!string.IsNullOrEmpty(result))
                return result;
            else
                return "Error";
        }

        private string FixDescription(string description)
        {
            description = description.Replace("\"", string.Empty);

            Regex fixStartDescription = new Regex(@"^\s+");
            MatchCollection matches = fixStartDescription.Matches(description);
            if (matches.Count > 0)
            {
                description = fixStartDescription.Replace(description, string.Empty);
            }

            description = description.Replace("\t", " ");
            description = description.Replace(" \r\n", "\r\n");
            description = description.Replace("\r\n\r\n\r\n", "\r\n\r\n");
            description = description.Replace("\r\n\r\n\r\n\r\n", "\r\n\r\n");
            description = description.Replace("\r\n\r\n\r\n\r\n\r\n", "\r\n\r\n");

            //Regex fixLineBreak = new Regex(@"((?:\r\n|\r|\n){2})[\r\n]+");
            //MatchCollection matchesLine = fixLineBreak.Matches(description);
            //if (matches.Count > 0)
            //{
            //    description = fixLineBreak.Replace(description, "\r\n\r\n");
            //}

            return description;
        }
    }
}
