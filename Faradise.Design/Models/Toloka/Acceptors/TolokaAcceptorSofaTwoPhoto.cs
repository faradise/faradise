﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.SofaTwoPhoto)]
    public class TolokaAcceptorSofaTwoPhoto : TolokaAcceptor
    {
        public TolokaAcceptorSofaTwoPhoto()
            : base("input:id", ProductModerationStatus.Photo)
        { }

        protected override string CreateHeader()
        {
            throw new NotImplementedException();
        }

        //protected override void ProcessProduct(ProductEntity product, IEnumerable<TolokaOutputRow> rows, Action<int, bool, string> report)
        //{
        //    var photos = rows.AsFloat("OUTPUT:result");

        //    int? photo = null;
        //    if(photos.Length > 0)
        //        photo = (int)photos[0].Value;

        //    bool isPhoto = photo != null ? true : false;

        //    if (photo != null && product.Pictures.Count > photo)
        //        product.Pictures.ToArray()[photo.Value].Priority = 1;

        //    report(product.Id, isPhoto, (photo.Value - 1).ToString());
        //}

        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            throw new NotImplementedException();
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            throw new NotImplementedException();
        }
    }
}
