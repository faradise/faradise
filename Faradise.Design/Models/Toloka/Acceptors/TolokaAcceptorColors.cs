﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using Faradise.Design.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.Colors)]
    public class TolokaAcceptorColors : TolokaAcceptor
    {
        private IDBTolokaAcceptorService _dbTolokaService = null;

        MarketplaceColorEntity[] _marketplaceColors = null;
        CompanyColorEntity[] _companyColors = null;

        public TolokaAcceptorColors()
            : base("input:id", ProductModerationStatus.Color)
        { }

        public override void ResolveServices(IServiceProvider provider)
        {
            try
            {
                _dbTolokaService = (IDBTolokaAcceptorService)provider.GetService(typeof(IDBTolokaAcceptorService));
            }
            catch (Exception e) { }
        }

        protected override string CreateHeader()
        {
            return string.Format("INPUT:id\tOUTPUT:result");
        }
        
        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            var id = row.AsInteger("INPUT:id");
            var outputResults = row.AsString("OUTPUT:result");
            var noPhoto = row.AsString("OUTPUT:nophoto");

            outputResults = Normalize(outputResults);

            List<Dictionary<int, bool>> results = new List<Dictionary<int, bool>>();
            foreach (var res in outputResults)
            {
                Dictionary<int, bool> values = JsonConvert.DeserializeObject<Dictionary<int, bool>>(res);
                results.Add(values);
            }

            if (string.IsNullOrEmpty(noPhoto[0]))
                noPhoto = new string[] { "false", "false", "false" };

            List<int> trueRes = new List<int>();

            if (results.Count == 3)
            {
                for (int i = 1; i <= results[0].Count; i++)
                {
                    if ((results[0][i] == true && results[1][i] == true && bool.Parse(noPhoto[0]) == false && bool.Parse(noPhoto[1]) == false)
                        || (results[1][i] == true && results[2][i] == true && bool.Parse(noPhoto[1]) == false && bool.Parse(noPhoto[2]) == false)
                        || (results[0][i] == true && results[2][i] == true && bool.Parse(noPhoto[0]) == false && bool.Parse(noPhoto[2]) == false))
                    {
                        trueRes.Add(i);
                    }
                }
            }

            if (trueRes.Count != 0)
                return string.Format("{0}\t{1}", id[0].Value, JsonConvert.SerializeObject(trueRes));
            else return string.Empty;
        }

        public override async Task<string> AcceptSegment(
            TolokaOutput output,
            Func<int, ProductEntity> extractor,
            Action<ProductEntity> postprocess)
        {
            try
            {
                var idsToRows = new Dictionary<int, List<TolokaOutputRow>>();

                var logBuilder = new StringBuilder();

                _marketplaceColors = _dbTolokaService.GetMarketplaceColors();
                _companyColors = _dbTolokaService.GetCompanyColors(_marketplaceColors);

                for (var i = 0; i < output.Length; i++)
                {
                    var row = output[i];
                    var productId = row.TakeInteger(_header);

                    if (productId.HasValue)
                    {
                        if (!idsToRows.ContainsKey(productId.Value))
                            idsToRows.Add(productId.Value, new List<TolokaOutputRow>());
                        idsToRows[productId.Value].Add(row);
                    }
                }

                int step = 50;

                for (var i = 0; i < idsToRows.Count / step + 1; i++)
                {
                    var part = idsToRows
                        .Skip(i * step)
                        .Take(step)
                        .ToList();

                    var products = _dbTolokaService.GetProducts(part.Select(x => x.Key).ToList());

                    foreach (var p in part)
                    {
                        try
                        {
                            var product = products.FirstOrDefault(x => x.Id == p.Key);
                            if (product == null)
                            {
                                logBuilder.AppendLine($"Product with id {p.Key} was not found in database");
                                continue;
                            }

                            if (product.ModerationStatus == null)
                                product.ModerationStatus = _moderationStatusOverride;
                            else product.ModerationStatus |= _moderationStatusOverride;

                            logBuilder.AppendLine(base.FixEnd(ProcessProductSegment(product, p.Value)));
                        }
                        catch (Exception e)
                        {
                            logBuilder.AppendLine(base.FixEnd(e.ToString()));
                        }
                    }

                    _dbTolokaService.SetProducts();
                }
            
            return logBuilder.ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            var photo = JsonConvert.DeserializeObject<List<int>>(row.AsString("OUTPUT:result")[0]);
            var id = row.AsInteger("INPUT:id")[0];

            if (photo != null && photo.Count != 0)
            {
                return id + ": " + _dbTolokaService.SetColors(product, photo, _marketplaceColors, _companyColors);
            }
            else
            {
                return id + ": " + "Error";
            }
        }

        private string[] Normalize(string[] texts)
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i] = texts[i].Replace("\"", string.Empty);
            }
            return texts;
        }
    }
}
