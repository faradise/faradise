﻿using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    public static class TolokaAcceptorExtensions
    {
        public static string[] AsString(this IEnumerable<TolokaOutputRow> rows, string key)
        {
            return rows.Select(x => x.Take(key)).ToArray();
        }

        public static int?[] AsInteger(this IEnumerable<TolokaOutputRow> rows, string key)
        {
            return rows.Select(x => x.TakeInteger(key)).ToArray();
        }

        public static float?[] AsFloat(this IEnumerable<TolokaOutputRow> rows, string key)
        {
            return rows.Select(x => x.TakeFloat(key)).ToArray();
        }
    }
}
