﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.SofaDescription)]
    public class TolokaAcceptorSofaDescription : TolokaAcceptor
    {
        public TolokaAcceptorSofaDescription()
            : base("input:id", ProductModerationStatus.Description)
        { }

        protected override string CreateHeader()
        {
            return string.Format("INPUT:id\tOUTPUT:description");
        }

        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            var descriptions = row.AsString("OUTPUT:description");
            var id = row.AsInteger("INPUT:id");
            var description = FixDescription(descriptions[0]);

            if(string.IsNullOrEmpty(description))
                return string.Empty;

            bool isSmall = description.ToCharArray().Length <= 50 ? true : false;
            bool firstIsLower = Char.IsLower(description.ToCharArray()[0]);

            if (!(isSmall || firstIsLower))
            {
                return string.Format("{0}\t{1}", id[0].Value, description);
            }
            return string.Empty;
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            var id = row.AsInteger("INPUT:id");
            var description = row.AsString("OUTPUT:description");

            product.Description = description[0];

            return string.Format("{0} - {1}", id[0].Value, true);
        }

        private string FixDescription(string description)
        {
            description = description.Replace("\"", string.Empty);

            Regex fixStartDescription = new Regex(@"^\s+");
            MatchCollection matches = fixStartDescription.Matches(description);
            if (matches.Count > 0)
            {
                description = fixStartDescription.Replace(description, string.Empty);
            }

            description = description.Replace("\t", " ");
            description = description.Replace(" \r\n", "\r\n");
            description = description.Replace("\r\n\r\n\r\n", "\r\n\r\n");
            description = description.Replace("\r\n\r\n\r\n\r\n", "\r\n\r\n");
            description = description.Replace("\r\n\r\n\r\n\r\n\r\n", "\r\n\r\n");
            
            return description;
        }
    }
}
