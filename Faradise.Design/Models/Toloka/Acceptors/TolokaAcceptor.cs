﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    public abstract class TolokaAcceptor
    {
        protected readonly string _header;

        protected readonly ProductModerationStatus _moderationStatusOverride;

        protected TolokaAcceptor(string header, ProductModerationStatus maskAfterAccept)
        {
            _header = header;
            _moderationStatusOverride = maskAfterAccept;
        }

        public virtual void ResolveServices(IServiceProvider provider)
        {   }

        public async Task Accept(
            TolokaOutput output,
            TolokaOutputType type,
            int taskId,
            Func<string, TolokaOutputType, int, Task<bool>> saveSegment)
        {
            var idsToRows = new Dictionary<int, List<TolokaOutputRow>>();

            var segmentBuilder = new StringBuilder();

            for (var i = 0; i < output.Length; i++)
            {
                var row = output[i];
                var productId = row.TakeInteger(_header);

                if (productId.HasValue)
                {
                    if (!idsToRows.ContainsKey(productId.Value))
                        idsToRows.Add(productId.Value, new List<TolokaOutputRow>());
                    idsToRows[productId.Value].Add(row);
                }
            }

            int counter = 0;
            int maxCount = 500;

            foreach (var v in idsToRows)
            {
                if (segmentBuilder.Length == 0)
                {
                    segmentBuilder.AppendLine(FixEnd(CreateHeader()));
                }

                var processedLine = ProcessProduct(v.Value);
                if (!string.IsNullOrEmpty(processedLine))
                {
                    segmentBuilder.AppendLine(FixEnd(processedLine));

                    counter++;

                    if (counter == maxCount)
                    {
                        counter = 0;
                        await saveSegment(segmentBuilder.ToString(), type, taskId);
                        segmentBuilder.Clear();
                    }
                }
            }            
            if (segmentBuilder.Length != 0)
            {
                await saveSegment(segmentBuilder.ToString(), type, taskId);
                segmentBuilder.Clear();
            }
        }

        public virtual async Task<string> AcceptSegment(
            TolokaOutput output,
            Func<int, ProductEntity> extractor,
            Action<ProductEntity> postprocess)
        {
            var idsToRows = new Dictionary<int, List<TolokaOutputRow>>();

            var logBuilder = new StringBuilder();

            for (var i = 0; i < output.Length; i++)
            {
                var row = output[i];
                var productId = row.TakeInteger(_header);

                if (productId.HasValue)
                {
                    if (!idsToRows.ContainsKey(productId.Value))
                        idsToRows.Add(productId.Value, new List<TolokaOutputRow>());
                    idsToRows[productId.Value].Add(row);
                }
            }

            foreach (var v in idsToRows)
            {
                var product = extractor(v.Key); // new ProductEntity() { Id = 1 }; 
                if (product == null)
                {
                    logBuilder.AppendLine($"Product with id {v.Key} was not found in database");
                    continue;
                }

                if (product.ModerationStatus == null)
                    product.ModerationStatus = _moderationStatusOverride;
                else product.ModerationStatus |= _moderationStatusOverride;

                logBuilder.AppendLine(FixEnd(ProcessProductSegment(product, v.Value)));

                postprocess?.Invoke(product);
            }

            return logBuilder.ToString();
        }

        protected string FixEnd(string str)
        {
            return str + "\t\r";
        }

        protected abstract string ProcessProduct(IEnumerable<TolokaOutputRow> row);
        protected abstract string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row);
        protected abstract string CreateHeader();
    }
}