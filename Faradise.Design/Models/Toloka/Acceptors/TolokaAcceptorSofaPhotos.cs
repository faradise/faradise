﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.DAL;
using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Toloka.Acceptors
{
    [TolokaAcceptorTarget(Type = TolokaOutputType.SofaPhotos)]
    public class TolokaAcceptorSofaPhotos : TolokaAcceptor
    {
        public TolokaAcceptorSofaPhotos()
            : base("input:id", ProductModerationStatus.Photo)
        { }

        protected override string CreateHeader()
        {
            return string.Format("INPUT:id\tOUTPUT:result");
        }

        protected override string ProcessProduct(IEnumerable<TolokaOutputRow> row)
        {
            var photos = row.AsFloat("OUTPUT:result");
            var id = row.AsInteger("INPUT:id");
            var photosUrl = row.AsString("INPUT:photos");

            var photo = FixPhotos(photos);
            var isPhoto = photo != null ? true : false;

            if (isPhoto)
                return string.Format("{0}\t{1}", id[0].Value, photo.Value);
            else return string.Empty;
        }

        protected override string ProcessProductSegment(ProductEntity product, IEnumerable<TolokaOutputRow> row)
        {
            var photo = row.AsInteger("OUTPUT:result")[0];
            var id = row.AsInteger("INPUT:id")[0];

            if (product.Vendor?.ToLower() != "askona" && photo != null && product.Pictures?.Count > photo)
            {
                product.Pictures.ToArray()[photo.Value].Priority = 1;
                return "Ok";
            }
            else
            {
                return "Error";
            }
        }

        private int? FixPhotos(float?[] photos)
        {
            int? result = null;

            if (photos[0] != null && photos[1] != null)
                result = photos[0].Value == photos[1].Value ? (int?)photos[0].Value : null;

            if (result == null && photos[0] != null && photos[2] != null)
                result = photos[0].Value == photos[2].Value ? (int?)photos[0].Value : null;

            if (result == null && photos[1] != null && photos[2] != null)
                result = photos[1].Value == photos[2].Value ? (int?)photos[1].Value : null;

            return result != -1 ? result : null;
        }
    }
}
