﻿using System;
using System.Collections.Generic;

namespace Faradise.Design.Models.Toloka
{
    public class TolokaOutputRow
    {
        private readonly Dictionary<string, string> _values = new Dictionary<string, string>();

        public string this[string index]
        {
            get
            {
                var key = index.Trim().ToLower();

                if (!_values.ContainsKey(key))
                    return null;
                    //throw new Exception($"Heading was not found: {index}");

                return _values[key];
            }
        }

        public void AddRecord(string header, string value)
        {
            _values.Add(header, value);
        }
    }
}
