﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Faradise.Design.Models.Toloka
{
    public class TolokaOutput
    {
        private readonly List<TolokaOutputRow> _rows = new List<TolokaOutputRow>();


        public int Length => _rows.Count;
        public TolokaOutputRow this[int index]
        {
            get
            {
                if (index >= _rows.Count)
                    throw new ArgumentOutOfRangeException(nameof(index));
                return _rows[index];
            }
        }

        public static TolokaOutput Parse(string text)
        {
            var output = new TolokaOutput();

            //text = text.Replace("\r", string.Empty);

            int indexOfChar = text.IndexOf('\n'); // равно 4

            
            var row = text.Substring(0, indexOfChar);
            text = text.Substring(indexOfChar + 1);

            var rows = text.Split(new string[] { "\t\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            
            var headerValues = row
                .Split('\t')
                .Select(x => x.Trim().ToLower())
                .ToArray();

            for (var i = 0; i < rows.Length; i++)
            {
                var textRow = Cleanup(rows[i]);
                var tolokaRow = new TolokaOutputRow();
                var values = textRow.Split('\t');
                var filled = false;

                for (var j = 0; j < headerValues.Length; j++)
                {
                    if (j >= values.Length)
                        continue;

                    tolokaRow.AddRecord(headerValues[j], values[j]);
                    filled = true;
                }

                if (filled)
                    output._rows.Add(tolokaRow);
            }

            return output;
        }

        private static string Cleanup(string s)
        {
            var open = false;

            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == '\"')
                    open = !open;

                if (s[i] == '\t' && open)
                {
                    s = s.Remove(i, 1);
                    i--;
                }
            }

            return s;
        }
    }
}
