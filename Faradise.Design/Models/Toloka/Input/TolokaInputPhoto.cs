﻿namespace Faradise.Design.Models.Toloka.Input
{
    public class TolokaInputPhoto
    {
        public int Id { get; set; }
        public string[] PhotoUrls { get; set; }
    }
}
