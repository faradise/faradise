﻿namespace Faradise.Design.Models.Toloka.Input
{
    public class TolokaInputSize
    {
        public int Id { get; set; }
        
        public string Url { get; set; }
    }
}
