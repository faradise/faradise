﻿using System.Collections.Generic;

namespace Faradise.Design.Models.Toloka.Input
{
    public class TolokaInputProducts
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public List<int> Ids { get; set; } = new List<int>();
    }
}
