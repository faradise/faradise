﻿using Faradise.Design.Controllers.API.Models.Toloka;
using Faradise.Design.Models.Enums;

namespace Faradise.Design.Models.Toloka
{
    public class TolokaTaskSegmentModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public TolokaOutputType Type { get; set; }
        public TolokaTaskStatus Status { get; set; }

        public int TaskId { get; set; }
    }
}
