﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Toloka
{
    public static class TolokaOutputRowExtensions
    {
        public static string Take(this TolokaOutputRow row, string key)
        {
            return row[key];
        }

        public static int? TakeInteger(this TolokaOutputRow row, string key)
        {
            var val = row[key];

            if (int.TryParse(val, out var result))
                return result;
            return null;
        }

        public static float? TakeFloat(this TolokaOutputRow row, string key)
        {
            var val = row[key];

            if (float.TryParse(val, out var result))
                return result;
            return null;
        }
    }
}
