﻿using System;
using Faradise.Design.Controllers.API.Models.Toloka;

namespace Faradise.Design.Models.Toloka
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TolokaAcceptorTargetAttribute : Attribute
    {
        public TolokaOutputType Type { get; set; }
    }
}
