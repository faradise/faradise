﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class ServicePriceInformation
    {
        //Колличество позиций
        public int Count { get; set; }
        
        // Цена за позицию
        public int Amount { get; set;}
        
        //Суммарная цена
        public int SumAmount { get; set; }
    }
}
