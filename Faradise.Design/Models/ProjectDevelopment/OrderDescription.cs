﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class OrderDescription
    {
        public string City { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Description { get; set; }
        public string PromoCode { get; set; }
    }
}
