﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class Collage
    {
        public int RoomId { get; set; }
        public RoomFile[] Photos { get; set; } // Id фото и ссылка на фото с cdn
        public string ArchiveUrl { get; set; }
    }
}
