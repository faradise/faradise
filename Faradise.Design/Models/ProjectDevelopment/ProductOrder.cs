﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class ProductOrder
    {
        public int RoomId { get; set; }
        public int ProductId { get; set; }
        public int Count { get; set; }
    }
}
