﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class Plan
    {
        public int RoomId { get; set; }
        public int Price { get; set; }
        public bool IsSelected { get; set; }
        public PaymentStatus? PaymentStatus { get; set; }
        public bool IsComplete { get; set; }
        public RoomFile[] Files { get; set; } // Id файла и ссылка на файл в cdn
        public string ArchiveUrl { get; set; }
    }
}
