﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class MarketplaceItem
    {
        public int ProductId { get; set; }
        public bool PaidFor { get; set; }
        public int Count { get; set; }
    }
}
