﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public enum DevelopmentStage
    {
        Moodboard = 0,
        Zoning = 1,
        Collage = 2,
        Visualization = 3,
        Plan = 4,
        Brigade = 5,
        Curation = 6,
        WaitForFinish = 7
    }
}
