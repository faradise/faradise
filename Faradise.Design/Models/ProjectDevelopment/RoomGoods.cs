﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class RoomGoods
    {
        public int RoomId { get; set; }
        public List<MarketplaceItem> MarketplaceItems { get; set; }
        public List<OutsideItem> OutsideItems { get; set; }

        public static RoomGoods Empty(int roomId)
        {
            return new RoomGoods()
            {
                RoomId = roomId,
                MarketplaceItems = new List<MarketplaceItem>(),
                OutsideItems = new List<OutsideItem>()
            };
        }
    }
}
