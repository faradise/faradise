﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class DevelopmentStageStatus
    {
        public DevelopmentStage Stage { get; set; }
        public bool DesignerIsReady { get; set; }
        public bool ClientIsReady { get; set; }
        public int TimeLeft { get; set; }
        public bool HasUnfinishedWork { get; set; }

        //TODO : Добавить обработку этого поля в DB
        public bool UserMadeDecision { get; set; }

        public bool IsPaid { get; set; }
        public bool NeedPayment { get; set; }
        public bool PaymentFailed { get; set; }

    }
}
