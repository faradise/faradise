﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.ProjectDevelopment
{
    public class OutsideItem
    {
        public string Name { get; set; }
        public string ImageLink { get; set; }
        public bool PaidFor { get; set; }
        public int Count { get; set; }
    }
}
