﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.DesignerPool
{
    public class RemoveDesigner
    {
        public int DesignerId { get; set; }
        public string Description { get; set; }
    }
}
