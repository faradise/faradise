﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.DesignerPool
{
    public class DesignerDatingInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PortfolioLink { get; set; }
        public string AvatarUrl { get; set; }
        public int Age { get; set; }
        public string City {get; set;}
        public Gender Gender { get; set; }
        public bool PersonalСontrol { get; set; }
    }
}
