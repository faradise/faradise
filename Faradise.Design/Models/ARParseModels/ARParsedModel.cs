﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public class ARParsedModel
    {
        public int FaradiseProductId { get; set; }
        public string GeometryUrl { get; set; }
        public string DiffuseUrl { get; set; }
        public string NormalUrl { get; set; }
        public string MetalicUrl { get; set; }
        public string RoughnessUrl { get; set; }
        public string MetallicRoughnessUrl { get; set; }
    }
}
