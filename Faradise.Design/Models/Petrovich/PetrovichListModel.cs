﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Faradise.Design.Models.Petrovich
{
    public class PetrovichListModel
    {
        public PetrovichListModel()
        {
            PetrovichModels = new List<PetrovichModel>();
        }

        public List<PetrovichModel> PetrovichModels { get; set; }

        public static PetrovichListModel Parse(string text)
        {
            var model = new PetrovichListModel();

            var rows = text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach(var row in rows)
            {
                var petrovichModel = new PetrovichModel();

                var values = row.Split('\t');

                petrovichModel.Id = int.Parse(values[0]);
                petrovichModel.CompanyCategory = values[1];
                petrovichModel.MarketplaceCategory = values[2];

                model.PetrovichModels.Add(petrovichModel);
            }

            for(var i = 0; i < model.PetrovichModels.Count; i++)
            {
                if (GetDepth(model.PetrovichModels[i].MarketplaceCategory) == 0)
                    continue;

                int parentId = i;
                while (GetDepth(model.PetrovichModels[parentId].MarketplaceCategory) != GetDepth(model.PetrovichModels[i].MarketplaceCategory) - 1)
                {
                    parentId--;
                }

                model.PetrovichModels[i].MarketplaceCategoryParent = model.PetrovichModels[parentId].MarketplaceCategory;
            }

            model.PetrovichModels.ForEach(x =>
                {
                    x.CompanyCategory = FixValue(x.CompanyCategory);
                    x.MarketplaceCategory = FixValue(x.MarketplaceCategory);
                    x.MarketplaceCategoryParent = FixValue(x.MarketplaceCategoryParent);
                });

            return model;
        }

        private static string FixValue(string value)
        {
            if (value == null)
                value = string.Empty;
            value = value.Replace("-- ", string.Empty);
            return value;
        }

        private static int GetDepth(string value)
        {
            Regex pattern = new Regex("-- ");
            return pattern.Matches(value).Count;
        }
    }
}
