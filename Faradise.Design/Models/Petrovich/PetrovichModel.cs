﻿namespace Faradise.Design.Models.Petrovich
{
    public class PetrovichModel
    {
        public int Id { get; set; }
        public string CompanyCategory { get; set; }
        public string MarketplaceCategory { get; set; }
        public string MarketplaceCategoryParent { get; set; }
    }
}
