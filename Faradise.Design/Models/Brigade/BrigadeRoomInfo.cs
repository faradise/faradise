﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Brigade
{
    public class BrigadeRoomInfo
    {
        public Room Purpose { get; set; }
        public string[] Zones { get; set; }
        public string ZoneArchive { get; set; }
        public string[] Collages { get; set; }
        public string CollageArchive { get; set; }
        public string[] WorkPlans { get; set; }
        public string WorkPlansArchive { get; set; }
    }
}
