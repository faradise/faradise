﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Brigade
{
    public enum BrigadeWorkStatus
    {
        Created = 0,
        Published = 1,
        Completed = 2
    }
}
