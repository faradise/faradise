﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Brigade
{
    public class BrigadeTechSpec
    {
        public int ProjectId { get; set; }
        public int DesignerId { get; set; }
        public string Description { get; set; }
        public DateTime PublishTime { get; set; }
        public TechSpecFile[] SpecFiles { get; set; }
        public BrigadeWorkStatus Status { get; set; }
        public BrigadeRoomInfo[] RoomInfos { get; set; }
    }
}
