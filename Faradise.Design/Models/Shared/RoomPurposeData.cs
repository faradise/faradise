﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public class RoomPurposeData
    {
        public int RoomId { get; set; }
        public Room Purpose { get; set; }
    }
}
