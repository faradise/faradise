﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public class TechSpecFile
    {
        public int FileId { get; set; }
        public string Link { get; set; }
        public string Name { get; set; }
    }
}
