﻿using Faradise.Design.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment
{
    public class Payment
    {
        public int Id { get; set; }
        public string IdempotenceKey { get; set; }
        public string YandexId { get; set; }
        public string RawJson { get; set; }
        public string PaymentRoute { get; set; }
        public PaymentTargetType PaymentTarget { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public PaymentStatus Status { get; set; }
        public PaymentMethod Method { get; set; }
        public string PlatformRoute { get; set; }
        public string Reciept { get; set; }
    }
}
