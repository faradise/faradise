﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment.JSON
{
    public class YandexKassaPaymentStatusNotification
    {
        public class PaymentObject
        {
            public string id { get; set; }
            public bool test { get; set; }
        }

        public string type { get; set; }
        public string Event { get; set; }
        public PaymentObject Object { get; set; }
    }
}
