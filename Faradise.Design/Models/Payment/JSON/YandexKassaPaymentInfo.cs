﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment.JSON
{
    public class YandexKassaPaymentInfo
    {
        public string id { get; set; }
        public string status { get; set; }
        public string paid { get; set; }
        public string description { get; set; }
    }
}
