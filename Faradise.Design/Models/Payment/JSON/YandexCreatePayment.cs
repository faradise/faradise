﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment.JSON
{
    public class YandexCreatePayment
    {
        public class Amount
        {
            public string value { get; set; }
            public string currency { get; set; }
        }

        public class Confurmation
        {
            public string type;
            public string return_url;
        }

        public class Item
        {
            public string description; //Название товара.
            public string quantity; //Количество.
            public Amount amount;
            public int vat_code; //Ставка НДС. Возможные значения — числа от 1 до 6.
            public string payment_subject;
            public string payment_mode;
        }

        public class Receipt
        {
            public Item[] items;
            public string phone;
            public string email;
            public int tax_system_code;
        }

        public Amount amount { get; set; }
        public Confurmation confirmation { get; set; }
        public string description { get; set; }
        public bool capture { get; set; }
        public Receipt receipt { get; set; }
    }
}
