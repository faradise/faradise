﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment.JSON
{
    public class YandexCreatePaymentResponse
    {
        public class Confirmation
        {
            public string type { get; set; }
            public string return_url { get; set; }
            public string confirmation_url { get; set; }
        }

        public string id { get; set; }
        public string status { get; set; }
        public bool paid { get; set; }
        public bool test { get; set; }
        public Confirmation confirmation { get; set; }
    }
}
