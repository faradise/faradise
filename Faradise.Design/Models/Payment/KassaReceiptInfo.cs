﻿using Faradise.Design.Internal.Exceptions;
using Faradise.Design.Models.Marketplace;
using Faradise.Design.Models.ProjectDevelopment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Payment
{
    public class KassaReceiptInfo
    {
        public class Item
        {
            public string Description { get; set; }
            public int Quantity { get; set; }
            public int Amount { get; set; }
            public string PaymentSubject { get; set; }
        }

        public List<Item> Items { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public static KassaReceiptInfo CreateForVisualization(ServicePriceInformation priceInfo, string email, string phone)
        {
            return new KassaReceiptInfo()
            {
                Email = email,
                Phone = phone,
                Items = new List<Item> { new Item { Description = "3D визуализация комнаты", Amount = priceInfo.Amount, Quantity = priceInfo.Count, PaymentSubject = "service" } }
            };
        }

        public static KassaReceiptInfo CreateForPlan(ServicePriceInformation priceInfo, string email, string phone)
        {
            return new KassaReceiptInfo()
            {
                Email = email,
                Phone = phone,
                Items = new List<Item> { new Item { Description = "Рабочий чертеж комнаты", Amount = priceInfo.Amount, Quantity = priceInfo.Count, PaymentSubject = "service" } }
            };
        }

        public static KassaReceiptInfo CreateForProducts(List<OrderedProduct> products, int shipmentPrice, int additionalprice, int absoluteDiscount, string email, string phone)
        {
            var items = new List<Item>();
            var canDiscount = new Dictionary<Item, bool>(products.Count);
            foreach (var product in products)
            {
                var item = new Item()
                {
                    Amount = product.Price,
                    Quantity = product.Count,
                    Description = product.Name.Replace("\"", string.Empty),
                    PaymentSubject = "commodity"
                };
                items.Add(item);
                canDiscount.Add(item, product.SupportPromoCode);
            }
            if (absoluteDiscount > 0)
            {
                var itemSum = products.Where(x => x.SupportPromoCode).Sum(x => (x.Price-1) * x.Count);
                var sumDiscount = absoluteDiscount;
                foreach (var item in items)
                {
                    if (canDiscount[item])
                    {
                        var weigth = (float)((item.Amount-1) * item.Quantity) / itemSum;
                        var discountPerItem = Math.Min(item.Amount - 1, (int)(sumDiscount * weigth / item.Quantity));
                        item.Amount -= discountPerItem;
                        absoluteDiscount -= discountPerItem * item.Quantity;
                    }
                }
                int safeCounter = 100;
                if (absoluteDiscount > 0)
                {
                    var bestItem = items.FirstOrDefault(x => canDiscount[x] && x.Amount > absoluteDiscount && x.Quantity == 1);
                    if (bestItem != null)
                    {
                        bestItem.Amount -= absoluteDiscount;
                        absoluteDiscount = 0;
                    }
                }
                while (absoluteDiscount > 0 && safeCounter > 0)
                {
                    var item = items.Where(x => canDiscount[x] && x.Amount > 1).OrderBy(x => x.Quantity).ThenByDescending(x => x.Amount).FirstOrDefault();
                    if (item == null)
                        break;
                    var discount = Math.Min(item.Amount - 1, absoluteDiscount);
                    if (item.Quantity > 1)
                    {
                        item.Quantity--;
                        items.Add(new Item { Amount = item.Amount - discount, Quantity = item.Quantity, Description = item.Description, PaymentSubject = "commodity" });
                        absoluteDiscount -= discount;
                    }
                    else
                    {
                        item.Amount = item.Amount - discount;
                        absoluteDiscount -= discount;
                    }
                    safeCounter--;
                }
                if (safeCounter == 0 || absoluteDiscount > 0)
                    throw new ServiceException("unable to make item discount for absolute pice compensation", "item_price_compemsation_failed");
            }
            if (shipmentPrice > 0)
                items.Add(new Item() { Amount = shipmentPrice, Quantity = 1, Description = "Доставка", PaymentSubject = "service" });
            if (additionalprice > 0)
                items.Add(new Item() { Amount = additionalprice, Quantity = 1, Description = "Дополнительные расходы", PaymentSubject = "service" });
            return new KassaReceiptInfo()
            {
                Email = email,
                Phone = phone,
                Items = items
            };
        }
    }

}
