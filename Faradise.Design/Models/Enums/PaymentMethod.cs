﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum PaymentMethod
    {
        YandexKassa = 0,
        Tinkoff = 1,
        Cash = 2
    }
}
