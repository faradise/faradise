﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum SaleRange
    {
        Marketplace = 0,
        Categories = 1,
        Companies = 2,
        Products = 3,
        UnMapped = 4
    }
}
