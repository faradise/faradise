﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public enum CooperateStage
    {
        Meeting = 0,
        PhotoAndProportions = 1,
        Style = 2,
        Budget = 3,
        AdditionalInfo = 4,
        WhatToSave = 5
    }
}
