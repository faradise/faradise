﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models
{
    public enum PaymentStatus
    {
        Pending = 0,
        Canceled = 1,
        Completed = 2,
        Manual = 3,
        Created = 4
    }
}
