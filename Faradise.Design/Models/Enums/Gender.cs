﻿namespace Faradise.Design.Models
{
    public enum Gender
    {
        Unknown = 0,
        Female,
        Male
    }
}
