﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum YMLUpdateTaskStatus
    {
        Pending = 0,
        InProcess = 1,
        Failed = 2,
        Succeeded = 3,
    }
}
