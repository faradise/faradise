﻿namespace Faradise.Design.Models
{
    public enum ProjectReason
    {
        Renew = 0,
        Renovation,
        MoveToNewPlace,
        SearchForNewFurniture,
        StartLivingTogether,
        NewHouse,
        NewOffice,
        Custom
    }
}
