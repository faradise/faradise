﻿namespace Faradise.Design.Models.Enums
{
    public enum DesignerRegistrationStage
    {
        Registered,
        SendTestTask,
        TestApproved,
        ReadyToWork
    }
}
