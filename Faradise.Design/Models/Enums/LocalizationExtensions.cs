﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public static class LocalizationExtensions
    {
        public static string Localize(this Room room)
        {
            switch (room)
            {
                case Room.Bathroom:
                    return "Туалет и/или ванная";
                case Room.BedRoom:
                    return "Спальная";
                case Room.ChildrenRoom:
                    return "Детская";
                case Room.Custom:
                    return "Другое";
                case Room.Hallway:
                    return "Прихожая";
                case Room.Kitchen:
                    return "Кухня";
                case Room.LivingRoom:
                    return "Гостиная";
                case Room.LoggiaOrBalcony:
                    return "Лоджия или балкон";
                case Room.Parlor:
                    return "Кабинет";
                case Room.Wardrobe:
                    return "Гардероб";
            }
            return "Не найдено";
        }

        public static string Localize(this ProjectStyle style)
        {
            switch (style)
            {
                case ProjectStyle.ArtDeco:
                    return "Арт-Деко";
                case ProjectStyle.Country:
                    return "Кантри";
                case ProjectStyle.Eclecticism:
                    return "Эклектика";
                case ProjectStyle.Fusion:
                    return "Фьюжн";
                case ProjectStyle.Industrial:
                    return "Индустриальный";
                case ProjectStyle.Minimalism:
                    return "Минимализм";
                case ProjectStyle.Modern:
                    return "Современный";
                case ProjectStyle.ModernClassic:
                    return "Современная класика";
                case ProjectStyle.Provence:
                    return "Прованс";
                case ProjectStyle.Scandinavian:
                    return "Скандинавский";
            }
            return "Не найдено";
        }

        public static string Localize(this ProjectReason style)
        {
            switch (style)
            {
                case ProjectReason.Custom:
                    return "Другое";
                case ProjectReason.MoveToNewPlace:
                    return "Переезд";
                case ProjectReason.NewHouse:
                    return "Новый дом";
                case ProjectReason.NewOffice:
                    return "Новый офис";
                case ProjectReason.Renew:
                    return "Обновление";
                case ProjectReason.Renovation:
                    return "Ремонт";
                case ProjectReason.SearchForNewFurniture:
                    return "Поиск новой мебели";
                case ProjectReason.StartLivingTogether:
                    return "Начали жить вместе";
            }
            return "Не найдено";
        }
    }
}
