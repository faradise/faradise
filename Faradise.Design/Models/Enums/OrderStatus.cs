﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum OrderStatus
    {
        Created = 0,
        WaitingForPayment = 1,
        Paid = 2,
        InWork = 3,
        Completed = 4,
        Canceled = 5,

        WaitingForCreditApprove = 10,
        CreditApprovedWaitingForManager = 11,
        CreditApprovedInWork = 12,
        CreditNotApproved = 13,
        CanceledCredit = 14
    }
}
