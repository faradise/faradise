﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum RoomArchiveType
    {
        Moodboard = 0,
        Collages,
        Zones,
        Plans,
        Visualization
    }
}
