﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum PromocodeError
    {
        None = 0,
        NotFound = 1,
        Expired = 2,
        Used = 3,
        NotStarted = 4,
        InsufficientPrice = 5,
        NoProductsForApply = 6
    }
}
