﻿namespace Faradise.Design.Models.Enums
{
    public enum TolokaTaskStatus
    {
        Created,
        Processing,
        Processed,
        Error
    }
}
