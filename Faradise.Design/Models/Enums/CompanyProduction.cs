﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    [Flags]
    public enum CompanyProduction
    {
        Unknown = 0,
        Furniture = 1, //мебель
        DesignFurniture = 2, //дизайнерская мебель
        Materials = 4, //отделочные материалы
        Electronics = 8 //электроника
    }
}
