﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum UploadFiletype
    {
        Unknown = 0,
        YML = 1,
        EXCEL = 2
    }
}
