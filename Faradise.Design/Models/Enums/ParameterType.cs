﻿namespace Faradise.Design.Models.Enums
{
    public enum ParameterType
    {
        Integer,
        String
    }
}