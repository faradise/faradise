﻿namespace Faradise.Design.Models
{
    public enum ProjectStyle
    {
        Eclecticism = 0,
        Provence,
        Fusion,
        ArtDeco,
        Country,
        ModernClassic,
        Scandinavian,
        Industrial,
        Modern,
        Minimalism
    }
}
