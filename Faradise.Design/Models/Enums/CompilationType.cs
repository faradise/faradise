﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum CompilationSource
    {
        ProductList = 0,
        Categories = 1
    }
}
