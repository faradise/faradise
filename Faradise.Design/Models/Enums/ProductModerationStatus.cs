﻿using System;

namespace Faradise.Design.Models.Enums
{
    [Flags]
    public enum ProductModerationStatus
    {
        Empty = 0,
        Photo = 1,
        Size = 2,
        Weight = 4,
        Description = 8,
        Color = 16,
        Category = 32,
        Parameters = 64,
        Rating = 128,
        CargoSize = 256,
        Additions = 512,
    }
}
