﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum DiscountType
    {
        Percent = 0,
        Absolute = 1
    }
}
