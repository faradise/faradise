﻿namespace Faradise.Design.Models
{
    public enum Room
    {
        BedRoom = 0,
        LivingRoom,
        Hallway,
        Parlor,
        Kitchen,
        ChildrenRoom,
        Wardrobe,
        LoggiaOrBalcony,
        Bathroom,
        Custom
    }
}
