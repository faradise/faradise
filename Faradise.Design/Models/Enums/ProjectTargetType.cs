﻿namespace Faradise.Design.Models
{
    public enum ProjectTargetType
    {
        Apartment = 0,
        CityHouse,
        VacationHouse,
        Office,
        Cafe
    }
}
