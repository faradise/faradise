﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum YMLFeedType
    {
        Full = 0,
        Feed10k = 1
    }
}
