﻿namespace Faradise.Design.Models
{
    /// <summary>
    /// Юридическое или физическое лицо
    /// </summary>
    public enum LegalType
    {
        Personal,
        Company
    }
}
