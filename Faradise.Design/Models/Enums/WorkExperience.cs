﻿namespace Faradise.Design.Models.Enums
{
    public enum WorkExperience
    {
        LessOneYear,
        OneToThreeYears,
        ThreeToFiveYears,
        MoreThanFiveYears
    }
}
