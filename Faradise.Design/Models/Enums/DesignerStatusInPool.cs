﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum DesignerStatusInPool
    {
        Appointed,
        Approved,
        Rejected
    }
}
