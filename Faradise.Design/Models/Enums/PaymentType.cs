﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum PaymentTargetType
    {
        VisualizationFreelance = 1,
        WorkPlans = 2,
        MarketplaceGoods = 3,
        BasketMarketplaceGoods = 4
    }
}
