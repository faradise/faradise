﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Faradise.Design.Models.Enums
{
    public enum FilterOrdering
    {
        None = 0,
        Ascending = 1,
        Descending = 2,
        Popularity = 3,
        AR = 4,
        Sale = 5
    }
}
