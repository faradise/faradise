﻿namespace Faradise.Design.Models
{
    /// <summary>
    /// Тариф
    /// </summary>
    public enum DesignerLevel
    {
        Novice,
        Specialist,
        Professional
    }
}
