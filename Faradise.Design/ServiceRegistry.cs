﻿using Faradise.Design.DAL;
using Faradise.Design.Models.DAL;
using Faradise.Design.Services;
using Faradise.Design.Services.AzureSearch;
using Faradise.Design.Services.Background;
using Faradise.Design.Services.Configs;
using Faradise.Design.Services.Implementation;
using Faradise.Design.Services.Implementation.MailSending;
using Faradise.Design.Services.Mock;
using Faradise.Design.Services.Production.DB;
using Faradise.Design.Services.Toloka;
using Faradise.Design.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Faradise.Design
{
    public static class ServiceRegistry
    {
        private static void RegisterOptions(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<SMSOptions>(configuration.GetSection("SMS"));
            services.Configure<CDNOptions>(configuration.GetSection("CDN"));
            services.Configure<TimeOptions>(configuration.GetSection("TimeOptions"));
            services.Configure<PriceOptions>(configuration.GetSection("PriceOptions"));
            services.Configure<DesignerPoolOptions>(configuration.GetSection("DesignerPoolOptions"));
            services.Configure<EmailOptions>(configuration.GetSection("EmailOptions"));
            services.Configure<YandexKassaOptions>(configuration.GetSection("YandexKassaOptions"));
            services.Configure<ServicesOptions>(configuration.GetSection("Services"));
            services.Configure<MarketplaceOptions>(configuration.GetSection("MarketplaceOptions"));
            services.Configure<AzureSearchOptions>(configuration.GetSection("AzureSearch"));
        }

        private static void RegisterBackgroundServices(IServiceCollection services)
        {
            services.AddTransient<MarketplaceProcessingBackgroundService>();
            services.AddTransient<PictureUploadBackgroundService>();
            services.AddTransient<PaymentCheckBackgroundService>();
            services.AddTransient<FeedsBackgroundService>();
            services.AddTransient<ArchiveUpdateBackgroundService>();
            services.AddTransient<CompanyFeedAcceptorBackgroundService>();
            services.AddTransient<AutoFeedTaskManagerBackgroundService>();
            services.AddTransient<IndexUpdaterService>();
            services.AddTransient<TolokaTaskBackgroundService>();
            services.AddTransient<TolokaTaskSegmentBackgroundService>();
            services.AddTransient<CacheWarmingBackgroundService>();
            services.AddTransient<FeedAnalyzerBackgroundService>();
            services.AddTransient<ProductCollectionBackgroudService>();
        }

        private static void RegisterDBContext(IServiceCollection services, IConfiguration configuration)
        {
            var cs = configuration.GetSection("DB")["ConnectionString"];

            services.AddDbContext<FaradiseDBContext>(options =>
                options
                    .UseLazyLoadingProxies()
                    .UseSqlServer(cs));

            services.AddDbContext<FaradiseDBQueryContext>(options =>
                options
                    .UseSqlServer(cs)
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
        }

        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            RegisterOptions(services, configuration);
            RegisterBackgroundServices(services);
            RegisterDBContext(services, configuration);

            if (FaradiseEnvironment.Integration)
                services.AddTransient<IDBMarketplaceQueryServiceProvider, DBMarketplaceQueryService>();
            else services.AddTransient<IDBMarketplaceQueryServiceProvider, DBMarketplaceQueryAzureService>();
            services.AddTransient<IDBMarketplaceQueryService, DBMarketplaceQueryCachedService>();
            services.AddTransient<DBMarketplaceQueryService>();

            services.AddTransient<DBCompilationQueryService>();
            services.AddTransient<IDBCompilationQueryService, DBCompilationQueryCachedService>();

            services.AddTransient<IDBCompilationService, DBCompilationService>();
            services.AddTransient<ICompilationService, CompilationService>();

            services.AddTransient<ICompressionService, CompressService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IDBProjectDefinitionService, DBProjectDefinitionService>();
            services.AddTransient<IProjectDefinitionService, ProjectDefinitionService>();
            services.AddTransient<IClientProjectService, ClientProjectService>();
            services.AddTransient<IDBClientProjectService, DBClientProjectService>();
            services.AddTransient<IDBUserService, DBUserService>();
            services.AddTransient<ITokenService, JWTTokenService>();

            services.AddTransient<IDBDesignerPoolService, DBDesignerPoolService>();
            services.AddTransient<IDesignerPoolService, DesignerPoolService>();

            services.AddTransient<IDBProjectDevelopmentService, DBProjectDevelopmentService>();
            services.AddTransient<IDBProjectOrderService, DBProjectOrderService>();
            services.AddTransient<IProjectDevelopmentService, ProjectDevelopmentService>();

            services.AddTransient<IDBProjectChatService, DBProjectChatService>();
            services.AddTransient<IProjectChatService, ProjectChatService>();

            services.AddSingleton<ICacheService, RedisCacheService>();
            services.AddSingleton<IPersistentStorageService, PersistentStorageService>();

            services.AddTransient<IDBPictureExtractorQueryService, DBPictureExtractorQueryService>();
            services.AddTransient<IDBPictureExtractorService, DBPictureExtractorService>();

            services.AddTransient<IDBPromoCodeService, DBPromoCodeService>();
            services.AddTransient<IPromoCodeService, PromoCodeService>();

            services.AddTransient<IDBAdminCompanyService, DBAdminCompanyService>();

            if (FaradiseEnvironment.Development || FaradiseEnvironment.Stable || FaradiseEnvironment.Integration)
            {
                services.AddSingleton<ISMSService, MockSMSService>();
                services.AddSingleton<IMailSendingService, MockMailSendingService>();
                services.AddTransient<IValidationService, MockValidationService>();
                services.AddTransient<IPaymentService, MockPaymentService>();
            }
            else
            {
                services.AddTransient<ISMSService, SMSService>();
                services.AddSingleton<IMailSendingService, UnisenderMailService>();
                services.AddTransient<IValidationService, ValidationService>();
                services.AddTransient<IPaymentService, PaymentService>();
            }


            services.AddTransient<IDesignerService, DesignerService>();
            services.AddTransient<IDBDesignerService, DBDesignerService>();
            services.AddSingleton<ICDNService, CDNService>();

            services.AddTransient<IProjectPreparationsService, ProjectPreparationsService>();
            services.AddTransient<IDBProjectFlowService, DBProjectFlowService>();

            services.AddTransient<IAdminService, AdminService>();
            services.AddTransient<IMarketplaceService, MarketplaceService>();
            services.AddTransient<IDBMarketplaceService, DBMarketplaceService>();
            services.AddTransient<IBasketService, BasketService>();
            services.AddTransient<IDBBasketService, DBBasketService>();
            services.AddTransient<IDBAdminMarketplaceService, DBAdminMarketplaceService>();
            services.AddTransient<IIndexerService, IndexerService>();
            services.AddSingleton<ISearchService, SearchService>();

            services.AddSingleton<PhoneUtility>();

            services.AddTransient<IDBVisualizationFreelanceService, DBVisualizationFreelanceService>();
            services.AddTransient<IVisualizationFreelanceService, VisualizationFreelanceService>();

            services.AddTransient<IDBBrigadeService, DBBrigadeService>();
            services.AddTransient<IBrigadeService, BrigadeService>();

            services.AddTransient<IDBPaymentService, DBPaymentService>();

            services.AddTransient<IDBTolokaSourceService, DBTolokaSourceService>();
            services.AddTransient<ITolokaAcceptorService, TolokaAcceptorService>();
            services.AddTransient<ITolokaService, TolokaService>();
            services.AddTransient<IDBTolokaService, DBTolokaService>();
            services.AddTransient<IDBTolokaAcceptorService, DBTolokaAcceptorService>();

            services.AddTransient<IDBBadgeService, DBBadgeService>();
            services.AddTransient<IBadgeService, BadgeService>();

            services.AddTransient<IDBSaleService, DBSaleService>();
            services.AddTransient<ISaleService, SaleService>();

            services.AddTransient<IDBFeedService, DBFeedService>();

            services.AddTransient<IDBArchiveService, DBArchiveService>();
            services.AddTransient<IArchiveService, ArchiveService>();

            services.AddTransient<IDBTildaService, DBTildaService>();

            services.AddTransient<IDBBannerService, DBBannerService>();
            services.AddTransient<DBBannerQueryService>();
            services.AddTransient<IDBBannerQueryService, DBBannerQueryCachedService>();

            services.AddTransient<IDBCorruptedPicturesService, DBCorruptedPicturesService>();

            services.AddTransient<IDBCategoryParameterService, DBCategoryParameterService>();

            services.AddTransient<IDBProductCollectionService, DBProductCollectionService>();
            services.AddTransient<IProductCollectionService, ProductCollectionService>();
        }
    }
}