﻿using System;
using System.Linq;
using Faradise.Design.Middleware;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Faradise.Design.Models.DAL;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Faradise.Design.DAL;
using Faradise.Design.Services;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace Faradise.Design
{
    public class Startup
    {
        public static DateTime TimeStarted { get; }

        static Startup()
        {
            TimeStarted = DateTime.Now;
        }

        public Startup(IConfiguration configuration)
        { 
            Configuration = configuration;
            if (FaradiseEnvironment.Integration)
                return;
            SetupSerilog();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("Cors", builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            ServiceRegistry.Configure(services, Configuration);

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddFaradiseSwagger();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env,
            ILoggerFactory logger, Microsoft.AspNetCore.Hosting.IApplicationLifetime lifetime)
        {
            logger.AddConsole(Configuration.GetSection("Logging"));
            logger.AddDebug();

            app.UseCors("Cors");

            app.UseLoggerHandlerMiddleware();
            app.UseExceptionHandler();
            app.UseExceptionHandlerMiddleware();
            app.UseMvc();

            app.UseAuthentication();
            app.UseFaradiseSwagger();

            SetupDatabase(app);
            ClearCache(app);
        }

        private void ClearCache(IApplicationBuilder app)
        {
            try
            {
                using (var serviceScope = app.ApplicationServices
                    .GetRequiredService<IServiceScopeFactory>()
                    .CreateScope())
                {
                    var cache = serviceScope.ServiceProvider.GetService<ICacheService>();
                    cache.Drop();
                }
            }
            catch (Exception)
            {
            }
        }

        private void SetupDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<FaradiseDBContext>();
                context.Database.Migrate();

                if (!context.MarketplaceEntity.Any())
                {
                    context.MarketplaceEntity.Add(new MarketplaceEntity {Name = "master"});
                    context.SaveChanges();
                }
            }
        }

        private void SetupSerilog()
        {
            var address = Configuration["Elastic"];
            var uri = new Uri(address);
            var logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Destructure.ByTransforming<CustomLog>(log => new
                {
                    message = log.Message,
                    args = log.Args.Select(JsonConvert.SerializeObject)
                })
                .Filter.ByIncludingOnly(MatchingExtensions.FromBaseClass<LogBase>())
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(uri)
                {
                    AutoRegisterTemplate = true
                })
                .CreateLogger();

            Log.Logger = logger;
        }
    }
}